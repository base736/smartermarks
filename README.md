# SmarterMarks #

SmarterMarks (online at [smartermarks.com](http://smartermarks.com)) is an open-source web tool that helps teachers build and score assessments, and interpret results. The project includes:

* A question editor, including tools to change formatting, insert and edit lists, tables, images, and variables, and a full WYSIWYG equation editor;
* An assessment generator that allows users to quickly build and version assessments;
* An engine for building PDF forms and for reading student responses from a scanned PDF;
* Analysis tools that allow users to easily interpret the results of assessments, including results collected from many classes or over several years; and
* Communities that make sharing questions and assessments easy.

### The repository ###

This repository has been created to open access to our code base. If you have questions or would like to contribute, I'd love to hear from you at [jason@smartermarks.com](mailto:jason@smartermarks.com)!
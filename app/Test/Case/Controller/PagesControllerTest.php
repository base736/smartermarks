<?php
App::uses('PagesController', 'Controller');

/**
 * PagesController Test Case
 *
 */
class PagesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.page',
		'app.document',
		'app.user',
		'app.section',
		'app.nr_section_question'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testContact method
 *
 * @return void
 */
	public function testContact() {
	}

}

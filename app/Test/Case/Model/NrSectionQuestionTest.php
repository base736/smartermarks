<?php
App::uses('NrSectionQuestion', 'Model');

/**
 * NrSectionQuestion Test Case
 *
 */
class NrSectionQuestionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.nr_section_question',
		'app.section',
		'app.page',
		'app.document',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NrSectionQuestion = ClassRegistry::init('NrSectionQuestion');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NrSectionQuestion);

		parent::tearDown();
	}

}

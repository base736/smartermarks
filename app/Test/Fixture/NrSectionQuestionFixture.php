<?php
/**
 * NrSectionQuestionFixture
 *
 */
class NrSectionQuestionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'primary'),
		'section_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'answer' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'answerFormat' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'fudgeFactor' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'allowPartialMatch' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 5),
		'category' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'updated' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'section_id' => 1,
			'answer' => 'Lorem ipsum dolor sit amet',
			'answerFormat' => 'Lorem ipsum dolor sit a',
			'fudgeFactor' => 'Lorem ipsum dolor sit a',
			'allowPartialMatch' => 1,
			'category' => 'Lorem ipsum dolor sit amet',
			'updated' => '2012-11-21 16:52:59',
			'created' => '2012-11-21 16:52:59'
		),
	);

}

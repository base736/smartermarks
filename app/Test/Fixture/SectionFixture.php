<?php
/**
 * SectionFixture
 *
 */
class SectionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'primary'),
		'page_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'section_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'header' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'columnCount' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'mc_choiceCount' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'questionCount' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'mc_answers' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'order' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'updated' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'page_id' => 1,
			'section_type' => 'Lorem ipsum dolor ',
			'header' => 'Lorem ipsum dolor sit amet',
			'columnCount' => 1,
			'mc_choiceCount' => 1,
			'questionCount' => 1,
			'mc_answers' => 'Lorem ipsum dolor sit amet',
			'order' => 1,
			'updated' => '2012-11-21 17:50:19',
			'created' => '2012-11-21 17:50:19'
		),
	);

}

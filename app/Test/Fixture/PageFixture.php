<?php
/**
 * PageFixture
 *
 */
class PageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'primary'),
		'document_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'title_showIDField' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 5),
		'title_idDigits' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'title_text' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'order' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'document_id' => 1,
			'title_showIDField' => 1,
			'title_idDigits' => 1,
			'title_text' => 'Lorem ipsum dolor sit amet',
			'order' => 1,
			'created' => '2012-11-21 16:52:31'
		),
	);

}

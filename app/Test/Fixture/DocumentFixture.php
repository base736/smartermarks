<?php
/**
 * DocumentFixture
 *
 */
class DocumentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'save_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'showAnswers' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 5),
		'copiesPerPage' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
		'topMarginSize' => array('type' => 'float', 'null' => false, 'default' => null),
		'bottomMarginSize' => array('type' => 'float', 'null' => false, 'default' => null),
		'leftMarginSize' => array('type' => 'float', 'null' => false, 'default' => null),
		'rightMarginSize' => array('type' => 'float', 'null' => false, 'default' => null),
		'updated' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'save_name' => 'Lorem ipsum dolor sit amet',
			'showAnswers' => 1,
			'copiesPerPage' => 1,
			'topMarginSize' => 1,
			'bottomMarginSize' => 1,
			'leftMarginSize' => 1,
			'rightMarginSize' => 1,
			'updated' => '2012-11-21 16:51:56',
			'created' => '2012-11-21 16:51:56'
		),
	);

}

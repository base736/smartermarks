/**************************************************************************************************/
/* SmarterMarks Statistical Model                                                                 */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

// This regularization constant ensures that "long-range" effects in student theta are dampened.
// Consider two courses, A and B, the latter at a higher grade level, which have some overlap (as is
// often the case in Physics, for example). Without regularization, even a single shared item will 
// tend to result in students in A fitting to lower thetas than those in B, since they have greater
// difficulty on that shared item. As a result, other items only on A's assessment will be fitted as 
// easier than they would be had they not appeared on an assessment with that shared item. While 
// arguably correct, this has the effect that derived difficulties for many items assessing A won't 
// match those obtained more directly from student scores. Regularization helps ensure that thetas
// and item difficulties reflect individual differences more than they do course level.

const theta_regularization = 5.0e-4;

class Correlation {
    constructor() {
        this.reset();
    }

    reset() {
        this.sumX = 0.0;
        this.sumY = 0.0;
        this.sumX2 = 0.0;
        this.sumXY = 0.0;
        this.sumY2 = 0.0;
        this.n = 0.0;

        this.sumX2_w = 0.0;
        this.sumXY_w = 0.0;
        this.sumY2_w = 0.0;
        this.sumWeight = 0.0;

        this.sumReg = 0.0;
    }

    pushValues(log_a, b, c, theta, score, weight) {
        const x = score;

        let y;
        const exponent = Math.exp(log_a) * (b - theta);
        if (exponent < 0) {
            const expValue = Math.exp(exponent);
            y = c + (1.0 - c) / (1.0 + expValue);
        } else {
            const expValue = Math.exp(-exponent);
            y = c + (1.0 - c) * expValue / (expValue + 1.0);
        }

        this.sumX += x;
        this.sumY += y;
        this.sumX2 += x * x;
        this.sumXY += x * y;
        this.sumY2 += y * y;
        this.n += 1;

        this.sumX2_w += x * x * weight;
        this.sumXY_w += x * y * weight;
        this.sumY2_w += y * y * weight;
        this.sumWeight += weight;

        this.sumReg += theta_regularization * Math.pow(theta, 2.0);
    }

    getErrorObjective() {
        return (this.sumX2_w + this.sumY2_w - 2.0 * this.sumXY_w) * this.n / this.sumWeight + this.sumReg;
    }

    getCorrelation(blurSigma) {
        const variance = blurSigma * blurSigma;

        const numerator = (this.sumXY / this.n) - (this.sumX / this.n) * (this.sumY / this.n);
        const denominator =
            Math.sqrt(this.sumX2 / this.n - Math.pow(this.sumX / this.n, 2.0) + variance) *
            Math.sqrt(this.sumY2 / this.n - Math.pow(this.sumY / this.n, 2.0) + variance);
        const correlation = denominator === 0.0 ? 0.0 : numerator / denominator;

        const varianceMax = 0.25; // Maximum variance of a set of numbers on the interval [0,1]
        const normalization = varianceMax / (varianceMax + variance);

        return correlation / normalization;
    }

    static add(a, b) {
        const c = new Correlation();

        c.sumX = a.sumX + b.sumX;
        c.sumY = a.sumY + b.sumY;
        c.sumX2 = a.sumX2 + b.sumX2;
        c.sumXY = a.sumXY + b.sumXY;
        c.sumY2 = a.sumY2 + b.sumY2;
        c.n = a.n + b.n;

        return c;
    }

    static subtract(a, b) {
        const c = new Correlation();

        c.sumX = a.sumX - b.sumX;
        c.sumY = a.sumY - b.sumY;
        c.sumX2 = a.sumX2 - b.sumX2;
        c.sumXY = a.sumXY - b.sumXY;
        c.sumY2 = a.sumY2 - b.sumY2;
        c.n = a.n - b.n;

        return c;
    }
}

export { Correlation };

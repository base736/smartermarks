/**************************************************************************************************/
/* SmarterMarks Statistical Model                                                                 */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { promises as fs } from 'fs';
import schedule from 'node-schedule';
import { Op } from 'sequelize';
import { Question, QuestionHandle, ResultSet, ResultData, VersionData } from './models/index.js';

import * as ScoringCommon from '/js/common/scoring.js';
import { Correlation } from './Correlation.js';

// Read the secrets.json file

import config from './secrets.json' with { type: 'json' };

// Function to generate a timed log entry

async function timedLog(message) {
    const now = new Date();
    const timestamp = now.toISOString();
    const logMessage = `[${timestamp}] ${message}\n`;
    await fs.appendFile('status.log', logMessage, 'utf8');
}

// Random number generator functions

function randUniform() {
    return Math.random();
}

function randNormal() {

    // Box-Muller transform

    let u = 0, v = 0;
    while (u === 0) u = Math.random(); // Convert [0,1) to (0,1)
    while (v === 0) v = Math.random();
    return Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
}

// CDF for a standard normal distribution

function normalCDF(x) {
    var z = x / Math.sqrt(2);
    var t = 1 / ( 1 + 0.3275911 * Math.abs(z));
    var a1 =  0.254829592;
    var a2 = -0.284496736;
    var a3 =  1.421413741;
    var a4 = -1.453152027;
    var a5 =  1.061405429;
    var erf = 1 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.exp(-z * z);
    var sign = (z < 0) ? -1 : 1;
    return 0.5 * (1 + sign * erf);
}

// Data structures

let studentData = [];
let itemData = [];

const responseBlockSize = 1e6;

let responseCount = 0;
let responseIsOmitted = [];
let responseStudentIndex = [];
let responseItemIndex = [];
let responseScore = [];

// Helper functions for block operations

function getBlockValue(data, index) {
    let dataBlock = Math.trunc(index / responseBlockSize);
    let dataOffset = index % responseBlockSize;
    return data[dataBlock][dataOffset];
}

function setBlockValue(data, index, value) {
    let dataBlock = Math.trunc(index / responseBlockSize);
    let dataOffset = index % responseBlockSize;
    data[dataBlock][dataOffset] = value;
}

// Candidate standard deviations

let temperature_init = 0.1;
const temperature_lambda = 0.05;
let c_theta_init = 0.1;
let c_b_init = 0.1;

let c_theta, c_b;

// Fit parameters

const targetAcceptance = 0.25;     // Target acceptance
const acceptanceP = 0.5;           // Proprortionality constant for acceptance controller

// Schedule for saving parameters

const saveSchedule = '30 2,10,18 * * *'; // Save parameters around 4am, noon, and 8pm Mountain time
var saveJob = false;

// Global variables

var mode = 'load';
var iteration = 0;

async function addResponseData(studentIndex, itemIndex, score) {
    let responseBlock = Math.trunc(responseCount / responseBlockSize);
    let responseOffset = responseCount % responseBlockSize;

    if (responseBlock >= responseIsOmitted.length) {
        if ((responseBlock === responseIsOmitted.length) && (responseOffset === 0)) {
            responseIsOmitted.push(new Uint8Array(responseBlockSize));
            responseStudentIndex.push(new Uint32Array(responseBlockSize));
            responseItemIndex.push(new Uint32Array(responseBlockSize));
            responseScore.push(new Float32Array(responseBlockSize));
        } else {
            throw new Error('Bad block and offset when filling response data.');
        }
    }

    responseIsOmitted[responseBlock][responseOffset] = 0;
    responseStudentIndex[responseBlock][responseOffset] = studentIndex;
    responseItemIndex[responseBlock][responseOffset] = itemIndex;
    responseScore[responseBlock][responseOffset] = score;

    studentData[studentIndex].responseIndices.push(responseCount);
    studentData[studentIndex].isOmitted = 0;

    itemData[itemIndex].responseIndices.push(responseCount);
    itemData[itemIndex].isOmitted = 0;

    responseCount++;
    if (responseCount % 1e6 === 0) {
        await timedLog(`${responseCount / 1e6}M responses: ${Math.round(process.memoryUsage().heapTotal / 1e6)}M used`);
    }
}

async function loadData() {

    await timedLog('Loading response data...');

    // Mark existing student and item data as processed and create index maps

    const studentIndexMap = {};
    const oldStudentIDs = new Set();
    studentData.forEach((student, index) => {
        studentIndexMap[student.studentID] = index;
        oldStudentIDs.add(student.studentID);
    });

    const itemIndexMap = {};
    const oldQuestionIDs = new Set();
    itemData.forEach((item, index) => {
        const questionID = item.questionID;
        const partID = item.partID;
        const partKey = `${questionID}.${partID}`;

        itemIndexMap[partKey] = index;
        oldQuestionIDs.add(questionID);
    });

    // Load ResultSets, checking old ones for new data

    let minResultSetID = 0;
    const chunkSize = 20;

    let resultSets = [];
    do {
        resultSets = await ResultSet.findAll({
            attributes: ['id'],
            where: {
                id: {
                    [Op.gt]: minResultSetID
                }
            },
            order: [['id', 'ASC']],
            limit: chunkSize,
            include: [
                {
                    model: ResultData,
                    as: 'ResultData',
                    attributes: ['id', 'results', 'version_data_id']
                }
            ]
        });
        if (resultSets.length == 0) break;
    
        resultSets = resultSets.map((resultSet) => resultSet.get({ plain: true }));
        
        // Collect all necessary VersionData IDs

        const versionDataIDs = new Set();
        resultSets.forEach((resultSet) => {
            resultSet.ResultData.forEach((thisData) => {
                versionDataIDs.add(thisData.version_data_id);
            });
        });

        // Bulk fetch scoring data

        const scoringDataJSON = await VersionData.findAll({
            attributes: ['id', 'scoring_json'],
            where: { id: Array.from(versionDataIDs) }
        });

        const scoringDataMap = {};
        scoringDataJSON.forEach((entry) => {
            const scoringData = JSON.parse(entry.scoring_json);
            scoringDataMap[entry.id] = scoringData;
        });

        // Process results using cached data

        for (const resultSet of resultSets) {
            if (resultSet.alpha <= 0.0) continue;
            if (resultSet.average_score <= 0.3) continue;
            if (resultSet.average_score >= 0.9) continue;

            var hasNewStudents = false;
            for (const resultData of resultSet.ResultData) {
                const studentID = resultData.id;
                const studentIsNew = !oldStudentIDs.has(studentID);
                hasNewStudents |= studentIsNew;
            }

            // Get QuestionHandle IDs for this ResultSet

            const questionHandleIDs = new Set();
            for (const resultData of resultSet.ResultData) {
                const versionDataID = resultData.version_data_id;
                
                if (!versionDataID || !(versionDataID in scoringDataMap)) {
                    throw new Error('Missing VersionData ID in loadData');
                }

                const scoringData = scoringDataMap[versionDataID];

                scoringData.Sections?.forEach((section) => {
                    section.Content?.forEach((root_entry) => {
                        if (root_entry.Source?.question_handle_id) {
                            questionHandleIDs.add(root_entry.Source.question_handle_id);
                        }
                    });
                });
            }

            if (questionHandleIDs.size > 0) {

                // Bulk fetch QuestionHandles

                const questionHandles = await QuestionHandle.findAll({
                    where: { id: Array.from(questionHandleIDs) },
                    include: [
                        {
                            model: Question,
                            as: 'Question',
                            attributes: ['id', 'question_content_id', 'content_ids']
                        }
                    ]
                });

                const questionDataMap = {};
                const questionContentIDs = new Set();

                questionHandles.forEach((questionHandle) => {
                    const questionHandlePlain = questionHandle.get({ plain: true });
                    questionDataMap[questionHandlePlain.id] = questionHandlePlain;
                    if (questionHandlePlain.Question.question_content_id) {
                        questionContentIDs.add(questionHandlePlain.Question.question_content_id);
                    }
                });

                // Bulk fetch questions by content ID

                const matchingQuestions = await Question.findAll({
                    where: {
                        question_content_id: Array.from(questionContentIDs)
                    },
                    attributes: ['id', 'question_content_id', 'json_data', 'content_ids', 'stats_json'],
                });

                const matchingQuestionsMap = {};
                var hasNewQuestions = false;
                matchingQuestions.forEach((question) => {
                    const questionPlain = question.get({ plain: true });
                    const questionContentID = questionPlain.question_content_id;

                    if (!(questionContentID in matchingQuestionsMap)) {
                        matchingQuestionsMap[questionContentID] = [];
                    }
                    matchingQuestionsMap[questionContentID].push(questionPlain);

                    const questionID = questionPlain.id;
                    const questionIsNew = !oldQuestionIDs.has(questionID);
                    hasNewQuestions |= questionIsNew;
                });

                if (hasNewStudents || hasNewQuestions) {

                    const versionQuestions = {};
                    for (const resultData of resultSet.ResultData) {
    
                        const studentID = resultData.id;
                        const versionDataID = resultData.version_data_id;
                        const studentIsNew = !oldStudentIDs.has(studentID);
    
                        if (!(versionDataID in versionQuestions)) {
    
                            // Get and cache question data for this version
    
                            const scoringData = scoringDataMap[versionDataID];
    
                            const questionSets = [];
    
                            for (const section of scoringData.Sections) {
                                if (!('Content' in section)) continue;
    
                                for (const rootEntry of section.Content) {
                                    if ('scoring' in rootEntry) {

                                        questionSets.push(false);

                                    } else {

                                        let questionSet = [];
    
                                        const questionHandleID = rootEntry.Source?.question_handle_id;
                                        if (questionHandleID && (questionHandleID in questionDataMap)) {
                                            const partID = rootEntry.Source.part_id;
        
                                            const rootQuestion = questionDataMap[questionHandleID];
                                            const questionContentID = rootQuestion.Question.question_content_id;
                                            const rootContentIDs = JSON.parse(rootQuestion.Question.content_ids);
        
                                            if ((questionContentID !== null) && partID && (partID in rootContentIDs)) {
                                                const partContentID = rootContentIDs[partID];
        
                                                if (!(questionContentID in matchingQuestionsMap)) {
                                                    throw new Error('Missing question contentID in loadData');
                                                }
                
                                                // Process matching questions
        
                                                matchingQuestionsMap[questionContentID].forEach((matchingQuestion) => {
                    
                                                    const targetQuestionID = matchingQuestion.id;
                                                    const targetData = JSON.parse(matchingQuestion.json_data);
                                                    const targetContentIDs = JSON.parse(matchingQuestion.content_ids);
        
                                                    const targetPartData = targetData.Parts.find(
                                                        (part) => targetContentIDs[part.id] === partContentID
                                                    );
        
                                                    if (targetPartData) {
                                                        if (targetPartData.type === 'mc_table') targetPartData.type = 'mc';
                                                        const style = targetPartData.type.slice(0, 2);
        
                                                        const entry = {
                                                            type: targetPartData.type,
                                                            Source: {
                                                                question_id: targetQuestionID,
                                                                part_id: targetPartData.id
                                                            }
                                                        };
        
                                                        if (matchingQuestion.stats_json !== null) {
                                                            const statsData = JSON.parse(matchingQuestion.stats_json);
                                                            if (targetPartData.id in statsData) {
                                                                entry.Parameters = {
                                                                    b: statsData[targetPartData.id].b
                                                                };    
                                                            }
                                                        }
        
                                                        // Build newAnswers for this question. newAnswers entries map answers to the root
                                                        // question to answer IDs in the target question. These are then scored according to
                                                        // the ScoredResponses entry of the target question, allowing responses from other
                                                        // items that present the same way to students to be used to inform stats on the
                                                        // target question.
        
                                                        let saveQuestion = true;
        
                                                        if (targetPartData.type !== rootEntry.type) {
        
                                                            saveQuestion = false;
        
                                                        } else if (style === 'mc') {
        
                                                            // Process multiple choice
        
                                                            const targetAnswers = targetData.Common?.Answers || targetPartData?.Answers;
        
                                                            const newAnswers = [];
                                                            for (const rootAnswer of rootEntry.Answers) {    
                                                                if (rootAnswer.id in rootContentIDs) {
                                                                    const rootContentID = rootContentIDs[rootAnswer.id];
        
                                                                    for (const targetAnswer of targetAnswers) {
                                                                        if (targetAnswer.id in targetContentIDs) {
                                                                            const targetContentID = targetContentIDs[targetAnswer.id];
        
                                                                            if (targetContentID == rootContentID) {
                                                                                newAnswers.push({
                                                                                    id: targetAnswer.id,
                                                                                    response: rootAnswer.response
                                                                                });
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
        
                                                            if (newAnswers.length == targetAnswers.length) {
                                                                entry.Answers = newAnswers;
                                                                entry.ScoredResponses = targetPartData.ScoredResponses;    
                                                            } else saveQuestion = false;
        
                                                        } else if (style === 'nr') {
        
                                                            // Process numeric response
        
                                                            if (!targetData.Engine.js) {
        
                                                                // No randomized and calculated variables -- leave responses as-is
        
                                                                const newAnswers = targetPartData.Answers.map(targetAnswer => ({
                                                                    id: targetAnswer.id,
                                                                    response: targetAnswer.template,
                                                                }));
                                                                
                                                                entry.Answers = newAnswers;
                                                                entry.ScoredResponses = targetPartData.ScoredResponses;
        
                                                            } else {
        
                                                                // Question has variables -- need to get equivalent responses
        
                                                                const targetAnswers = targetPartData.Answers;
                                                                const newAnswers = [];
        
                                                                for (const rootAnswer of rootEntry.Answers) {
                                                                    if (rootAnswer.id in rootContentIDs) {
                                                                        const rootContentID = rootContentIDs[rootAnswer.id];
                    
                                                                        for (const targetAnswer of targetAnswers) {
                                                                            if (targetAnswer.id in targetContentIDs) {
                                                                                const targetContentID = targetContentIDs[targetAnswer.id];
        
                                                                                if (targetContentID == rootContentID) {
                                                                                    newAnswers.push({
                                                                                        id: targetAnswer.id,
                                                                                        response: rootAnswer.response
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
        
                                                                if (newAnswers.length == targetAnswers.length) {
                                                                    entry.Answers = newAnswers;
                                                                    entry.ScoredResponses = targetPartData.ScoredResponses;    
                                                                } else saveQuestion = false;
                                                            }
        
                                                            entry.TypeDetails = targetPartData.TypeDetails;
        
                                                        } else if (style === 'wr') {
        
                                                            // Process written response
        
                                                            entry.TypeDetails = targetPartData.TypeDetails;
        
                                                        }
        
                                                        if (saveQuestion) {
                                                            questionSet.push(ScoringCommon.augmentQuestion(entry));
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                        questionSets.push(questionSet);

                                    }
                                }
                            }
                            versionQuestions[versionDataID] = questionSets;
                        }
    
                        // Score responses and populate data arrays
    
                        const questionSets = versionQuestions[versionDataID];
                        const responseData = JSON.parse(resultData.results);
                        if (responseData.length !== questionSets.length) {
                            continue;
        //                    throw new Error('Question count mismatch loadData');                    
                        }
    
                        for (let i = 0; i < questionSets.length; ++i) {
                            if (questionSets[i] === false) continue;
                            
                            for (const questionEntry of questionSets[i]) {
                                if (!questionEntry.Source?.question_id) continue;
                                if (questionEntry.maxValue === 0.0) continue;
    
                                const questionID = questionEntry.Source.question_id;
                                const itemIsNew = !oldQuestionIDs.has(questionID);

                                // Check that question has not yet been added for this student (possible if two equivalent
                                // questions are used in the same assessment) and that either the student or the item was not
                                // added in a previous call to loadData().
    
                                if (studentIsNew || itemIsNew) {
                                    const partID = questionEntry.Source.part_id;
                                    const partKey = `${questionID}.${partID}`;

                                    const result = ScoringCommon.scoreResponse(responseData[i], questionEntry);
                                    if (result === false) continue;

                                    const x = result.score / questionEntry.maxValue;

                                    if (!(partKey in itemIndexMap)) {
            
                                        // Add item data
            
                                        const newItem = {
                                            questionID: questionID,
                                            partID: partID,
                                            maxValue: questionEntry.maxValue,
                                            responseIndices: []
                                        };
            
                                        if ('Parameters' in questionEntry) {
                                            newItem.b = questionEntry.Parameters.b;
                                        }
            
                                        itemIndexMap[partKey] = itemData.length;
                                        itemData.push(newItem);
                                    }
            
                                    // Add student data if needed
                                    
                                    if (!(studentID in studentIndexMap)) {
                                        const newStudent = {
                                            studentID: resultData.id,
                                            responseIndices: [],
                                        };
            
                                        studentIndexMap[studentID] = studentData.length;
                                        studentData.push(newStudent);        
                                    }
                                    
                                    // Add response data
    
                                    const itemIndex = itemIndexMap[partKey];
                                    const studentIndex = studentIndexMap[studentID];
                                    await addResponseData(studentIndex, itemIndex, x);
    
                                }
                            }
                        }
                    }
                }
            }
        }

        minResultSetID = resultSets[resultSets.length - 1].id;
    } while (resultSets.length >= chunkSize);
}

async function cleanData() {

    // Initialize isOmitted
    
    studentData.forEach((student) => {
        student.isOmitted = (student.responseIndices.length > 0) ? 0 : 1;
    });

    itemData.forEach((item) => {
        item.isOmitted = (item.responseIndices.length > 0) ? 0 : 1;
    });

    for (let i = 0; i < responseCount; ++i) {
        setBlockValue(responseIsOmitted, i, 0);
    }

    // Omit students and items with answers that are all correct or all incorrect

    await timedLog('Omitting edge cases...');

    studentData.forEach((student) => {
        if (student.isOmitted) return;

        let firstScore = -1;
        let omitStudent = true;

        student.responseIndices.forEach((index) => {
            const score = getBlockValue(responseScore, index);
            if (firstScore === -1) firstScore = score;
            if (score > 0.0 && score < 1.0) omitStudent = false;
            else if (score !== firstScore) omitStudent = false;
        });

        if (omitStudent) student.isOmitted = 1;
    });

    itemData.forEach((item) => {
        if (item.isOmitted) return;

        let firstScore = -1;
        let omitItem = true;

        item.responseIndices.forEach((index) => {
            const score = getBlockValue(responseScore, index);
            if (firstScore === -1) firstScore = score;
            if (score > 0.0 && score < 1.0) omitItem = false;
            else if (score !== firstScore) omitItem = false;
        });

        if (omitItem) item.isOmitted = 1;
    });

    // Omit students and items with no un-omitted responses until none remain

    await timedLog('Omitting to consistency...');

    var done;
    do {
        done = true;

        studentData.forEach((student) => {
            if (student.isOmitted) return;
    
            let omitStudent = true;    
            student.responseIndices.forEach((index) => {
                const itemIndex = getBlockValue(responseItemIndex, index);
                if (!itemData[itemIndex].isOmitted) omitStudent = false;
            });
    
            if (omitStudent) {
                student.isOmitted = 1;
                done = false;
            }
        });

        itemData.forEach((item) => {
            if (item.isOmitted) return;
    
            let omitItem = true;    
            item.responseIndices.forEach((index) => {
                const studentIndex = getBlockValue(responseStudentIndex, index);
                if (!studentData[studentIndex].isOmitted) omitItem = false;
            });
    
            if (omitItem) {
                item.isOmitted = 1;
                done = false;
            }
        });

    } while (!done);

    for (let i = 0; i < responseCount; ++i) {
        const itemIndex = getBlockValue(responseItemIndex, i);
        if (itemData[itemIndex].isOmitted) {
            setBlockValue(responseIsOmitted, i, 1);
            continue;
        }
        
        const studentIndex = getBlockValue(responseStudentIndex, i);
        if (studentData[studentIndex].isOmitted) {
            setBlockValue(responseIsOmitted, i, 1);
            continue;
        }
    }
}

async function initModel() {
    
    // Initialize student parameters

    await timedLog('Initializing student parameters...');

    let theta_average = 0.0;
    let theta_n = 0;

    studentData.forEach((student) => {
        if (student.isOmitted) return;

        let totalScore = 0.0;
        student.responseIndices.forEach((index) => {
            totalScore += getBlockValue(responseScore, index);
        });

        const outOf = student.responseIndices.length;
        if ((totalScore <= 0.0) || (totalScore >= outOf)) {
            throw new Error('Bad total score in student');
        }

        let thetaRaw = Math.log(totalScore / (outOf - totalScore));

        theta_average += thetaRaw;
        theta_n++;

        if (!('theta' in student)) student.theta_raw = thetaRaw;  // Converted to theta below
    });

    theta_average /= theta_n;
    studentData.forEach((student) => {
        if (student.isOmitted) return;
        if ('theta_raw' in student) {
            student.theta = student.theta_raw - theta_average;
            delete student.theta_raw;
        }
    });

    // Initialize item parameters

    await timedLog('Initializing item parameters...');

    let b_average = 0.0;
    let b_n = 0;

    itemData.forEach((item) => {
        if (item.isOmitted) return;

        let totalScore = 0.0;
        item.responseIndices.forEach((index) => {
            totalScore += getBlockValue(responseScore, index);
        });

        const outOf = item.responseIndices.length;
        if ((totalScore <= 0.0) || (totalScore >= outOf)) {
            throw new Error('Bad total score in item');
        }

        let bRaw = Math.log((outOf - totalScore) / totalScore);

        b_average += bRaw;
        b_n++;

        if (!('log_a' in item)) item.log_a = 0.0;  // Initial estimate for a
        if (!('b' in item)) item.b_raw = bRaw;     // Converted to b below
        if (!('c' in item)) item.c = 0.0;          // Set to zero because we assume good distractors, not guessing
    });

    b_average /= b_n;
    itemData.forEach((item) => {
        if (item.isOmitted) return;
        if ('b_raw' in item) {
            item.b = item.b_raw - b_average;
            delete item.b_raw;
        }
    });

    // Initialize candidate distributions

    c_theta = c_theta_init;
    c_b = c_b_init;
}

async function stepModelFit(temperature) {

    // Step student parameters

    let student_accepted = 0;
    let student_total = 0;

    studentData.forEach((student) => {
        if (student.isOmitted) return;

        const theta_last = student.theta;
        const theta_proposed = randNormal() * c_theta + theta_last;

        const delta_last = new Correlation();
        const delta_proposed = new Correlation();

        student.responseIndices.forEach((index) => {
            if (getBlockValue(responseIsOmitted, index)) return;

            const itemIndex = getBlockValue(responseItemIndex, index);
            const score = getBlockValue(responseScore, index);
            const item = itemData[itemIndex];

            const weight = 1.0 / (student.responseIndices.length * item.responseIndices.length);
            delta_last.pushValues(item.log_a, item.b, item.c, theta_last, score, weight);
            delta_proposed.pushValues(item.log_a, item.b, item.c, theta_proposed, score, weight);
        });

        const f_last = delta_last.getErrorObjective();
        const f_proposed = delta_proposed.getErrorObjective();

        let isAccepted;
        if (temperature > 0.0) {
            const acceptance = Math.exp(-(f_proposed - f_last) / temperature);
            isAccepted = randUniform() <= acceptance;
        } else {
            isAccepted = f_proposed < f_last;
        }

        if (isAccepted) {
            student_accepted++;
            student.theta = theta_proposed;
        }
        student_total++;
    });

    // Adjust step sizes to give a desired acceptance rate

    const student_rate = student_accepted / student_total;
    const studentDifference = student_rate - targetAcceptance;
    c_theta = Math.exp(Math.log(c_theta) + acceptanceP * studentDifference);

    // Transform item parameters to leave expected scores invariant when theta -> (theta + offset) * scale.

    var sum_theta = 0.0;
    var sum_theta2 = 0.0;
    var theta_n = 0;

    studentData.forEach((student) => {
        if (student.isOmitted) return;
        sum_theta += student.theta;
        sum_theta2 += student.theta * student.theta;
        theta_n++;
    });

    const theta_mean = sum_theta / theta_n;
    const theta_meanSquare = sum_theta2 / theta_n;
    const theta_stdev = Math.sqrt(theta_meanSquare - theta_mean * theta_mean);

    const offset = -theta_mean;
    const scale = 1.0 / theta_stdev;

    // Apply normalization to student thetas

    studentData.forEach((student) => {
        if (student.isOmitted) return;
        student.theta = (student.theta + offset) * scale;
    });

    // Apply normalization to item parameters

    itemData.forEach((item) => {
        if (item.isOmitted) return;
        item.log_a -= Math.log(scale);
        item.b = (item.b + offset) * scale;
    });

    // Step item 'b'

    let b_accepted = 0;
    let b_total = 0;

    itemData.forEach((item) => {
        if (item.isOmitted) return;

        const b_last = item.b;
        const b_proposed = randNormal() * c_b + b_last;

        const delta_last = new Correlation();
        const delta_proposed = new Correlation();

        item.responseIndices.forEach((index) => {
            if (getBlockValue(responseIsOmitted, index)) return;

            const studentIndex = getBlockValue(responseStudentIndex, index);
            const score = getBlockValue(responseScore, index);
            const student = studentData[studentIndex];

            const weight = 1.0 / (student.responseIndices.length * item.responseIndices.length);
            delta_last.pushValues(item.log_a, b_last, item.c, student.theta, score, weight);
            delta_proposed.pushValues(item.log_a, b_proposed, item.c, student.theta, score, weight);
        });

        const f_last = delta_last.getErrorObjective();
        const f_proposed = delta_proposed.getErrorObjective();

        let isAccepted;
        if (temperature > 0.0) {
            const acceptance = Math.exp(-(f_proposed - f_last) / temperature);
            isAccepted = randUniform() <= acceptance;
        } else {
            isAccepted = f_proposed < f_last;
        }

        if (isAccepted) {
            b_accepted++;
            item.b = b_proposed;
        }
        b_total++;
    });

    // Adjust step size to give a desired acceptance rate

    const b_rate = b_accepted / b_total;
    const itemDifferenceB = b_rate - targetAcceptance;
    c_b = Math.exp(Math.log(c_b) + acceptanceP * itemDifferenceB);
}

function getFitError() {

    // Get fit error for the whole data set

    const correlation_all = new Correlation();
    var includedCount = 0;

    for (let i = 0; i < responseCount; ++i) {
        if (getBlockValue(responseIsOmitted, i)) continue;

        const itemIndex = getBlockValue(responseItemIndex, i);
        const item = itemData[itemIndex];

        const studentIndex = getBlockValue(responseStudentIndex, i);
        const student = studentData[studentIndex];

        const score = getBlockValue(responseScore, i);

        const weight = 1.0 / (student.responseIndices.length * item.responseIndices.length);
        correlation_all.pushValues(item.log_a, item.b, item.c, student.theta, score, weight);
        ++includedCount;
    }

    return correlation_all.getErrorObjective() / includedCount;
}

function setMode(newMode) {
    mode = newMode;
}

async function stepSolution() {

    // Fit item parameters

    let temperature = temperature_init * Math.exp(-temperature_lambda * iteration);
    await stepModelFit(temperature);

    let f_current = getFitError();
    const logMessage = `${iteration}   ${c_theta.toFixed(5)} ${c_b.toFixed(5)}   ${f_current.toExponential(10)}`;
    await timedLog(logMessage);

    ++iteration;
}

async function saveParameters() {

    // Save item parameters to database

    await timedLog('Saving item parameters...');

    var statsBuffer = {};

    // Gather part statistics for each question

    for (const item of itemData) {
        if (item.isOmitted) continue;

        const correlation_item = new Correlation();
        var partResponseCount = 0;
        item.responseIndices.forEach((index) => {
            if (getBlockValue(responseIsOmitted, index)) return;
            ++partResponseCount;

            const studentIndex = getBlockValue(responseStudentIndex, index);
            const score = getBlockValue(responseScore, index);
            const student = studentData[studentIndex];
    
            correlation_item.pushValues(item.log_a, item.b, item.c, student.theta, score);
        });

        const blurSigma = 0.1;
        const correlation = correlation_item.getCorrelation(blurSigma);

        if (!(item.questionID in statsBuffer)) {
            statsBuffer[item.questionID] = {};
        }
        statsBuffer[item.questionID][item.partID] = {
            maxValue: item.maxValue,
            n: partResponseCount,
            log_a: item.log_a,
            b: item.b,
            correlation: correlation
        };
    }

    // Prepare for stats update

    const updateSecret = config.stats_server.secret;

    const willUpdateURL = config.stats_server.will_update_url;
    await fetch(`${willUpdateURL}?secret=${updateSecret}`, { method: 'GET' });

    // Save stats for each question to the database
    
    const statsChunkSize = 100;
    const questionIDs = Object.keys(statsBuffer);

    for (let i = 0; i < questionIDs.length; i += statsChunkSize) {
        const thisQuestionIDs = questionIDs.slice(i, i + statsChunkSize);

        const sendBuffer = [];
        for (const questionID of thisQuestionIDs) {
            const questionStatsData = statsBuffer[questionID];

            var itemResponseCount = 0;
            var questionDifficulty = 0.0;
            var questionDiscrimination = 0.0;
            var totalMaxValue = 0.0;
            for (const partStatsData of Object.values(questionStatsData)) {
                const partMaxValue = partStatsData.maxValue;
                delete partStatsData.maxValue;

                const partResponseCount = partStatsData.n;
                delete partStatsData.n;

                // Take the max since some parts may sometimes have been made bonus/omitted questions
                // (giving different counts for each part)

                itemResponseCount = Math.max(itemResponseCount, partResponseCount);

                // Add to sums for item difficulty and discrimination

                questionDifficulty += normalCDF(-partStatsData.b) * partMaxValue;
                questionDiscrimination += partStatsData.correlation * partMaxValue;
                totalMaxValue += partMaxValue;
            }
            questionDifficulty /= totalMaxValue;
            questionDiscrimination /= totalMaxValue;

            sendBuffer.push({
                id: parseInt(questionID),
                stats_count: itemResponseCount,
                stats_difficulty: questionDifficulty,
                stats_discrimination: questionDiscrimination,
                stats_json: JSON.stringify(questionStatsData)
            });
        }

        await Question.bulkCreate(sendBuffer, {
            fields: ['id', 'stats_count', 'stats_difficulty', 'stats_discrimination', 'stats_json'],
            updateOnDuplicate: ['stats_count', 'stats_difficulty', 'stats_discrimination', 'stats_json']
        });
    }

    // Prompt the main server to update the question index

    const didUpdateURL = config.stats_server.did_update_url;
    await fetch(`${didUpdateURL}?secret=${updateSecret}`, { method: 'GET' });
}

async function runLoop() {

    if (mode === 'load') {

        await loadData();
        await cleanData();
        await initModel();

        if (saveJob === false) {
            await timedLog("Scheduling parameter save");

            saveJob = schedule.scheduleJob(saveSchedule, () => {
                setMode('save');
            });
        }

        await timedLog('Solving for item parameters...');

        iteration = 0;
        setMode('solve');

    } else if (mode === 'solve') {

        await stepSolution();

    } else if (mode === 'save') {

        await saveParameters();
        setMode('load');

    } else {

        throw new Error(`Unexpected mode "${mode}".`);

    }

    // Schedule next iteration

    setImmediate(runLoop);
}

runLoop();

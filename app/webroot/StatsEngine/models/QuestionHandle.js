/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export default (sequelize, DataTypes) => {
    const QuestionHandle = sequelize.define(
        'QuestionHandle',
        {
            id: {
                type: DataTypes.INTEGER(20),
                primaryKey: true,
                autoIncrement: true
            },
            question_id: {
                type: DataTypes.INTEGER(20),
                allowNull: false
            },
            community_id: {
                type: DataTypes.INTEGER(20),
                allowNull: true
            },
            user_id: {
                type: DataTypes.INTEGER(20),
                allowNull: true
            },
            sitting_id: {
                type: DataTypes.INTEGER(20),
                allowNull: true
            },
            name_alias: {
                type: DataTypes.TEXT('medium'),
                allowNull: false,
                collate: 'utf8mb4_unicode_ci'
            }
        },
        {
            tableName: 'question_handles',
            timestamps: false,
            indexes: [
                {
                    name: 'idx_question_id',
                    fields: ['question_id']
                },
                {
                    name: 'idx_community_id',
                    fields: ['community_id']
                },
                {
                    name: 'idx_user_id',
                    fields: ['user_id']
                },
                {
                    name: 'idx_sitting_id',
                    fields: ['sitting_id']
                }
            ]
        }
    );

    QuestionHandle.associate = (models) => {
        QuestionHandle.belongsTo(models.Question, {
            foreignKey: 'question_id',
            as: 'Question'
        });
/*
        QuestionHandle.hasMany(models.UserQuestion, {
            foreignKey: 'question_handle_id',
            as: 'UserQuestions',
            onDelete: 'CASCADE',
            hooks: true
        });

        QuestionHandle.hasMany(models.AssessmentItem, {
            foreignKey: 'question_handle_id',
            as: 'AssessmentItems',
            onDelete: 'CASCADE',
            hooks: true
        });
*/
    };

    return QuestionHandle;
};

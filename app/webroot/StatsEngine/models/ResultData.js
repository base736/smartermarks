/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export default (sequelize, DataTypes) => {
    const ResultData = sequelize.define(
        'ResultData',
        {
            id: {
                type: DataTypes.INTEGER(20),
                primaryKey: true,
                autoIncrement: true
            },
            result_set_id: {
                type: DataTypes.INTEGER(20),
                allowNull: false
            },
            version_data_id: {
                type: DataTypes.INTEGER(20),
                allowNull: true
            },
            results: {
                type: DataTypes.TEXT('medium'),
                allowNull: false,
                collate: 'utf8mb4_unicode_ci'
            }
        },
        {
            tableName: 'result_data',
            timestamps: false,
            indexes: [
                {
                    name: 'idx_result_set_id',
                    fields: ['result_set_id']
                },
                {
                    name: 'idx_version_data_id',
                    fields: ['version_data_id']
                }
            ]
        }
    );

    ResultData.associate = (models) => {
        ResultData.belongsTo(models.VersionData, {
            foreignKey: 'version_data_id',
            as: 'VersionData',
        });

        ResultData.belongsTo(models.ResultSet, {
            foreignKey: 'result_set_id',
            as: 'ResultSet',
        });
    };

    return ResultData;
};
  
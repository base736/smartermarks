/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export default (sequelize, DataTypes) => {
    const VersionData = sequelize.define(
        'VersionData',
        {
            id: {
                type: DataTypes.INTEGER(20),
                primaryKey: true,
                autoIncrement: true
            },
            html_gzip: {
                type: DataTypes.BLOB('medium'),
                allowNull: true
            },
            html: {
                type: DataTypes.TEXT('long'),
                allowNull: true,
                collate: 'utf8mb4_unicode_ci'
            },
            scoring_json: {
                type: DataTypes.TEXT('long'),
                allowNull: false,
                collate: 'utf8mb4_unicode_ci'
            }
        },
        {
            tableName: 'version_data',
            timestamps: false
        }
    );

    VersionData.associate = (models) => {
/*
        VersionData.hasMany(models.Document, {
            foreignKey: 'version_data_id',
            as: 'Documents',
            onDelete: 'CASCADE',
            hooks: true
        });
*/
        VersionData.hasMany(models.ResultData, {
            foreignKey: 'version_data_id',
            as: 'ResultData',
            onDelete: 'CASCADE',
            hooks: true
        });
/*
        VersionData.hasMany(models.Sitting, {
            foreignKey: 'version_data_id',
            as: 'Sittings',
            onDelete: 'CASCADE',
            hooks: true
        });
*/
    };

    return VersionData;
};
  
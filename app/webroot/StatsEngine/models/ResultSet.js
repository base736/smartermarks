/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export default (sequelize, DataTypes) => {
    const ResultSet = sequelize.define(
        'ResultSet',
        {
            id: {
                type: DataTypes.INTEGER(20),
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                type: DataTypes.INTEGER(20),
                allowNull: false
            },
            created: {
                type: DataTypes.DATE,
                allowNull: true
            },
            modified: {
                type: DataTypes.DATE,
                allowNull: true
            },
            source_model: {
                type: DataTypes.STRING(20),
                allowNull: false,
                collate: 'utf8mb4_unicode_ci'
            },
            stats_status: {
                type: DataTypes.STRING(16),
                allowNull: false,
                defaultValue: 'update',
                collate: 'utf8mb4_unicode_ci'
            },
            average_score: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0
            },
            alpha: {
                type: DataTypes.FLOAT,
                allowNull: false,
                defaultValue: 0
            }
        },
        {
            tableName: 'result_sets',
            timestamps: false,
            indexes: [
                {
                    name: 'idx_user_id',
                    fields: ['user_id']
                },
                {
                    name: 'idx_stats_status',
                    fields: ['stats_status']
                }
            ]
        }      
    );

    ResultSet.associate = (models) => {
/*
        ResultSet.hasMany(models.Sitting, {
            foreignKey: 'result_set_id',
            as: 'Sittings'
        });

        ResultSet.hasMany(models.ResponsePaper, {
            foreignKey: 'result_set_id',
            as: 'ResponsePapers'
        });
*/
        ResultSet.hasMany(models.ResultData, {
            foreignKey: 'result_set_id',
            as: 'ResultData',
            onDelete: 'CASCADE',
            hooks: true
        });
/*
        ResultSet.hasMany(models.QuestionStatistics, {
            foreignKey: 'result_set_id',
            as: 'QuestionStatistics',
            onDelete: 'CASCADE',
            hooks: true
        });
*/
    };

    return ResultSet;
};

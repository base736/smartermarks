/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { Sequelize, DataTypes } from 'sequelize';
import config from '../secrets.json' with { type: 'json' };

// Extract database configuration
const dbConfig = config.database;

// Initialize Sequelize
const sequelize = new Sequelize(dbConfig.database, dbConfig.login, dbConfig.password, {
    host: dbConfig.host || 'localhost',
    port: dbConfig.port || 3306,
    dialect: 'mysql',
    dialectOptions: {
        socketPath: dbConfig.unix_socket || undefined
    },
    define: {
        charset: dbConfig.encoding || 'utf8mb4',
        collate: 'utf8mb4_unicode_ci'
    },
    logging: false,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

// Import models using dynamic imports
import QuestionModel from './Question.js';
import QuestionHandleModel from './QuestionHandle.js';
import ResultSetModel from './ResultSet.js';
import ResultDataModel from './ResultData.js';
import VersionDataModel from './VersionData.js';

// Initialize models
const Question = QuestionModel(sequelize, DataTypes);
const QuestionHandle = QuestionHandleModel(sequelize, DataTypes);
const ResultSet = ResultSetModel(sequelize, DataTypes);
const ResultData = ResultDataModel(sequelize, DataTypes);
const VersionData = VersionDataModel(sequelize, DataTypes);

// Apply associations
ResultSet.associate({
    ResultData
});

ResultData.associate({
    ResultSet,
    VersionData
});

VersionData.associate({
    ResultData
});

Question.associate({
    QuestionHandle
});

QuestionHandle.associate({
    Question
});

// Export models and Sequelize instance

export {
    Sequelize,
    sequelize,
    Question,
    QuestionHandle,
    ResultSet,
    ResultData,
    VersionData
};
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export default (sequelize, DataTypes) => {
    const Question = sequelize.define(
        'Question',
        {
            id: {
                type: DataTypes.INTEGER(20),
                primaryKey: true,
                autoIncrement: true
            },
            creator_id: {
                type: DataTypes.INTEGER(20),
                allowNull: false
            },
            parent_id: {
                type: DataTypes.INTEGER(20),
                allowNull: true
            },
            created: {
                type: DataTypes.DATE,
                allowNull: true
            },
            modified: {
                type: DataTypes.DATE,
                allowNull: true
            },
            needs_index: {
                type: DataTypes.DATE,
                allowNull: true
            },
            last_content_id: {
                type: DataTypes.STRING(32),
                allowNull: true,
                collate: 'utf8mb4_unicode_ci'
            },
            open_status: {
                type: DataTypes.INTEGER(5),
                allowNull: false,
                defaultValue: 0
            },
            opened: {
                type: DataTypes.DATE,
                allowNull: true
            },
            name: {
                type: DataTypes.TEXT('medium'),
                allowNull: false,
                collate: 'utf8mb4_unicode_ci'
            },
            json_data: {
                type: DataTypes.TEXT('long'),
                allowNull: true,
                collate: 'utf8mb4_unicode_ci'
            },
            last_used: {
                type: DataTypes.TEXT('medium'),
                allowNull: false,
                collate: 'utf8mb4_unicode_ci'
            },
            question_content_id: {
                type: DataTypes.STRING(32),
                allowNull: true,
                collate: 'utf8mb4_unicode_ci'
            },
            content_ids: {
                type: DataTypes.TEXT('medium'),
                allowNull: true,
                collate: 'utf8mb4_unicode_ci'
            },
            scoring_hash: {
                type: DataTypes.STRING(32),
                allowNull: false,
                collate: 'utf8mb4_unicode_ci'
            },
            stats_count: {
                type: DataTypes.INTEGER(20),
                allowNull: false,
                defaultValue: 0
            },
            stats_difficulty: {
                type: DataTypes.FLOAT,
                allowNull: true,
                defaultValue: 0
            },
            stats_discrimination: {
                type: DataTypes.FLOAT,
                allowNull: true,
                defaultValue: 0
            },
            stats_json: {
                type: DataTypes.TEXT,
                allowNull: false,
                collate: 'utf8mb4_unicode_ci'
            }
        },
        {
            tableName: 'questions',
            timestamps: false,
            indexes: [
                {
                    name: 'idx_parent_id',
                    fields: ['parent_id']
                },
                {
                    name: 'idx_creator_id',
                    fields: ['creator_id']
                },
                {
                    name: 'idx_question_content_id',
                    fields: ['question_content_id']
                },
                {
                    name: 'idx_needs_index',
                    fields: ['needs_index']
                }
            ]
        }
    );

    Question.associate = (models) => {
        Question.hasMany(models.QuestionHandle, {
            foreignKey: 'question_id',
            as: 'QuestionHandles',
            onDelete: 'CASCADE',
            hooks: true
        });
    };

    return Question;
};

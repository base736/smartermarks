const express = require('express');
const puppeteer = require('puppeteer');
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

let browser;

function getTimestamp() {
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, '0');
    const day = String(now.getDate()).padStart(2, '0');
    const hours = String(now.getHours()).padStart(2, '0');
    const minutes = String(now.getMinutes()).padStart(2, '0');
    const seconds = String(now.getSeconds()).padStart(2, '0');
    const milliseconds = String(now.getMilliseconds()).padStart(3, '0');
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}.${milliseconds}`;
}

function timedLog(...args) {
    console.log(`[${getTimestamp()}]`, ...args);
}

async function initBrowser() {
    try {
        browser = await puppeteer.launch({
            args: [
                '--no-sandbox',
                '--disable-gpu' // helps with performance on some systems
            ],
            headless: true
        });
        timedLog('Puppeteer browser initialized.');
    } catch (error) {
        timedLog('Failed to initialize Puppeteer:', error);
        process.exit(1);
    }
}

async function ensureBrowser() {

    // Make sure we have an initialized browser

    if (!browser) {
        timedLog('No existing browser. Initializing...');
        await initBrowser();
        return browser;
    }

    // Check that the browser is alive

    try {
        await browser.version();
    } catch (error) {
        timedLog('Browser unresponsive, re-initializing:', error.message);
        await initBrowser();
    }

    return browser;
}

async function generatePDF(data) {
    const activeBrowser = await ensureBrowser();
    let page;
    try {
        
        page = await activeBrowser.newPage();
        await page.goto(data.contentURL);

        // Create working directory
        const dir = `temp/${data.jobName}`;
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
        }
        const pdfFilePath = `temp/${data.jobName}/${data.fileName}`;

        const pdfParameters = {
            path: pdfFilePath,
            format: data.pageSize,
            margin: {
                top: data.topMarginSize,
                right: data.rightMarginSize,
                bottom: data.bottomMarginSize,
                left: data.leftMarginSize,
            },
            displayHeaderFooter: true
        };

        // If user provided custom header, use it for both header and footer
        if ('header' in data) {
            const headerFooterHTML = `
                <div style="width:100%; font-size:12pt; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                    ${data.header}
                </div>
            `;
            pdfParameters.headerTemplate = headerFooterHTML;
            pdfParameters.footerTemplate = headerFooterHTML;
        } else {
            const footerHTML = `
                <div style="width:100%; font-size:8.2pt; font-family:Times, serif; text-align:center;">
                    Page <span class='pageNumber'></span>/<span class='totalPages'></span>
                </div>
            `;
            pdfParameters.headerTemplate = '<div></div>'; // empty header
            pdfParameters.footerTemplate = footerHTML;
        }

        // Wait for paged loaded selector
        await page.waitForSelector('.page_loaded', { timeout: 30000 });
        await page.pdf(pdfParameters);

        return true;

    } catch (error) {

        timedLog('Error encountered while building PDF:', error.message);
        return false;

    } finally {

        if (page) {
            try {
                await page.close();
            } catch (err) {
                timedLog('Error closing page:', err.message);
            }
        }
    }
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Check that server is running an Puppeteer is healthy

app.get('/health', async (req, res) => {
    try {
        const activeBrowser = await ensureBrowser(); 
        await activeBrowser.version();
        return res.status(200).send('OK');
    } catch (error) {
        timedLog('Health check failure:', error.message);
        return res.status(500).send('Browser is not healthy');
    }
});

app.post('/', async (req, res) => {
    const data = req.body;
    if (!data.jobName || !data.contentURL) {
        timedLog('Invalid request: Missing jobName or contentURL.');
        return res.status(400).json({ success: false, message: 'Missing jobName or contentURL.' });
    }

    try {
        const success = await generatePDF(data);
        return res.json({ success });
    } catch (error) {
        timedLog('Unexpected error:', error);
        return res.status(500).json({ success: false });
    }
});

app.listen(port, async () => {
    if (!fs.existsSync('temp')) {
        timedLog('No temp folder found..');
        process.exit(1);
    }

    timedLog(`Server listening at http://localhost:${port}`);
    await initBrowser();  
});

process.on('SIGTERM', async () => {
    timedLog('SIGTERM signal received. Closing Puppeteer browser.');
    if (browser) {
        await browser.close();
    }
    process.exit(0);
});

process.on('SIGINT', async () => {
    timedLog('SIGINT signal received. Closing Puppeteer browser.');
    if (browser) {
        await browser.close();
    }
    process.exit(0);
});
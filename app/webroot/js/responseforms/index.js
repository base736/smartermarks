/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as IndexCommon from '/js/common/index.js?7.0';
import * as Folders from '/js/common/folders.js?7.0';
import * as ClassListCommon from '/js/classlists/common.js?7.0';

function drawIndexRow(data) {

	// Add a row in the index table for this entry

	var canCopy = 1;
	var canDelete = 0;
	if (('Permissions' in data) && (data['Permissions'].indexOf('can_delete') >= 0)) canDelete = 1;

	var html = "<tr data-document-id='" + data['Document']['id'] + "' " +
        "data-version='" + data['Document']['version'] + "' " +
        "data-can-copy='" + canCopy + "' " +
        "data-can-delete='" + canDelete + "' " +
		"data-has-template='" + (data['Document']['has_html'] ? 1 : 0) + "'>";

	html += '<td style="width:15px; text-align:center;">' + 
		'<input type="checkbox" class="item_select" style="margin:1px 0px;" autocomplete="off" />' + 
		'</td>';

	if ('User' in data) {
		html += '<td style="width:250px;">' + data['User']['email'] + '</td>';
	}

	var date = dayjs.utc(data['Document']['moved']).local();
	html += '<td style="width:120px;">' + date.format('YYYY-MM-DD HH:mm') + '</td>';

	html += '<td><div style="display:flex; flex-direction:row; align-items:flex-start; gap:5px;">';
	html += '<div class="index_folder_link" data-folder-id="' + data['Document']['folder_id'] + '"></div>';
	if (data['Document']['is_new'] >= 2) html += "<div class='index_new_label label label-info'>New</div> ";
	html += '<div class="index_item_name" style="display:inline-block">' + escapeHTML(data['Document']['save_name']) + '</div>';
	html += '</div></td>';

	html += "</tr>";
	
	$('.index_table').append(html);
}

function finishIndex() {
	if (IndexCommon.activeNode === false) {

		$('#newVersion_button').addClass('always-disabled-link');
		$('#share_dropdown').removeAttr('disabled');
		$('#sendCopy_button').removeClass('always-disabled-link');
		$('#newItem_button').hide();

	} else {
	
		// Update button availability

		if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_add') >= 0) {
			$('#newVersion_button').removeClass('always-disabled-link');
		} else $('#newVersion_button').addClass('always-disabled-link');

		if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_export') >= 0) {
			$('#share_dropdown').removeAttr('disabled');
			$('#sendCopy_button').removeClass('always-disabled-link');
		} else {
			$('#share_dropdown').attr('disabled', 'disabled');
			$('#sendCopy_button').addClass('always-disabled-link');
		}

		// Set up right-side tab buttons

		$('.tab_right').hide();
		if (IndexCommon.activeNode.id == IndexCommon.roots['Trash'].id) $('#emptyTrash_button').show();
		else {
			var canAdd = Folders.isItemFolder(treeData, IndexCommon.activeNode.id);
			if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_add') == -1) canAdd = false;
			if (canAdd) {
				$('#newItem_button .tab_icon').css('background-image', 'url("/img/tab_icons/add_item_icon.svg")');
				$('#newItem_button .tab_text').html('New Form');
				$('#newItem_button').show();
			} else $('#newItem_button').hide();
		}

		// Check for an empty index

		if ($('.index_table tr').length == 0) {
			var html = "";
			html += '<tbody><tr class="disabled"><td colspan="5"><div style="padding-left:10px;">';
			html += '<i>No items to show here.';
			if (IndexCommon.activeNode.id != IndexCommon.roots['Trash'].id) html += ' Click "New Form" in the menu bar to create a new form.';
			html += '</i></div></td></tr></tbody>';
			$('.index_table').html(html);
		}
		
	}
}

$(document).ready(function() {

	localStorage.removeItem('reportData');  // Clear report cache

    IndexCommon.initialize({ drawIndexRow, finishIndex });
    ClassListCommon.initialize();

	var documentScoring = false;
	var documentLayout = false;
	
	$('#editDocument_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;
		
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var documentID = $(this).closest('tr').attr('data-document-id');
				window.location.href = '/Documents/build/' + documentID;
			}
		});
	});
	
	$(document).on('dblclick', '.index_table tbody tr', function(e) {
		e.preventDefault();
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		var documentID = $(this).attr('data-document-id');
		if (numChecked == 0) {
			window.location.href = '/Documents/build/' + documentID;
		} else if (numChecked == 1) {
			var $selectedID = $selectedCheckbox.closest('tr').attr('data-document-id');
			if ($selectedID == documentID) {
				window.location.href = '/Documents/build/' + documentID;
			}
		}
	});
	
	$(document).on('willModifyItemActions', function(event) {
		$('button.needs_v3').removeClass('disabled');
		$('a.needs_v3').removeClass('disabled-link');
		$('.needs_generated').removeClass('disabled-link');
		$('.not_generated').removeClass('disabled-link');
	});
	
	$(document).on('modifyItemActions', function(event) {
        var hasEarlyVersion = false;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				if ($(this).closest('tr').data('has-template') != 1) $('.needs_generated').addClass('disabled-link');
				if ($(this).closest('tr').data('has-template') != 0) $('.not_generated').addClass('disabled-link');
                if ($(this).closest('tr').attr('data-version') < 3) hasEarlyVersion = true;
            }
		});

        if (hasEarlyVersion && $('.item_select:checked').length > 1) {
            $('a.needs_v3').addClass('disabled-link');
            $('button.needs_v3').addClass('disabled');
        }
	});
    
    $('#buildGenericPDF_button').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

        var documentID = false;
        $('.item_select').each(function() {
			if ($(this).prop('checked')) {
				documentID = $(this).closest('tr').attr('data-document-id');
			}
		});

        if (documentID === false) {

            $.gritter.add({
                title: "Error",
                text: "Unable to print form.",
                image: "/img/error.svg"
            });

        } else {

			var documentLists = {};
            documentLists[documentID] = false;

			navigateWithPost('/build_pdf', {
				pdfType: 'response_form',
				documentListsJSON: JSON.stringify(documentLists)
			});
    
        }
	});

    var classListDrawCallback = function() {
        var documentData = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
                var documentID = parseInt($(this).closest('tr').attr('data-document-id'));
                var documentName = $(this).closest('tr').find('.index_item_name').html();
				documentData.push({
                    id: documentID,
                    name: documentName
                });
			}
		});

        documentData.sort(function(a, b) {
            if (a.name < b.name) return -1;
            else if (a.name > b.name) return 1;
            else if (a.id < b.id) return -1;
            else if (a.id > b.id) return 1;
            else return 0;
        });

        var $tableRows = $('#class_list_wrapper .list_row');
        for (var i = 0; i < documentData.length; ++i) {
            var insertionIndex = Math.floor($tableRows.length * i / documentData.length);
            var html = '<div class="list_header" data-document-id="' + documentData[i].id + 
                '"><b class="list_student_count"></b> '  + documentData[i].name + '</div>';
            $tableRows.eq(insertionIndex).before(html);
        }
        classListSortCallback();
    }

    var classListSortCallback = function() {
        $('#class_list_wrapper .list_header').each(function() {
            var studentCount = $(this).nextUntil('.list_header').length;
            var html = '(' + studentCount + ' ' + ((studentCount == 1) ? 'student' : 'students') + ')';
            $(this).find('.list_student_count').html(html);
        });
    }

	$('#buildClassPDF_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

        ClassListCommon.initializeClassLists({
            needsEmail: false,
            classListDidDraw: classListDrawCallback,
            classListDidSort: classListSortCallback
        });
        $('#buildClassPDF_dialog').dialog('open');        
        document.activeElement.blur();
	});

    var printClassPDF = function() {
        var documentLists = {};
        $('#class_list_wrapper .list_header').each(function() {
            var documentID = parseInt($(this).attr('data-document-id'));
            documentLists[documentID] = [];
            $(this).nextUntil('.list_header').each(function() {
                if ($(this).find('.class_list_checkbox:checked').length == 1) {
                    documentLists[documentID].push(JSON.parse($(this).attr('data-json-data')));
                }
            });
        });

		navigateWithPost('/build_pdf', {
			pdfType: 'response_form',
			documentListsJSON: JSON.stringify(documentLists)
		});

		$('#buildClassPDF_dialog').dialog('close');
    }

    $('#buildClassPDF_dialog').dialog({
        autoOpen: false,
        width: 500,
        modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$(this).dialog("close");
			},
			"Print": printClassPDF
        }, create: function() {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
            $buttonPane.find('button:contains("Print")').attr('id', 'class_list_continue');
        }, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Print")').addClass('btn btn-save btn_font');
		}, close: function() {
            $('form').validationEngine('hideAll');
        }
    });
    
	$('#buildKey_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

		var documentID = false;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				documentID = $(this).closest('tr').attr('data-document-id');
			}
		});

		if (documentID !== false) {
			navigateWithPost('/build_pdf', {
				pdfType: 'pdf_from_url',
				url: '/Documents/print_key/' + documentID,
				target: 'key.pdf'
			});	
		}
	});
	
	$('#buildQuestions_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

		var documentID = -1;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				documentID = $(this).closest('tr').attr('data-document-id');
			}
		});
		
		$('#buildQuestions_docId').val(documentID);
		
		if ('formLocation' in userDefaults.Document) {
			$('#buildQuestions_form').val(userDefaults.Document.formLocation);
		} else $('#buildQuestions_form').val('none');

		$('#buildQuestions_dialog').dialog('open');
	});
	
	function buildQuestions() {
		navigateWithPost('/build_pdf', {
			pdfType: 'question_book',
			documentID: $('#buildQuestions_docId').val(),
			formLocation: $('#buildQuestions_form').val()
		});
		
		$('#buildQuestions_dialog').dialog('close');
	}

    $('#buildQuestions_dialog').dialog({
        autoOpen: false,width: 365,modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$(this).dialog("close");
			},
			"Build": buildQuestions
        }, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Build")').addClass('btn btn-save btn_font');
			$buttonPane.find('button:contains("Build")').focus();
		}, close: function() {
            $('form').validationEngine('hideAll');
        }
    });

	$('#newScans_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

        var documentIDs = [];
        var hasEarlyVersion = false;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var documentID = $(this).closest('tr').attr('data-document-id');
                documentIDs.push(parseInt(documentID));

                var version = $(this).closest('tr').attr('data-version');
                if (version < 3) hasEarlyVersion = true;
			}
		});

        if ((documentIDs.length > 1) && hasEarlyVersion) {

            $.gritter.add({
                title: "Error",
                text: "Selected forms do now allow scoring several at once.",
                image: "/img/error.svg"
            });

        } else {

            window.location.href = '/Scores/mark_new?ids=' + documentIDs.join(',');

        }
    });
	
	$('#browseScans_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var documentID = $(this).closest('tr').attr('data-document-id');
				window.location.href = '/Scores/index/' + documentID;
			}
		});
	});
		
	$(document).on('keydown', '.versionInput', function(e) {
		var $sectionWrapper = $(this).closest('.section_wrapper');
		$sectionWrapper.find('.versionInput').each(function() {
			$(this).validationEngine('hide');
		})
	});

	$(document).on('keyup', '.versionInput', function(e) {
		validateMap($(this).attr('id'), false);
	});

	function validateMap(skipID, checkBlanks) {
		var partsRegex = /^([0-9]+)([a-z]?)$/;
		
		var isValid = true;
		var hasBlank = false;
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			if (documentScoring.Sections[i].type == 'page_break') continue;
			if (documentScoring.Sections[i].Content.length == 0) continue;
			
			var values = [];
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				var thisValue = $('#index_' + i + '_' + j).val().trim();
				if (skipID !== 'index_' + i + '_' + j) {
					values.push(thisValue.toLowerCase());
					if (!hasBlank && checkBlanks && (thisValue.length == 0)) {
						$('#index_' + i + '_' + j).validationEngine('showPrompt', '* Must not be blank', 'error', 'bottomLeft', true);
						hasBlank = true;
					}
				} else values.push('');
			}

			var groupedIndices = [];
			var thisGroup = [];
			var lastNumber = parseInt(documentScoring.Sections[i].Content[0].number);
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				var thisNumber = parseInt(documentScoring.Sections[i].Content[j].number);
				if (thisNumber != lastNumber) {
					groupedIndices.push(thisGroup);
					thisGroup = [];
				}
				thisGroup.push(j);
				lastNumber = thisNumber;
			}
			groupedIndices.push(thisGroup);

			var oldNumbers = [];
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				oldNumbers.push(parseInt(documentScoring.Sections[i].Content[j].number));
			}

			var groupNumbers = [];
			for (var j = 0; j < groupedIndices.length; ++j) {

				var thisGroupNumber = false;
				for (var k = 0; k < groupedIndices[j].length; ++k) {
					var index = groupedIndices[j][k];
					var fieldID = '#index_' + i + '_' + index;
					var value = values[index];

					var match = partsRegex.exec(documentScoring.Sections[i].Content[index].number);
					var oldAlpha = match[2];

					if ((fieldID !== skipID) && (value.length > 0)) {
						var thisValid = true;

						var match = partsRegex.exec(value);
						if (match !== null) {
							var newNumber = parseInt(match[1]);
							var newAlpha = match[2];

							var count = 0;
							for (var testIndex = 0; testIndex < values.length; ++testIndex) {
								if (values[index] == values[testIndex]) count++;
							}
							
							if (count == 1) {

								if (newAlpha.length > 0) {
									var alphaIndex = newAlpha.charCodeAt(0) - 97;
									if (oldAlpha.length == 0) {
										$('#index_' + i + '_' + index).validationEngine('showPrompt', 
											'* Must not include a letter', 'error', 'bottomLeft', true);
										thisValid = false;
									} else if (alphaIndex >= groupedIndices[j].length) {
										$('#index_' + i + '_' + index).validationEngine('showPrompt', 
											'* Invalid question letter', 'error', 'bottomLeft', true);
										thisValid = false;
									}
								} else if (oldAlpha.length > 0) {
									$('#index_' + i + '_' + index).validationEngine('showPrompt', 
										'* Must include a letter', 'error', 'bottomLeft', true);
									thisValid = false;
								} else if (oldNumbers.indexOf(newNumber) == -1) {
									$('#index_' + i + '_' + index).validationEngine('showPrompt', 
										'* Question number not in section', 'error', 'bottomLeft', true);
									thisValid = false;
								}
								
								if (thisValid) {
									if (thisGroupNumber === false) thisGroupNumber = newNumber;
									else if (thisGroupNumber != newNumber) thisGroupNumber = -1;
								}

							} else {
								$(fieldID).validationEngine('showPrompt', 
									'* Repeated question label', 'error', 'bottomLeft', true);
								thisValid = false;
							}

						} else {
							$(fieldID).validationEngine('showPrompt', 
								'* Invalid question label', 'error', 'bottomLeft', true);
							thisValid = false;
						}
					}
				}
				groupNumbers.push(thisGroupNumber);
			}

			for (var j = 0; j < groupedIndices.length; ++j) {
				if (groupNumbers[j] == -1) {

					for (var k = 0; k < groupedIndices[j].length; ++k) {
						var index = groupedIndices[j][k];
						if (values[index].length > 0) {
							$('#index_' + i + '_' + index).validationEngine('showPrompt', 
								'* Inconsistent question number', 'error', 'bottomLeft', true);
						}
					}

				}
			}
		}

		return isValid;
	}

	$('#newVersion_button').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;
		
		var documentID = -1;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				documentID = $(this).closest('tr').attr('data-document-id');
			}
		});

		ajaxFetch('/Documents/get/' + documentID)
		.then(async response => {
			const responseData = await response.json();

			if (responseData.success) {
				var isValid = true;

                if (!('Version' in responseData)) isValid = false;
				else if (!('scoring_string' in responseData.Version)) isValid = false;
				else if (!('scoring_hash' in responseData.Version)) isValid = false;
				else if (responseData.Version.scoring_hash != SparkMD5.hash(responseData.Version.scoring_string)) isValid = false;

                if (!('Document' in responseData)) isValid = false;
				else if (!('layout_string' in responseData.Document)) isValid = false;
				else if (!('layout_hash' in responseData.Document)) isValid = false;
				else if (responseData.Document.layout_hash != SparkMD5.hash(responseData.Document.layout_string)) isValid = false;

				if (isValid) {

                    // Load document data

                    documentScoring = JSON.parse(responseData.Version.scoring_string);
                    documentLayout = JSON.parse(responseData.Document.layout_string);

                    $('#map_invert').prop("checked", false);
                    $('#version_map_original').html('("' + responseData.Document.save_name + '") ');

                    var hasEmptySections = false;

                    var versioningHTML = "";
                    for (var i = 0; i < documentScoring.Sections.length; ++i) {
                        var layoutSection = false;
                        for (var j = 0; j < documentLayout.Body.length; ++j) {
                            if (documentLayout.Body[j].id == documentScoring.Sections[i].id) {
                                layoutSection = documentLayout.Body[j];
                            }
                        }

                        versioningHTML += "<div class='section_wrapper' data-section-id='" + documentScoring.Sections[i].id + "'>";
                        versioningHTML += "<label>" + layoutSection.header + "</label>";

                        if (documentScoring.Sections[i].Content.length > 0) {
                            for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
                                var thisNumber = documentScoring.Sections[i].Content[j].number;
                                versioningHTML += 
                                    "<div class='version_entry'> \
                                        <span style='display: inline-block; width: 25px; position:relative; top:1px;' data-index=" + i + ">" + thisNumber + ".</span>\
                                        <input type='text' class='versionInput' id='index_" + i + "_" + j + "' data-section='" + i + "' data-question='" + j + "'>\
                                    </div>";
                            }	
                        } else hasEmptySections = true;

                        versioningHTML += "</div>";

                    }

                    if (documentScoring.Sections.length == 0) {
                        $.gritter.add({
                            title: "Error",
                            text: "Form contains no sections. Can't create a version.",
                            image: "/img/error.svg"
                        });
                    } else if (hasEmptySections) {
                        $.gritter.add({
                            title: "Error",
                            text: "Form contains empty sections. Can't create a version.",
                            image: "/img/error.svg"
                        });
                    } else {
                        $('#version_data_wrapper').html("<div style='width: 650px;'>" + versioningHTML + "</div>");
                        $('#newVersion_dialog').dialog('open');
                        $('#index_0_0').focus();
                    }
                    
                }
			}
        });
	});
	
	function buildVersion() {
		var $dialog = $(this);
		if (validateMap(false, true)) {

			// Apply map to document data

			var partsRegex = /^([0-9]+)([a-z]?)$/;

			for (var i = 0; i < documentScoring.Sections.length; ++i) {
				for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
					var newNumber = $('#index_' + i + '_' + j).val().trim();
					documentScoring.Sections[i].Content[j].number = newNumber;
				}

				documentScoring.Sections[i].Content.sort(function(a, b) {
					var aMatch = partsRegex.exec(a.number);
					var aNumber = parseInt(aMatch[1]);
					var aAlpha = aMatch[2];
	
					var bMatch = partsRegex.exec(b.number);
					var bNumber = parseInt(bMatch[1]);
					var bAlpha = bMatch[2];
	
					if (aNumber < bNumber) return -1;
					else if (aNumber > bNumber) return 1;
					else if (aAlpha < bAlpha) return -1;
					else if (aAlpha > bAlpha) return 1;
					else return 0;
				});
			}

			// Create new form and forward to build interface

	    	if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			ajaxFetch('/Documents/create', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					folder_id: IndexCommon.activeNode.data.folder_id,
					scoring_data: JSON.stringify(documentScoring),
					layout_data: JSON.stringify(documentLayout)
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
							
				if (responseData.success) {
					window.location.href = '/Documents/build/' + responseData.id;
				} else {
					var message = "Unable to create version.";
					if ('message' in responseData) message = responseData.message;
	
					$.gritter.add({
						title: "Error",
						text: message,
						image: "/img/error.svg"
					});	
				}
	 		});
	 	}
	}

	$('#newVersion_dialog').dialog({
        autoOpen: false,
		width: 675,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Build': buildVersion
		}, open: function(event) {
		   	var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Build")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});	

// Login dialogs

	$('#hide_intro_howto').click(function(e) {
		ajaxFetch('/Users/hide_tip/intro_howto', {
			method: 'POST'
		}).then(async response => {
			$('#intro_howto_wrapper').hide();
		});
	});
	
	$('#hide_feb_6_2025_message').click(function(e) {
		ajaxFetch('/Users/hide_tip/feb_6_2025_message', {
			method: 'POST'
		}).then(async response => {
			$('#feb_6_2025_message_wrapper').hide();
		});
	});
	
	$('#hide_jan_21_2025_message').click(function(e) {
		ajaxFetch('/Users/hide_tip/jan_21_2025_message', {
			method: 'POST'
		}).then(async response => {
			$('#jan_21_2025_message_wrapper').hide();
		});
	});
});


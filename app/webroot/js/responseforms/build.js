/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { localizationInstance } from '/js/FormEngine/Localization.js?7.0';

import * as ClassListCommon from '/js/classlists/common.js?7.0';
import * as PreviewCommon from '/js/common/preview.js?7.0';
import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as ScrollingCommon from '/js/common/scrolling.js?7.0';
import * as HtmlEdit from '/js/HTMLEditor/html_edit.js?7.0';
import * as StatsCommon from '/js/common/stats.js?7.0';
import * as QuestionDetails from '/js/common/question_details.js?7.0';

$.extend($.ui.dialog.prototype.options, {
	resizable: false,
    create: function() {
        var $this = $(this);

        $this.keypress(function(e) {
			if (e.key === 'Enter') {
				if ($this.attr('id') == 'editDocument_form') {
					if ($(':focus').attr('id') != 'ddoc_title_text') e.preventDefault();
				} else if ($(this).attr('id') == 'editOutcome_dialog') {
					e.preventDefault();
				}
            }
        });
    }
});

var documentVersionID;

var documentScoring;
var activeSectionScoring;

var documentLayout;
var activeSectionLayout;

var renderHTML = false;
var questionData = {};

var reportOptions = [
    { name: 'reportsPerPage', suffix: '_student_layout' },
    { name: 'weights', suffix: '_student_weights' },
    { name: 'outcomes', suffix: '_student_outcomes' },
    { name: 'responses', suffix: '_student_responses' },
    { name: 'results', suffix: '_student_results' }
];

document.addEventListener("DOMContentLoaded", async function() {
    
    ClassListCommon.initialize();
    ScrollingCommon.initialize();
    await RenderTools.initialize();
    await HtmlEdit.initialize();
    QuestionDetails.initialize();

	StatsCommon.initFilterInterface();

    var bufferedItems = [];
	var lastCheckboxIndex;

	var $draggingSection = null;

	var initialize = async function() { 
		const response = await ajaxFetch('/Documents/get/' + documentID);
		const responseData = await response.json();

		if (responseData.success) {
			var isValid = true;

			if (!('Version' in responseData)) isValid = false;

			if (!('scoring_string' in responseData.Version)) isValid = false;
			else if (!('scoring_hash' in responseData.Version)) isValid = false;
			else if (responseData.Version.scoring_hash != SparkMD5.hash(responseData.Version.scoring_string)) isValid = false;

			if (!('Document' in responseData)) isValid = false;

			if (!('layout_string' in responseData.Document)) isValid = false;
			else if (!('layout_hash' in responseData.Document)) isValid = false;
			else if (responseData.Document.layout_hash != SparkMD5.hash(responseData.Document.layout_string)) isValid = false;

			if (isValid) {

				// Load document data

				documentScoring = JSON.parse(responseData.Version.scoring_string);
				documentLayout = JSON.parse(responseData.Document.layout_string);
				localizationInstance.setLanguage(documentLayout.Settings.language);

				if ('html' in responseData.Version) {

					// Fill question book HTML

					$('#assessment_preview_wrapper').html(responseData.Version.html);

					// Remove section and question wrappers if they are present

					$('#assessment_preview_wrapper .preview_section_wrapper').each(function() {
						if ($(this).children().length > 0) $(this).children().eq(0).unwrap();
						else $(this).remove();
					});
					$('#assessment_preview_wrapper .question_wrapper').each(function() {
						if ($(this).children().length > 0) $(this).children().eq(0).unwrap();
						else $(this).remove();
					});

					// Set attributes for parts and answers

					var styleData = {};
					var goodMap = true;

					for (var i = 0; i < documentScoring.Sections.length; ++i) {
						var sectionID = documentScoring.Sections[i].id;

						var style = false;
						for (var j = 0; j < documentLayout.Body.length; ++j) {
							if (documentLayout.Body[j].id == sectionID) style = documentLayout.Body[j].type;
						}

						if (style !== false) {
							if (!(style in styleData)) styleData[style] = []; 
							for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
								var thisContent = documentScoring.Sections[i].Content[j];

								var styleEntry = {
									attributes: [{
										name: 'data-content-id',
										value: thisContent.id
									}]
								};

								if ('Source' in thisContent) {
									styleEntry.attributes.push({
										name: 'data-question-handle-id',
										value: thisContent.Source.question_handle_id
									});
									styleEntry.attributes.push({
										name: 'data-part-id',
										value: thisContent.Source.part_id
									});
								}

								if ('Answers' in thisContent) {
									styleEntry.answers = thisContent.Answers;    
								}

								styleData[style].push(styleEntry);

							}
						} else goodMap = false;
					}

					if (goodMap) {
						$('#assessment_preview_wrapper .part_wrapper').each(function() {
							var thisStyle = $(this).attr('data-type').substring(0, 2);
							if ((thisStyle in styleData) && (styleData[thisStyle].length > 0)) {
								var thisData = styleData[thisStyle].shift();
								for (var i = 0; i < thisData.attributes.length; ++i) {
									$(this).attr(thisData.attributes[i].name, thisData.attributes[i].value);
								}

								if (thisStyle == 'mc') {

									var $answers = $(this).find('.question_answer');
									if (('answers' in thisData) && (thisData.answers.length == $answers.length)) {
										for (var i = 0; i < thisData.answers.length; ++i) {
											$answers.eq(i).attr('data-answer-id', thisData.answers[i].id);
										}
									}    

								} else if (thisStyle == 'nr') {

									$(this).find('.hidden_answer_list').remove();

									var $answerList = $('<div class="hidden_answer_list"></div>');
									$(this).find('.question_text').append($answerList);

									if ('answers' in thisData) {
										for (var i = 0; i < thisData.answers.length; ++i) {
											$answerList.append('<div class="question_answer" data-answer-id="' + 
												thisData.answers[i].id + '">' + thisData.answers[i].response + '</div>');
										}                                            
									}

								}
							} else goodMap = false;
						});
					}

					if (!goodMap) {
						$('#assessment_preview_wrapper .part_wrapper').removeAttr('data-content-id');
					}
	
					renderHTML = true;

					// Load question data

					if ('Questions' in responseData) {
						for (var id in responseData.Questions) {
							if (responseData.Questions.hasOwnProperty(id)) {
								var thisData = PreviewCommon.parseQuestionData(responseData.Questions[id]);
								if (thisData !== false) {

									// Replace scoring data with that from the document scoring

									for (var i = 0; i < thisData.json_data.Parts.length; ++i) {
										if (!('ScoredResponses' in thisData.json_data.Parts[i])) continue;
										var questionEntry = thisData.json_data.Parts[i];

										for (var j = 0; j < documentScoring.Sections.length; ++j) {
											if (!('Content' in documentScoring.Sections[j])) continue;

											for (var k = 0; k < documentScoring.Sections[j].Content.length; ++k) {
												var scoringEntry = documentScoring.Sections[j].Content[k];

												var isMatch = true;
												if (!('Source' in scoringEntry)) isMatch = false;
												else if (thisData.question_handle_id !== scoringEntry.Source.question_handle_id) isMatch = false;
												else if (questionEntry.id !== scoringEntry.Source.part_id) isMatch = false;

												if (isMatch) {
													questionEntry.hasVariables = StatsCommon.hasVariables(questionEntry);
						
													questionEntry.Answers = [];
													for (var l = 0; l < scoringEntry.Answers.length; ++l) {
														questionEntry.Answers.push({
															id: scoringEntry.Answers[l].id,
															template: scoringEntry.Answers[l].response
														});
													}

													var scoringCopy = JSON.parse(JSON.stringify(scoringEntry.ScoredResponses));
													questionEntry.ScoredResponses = scoringCopy;
												}
											}
										}
									}

									var questionHandleID = thisData.question_handle_id;
									questionData[questionHandleID] = thisData;
								}
							}
						}
					}

					if ('Statistics' in responseData) {
						StatsCommon.initFullStatistics(responseData.Statistics);
					}
				}

				documentVersionID = responseData.Document.version_id;

				// Initialize used IDs

				for (var i = 0; i < documentScoring.Sections.length; ++i) {
					var section = documentScoring.Sections[i];
					usedIDs.push(section.id);

					if (documentScoring.Sections[i].type != 'page_break') {
						for (var j = 0; j < section.Content.length; ++j) {
							var content = section.Content[j];
							usedIDs.push(content.id);
							if ('Answers' in content) {
								for (var k = 0; k < content.Answers.length; ++k) {
									usedIDs.push(content.Answers[k].id);
								}	
							}
						}
					}
				}

				for (var i = 0; i < documentLayout.Body.length; ++i) {
					usedIDs.push(documentLayout.Body[i].id);
				}

				// Draw document

				$('#document_title').text(responseData.Document.save_name);
				if (!canEdit) $('#document_title').append(' &mdash; <b>READ ONLY</b>');

				$('#loading_panel').hide();
				$('#view_menu').menu('enable');			

				// Show the selected view

				triggerActiveView();

				// Enable settings and print buttons
				
				$('.startEditDoc').removeAttr('disabled');
				$('#print_dropdown').removeAttr('disabled');

				return;
				
			} else {

				throw new Error('Invalid form data.');

			}
		} else {

			throw new Error('Failed to fetch form data.');

        }
	}

	var triggerActiveView = function() {
	
		// Show the selected view

		var urlVars = getUrlVars();

		if ('view' in urlVars) {
			var $viewButton = $('.view_button[data-view-type="' + urlVars.view + '"]:visible');
			if ($viewButton.length == 1) $viewButton.trigger('click');
			else $('.view_button').first().trigger('click');
		} else $('.view_button').first().trigger('click');
	}

	$(".view_button").click(function(e) {
		e.preventDefault();
		if (!$("#view_menu").menu("option", "disabled")) {
			var urlVars = getUrlVars();
			urlVars.view = $(this).attr('data-view-type');
			setUrlVars(urlVars);

			$(".view_button").removeClass("ui-state-active");
			$(this).addClass("ui-state-active");

			drawView();
		}
	});

	var drawView = function() {
		$('.right_panel').hide();

		var urlVars = getUrlVars();
		if (urlVars.view == 'form') {

			$('#form_navigation_wrapper').show();
			$('#question_navigation_wrapper').hide();

			drawDetails();

		} else if (urlVars.view == 'questions') {

			$('#form_navigation_wrapper').hide();
			$('#question_navigation_wrapper').show();
			$('#questions_panel').show();

			drawQuestions();

		} else if (urlVars.view == 'statistics') {

			$('#form_navigation_wrapper').hide();
			$('#question_navigation_wrapper').show();
			$('#statistics_wrapper').show();
			$('#statistics_panel').show();

			drawStatistics();

		}
	}

	var drawDetails = function() {
		$('.question_details_wrapper').hide();

		drawLeftBar();

		var urlVars = getUrlVars();
		if (('s' in urlVars) || ('c' in urlVars)) {
			drawActiveSection();
		} else {
			$('#form_detail_panel').show();
			drawDocument();
		}

		$('.edit_control').show();
		if (canEdit && $('#form_sections_menu').is(':visible')) {
			$('#form_sections_menu').sortable('option', 'disabled', false);
		}
	}

	var drawQuestions = async function() {

		var urlVars = getUrlVars();
		if ('s' in urlVars) {
			delete urlVars.s;
			setUrlVars(urlVars);
		}

		drawLeftBar();

		if (renderHTML) {
			await RenderTools.setRenderStyle(documentLayout.Settings.numberingType);

			$('#assessment_preview_wrapper .assessment_title').find('*').each(function() {
				if (this.hasAttribute('style')) {
					var oldStyle = $(this).attr('style');
					var newStyle = oldStyle.replace(/width:[ ]?3in/, 'width:1in');
					$(this).attr('style', newStyle);
				}
			});

			await RenderTools.renderEquations($('#assessment_preview_wrapper'));

			if (documentLayout.Settings.mcFormat == 'Auto') {
				$('#assessment_preview_wrapper .part_wrapper[data-type="mc"]').each(function() { 
					RenderTools.setMCWidths($(this)); 
				});
			} else $('#assessment_preview_wrapper .question_answer').css('width', '100%');	

			$('#assessment_preview_wrapper .part_wrapper').each(function() {
				if ($(this).find('.question_answer_mc, .question_answer_table').length > 0) {
					RenderTools.setMCImageWidths($(this));
				}
			});

			renderHTML = false;
		}

		if ('c' in urlVars) {

			selectItem(urlVars.c);
			
		} else {

			if ($('#question_sections_menu .menu_item').length > 0) {			
				$('#question_sections_menu .menu_item').removeClass('active');
				$('#question_sections_menu .menu_item').eq(0).addClass('active');
			}

			$('#question_navigation_wrapper .items_panel').eq(0).show();
	
		}
	}

	var drawStatistics = async function() {
		$("#stats_print").attr('disabled', 'disabled');

		drawLeftBar();

		await RenderTools.setRenderStyle(documentLayout.Settings.numberingType);

		$('.edit_control').hide();

		$('#statistics_graphs').html("<div style='font-style:italic'>Loading statistics</div>");

		var urlVars = getUrlVars();
		if ('c' in urlVars) {

			selectItem(urlVars.c);

			var partData = false;
			for (var i = 0; i < documentScoring.Sections.length; ++i) {
				for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
					if (documentScoring.Sections[i].Content[j].id == urlVars.c) {
						partData = documentScoring.Sections[i].Content[j];
					}
				}
			}

			if ((partData !== false) && ('Source' in partData)) {
				var questionHandleID = partData.Source.question_handle_id;
				var partID = partData.Source.part_id;

				$('#item_statistics_data').html("<div style='font-style:italic'>Loading statistics</div>");
				$('#statistics_n').html('0');
	
				$('#statistics_item').empty();
				$('#statistics_item').attr('data-question-handle-id', questionHandleID);

				var $part = $('#assessment_preview_wrapper .part_wrapper[data-content-id="' + urlVars.c + '"]');
				var $partCopy = $part.clone();

				$partCopy.removeClass('question_selected');
				$('#statistics_item').append($partCopy);

				var thisData = questionData[questionHandleID].json_data;

				var hasContext = false;
				for (var i = 0; i < thisData.Parts.length; ++i) {
					if (thisData.Parts[i].id == partID) break;
					else if (thisData.Parts[i].type == 'context') hasContext = true;
				}

				if (hasContext) {
					var $contextWrapper = $part.prevAll('.context_wrapper').eq(0);
					$('#statistics_item').prepend($contextWrapper.clone());
					$('#statistics_item').prepend($contextWrapper.prev('.context_header').clone());
				}

				await RenderTools.renderEquations($('#statistics_item'));

				if (documentLayout.Settings.mcFormat == 'Auto') {
					$('#statistics_item .part_wrapper[data-type="mc"]').each(function() { 
						RenderTools.setMCWidths($(this)); 
					});
				} else $('#statistics_item .question_answer').css('width', '100%');	

				$('#statistics_item .part_wrapper').each(function() {
					if ($(this).find('.question_answer_mc, .question_answer_table').length > 0) {
						RenderTools.setMCImageWidths($(this));
					}
				});

			} else {

				$('#statistics_item').html('<i>No statistics available.</i>');
				$('#item_statistics_wrapper').hide();
	
			}
		
		} else {

			if ($('#question_sections_menu .menu_item').length > 0) {			
				$('#question_sections_menu .menu_item').removeClass('active');
				$('#question_sections_menu .menu_item').eq(0).addClass('active');
			}

			$('#question_navigation_wrapper .items_panel').eq(0).show();

			$('#statistics_item').html('<i>No question selected.</i>');
			$('#item_statistics_wrapper').hide();

		}
		
		var questionIDs = [];
		var questionHandleIDs = [];
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				var thisEntry = documentScoring.Sections[i].Content[j];
				if ('Source' in thisEntry) {
					var questionHandleID = thisEntry.Source.question_handle_id;
					if (questionHandleIDs.indexOf(questionHandleID) == -1) {
						questionIDs.push(parseInt(questionData[questionHandleID].id));
						questionHandleIDs.push(questionHandleID);	
					}
				}
			}
		}

		for (var i = 0; i < questionHandleIDs.length; ++i) {
			questionHandleIDs[i] = [questionHandleIDs[i]];
		}

		await StatsCommon.loadFilteredStatistics(questionIDs);

		var urlVars = getUrlVars();
		if ('c' in urlVars) {
			var questionHandleID = parseInt($('#statistics_item').attr('data-question-handle-id'));
			var questionID = questionData[questionHandleID].id;

			var renderAsVersion = $('#stats_filter_by_version').prop('checked');

			StatsCommon.rebuildItemStatistics(questionID, questionData[questionHandleID].json_data, documentLayout.Settings.numberingType, renderAsVersion);
			$('#item_statistics_wrapper .question_number').hide();
		}
		StatsCommon.rebuildAssessmentStatistics(questionHandleIDs, questionData);

		ScrollingCommon.resetLeftPanel();

		$("#stats_print").removeAttr('disabled');
	}

	var drawLeftBar = function() {
		if (documentLayout.Body.length > 0) {

			$('#form_sections_menu').empty();
			$('#question_sections_menu').empty();
			$('.items_panel').remove();

			var activeSectionID = null;
			var activeContentID = null;
	
			var urlVars = getUrlVars();
			if ('s' in urlVars) {
				activeSectionID = urlVars.s;
			} else if ('c' in urlVars) {
				for (var i = 0; i < documentScoring.Sections.length; ++i) {
					for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
						if (documentScoring.Sections[i].Content[j].id == urlVars.c) {
							activeSectionID = documentScoring.Sections[i].id;
						}	
					}
				}
	
				if (activeSectionID != null) {
					activeContentID = urlVars.c;
				}
			}
	
			var foundSection = false;

			for (var i = 0; i < documentLayout.Body.length; ++i) {
				var layoutData = documentLayout.Body[i];
				if (layoutData.id == activeSectionID) foundSection = true;

				var newHTML = '';
				if (layoutData.type == 'page_break') {

					newHTML += 
						'<li class="formatting_item"><div class="menu_item" data-section-id="' + layoutData.id + '"> \
							<div class="menu_text">Page break</div>';
					if (canEdit) newHTML += '<div title="Delete" class="menu_button delete_icon"></div>';
					newHTML += '</div></li>';		

				} else {

					newHTML += 
						'<li><div class="menu_item" data-section-id="' + layoutData.id + '"> \
							<div class="menu_text">' + layoutData.header + ' (' + layoutData.type.toUpperCase() + ' Section)</div>';
					if (canEdit && !isGenerated) newHTML += '<div title="Delete" class="menu_button delete_icon"></div>';
					newHTML += '</div></li>';
				}
				
				$('#form_sections_menu').append(newHTML);
			}

			var questionSectionIndex = -1;
			var hasQuestionSections = true;
			var itemsPanelHTML = false;
			$('#assessment_preview_wrapper').children().each(function() {

				if ($(this).is('.preview_header') && hasQuestionSections) {

					if (itemsPanelHTML !== false) {
						itemsPanelHTML += '</ul></div></div></div>';
						$('#question_navigation_wrapper').append(itemsPanelHTML);
					}

					questionSectionIndex++;

					itemsPanelHTML =
						'<div class="items_panel" data-section-index="' + questionSectionIndex + '" style="display:none;"> \
							<div class="window_bar">' + $(this).text() + '</div> \
							<div class="window_content" style="padding:5px;"> \
								<div class="layout_panel"> \
									<ul class="items_menu menu_wrapper">';

					var sectionMenuHTML = 
						'<li>' + 
							'<div class="menu_item">' +
								'<div class="menu_text">' + $(this).text() + '</div>' +
							'</div>' + 
						'</li>';
					$('#question_sections_menu').append(sectionMenuHTML);


				} else if ($(this).is('.part_wrapper')) {

					if ((questionSectionIndex == -1) && hasQuestionSections) {
						itemsPanelHTML =
							'<div class="items_panel" data-section-index="-1" style="display:none;"> \
								<div class="window_bar">Questions</div> \
								<div class="window_content" style="padding:5px;"> \
									<div class="layout_panel"> \
										<ul class="items_menu menu_wrapper">';

						hasQuestionSections = false;
					}

					// Get question number
		
					var firstPart = $(this).prevAll('.context_wrapper').eq(0).find('.question_number').text();
					if (firstPart == '&nbsp;') firstPart = '';
					else firstPart = firstPart.slice(0, -1);
					var secondPart = $(this).children('.question_number').text().slice(0, -1);
		
					// Build question entry in left panel

					itemsPanelHTML += 
						'<li>' +
							'<div class="menu_item" data-content-id="' + $(this).attr('data-content-id') + '">' +		
								'<div class="menu_text">' + 
									firstPart + secondPart + '. ' + $(this).find('.question_prompt').text() + 
								'</div>' + 
							'</div>' + 
						'</li>';

				}
			});

			if (hasQuestionSections) $('#question_sections_wrapper').show();
			else $('#question_sections_wrapper').hide();

			if (itemsPanelHTML !== false) {
				itemsPanelHTML += '</ul></div></div></div>';
				$('#question_navigation_wrapper').append(itemsPanelHTML);
				itemsPanelHTML = false;
			}

			$('#empty_layout').hide();
			$('#form_sections_menu').show();

			if (canEdit) {
				var sortableOptions = {
					start: function(event, ui) {
						$draggingSection = ui.item;
					}, 
					stop: onSectionSortStop
				};
				if (!canReorder) sortableOptions['cancel'] = 'li:not(.formatting_item)';
				$('#form_sections_menu').sortable(sortableOptions);
			}
	
			if (!foundSection) {

                delete urlVars.s;
				activeSectionID = null;
                
                delete urlVars.c;
				activeContentID = null;

                setUrlVars(urlVars);
                
			}

			if (activeSectionID != null) {
				if (activeContentID != null) selectItem(activeContentID);
				else selectSection(activeSectionID);
			}
	
		} else {

			$('#empty_layout').show();
			$('#form_sections_menu').hide();

			var urlVars = getUrlVars();
            delete urlVars.s;
            delete urlVars.c;
            setUrlVars(urlVars);

		}
    }
    
    // Initialize statistics filters
	
	$('#stats_update').on('mousedown', function(e) {
		e.preventDefault();
		
		$('#statistics_graphs').html("<div style='font-style:italic'>Loading statistics&hellip;</div>");
		$('#item_statistics_data').html("<div style='font-style:italic'>Loading statistics&hellip;</div>");
		$('#statistics_n').html('0');

		var questionIDs = [];
		var questionHandleIDs = [];
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				var thisEntry = documentScoring.Sections[i].Content[j];
				if ('Source' in thisEntry) {
					var questionHandleID = thisEntry.Source.question_handle_id;
					if (questionHandleIDs.indexOf(questionHandleID) == -1) {
						questionIDs.push(parseInt(questionData[questionHandleID].id));
						questionHandleIDs.push(questionHandleID);	
					}
				}
			}
		}

		for (var i = 0; i < questionHandleIDs.length; ++i) {
			questionHandleIDs[i] = [questionHandleIDs[i]];
		}

		showSubmitProgress($('#stats_button_wrapper'));
		$("#stats_print").attr('disabled', 'disabled');
	
		StatsCommon.loadFilteredStatistics(questionIDs)
		.then(function() {
			var urlVars = getUrlVars();
			if ('c' in urlVars) {
				var questionHandleID = $('#statistics_item').attr('data-question-handle-id');
				var questionID = questionData[questionHandleID].id;

				var renderAsVersion = $('#stats_filter_by_version').prop('checked');

				StatsCommon.rebuildItemStatistics(questionID, questionData[questionHandleID].json_data, documentLayout.Settings.numberingType, renderAsVersion);
			}
			StatsCommon.rebuildAssessmentStatistics(questionHandleIDs, questionData);

			hideSubmitProgress($('#stats_button_wrapper'));
			$("#stats_print").removeAttr('disabled');
		});
	});

	$('#stats_print').on('mousedown', function(e) {
		e.preventDefault();
	
		var questionHandleIDs = [];
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				var thisEntry = documentScoring.Sections[i].Content[j];
				if ('Source' in thisEntry) {
					var questionHandleID = thisEntry.Source.question_handle_id;
					if (questionHandleIDs.indexOf(questionHandleID) == -1) {
						questionHandleIDs.push(questionHandleID);	
					}
				}
			}
		}

		for (var i = 0; i < questionHandleIDs.length; ++i) {
			questionHandleIDs[i] = [questionHandleIDs[i]];
		}

		var renderAsVersion = $('#stats_filter_by_version').prop('checked');

		var statsHTML = StatsCommon.buildStatsHTML(questionHandleIDs, questionData, renderAsVersion);

		navigateWithPost('/build_pdf', {
			pdfType: 'pdf_from_url',
			url: '/Documents/print_questions/' + documentID,
			postDataJSON: JSON.stringify({ html: statsHTML }),
			target: 'statistics.pdf'
		});

	});
	
	$('#version_conflict_dialog').dialog({
		autoOpen: false,
		width:400,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Go back")').addClass('btn btn_font');
			$buttonPane.find('button:contains("Reload")').addClass('btn btn-primary btn_font');
		}
    });

	function saveDocumentData() {

		// Save assessment

		var scoringString = JSON.stringify(documentScoring);
		var layoutString = JSON.stringify(documentLayout);

		var postData = {
			version_id: documentVersionID,
			scoring_string: scoringString,
			scoring_hash: SparkMD5.hash(scoringString),
			layout_string: layoutString,
			layout_hash: SparkMD5.hash(layoutString)
		};

		var newSaveName = $('#ddoc_save_name').val();
		if ((newSaveName.length > 0) && (newSaveName != $('#document_title').text())) {
			postData.save_name = newSaveName;
		}

		return ajaxFetch('/Documents/save/' + documentID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(postData)
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {

				// Update version

				documentVersionID = responseData.version_id;

				// Update view

				drawView();
				return;

			} else if (('error_type' in responseData) && (responseData.error_type == 'version_conflict')) {

				return new Promise((resolve, reject) => {
					$('#version_conflict_dialog').dialog(
						'option', 
						'buttons', {
							'Go back': function() {
								$(this).dialog("close");
								reject();
							},
							'Reload': function() {
								window.onbeforeunload = null;
								window.location.reload();
							}
						}
					);
		
					$('#version_conflict_dialog').dialog('open');
				});

			} else {

				throw new Error('Unable to save form');

			}
		});
	}

	function cleanOutcomes() {
		var allowedItemIDs = [];
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				allowedItemIDs.push(documentScoring.Sections[i].Content[j].id);
			}
		}

		var outcomeIndex = 0;
		while (outcomeIndex < documentScoring.Outcomes.length) {
			var itemIndex = 0;
			while (itemIndex < documentScoring.Outcomes[outcomeIndex].item_ids.length) {
				var outcomeItemID = documentScoring.Outcomes[outcomeIndex].item_ids[itemIndex];
				if (allowedItemIDs.indexOf(outcomeItemID) == -1) {
					documentScoring.Outcomes[outcomeIndex].item_ids.splice(itemIndex, 1);
				} else ++itemIndex;
			}

			if (documentScoring.Outcomes[outcomeIndex].item_ids.length == 0) {
				documentScoring.Outcomes.splice(outcomeIndex, 1);
			} else ++outcomeIndex;
		}	
	}

	function cleanWeights() {
		if (documentScoring.Scoring.type == 'Total') {

		} else if (documentScoring.Scoring.type == 'Custom') {
			
			var allowedValues = [];
			for (var i = 0; i < documentScoring.Sections.length; ++i) {
				for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
					allowedValues.push(documentScoring.Sections[i].Content[j].id);
				}
			}

			var weightIndex = 0;
			while (weightIndex < documentScoring.Scoring.weights.length) {
				var itemIDs = documentScoring.Scoring.weights[weightIndex].item_ids;

				var itemIndex = 0;
				while (itemIndex < itemIDs.length) {
					if (allowedValues.indexOf(itemIDs[itemIndex]) == -1) {
						itemIDs.splice(itemIndex, 1);
					} else ++itemIndex;
				}

				if (itemIDs.length == 0) documentScoring.Scoring.weights.splice(weightIndex, 1);
				else ++weightIndex;
			}

		} else {

			var idName;
			var allowedValues = [];

			if (documentScoring.Scoring.type == 'Sections') {
				idName = 'section_id';
				for (var i = 0; i < documentScoring.Sections.length; ++i) {
					allowedValues.push(documentScoring.Sections[i].id);
				}				
			} else if (documentScoring.Scoring.type == 'Outcomes') {
				idName = 'outcome_id';
				for (var i = 0; i < documentScoring.Outcomes.length; ++i) {
					allowedValues.push(documentScoring.Outcomes[i].id);
				}
			}
	
			var weightIndex = 0;
			while (weightIndex < documentScoring.Scoring.weights.length) {
				var thisID = documentScoring.Scoring.weights[weightIndex][idName];
				if (allowedValues.indexOf(thisID) == -1) {
					documentScoring.Scoring.weights.splice(weightIndex, 1);
				} else ++weightIndex;
			}

		}
	}
    
	/**************************************************************************************************/
	/* Editing settings                                                                               */
	/**************************************************************************************************/

	var updateSettingsVisibility = function(prefix) {
		if ($(prefix + '_title_showNameField').prop('checked')) {
			$(prefix + '_nameDetails_container').show();
		} else $(prefix + '_nameDetails_container').hide();

		if ($(prefix + '_title_showIDField').prop('checked')) {
			$(prefix + '_idDetails_container').show();
		} else $(prefix + '_idDetails_container').hide();

        if ($(prefix + '_scoring_type').val() == 'Total') {
            $(prefix + '_student_weights').parent().hide();
        } else $(prefix + '_student_weights').parent().show();

        if (documentScoring.Outcomes.length > 0) {
            $(prefix + '_student_outcomes').parent().show();
        } else $(prefix + '_student_outcomes').parent().hide();

        if ($(prefix + '_student_responses').val() == 'none') {
            $(prefix + '_student_incorrectonly').parent().hide();
        } else $(prefix + '_student_incorrectonly').parent().show();

        if ($(prefix + '_student_results').val() != 'numerical') {
            $(prefix + '_proficiencies_wrapper').show();
        } else $(prefix + '_proficiencies_wrapper').hide();
	}

	function resizeSettingsWrappers() {
		$('#editDocument_buttons').css('margin-top', '5px');
		$('#editDocument_wrapper_left').css('height', 'auto');
		var leftWrapperHeight = $('#editDocument_wrapper_left').outerHeight();
		$('#editDocument_wrapper_right').css('height', 'auto');
		var rightWrapperHeight = $('#editDocument_wrapper_right').outerHeight();

		var newHeight = Math.max(leftWrapperHeight, rightWrapperHeight);
		$('#editDocument_wrapper_left').outerHeight(newHeight);
		$('#editDocument_wrapper_right').outerHeight(newHeight);
		if (rightWrapperHeight > leftWrapperHeight) {
			var newTopMargin = (rightWrapperHeight - leftWrapperHeight + 5) + 'px';
			$('#editDocument_buttons').css('margin-top', newTopMargin);
		}
	}
	
	var drawBasicSettings = function(prefix) {

		$(prefix + '_save_name').val($('#document_title').text());

		var titleText = '';
		for (var i = 0; i < documentLayout.Title.text.length; ++i) {
			if (i > 0) titleText += "\n";
			titleText += documentLayout.Title.text[i];
		}
		$(prefix + '_title_text').val(titleText);

		if ('nameField' in documentLayout.Title) {
			$(prefix + '_title_showNameField').prop('checked', true);
			$(prefix + '_title_nameLabel').val(documentLayout.Title.nameField.label);
			$(prefix + '_title_nameLetters').val(documentLayout.Title.nameField.bubbles);
		} else {
			$(prefix + '_title_showNameField').prop('checked', false);
			$(prefix + '_title_nameLabel').val(userDefaults.Document.Title.nameField.label);
			$(prefix + '_title_nameLetters').val(userDefaults.Document.Title.nameField.bubbles);
		}
				
		if ('idField' in documentLayout.Title) {
			$(prefix + '_title_showIDField').prop('checked', true);
			$(prefix + '_title_idLabel').val(documentLayout.Title.idField.label);
			$(prefix + '_title_idDigits').val(documentLayout.Title.idField.bubbles);
		} else {
			$(prefix + '_title_showIDField').prop('checked', false);
			$(prefix + '_title_idLabel').val(userDefaults.Document.Title.idField.label);
			$(prefix + '_title_idDigits').val(userDefaults.Document.Title.idField.bubbles);
		}
		
		$(prefix + '_page_size').val(documentLayout.Page.pageSize);

		$(prefix + '_topMarginSize').val(documentLayout.Page.marginSizes.top);
		$(prefix + '_bottomMarginSize').val(documentLayout.Page.marginSizes.bottom);
		$(prefix + '_leftMarginSize').val(documentLayout.Page.marginSizes.left);
		$(prefix + '_rightMarginSize').val(documentLayout.Page.marginSizes.right);

		$(prefix + '_copiesPerPage').val(documentLayout.Page.copiesPerPage);
		$(prefix + '_show_logo').prop('checked', ('logo' in documentLayout.Title)).trigger('change');

        $(prefix + '_version').val(documentLayout.Settings.version);

		$(prefix + '_language').val(documentLayout.Settings.language);

		for (var i = 0; i < reportOptions.length; ++i) {
			var thisName = reportOptions[i].name;
			var thisSuffix = reportOptions[i].suffix;
            if (thisName in documentLayout.StudentReports) {
                $(prefix + thisSuffix).val(documentLayout.StudentReports[thisName]);
            } else $(prefix + thisSuffix).val(userDefaults.Document.Reports.Student[thisName]);	    
		}

        if ('incorrectOnly' in documentLayout.StudentReports) {
            $(prefix + '_student_incorrectonly').prop('checked', documentLayout.StudentReports.incorrectOnly);
        } else $(prefix + '_student_incorrectonly').prop('checked', userDefaults.Document.Reports.Student.incorrectOnly);	
    }

	var drawDocument = function() {
		$('#form_sections_menu .menu_item').removeClass('active');

		$('.items_panel').hide();

		$('.document_detail_container').hide();
		$('#outcomes_table').hide();

		// Draw basic settings

		drawBasicSettings('#doc');

		// Draw name lines

		if ('namelines' in documentLayout.Title) {
			$('#doc_title_nameLines').val(documentLayout.Title.nameLines.join("\n"));
			var lineHeight = parseFloat($('#doc_title_nameLines').css('line-height'));
			$('#doc_title_nameLines').height(lineHeight * documentLayout.Title.nameLines.length);	
			$('#doc_nameLines_wrapper').show();
		} else $('#doc_nameLines_wrapper').hide();

        // Draw scoring type

        $('#doc_scoring_type').val(documentScoring.Scoring.type);

		// Draw proficiency levels

		var proficiencyLevels;
		if ('outcomesLevels' in documentLayout.StudentReports) proficiencyLevels = documentLayout.StudentReports.outcomesLevels;
		else proficiencyLevels = userDefaults.Document.Reports.Student.outcomesLevels;
		
		var tableHTML = "";
		for (var i = 0; i < proficiencyLevels.length; ++i) {
			var thisName = proficiencyLevels[i].name;
			var thisFrom = proficiencyLevels[i].from;
			var thisTo;
			if (i + 1 < proficiencyLevels.length) thisTo = proficiencyLevels[i + 1].from;
			else thisTo = 100;
			tableHTML += '<div class="levels_row"><div style="width:120px;">' + thisName + '</div><div style="width:auto;">' + thisFrom + '-' + thisTo + '%</div></div>';
		}
		$('#doc_levels_wrapper').html(tableHTML);

		// Finish up

		updateSettingsVisibility('#doc');
		$('#document_detail').show();

		if (showSettings > 0) $('.startEditDoc').trigger('click');
	}

	$('.document_settings_tab').on('mousedown', function() {
		if (!$(this).hasClass('selected')) {
			var $form = $(this).closest('form');
			if (checkProficiencyLevels() && $form.validationEngine('validate')) {
				$('.document_settings_tab').removeClass('selected');
				$(this).addClass('selected');
				
				$('.settings_wrapper').hide();
				$('#' + $(this).attr('data-wrapper-id')).show();
				
				resizeSettingsWrappers();
			}
		}
		return false;
	});

	$('.startEditDoc').on('click', function() {
		if (!canEdit) return;

		// Draw basic settings

		drawBasicSettings('#ddoc');

		// Draw name lines

		if ('nameLines' in documentLayout.Title) {
			$('#ddoc_title_nameLines').val(JSON.stringify(documentLayout.Title.nameLines));
		} else $('#ddoc_title_nameLines').val('[]');
		rebuildNameLines();

		// Manage logo controls

		if ($('#ddoc_show_logo').prop('checked')) {
			$('#ddoc_show_logo').removeAttr('disabled');
			$('#ddoc_show_logo_label').removeClass("disabled");
		} else if (userDefaults.Common.Title.logo.imageHash == false) {
			$('#ddoc_show_logo').attr('disabled', 'disabled');
			$('#ddoc_show_logo_label').addClass("disabled");
		} else {
			$('#ddoc_show_logo').removeAttr('disabled');
			$('#ddoc_show_logo_label').removeClass("disabled");
		}
		
		// Draw scoring controls

		$('#ddoc_scoring_type').val(documentScoring.Scoring.type);

		var weights = ('weights' in documentScoring.Scoring) ? documentScoring.Scoring.weights : [];
		$('#ddoc_weights').val(JSON.stringify(weights));
		drawWeightBars();
		
		if (isGenerated) $('#ddoc_scoring_type').attr('disabled', 'disabled');
		else $('#ddoc_scoring_type').removeAttr('disabled');

		// Draw proficiency levels

		if ('outcomesLevels' in documentLayout.StudentReports) {
			$('#ddoc_proficiencyLevels').val(JSON.stringify(documentLayout.StudentReports.outcomesLevels));
		} else $('#ddoc_proficiencyLevels').val(JSON.stringify(userDefaults.Document.Reports.Student.outcomesLevels));
		rebuildProficiencyLevels();

		// Finish up

		$('#editDocument_form').dialog('open');
		
		updateSettingsVisibility('#ddoc');
		resizeSettingsWrappers();
		
		$('.document_settings_tab:first-child').trigger('mousedown');
		if (showSettings > 0) $('#ddoc_save_name').select();
    });

	$('#editDocument_form').dialog({
		autoOpen: false,
		width:715,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, open: function(event) {
		 	$('#ddoc_save_name').focus();
		 	initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
    });

	$('#editDocument_form').find('button:contains("Cancel")').click(function() {
		if (canCancel == 1) {
			if (pendingAJAXPost) return false;
			showSubmitProgress($('#editDocument_form'));
			pendingAJAXPost = true;

			ajaxFetch('/Documents/delete/' + documentID, {
				method: 'POST'
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
				if (responseData.success) {

					addClientFlash({
						title: "Success",
						text: "New form cancelled.",
						image: "/img/success.svg"
					});
					window.location = '/Documents/index';
	
				} else {

					throw new Error("Unable to cancel form");

				}
			}).catch(function() {
				showSubmitError();
			});
		} else $('#editDocument_form').dialog("close");
	});
		 	
	$('#editDocument_form').find('button:contains("Save")').click(function() {
		var contextedSave = $.proxy(settingsSave, $('#editDocument_form'));
		contextedSave();
	});

    $('.document_settings_tab').on('click', function() {
	    if ($(this).attr('data-wrapper-id') == 'scoring_settings_wrapper') {
			$('.section_weight_name_wrapper').each(function() {
				var targetWidth = $(this).innerWidth() - 1;
				var $count = $(this).find('.section_weight_count');
				if ($count.length > 0) targetWidth -= $count.outerWidth();

				$(this).find('.section_weight_name').css('width', 'auto');
				if ($(this).find('.section_weight_name').width() > targetWidth) {
					$(this).find('.section_weight_name').width(targetWidth);
				}				
			});			
	    }
    });
  
	// Editing basic settings

	$('#ddoc_title_showNameField').on('change', function() {
		updateSettingsVisibility('#ddoc');
		resizeSettingsWrappers();
    });

    $('#ddoc_title_showIDField').on('change', function() {
		updateSettingsVisibility('#ddoc');
		resizeSettingsWrappers();
    });

	$('#ddoc_show_logo').on('change', function() {
		if (!$('#ddoc_show_logo').prop('checked')) {
			if (userDefaults.Common.Title.logo.imageHash == false) {
				$('#ddoc_show_logo').attr('disabled', 'disabled');
				$('#ddoc_show_logo_label').addClass("disabled");
			} else {
				$('#ddoc_show_logo').removeAttr('disabled');
				$('#ddoc_show_logo_label').removeClass("disabled");
			}
		}

		updateSettingsVisibility('#ddoc');
		resizeSettingsWrappers();
	});

	$('#ddoc_nameLines_wrapper').on('keydown', 'input', function(e) {
		if ((e.key === 'Tab') || (e.key === 'Enter')) {
			e.preventDefault();
			var $nextRow = $(this).closest('.nameLines_row').next('.nameLines_row');
			if ($nextRow.length != 0) $nextRow.find('input').focus().select();
			else $(this).closest('.nameLines_row').find('.namelineAdd').trigger('click');
		}
	});

	$('#ddoc_nameLines_wrapper').on('click', '.namelineAdd', function() {
		var index = parseInt($(this).attr('data-index')) + 1;
		if (index == 0) {
			if (userDefaults.Common.Title.nameLines.length > 0) {
				$('#ddoc_title_nameLines').val(JSON.stringify(userDefaults.Common.Title.nameLines));
			} else $('#ddoc_title_nameLines').val('Name');
		} else {
			rebuildNameLinesField();
			var nameLines = JSON.parse($('#ddoc_title_nameLines').val());
			nameLines.splice(index, 0, "");
			$('#ddoc_title_nameLines').val(JSON.stringify(nameLines));
		}
		rebuildNameLines();
		resizeSettingsWrappers();
		$('#ddoc_nameLines_wrapper .nameLines_row').eq(index).find('input').focus();
    });
    
	$(document).on('click', '.namelineDelete', function() {
		rebuildNameLinesField();
		var index = parseInt($(this).attr('data-index'));
		var nameLines = JSON.parse($('#ddoc_title_nameLines').val());
		nameLines.splice(index, 1);
		$('#ddoc_title_nameLines').val(JSON.stringify(nameLines));
   	    rebuildNameLines();
   	    resizeSettingsWrappers();
    });

	function rebuildNameLinesField() {
		var nameLines = [];

		var i = 0;
		while ($('#ddoc_nameline_' + i).length > 0) {
			nameLines.push($('#ddoc_nameline_' + i).val());
			++i;
		}
		$('#ddoc_title_nameLines').val(JSON.stringify(nameLines));
	}

	function rebuildNameLines() {
		var nameLines = JSON.parse($('#ddoc_title_nameLines').val());

		var add_to = "";
		if (nameLines.length == 0) {
			add_to += '<div class="nameLines_row" style="padding-bottom:5px;">';
			add_to += '<input class="defaultTextBox2" style="width:200px !important; margin-bottom:0px;" type="text" value="No name lines shown" disabled />';
			add_to += '<button title="Add" class="menu_button add_icon namelineAdd" style="margin-left:5px;" data-index=-1>&nbsp;</button>';
			add_to += '<button title="Delete" class="menu_button delete_icon namelineDelete" disabled data-index=-1>&nbsp;</button>';
			add_to += '</div>';
		} else {
			for (var j = 0; j < nameLines.length; ++j) {
				add_to += '<div class="nameLines_row" style="padding-bottom:5px;">';
				add_to += '<input id="ddoc_nameline_' + j + '" class="validate[required] defaultTextBox2" style="width:200px !important; margin-bottom:0px;" type="text" value="' + nameLines[j] + '" />';
				add_to += '<button title="Add" class="menu_button add_icon namelineAdd" style="margin-left:5px;" data-index=' + j + '>&nbsp;</button>';
				add_to += '<button title="Delete" class="menu_button delete_icon namelineDelete" data-index=' + j + '>&nbsp;</button>';
				add_to += '</div>';
			}
		}
		$('#ddoc_nameLines_wrapper').html(add_to);
	}

	// Editing section or outcome weights

    $('#ddoc_scoring_type').on('change', function() {
		$('#ddoc_weights').val('[]');
		drawWeightBars();
		updateSettingsVisibility('#ddoc');
	});

	function updateWeightBars() {
	    var weights_total = 0.0;
	    $('.section_weight').each(function() {
			var weight = parseFloat($(this).val());
			if (!isNaN(weight)) weights_total += parseFloat($(this).val()); 
	    });
	    
	    $('.section_weights_row').each(function() {
			var weight = parseFloat($(this).find('.section_weight').val());
			if (isNaN(weight)) weight = 0.0;
			
			var weight_percent;
			if (weight == 0.0) weight_percent = 0.0;
			else weight_percent = weight * 100 / weights_total;

			$(this).find('.section_weight_bar').width(164 * weight_percent / 100);
			$(this).find('.section_weight_percent').html(weight_percent.toFixed(0) + "%");
	    });
    }
    
    function drawWeightBars() {
		if ($('#ddoc_scoring_type').val() == 'Total') {
			$('#section_weights').hide();
		} else {
			var weights = JSON.parse($('#ddoc_weights').val());
	
			var weightsHTML = "";
			if ($('#ddoc_scoring_type').val() == 'Sections') {

				for (var i = 0; i < documentScoring.Sections.length; ++i) {
					var sectionID = documentScoring.Sections[i].id;
					var numItems = documentScoring.Sections[i].Content.length;

					var header = '[No header]';
					for (var j = 0; j < documentLayout.Body.length; ++j) {
						if (documentLayout.Body[j].id == sectionID) header = documentLayout.Body[j].header;
					}

					var sectionWeight = 0.0;
					for (var j = 0; j < weights.length; ++j) {
						if (weights[j].section_id == sectionID) sectionWeight = weights[j].weight;
					}

					weightsHTML += 
						'<div class="section_weights_row" data-section-id="' + sectionID + '">' +
							'<div class="section_weight_name_wrapper">' +
								'<div class="section_weight_name">' + header + '</div>' +
								'<div class="section_weight_count"> (' + numItems + ' questions)</div>' +
							'</div>' + 
							'<div>' + 
								'<div class="section_weight_label">Weight:</div>' + 
								'<input class="section_weight validate[custom[isPosDecimal]]" value="' + sectionWeight + '"/>' + 
								'<div class="section_weight_visual">' + 
									'<div class="section_weight_bar"></div>' + 
									'<div class="section_weight_percent"></div>' + 
								'</div>' + 
							'</div>' + 
						'</div>';
				}

			} else if ($('#ddoc_scoring_type').val() == 'Outcomes') {

				var sortedCategories = [];
				for (var i = 0; i < documentScoring.Outcomes.length; ++i) {
					sortedCategories.push({
						id: documentScoring.Outcomes[i].id,
						name: documentScoring.Outcomes[i].name,
						count: documentScoring.Outcomes[i].item_ids.length
					});
				}

				sortedCategories.sort(function(a, b) {
					return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
				});

				if (sortedCategories.length > 0) {
					for (var i = 0; i < sortedCategories.length; ++i) {
						var outcomeID = sortedCategories[i].id;

						var outcomeWeight = 0.0;
						for (var j = 0; j < weights.length; ++j) {
							if (weights[j].outcome_id == outcomeID) outcomeWeight = weights[j].weight;
						}

						weightsHTML += 
							'<div class="section_weights_row" data-outcome-id="' + outcomeID + '">' + 
								'<div class="section_weight_name_wrapper">' + 
									'<div class="section_weight_name">' + sortedCategories[i].name + '</div>' + 
									'<div class="section_weight_count"> (' + sortedCategories[i].count + ' questions)</div>' + 
								'</div>' + 
								'<div>' + 
									'<div class="section_weight_label">Weight:</div>' + 
									'<input class="section_weight validate[custom[isPosDecimal]]" value="' + outcomeWeight + '"/>' + 
									'<div class="section_weight_visual">' + 
										'<div class="section_weight_bar"></div>' + 
										'<div class="section_weight_percent"></div>' + 
									'</div>' + 
								'</div>' + 
							'</div>';
					}
				}		

			} else if ($('#ddoc_scoring_type').val() == 'Custom') {

				for (var i = 0; i < weights.length; ++i) {

					weightsHTML += 
						'<div class="section_weights_row" data-custom-index="' + i + '">' + 
							'<div class="section_weight_name_wrapper">' + 
								'<div class="section_weight_name">' + weights[i].name + '</div>' + 
								'<div class="section_weight_count"> (' + weights[i].item_ids.length + ' questions)</div>' + 
							'</div>' + 
							'<div>' + 
								'<div class="section_weight_label">Weight:</div>' + 
								'<input class="section_weight" value="' + weights[i].weight + '"/>' + 
								'<div class="section_weight_visual">' + 
									'<div class="section_weight_bar"></div>' + 
									'<div class="section_weight_percent"></div>' + 
								'</div>' + 
							'</div>' + 
						'</div>';

				}	

			}
			$('#section_weights').html(weightsHTML);
			
			updateWeightBars();
			$('#section_weights').show();
		}
    }
    
    function rebuildWeights() {
		if ($('#ddoc_scoring_type').val() == 'Sections') {
			var weights = [];
		    $('.section_weights_row').each(function() {
				weights.push({
					section_id: $(this).attr('data-section-id'),
					weight: parseFloat($(this).find('.section_weight').val())
				});
			});
			$('#ddoc_weights').val(JSON.stringify(weights));
		} else if ($('#ddoc_scoring_type').val() == 'Outcomes') {
			var weights = [];
		    $('.section_weights_row').each(function() {
				weights.push({
					outcome_id: $(this).attr('data-outcome-id'),
					weight: parseFloat($(this).find('.section_weight').val())
				});
			});
			$('#ddoc_weights').val(JSON.stringify(weights));
		} else if ($('#ddoc_scoring_type').val() == 'Custom') {
			var weights = JSON.parse($('#ddoc_weights').val());
		    $('.section_weights_row').each(function() {
			    var index = parseInt($(this).attr('data-custom-index'));
				var weight = parseFloat($(this).find('.section_weight').val());
				weights[index].weight = weight;
			});
			$('#ddoc_weights').val(JSON.stringify(weights));
		} else $('#ddoc_weights').val('[]');
    }

	$('#section_weights').on('keydown', '.section_weight', function(e) {
		if ((e.key === 'Enter') || (e.key === 'Tab')) {
			e.preventDefault();
			var $nextRow = $(this).closest('.section_weights_row').next('.section_weights_row');
			if ($nextRow.length != 0) {
				$nextRow.find('.section_weight').focus().select();
			}
		}
	});

    $('#section_weights').on('change', '.section_weight', function() {
	    updateWeightBars();
    });

	// Editing report settings
    
    $('#ddoc_student_responses').on('change', function() {
		updateSettingsVisibility('#ddoc');
		resizeSettingsWrappers();
	});

    $('#ddoc_student_results').on('change', function() {
		updateSettingsVisibility('#ddoc');
		resizeSettingsWrappers();
	});
    
	$(document).on('click', '.proficiencyLevelAdd', function() {
		rebuildProficiencyLevelsField();
		var index = parseInt($(this).attr('data-index')) + 1;
		var proficiencyLevels = JSON.parse($('#ddoc_proficiencyLevels').val());
		proficiencyLevels.splice(index, 0, {name:'', from:0});
		$('#ddoc_proficiencyLevels').val(JSON.stringify(proficiencyLevels));
		rebuildProficiencyLevels();
		resizeSettingsWrappers();
    });
    
	$(document).on('click', '.proficiencyLevelDelete', function() {
		rebuildProficiencyLevelsField();
		var index = parseInt($(this).attr('data-index'));
		var proficiencyLevels = JSON.parse($('#ddoc_proficiencyLevels').val());
		proficiencyLevels.splice(index, 1);
		$('#ddoc_proficiencyLevels').val(JSON.stringify(proficiencyLevels));
   	    rebuildProficiencyLevels();
   	    resizeSettingsWrappers();
    });

	function rebuildProficiencyLevelsField() {
		var newData = [];
		var i = 0;
		while ($('#ddoc_levelname_' + i).length > 0) {
			var thisName = $('#ddoc_levelname_' + i).val();
			var thisFrom = parseInt($('#ddoc_levelfrom_' + i).val());
			newData.push({name:thisName,from:thisFrom});
			i++;
		}
		$('#ddoc_proficiencyLevels').val(JSON.stringify(newData));
	}

	function rebuildProficiencyLevels() {
		var levelsString = $('#ddoc_proficiencyLevels').val();
		var proficiencyLevels = JSON.parse(levelsString);
		
		var add_to = "";
		for (var i = 0; i < proficiencyLevels.length; ++i) {
			var thisName = proficiencyLevels[i].name;
			var thisFrom = proficiencyLevels[i].from;
			add_to += '<div style="padding-bottom:5px;">';
			add_to += '<input id="ddoc_levelname_' + i + '" class="defaultTextBox2" data-prompt-position="topLeft" style="width:110px !important; margin-bottom:0px; margin-right:10px;" type="text" value="' + thisName + '" />';
			add_to += '<input id="ddoc_levelfrom_' + i + '" class="defaultTextBox2 intSpinner" style="width:20px !important; margin-bottom:0px;" type="text" value="' + thisFrom + '" />';
			add_to += '<button title="Add" class="menu_button add_icon proficiencyLevelAdd" style="margin-left:10px;" data-index=' + i + '>&nbsp;</button>';
			add_to += '<button title="Delete" class="menu_button delete_icon proficiencyLevelDelete" ' + ((i == 0) ? 'disabled ' : '') + ' data-index=' + i + '>&nbsp;</button>';
			add_to += '</div>';
		}
		$('#ddoc_levels_wrapper').html(add_to);

		$('#ddoc_levels_wrapper .intSpinner').spinner({
			step: 5,
			min: 0,
			max: 95
		});
		
		$('#ddoc_levelfrom_0').spinner('disable');
	}

	function checkProficiencyLevels() {
		var isValid = true;

		if ($('#ddoc_proficiencies_wrapper').is(':visible')) {
			var $target = $('#ddoc_levelname_0');

			var i = 0;
			while ($('#ddoc_levelname_' + i).length > 0) {
				if ($('#ddoc_levelname_' + i).val().length == 0) {
					$target.validationEngine('showPrompt', '* These fields are required', 'error', 'topLeft', true);
					isValid = false;
				}
				for (var j = 0; j < i; ++j) {
					if ($('#ddoc_levelname_' + i).val() == $('#ddoc_levelname_' + j).val()) {
						$target.validationEngine('showPrompt', '* Names must be unique', 'error', 'topLeft', true);
						isValid = false;
					}
				}
				i++;
			}
	
			var i = 1;
			if ($('#ddoc_levelfrom_0').val().length == 0) {
				$target.validationEngine('showPrompt', '* These fields are required', 'error', 'topLeft', true);
				isValid = false;
			}
			var lastFrom = parseInt($('#ddoc_levelfrom_0').val());
			if (isNaN($('#ddoc_levelfrom_0').val()) || (lastFrom < 0) || (lastFrom >= 100)) {
				$target.validationEngine('showPrompt', '* Levels must be between 0 and 100', 'error', 'topLeft', true);
				isValid = false;
			}
			while ($('#ddoc_levelfrom_' + i).length > 0) {
				if ($('#ddoc_levelfrom_' + i).val().length == 0) {
					$target.validationEngine('showPrompt', '* These fields are required', 'error', 'topLeft', true);
					isValid = false;
				}
				var thisFrom = parseInt($('#ddoc_levelfrom_' + i).val());
				if (isNaN($('#ddoc_levelfrom_' + i).val()) || (thisFrom < 0) || (thisFrom >= 100)) {
					$target.validationEngine('showPrompt', '* Levels must be between 0 and 100', 'error', 'topLeft', true);
					isValid = false;
				}
				if (thisFrom <= lastFrom) {
					$target.validationEngine('showPrompt', '* Levels must be in increasing order', 'error', 'topLeft', true);
					isValid = false;
				}
				lastFrom = thisFrom;
				i++;
			}
		}

		return isValid;
	}

	// Saving settings

	var settingsSave = function() {
	 	var $dialog = $(this);
		var $form = $dialog.find('form');

		if ($('#ddoc_title_showNameField').prop('checked') && ($('#ddoc_title_nameLetters').val().length == 0)) {
			$('#ddoc_title_nameLetters').validationEngine('showPrompt', '* This field is required', 'error', 'topLeft', true);
		} else if ($('#ddoc_title_showIDField').prop('checked') && ($('#ddoc_title_idDigits').val().length == 0)) {
			$('#ddoc_title_idDigits').validationEngine('showPrompt', '* This field is required', 'error', 'topLeft', true);
		} else if (checkProficiencyLevels() && $form.validationEngine('validate')) {
					
			canCancel = 0;
			showSettings = 0;

			rebuildNameLinesField();
			rebuildProficiencyLevelsField();
			rebuildWeights();
	
			// Update main settings

			documentLayout.Title.text = $('#ddoc_title_text').val().split("\n");

			if ($('#ddoc_title_showNameField').prop('checked')) {
				documentLayout.Title.nameField = {
					label: $('#ddoc_title_nameLabel').val(),
					bubbles: parseInt($('#ddoc_title_nameLetters').val())
				};
			} else delete documentLayout.Title.nameField;

			if ($('#ddoc_title_showIDField').prop('checked')) {
				documentLayout.Title.idField = {
					label: $('#ddoc_title_idLabel').val(),
					bubbles: parseInt($('#ddoc_title_idDigits').val())
				};
			} else delete documentLayout.Title.idField;

			var nameLines = JSON.parse($('#ddoc_title_nameLines').val());
			if (nameLines.length > 0) documentLayout.Title.nameLines = nameLines;
			else delete documentLayout.Title.nameLines;

			// Update layout data

			documentLayout.Page.pageSize = $('#ddoc_page_size').val();

			documentLayout.Page.marginSizes = {
				top: parseFloat($('#ddoc_topMarginSize').val()),
				bottom: parseFloat($('#ddoc_bottomMarginSize').val()),
				left: parseFloat($('#ddoc_leftMarginSize').val()),
				right: parseFloat($('#ddoc_rightMarginSize').val())
			};
			
			documentLayout.Page.copiesPerPage = parseInt($('#ddoc_copiesPerPage').val());

			if ($('#ddoc_show_logo').prop('checked')) {
				if (!('logo' in documentLayout.Title)) {
					documentLayout.Title.logo = userDefaults.Common.Title.logo;
				}
			} else delete documentLayout.Title.logo;

			documentLayout.Settings.version = parseInt($('#ddoc_version').val());
            
			documentLayout.Settings.language = $('#ddoc_language').val();

			// Update reports data
	
			documentLayout.StudentReports = {};
            for (var i = 0; i < reportOptions.length; ++i) {
                var thisName = reportOptions[i].name;
                var thisSuffix = reportOptions[i].suffix;
                documentLayout.StudentReports[thisName] = $('#ddoc' + thisSuffix).val();
            }
    
            if (documentLayout.StudentReports.responses != 'none') {
                documentLayout.StudentReports.incorrectOnly = $('#ddoc_student_incorrectonly').prop('checked');
            } else delete documentLayout.StudentReports.incorrectOnly;

            if (documentLayout.StudentReports.results != 'numerical') {
                documentLayout.StudentReports.outcomesLevels = JSON.parse($('#ddoc_proficiencyLevels').val());
            } else delete documentLayout.StudentReports.outcomesLevels;

			// Update scoring data

			documentScoring.Scoring.type = $('#ddoc_scoring_type').val();
			if (documentScoring.Scoring.type != 'Total') {
				documentScoring.Scoring.weights = JSON.parse($('#ddoc_weights').val());
			} else delete documentScoring.Scoring.weights;

			// Forward to settings view

			var urlVars = getUrlVars();
			delete urlVars.s;
			setUrlVars(urlVars);

			// Save form

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveDocumentData()
			.then(function() {

				// Finish up

				$('#document_title').text($('#ddoc_save_name').val());
				if (!canEdit) $('#document_title').append(' &mdash; <b>READ ONLY</b>');

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Settings saved.",
					image: "/img/success.svg"
				});

			}).catch(function() {
				showSubmitError($dialog);			
			});
		}
	}

	/**************************************************************************************************/
	/* Working with sections                                                                          */
	/**************************************************************************************************/

	var sectionSave = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')) {
			var index;
			for (index = 0; index < documentLayout.Body.length; ++index) {
				if (documentLayout.Body[index].id == activeSectionLayout.id) break;
			}

			if (index < documentLayout.Body.length) {
				for (var i = 0; i < documentScoring.Sections.length; ++i) {
					if (documentScoring.Sections[i].id == activeSectionLayout.id){
						documentScoring.Sections[i] = activeSectionScoring;
					}
				}
				documentLayout.Body[index] = activeSectionLayout;
			} else {
				documentScoring.Sections.push(activeSectionScoring);
				documentLayout.Body.push(activeSectionLayout);
			}

			// Remove any outcomes whose items are no longer in this section

			cleanOutcomes();

			// Forward to this section

			var urlVars = getUrlVars();
			urlVars.s = activeSectionLayout.id;
			setUrlVars(urlVars);

			// Save document

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveDocumentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Section saved.",
					image: "/img/success.svg"
				});

			}).catch(function() {
				showSubmitError($dialog);			
			});
		}
	}

	var drawActiveSection = function() {
		$('#form_sections_menu .menu_item').each(function() {
			if ($(this).attr('data-section-id') != activeSectionLayout.id) $(this).removeClass('active');
			else $(this).addClass('active');
		});

		$('.items_panel').hide();

		$('.right_panel').hide();
		if (activeSectionLayout.type == 'mc') {

			$('.mc_details').show();

			$('#mc_header').val(activeSectionLayout.header);
			$('#mc_columnCount').val(activeSectionLayout.columnCount);
			$('#mc_questionCount').val(activeSectionScoring.Content.length);

			var type, choiceCount;

			if (activeSectionScoring.Content.length == 0) {

				$('#mc_startNumber').val(1);

				type = 'Letter';
				choiceCount = userDefaults.Document.Sections.mc_choiceCount;
				
			} else {

				var startNumber = parseInt(activeSectionScoring.Content[0].number);
				$('#mc_startNumber').val(startNumber);

				type = (activeSectionScoring.Content[0].type == 'mc_truefalse') ? 'TrueFalse' : 'Letter';
				choiceCount = activeSectionScoring.Content[0].Answers.length;
				for (var i = 1; i < activeSectionScoring.Content.length; ++i) {
					var thisType = (activeSectionScoring.Content[i].type == 'mc_truefalse') ? 'TrueFalse' : 'Letter';
					if (thisType != type) type = 'Many';

					var thisChoiceCount = activeSectionScoring.Content[i].Answers.length;
					if (thisChoiceCount != choiceCount) choiceCount = 'Many';
				}
			}

			if (type == 'Many') {
				$('#mc_mc_type').hide();
				$('#mc_mc_type_many').show();
				$('#mc_mc_type_label').addClass("disabled");
			} else {
				$('#mc_mc_type').val(type);
				$('#mc_mc_type').show();
				$('#mc_mc_type_many').hide();
				$('#mc_mc_type_label').removeClass("disabled");
			}

			if (choiceCount == 'Many') {
				$('#mc_mc_choiceCount').hide();
				$('#mc_mc_choiceCount_many').show();
				$('#mc_mc_choiceCount_label').addClass("disabled");
			} else {
				$('#mc_mc_choiceCount').show().val(choiceCount);
				$('#mc_mc_choiceCount_many').hide();
				$('#mc_mc_choiceCount_label').removeClass("disabled");
			}
		
			var answerHTML = "";
			for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
				var simpleKey;
				if (activeSectionScoring.Content[i].ScoredResponses.length == 0) simpleKey = true;
				else if (activeSectionScoring.Content[i].ScoredResponses.length > 1) simpleKey = false;
				else simpleKey = ('id' in activeSectionScoring.Content[i].ScoredResponses[0]);

				var thisAnswer;
				if (simpleKey) {
					thisAnswer = '';
					if (activeSectionScoring.Content[i].ScoredResponses.length == 1) {
						var answerID = activeSectionScoring.Content[i].ScoredResponses[0].id;
						for (var j = 0; j < activeSectionScoring.Content[i].Answers.length; ++j) {
							if (activeSectionScoring.Content[i].Answers[j].id == answerID) {
								thisAnswer += activeSectionScoring.Content[i].Answers[j].response;
							}
						}	
					} else thisAnswer = '_';
				} else thisAnswer = '+';

				answerHTML += thisAnswer;
				if (i % 25 == 24) answerHTML += '\n';
				else if (i % 5 == 4) answerHTML += ' ';
			}
			$('#mc_detail_answers').val(answerHTML);

			$('#mcsection_detail').show();

		} else if (activeSectionLayout.type == 'nr') {

			$('.nr_details').show();

			$('#nr_header').val(activeSectionLayout.header);
			$('#nr_nr_style').val(activeSectionLayout.fieldStyle);
			$('#nr_columnCount').val(activeSectionLayout.columnCount);

			var digitCount = 4;
            if (activeSectionScoring.Content.length > 0) {
                digitCount = activeSectionScoring.Content[0].TypeDetails.nrDigits;
                for (var i = 1; i < activeSectionScoring.Content.length; ++i) {
                    var thisDigitCount = activeSectionScoring.Content[i].TypeDetails.nrDigits;
                    if (thisDigitCount != digitCount) digitCount = 'Many';
                }
            }

			if (digitCount == 'Many') {
				$('#nr_nr_digitCount').hide();
				$('#nr_nr_digitCount_many').show();
			} else {
				$('#nr_nr_digitCount').show().val(digitCount);
				$('#nr_nr_digitCount_many').hide();
			}

			rebuildNRQuestions();

			$('#nrsection_detail').show();
			$('#nr_table_panel').show();

		} else if (activeSectionLayout.type == 'wr') {

			$('.wr_details').show();

			$('#wr_header').val(activeSectionLayout.header);
			$('#wr_wr_includeSpace').prop('checked', activeSectionLayout.includeSpace);
			$('#wr_columnCount').val(activeSectionLayout.columnCount);

			rebuildWRQuestions();

			$('#wrsection_detail').show();
			$('#wr_table_panel').show();

		} else {

			$.gritter.add({
				title: "Error",
				text: "Unsupported section type.",
				image: "/img/error.svg"
			});
			return false;

		}

		// Draw outcomes
							
		rebuildOutcomesList();		
		$('#outcomes_table').show();

		// Draw section weights if required

		if (documentScoring.Scoring.type == 'Sections') {
			var weights = documentScoring.Scoring.weights;
			var thisWeight = 0.0;
			var totalWeight = 0.0;
			for (var i = 0; i < weights.length; ++i) {
				if (weights[i].section_id == activeSectionScoring.id) thisWeight = weights[i].weight;
				totalWeight += weights[i].weight;
			}
			if (totalWeight == 0.0) $('#' + activeSectionLayout.type + '_weight_percent').val('0');
			else {
				var weight_percent = thisWeight * 100 / totalWeight;
				$('#' + activeSectionLayout.type + '_weight_percent').val(weight_percent.toFixed(0));
			}
			$('#' + activeSectionLayout.type + '_weight_wrapper').show();
		} else $('#' + activeSectionLayout.type + '_weight_wrapper').hide();
		
		// Enable and disable interface as required

		if (!canEdit) {
			$('#openAddOutcome').prop('disabled', true);
			$('.add_icon').prop('disabled', true);
		} else if (activeSectionScoring.Content.length == 0) {
			$('#openAddOutcome').prop('disabled', true);
			$('.add_icon').prop('disabled', true);
		} else {
			$('#openAddOutcome').prop('disabled', false);
			$('.add_icon').prop('disabled', false);
		}

		if (isGenerated) {
			$('label.freeze-generated').addClass("disabled");
			$('input.freeze-generated').attr("disabled", "disabled");
			$('select.freeze-generated').attr("disabled", "disabled");
			$('.intSpinner.freeze-generated').spinner('disable');
		}
	}

	$('#form_sections_menu').on('click', '.menu_item', function(e) {
		e.stopPropagation();
		
		if ($(this).parent().is('.formatting_item')) return false;

		var sectionID = $(this).attr('data-section-id');
		selectSection(sectionID);

		drawDetails();
	});

	var selectSection = function(sectionID) {
		$('#form_sections_menu .menu_item').removeClass('active');
		$('#form_sections_menu .menu_item[data-section-id="' + sectionID + '"]').addClass('active');

		var urlVars = getUrlVars();
		urlVars.s = sectionID;
		delete urlVars.c;
		setUrlVars(urlVars);

		activeSectionScoring = false;
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			if (documentScoring.Sections[i].id == sectionID) {
				activeSectionScoring = JSON.parse(JSON.stringify(documentScoring.Sections[i]));
			}
		}

		activeSectionLayout = false;
		for (var i = 0; i < documentLayout.Body.length; ++i) {
			if (documentLayout.Body[i].id == sectionID) {
				activeSectionLayout = JSON.parse(JSON.stringify(documentLayout.Body[i]));
			}
		}
	}

	$('#question_sections_menu').on('click', '.menu_item', function(e) {
		e.stopPropagation();
		
		var sectionIndex = $(this).closest('li').index();
		
		$('#question_sections_menu .menu_item').removeClass('active');
		$('#question_sections_menu .menu_item').eq(sectionIndex).addClass('active');

		$('.items_panel').hide();
		$('.items_panel[data-section-index="' + sectionIndex + '"]').show();

		$('.items_menu .menu_item').removeClass('active');

		var urlVars = getUrlVars();
		delete urlVars.s;
		delete urlVars.c;
		setUrlVars(urlVars);

		$('#assessment_preview_wrapper .part_wrapper').removeClass('question_selected');

		var urlVars = getUrlVars();
		if (urlVars.view == 'statistics') {
			$('#statistics_item').html('<i>No question selected.</i>');
			$('#item_statistics_wrapper').hide();
		}
	});

	$(document).on('click', '.items_menu .menu_item', function(e) {
        e.stopPropagation();

		// Select the item in the assessment preview
		
		var contentID = $(this).attr('data-content-id');
		selectItem(contentID);

		var urlVars = getUrlVars();
		if (urlVars.view == 'questions') {

			// Move to question

			var $itemWrapper = $('#assessment_preview_wrapper .part_wrapper[data-content-id="' + contentID + '"]');

			var itemTop = $itemWrapper.offset().top;
			$("html, body").animate({
				scrollTop: itemTop - 10
			}, 1000);
		} else if (urlVars.view == 'statistics') {
			drawStatistics();
		}
    });   

	$('#assessment_preview_wrapper').on('click', '.part_wrapper', function(e) {
        
        // Select the item in the assessment preview
        
		if ($(e.target).closest('.item_menu_wrapper').length == 0) {
			var contentID = $(this).attr('data-content-id');
			selectItem(contentID);
		}
    });    

	var selectItem = function(contentID) {
		var urlVars = getUrlVars();
		urlVars.c = contentID;
		delete urlVars.s;
		setUrlVars(urlVars);

		// Find and highlight form section

		var sectionID = false;
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				if (documentScoring.Sections[i].Content[j].id == contentID) {
					sectionID = documentScoring.Sections[i].id;
				}	
			}
		}

		$('#form_sections_menu .menu_item').removeClass('active');
		if (sectionID !== false) {
			$('#form_sections_menu .menu_item[data-section-id="' + sectionID + '"]').addClass('active');			
		}

		activeSectionScoring = false;
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			if (documentScoring.Sections[i].id == sectionID) {
				activeSectionScoring = JSON.parse(JSON.stringify(documentScoring.Sections[i]));
			}
		}

		activeSectionLayout = false;
		for (var i = 0; i < documentLayout.Body.length; ++i) {
			if (documentLayout.Body[i].id == sectionID) {
				activeSectionLayout = JSON.parse(JSON.stringify(documentLayout.Body[i]));
			}
		}

		// Find and highlight question section

		var $menuItem = $('#question_navigation_wrapper .menu_item[data-content-id="' + contentID + '"]');
		var $items_panel = $menuItem.closest('.items_panel');
		var sectionIndex = $items_panel.attr('data-section-index');

		$('#question_sections_menu .menu_item').removeClass('active');
		if (sectionIndex != -1) {
			$('#question_sections_menu .menu_item').eq(sectionIndex).addClass('active');			
		}

		// Show item panel

		$('#question_navigation_wrapper .items_panel').hide();
		$items_panel.show();

	   	// Highlight the selected item link and unhighlight others

		$('#question_navigation_wrapper .items_menu .menu_item').removeClass('active');
		$menuItem.addClass('active');

		// Highlight question

		$('#assessment_preview_wrapper .part_wrapper').removeClass('question_selected');
		var $itemWrapper = $('#assessment_preview_wrapper .part_wrapper[data-content-id="' + contentID + '"]');
		$itemWrapper.addClass('question_selected');
		
		ScrollingCommon.fixLeftPanelPosition();
    }

    var onSectionSortStop = function(ev, ui) {
        var $parent = ui.item.parent();

		var sectionID = $draggingSection.find('.menu_item').attr('data-section-id');
		$draggingSection = null;

		var sectionType = false;
		for (var i = 0; i < documentLayout.Body.length; ++i) {
			if (documentLayout.Body[i].id == sectionID) sectionType = documentLayout.Body[i].type;
		}

		var originalLayout = JSON.stringify(documentLayout.Body);

		var sectionOrder = [];
		$parent.find('.menu_item').each(function() {
			sectionOrder.push($(this).attr('data-section-id'));
		});

		var scoringSections = {};
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			scoringSections[documentScoring.Sections[i].id] = documentScoring.Sections[i];
		}
		documentScoring.Sections = [];
		for (var i = 0; i < sectionOrder.length; ++i) {
			if (sectionOrder[i] in scoringSections) documentScoring.Sections.push(scoringSections[sectionOrder[i]]);
		}

		var layoutSections = {};
		for (var i = 0; i < documentLayout.Body.length; ++i) {
			layoutSections[documentLayout.Body[i].id] = documentLayout.Body[i];
		}
		documentLayout.Body = [];
		for (var i = 0; i < sectionOrder.length; ++i) {
			if (sectionOrder[i] in layoutSections) documentLayout.Body.push(layoutSections[sectionOrder[i]]);
		}

		var newLayout = JSON.stringify(documentLayout.Body);

		if (newLayout != originalLayout) {

			// Save assessment

			if (pendingAJAXPost) return false;
			pendingAJAXPost = true;
			
			saveDocumentData()
			.then(function() {

				// Finish up

				$.gritter.add({
					title: "Success",
					text: ((sectionType == 'page_break') ? 'Page break' : 'Section') + " moved.",
					image: "/img/success.svg"
				});

			}).catch(function(error) {
				showSubmitError();			
			});
		}
    }

	var deleteSection = function() {
		var $dialog = $(this);

		var sectionID = $('#deleteSection_dialog').attr('data-section-id');

		var sectionType = false;

		var index = 0;
		while (index < documentLayout.Body.length) {
			if (documentLayout.Body[index].id == sectionID) {
				sectionType = documentLayout.Body[index].type;
				documentLayout.Body.splice(index, 1);
			} else ++index;
		}

		var index = 0;
		while (index < documentScoring.Sections.length) {
			if (documentScoring.Sections[index].id == sectionID) {
				documentScoring.Sections.splice(index, 1);
			} else ++index;
		}

		// Remove outcomes and weights attached to this section

		cleanOutcomes();
		cleanWeights();

		// Forward to document settings

		var urlVars = getUrlVars();
		delete urlVars.s;
		setUrlVars(urlVars);

		// Save document

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;
		
		saveDocumentData()
		.then(function() {

			// Finish up

			$dialog.dialog("close");

			$.gritter.add({
				title: "Success",
				text: ((sectionType == 'page_break') ? 'Page break' : 'Section') + " deleted.",
				image: "/img/success.svg"
			});

		}).catch(function() {
			showSubmitError($dialog);			
		});
	}

	$('#deleteSection_dialog').dialog({
		autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				$(this).dialog("close");
			},
			'Delete': deleteSection
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
			$buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});	

	$('#form_sections_menu').on('click', '.delete_icon', function(e) {
		if (!canEdit) return;
		e.stopPropagation();
		
		var sectionID = $(this).closest('.menu_item').attr('data-section-id');
		$('#deleteSection_dialog').attr('data-section-id', sectionID);
		
		var thisSection = null;
		for (var i = 0; i < documentLayout.Body.length; ++i) {
			if (sectionID == documentLayout.Body[i].id) thisSection = documentLayout.Body[i];
		}

		if (thisSection.type == 'page_break') {
			var contextedDelete = $.proxy(deleteSection, $('#deleteSection_dialog'));
			contextedDelete();
		} else {
			$('#deleteSection_text').html('<p>Permanently delete the section "' + thisSection.header + '" from this form?</p>');
			$('#deleteSection_dialog').dialog('open');	
		}
	});

	$('#addPageBreak').on('click', function(e) {
		documentLayout.Body.push({
			id: getNewID(4),
			type: 'page_break'
		});

		// Save document

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;
		
		saveDocumentData()
		.then(function() {

			// Finish up

			$.gritter.add({
				title: "Success",
				text: "Page break added.",
				image: "/img/success.svg"
			});

		}).catch(function() {
			showSubmitError();			
		});
	});     

	/**************************************************************************************************/
	/* Working with items                                                                             */
	/**************************************************************************************************/

	$(document).on('mouseenter', '#assessment_preview_wrapper .part_wrapper', function(e) {
		$(this).addClass('selected');

		if (this.hasAttribute('data-content-id') && ($('#question_menu_template li').length > 0)) {
			$(this).append($('#question_menu_template').html());
			$(this).find('.item_menu_button').dropdown();
		}
	});
	
	$(document).on('mouseleave', '#assessment_preview_wrapper .part_wrapper', function(e) {
		if ($('.item_menu.open').length == 0)  {
			$(this).removeClass('selected');
			$(this).find('.item_menu_wrapper').remove();
		}
	});

	$(document).on('click', '.item_menu_scoring', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var $wrapper = $(this).closest('.part_wrapper');
		var contentID = $wrapper.attr('data-content-id');

		activeSectionScoring = false;
		QuestionDetails.setActiveItem(false);
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				if (documentScoring.Sections[i].Content[j].id == contentID) {
					activeSectionScoring = JSON.parse(JSON.stringify(documentScoring.Sections[i]));
					QuestionDetails.setActiveItem(JSON.parse(JSON.stringify(documentScoring.Sections[i].Content[j])));
				}
			}
		}

		activeSectionLayout = false;
		for (var i = 0; i < documentLayout.Body.length; ++i) {
			if (documentLayout.Body[i].id == activeSectionScoring.id) {
				activeSectionLayout = JSON.parse(JSON.stringify(documentLayout.Body[i]));
			}
		}

		var style = $wrapper.attr('data-type').substring(0, 2);
		if (style == 'mc') {

			$('#editMCQuestion_dialog').dialog(
				'option', 
				'buttons', {
					'Cancel': function() {
						$(this).dialog("close");
					},
				   'Done': function() {
						if (QuestionDetails.validateMCQuestion()) {
							var contextedSave = $.proxy(saveActiveItem, $('#editMCQuestion_dialog'));
							contextedSave();
						}
					}
				}
			);
	
			startEditMCItem();

		} else if (style == 'nr') {

			startEditNRItem();

		} else if (style == 'wr') {

			startEditWRItem();

		}
	});

	var updateSectionItem = function(itemData) {
			
		var index;
		for (index = 0; index < activeSectionScoring.Content.length; ++index) {
			if (activeSectionScoring.Content[index].id == itemData.id) break;
		}

		if (index < activeSectionScoring.Content.length) {

            if ('Answers' in itemData) {

                // Check if new item data includes new answers, and assign new IDs if it does

                var answerMap = [];
                for (var i = 0; i < itemData.Answers.length; ++i) {
                    var newAnswer = itemData.Answers[i];

                    for (var j = 0; j < activeSectionScoring.Content[index].Answers.length; ++j) {
                        var oldAnswer = activeSectionScoring.Content[index].Answers[j];
                        if ((newAnswer.id == oldAnswer.id) && (newAnswer.response != oldAnswer.response)) {
                            var newAnswerID = getNewID(4);
                            itemData.Answers[i].id = newAnswerID;
                            answerMap.push({
                                oldID: oldAnswer.id,
                                newID: newAnswerID
                            });
                        }
                    }
                }

                // Renumber scored response IDs

                if (answerMap.length > 0) {
                    for (var i = 0; i < itemData.ScoredResponses.length; ++i) {
                        if ('id' in itemData.ScoredResponses[i]) {
                            for (var j = 0; j < answerMap.length; ++j) {
                                if (itemData.ScoredResponses[i].id == answerMap[j].oldID) {
                                    itemData.ScoredResponses[i].id = answerMap[j].newID;
                                }
                            }
                        } else if ('ids' in itemData.ScoredResponses[i]) {
                            for (var j = 0; j < itemData.ScoredResponses[i].ids.length; ++j) {
                                for (var k = 0; k < answerMap.length; ++k) {
                                    if (itemData.ScoredResponses[i].ids[j] == answerMap[k].oldID) {
                                        itemData.ScoredResponses[i].ids[j] = answerMap[k].newID;
                                    }
                                }    
                            }
                        }
                    }
                }
            }

			// Save new item, renumbering if required

			var partsRegex = /^([0-9]+)([a-z]?)$/;

			var match = partsRegex.exec(activeSectionScoring.Content[index].number);
			var oldNumber = parseInt(match[1]);

			activeSectionScoring.Content[index] = itemData;

			var match = partsRegex.exec(itemData.number);
			var newNumber = parseInt(match[1]);
			var newAlpha = match[2];

			// Update items that were part of the same question as the modified item

			var delta = false;
			while (++index < activeSectionScoring.Content.length) {
				var thisNumber = parseInt(activeSectionScoring.Content[index].number);
				if (thisNumber == oldNumber) {
					if (newAlpha == '') newNumber++;
					else newAlpha = String.fromCharCode(newAlpha.charCodeAt(0) + 1);
					activeSectionScoring.Content[index].number = newNumber.toString() + newAlpha;
				} else {
					delta = newNumber - oldNumber;
					break;
				}
			}

			// Update the rest

			while (index < activeSectionScoring.Content.length) {
				var match = partsRegex.exec(activeSectionScoring.Content[index].number);
				var thisNumber = parseInt(match[1]) + delta;
				var thisAlpha = match[2];
				activeSectionScoring.Content[index].number = thisNumber.toString() + thisAlpha;

				++index;
			}

		} else {

			activeSectionScoring.Content.push(itemData);

		}

		return true;
	}

	var saveActiveItem = function() {
		var $dialog = $(this);

		updateSectionItem(QuestionDetails.activeItem);

		var validID = true;
		var sectionIndex;

		for (sectionIndex = 0; sectionIndex < documentScoring.Sections.length; ++sectionIndex) {
			if (documentScoring.Sections[sectionIndex].id == activeSectionScoring.id) break;
		}
		if (sectionIndex < documentScoring.Sections.length) {
			documentScoring.Sections[sectionIndex] = activeSectionScoring;
		} else validID = false;

		if (validID) {
			for (sectionIndex = 0; sectionIndex < documentLayout.Body.length; ++sectionIndex) {
				if (documentLayout.Body[sectionIndex].id == activeSectionLayout.id) break;
			}
			if (sectionIndex < documentLayout.Body.length) {
				if (activeSectionLayout.type == 'nr') updateNRFieldStyle();
				documentLayout.Body[sectionIndex] = activeSectionLayout;
			} else validID = false;	
		}

		if (validID) {

			// Save document

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveDocumentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Question saved.",
					image: "/img/success.svg"
				});

			}).catch(function() {
				showSubmitError($dialog);			
			});
		}
	}
	
	var deleteItem = function(itemID) {
		var $dialog = $(this);
		
		// Remove target item

		var index, itemNumber;
		for (index = 0; index < activeSectionScoring.Content.length; ++index) {
			if (activeSectionScoring.Content[index].id == itemID) break;
		}
        if (index == activeSectionScoring.Content.length) {
            showSubmitError($dialog);
            return;
        }

        itemNumber = activeSectionScoring.Content[index].number;
        activeSectionScoring.Content.splice(index, 1);

		var partsRegex = /^([0-9]+)([a-z]?)$/;

		var match = partsRegex.exec(itemNumber);
		var newNumber = parseInt(match[1]);
		var newAlpha = match[2];

        var delta = -1;
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
            var thisNumber = parseInt(activeSectionScoring.Content[i].number);
            if (thisNumber == newNumber) delta = 0;
		}

		// Update numbering for items that were part of the same question as the modified item

		while (index < activeSectionScoring.Content.length) {
			var thisNumber = parseInt(activeSectionScoring.Content[index].number);
			if (thisNumber == newNumber) {
				activeSectionScoring.Content[index].number = newNumber.toString() + newAlpha;
				newAlpha = String.fromCharCode(newAlpha.charCodeAt(0) + 1);
				++index;
			} else break;
		}

		// Update numbering for the rest

		while (index < activeSectionScoring.Content.length) {
			var match = partsRegex.exec(activeSectionScoring.Content[index].number);
			var thisNumber = parseInt(match[1]) + delta;
			var thisAlpha = match[2];
			activeSectionScoring.Content[index].number = thisNumber.toString() + thisAlpha;
			++index;
		}

		// Update outcomes that included this item

		var outcomeIndex = 0;
		while (outcomeIndex < documentScoring.Outcomes.length) {
			var itemIndex = documentScoring.Outcomes[outcomeIndex].item_ids.indexOf(itemID);
			if (itemIndex >= 0) {
				documentScoring.Outcomes[outcomeIndex].item_ids.splice(itemIndex, 1);
				if (documentScoring.Outcomes[outcomeIndex].item_ids.length == 0) {
					documentScoring.Outcomes.splice(outcomeIndex, 1);
				} else ++outcomeIndex;
			} else ++outcomeIndex;
		}

		// Update active section and save

		var sectionIndex;
		for (sectionIndex = 0; sectionIndex < documentScoring.Sections.length; ++sectionIndex) {
			if (documentScoring.Sections[sectionIndex].id == activeSectionScoring.id) break;
		}

		if (sectionIndex < documentScoring.Sections.length) {
			documentScoring.Sections[sectionIndex] = activeSectionScoring;

			cleanOutcomes();
			cleanWeights();

			// Save document

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveDocumentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Question deleted.",
					image: "/img/success.svg"
				});

			}).catch(function() {
				showSubmitError($dialog);			
			});
		}
	}

	/**************************************************************************************************/
	/* Adding and editing MC sections                                                                 */
	/**************************************************************************************************/

	$('#addMCSection_dialog').dialog({
		autoOpen: false,
		width: 640,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.custom-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			$('#dmc_header').focus();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$('#addMCSection_dialog').find('button:contains("Cancel")').click(function(){
		$('#addMCSection_dialog').dialog("close");
		drawView();
	});
			
	$('#addMCSection_dialog').find('button:contains("Save")').click(function(){
		var contextedSave = $.proxy(sectionSave, $('#addMCSection_dialog'));
		contextedSave();
	});	

	$(document).on('click', '.openAddMCsection', async function(e){
		e.preventDefault();
		if (!canEdit || isGenerated) return;

		var sectionID = getNewID(4);

		activeSectionScoring = {
			id: sectionID,
			Content: []
		};

		activeSectionLayout = {
			id: sectionID,
			type: 'mc',
			header: await localizationInstance.getString('defaults_mc_header'),
			columnCount: userDefaults.Document.Sections.columnCount,
		};

		bufferedItems = [];

		rebuildMCEditor();

        $('#addMCSection_dialog').dialog('open');
		$('#dmc_questionCount').focus();
    });

	$('#startEditMc').click(function(e) {
		e.preventDefault();
		if (!canEdit) return;
		
		bufferedItems = [];

		rebuildMCEditor();

		$('#addMCSection_dialog').dialog('open');
	});

	function rebuildMCEditor() {
		$('#dmc_header').val(activeSectionLayout.header);
		$('#dmc_columnCount').val(activeSectionLayout.columnCount);
		if (activeSectionScoring.Content.length > 0) {
			$('#dmc_questionCount').val(activeSectionScoring.Content.length);
		} else $('#dmc_questionCount').val('');

		var type, choiceCount;

		if (activeSectionScoring.Content.length == 0) {

			$('#dmc_startNumber').val(1);

			type = 'Letter';
			choiceCount = userDefaults.Document.Sections.mc_choiceCount;
			
		} else {

			var startNumber = parseInt(activeSectionScoring.Content[0].number);
			$('#dmc_startNumber').val(startNumber);

			type = (activeSectionScoring.Content[0].type == 'mc_truefalse') ? 'TrueFalse' : 'Letter';
			choiceCount = activeSectionScoring.Content[0].Answers.length;
			for (var i = 1; i < activeSectionScoring.Content.length; ++i) {
				var thisType = (activeSectionScoring.Content[i].type == 'mc_truefalse') ? 'TrueFalse' : 'Letter';
				if (thisType != type) type = 'Many';

				var thisChoiceCount = activeSectionScoring.Content[i].Answers.length;
				if (thisChoiceCount != choiceCount) choiceCount = 'Many';
			}
		}

		$('#dmc_mc_type').val(type);

		if (type == 'Many') {
			$('#dmc_mc_type').hide();
			$('#dmc_mc_type_many').show();
			$('#dmc_mc_type_label').addClass("disabled");
		} else {
			$('#dmc_mc_type').show();
			$('#dmc_mc_type_many').hide();
			$('#dmc_mc_type_label').removeClass("disabled");
		}

		if (choiceCount == 'Many') {
			$('#dmc_mc_choiceCount').parent().hide();
			$('#dmc_mc_choiceCount_many').show();
			$('#dmc_mc_choiceCount_label').addClass("disabled");
		} else {
			$('#dmc_mc_choiceCount').val(choiceCount);
			$('#dmc_mc_choiceCount').parent().show();
			$('#dmc_mc_choiceCount_many').hide();

			if (type == 'TrueFalse') {
				$('#dmc_mc_choiceCount').spinner('disable');
				$('#dmc_mc_choiceCount_label').addClass("disabled");
			} else {
				$('#dmc_mc_choiceCount').spinner('enable');
				$('#dmc_mc_choiceCount_label').removeClass("disabled");
			}
		}

		if ((type == 'Many') || (choiceCount == 'Many')) {

			$('#dmc_options_note').html(
				'<img src="/img/icons/lock.png" /> \
				 <span class="input_subtext">Some options are locked because the section contains different response types.</span>'
			);

		} else {

			$('#dmc_options_note').html(
				'<span class="input_subtext" style="margin-left:14px;">To customize these for a single question, click on the question number below.</span>'
			);
		}

		rebuildMCKey();
	}

	$('#mcKeyClearAll').click(function() {
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			activeSectionScoring.Content[i].ScoredResponses = [];
		}
		rebuildMCKey();
    });

	$('#dmc_header').on('input', function() {
		activeSectionLayout.header = $('#dmc_header').val();
	});
	
	$('#dmc_columnCount').on('change', function() {
		if ($('#dmc_columnCount').val() == 'auto') activeSectionLayout.columnCount = 'auto';
		else activeSectionLayout.columnCount = parseInt($('#dmc_columnCount').val());
	});

	function apply_mc_questionCount() {
		var numQuestions = parseInt($('#dmc_questionCount').val());
		if (isNaN(numQuestions)) numQuestions = 0;
		else if (numQuestions > 500) numQuestions = 50;

		var fillAnswers = false;
		var fillType;

		// Add questions if necessary

		while (activeSectionScoring.Content.length < numQuestions) {

			var newItem;
			if (bufferedItems.length > 0) {

				newItem = bufferedItems.pop();

			} else {

				if (fillAnswers == false) {
					if (activeSectionScoring.Content.length > 0) {
	
						var answerCounts = {};
						for (var j = 0; j < activeSectionScoring.Content.length; ++j) {
							var thisAnswers = '';
							for (var k = 0; k < activeSectionScoring.Content[j].Answers.length; ++k) {
								thisAnswers += activeSectionScoring.Content[j].Answers[k].response;
							}
							if (!(thisAnswers in answerCounts)) answerCounts[thisAnswers] = 0;
							answerCounts[thisAnswers]++;
						}
	
						var bestAnswers = '';
						var bestCount = 0;
						for (var thisAnswers in answerCounts) {
							if (answerCounts[thisAnswers] > bestCount) {
								bestAnswers = thisAnswers;
								bestCount = answerCounts[thisAnswers];
							}
						}	
	
					} else if ($('#dmc_mc_type').val() == 'TrueFalse') {
						
						bestAnswers = 'tf';
	
					} else {
	
						var choiceCount = parseInt($('#dmc_mc_choiceCount').val());
						bestAnswers = '';
						for (var j = 0; j < choiceCount; ++j) {
							bestAnswers += String.fromCharCode(97 + j);
						}
					}
	
					fillAnswers = [];
					for (var j = 0; j < bestAnswers.length; ++j) {
						fillAnswers.push({
							id: false,
							response: bestAnswers[j]
						});
					}
	
					if (bestAnswers == 'tf') fillType = 'mc_truefalse';
					else fillType = 'mc';
				}
	
				var thisAnswers = JSON.parse(JSON.stringify(fillAnswers));
				for (var j = 0; j < thisAnswers.length; ++j) {
					thisAnswers[j].id = getNewID(4);
				}
	
				var thisNumber;
				if (activeSectionScoring.Content.length > 0) {
					var lastIndex = activeSectionScoring.Content.length - 1;
					thisNumber = parseInt(activeSectionScoring.Content[lastIndex].number) + 1;
				} else thisNumber = parseInt($('#dmc_startNumber').val());
	
				newItem = {
					id: getNewID(4),
					type: fillType,
					number: thisNumber.toString(),
					Answers: thisAnswers,
					ScoredResponses: [],
					scoring: 'omit'
				};				
			}

			activeSectionScoring.Content.push(newItem);				
		}

		// Remove questions if necessary

		while (activeSectionScoring.Content.length > numQuestions) {
			bufferedItems.push(activeSectionScoring.Content.pop());
		}

		// Redraw key

		rebuildMCKey();
	}

	$('#dmc_questionCount').on('keyup', apply_mc_questionCount);
    $('#dmc_questionCount').spinner({ stop: apply_mc_questionCount });

	function apply_mc_startNumber() {
		if (activeSectionScoring.Content.length == 0) return;
		if ($('#dmc_startNumber').val().length == 0) return;

		bufferedItems = [];

		var new_startNumber = parseInt($('#dmc_startNumber').val());
		var old_startNumber = parseInt(activeSectionScoring.Content[0].number);
		var difference = new_startNumber - old_startNumber;

		if (isNaN(new_startNumber)) return;

		var partsRegex = /^([0-9]+)([a-z]?)$/;
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			var match = partsRegex.exec(activeSectionScoring.Content[i].number);
			activeSectionScoring.Content[i].number = (parseInt(match[1]) + difference).toString() + match[2];
		}

		rebuildMCKey();
	}

    $('#dmc_startNumber').on('keyup', apply_mc_startNumber);
    $('#dmc_startNumber').spinner({ stop: apply_mc_startNumber });

	function apply_mc_choiceCount() {
		bufferedItems = [];

		var numChoices = parseInt($('#dmc_mc_choiceCount').val());

		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			while (activeSectionScoring.Content[i].Answers.length < numChoices) {
				var numAnswers = activeSectionScoring.Content[i].Answers.length;
				activeSectionScoring.Content[i].Answers.push({
					id: getNewID(4),
					response: String.fromCharCode(97 + numAnswers)
				});
			}

			while (activeSectionScoring.Content[i].Answers.length > numChoices) {
				var removedAnswer = activeSectionScoring.Content[i].Answers.pop();
				var scoredResponses = activeSectionScoring.Content[i].ScoredResponses;

				var responseIndex = 0;
				while (responseIndex < scoredResponses.length) {
					var thisResponse = scoredResponses[responseIndex];

					var isGood = true;
					if ('ids' in thisResponse) {
						for (var j = 0; j < thisResponse.ids.length; ++j) {
							if (thisResponse.ids[j] == removedAnswer.id) isGood = false;
						}
					} else if (thisResponse.id == removedAnswer.id) isGood = false;

					if (!isGood) scoredResponses = scoredResponses.splice(responseIndex, 1);
					else responseIndex++;
				}

				activeSectionScoring.Content[i].ScoredResponses = scoredResponses;
			}
		}

		rebuildMCKey();
    }

    $('#dmc_mc_choiceCount').on('keyup', apply_mc_choiceCount);
    $('#dmc_mc_choiceCount').spinner({ stop: apply_mc_choiceCount });

	function apply_mc_type() {
		bufferedItems = [];

		var new_type, fillLetters;
		if ($('#dmc_mc_type').val() == 'Letter') {

			new_type = 'mc';

			fillLetters = '';
			var numChoices = userDefaults.Document.Sections.mc_choiceCount;
			for (var i = 0; i < numChoices; ++i) fillLetters += String.fromCharCode(97 + i);

		} else {

			new_type = 'mc_truefalse';
			fillLetters = 'tf';

		}
		
		var fillAnswers = [];
		for (var j = 0; j < fillLetters.length; ++j) {
			fillAnswers.push({
				id: false,
				response: fillLetters[j]
			});
		}

		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			var thisAnswers = JSON.parse(JSON.stringify(fillAnswers));
			for (var j = 0; j < thisAnswers.length; ++j) {
				thisAnswers[j].id = getNewID(4);
			}

			activeSectionScoring.Content[i].type = new_type;
			activeSectionScoring.Content[i].Answers = thisAnswers;
			activeSectionScoring.Content[i].ScoredResponses = [];
		}

		rebuildMCEditor();
		$('#dmc_mc_type').blur();
	}

	$('#dmc_mc_type').on('change', apply_mc_type);

	function rebuildMCKey() {
		var numQuestions = activeSectionScoring.Content.length;

		var keyHTML = '';
		for (var i = 0; i < numQuestions; i++) {
			keyHTML += 
				"<div class='mc_answer_wrapper' id='dmc_wrapper_" + i + "'> \
					<span class='mcQuestionIndex mcQuestionLink' data-index=" + i + ">" + activeSectionScoring.Content[i].number + ".</span> \
					<input type='input' class='mc_answers' id='dmc_answers_" + i + "' maxlength='1' /> \
					<input type='input' class='mcQuestionLink' id='dmc_editimage_" + i + "' maxlength='1' data-index=" + i + " onfocus='this.blur()' /> \
				</div>";
		}
		$('#mc_answer_key').html(keyHTML);

		for (var i = 0; i < numQuestions; i++) {
			var omittedWithKey = true;
			if (!('scoring' in activeSectionScoring.Content[i])) omittedWithKey = false;
			else if (activeSectionScoring.Content[i].scoring != 'omit') omittedWithKey = false;
			else if (activeSectionScoring.Content[i].ScoredResponses.length == 0) omittedWithKey = false;

			if (omittedWithKey) {

				$('#dmc_answers_' + i).hide();
				$('#dmc_editimage_' + i).val(" ");
				$('#dmc_editimage_' + i).show();

			} else {

				var simpleKey;
				if (activeSectionScoring.Content[i].ScoredResponses.length == 0) simpleKey = true;
				else if (activeSectionScoring.Content[i].ScoredResponses.length > 1) simpleKey = false;
				else simpleKey = ('id' in activeSectionScoring.Content[i].ScoredResponses[0]);

				if (simpleKey) {
					var answerString = '';
					if (activeSectionScoring.Content[i].ScoredResponses.length == 1) {
						var answerID = activeSectionScoring.Content[i].ScoredResponses[0].id;
						for (var j = 0; j < activeSectionScoring.Content[i].Answers.length; ++j) {
							if (activeSectionScoring.Content[i].Answers[j].id == answerID) {
								answerString += activeSectionScoring.Content[i].Answers[j].response;
							}
						}	
					}

					$('#dmc_answers_' + i).val(answerString);
					$('#dmc_answers_' + i).show();
					$('#dmc_editimage_' + i).hide();
				} else {
					$('#dmc_answers_' + i).hide();
					$('#dmc_editimage_' + i).val("+");
					$('#dmc_editimage_' + i).show();
				}

			}
		}
    }

	$(document).on('keyup', '.mc_answers', function(e) {
		var index = parseInt($(this).siblings('.mcQuestionIndex').attr('data-index'));
		if (e.keyCode > 32) {
			
			var newResponse = String.fromCharCode(e.keyCode).toLowerCase();

			var answerID = false;
			for (var i = 0; i < activeSectionScoring.Content[index].Answers.length; ++i) {
				if (newResponse == activeSectionScoring.Content[index].Answers[i].response) {
					answerID = activeSectionScoring.Content[index].Answers[i].id;
				}
			}
			
			if (answerID === false) {
	
				$(this).val("");
				activeSectionScoring.Content[index].ScoredResponses = [];
				activeSectionScoring.Content[index].scoring = 'omit';

				var messageMiddle = '';
				if (activeSectionScoring.Content[index].type == 'mc_truefalse') {
					messageMiddle = "Please enter a 't' or 'f'";
				} else {
					var numAnswers = activeSectionScoring.Content[index].Answers.length;
					var lastAllowed = activeSectionScoring.Content[index].Answers[numAnswers - 1].response;
					messageMiddle = "Please enter a response from 'a' to '" + lastAllowed + "'";
				}
	
				if ((e.keyCode >= 48) && (e.keyCode <= 90)) {
					alert("'" + newResponse + "' is not a valid response for this question. " + messageMiddle + 
						", or change the settings above.");
				} else {
					alert("Invalid key pressed. " + messageMiddle + ".");
				}
	
			} else {
				
				$(this).val(newResponse);

				delete activeSectionScoring.Content[index].scoring;
				activeSectionScoring.Content[index].ScoredResponses = [{
					id: answerID,
					value: 1.0
				}];
	
				var $next = $(this);
				do {
					var $nextWrapper = $next.closest('.mc_answer_wrapper').next('.mc_answer_wrapper');
					if ($nextWrapper.length > 0) $next = $nextWrapper.find('.mc_answers');
					else break;
				} while (!$next.is(":visible"));
				$next.focus();
	
			}
		} else {

			$(this).val("");
			activeSectionScoring.Content[index].ScoredResponses = [];

		}
	});
	
	/**************************************************************************************************/
	/* Editing a single MC question                                                                   */
	/**************************************************************************************************/

	var questionDetailSave = function() {
		var $dialog = $(this);

		// Save edited question

		if (updateSectionItem(QuestionDetails.activeItem)) {

			// Finish up

			rebuildMCEditor();

			$.gritter.add({
				title: "Success",
				text: "Question updated.",
				image: "/img/success.svg"
			});
			
		} else {

			$.gritter.add({
				title: "Error",
				text: "Unable to update question.",
				image: "/img/error.svg"
			});

		}

		$dialog.dialog("close");
	}

	$('#editMCQuestion_dialog').dialog({
        autoOpen: false,width: 450,modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			$buttonPane.find('button:contains("Done")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
		}, close: function() {
        }
    });

	$(document).on('click', '.mcQuestionLink', function() {
		bufferedItems = [];

		$('#editMCQuestion_dialog').dialog(
			'option', 
			'buttons', {
				'Cancel': function() {
					$(this).dialog("close");
				},
			   'Done': function() {
					if (QuestionDetails.validateMCQuestion()) {
				 		var contextedSave = $.proxy(questionDetailSave, $('#editMCQuestion_dialog'));
				 		contextedSave();
			 		}
		 		}
			}
		);
	 
		var index = parseInt($(this).attr('data-index'));
		QuestionDetails.setActiveItem(JSON.parse(JSON.stringify(activeSectionScoring.Content[index])));

		startEditMCItem();
    });

	var startEditMCItem = function() {

		// Get numbering options

		var index = false;
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			if (activeSectionScoring.Content[i].id == QuestionDetails.activeItem.id) index = i;
		}

		var numberingOptions = [];
		var partsRegex = /^([0-9]+)([a-z]?)$/;
		var nextNumber;
		if (index == 0) {

			var match = partsRegex.exec(QuestionDetails.activeItem.number);
			nextNumber = match[1];

		} else {

			var lastQuestion = activeSectionScoring.Content[index - 1];
			var match = partsRegex.exec(lastQuestion.number);
			nextNumber = parseInt(match[1]) + 1;

			if (match[2] != '') {
				var nextLetter = String.fromCharCode(match[2].charCodeAt(0) + 1);
				numberingOptions.push(match[1] + nextLetter);
			}	
		}
		numberingOptions.push(nextNumber);
		numberingOptions.push(nextNumber + 'a');

		$('#dmcq_numberingType').empty();
		for (var i = 0; i < numberingOptions.length; ++i) {
			var value = numberingOptions[i];
			$('#dmcq_numberingType').append('<option value="' + value + '">' + value + '</option>')
		}
		$('#dmcq_numberingType').val(QuestionDetails.activeItem.number);

		// Show remaining details

		QuestionDetails.rebuildMCDetails();

        $('#editMCQuestion_dialog').dialog('open');

		document.activeElement.blur();		
	}

	/**************************************************************************************************/
	/* Adding and editing NR sections                                                                 */
	/**************************************************************************************************/

	$('#addNRSection_dialog').dialog({
		autoOpen: false,
		width:670,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.custom-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			$buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$('#addNRSection_dialog').find('button:contains("Cancel")').click(function(){
		$('#addNRSection_dialog').dialog("close");
		drawView();
	});
			
	$('#addNRSection_dialog').find('button:contains("Save")').click(function(){
		var contextedSave = $.proxy(sectionSave, $('#addNRSection_dialog'));
		contextedSave();
	});

	$(document).on('click', '.openAddNRsection', async function(e){
		e.preventDefault();
		if (!canEdit || isGenerated) return;

		var sectionID = getNewID(4);

		activeSectionScoring = {
			id: sectionID,
			Content: []
		};

		activeSectionLayout = {
			id: sectionID,
			type: 'nr',
			header: await localizationInstance.getString('defaults_nr_header'),
			columnCount: userDefaults.Document.Sections.columnCount,
			fieldStyle: 'Default'
		};

		bufferedItems = [];

		$('#dnr_header').val(activeSectionLayout.header);
		$('#dnr_nr_style').val(activeSectionLayout.fieldStyle);
		$('#dnr_columnCount').val(activeSectionLayout.columnCount);

		if (activeSectionScoring.Content.length == 0) {

			$('#dnr_startNumber').val(1);
			$('#dnr_nr_digitCount').val(userDefaults.Question.TypeDetails.nr.nrDigits);
			$('#dnr_questionCount').val('');

		} else {

			var startNumber = parseInt(activeSectionScoring.Content[0].number);
			$('#dnr_startNumber').val(startNumber);
			$('#dnr_nr_digitCount').val(activeSectionScoring.Content[0].TypeDetails.nrDigits);
			$('#dnr_questionCount').val(activeSectionScoring.Content.length);

		}

		rebuildNRCreateQuestions();

        $('#addNRSection_dialog').dialog('open');
		$('#dnr_questionCount').focus();
    });

	$('#dnr_header').on('input', function() {
		activeSectionLayout.header = $('#dnr_header').val();
	});

	$('#dnr_nr_style').on('change', function() {
		activeSectionLayout.fieldStyle = $('#dnr_nr_style').val();
	});

	$('#dnr_columnCount').on('change', function() {
		if ($('#dnr_columnCount').val() == 'auto') activeSectionLayout.columnCount = 'auto';
		else activeSectionLayout.columnCount = parseInt($('#dnr_columnCount').val());
	});

	$('#startEditNr').click(function(e) {
		e.preventDefault();
		if (!canEdit) return;

		$('#enr_header').val(activeSectionLayout.header);
		$('#enr_nr_style').val(activeSectionLayout.fieldStyle);
		$('#enr_columnCount').val(activeSectionLayout.columnCount);

		if (activeSectionScoring.Content.length == 0) {

			$('#enr_startNumber').val(1);
			$('#enr_questionCount').val('');
			$('#enr_questionCount').spinner('option', 'min', 0);
			$('#enr_nr_digitCount').val(userDefaults.Question.TypeDetails.nr.nrDigits);

		} else {

			var startNumber = parseInt(activeSectionScoring.Content[0].number);
			$('#enr_startNumber').val(startNumber);
			$('#enr_questionCount').val(activeSectionScoring.Content.length);
			$('#enr_questionCount').spinner('option', 'min', activeSectionScoring.Content.length);

			var digitCount = activeSectionScoring.Content[0].TypeDetails.nrDigits;
			for (var i = 1; i < activeSectionScoring.Content.length; ++i) {
				var thisDigitCount = activeSectionScoring.Content[i].TypeDetails.nrDigits;
				if (thisDigitCount != digitCount) digitCount = 'Many';
			}

			if (digitCount == 'Many') {
				$('#enr_nr_digitCount').parent().hide();
				$('#enr_nr_digitCount_many').show();
			} else {
				$('#enr_nr_digitCount').val(digitCount);
				$('#enr_nr_digitCount').parent().show();
				$('#enr_nr_digitCount_many').hide();
			}
			
		}

		$('#editNRSection_dialog').dialog('open');
		if (activeSectionScoring.Content.length == 0) $('#enr_questionCount').focus();
		else $('#enr_header').focus();
	});

	$('#enr_header').on('input', function() {
		activeSectionLayout.header = $('#enr_header').val();
	});
	
	$('#enr_nr_style').on('change', function() {
		activeSectionLayout.fieldStyle = $('#enr_nr_style').val();
	});
	
	$('#enr_columnCount').on('change', function() {
		if ($('#enr_columnCount').val() == 'auto') activeSectionLayout.columnCount = 'auto';
		else activeSectionLayout.columnCount = parseInt($('#enr_columnCount').val());
	});

	function rebuildNRCreateQuestions() {
		var numQuestions = activeSectionScoring.Content.length;
		var startNumber = parseInt($('#dnr_startNumber').val());
	    if (isNaN(startNumber)) startNumber = 1;

		var questionHTML = 
			"<div class='nr_question_row'> \
				<div class='labeled-input'> \
					<label class='nr_number' style='width:15px;text-align:left;'></label> \
					<select class='nr_type defaultSelectBox pad5px' style='height: 28px; width: 90px; padding-left:5px;'> \
						<option value='nr'>No type</option> \
						<option value='nr_decimal'>Decimal</option> \
						<option value='nr_scientific'>Scientific</option> \
						<option value='nr_fraction'>Fraction</option> \
						<option value='nr_selection'>Selection</option> \
					</select> \
				</div> \
				<div class='labeled-input dnr_answer_input'> \
					<label style='width:60px;'>Answer:</label> \
					<input class='nr_answer' data-prompt-position='centerLeft' type='text' style='width:40px;' /> \
				</div> \
			</div>";

		$("#dnr_question_list").empty();
		for (var i = 0; i < numQuestions; ++i) {
			var $questionRow = $(questionHTML).appendTo($("#dnr_question_list"));
			$questionRow.find('.nr_number').text((startNumber + i) + '.');
			$questionRow.find('.nr_type').val(activeSectionScoring.Content[i].type);
			if (activeSectionScoring.Content[i].Answers.length == 1) {
				var response = activeSectionScoring.Content[i].Answers[0].response;
				$questionRow.find('.nr_answer').val(response);
			}
		}

		apply_dnr_validations();
	}

	function apply_nr_questionCount(prefix) {
		var numQuestions = parseInt($(prefix + '_questionCount').val());
		if (isNaN(numQuestions)) numQuestions = 0;
		else if (numQuestions > 50) numQuestions = 50;

		// Add questions if necessary

		while (activeSectionScoring.Content.length < numQuestions) {

			var newItem;
			if (bufferedItems.length > 0) {

				newItem = bufferedItems.pop();

			} else {

				var thisNumber;
				if (activeSectionScoring.Content.length > 0) {
					var lastIndex = activeSectionScoring.Content.length - 1;
					thisNumber = parseInt(activeSectionScoring.Content[lastIndex].number) + 1;
				} else thisNumber = parseInt($(prefix + '_startNumber').val());
	
				newItem = {
					id: getNewID(4),
					type: 'nr',
					number: thisNumber.toString(),
					Answers: [],
					ScoredResponses: [],
					scoring: 'omit'
				};
				var digitCount = parseInt($(prefix + '_nr_digitCount').val());
				QuestionDetails.init_nr_typeDetails(newItem, digitCount);
			}

			activeSectionScoring.Content.push(newItem);				
		}

		// Remove questions if necessary

		while (activeSectionScoring.Content.length > numQuestions) {
			bufferedItems.push(activeSectionScoring.Content.pop());
		}

		// Redraw question list

		rebuildNRCreateQuestions();
	}

	$('#dnr_questionCount').on('keyup', function() {
		apply_nr_questionCount('#dnr');
	});
    $('#dnr_questionCount').spinner({ 
		min: 0,
		stop: function() {
			apply_nr_questionCount('#dnr');	
		}
	});

	$('#enr_questionCount').on('keyup', function() {
		apply_nr_questionCount('#enr');
	});
    $('#enr_questionCount').spinner({ 
		min: 0,
		stop: function() {
			apply_nr_questionCount('#enr');	
		}
	});

	function apply_nr_startNumber(startNumber) {
		if (activeSectionScoring.Content.length == 0) return;
		if (isNaN(startNumber)) return;

		bufferedItems = [];

		var delta = startNumber - parseInt(activeSectionScoring.Content[0].number);
		var partsRegex = /^([0-9]+)([a-z]?)$/;

		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			var match = partsRegex.exec(activeSectionScoring.Content[i].number);
			var newNumber = parseInt(match[1]) + delta;
			var newAlpha = match[2];
	
			activeSectionScoring.Content[i].number = newNumber.toString() + newAlpha;
		}

		rebuildNRCreateQuestions();
	}

    $('#dnr_startNumber').on('keyup', function() {
		var startNumber = parseInt($('#dnr_startNumber').val());
		apply_nr_startNumber(startNumber);
	});
    $('#dnr_startNumber').spinner({ 
		stop: function() {
			var startNumber = parseInt($('#dnr_startNumber').val());
			apply_nr_startNumber(startNumber);
		}
	});

    $('#enr_startNumber').on('keyup', function() {
		var startNumber = parseInt($('#enr_startNumber').val());
		apply_nr_startNumber(startNumber);
	});
    $('#enr_startNumber').spinner({ 
		stop: function() {
			var startNumber = parseInt($('#enr_startNumber').val());
			apply_nr_startNumber(startNumber);
		}
	});

	function apply_dnr_validations() {
		var numBlanks = parseInt($('#dnr_nr_digitCount').val());

		$('#dnr_question_list .nr_question_row').each(function() {
			var type = $(this).find('.nr_type').val();
			var answerInput = $(this).find('.nr_answer')[0];

			answerInput.className = 'nr_answer defaultTextBox2';
			if (type == 'nr_decimal') {
				answerInput.className += ' validate[custom[isDecimal],maxSize[' + numBlanks + ']]';
			} else if (type == 'nr_scientific') {
				answerInput.className += ' validate[custom[isScientific]]';
			} else if (type == 'nr_selection')  {
				answerInput.className += ' validate[custom[isSelection],maxSize[' + numBlanks + ']';
			} else if (type == 'nr_fraction') {
				answerInput.className += ' validate[custom[isFraction],maxSize[' + numBlanks + ']';
			} else {
				answerInput.setAttribute('disabled', 'disabled');
			}
		});
	}

	function apply_nr_digitCount(numDigits) {
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			activeSectionScoring.Content[i].TypeDetails.nrDigits = numDigits;
		}
	}

    $('#dnr_nr_digitCount').on('keyup', function() {
		var numDigits = parseInt($('#dnr_nr_digitCount').val());
		apply_nr_digitCount(numDigits);
		apply_dnr_validations();
	});
    $('#dnr_nr_digitCount').spinner({ 
		stop: function() {
			var numDigits = parseInt($('#dnr_nr_digitCount').val());
			apply_nr_digitCount(numDigits);
			apply_dnr_validations();
		}
	});

    $('#enr_nr_digitCount').on('keyup', function() {
		var numDigits = parseInt($('#enr_nr_digitCount').val());
		apply_nr_digitCount(numDigits);
	});
    $('#enr_nr_digitCount').spinner({ 
		stop: function() {
			var numDigits = parseInt($('#enr_nr_digitCount').val());
			apply_nr_digitCount(numDigits);
		}
	});

	function updateNRFieldStyle() {
		if (activeSectionLayout.type != 'nr') return;
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			if (activeSectionScoring.Content[i].type == 'nr_fraction') {
				activeSectionLayout.fieldStyle = 'Fractions';
			} else if (activeSectionLayout.fieldStyle !== 'Fractions') {
				var needsNegative = false;
				for (var j = 0; j < activeSectionScoring.Content[i].Answers.length; ++j) {
					needsNegative |= (activeSectionScoring.Content[i].Answers[j].response[0] == '-');
				}
				if (needsNegative) activeSectionLayout.fieldStyle = 'Negatives';
			}
		}
	}

	$('#dnr_question_list').on('change', '.nr_type', function() {
		var index = $(this).closest('.nr_question_row').index();

		var newType = $(this).val();
		activeSectionScoring.Content[index].type = newType;
		var digitCount = parseInt($('#dnr_nr_digitCount').val());
		QuestionDetails.init_nr_typeDetails(activeSectionScoring.Content[index], digitCount);
		
        activeSectionScoring.Content[index].Answers = [];
        activeSectionScoring.Content[index].ScoredResponses = [];	

		rebuildNRCreateQuestions();

		activeSectionLayout.fieldStyle = 'Default';
		updateNRFieldStyle();

		$('#dnr_nr_style').val(activeSectionLayout.fieldStyle);

		if (newType == 'nr') {
			document.activeElement.blur();
		} else {
			var $row = $('#dnr_question_list .nr_question_row').eq(index);
			$row.find('.nr_answer').focus();
		}
	});

	$('#dnr_question_list').on('input', '.nr_answer', function() {
		var index = $(this).closest('.nr_question_row').index();

		var newAnswer = $(this).val();
		if (newAnswer.length == 0) {

			activeSectionScoring.Content[index].scoring = 'omit';
			activeSectionScoring.Content[index].Answers = [];
			activeSectionScoring.Content[index].ScoredResponses = [];

		} else {

			if (activeSectionScoring.Content[index].Answers.length != 1) {
				var answerID = getNewID(4);
				activeSectionScoring.Content[index].Answers = [{
					id: answerID
				}];
				activeSectionScoring.Content[index].ScoredResponses = [{
					id: answerID,
					value: 1.0
				}];
			}

			activeSectionScoring.Content[index].Answers[0].response = newAnswer;
			delete activeSectionScoring.Content[index].scoring;

		}

		activeSectionLayout.fieldStyle = 'Default';
		updateNRFieldStyle();

		$('#dnr_nr_style').val(activeSectionLayout.fieldStyle);
	});

	$('#editNRSection_dialog').dialog({
		autoOpen: false,
		width: 440,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.custom-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			$buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$('#editNRSection_dialog').find('button:contains("Cancel")').click(function(){
		$('#editNRSection_dialog').dialog("close");
	});
			
	$('#editNRSection_dialog').find('button:contains("Save")').click(function(){
		var contextedSave = $.proxy(sectionSave, $('#editNRSection_dialog'));
		contextedSave();
	});

	/**************************************************************************************************/
	/* Viewing, adding, editing, and deleting single NR questions                                     */
	/**************************************************************************************************/

	$('#editNRQuestion_dialog').dialog({
        autoOpen: false,width: 375,modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$(this).dialog('close');
			},
			"Save": function() {
				if (QuestionDetails.validateNRQuestion() && $('#editNRQuestion_dialog form').validationEngine('validate')) {
					var contextedSave = $.proxy(saveActiveItem, $('#editNRQuestion_dialog'));
					contextedSave();
				}
			}
        }, open: function(event) {
		 	var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
        	QuestionDetails.rebuildNRValidations();
		 	$('#dnq_answer_0').focus();
		 	initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

	var rebuildNRQuestions = function() {
		$('#nr_questionlist').empty();
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			var itemData = activeSectionScoring.Content[i];

			var itemOptions = '';
			if ('scoring' in itemData) {
				if (itemData.scoring == 'bonus') itemOptions = "Bonus";
			}
		
			if (itemData.type == 'nr_selection') {
				if (itemData.TypeDetails.ignoreOrder) {
					if (itemOptions.length > 0) itemOptions += "; ";
					itemOptions += "Unordered";	
				}
				var partialValue = itemData.TypeDetails.partialValue;
				if (partialValue > 0) {
					if (itemOptions.length > 0) itemOptions += "; ";
					itemOptions += itemData.TypeDetails.partialValue + " for partial matches";	
				}
				if (itemData.TypeDetails.markByDigits) {
					if (itemOptions.length > 0) itemOptions += "; ";
					itemOptions += "Marks are per correct digit";	
				}
			} else if ((itemData.type == 'nr_decimal') || (itemData.type == 'nr_scientific')) {
				if (itemOptions.length > 0) itemOptions += "; ";
				var fudgeFactor = itemData.TypeDetails.fudgeFactor;
				if (fudgeFactor > 0.0) {
					if (fudgeFactor < 1.0) fudgeFactor *= 100;
					itemOptions += fudgeFactor + "% tolerance";
				} else itemOptions += "No tolerance";
		
				var tensValue = itemData.TypeDetails.tensValue;
				if (tensValue > 0.0) {
					if (itemOptions.length > 0) itemOptions += "; ";
					itemOptions += itemData.TypeDetails.tensValue + " for factors of 10";
				}
		
				var sigDigsValue = itemData.TypeDetails.sigDigsValue;
				if (sigDigsValue != 1.0) {
					if (itemOptions.length > 0) itemOptions += "; ";
					itemOptions += itemData.TypeDetails.sigDigsValue + " for wrong sig digs";
				}
			}

			var itemType;
			if (itemData.type == 'nr') itemType = 'None';
			else {
				var remainder = itemData.type.substr(3);
				itemType = remainder.substr(0, 1).toUpperCase() + remainder.substr(1);
			}
			
			var itemAnswer;
			if (itemData.type == 'nr') {
				itemAnswer = "";
				itemOptions = "";
			} else if (('scoring' in itemData) && (itemData.scoring == 'omit')) {
				itemAnswer = "<i>Omitted</i>";
				itemOptions = "";
			} else if (itemData.ScoredResponses.length == 1) {
				var itemAnswer = '';
				for (var j = 0; j < itemData.Answers.length; ++j) {
					if (itemData.Answers[j].id == itemData.ScoredResponses[0].id) {
						itemAnswer = itemData.Answers[j].response;
					}
				}
			} else {
				itemAnswer = "<i>See details</i>";
			}
		
			var rowHTML = 
			'<tr class="nr_question_row"> \
				<td class="top nr_number">' + itemData.number + '</td>\
				<td class="top nr_type">' + itemType + '</td>\
				<td class="top nr_answer">' + itemAnswer + '</td>\
				<td class="top nr_options">' + itemOptions + '</td>\
				<td class="last top nr_actions" style="padding-right: 10px;" align="right">';
				
			if (canEdit) {
				rowHTML += 
					'<div style="display:block">\
						<button title="Edit" class="menu_button edit_icon questionEdit">&nbsp;</button>';
				if (!isGenerated) {
					rowHTML += 
						'<button title="Delete" class="menu_button delete_icon questionDelete">&nbsp;</button>';
				}
				rowHTML += 
					'</div>';
			}
			rowHTML += 
				'</td>\
			</tr>';	
			
			$('#nr_questionlist').append(rowHTML);
		}
	}

	$('#openAddNRQuestion').click(function(e) {
		e.preventDefault();
		if (!canEdit || isGenerated) return;

		// Get numbering options

		var numberingOptions = [];
		var partsRegex = /^([0-9]+)([a-z]?)$/;
		var nextNumber;
		if (activeSectionScoring.Content.length == 0) {

			nextNumber = 1;

		} else {

			var lastQuestion = activeSectionScoring.Content[activeSectionScoring.Content.length - 1];
			var match = partsRegex.exec(lastQuestion.number);
			nextNumber = parseInt(match[1]) + 1;

			if (match[2] != '') {
				var nextLetter = String.fromCharCode(match[2].charCodeAt(0) + 1);
				numberingOptions.push(match[1] + nextLetter);
			}	
		}
		numberingOptions.push(nextNumber);
		numberingOptions.push(nextNumber + 'a');

		$('#dnq_numberingType').empty();
		for (var i = 0; i < numberingOptions.length; ++i) {
			var value = numberingOptions[i];
			$('#dnq_numberingType').append('<option value="' + value + '">' + value + '</option>')
		}
		$('#dnq_numberingType').val(nextNumber);

		// Create new item

		QuestionDetails.setActiveItem({
			id: getNewID(4),
			type: 'nr',
			number: nextNumber.toString(),
			Answers: [],
			ScoredResponses: [],
			TypeDetails: {
				nrDigits: parseInt($('#nr_nr_digitCount').val())
			}
		});

		// Show remaining details

		QuestionDetails.rebuildNRDetails();
	
		$('#editNRQuestion_dialog').dialog('open');
		document.activeElement.blur();
	});
   
	$('#nr_questionlist').on('click', '.questionEdit', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		bufferedItems = [];

		var index = $(this).closest('.nr_question_row').index();
		QuestionDetails.setActiveItem(JSON.parse(JSON.stringify(activeSectionScoring.Content[index])));
		startEditNRItem();
	});

	var startEditNRItem = function() {
		
		// Get numbering options

		var index = false;
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			if (activeSectionScoring.Content[i].id == QuestionDetails.activeItem.id) index = i;
		}

		var numberingOptions = [];
		if (isGenerated) {

			numberingOptions.push(QuestionDetails.activeItem.number);

		} else {

			var partsRegex = /^([0-9]+)([a-z]?)$/;
			var nextNumber;
			if (index == 0) {
	
				var match = partsRegex.exec(QuestionDetails.activeItem.number);
				nextNumber = match[1];
	
			} else {
	
				var lastQuestion = activeSectionScoring.Content[index - 1];
				var match = partsRegex.exec(lastQuestion.number);
				nextNumber = parseInt(match[1]) + 1;
	
				if (match[2] != '') {
					var nextLetter = String.fromCharCode(match[2].charCodeAt(0) + 1);
					numberingOptions.push(match[1] + nextLetter);
				}	
			}
			numberingOptions.push(nextNumber);
			numberingOptions.push(nextNumber + 'a');

		}

		$('#dnq_numberingType').empty();
		for (var i = 0; i < numberingOptions.length; ++i) {
			var value = numberingOptions[i];
			$('#dnq_numberingType').append('<option value="' + value + '">' + value + '</option>')
		}
		$('#dnq_numberingType').val(QuestionDetails.activeItem.number);

		// Show remaining details

		QuestionDetails.rebuildNRDetails();

		$('#editNRQuestion_dialog').dialog('open');
		document.activeElement.blur();
	}

	$('#deleteNRQuestion_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': function() {
				var itemID = $('#deleteNRQuestion_dialog').attr('data-item-id');
				var contextedDelete = $.proxy(deleteItem, $('#deleteNRQuestion_dialog'), itemID);
				contextedDelete();
			}
		}, open: function(event) {
		   	var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$('#nr_questionlist').on('click', '.questionDelete', function(e) {
		e.preventDefault();
		if (!canEdit || isGenerated) return;

		var index = $(this).closest('.nr_question_row').index();
		var itemID = activeSectionScoring.Content[index].id;

		var $row = $(this).closest('tr');
		$('#deleteNRQuestion_text').html('<p>Permanently delete question ' + $row.children('.nr_number').text() + ' from this section?</p>');
		$('#deleteNRQuestion_dialog').attr('data-item-id', itemID);
		$('#deleteNRQuestion_dialog').dialog('open');
    });

	/**************************************************************************************************/
	/* Adding and editing WR sections                                                                 */
	/**************************************************************************************************/

	$('#editWRSection_dialog').dialog({
		autoOpen: false,
		width: 420,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				$(this).dialog("close");
			},
			'Save': sectionSave
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			$buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$(document).on('click', '.openAddWRsection', async function(e){
		e.preventDefault();
		if (!canEdit || isGenerated) return;

		var sectionID = getNewID(4);

		activeSectionScoring = {
			id: sectionID,
			Content: []
		};

		activeSectionLayout = {
			id: sectionID,
			type: 'wr',
			header: await localizationInstance.getString('defaults_wr_header'),
			includeSpace: userDefaults.Document.Sections.wr_includeSpace
		};

        if (activeSectionLayout.includeSpace) activeSectionLayout.columnCount = 1;
        else activeSectionLayout.columnCount = 2;

		bufferedItems = [];

		rebuildWREditor();

        $('#editWRSection_dialog').dialog('open');
		$('#dwr_questionCount').focus();
    });

	$('#startEditWr').click(function(e) {
		e.preventDefault();
		if (!canEdit) return;

		bufferedItems = [];

		rebuildWREditor();
		
		$('#editWRSection_dialog').dialog('open');
		if (activeSectionScoring.Content.length == 0) $('#ewr_questionCount').focus();
		else $('#ewr_header').focus();
	});

	function updateWRIncludeSpace() {
		$('#dwr_wr_includeSpace').prop('checked', activeSectionLayout.includeSpace);
		if (activeSectionLayout.includeSpace) {
			$('#dwr_columnCount').spinner('disable');
			$('#dwr_columnCount').closest('.labeled-input').children('label').addClass("disabled");
		} else {
			$('#dwr_columnCount').spinner('enable');
			$('#dwr_columnCount').closest('.labeled-input').children('label').removeClass("disabled");
		}
	}

	function rebuildWREditor() {
		$('#dwr_header').val(activeSectionLayout.header);
		$('#dwr_columnCount').val(activeSectionLayout.columnCount);

		updateWRIncludeSpace();

		if (activeSectionScoring.Content.length == 0) {

			$('#dwr_startNumber').val(1);
			$('#dwr_questionCount').val('');
			$('#dwr_questionCount').spinner('option', 'min', 0);

		} else {

			var startNumber = parseInt(activeSectionScoring.Content[0].number);
			$('#dwr_startNumber').val(startNumber);
			$('#dwr_questionCount').val(activeSectionScoring.Content.length);
			$('#dwr_questionCount').spinner('option', 'min', activeSectionScoring.Content.length);

		}
	}

	$('#dwr_header').on('input', function() {
		activeSectionLayout.header = $('#dwr_header').val();
	});

	$('#dwr_wr_includeSpace').click(function() {
		activeSectionLayout.includeSpace = $('#dwr_wr_includeSpace').prop('checked');
		updateWRIncludeSpace();

		if (activeSectionLayout.includeSpace) activeSectionLayout.columnCount = 1;
		else activeSectionLayout.columnCount = 2;
		$('#dwr_columnCount').val(activeSectionLayout.columnCount);
	});

	$('#dwr_columnCount').on('change', function() {
		activeSectionLayout.columnCount = parseInt($('#dwr_columnCount').val());
	});
    $('#dwr_columnCount').spinner({ 
		stop: function() {
			activeSectionLayout.columnCount = parseInt($('#dwr_columnCount').val());
		}
	});

	function apply_wr_startNumber(startNumber) {
		if (activeSectionScoring.Content.length == 0) return;
		if (isNaN(startNumber)) return;

		var delta = startNumber - parseInt(activeSectionScoring.Content[0].number);
		var partsRegex = /^([0-9]+)([a-z]?)$/;

		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			var match = partsRegex.exec(activeSectionScoring.Content[i].number);
			var newNumber = parseInt(match[1]) + delta;
			var newAlpha = match[2];
	
			activeSectionScoring.Content[i].number = newNumber.toString() + newAlpha;
		}
	}

    $('#dwr_startNumber').on('keyup', function() {
		var startNumber = parseInt($('#dwr_startNumber').val());
		apply_wr_startNumber(startNumber);
	});
    $('#dwr_startNumber').spinner({ 
		stop: function() {
			var startNumber = parseInt($('#dwr_startNumber').val());
			apply_wr_startNumber(startNumber);
		}
	});
	
	function apply_wr_questionCount(numQuestions) {
		var numQuestions = parseInt($('#dwr_questionCount').val());
		if (isNaN(numQuestions)) numQuestions = 0;
		else if (numQuestions > 50) numQuestions = 50;

		// Add questions if necessary

		while (activeSectionScoring.Content.length < numQuestions) {

			var newItem;
			if (bufferedItems.length > 0) {

				newItem = bufferedItems.pop();

			} else {

				var thisNumber;
				if (activeSectionScoring.Content.length > 0) {
					var lastIndex = activeSectionScoring.Content.length - 1;
					thisNumber = parseInt(activeSectionScoring.Content[lastIndex].number) + 1;
				} else thisNumber = parseInt($('#dwr_startNumber').val());
	
				newItem = {
					id: getNewID(4),
					type: 'wr',
					number: thisNumber.toString(),
					TypeDetails: userDefaults.Question.TypeDetails.wr
				};
			}

			activeSectionScoring.Content.push(newItem);				
		}

		// Remove questions if necessary

		while (activeSectionScoring.Content.length > numQuestions) {
			bufferedItems.push(activeSectionScoring.Content.pop());
		}
	}

	$('#dwr_questionCount').on('keyup', apply_wr_questionCount);
    $('#dwr_questionCount').spinner({ 
		min: 0,
		stop: apply_wr_questionCount
	});

	/**************************************************************************************************/
	/* Viewing, adding, editing, and deleting single WR questions                                     */
	/**************************************************************************************************/

	$('#editWRQuestion_dialog').dialog({
		autoOpen: false,
		width: 400,
		minWidth: 400,
		height: 100,
		modal: true,
		resizable: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() { 
				$(this).dialog("close");
			},
			"Save": function() {
				if (QuestionDetails.validateWRQuestion() && $('#editWRQuestion_dialog form').validationEngine('validate')) {
					var contextedSave = $.proxy(saveActiveItem, $('#editWRQuestion_dialog'));
					contextedSave();
				}
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, resize: QuestionDetails.resizeImagePrompt
	});

	var rebuildWRQuestions = function() {
		$('#wr_questionlist').empty();
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			var itemData = activeSectionScoring.Content[i];

			// Get prompt text or placeholder

			var formattedText = '<p>';
			if ('scoring' in itemData) {
				if (itemData.scoring == 'bonus') formattedText += "<i>(Bonus)</i> ";
				else if (itemData.scoring == 'omitted') formattedText += "<i>Omitted</i>";
			} else {
				if (!('prompt' in itemData.TypeDetails)) formattedText += "<i>No text</i>";
				else if (itemData.TypeDetails.prompt.type == 'text') {
					if (itemData.TypeDetails.prompt.text.length > 0) {
						formattedText += escapeHTML(itemData.TypeDetails.prompt.text).replace(/\n/g, '</p><p>');
					} else formattedText += "<i>No text</i>";
				} else formattedText += "<i>Image</i>";	
			}
			formattedText += "</p>";

			// Get total score

			var totalScore = 0;
			for (var j = 0; j < itemData.TypeDetails.criteria.length; ++j) {
				totalScore += itemData.TypeDetails.criteria[j].value;
			}
	
			// Build row
			
			var rowHTML = '<tr class="wr_question_row"> \
				<td class="top wr_number">' + itemData.number + '</td> \
				<td class="top wr_prompt">' + formattedText + '</td> \
				<td class="top wr_score">' + totalScore + '</td> \
				<td class="last top wr_actions" style="padding-right: 10px;" align="right">';

			if (canEdit) {
				rowHTML += 
					'<div style="display:block">\
						<button title="Edit" class="menu_button edit_icon questionEdit">&nbsp;</button>';
				if (!isGenerated) rowHTML += 
						'<button title="Delete" class="menu_button delete_icon questionDelete">&nbsp;</button>';
				rowHTML +=
					'</div>';
			}
			rowHTML += '</td></tr>';

			$('#wr_questionlist').append(rowHTML);
		}
	}

	$('#openAddWRQuestion').click(function(e) {
		e.preventDefault();
		if (!canEdit || isGenerated) return;

		// Get numbering options

		var numberingOptions = [];
		var partsRegex = /^([0-9]+)([a-z]?)$/;
		var nextNumber;
		if (activeSectionScoring.Content.length == 0) {

			nextNumber = 1;

		} else {

			var lastQuestion = activeSectionScoring.Content[activeSectionScoring.Content.length - 1];
			var match = partsRegex.exec(lastQuestion.number);
			nextNumber = parseInt(match[1]) + 1;

			if (match[2] != '') {
				var nextLetter = String.fromCharCode(match[2].charCodeAt(0) + 1);
				numberingOptions.push(match[1] + nextLetter);
			}	
		}
		numberingOptions.push(nextNumber);
		numberingOptions.push(nextNumber + 'a');

		$('#dwq_numberingType').empty();
		for (var i = 0; i < numberingOptions.length; ++i) {
			var value = numberingOptions[i];
			$('#dwq_numberingType').append('<option value="' + value + '">' + value + '</option>')
		}
		$('#dwq_numberingType').val(nextNumber);

		// Create new item

		QuestionDetails.setActiveItem({
			id: getNewID(4),
			type: 'wr',
			number: nextNumber.toString(),
			Rubric: { html: "" },
			TypeDetails: userDefaults.Question.TypeDetails.wr
		});

		// Show remaining details

		$('#editWRQuestion_dialog').dialog('open');

		QuestionDetails.rebuildWRDetails();	
		document.activeElement.blur();
	});

	$('#wr_questionlist').on('click', '.questionEdit', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		bufferedItems = [];

		var index = $(this).closest('.wr_question_row').index();
		QuestionDetails.setActiveItem(JSON.parse(JSON.stringify(activeSectionScoring.Content[index])));
		startEditWRItem();
	});

	var startEditWRItem = function() {

		// Get numbering options

		var index = false;
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			if (activeSectionScoring.Content[i].id == QuestionDetails.activeItem.id) index = i;
		}

		var numberingOptions = [];
		var partsRegex = /^([0-9]+)([a-z]?)$/;
		var nextNumber;
		if (index == 0) {

			var match = partsRegex.exec(QuestionDetails.activeItem.number);
			nextNumber = match[1];

		} else {

			var lastQuestion = activeSectionScoring.Content[index - 1];
			var match = partsRegex.exec(lastQuestion.number);
			nextNumber = parseInt(match[1]) + 1;

			if (match[2] != '') {
				var nextLetter = String.fromCharCode(match[2].charCodeAt(0) + 1);
				numberingOptions.push(match[1] + nextLetter);
			}	
		}
		numberingOptions.push(nextNumber);
		numberingOptions.push(nextNumber + 'a');

		$('#dwq_numberingType').empty();
		for (var i = 0; i < numberingOptions.length; ++i) {
			var value = numberingOptions[i];
			$('#dwq_numberingType').append('<option value="' + value + '">' + value + '</option>')
		}
		$('#dwq_numberingType').val(QuestionDetails.activeItem.number);

		// Show remaining details

		$('#editWRQuestion_dialog').dialog('open');

		QuestionDetails.rebuildWRDetails(false);
		document.activeElement.blur();		
	}
	
	$('#deleteWRQuestion_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': function() {
				var itemID = $('#deleteWRQuestion_dialog').attr('data-item-id');
				var contextedDelete = $.proxy(deleteItem, $('#deleteWRQuestion_dialog'), itemID);
				contextedDelete();
			}
		}, open: function(event) {
		   	var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#wr_questionlist').on('click', '.questionDelete', function(e) {
		e.preventDefault();
		if (!canEdit || isGenerated) return;

		var index = $(this).closest('.wr_question_row').index();
		var itemID = activeSectionScoring.Content[index].id;

		var $row = $(this).closest('tr');
		$('#deleteWRQuestion_text').html('<p>Permanently delete question ' + $row.children('.wr_number').text() + ' from this section?</p>');
		$('#deleteWRQuestion_dialog').attr('data-item-id', itemID);
		$('#deleteWRQuestion_dialog').dialog('open');
    });

	/**************************************************************************************************/
	/* Working with outcomes                                                                          */
	/**************************************************************************************************/

	var rebuildOutcomesList = function() {
		
		var listHTML = "";
		if (documentScoring.Outcomes.length === 0) {

			listHTML += 
		 	'<tr class="category_blank">\
		 		<td colspan="3" style="font-style:italic; border-right:none;">No learning outcomes defined. Click "Add New Outcome" to create one.</td>\
			</tr>';

		} else {
			var sortedOutcomes = JSON.parse(JSON.stringify(documentScoring.Outcomes));
			sortedOutcomes.sort(function(a, b) {
				if (a.name < b.name) return -1;
				else if (a.name > b.name) return 1;
				else return 0;
			});

			// Get numbers for all sections, and for this section alone

			var indexNumbers = [];
			var sectionNumbers = [];
			var hasDuplicates = false;

			for (var i = 0; i < documentScoring.Sections.length; ++i) {
				var sectionID = documentScoring.Sections[i].id;
				for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
					var thisNumber = documentScoring.Sections[i].Content[j].number;

					if (indexNumbers.indexOf(thisNumber) >= 0) hasDuplicates = true;
					else indexNumbers.push(thisNumber);
					
					if (activeSectionScoring.id == sectionID) {
						sectionNumbers.push(thisNumber);
					}
				}
			}

			// Choose and sort a list of numbers to determine contiguous questions

			if (hasDuplicates) indexNumbers = sectionNumbers;

			var partsRegex = /^([0-9]+)([a-z]?)$/;

			indexNumbers.sort(function(a, b) {
				var aMatch = partsRegex.exec(a);
				var aNumber = parseInt(aMatch[1]);
				var aAlpha = aMatch[2];

				var bMatch = partsRegex.exec(b);
				var bNumber = parseInt(bMatch[1]);
				var bAlpha = bMatch[2];

				if (aNumber < bNumber) return -1;
				else if (aNumber > bNumber) return 1;
				else if (aAlpha < bAlpha) return -1;
				else if (aAlpha > bAlpha) return 1;
				else return 0;
			});

			// Build lists of question numbers for each outcome

			for (var i = 0; i < sortedOutcomes.length; i++) {
				var questionList = "";
				var isNew = true;

				var startIndex = -1;
				for (var index = 0; index <= indexNumbers.length; ++index) {
					var inOutcome;
					if (index < indexNumbers.length) {
						var itemIndex;
						for (itemIndex = 0; itemIndex < activeSectionScoring.Content.length; ++itemIndex) {
							if (activeSectionScoring.Content[itemIndex].number == indexNumbers[index]) break;
						}
	
						inOutcome = true;
						if (itemIndex < activeSectionScoring.Content.length) {
							var itemID = activeSectionScoring.Content[itemIndex].id;
							if (sortedOutcomes[i].item_ids.indexOf(itemID) == -1) inOutcome = false;
						} else inOutcome = false;	
					} else inOutcome = false;

					if (inOutcome) {

						if (startIndex == -1) startIndex = index;
						isNew = false;

					} else if (startIndex >= 0) {

						if (questionList.length > 0) questionList += ', ';
						if (index == startIndex + 1) {
							questionList += indexNumbers[startIndex];
						} else if (index == startIndex + 2) {
							questionList += indexNumbers[startIndex] + ', ';
							questionList += indexNumbers[startIndex + 1];
						} else {
							questionList += indexNumbers[startIndex] + '&ndash;';
							questionList += indexNumbers[index - 1];
						}

						startIndex = -1;
						
					}					
				}

		 		listHTML += 
					'<tr class="outcomes_row" data-outcome-id="' + sortedOutcomes[i].id + '"' + (isNew ? ' style="color:#AAAAAA;"' : '') + '>\
						<td class="outcome_name">' + escapeHTML(sortedOutcomes[i].name) + '</td>\
						<td><div class="category_questionlist" style="width: 320px; ">' + questionList +
						'</div><div style="clear: both;"></div></td>\
						<td class="last top" style="padding-right: 10px;" align="right">';
		 		if (canEdit) listHTML += 
							'<div style="display:block">' + 
								'<button title="Add" class="menu_button edit_outcome add_icon"' + (isNew ? '' : ' style="display:none;"') + '>&nbsp;</button>' + 
								'<button title="Edit" class="menu_button edit_outcome edit_icon"' + (isNew ? ' style="display:none;"' : '') + '>&nbsp;</button>' + 
								'<button title="Delete" class="menu_button delete_outcome delete_icon"' + (isNew ? ' style="display:none;"' : '') + '>&nbsp;</button>\
							</div>';
				listHTML += 
						'</td>\
					</tr>';
			}
		}
		
		$('#outcomesList').html(listHTML);
	}
	
	var outcomeSave = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		var validated = $form.validationEngine('validate');

		if ($('#outcome_selection_list .outcome_checkbox:checked').length == 0) {
			$('#outcome_selection_list').validationEngine('showPrompt', '* At least one item must be selected', 'error', 'bottomLeft', true);
			validated = false;
		}

		var outcomeID = $('#editOutcome_dialog').attr('data-outcome-id');
		var outcomeName = $('#outcome_name').val().trim();
		for (var i = 0; i < documentScoring.Outcomes.length; ++i) {
			if ((documentScoring.Outcomes[i].id != outcomeID) && (documentScoring.Outcomes[i].name == outcomeName)) {
				$('#outcome_name').validationEngine('showPrompt', '* Outcome names must be unique', 'error', 'bottomLeft', true);
				validated = false;
				break;
			} 
		}

		if (validated) {
			var index;
			for (index = 0; index < documentScoring.Outcomes.length; ++index) {
				if (documentScoring.Outcomes[index].id == outcomeID) break;
			}

			if (index == documentScoring.Outcomes.length) {
				documentScoring.Outcomes.push({
					id: outcomeID,
					name: outcomeName,
					item_ids: []
				});
			} else documentScoring.Outcomes[index].name = outcomeName

			var $selection_checkboxes = $('#outcome_selection_list .outcome_checkbox');
			for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
				var itemID = activeSectionScoring.Content[i].id;
				var itemIndex = documentScoring.Outcomes[index].item_ids.indexOf(itemID);
				if ($selection_checkboxes.eq(i).prop('checked')) {
					if (itemIndex == -1) documentScoring.Outcomes[index].item_ids.push(itemID);
				} else if (itemIndex >= 0) {
					documentScoring.Outcomes[index].item_ids.splice(itemIndex, 1);
				}
			}
	
			// Save form
	
			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveDocumentData()
			.then(function() {
	
				// Finish up
	
				$dialog.dialog("close");
	
				$.gritter.add({
					title: "Success",
					text: "Outcome saved.",
					image: "/img/success.svg"
				});

			}).catch(function() {
				showSubmitError($dialog);			
			});
		}
	}
    
	$('#editOutcome_dialog').dialog({
        autoOpen: false,
		width: 595,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		},
        buttons: {
            "Cancel": function() { 
				$(this).dialog("close");
			},
            "Save": outcomeSave
		},
		open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			initSubmitProgress($(this));
		},
		"close": function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

	$('#outcome_name').on('keydown', function(e) {
		$('form').validationEngine('hideAll');
    });

    $('#outcomesList').on('click', '.edit_outcome', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var outcomeID = $(this).closest('.outcomes_row').attr('data-outcome-id');
		$('#editOutcome_dialog').attr('data-outcome-id', outcomeID);

		var index;
		for (index = 0; index < documentScoring.Outcomes.length; ++index) {
			if (documentScoring.Outcomes[index].id == outcomeID) break;
		}

		$('#outcome_name').val(documentScoring.Outcomes[index].name);
		var itemIDs = documentScoring.Outcomes[index].item_ids;

	 	var cat_checkboxes = "";
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			var thisNumber = activeSectionScoring.Content[i].number;
			var itemID = activeSectionScoring.Content[i].id;
			var inOutcome = (itemIDs.indexOf(itemID) >= 0);
			
			cat_checkboxes += 
				"<div class='outcome_selection_entry'> \
					<label for='outcome_selection_" + i + "'>" + thisNumber + ".</label> \
					<input type='checkbox' class='outcome_checkbox' id='outcome_selection_" + i + "' " + (inOutcome ? 'checked' : '') + "> \
				</div>";
		}
		$('#outcome_selection_list').html(cat_checkboxes);
		 
		lastCheckboxIndex = -1;
		$('#editOutcome_dialog').dialog('open');		
    });

	$('#outcome_selection_list').on('click', '.outcome_checkbox', function(e) {
		var thisIndex = $(this).closest('.outcome_selection_entry').index();
		if (e.shiftKey && (lastCheckboxIndex != -1)) {
			var $selection_checkboxes = $('#outcome_selection_list .outcome_checkbox');
			var lastChecked = $selection_checkboxes.eq(lastCheckboxIndex).prop("checked");
			var thisChecked = $selection_checkboxes.eq(thisIndex).prop("checked");
			if (lastChecked == thisChecked) {
				var minIndex = Math.min(thisIndex, lastCheckboxIndex) + 1;
				var maxIndex = Math.max(thisIndex, lastCheckboxIndex) - 1;
				for (var i = minIndex; i <= maxIndex; i++) {
					$selection_checkboxes.eq(i).prop("checked", thisChecked);
				}
			}
			lastCheckboxIndex = -1;
		} else lastCheckboxIndex = thisIndex;
    });
    
	$('#outcomes_checkAll').click(function() {
		$('#outcome_selection_list .outcome_checkbox').prop('checked', true);
    });

	$('#outcomes_clearAll').click(function() {
		$('#outcome_selection_list .outcome_checkbox').prop('checked', false);
    });
	
    var deleteOutcome = function() {
	   	var $dialog = $(this);
	   	
	   	var outcomeID = $('#deleteOutcome_dialog').attr('data-outcome-id');

		var outcomeIndex;
		for (outcomeIndex = 0; outcomeIndex < documentScoring.Outcomes.length; ++outcomeIndex) {
			if (documentScoring.Outcomes[outcomeIndex].id == outcomeID) break;
		}

		var gritterText = "Outcome removed from section.";
		if (outcomeIndex < documentScoring.Outcomes.length) {
			var itemIDs = documentScoring.Outcomes[outcomeIndex].item_ids;

			var itemIndex = 0;
			while (itemIndex < itemIDs.length) {
				var itemID = itemIDs[itemIndex];

				var inSection = false;
				for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
					if (activeSectionScoring.Content[i].id == itemID) inSection = true;
				}

				if (inSection) itemIDs.splice(itemIndex, 1);
				else ++itemIndex;
			}

			if (itemIDs.length == 0) {
				documentScoring.Outcomes.splice(outcomeIndex, 1);
				cleanWeights();

				gritterText = "Outcome deleted.";
			}
		}

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;
		
		saveDocumentData()
		.then(function() {

			// Finish up

			$('#document_title').text($('#ddoc_save_name').val());
			if (!canEdit) $('#document_title').append(' &mdash; <b>READ ONLY</b>');

			$dialog.dialog("close");

			$.gritter.add({
				title: "Success",
				text: gritterText,
				image: "/img/success.svg"
			});
			
		}).catch(function() {
			showSubmitError($dialog);			
		});
    }

	$('#deleteOutcome_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteOutcome
		}, open: function(event) {
		   	var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

    $('#outcomesList').on('click', '.delete_outcome', function(e) {
		e.preventDefault();
		if (!canEdit) return;
		
		var $outcomeRow = $(this).closest('.outcomes_row');
		var outcomeID = $outcomeRow.attr('data-outcome-id');
		var outcomeName = $outcomeRow.children('.outcome_name').text();

		$('#deleteOutcome_text').html('<p>Remove the outcome "' + outcomeName + '" from this section?</p>');
		$('#deleteOutcome_dialog').attr('data-outcome-id', outcomeID);
		$('#deleteOutcome_dialog').dialog('open');
    });
    
    $('#openAddOutcome').click(function(e){
		e.preventDefault();
		if (!canEdit) return;

		$('#editOutcome_dialog').attr('data-outcome-id', getNewID(4));
		$('#outcome_name').val('');

	 	var cat_checkboxes = "";
		for (var i = 0; i < activeSectionScoring.Content.length; ++i) {
			var thisNumber = activeSectionScoring.Content[i].number;
			var itemID = activeSectionScoring.Content[i].id;
			
			cat_checkboxes += 
				"<div class='outcome_selection_entry'> \
					<label for='outcome_selection_" + i + "'>" + thisNumber + ".</label> \
					<input type='checkbox' class='outcome_checkbox' id='outcome_selection_" + i + "'> \
				</div>";
		}
		$('#outcome_selection_list').html(cat_checkboxes);
		 
		lastCheckboxIndex = -1;
		$('#editOutcome_dialog').dialog('open');
    });
    
 	/**************************************************************************************************/
	/* Build functions                                                                                */
	/**************************************************************************************************/

	$('#buildQuestions_button').click(function(e){
		e.preventDefault();
		
		if ('formLocation' in userDefaults.Document) {
			$('#buildQuestions_form').val(userDefaults.Document.formLocation);
		} else $('#buildQuestions_form').val('none');

		$('#buildQuestions_dialog').dialog('open');
	});
	
    $('#buildQuestions_dialog').dialog({
        autoOpen: false,width: 365,modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() { 
				$(this).dialog("close");
			},
			"Build": function() {

				navigateWithPost('/build_pdf', {
					pdfType: 'question_book',
					documentID: documentID,
					formLocation: $('#buildQuestions_form').val()
				});
				$('#buildQuestions_dialog').dialog('close');

			}
        }, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Build")').addClass('btn btn-save btn_font');
			$buttonPane.find('button:contains("Build")').focus();
		}, close: function() {
            $('form').validationEngine('hideAll');
        }
    });

	$('#buildGenericPDF_button').click(function(e) {
		e.preventDefault();

		var documentLists = {};
		documentLists[documentID] = false;

		navigateWithPost('/build_pdf', {
			pdfType: 'response_form',
			documentListsJSON: JSON.stringify(documentLists)
		});
	});
	
	$('#buildKey_button').click(function(e){
		e.preventDefault();

		navigateWithPost('/build_pdf', {
			pdfType: 'pdf_from_url',
			url: '/Documents/print_key/' + documentID,
			target: 'key.pdf'
		});
	});
	
	$('#buildClassPDF_button').click(function(e){
		e.preventDefault();

        ClassListCommon.initializeClassLists({
            needsEmail: false
        });
        $('#buildClassPDF_dialog').dialog('open');        
        document.activeElement.blur();
	});

    var printClassPDF = function() {
        var documentLists = {};
        documentLists[documentID] = [];
        $('#class_list_wrapper .list_row').each(function() {
            if ($(this).find('.class_list_checkbox').prop('checked')) {
                documentLists[documentID].push(JSON.parse($(this).attr('data-json-data')));
            }
        });

		navigateWithPost('/build_pdf', {
			pdfType: 'response_form',
			documentListsJSON: JSON.stringify(documentLists)
		});

		$('#buildClassPDF_dialog').dialog('close');
    }

    $('#buildClassPDF_dialog').dialog({
        autoOpen: false,
        width: 500,
        modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$(this).dialog("close");
			},
			"Print": printClassPDF
        }, create: function() {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
            $buttonPane.find('button:contains("Print")').attr('id', 'class_list_continue');
        }, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Print")').addClass('btn btn-save btn_font');
		}, close: function() {
            $('form').validationEngine('hideAll');
        }
    });

 	/**************************************************************************************************/
	/* Global configuration                                                                           */
	/**************************************************************************************************/

	$('form').validationEngine({
		validationEventTrigger: 'submit',
		promptPosition : "bottomLeft"
	}).submit(function(e){ e.preventDefault() });

    $('.marginSpinner').spinner({
	    step: 0.25,
        min: 0
    });

    $('.intSpinner').spinner({
	    step: 1,
	    min: 1
    });

	$('#view_menu').menu({
		disabled: true
	});

	$('#ddoc_topMarginSize').spinner('option', 'min', 0.0);
	$('#ddoc_topMarginSize').spinner('option', 'max', 5.5);

	$('#ddoc_bottomMarginSize').spinner('option', 'min', 0.0);
	$('#ddoc_bottomMarginSize').spinner('option', 'max', 5.5);

	$('#ddoc_leftMarginSize').spinner('option', 'min', 0.0);
	$('#ddoc_leftMarginSize').spinner('option', 'max', 4.25);

	$('#ddoc_rightMarginSize').spinner('option', 'min', 0.0);
	$('#ddoc_rightMarginSize').spinner('option', 'max', 4.25);
    
    $('#dwq_height').spinner('option', 'min', 0.5);
	$('#dwq_height').spinner('option', 'max', 10.0);

 	/**************************************************************************************************/
	/* Load form                                                                                      */
	/**************************************************************************************************/

	initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});
		window.location.href = '/Documents';
	});
});
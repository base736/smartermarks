/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenderTools from '/js/assessments/render_tools.js?7.0';

document.addEventListener("DOMContentLoaded", async function() {

	var pointsPerInch = 72.0;
	var pageWidth = 8.5 * pointsPerInch;
	var leftMarginSize = 0.75 * pointsPerInch;
	var rightMarginSize = 0.75 * pointsPerInch;
	var itemIndent = 0.43 * pointsPerInch;
	var minGapWidth = 0.20 * pointsPerInch;
	var circleSize = 10.0;
	var circleSpacing = 6.0;
	var defaultChoices = 4;

	function formatAnswer(answer, type) {
        var formattedAnswer = false;

		if (type == 'nr') {
			var parts = answer.split(/[eE]/);
			if (parts.length == 2) {
				formattedAnswer = parts[0] + '&times;10<sup>' + parts[1] + '</sup>';
			} else formattedAnswer = answer;
		} else formattedAnswer = answer;
		
		return formattedAnswer;
	}
	
	var keyHTML = "";
	var totalScore = 0.0;
	for (var i = 0; i < documentScoring.Sections.length; ++i) {
		var scoringSection = documentScoring.Sections[i];
		var layoutSection = false;
		for (var j = 0; j < documentLayout.Body.length; ++j) {
			if (documentLayout.Body[j].id == scoringSection.id) {
				layoutSection = documentLayout.Body[j];
			}
		}

		var numQuestions = scoringSection.Content.length;
		var numColumns = 1;

		if (layoutSection.type == 'mc') {
			numColumns = layoutSection.columnCount;
			if (numColumns == 'auto') {
				
				var choices = [];
				for (var j = 0; j < scoringSection.Content.length; ++j) {
					choices.push(scoringSection.Content[j].Answers.length);
				}
				
				var minColumns = 1;
				var maxColumns = (numQuestions < 5) ? 5 : 10;
				
				numColumns = -1;
				for (var thisColumns = minColumns; thisColumns <= maxColumns; ++thisColumns) {
			
					var columnChoices = [];
					for (var j = 0; j < thisColumns; ++j) columnChoices.push(-1);
					
					var columnNum = 0;
					var rowNum = 0;
					var thisRows = Math.ceil(numQuestions / thisColumns);
			
					for (var j = 0; j < numQuestions; ++j) {
						if (choices[j] > columnChoices[columnNum]) columnChoices[columnNum] = choices[j];
						if (++rowNum >= thisRows) {
							rowNum = 0;
							columnNum++;
						}
					}
					
					for (var j = 0; j < thisColumns; ++j) {
						if (columnChoices[j] == -1) columnChoices[j] = defaultChoices;
					}
					
					var thisGapWidth = 0.0;
					if (thisColumns > 1)
					{
						var usedWidth = 0.0;
						for (var j = 0; j < thisColumns; ++j) usedWidth += itemIndent + (columnChoices[j] - 1) * (circleSize + circleSpacing) + circleSize / 2.0;
						thisGapWidth = (pageWidth - leftMarginSize - rightMarginSize - usedWidth) / (thisColumns - 1);
					}
					
					if ((thisColumns == minColumns) || (thisGapWidth >= minGapWidth)) numColumns = thisColumns;
				}
			}
		}
		
		keyHTML += '<div class="key_header">' + layoutSection.header + '</div>';
		var numRows = Math.ceil(numQuestions / numColumns);
		for (var r = 0; r < numRows; ++r) {
			keyHTML += '<div class="key_row">';
			for (var c = 0; c < numColumns; ++c) {
				var j;
				if (layoutSection.type == 'mc') j = c * numRows + r;
				else j = r * numColumns + c;
					
				if (j < numQuestions) {
					var outOf = 0.0;
					var isOmitted = false;
					var includeInTotal = true;

					var extra = "";
					if ('scoring' in scoringSection.Content[j]) {

						if (scoringSection.Content[j].scoring == 'omit') {
							includeInTotal = false;
							isOmitted = true;
							extra += ", omitted";
						} else if (scoringSection.Content[j].scoring == 'bonus') {
							includeInTotal = false;
							extra += ", bonus";
						}

					} else if (layoutSection.type == 'nr') {

						if (scoringSection.Content[j].type == 'nr_selection') {
							if (scoringSection.Content[j].TypeDetails.markByDigits) extra += ", marks per digit";
							if (scoringSection.Content[j].TypeDetails.ignoreOrder) extra += ", any order";
						} else if (scoringSection.Content[j].type != 'nr_fraction') {
							var fudgeFactor = scoringSection.Content[j].TypeDetails.fudgeFactor;
							if (fudgeFactor != 0.0) {
								if (fudgeFactor < 1.0) fudgeFactor *= 100.0;
								extra += ", within " + fudgeFactor + "%";
							}
						}
						
					}
					
					var answersHTML = "";
					if (layoutSection.type == 'wr') {
						var criteria = scoringSection.Content[j].TypeDetails.criteria;
						for (var k = 0; k < criteria.length; ++k) outOf += criteria[k].value;

						if ('Rubric' in scoringSection.Content[j]) {
							answersHTML = scoringSection.Content[j].Rubric.html;
						} else {
							answersHTML = "<div>";
							for (var k = 0; k < criteria.length; ++k) {
								answersHTML += "<div";
								if (k > 0) answersHTML += " style='margin-top:5px;'";
								answersHTML += ">" + criteria[k].label + " /" + criteria[k].value + "</div>";
							}
							answersHTML += "</div>";
						}
					} else {
						var answerMap = {};
						for (var k = 0; k < scoringSection.Content[j].Answers.length; ++k) {
							var thisAnswer = scoringSection.Content[j].Answers[k];
							answerMap[thisAnswer.id] = thisAnswer.response.toUpperCase();
						}

						var simpleKey = true;
						if (scoringSection.Content[j].ScoredResponses.length == 1) {
							var scoreEntry = scoringSection.Content[j].ScoredResponses[0];
							if (scoreEntry.value != 1.0) simpleKey = false;
						} else simpleKey = false;

						outOf = 0.0;
						for (var k = 0; k < scoringSection.Content[j].ScoredResponses.length; ++k) {
							var scoreEntry = scoringSection.Content[j].ScoredResponses[k];

							var response = '';
							var ids = ('ids' in scoreEntry) ? scoreEntry.ids : [scoreEntry.id];
							for (var index = 0; index < ids.length; ++index) response += answerMap[ids[index]];

							var thisScore = scoreEntry.value;

                            var thisOutOf = thisScore;
                            if (scoringSection.Content[j].type == 'nr_selection') {
    							if (scoringSection.Content[j].TypeDetails.markByDigits) {
                                    thisOutOf *= response.length;
                                }
                            }

							if (thisOutOf > outOf) outOf = thisOutOf;

							if (simpleKey) {
								answersHTML += "<b>" + formatAnswer(response, layoutSection.type) + "</b>";
								if (extra.length > 0) answersHTML += " (" + extra.substring(2) + ")";
							} else {
								answersHTML += "<div><b>" + formatAnswer(response, layoutSection.type) + "</b> (";
								if (isOmitted) answersHTML += extra.substring(2);
								else answersHTML += thisScore.toString() + extra;								
								answersHTML += ")</div>";
							}
						}
					}
					
					if (includeInTotal) totalScore += parseFloat(outOf);
					
					keyHTML += '<div class="key_column"><div class="key_number">' + scoringSection.Content[j].number + 
						'.</div><div class="key_answer">' + answersHTML + '</div></div>';
				} else {
					keyHTML += '<div class="key_column"></div>';
				}
			}
			keyHTML += "</div>";
		}
	}

	if (documentScoring.Scoring.type == 'Total') {
        var scoreString = (totalScore == Math.floor(totalScore)) ? totalScore.toFixed(0) : totalScore.toFixed(2);
		keyHTML += "<div style='margin-top:20px'>Total score: " + scoreString + " marks</div>";
	}
	$("#key_wrapper").html(keyHTML);

    await RenderTools.initialize();
	await RenderTools.renderEquations($('#key_wrapper'));

	$('body').append('<span class="page_loaded"></span>');
});
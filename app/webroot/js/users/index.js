/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as TableSelection from '/js/common/table_selection.js?7.0';

$.extend($.ui.dialog.prototype.options, {
	resizable: false,
    create: function() {
        var $this = $(this);

        $this.keypress(function(e) {
            if (e.key === 'Enter') {
	        	if ($this.attr('id') != 'renewUsers_dialog') {
	                $this.parent().find('.ui-dialog-buttonpane .btn-save').click();
					return false;
	        	}
            }
        });
    }
});

$(document).ready(function() {

    TableSelection.initialize();

	$('#hasExpiry').prop('checked', true);
	$(".datepicker").datepicker({
		dateFormat: "MM d, yy",
		onSelect: function(dateText) {
			var today = new Date().toISOString().slice(0, 10);
			$('#renewal_note').val(today + ": Renewed through " + dateText);
		}
	});
	$(".datepicker").datepicker('setDate', new Date());

	$('#hasExpiry').on('click', function() {
		if ($('#hasExpiry').prop('checked') == true) {
			$('#expiry').removeAttr('disabled');
			$('#expiry_warnings').removeAttr('disabled');
		} else {
			$('#expiry').attr("disabled", "disabled");
			$('#expiry_warnings').attr("disabled", "disabled");
		}
	});

	$('#renewUsers_button').click(function(e) {
		e.preventDefault();
		
		var userId = '';
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				userId = $(this).closest('tr').attr('data-user-id');
			}
		});

		if (userId == '') window.location.href = "/Users/renew_group";
		else window.location.href = "/Users/renew_group?u=" + userId;
	});

	var deleteUser = function() {
		var $dialog = $(this);

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var delete_ids = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				delete_ids.push($(this).closest('tr').data('user-id'));
			}
		});

		ajaxFetch('/Users/delete/', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				user_ids: delete_ids
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				addClientFlash({
					title: "Success",
					text: responseData.count + " user" + ((responseData.count == 1) ? '' : 's') + " deleted",
					image: "/img/success.svg"
				});
			} else {
				addClientFlash({
					title: "Error",
					text: "Unable to delete user" + ((responseData.count == 1) ? '' : 's'),
					image: "/img/error.svg"
				});
			}
			window.location.reload();
		});
	}
	
	$('#deleteUser_button').click(function(e){
		e.preventDefault();
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked == 1) {
			var $userEmail = $selectedCheckbox.closest('tr').find('.td_userEmail').html();
			$('#deleteUser_text').html('<p>Delete user "' + $userEmail + '"?</p>');
		} else {
			$('#deleteUser_text').html('<p>Delete the ' + numChecked + ' selected users?</p>');
		}

		$('#deleteUser_dialog').dialog('open');
	});

	$('#deleteUser_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteUser
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
		   	initSubmitProgress($(this));
	      	$('#email').focus();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});		
	
	$('#approveUser_button, #disableUser_button').click(function(e) {

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		var newStatus;
		if ($(this).is('#approveUser_button')) newStatus = 1;
		else newStatus = 0;

		var selectedUsers = [];
		var goodSelection = true;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var approved = parseInt($(this).closest('tr').data('approved'));
				if (1 - approved != newStatus) goodSelection = false;

				var userID = $(this).closest('tr').data('user-id');
				selectedUsers.push(userID);
			}
		});

		if (goodSelection) {
			ajaxFetch('/Users/change_approved', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					user_ids: selectedUsers,
					new_status: newStatus
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();

				if (responseData.success) {
					addClientFlash({
						title: "Success",
						text: responseData.count + " account" + ((responseData.count == 1) ? '' : 's') + " " + 
							((newStatus == 1) ? 'approved' : 'disabled'),
						image: "/img/success.svg"
					});		
				} else {
					addClientFlash({
						title: "Error",
						text: "Error " + ((newStatus == 1) ? 'approving' : 'disabling') + " accounts",
						image: "/img/error.svg"
					});
				}
				window.location.reload();
				
			});	
		} else {
			pendingAJAXPost = false;

			$.gritter.add({
				title: "Error",
				text: "Cannot " + ((newStatus == 1) ? 'approve' : 'disable') + " selected accounts",
				image: "/img/error.svg"
			});		
		}
	});

	$('#userHistory_button').click(function(e) {
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var $userID = $(this).closest('tr').data('user-id');
				window.location = "/users/history/" + $userID;
			}
		});
	});

	$('#userEdit_button').click(function(e) {
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var $userID = $(this).closest('tr').data('user-id');
				window.location = "/users/edit/" + $userID;
			}
		});
	});

	function submitSearch() {
		var urlVars = getUrlVars();

		delete urlVars.sort;
		delete urlVars.direction;
		delete urlVars.page;

		urlVars.search = $('#search_input').val().trim();
		urlVars.role = $('#search_role').val();
		var newQuery = jQuery.param(urlVars);
        
		window.location.href = window.location.href.split('?')[0] + "?" + newQuery;
	}
	
	$('#search_input').on('keypress', function(e) {
	    if (e.key === 'Enter') submitSearch();
	});

	$('.search_icon').on('click', submitSearch);

	$('.search_dropdown').on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var $menu = $(this).parent().find('.search_menu');
		if (!$menu.is(":visible")) {
			$menu.show();
			$menu.find('input').focus();
			$menu.find('input').select();
		} else $menu.hide();
	});
	
	$('#search_role').on('change', function() {
		$('#search_input').focus();
	});

	$(document).on('click', function(e) {
		var $target = $(e.target);
		$('.search_menu').each(function() {
			if (!$target.is($(this)) && ($(this).find($target).length == 0)) $(this).hide();
		});
   	});

	var urlVars = getUrlVars();
	if ('search' in urlVars) {
		var search = urlVars.search.replace(/\+/g, '%20')
		$('#search_input').val(decodeURIComponent(search));
	}
	if ('role' in urlVars) {
		var role = urlVars.role.replace(/\+/g, '%20')
		$('#search_role').val(decodeURIComponent(role));
	}
});
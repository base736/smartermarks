/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenewCommon from '/js/common/renewal.js?7.0';

var finishAdd = function(bufferedResult) {
	var numAdded = 0;
	var numSkipped = 0;

	if ('new_users' in bufferedResult) {
		for (var i = 0; i < bufferedResult.new_users.length; ++i) {
			var isNew = true;
			for (var j = 0; j < groupDetails.length; ++j) {
				if (bufferedResult.new_users[i].email == groupDetails[j].email) isNew = false;
			}
			if (isNew) {
				numAdded++;
				groupDetails.push({
					email: bufferedResult.new_users[i].email,
					name: bufferedResult.new_users[i].name,
					expires: parseInt(Date.now() / 1000),
					is_admin: 0
				});	
			} else numSkipped++;
		}
	}

	for (var i = 0; i < bufferedResult.details.length; ++i) {
		var exists = false;
		for (var j = 0; j < groupDetails.length; ++j) {
			if (groupDetails[j].id == bufferedResult.details[i].id) exists = true;
		}
		if (!exists) {
			numAdded++;
			bufferedResult.details[i].is_admin = 0;
			groupDetails.push(bufferedResult.details[i]);
		} else numSkipped++;
	}

	var flashMessage;
	if (numAdded == 0) flashMessage = "No users added";
	else flashMessage = numAdded.toString() + " user" + ((numAdded == 1) ? '' : 's') + " added";
	if (numSkipped > 0) flashMessage += "; " + numSkipped.toString() + " skipped";
	flashMessage += ".";

	if (numAdded > 0) {
		$.gritter.add({
			title: "Success",
			text: flashMessage,
			image: "/img/success.svg"
		});
	} else {
		$.gritter.add({
			title: "Error",
			text: flashMessage,
			image: "/img/error.svg"
		});
	}
	
	rebuildMemberList();
}

var rebuildMemberList = function() {

	// Build renewal date

	var nowTimestamp = dayjs().unix();
	var minTimestamp = dayjs().subtract(1, 'month').unix();
	var maxTimestamp = dayjs().add(1, 'month').unix();

	var expiries = [];
	for (var i = 0; i < groupDetails.length; ++i) {
		var thisExpires = null;
		if ('expires' in groupDetails[i]) {
			if (Number.isInteger(groupDetails[i].expires)) thisExpires = groupDetails[i].expires;
			else if (groupDetails[i].expires == 'trial') thisExpires = nowTimestamp;
		}

		if ((thisExpires != null) && (thisExpires >= minTimestamp) && (thisExpires <= maxTimestamp)) {
			expiries.push(thisExpires);
		}
	}
	if (expiries.length == 0) expiries = [nowTimestamp];

	expiries.sort();
	var expiry_timestamp = expiries[Math.floor(expiries.length / 2)];
	var baseExpiry = dayjs.unix(expiry_timestamp);
	var newExpiry = baseExpiry.add(1, 'year');
	
	if (userRole == 'admin') {
		if ($('#renewal_expiry').val().length == 0) {
			$('#renewal_expiry').val(newExpiry.format('MMMM D, YYYY'));
		} else newExpiry = dayjs($('#renewal_expiry').val());
	} else $('#payment_expires').html(newExpiry.format('MMMM D, YYYY'));

	var milestones = [];
	var thisMilestone = newExpiry.subtract(45, 'day');
	var now = dayjs();
	do {
		milestones.push(thisMilestone.unix());
		thisMilestone = thisMilestone.subtract(3, 'month');
	} while (thisMilestone.diff(now) > 0);

	// Sort member list

	groupDetails.sort(function(a, b) {
		if (a.is_admin > b.is_admin) return -1;
		else if (a.is_admin < b.is_admin) return 1;
		else if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
		else if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
		else return 0;
	});

	// Build member list

	var newHTML = "";

	var hasAdminHeader = false;
	var hasMemberHeader = false;
	var adminCount = 0;

	var timeRequired = 0;
	var paidCount = 0;

    var prepaidCount = parseInt($('#prepaid_count').val());
    if (prepaidCount > 0) {
        var needsPayment = false;
        for (var j = 0; j < milestones.length; ++j) {
            if (milestones[j] > nowTimestamp) {
                needsPayment = true;
                timeRequired += 0.25 * prepaidCount;
            }
        }
        if (needsPayment) paidCount += prepaidCount;
    }

	for (var i = 0; i < groupDetails.length; ++i) {
		var thisExpires = null;
		if ('expires' in groupDetails[i]) {
			if (Number.isInteger(groupDetails[i].expires)) thisExpires = groupDetails[i].expires;
			else if (groupDetails[i].expires == 'trial') thisExpires = nowTimestamp;
		}

		if (thisExpires != null) {	
			var needsPayment = false;
			for (var j = 0; j < milestones.length; ++j) {
				if (milestones[j] > thisExpires) {
					needsPayment = true;
					timeRequired += 0.25;
				}
			}
			if (needsPayment) paidCount++;
		}

		if ((groupDetails.length > 1) && (groupDetails[i].is_admin == 1) && !hasAdminHeader) {
			newHTML += '<tr><td span="3" style="font-weight:bold;">Group administrators</td></tr>';
			hasAdminHeader = true;
		} else if ((groupDetails[i].is_admin == 0) && !hasMemberHeader) {
			newHTML += '<tr><td span="3" style="font-weight:bold;">Other members</td></tr>';
			hasMemberHeader = true;
		}

		newHTML += '<tr'
		if ('id' in groupDetails[i]) {
			newHTML += ' data-user-id="' + groupDetails[i].id + '"';
		}
		newHTML += '>';
		newHTML += '<td class="renew_name">' + groupDetails[i].name + '</td>';
		newHTML += '<td class="renew_email">' + groupDetails[i].email + '</td>';

		newHTML += '<td class="renew_expires">';
		if (!('id' in groupDetails[i])) newHTML += '<i>New user</i>';
		else if ('expires' in groupDetails[i]) {
			if (Number.isInteger(groupDetails[i].expires)) {
				newHTML += dayjs.unix(groupDetails[i].expires).format('MMMM D, YYYY');
			} else if (groupDetails[i].expires == 'trial') {
				newHTML += '<i>Trial account</i>';
			} else if (groupDetails[i].expires == 'none') {
				newHTML += '<i>No expiry</i>';
			}
		} else newHTML += '<i>No expiry</i>';
		newHTML += '</td>';

		newHTML += '<td class="renew_actions">';
		newHTML += '<div class="wait_icon_actions"></div>';
		if (groupDetails[i].is_admin == 0) {
			newHTML += '<button title="Make admin" class="actions_button make_admin arrow_up_icon"' +
				(('id' in groupDetails[i]) ? '' : ' disabled') + '></button>';
			newHTML += '<button title="Remove member" class="actions_button remove_member delete_icon"></button>';
		} else {
			newHTML += '<button title="Remove as admin" class="actions_button remove_admin arrow_down_icon"></button>';
			newHTML += '<button title="Remove member" class="actions_button remove_member delete_icon" disabled></button>';
			adminCount++;
		}
		newHTML += '</td>';
		newHTML += '</tr>';
	}

	if (groupDetails.length == 0) {
		newHTML += '<tr>';
		newHTML += '<td colspan=4 style="font-style:italic;">No members in group. Click "Add users" to renew existing users.</td>';
		newHTML += '</tr>';
    }

    var totalTeachers = groupDetails.length + prepaidCount;

    if (totalTeachers == 0) {
		$('#renew_button').prop('disabled', true);
	} else if ($('#billing_type').val() == 'select') {
		$('#renew_button').prop('disabled', true);
	} else if (userRole == 'admin') {
		$('#renew_button').prop('disabled', false);
	} else {
		$('#renew_button').prop('disabled', (timeRequired == 0));
	}

	$('#userData').html(newHTML);

	if (adminCount == 1) {
		$('.remove_admin').attr('disabled', 'disabled');
	}

	// Build payment information

	var rate;
	if (userRole == 'admin') {
		if ($('#billing_type').val() == 'select') {
			$('.pricing_required').hide();
			$('#pricing_warning').show();
			rate = pricing['items']['renewal-individual-1yr']['amount'];
			$('#payment_rate_type').html('rate not selected');	
		} else {
			$('.pricing_required').show();
			$('#pricing_warning').hide();
			rate = pricing['items'][$('#billing_type').val()]['amount'];
			$('#payment_rate_type').html('defined above');	
		}
	} else if (paidCount < 10) {
		$('#billing_type').val('renewal-individual-1yr');
		rate = pricing['items']['renewal-individual-1yr']['amount'];
		$('#payment_rate_type').html('fewer than 10 teachers paid');
	} else {
		$('#billing_type').val('renewal-group-1yr');
		rate = pricing['items']['renewal-group-1yr']['amount'];
		$('#payment_rate_type').html('10 teachers or more paid');
	}
	var total = rate * timeRequired;
    
	$('#payment_size').html(totalTeachers);
	$('#payment_size_suffix').html((totalTeachers == 1) ? '' : 's');
	if (paidCount < totalTeachers) {
		$('#payment_unpaid').html('(' + (totalTeachers - paidCount) + ' at no charge)');
		$('#payment_unpaid').show();
	} else $('#payment_unpaid').hide();

	$('#payment_rate').html(rate.toFixed(0));
	$('#payment_units').html(timeRequired.toFixed(2));
	$('#payment_total').html(total.toFixed(2));

	enableConfirmation();
}

function enableConfirmation() {
	window.onbeforeunload = function() {
		return true;
	};
}

$(document).ready(function() {

    RenewCommon.initialize({ finishAdd });

	$('#renew_button').on('click', function() {

		var validated = $('#renewal_form').validationEngine('validate');

		if ((userRole == 'admin') && ($('#billing_type').val() == 'select')) {
			$('#billing_type').validationEngine('showPrompt', '* Must choose a billing type', 'error', 'bottomLeft', true);					
			validated = false;
		}

		if (validated) {
			$('.payment_button').prop('disabled', true);
			$(this).siblings('.wait_icon').show();
	
			var user_ids = [];
			var new_users = [];
			for (var i = 0; i < groupDetails.length; ++i) {
				if ('id' in groupDetails[i]) {
					user_ids.push(groupDetails[i].id);
				} else {
					new_users.push({
						email: groupDetails[i].email,
						name: groupDetails[i].name
					});
				}
			}
	
			var group_admins = [];
			for (var i = 0; i < groupDetails.length; ++i) {
				if (groupDetails[i].is_admin == 1) group_admins.push(groupDetails[i].id);
			}
	
			var expires;
			if (userRole == 'admin') expires = dayjs($('#renewal_expiry').val());
			else expires = dayjs($('#payment_expires').html());

			var itemEntry = {
				school_name: $('#school_name').val(),
				group_name: $('#group_name').val(),
				admin_ids: group_admins,
				expires: expires.unix()
			};

            var prepaidCount = parseInt($('#prepaid_count').val());

			if (new_users.length > 0) itemEntry.new_users = new_users;
			if (user_ids.length > 0) itemEntry.user_ids = user_ids;
            if (prepaidCount > 0) itemEntry.new_prepaid = prepaidCount;
	
			if (userRole == 'admin') {
				itemEntry.type = $('#billing_type').val();
			}

			var purchase = {
				items: [itemEntry]
			};

			RenewCommon.startPayment(purchase, function(nextStep) {
				if (nextStep == 'getPayment') {
					if (userRole == 'admin') {
						$('#admin_confirmation_dialog').dialog('open');
					} else $('#payment_dialog').dialog('open');
				} else {
					$.gritter.add({
						title: "Success",
						text: "No renewal required",
						image: "/img/success.svg"
					});
				}
			});	
		}
	});

	$(document).on('click', '.make_admin', function() {
		var $row = $(this).closest('tr');

		var thisID = parseInt($row.attr('data-user-id'));
		for (var i = 0; i < groupDetails.length; ++i) {
			if (groupDetails[i].id == thisID) groupDetails[i].is_admin = 1;
		}

		$.gritter.add({
			title: "Success",
			text: "Group administrators changed",
			image: "/img/success.svg"
		});					

		rebuildMemberList();
	});

	$(document).on('click', '.remove_admin', function() {
		var $row = $(this).closest('tr');
		var thisID = parseInt($row.attr('data-user-id'));
		for (var i = 0; i < groupDetails.length; ++i) {
			if (groupDetails[i].id == thisID) groupDetails[i].is_admin = 0;
		}

		$.gritter.add({
			title: "Success",
			text: "Group administrators changed",
			image: "/img/success.svg"
		});					

		rebuildMemberList();
	});

	$(document).on('click', '.remove_member', function() {
		var deleteEmail = $(this).closest('tr').find('.renew_email').text();
		for (var i = 0; i < groupDetails.length; ++i) {
			if (groupDetails[i].email == deleteEmail) {
				groupDetails.splice(i, 1);
				break;
			}
		}
		
		$.gritter.add({
			title: "Success",
			text: "Member removed",
			image: "/img/success.svg"
		});					

		rebuildMemberList();
	});

	$('#billing_type').on('change', function() {
		rebuildMemberList();
	});

	$('input').on('input', enableConfirmation);

    $('#prepaid_count').spinner({
	    step: 1,
	    min: 0
    });
    $('#prepaid_count').val(0);

	$('#prepaid_count').on('keyup', rebuildMemberList);
    $('#prepaid_count').spinner({ stop: rebuildMemberList });

	$('#renewal_expiry').datepicker({
		dateFormat: "MM d, yy",
		onSelect: function(d, i) {
			rebuildMemberList();
	   }
	});

    $('form').validationEngine({
		validationEventTrigger: 'submit',
		promptPosition : "bottomLeft"
  	}).submit(function(e){ e.preventDefault() });

	window.onbeforeunload = null;

	rebuildMemberList();	
});

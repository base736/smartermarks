/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenewCommon from '/js/common/renewal.js?7.0';

$(document).ready(function() {

    RenewCommon.initialize();

	$('#renew_individual').on('click', function() {
		$('.payment_button').prop('disabled', true);
		$(this).siblings('.wait_icon').show();

		var purchase = {
			items: [
				{
					user_ids: [userID]
				}
			]
		};

		RenewCommon.startPayment(purchase, function(nextStep) {
			if (nextStep == 'getPayment') {
				$('#payment_dialog').dialog('open');
			} else {
				$.gritter.add({
					title: "Success",
					text: "No renewal required",
					image: "/img/success.svg"
				});							
			}
		});
	});

    $('form').validationEngine({
		validationEventTrigger: 'submit',
		promptPosition : "bottomLeft"
   }).submit(function(e){ e.preventDefault() });
});

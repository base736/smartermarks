/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { ImageUpload } from '/js/ImageUpload/ImageUpload.js?7.0';

$(document).ready(function() {

	for (var key in jsRegions) $('#user_tz_region').append('<option value="' + key + '">' + jsRegions[key] + '</option>');

	function initialize() {

		/**************************************************************************************************/
		/* Draw admin settings                                                                            */
		/**************************************************************************************************/
		
		if (authRole == 'admin') {

			if (userData.created != null) {
				var date = dayjs.utc(userData.created).local();
				$('#user_created').html(date.format('MMMM D, YYYY'));	
			}

			if (userData.expires != null) {
				var date = dayjs.utc(userData.expires).local();
				$('#expires_label').html('Account expires ' + date.format('MMMM D, YYYY'));
				$('#user_has_expiry').prop('checked', true);
				$('#user_expires').datepicker('setDate', date.format('MMMM D, YYYY'));
			} else {
				$('#expires_label').html('No expiry date for this account');
				$('#user_has_expiry').prop('checked', false);
			}
	
			if (userData.locked != null) {
				var date = dayjs.utc(userData.locked).local();
				$('#user_is_locked').prop('checked', true);
				$('#user_locked').datepicker('setDate', date.format('MMMM D, YYYY'));
			} else {
				$('#user_is_locked').prop('checked', false);
			}
	
			formatExpiryFields();
	
			$('#user_message').val(userData.message);
			$('#user_notes').val(userData.notes);
		}

		/**************************************************************************************************/
		/* Draw account settings                                                                          */
		/**************************************************************************************************/

		if ((userData.role == 'admin') && ($('#admin_role').length == 0)) {
			$('#user_role').append('<option id="admin_role" value="admin">SmarterMarks Admin</option>');
		} else $('#admin_role').remove();
		$('#user_role').val(userData.role);		

		$('#user_name').val(userData.name);
		$('#user_school').val(userData.school);
		$('#user_email').val(userData.email);
	
		var tzRegion = userData.timezone.split('/')[0];
		$('#user_tz_region').val(tzRegion);
		
		updateCities();
		$('#user_tz_city').val(userData.timezone);

		$('#open_created').prop('checked', userData.open_created == 1);

		/**************************************************************************************************/
		/* Draw remaining settings                                                                        */
		/**************************************************************************************************/

		var defaults = JSON.parse(userData.defaults);

		$('.loadedField').each(function() {
			var idParts = $(this).attr('id').split('-');
			var parent = defaults;
			for (var i = 1; i < idParts.length; ++i) {
				if (idParts[i] in parent) parent = parent[idParts[i]];
				else alert("Missing field in " + $(this).attr('id'));
			}

			if ($(this).is('.loadedBoolean')) $(this).prop('checked', parent);
			else if ($(this).is('.loadedJSON')) $(this).val(JSON.stringify(parent));
			else $(this).val(parent);
		});

		if ($('#defaults-Common-Title-logo-imageHash').length > 0) {
			if (defaults.Common.Title.logo.imageHash === false) {
				$('#defaults-Common-Title-logo-imageHash').val('');
				$('#include_logo').prop('checked', false);
				$('#logo_image_wrapper').html('');
				$('#height_wrapper').hide();
			} else {
				$('#defaults-Common-Title-logo-imageHash').val(defaults.Common.Title.logo.imageHash);
				$('#include_logo').prop('checked', true);
				$('#logo_image_wrapper').html('<img style="height:70px;" src="' + 
					userdataPrefix + $('#defaults-Common-Title-logo-imageHash').val() + '" />');
				$('#height_wrapper').show();
			}
		}
		
		$('.defaults-Document-Sections-columnCount').val(defaults.Document.Sections.columnCount);

		var fudgeFactor = parseFloat($('#defaults-Question-TypeDetails-nr-fudgeFactor').val());
		if (isNaN(fudgeFactor) || (fudgeFactor == 0.0)) {
			$('#allowSmallErrors').prop('checked',false);
			$('#fudgeFactorWrapper').hide();
		} else {
			$('#allowSmallErrors').prop('checked',true);
			$('#fudgeFactorWrapper').show();
		}

		var tensValue = parseFloat($('#defaults-Question-TypeDetails-nr-tensValue').val());
		if (isNaN(tensValue) || (tensValue == 0.0)) {
			$('#allowFactorOfTen').prop('checked',false);
			$('#tensValueWrapper').hide();
		} else {
			$('#allowFactorOfTen').prop('checked',true);
			$('#tensValueWrapper').show();
		}

		if (($('#defaults-Question-TypeDetails-nr-sigDigsBehaviour').val() == 'Zeros') || 
			($('#defaults-Question-TypeDetails-nr-sigDigsBehaviour').val() == 'Round')) {
			$('#sigDigsValueWrapper').show();
		} else {
			$('#sigDigsValueWrapper').hide();
		}

		var partialValue = parseFloat($('#defaults-Question-TypeDetails-nr-partialValue').val());
		if (isNaN(partialValue) || (partialValue == 0.0)) {
			$('#allowPartialMatch').prop('checked',false);
			$('#partialValueWrapper').hide();
		} else {
			$('#allowPartialMatch').prop('checked',true);
			$('#partialValueWrapper').show();
		}

		rebuildProficiencyLevels();
		rebuildNameLines();
		rebuildWRCriteria();
		updateFormatInputs();

		// Show fields as appropriate

		applyUserRole();

		document.activeElement.blur();
	}
	
	function formatExpiryFields() {
		if ($('#user_has_expiry').prop('checked')) {
			$('#user_is_locked').removeAttr('disabled');
			$('#user_expires').show();
		} else {
			$('#user_is_locked').prop('checked', false).attr('disabled', 'disabled');
			$('#user_expires').val('').hide();
		}

		if ($('#user_is_locked').prop('checked')) {
			$('#user_locked').show();
		} else {
			$('#user_locked').val('').hide();
		}
	}

	$('#user_has_expiry').on('click', formatExpiryFields);
	$('#user_is_locked').on('click', formatExpiryFields);

	function updateCities() {
		var region = $('#user_tz_region').val();
		var timezone = false;
	
		$('#user_tz_city').empty();
		for (var key in jsCities[region]) {
			if (timezone === false) timezone = key;
			$('#user_tz_city').append('<option value="' + key + '">' + jsCities[region][key] + '</option>')
		}
		$('#user_tz_city').val(key);
	}
	
	$('#user_tz_region').on('change', updateCities);

	$('#allowPartialMatch').on('click', function() {
		if ($('#allowPartialMatch').prop('checked') == true) {
			$('#partialValueWrapper').show();
			$('#defaults-Question-TypeDetails-nr-partialValue').val('0.5');
		} else {
			$('#partialValueWrapper').hide();
			$('#defaults-Question-TypeDetails-nr-partialValue').val('0.0');
		}
	});

	$('#allowSmallErrors').on('click', function() {
		if ($('#allowSmallErrors').prop('checked') == true) {
			$('#fudgeFactorWrapper').show();
			$('#defaults-Question-TypeDetails-nr-fudgeFactor').val('1');
		} else {
			$('#fudgeFactorWrapper').hide();
			$('#defaults-Question-TypeDetails-nr-fudgeFactor').val('0');
		}
	});

	$('#allowFactorOfTen').on('click', function() {
		if ($('#allowFactorOfTen').prop('checked') == true) {
			$('#tensValueWrapper').show();
			$('#defaults-Question-TypeDetails-nr-tensValue').val('0.5');
		} else {
			$('#tensValueWrapper').hide();
			$('#defaults-Question-TypeDetails-nr-tensValue').val('0.0');
		}
	});

	$('#defaults-Question-TypeDetails-nr-sigDigsBehaviour').on('change', function() {
		if (($('#defaults-Question-TypeDetails-nr-sigDigsBehaviour').val() == 'Zeros') || 
			($('#defaults-Question-TypeDetails-nr-sigDigsBehaviour').val() == 'Round')) {
			$('#sigDigsValueWrapper').show();
			$('#defaults-Question-TypeDetails-nr-sigDigsValue').val('1.0');
		} else {
			$('#sigDigsValueWrapper').hide();
			$('#defaults-Question-TypeDetails-nr-sigDigsValue').val('0.0');
		}
	});

	$('#include_logo').on('click', function(e) {
		if ($('#include_logo').prop('checked')) {
			$('#logo_target').val(uniqid());
			$('#logo_extension').val('');
			$("#logoFilename").html('No file selected');
			$("#progressbar").css('display', 'none');

			$("#addLogo_form").next().find("button:contains('Upload')").attr('disabled', true);
			$('#addLogo_form').dialog('open');
		} else {
			$('#defaults-Common-Title-logo-imageHash').val('');
			$('#logo_image_wrapper').html('');
			$('#height_wrapper').hide();
		}
	});

	$(document).on('click', '.wrCriteriaAdd', function() {
		rebuildWRCriteriaField();
		var index = $(this).closest('.options_row').index() + 1;
		var criteria = JSON.parse($('#defaults-Question-TypeDetails-wr-criteria').val());
		criteria.splice(index, 0, {label: '', value: 1});
		$('#defaults-Question-TypeDetails-wr-criteria').val(JSON.stringify(criteria));
   	    rebuildWRCriteria();
		$('#criteria_label_' + index).focus();
    });
    
	$(document).on('click', '.wrCriteriaDelete', function() {
		rebuildWRCriteriaField();
		var index = $(this).closest('.options_row').index();
		var criteria = JSON.parse($('#defaults-Question-TypeDetails-wr-criteria').val());
		criteria.splice(index, 1);
		$('#defaults-Question-TypeDetails-wr-criteria').val(JSON.stringify(criteria));
   	    rebuildWRCriteria();
    });

    var rebuildWRCriteriaField = function() {
		var criteria = [];

		var j = 0;
		while ($('#criteria_value_' + j).length > 0) {
			criteria.push({
				label: $('#criteria_label_' + j).val(),
				value: parseInt($('#criteria_value_' + j).val())
			});
			++j;
		}

		$('#defaults-Question-TypeDetails-wr-criteria').val(JSON.stringify(criteria));
    }
    
    var rebuildWRCriteria = function() {    
		var criteria = JSON.parse($('#defaults-Question-TypeDetails-wr-criteria').val());

	    var add_to = "";
		for (var j = 0; j < criteria.length; ++j) {
			add_to += '<div class="options_row">'
			add_to += '<input class="wr_criterion defaultTextBox2';
			add_to += '" style="width:110px; margin-right:10px;" type="text" id="criteria_label_' + j + '" value="' + criteria[j].label + '"/>';
			add_to += '<input data-prompt-position="centerLeft" class="validate[required,custom[isPosInteger],min[0],max[9]] defaultTextBox2 intSpinner" style="width:30px;" maxlength="1" type="text" id="criteria_value_' + j + '" value="' + criteria[j].value + '"/>';
			add_to += '<button title="Add" class="actions_button add_icon wrCriteriaAdd" style="margin-left:15px;">&nbsp;</button>';
			add_to += '<button title="Delete" class="actions_button delete_icon wrCriteriaDelete" ' + ((criteria.length == 1) ? 'disabled ' : '') + '>&nbsp;</button>';
			add_to += '</div>';
		}
		$('#criteriaWrapper').html(add_to);

		$('#criteriaWrapper .intSpinner').spinner({
			step: 1,
			min: 1
		});
    }
	
	$('.defaults-Document-Sections-columnCount').on('change', function(e,ui){
	    var $value = $(this).val();
	    $('.defaults-Document-Sections-columnCount').val($value);
    });

	let logoImageUpload = null;
	let logoFile = null;

	$('#logoUpload').on('change', function(e) {
		const files = e.target.files;
		const $uploadButton = $("#addLogo_form").parent().find("button:contains('Upload')");

		if (!files || !files[0]) {

			// No file chosen
			logoFile = null;
			$('#logoFilename').text('No file selected');
			$uploadButton.attr('disabled', 'disabled');

		} else {

			// Store the file, enable “Upload” button
			logoFile = files[0];
			$('#logoFilename').text(logoFile.name);
			$uploadButton.removeAttr('disabled');

		}
	});

	var logoAdd = function() {
		if (logoImageUpload) {

			return false;

		} else if (logoFile) {

			// Check file size
			if (logoFile.size > 10000000) {
				$('#logoFilename').validationEngine(
					'showPrompt',
					'* Images must be smaller than 10 MB',
					'error',
					'bottomLeft',
					true
				);
				return;
			}

			// Start the upload

			const $uploadButton = $("#addLogo_form").parent().find("button:contains('Upload')");
			$uploadButton.attr('disabled', 'disabled');

			$('#progressbar').progressbar('value', 0).show();
			$('#logoUpload').attr('disabled', 'disabled');
			$('#select_button').attr('disabled', true);

			logoImageUpload = new ImageUpload(logoFile, (percent) => {
				$('#progressbar').progressbar('value', percent);
			});

			logoImageUpload.start()
			.then((responseData) => {

				$uploadButton.removeAttr('disabled');
				$('#logoUpload').removeAttr('disabled');
				$('#select_button').removeAttr('disabled');
				$('#progressbar').hide();

				logoImageUpload = null;
				logoFile = null;

				$('#defaults-Common-Title-logo-imageHash').val(responseData.imageHash);
				$('#logo_image_wrapper').html(`<img style="height:70px;" src="${responseData.imageURL}" />`);
				$('#height_wrapper').show();

				$('#addLogo_form').dialog("close");

			}).catch((err) => {

				if (err && err.message == 'Upload cancelled') {
					$.gritter.add({
						title: "Success",
						text: "Image upload cancelled.",
						image: "/img/success.svg"
					});	
				} else {
					showSubmitError();
				}

				// Reset UI
				$uploadButton.removeAttr('disabled');
				$('#logoUpload').removeAttr('disabled');
				$('#select_button').removeAttr('disabled');
				$('#progressbar').hide();

				logoImageUpload = null;
			});
		}
	}

	$('#addLogo_form').dialog({
		autoOpen: false,
		width:500,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
			'Upload': logoAdd,
            'Cancel': function() {
				if (logoImageUpload) {
					logoImageUpload.abort();
					logoImageUpload = null;	
				} else {
					$('#include_logo').prop('checked', false);
					$(this).dialog("close"); 
				}	
            }
		}, open: function(event) {
		 	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$('.ui-dialog-buttonpane').find('button:contains("Upload")').addClass('btn btn-primary btn_font');
			initSubmitProgress($(this));
		}, close: function(){
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		},
        progressall: function (e, data) {
			$("#progressbar").progressbar("value", Math.round(data.loaded / data.total * 100));
        }
    });

	$(document).on('click', '.namelineAdd', function() {
		if ($(this).attr('data-index') == -1) {
			$('#defaults-Common-Title-nameLines').val(JSON.stringify(['Name']));
		} else {
			rebuildNameLinesField();
			var index = parseInt($(this).attr('data-index')) + 1;
			var nameLines = JSON.parse($('#defaults-Common-Title-nameLines').val());
			nameLines.splice(index, 0, "");
			$('#defaults-Common-Title-nameLines').val(JSON.stringify(nameLines));
		}
		rebuildNameLines();
    });
    
	$(document).on('click', '.namelineDelete', function() {
		rebuildNameLinesField();
		var index = parseInt($(this).attr('data-index'));
		var nameLines = JSON.parse($('#defaults-Common-Title-nameLines').val());
		nameLines.splice(index, 1);
		$('#defaults-Common-Title-nameLines').val(JSON.stringify(nameLines));
   	    rebuildNameLines();
    });

	function rebuildNameLinesField() {
		var nameLines = [];
		var i = 0;
		while ($('#nameline_' + i).length > 0) {
			nameLines.push($('#nameline_' + i).val());
			++i;
		}
		$('#defaults-Common-Title-nameLines').val(JSON.stringify(nameLines));
	}

	function rebuildNameLines() {
		var nameLines = JSON.parse($('#defaults-Common-Title-nameLines').val());

		var add_to = "";
		if (nameLines.length == 0) {
			add_to += '<div class="nameLines_row" style="padding-bottom:5px;">';
			add_to += '<input class="defaultTextBox2" style="width:200px !important; margin-bottom:0px;" type="text" value="No name lines shown" disabled />';
			add_to += '<button title="Add" class="actions_button add_icon namelineAdd" style="margin-left:5px;" data-index=-1>&nbsp;</button>';
			add_to += '<button title="Delete" class="actions_button delete_icon namelineDelete" data-index=-1 disabled>&nbsp;</button>';
			add_to += '</div>';
		} else {
			for (var i = 0; i < nameLines.length; ++i) {
				add_to += '<div class="nameLines_row" style="padding-bottom:5px;">';
				add_to += '<input id="nameline_' + i + '" class="validate[required] defaultTextBox2" style="width:200px !important; margin-bottom:0px;" type="text" value="' + nameLines[i] + '" />';
				add_to += '<button title="Add" class="actions_button add_icon namelineAdd" style="margin-left:5px;" data-index=' + i + '>&nbsp;</button>';
				add_to += '<button title="Delete" class="actions_button delete_icon namelineDelete" data-index=' + i + '>&nbsp;</button>';
				add_to += '</div>';
			}
		}
		$('#nameLines_wrapper').html(add_to);

		$('#editDocument_wrapper_left').height(35 * nameLines.length + 380);
		$('#editDocument_wrapper_right').height(35 * nameLines.length + 405);
	}

	$(document).on('click', '.proficiencyLevelAdd', function() {
		rebuildProficiencyLevelsField();
		var index = parseInt($(this).attr('data-index')) + 1;
		var proficiencyLevels = JSON.parse($('#defaults-Document-Reports-Student-outcomesLevels').val());
		proficiencyLevels.splice(index, 0, {name:'',from:0});
		$('#defaults-Document-Reports-Student-outcomesLevels').val(JSON.stringify(proficiencyLevels));
		rebuildProficiencyLevels();
    });
    
	$(document).on('click', '.proficiencyLevelDelete', function() {
		rebuildProficiencyLevelsField();
		var index = parseInt($(this).attr('data-index'));
		var proficiencyLevels = JSON.parse($('#defaults-Document-Reports-Student-outcomesLevels').val());
		proficiencyLevels.splice(index, 1);
		$('#defaults-Document-Reports-Student-outcomesLevels').val(JSON.stringify(proficiencyLevels));
   	    rebuildProficiencyLevels();
    });

	function rebuildProficiencyLevelsField() {
		var newData = [];
		var i = 0;
		while ($('#user_levelname_' + i).length > 0) {
			var thisName = $('#user_levelname_' + i).val();
			var thisFrom = parseInt($('#user_levelfrom_' + i).val());
			newData.push({name:thisName,from:thisFrom});
			i++;
		}
		if (newData.length == 0) $('#defaults-Document-Reports-Student-outcomesLevels').val('');
		else $('#defaults-Document-Reports-Student-outcomesLevels').val(JSON.stringify(newData));
	}

	function rebuildProficiencyLevels() {
		var proficiencyLevels = JSON.parse($('#defaults-Document-Reports-Student-outcomesLevels').val());
		
		var levelsHTML = "";
		for (var i = 0; i < proficiencyLevels.length; ++i) {
			var thisName = proficiencyLevels[i].name;
			var thisFrom = proficiencyLevels[i].from;
			levelsHTML += '<div style="padding-bottom:5px;">';
			levelsHTML += '<input id="user_levelname_' + i + '" class="defaultTextBox2" style="width:125px !important; margin-bottom:0px; margin-right:10px;" type="text" value="' + thisName + '" />';
			levelsHTML += '<input id="user_levelfrom_' + i + '" class="defaultTextBox2 intSpinner" data-prompt-position="bottomRight" style="width:20px !important; margin-bottom:0px;" type="text" value="' + thisFrom + '" />';
			levelsHTML += '<button title="Add" class="actions_button add_icon proficiencyLevelAdd" style="margin-left:15px;" data-index=' + i + '>&nbsp;</button>';
			levelsHTML += '<button title="Delete" class="actions_button delete_icon proficiencyLevelDelete" ' + ((i == 0) ? 'disabled ' : '') + ' data-index=' + i + '>&nbsp;</button>';
			levelsHTML += '</div>';
		}
		$('#user_proficiencyLevels_wrapper').html(levelsHTML);

		$('#user_proficiencyLevels_wrapper .intSpinner').spinner({
			step: 5,
			min: 0,
			max: 95
		});
		
		$('#user_levelfrom_0').spinner('disable');
	}
	
	function roleUpdated() {
		$('#userName, #userSchool').removeClass();
		if ($('#role').val() == 'student') {
			$('#userName').addClass('validate[custom[onlyNameCharacters]]');
		} else {
			$('#userName').addClass('validate[required,custom[onlyNameCharacters]]');
			$('#userSchool').addClass('validate[required]');
		}
		$('#userName, #userSchool').addClass('defaultTextBox2');

		if (($('#role').val() == 'school') || ($('#role').val() == 'admin')) {
			$('#hasExpiry, #hasLocked').prop('checked', false);
			$('#hasExpiry, #hasLocked').attr('disabled', 'disabled');
		} else $('#hasExpiry, #hasLocked').removeAttr('disabled');
		formatExpiryFields();
	}
	
	if ($('#role').val() == 'admin') {
		$('#role').attr('disabled', 'disabled');
	} else $('#role').find('option[value="admin"]').remove();

	$('#role').on('change', roleUpdated);
	roleUpdated();	
	
	function enableConfirmation() {
		window.onbeforeunload = function() {
			return true;
		};
	}

	$('input').on('input', enableConfirmation);
	$('select').on('change', enableConfirmation);
	$(document).on('click', '.add_icon, .delete_icon', enableConfirmation);

	function checkProficiencyLevels() {
		var isValid = true;

		var $target = $('#user_levelname_0');

		var i = 0;
		while ($('#user_levelname_' + i).length > 0) {
			if ($('#user_levelname_' + i).val().length == 0) {
				$target.validationEngine('showPrompt', '* These fields are required', 'error', 'topLeft', true);
				isValid = false;
			}
			for (var j = 0; j < i; ++j) {
				if ($('#user_levelname_' + i).val() == $('#user_levelname_' + j).val()) {
					$target.validationEngine('showPrompt', '* Names must be unique', 'error', 'topLeft', true);
					isValid = false;
				}
			}
			i++;
		}

		var i = 1;
		if ($('#user_levelfrom_0').val().length == 0) {
			$target.validationEngine('showPrompt', '* These fields are required', 'error', 'topLeft', true);
			isValid = false;
		}
		var lastFrom = parseInt($('#user_levelfrom_0').val());
		if (isNaN($('#user_levelfrom_0').val()) || (lastFrom < 0) || (lastFrom >= 100)) {
			$target.validationEngine('showPrompt', '* Levels must be between 0 and 100', 'error', 'topLeft', true);
			isValid = false;
		}
		while ($('#user_levelfrom_' + i).length > 0) {
			if ($('#user_levelfrom_' + i).val().length == 0) {
				$target.validationEngine('showPrompt', '* These fields are required', 'error', 'topLeft', true);
				isValid = false;
			}
			var thisFrom = parseInt($('#user_levelfrom_' + i).val());
			if (isNaN($('#user_levelfrom_' + i).val()) || (thisFrom < 0) || (thisFrom >= 100)) {
				$target.validationEngine('showPrompt', '* Levels must be between 0 and 100', 'error', 'topLeft', true);
				isValid = false;
			}
			if (thisFrom <= lastFrom) {
				$target.validationEngine('showPrompt', '* Levels must be in increasing order', 'error', 'topLeft', true);
				isValid = false;
			}
			lastFrom = thisFrom;
			i++;
		}

		return isValid;
	}

	function saveUserData() {

		// Save assessment

		var data_string = JSON.stringify(userData);

		var postData = {
			data_string: data_string,
			data_hash: SparkMD5.hash(data_string)
		};

		return ajaxFetch('/Users/save/' + userData.id, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(postData)
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				return responseData;
			} else if ('message' in responseData) {
				throw new Error(responseData.message);
			} else {
				throw new Error("Error saving user data.");
			}

		}).catch(function() {
			reject(false);
		});
	}

	$('#settingsSave').on('click', function() {
		var $form = $('#userEdit_form');
		
		$form.validationEngine('hideAll');
		if (checkProficiencyLevels() && $form.validationEngine('validate')) {

			/**************************************************************************************************/
			/* Update admin settings                                                                          */
			/**************************************************************************************************/

			if (authRole == 'admin') {
				
				userData.role = $('#user_role').val();

				if ($('#user_has_expiry').prop('checked')) {
					var expires = dayjs($('#user_expires').datepicker('getDate')).utc();
					userData.expires = expires.format('YYYY-MM-DD HH:mm:ss');
				} else userData.expires = null;

				if ($('#user_is_locked').prop('checked')) {
					var locked = dayjs($('#user_locked').datepicker('getDate')).utc();
					userData.locked = locked.format('YYYY-MM-DD HH:mm:ss');
				} else userData.locked = null;

				userData.message = $('#user_message').val();
				userData.notes = $('#user_notes').val();
			}

			/**************************************************************************************************/
			/* Update account settings                                                                        */
			/**************************************************************************************************/

			userData.name = $('#user_name').val();
			userData.school = $('#user_school').val();
			userData.email = $('#user_email').val();
			userData.timezone = $('#user_tz_city').val();
			userData.open_created = $('#open_created').prop('checked') ? 1 : 0;

			/**************************************************************************************************/
			/* Update remaining settings                                                                      */
			/**************************************************************************************************/

			var defaults = JSON.parse(userData.defaults);

			rebuildProficiencyLevelsField();
			rebuildNameLinesField();
			rebuildWRCriteriaField();

			$('.loadedField').each(function() {
				var idParts = $(this).attr('id').split('-');

				var parent = defaults;
				idParts.shift();
				
				while (idParts.length > 1) {
					var nextKey = idParts.shift();
					if (nextKey in parent) parent = parent[nextKey];
					else alert("Missing field in " + $(this).attr('id'));
				}
				var nextKey = idParts[0];

				if ($(this).is('.loadedJSON')) {
					parent[nextKey] = JSON.parse($(this).val());
				} else if ($(this).is('.loadedText')) {
					parent[nextKey] = $(this).val();
				} else if ($(this).is('.loadedFloat')) {
					parent[nextKey] = parseFloat($(this).val());
				} else if ($(this).is('.loadedInteger')) {
					parent[nextKey] = parseInt($(this).val());
				} else if ($(this).is('.loadedBoolean')) {
					parent[nextKey] = $(this).prop('checked');				
				} else {
					alert("Bad type in " + $(this).attr('id'));
				}
			});

			if ($('#defaults-Common-Title-logo-imageHash').length > 0) {
				if ($('#defaults-Common-Title-logo-imageHash').val().length > 0) {
					defaults.Common.Title.logo.imageHash = $('#defaults-Common-Title-logo-imageHash').val();
				} else defaults.Common.Title.logo.imageHash = false;
			}

			var columnCount = $('.defaults-Document-Sections-columnCount').val();
			if (columnCount == 'auto') defaults.Document.Sections.columnCount = columnCount;
			else defaults.Document.Sections.columnCount = parseInt(columnCount);

			userData.defaults = JSON.stringify(defaults);

			/**************************************************************************************************/
			/* Save settings                                                                                  */
			/**************************************************************************************************/

			if (pendingAJAXPost) return false;
			showSubmitProgress($form);
			pendingAJAXPost = true;

			saveUserData()
			.then(function(response) {

				// Finish up

				hideSubmitProgress($form);

				var message = ('message' in response) ? response.message : "Settings saved.";

				addClientFlash({
					title: "Success",
					text: message,
					image: "/img/success.svg"
				});

				window.onbeforeunload = null;
				if ('redirect' in response) {
					window.location.href = response.redirect;
				} else window.location.reload();
			})
			.catch(function(message) {
				if (message !== false) {
					$('#user_email').validationEngine('showPrompt', '* ' + message, 'error', 'bottomLeft', true);
				} else showSubmitError();

				hideSubmitProgress($form);
			});
		}
	});

	function applyUserRole() {
		if ($('#user_role').val() == 'teacher') {
			$('.edit_wrapper_teacher').show();
		} else {
			$('.edit_wrapper_teacher').hide();
		}

		if ($('#user_role').val() == 'student') {
			$('#user_name, #user_school').removeClass('validate[required]');
		} else {
			$('#user_name, #user_school').addClass('validate[required]');
		}
	}

	$('#user_role').on('change', function() {
		applyUserRole();
	});

	$(".datepicker").datepicker({dateFormat: "MM d, yy"});

    $('.intSpinner').spinner({
	    step: 1,
		min: 1,
		stop: enableConfirmation
    });

    $('.marginSpinner').spinner({
	    step: 0.25,
        min: 0,
		stop: enableConfirmation
    });

	$('form').validationEngine({
		validationEventTrigger: 'submit',
		promptPosition : "bottomLeft",
		focusFirstField: false
	}).submit(function(e){ e.preventDefault() });

	function updateFormatInputs() {
        var sigDigsType = $('#defaults-Question-Engine-sigDigsType').val();
		if ((sigDigsType == 'SigDigs') || (sigDigsType == 'Scientific')) {
			var label = "sig dig";
			if ($('#defaults-Question-Engine-sigDigs').val() != 1) label += 's';
			$('#var_sdLabel').html(label);

			$('#defaults-Question-Engine-sigDigs').spinner('option', 'min', 1);
			$('#var_sd_wrapper').show();
		} else if (sigDigsType == 'Decimals') {
			var label = "decimal";
			if ($('#defaults-Question-Engine-sigDigs').val() != 1) label += 's';
			$('#var_sdLabel').html(label);

			$('#defaults-Question-Engine-sigDigs').spinner('option', 'min', 0);
			$('#var_sd_wrapper').show();
		} else {
			$('#var_sd_wrapper').hide();
		}
	}

	$('#defaults-Question-Engine-sigDigsType').on('change', function() {
        var sigDigsType = $('#defaults-Question-Engine-sigDigsType').val();
		if ((sigDigsType == 'SigDigs') || (sigDigsType == 'Scientific')) {
			$('#defaults-Question-Engine-sigDigs').val(3);
		} else if (sigDigsType == 'Decimals') {
			$('#defaults-Question-Engine-sigDigs').val(1);
		} else {
			$('#defaults-Question-Engine-sigDigs').val(0);
		}

		updateFormatInputs();
	});

	$('#defaults-Question-Engine-sigDigs').on('keyup', updateFormatInputs);
    $('#defaults-Question-Engine-sigDigs').spinner({ stop: updateFormatInputs });

    $("#progressbar").progressbar();

	initSubmitProgress($('#userEdit_form'));

	initialize();
});

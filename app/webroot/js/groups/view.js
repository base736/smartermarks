/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenewCommon from '/js/common/renewal.js?7.0';

var groupDetails = false;

var finishAdd = function(bufferedResult) {
	var new_users;
	if ('new_users' in bufferedResult) {
		new_users = bufferedResult.new_users;
	} else new_users = [];

	var user_ids = [];
	for (var i = 0; i < bufferedResult.details.length; ++i) {
		var exists = false;
		for (var j = 0; j < groupDetails.length; ++j) {
			if (groupDetails[j].id == bufferedResult.details[i].id) exists = true;
		}
		if (!exists) user_ids.push(bufferedResult.details[i].id);
	}
	
	if ((new_users.length > 0) || (user_ids.length > 0)) {
		var itemEntry = {
			group_id: groupID
		};

		if (new_users.length > 0) itemEntry.new_users = new_users;
		if (user_ids.length > 0) itemEntry.user_ids = user_ids;

		var purchase = {
			items: [itemEntry]
		};

		RenewCommon.startPayment(purchase, function(nextStep) {
			if (nextStep == 'getPayment') {
				if (userRole == 'admin') {
					$('#admin_confirmation_dialog').dialog('open');
				} else $('#payment_dialog').dialog('open');
			} else if (nextStep == 'getConfirmation') {
                $('#user_confirmation_dialog').dialog('open');
            } else reloadMembers();
		});
	} else {
		$.gritter.add({
			title: "Error",
			text: "No users to add",
			image: "/img/error.svg"
		});
	}
}

var reloadMembers = function() {
	$('#userData').html('<tr class="disabled"><td colspan="5"><div style="padding-left:10px;">' +
		'<div class="wait_icon"></div><i>Loading&hellip;</i></div></td></tr>');

	ajaxFetch('/Groups/get_members/' + groupID)
	.then(async response => {
		const responseData = await response.json();

		if (responseData.success) {
			groupDetails = responseData.members;
			rebuildMemberList();
		}
	});
}

var rebuildMemberList = function() {

	// Sort member list

	groupDetails.sort(function(a, b) {
		if (a.is_admin > b.is_admin) return -1;
		else if (a.is_admin < b.is_admin) return 1;
		else if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
		else if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
		else return 0;
	});		

	// Build member list

	var newHTML = "";

	var hasAdminHeader = false;
	var hasMemberHeader = false;
    var hasBounced = false;
	var adminCount = 0;
	var isAdmin = (userRole == 'admin');
	for (var i = 0; i < groupDetails.length; ++i) {
		if ((groupDetails[i].is_admin == 1) && (groupDetails[i].id == userID)) isAdmin = true;

		if ((groupDetails.length > 1) && (groupDetails[i].is_admin == 1) && !hasAdminHeader) {
			newHTML += '<tr><td span="3" style="font-weight:bold;">Group administrators</td></tr>';
			hasAdminHeader = true;
		} else if ((groupDetails[i].is_admin == 0) && !hasMemberHeader) {
			newHTML += '<tr><td span="3" style="font-weight:bold;">Other members</td></tr>';
			hasMemberHeader = true;
		}

		newHTML += '<tr data-user-id="' + groupDetails[i].id + '"';
        if (groupDetails[i].email_bounced == 1) {
            newHTML += ' class="bounced_email"';
            hasBounced = true;
        }
        newHTML += '>';
		newHTML += '<td class="renew_name">' + groupDetails[i].name + '</td>';
		newHTML += '<td class="renew_email">' + groupDetails[i].email + '</td>';
		newHTML += '<td>';
		newHTML += '<div class="wait_icon_actions"></div>';
		if (groupDetails[i].is_admin == 0) {
            if (groupDetails[i].email_bounced) {
                newHTML += '<button title="Re-send verification" class="actions_button send_verification email_icon" style="margin-right:3px;"></button>';
                newHTML += '<button title="Edit email" class="actions_button change_email edit_icon"></button>';
            } else {
                newHTML += '<button title="Make admin" class="actions_button make_admin arrow_up_icon"></button>';
            }
			newHTML += '<button title="Remove member" class="actions_button remove_member delete_icon"></button>';
		} else {
			newHTML += '<button title="Remove as admin" class="actions_button remove_admin arrow_down_icon"></button>';
			newHTML += '<button title="Remove member" class="actions_button remove_member delete_icon" disabled></button>';
			adminCount++;
		}
		newHTML += '</td>';
		newHTML += '</tr>';	
	}

    if (hasBounced) $('#email_bounced_notice').show();
    else $('#email_bounced_notice').hide();

	if (isAdmin && expires.isAfter(now)) $('#addUsers_button').show();
	else $('#addUsers_button').hide();

	$('#userData').html(newHTML);

	if (adminCount == 1) {
		$('.remove_admin').attr('disabled', 'disabled');
	}
}

$(document).ready(function() {

    RenewCommon.initialize({ finishAdd });

	function removeMember() {
		var $dialog = $(this);

	    if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/Groups/member_remove/' + groupID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				user_id: parseInt($('#removeMember_dialog').attr('data-member-id'))
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				var deleteID = parseInt($('#removeMember_dialog').attr('data-member-id'));
				
				for (var i = 0; i < groupDetails.length; ++i) {
					if (groupDetails[i].id == deleteID) {
						groupDetails.splice(i, 1);
						break;
					}
				}
						
				rebuildMemberList();
				$dialog.dialog('close');

				$.gritter.add({
					title: "Success",
					text: "Member removed from group",
					image: "/img/success.svg"
				});					
		
			} else showSubmitError($dialog);

		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$('#removeMember_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Remove': removeMember
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Remove")').addClass('btn btn-save btn_font');
		  	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$(document).on('click', '.remove_member', function() {
		var $row = $(this).closest('tr');
		$('#removeMember_dialog').attr('data-member-id', $row.attr('data-user-id'));
		$('#remove_user_email').html($row.find('.renew_email').html());

		$('#removeMember_dialog').dialog('open');
	});

	function changeAdmin() {
		var $dialog = $('#changeAdmin_dialog');

	    if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var memberID = parseInt($('#changeAdmin_dialog').attr('data-member-id'));
		var isAdmin = parseInt($('#changeAdmin_dialog').attr('data-is-admin'));

		ajaxFetch('/Groups/set_admin/' + groupID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				user_id: memberID,
				is_admin: isAdmin
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				for (var i = 0; i < groupDetails.length; ++i) {
					if (groupDetails[i].id == memberID) groupDetails[i].is_admin = isAdmin;
				}

				if ((memberID == userID) && (isAdmin == 0)) {
					window.location.replace('/');
				} else {
					rebuildMemberList();
					$dialog.dialog('close');

					$.gritter.add({
						title: "Success",
						text: "Group administrators changed",
						image: "/img/success.svg"
					});					
				}
		
			} else showSubmitError($dialog);

		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$('#changeAdmin_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': changeAdmin
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
		  	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$(document).on('click', '.make_admin', function() {
		var $row = $(this).closest('tr');
		var memberID = $row.attr('data-user-id');

		$('#changeAdmin_dialog').attr('data-member-id', memberID);
		$('#changeAdmin_dialog').attr('data-is-admin', 1);

		$row.find('.wait_icon_actions').css('opacity', 1);
		changeAdmin();
	});

	$(document).on('click', '.remove_admin', function() {
		var $row = $(this).closest('tr');
		var memberID = $row.attr('data-user-id');

		$('#changeAdmin_dialog').attr('data-member-id', memberID);
		$('#changeAdmin_dialog').attr('data-is-admin', 0);

		if ((memberID == userID) && (userRole != 'admin')) {
			$('#changeAdmin_dialog').dialog('open');				
		} else {
			$row.find('.wait_icon_actions').css('opacity', 1);
			changeAdmin();
		}
	});

	$('#renewGroup_button').click(function(e) {
		e.preventDefault();		
		window.location.href = "/Users/renew_group?g=" + groupID;
	});

	$(document).on('click', '.change_email', function(e) {
        var userID = $(this).closest('tr').attr('data-user-id');
        $('#editEmail_dialog').attr('data-user-id', userID);

        var email = $(this).closest('tr').find('.renew_email').text();
		$('#new_email').val(email);

		$('#editEmail_dialog').dialog('open');
	});
	
	var saveEmail = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')) {
            var userID = $dialog.attr('data-user-id');
			var newEmail = $('#new_email').val().trim();
		
			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			ajaxFetch('/Groups/member_change/' + groupID, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					user_id: userID,
					new_email: newEmail
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
    
                if (responseData.success) {

                    for (var i = 0; i < groupDetails.length; ++i) {
                        if (groupDetails[i].id == responseData.old_id) {
                            groupDetails[i].id = responseData.new_id;
                            groupDetails[i].email = responseData.new_email;
                            groupDetails[i].email_bounced = responseData.new_email_bounced;
                            break;
                        }
                    }

                    rebuildMemberList();
    
                    $.gritter.add({
                        title: "Success",
                        text: responseData.message,
                        image: "/img/success.svg"
                    });
                    $dialog.dialog('close');

                } else {

                    $.gritter.add({
                        title: "Error",
                        text: responseData.message,
                        image: "/img/error.svg"
                    });

                }
            }).catch(function() {
                showSubmitError($dialog);
            });

		}
	}

	$('#editEmail_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': saveEmail
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

    $(document).on('click', '.send_verification', function(e) {
        var userID = $(this).closest('tr').attr('data-user-id');
        $('#sendVerification_dialog').attr('data-user-id', userID);

        var email = $(this).closest('tr').find('.renew_email').text();        
		$('#verification_email').text(email);

		$('#sendVerification_dialog').dialog('open');
	});

	var sendVerification = function() {
		var $dialog = $(this);

        var userID = $dialog.attr('data-user-id');

	    if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/Users/resend_verification/' + userID, {
			method: 'POST'
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {

                for (var i = 0; i < groupDetails.length; ++i) {
                    if (groupDetails[i].id == userID) {
                        groupDetails[i].email_bounced = 0;
                        break;
                    }
                }

                rebuildMemberList();

				$.gritter.add({
					title: "Success",
					text: "Verification email sent.",
					image: "/img/success.svg"
				});
				$dialog.dialog('close');

			} else {

				showSubmitError($dialog);

			}
		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$('#sendVerification_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Send': sendVerification
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Send")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});	
    
    $('#expires_text').html(expires.format('MMMM D, YYYY'));

	reloadMembers();
});
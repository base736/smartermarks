/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as TableSelection from '/js/common/table_selection.js?7.0';

window.onload = function() {

// To reload items if we got here by hitting the back button while editing one 

    var e = document.getElementById("refreshed");
    if (e.value=="no") e.value="yes";
    else {
        e.value="no";
        window.location.reload();
    }
};
    
$(document).ready(function() {
	
    TableSelection.initialize();

	var sortType = 'Created';
	if (('Indices' in userDefaults) && ('sort' in userDefaults.Indices)) {
		if (userDefaults.Indices.sort == 'name') sortType = 'Name';
	}

	$('#sort_type option').each(function() {
		$(this).prop('selected', ($(this).text() == sortType));
	});

    $('#editGroup_button').click(function(e) {
		e.preventDefault();
		if ($(this).prop('disabled')) return;

		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var $tr = $(this).closest('tr');
				var $groupID = $tr.data('group-id');
				window.location = "/Groups/view/" + $groupID;
			}
		});
	});

    $(document).on('dblclick', '.index_table tbody tr', function(e) {
		e.preventDefault();
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		var $groupID = $(this).data('group-id');
		if (numChecked == 0) {
			window.location = "/Groups/view/" + $groupID;
		} else if (numChecked == 1) {
			var $selectedID = $selectedCheckbox.closest('tr').attr('data-group-id');
			if ($selectedID == $groupID) {
				window.location = "/Groups/view/" + $groupID;
			}
		}
	});

    $(document).on('click', '.header_sort', function() {
		var urlVars = getUrlVars();
		if (!('search' in urlVars)) {
			delete urlVars.page;
			urlVars.sort = $('#sort_type').val();
			urlVars.direction = $(this).attr('data-direction');
			var newQuery = jQuery.param(urlVars);
			window.location.href = window.location.href.split('?')[0] + "?" + newQuery;
		}
	});
	
	$(document).on('change', '#sort_type', function() {
		var urlVars = getUrlVars();
		delete urlVars.page;
		urlVars.sort = $('#sort_type').val();
		var $selectedOption = $('#sort_type option:selected');
		urlVars.direction = $selectedOption.attr('data-default-direction');
		var newQuery = jQuery.param(urlVars);
		window.location.href = window.location.href.split('?')[0] + "?" + newQuery;
	});
	
	$('.search_dropdown').on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var $menu = $(this).parent().find('.search_menu');
		if (!$menu.is(":visible")) {
			$menu.show();
			$menu.find('input').focus();
			$menu.find('input').select();
		} else $menu.hide();
	});

	var deleteGroup = function() {
		var $dialog = $(this);

		var selectedIds = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var itemID = $(this).closest('tr').attr('data-group-id');
				selectedIds.push(itemID);
			}
		});

    	if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/Groups/delete', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				itemIds: selectedIds,
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			var urlVars = getUrlVars();
			if ('search' in urlVars) {
				delete urlVars.search;
				var newQuery = jQuery.param(urlVars);
				window.location.href = window.location.href.split('?')[0] + "?" + newQuery;
			} else window.location.reload();
		});
	}
	
	$(document).bind('keydown', function (e) {
		if ($('.ui-dialog:visible').length == 0) {
	    	if (((e.key === 'Backspace') || (e.key === 'Delete')) && !$(e.target).is("input:not([readonly]), textarea")) {
				$('#deleteGroup_button').trigger("click");
			}
		}
	});

	$('#deleteGroup_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
				
		if (numChecked == 1) {
			var $groupName = $selectedCheckbox.closest('tr').find('.td_itemName').html();
			$('#deleteGroup_name').html($groupName);
			$('#deleteGroup_dialog').dialog('open');
		}
	});

	$('#deleteGroup_dialog').dialog({
        autoOpen: false,
		width: 400,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteGroup
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#renewGroup_button').click(function(e) {
		e.preventDefault();
		
		var groupID = '';
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				groupID = $(this).closest('tr').attr('data-group-id');
			}
		});

		if (groupID == '') window.location.href = "/Users/renew_group";
		else window.location.href = "/Users/renew_group?g=" + groupID;
	});

	function submitSearch() {
		var urlVars = getUrlVars();
		urlVars.search = $('#search_input').val().trim();
		var newQuery = jQuery.param(urlVars);
		window.location.href = window.location.href.split('?')[0] + "?" + newQuery;
	}

	$('#search_type').on('change', function() {
		$('#search_input').focus();
	});

	$('#search_input').on('keypress', function(e) {
	    if (e.key === 'Enter') submitSearch();
	});

	$('.search_icon').on('click', function(e) {
		submitSearch();
    });

	var urlVars = getUrlVars();
	if ('search' in urlVars) {
		var search = urlVars.search.replace(/\+/g, '%20')
		$('#search_input').val(decodeURIComponent(search));
		$('#search_count').show();
	}
});

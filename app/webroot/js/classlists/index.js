/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as TableSelection from '/js/common/table_selection.js?7.0';

$(document).ready(function() {

    TableSelection.initialize();

    var dragTarget = false;

    var sortedKeys = [
        {key: 'label', title: 'Form label'},
        {key: 'name', title: 'Student name'},
        {key: 'id', title: 'Student ID'},
        {key: 'email', title: 'Student email'}
    ];

    var listStep = 0;

    var workingTimeout = false;

    var initialize = async function() { 
		const response = await ajaxFetch('/ClassLists/get');
		const responseData = await response.json();

        if (responseData.success) {
            var html = '<tbody>';
            if ('results' in responseData) {

                if ((window.opener != null) && (typeof window.opener.classListCallback === "function")) {
                    try {
                        window.opener.classListCallback(responseData.results);
                    } catch(e) {
                        console.log("Failed class list callback.");
                    }        
                }

                if (responseData.results.length > 0) {

                    // Draw index

                    for (var i = 0; i < responseData.results.length; ++i) {
                        var data = responseData.results[i];
                        var json_data = JSON.parse(data.json_data);

                        html += "<tr>";

                        html += '<td style="width:15px; text-align:center;">' + 
                        '<input type="checkbox" class="item_select" style="margin:1px 0px;" autocomplete="off" />' + 
                        '</td>';
                
                        html += '<td class="list_id" style="display:none;">' + data.id + '</td>';
                        html += '<td class="list_json" style="display:none;">' + data.json_data + '</td>';

                        var date = dayjs.utc(data.created).local();
                        html += '<td class="list_created" style="display:none;">' + date.unix() + '</td>';
                        html += '<td style="width:120px;">' + date.format('YYYY-MM-DD HH:mm') + '</td>';    

                        if (data.last_used == null) {
                            html += '<td class="list_last_used" style="display:none;">' + dayjs().unix() + '</td>';
                            html += '<td style="width:120px;"><i>Unused</i></td>';    
                        } else {
                            var date = dayjs.utc(data.last_used).local();
                            html += '<td class="list_last_used" style="display:none;">' + date.unix() + '</td>';
                            html += '<td style="width:120px;">' + date.format('YYYY-MM-DD HH:mm') + '</td>';    
                        }

                        html += '<td style="width:80px;">' + json_data.length + '</td>';

                        html += '<td class="list_name">';
                        if (data.is_new > 0) html += "<div class='index_new_label label label-info'>New</div> ";
                        html += data.name;
                        html += '</td>';

                        html += '</tr>';
                    }    

                } else {

                    html += '<tr><td colspan="2"><div style="padding-left:10px; color:#aaa;">';
                    html += '<i>No class lists to show here. Click "New Class List" in the menu bar to create one.</i>';
                    html += '</div></td></tr>';

                }
            }
            
            html += '</tbody>';

            $('#class_lists').html(html);
            TableSelection.updateItemActions();

            // Show received copy dialog

            showNextDialog();

            // Sort index

            $('#sort_type').trigger('change');

            return;

        } else {

			throw new Error('Failed to fetch class list data.');

        }
    }

    // Common functions for creating and editing class lists

    var drawClassList = function(listData, $target) {

        var html = '<div class="list_grid" style="width:100%; height:100%; display:flex; flex-direction:column;">';

        html += '<div class="class_list_row">';
        for (var j = 0; j < sortedKeys.length; ++j) {
            var thisHeader = '<div style="flex:none;">' + sortedKeys[j].title + '</div>';
            if (sortedKeys[j].key == 'label') {
                var buttonHTML = 
                    '<div class="btn-group" style="flex:none"> \
                        <button class="btn btn_font btn-mini dropdown-toggle" data-toggle="dropdown" href="#"> \
                            Fill from <span class="caret"></span> \
                        </button> \
                        <ul class="label_fill_list dropdown-menu text-right" style="right:0; left:auto;">';

                for (var k = 0; k < sortedKeys.length; ++k) {
                    if (sortedKeys[k].key !== 'label') {
                        buttonHTML += '<li><a href="#" class="fill_option" data-type="' + sortedKeys[k].key + '">' + sortedKeys[k].title + '</a></li>';
                    }
                }

                buttonHTML += 
                        '</ul> \
                    </div>';

                thisHeader += buttonHTML;
            }
            html += "<div data-type='" + sortedKeys[j].key + "' class='class_list_header'>" + thisHeader + "</div>";
        }
        html += "<div data-type='actions' class='class_list_header' style='text-align:center;'>Actions</div>";
        html += "</div>";

        for (var i = 0; i < listData.length; ++i) {
            html += '<div class="class_list_row">';
            for (var j = 0; j < sortedKeys.length; ++j) {
                var key = sortedKeys[j].key;
                var value = (key in listData[i]) ? listData[i][key] : '';
                html += "<div data-type='" + key + "' class='class_list_body' contenteditable>" + value + "</div>";
            }
            html += "<div data-type='actions' class='class_list_body' style='display:flex; flex-direction:row; justify-content:center; align-items:center;'>";
            html += '<div title="Move" class="menu_button move_icon">&nbsp;</div>';
            html += '<button type="button" title="Add" class="menu_button add_icon">&nbsp;</button>';
            html += '<button type="button" title="Delete" class="menu_button delete_icon">&nbsp;</button>';
            html += "</div>";
            html += "</div>";
        }

        html += '</div>';

        $target.find('.list_grid_wrapper').html(html);
        $target.find('.list_grid').sortable({
            handle: ".move_icon"
        });        

        updateTableVisibility($target);
        updateTableWidths($target);
        updateListWarnings($target);        
    }

    $(document).on('change', '.columns-checkbox', function() {
        var $target = $(this).closest('.list_edit_wrapper');

        if ($(this).prop('checked')) {
            var $key = $(this).attr('data-key');
            var $newCells = $target.find('.class_list_body[data-type="' + $key + '"]');
            $newCells.addClass('working');
        }

        updateTableVisibility($target);
        updateTableWidths($target);
        updateListWarnings($target);        
    });

    $(document).on('click', '.label_fill_list .fill_option', function() {
        var sourceType = $(this).attr('data-type');

        $('.list_grid .class_list_body[data-type="label"]').each(function() {
            var $row = $(this).closest('.class_list_row');
            var $source = $row.find('.class_list_body[data-type="' + sourceType + '"]');
            $(this).text($source.text());    
        });

        updateTableWidths($target);
        updateListWarnings($target);
    });

    $(document).on('keydown', '.list_grid .class_list_body', function(e) {
        if (e.key === 'Tab') {
            e.preventDefault();
            var $nextTarget = $(this).next();
            if (!$nextTarget.is('.class_list_body') || $nextTarget.attr('data-type') == 'actions') {
                const $nextRow = $(this).closest('.class_list_row').next();
                if ($nextRow.is('.class_list_row')) {
                    $nextTarget = $nextRow.find('.class_list_body:visible').first();
                } else {
                    const $wrapper = $(this).closest('.list_edit_wrapper');
                    $nextTarget = $wrapper.find('.class_list_body:visible').first();
                }
            }
            $nextTarget.focus();
        } else if (e.key === 'Enter') {
            e.preventDefault();
            const index = $(this).index();
            const $nextRow = $(this).closest('.class_list_row').next();
            if ($nextRow.is('.class_list_row')) {
                $nextTarget = $nextRow.find('.class_list_body').eq(index);
            } else {
                const $wrapper = $(this).closest('.list_edit_wrapper');
                $nextTarget = $wrapper.find('.class_list_body').eq(index);
            }
            $nextTarget.focus();
        }
    });

    $(document).on('keypress', '.list_grid .class_list_body', function(e) {
        var $wrapper = $(this).closest('.list_edit_wrapper');
        updateTableWidths($wrapper);
        updateListWarnings($wrapper);    
    });

    $(document).on('mousedown', '.list_grid .class_list_body', function(e) {
        e.stopPropagation();
    });

    $(document).on('mousedown', '.list_grid_wrapper', function(e) {
        e.preventDefault();
        document.activeElement.blur();
    });
/*
    $(document).on('paste', '.list_grid .class_list_body', function(e) {
        e.preventDefault();

        var clipboardData = e.originalEvent.clipboardData || window.clipboardData;
        var lines = clipboardData.getData('Text').split('\n');

    });
*/
    $(document).on('click', '.columns-menu-row input', function (e) {
        e.preventDefault();
    });

    $(document).on('click', '.columns-menu-row', function (e) {
        e.stopPropagation();
        var isChecked = $(this).find('input').prop('checked');
        $(this).find('input').prop('checked', !isChecked).trigger('change');
    });

    $(document).on('click', '.list_grid .add_icon', function() {
        var $row = $(this).closest('.class_list_row');
        var $wrapper = $row.closest('.list_edit_wrapper');

        var $next = $row.clone();
        $next.find('.class_list_body').each(function() {
            if ($(this).attr('data-type') != 'actions') {
                $(this).addClass('working').empty();
            }
        });

        $row.after($next);
        $next.find('.class_list_body:visible').first().focus();

        updateListWarnings($wrapper);
    });

    $(document).on('blur', '.class_list_body.working', function() {
        var $wrapper = $(this).closest('.list_edit_wrapper');
        workingTimeout = setTimeout(function() {
            $wrapper.find('.class_list_body').removeClass('working');
            updateListWarnings($wrapper);
        }, 100);
    });

    $(document).on('focus', '.class_list_body', function() {
        clearTimeout(workingTimeout);
        workingTimeout = false;

        if (!$(this).is('.working')) {
            var $wrapper = $(this).closest('.list_edit_wrapper');
            $wrapper.find('.class_list_body').removeClass('working');
            updateListWarnings($wrapper);
        }
    });

    $(document).on('click', '.list_grid .delete_icon', function() {
        var $row = $(this).closest('.class_list_row');
        var $wrapper = $row.closest('.list_edit_wrapper');
        
        $row.remove();

        updateListWarnings($wrapper);
    });

    var updateTableVisibility = function($target) {
        $target.find('.columns-checkbox').each(function() {
            if ($(this).prop('checked')) {
                $target.find('.list_grid .class_list_header[data-type="' + $(this).attr('data-key') + '"]').show();
                $target.find('.list_grid .class_list_body[data-type="' + $(this).attr('data-key') + '"]').show();
                $target.find('.label_fill_list .fill_option[data-type="' + $(this).attr('data-key') + '"]').parent().show();
            } else {
                $target.find('.list_grid .class_list_header[data-type="' + $(this).attr('data-key') + '"]').hide();
                $target.find('.list_grid .class_list_body[data-type="' + $(this).attr('data-key') + '"]').hide();
                $target.find('.label_fill_list .fill_option[data-type="' + $(this).attr('data-key') + '"]').parent().hide();
            }
        });
    }

    var updateTableWidths = function($target) {
        var typeWidths = {};
        var tableWidth = $target.find('.list_grid_wrapper').width();
        $target.find('.class_list_header, .class_list_body').css('width', 'auto');
        $target.find('.list_grid .class_list_row').each(function() {
            var rowWidth = 0;
            $(this).find('.class_list_header:visible, .class_list_body:visible').each(function() {
                var thisWidth = $(this).outerWidth();

                var type = $(this).attr('data-type');
                if (!(type in typeWidths)) typeWidths[type] = 0;
                if (thisWidth > typeWidths[type]) typeWidths[type] = thisWidth;

                rowWidth += thisWidth;
            });
            if (rowWidth > tableWidth) tableWidth = rowWidth;
        });

        $target.find('.list_grid').width(tableWidth);

        var requiredWidth = 0;
        var numKeys = 0;
        for (var type in typeWidths) {
            if (type != 'actions') numKeys++;
            requiredWidth += typeWidths[type];
        }
        var extraWidth = tableWidth - requiredWidth;
        for (var type in typeWidths) {
            if (type == 'actions') {
                $('.list_grid .class_list_header[data-type="' + type + '"]').css('width', typeWidths[type]);
                $('.list_grid .class_list_body[data-type="' + type + '"]').css('width', typeWidths[type]);
            } else {
                var width = typeWidths[type] + extraWidth / numKeys;
                $('.list_grid .class_list_header[data-type="' + type + '"]').css('width', width);
                $('.list_grid .class_list_body[data-type="' + type + '"]').css('width', width);
            }
        }
    }

    var updateListWarnings = function($target) {
        var missingKeys = {};
        var hasMissingKeys = false;

        $target.find('.columns-checkbox:checked').each(function() {
            var key = $(this).attr('data-key');
            $target.find('.class_list_body[data-type="' + key + '"]:not(.working)').each(function() {
                if ($(this).text().length == 0) {
                    var row = $(this).closest('.class_list_row').index();
                    if (!(key in missingKeys)) missingKeys[key] = [];
                    missingKeys[key].push(row);
                    hasMissingKeys = true;
                }
            });
        });

        if (hasMissingKeys) {
            var html = '';
            for (var key in missingKeys) {
                var rowText = ((missingKeys[key].length == 1) ? 'row ' : 'rows ') +  numbersToList(missingKeys[key]);
                html += '<li>Missing ' + (key == 'id' ? 'ID' : key) + ' in ' + rowText + '.</li>';
            }
            $target.find('.list_warnings').html(html);

            $target.find('.list_warnings_wrapper').show();
        } else $target.find('.list_warnings_wrapper').hide();
    }

    // Editing existing class lists

    var saveList = function($dialog) {
		var $form = $dialog.find('form');

        // Save class list

        var listData = [];
        $form.find('.class_list_row').each(function() {
            if ($(this).index() == 0) return;

            var entry = {};
            for (var i = 0; i < sortedKeys.length; ++i) {
                var $element = $(this).find('.class_list_body[data-type="' + sortedKeys[i].key + '"]');
                var includeElement = $element.is(':visible') && ($element.length == 1) && ($element.text().length > 0);
                if (includeElement) entry[sortedKeys[i].key] = $element.text();
            }
            listData.push(entry);
        });

        if ($form.validationEngine('validate')) {
    
            var postData = {
                name: $form.find('.list_name').val(),
                json_data: JSON.stringify(listData)
            };

            if ($dialog[0].hasAttribute('data-list-id')) {
                postData.id = $dialog.attr('data-list-id');
            }
    
            if (pendingAJAXPost) return false;
            showSubmitProgress($dialog);
            pendingAJAXPost = true;

            ajaxFetch('/ClassLists/save/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(postData)
            }).then(async response => {
                pendingAJAXPost = false;
                const responseData = await response.json();
                
                if (responseData.success) {
                    initialize().then(function() {
                        $dialog.dialog('close');

                        $.gritter.add({
                            title: "Success",
                            text: "Class list saved",
                            image: "/img/success.svg"
                        });	            
                    });
                }            

            }).catch(function() {
                showSubmitError($dialog);			
            });

        }
    }

    $('#editList_dialog').dialog({
		autoOpen: false,
		width: 600,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() {
                if (!$(this)[0].hasAttribute('data-list-id')) {
                    $.gritter.add({
                        title: "Success",
                        text: "New list cancelled",
                        image: "/img/success.svg"
                    });	            
                }
                $(this).dialog("close");
            },
            'Save': function() {
                saveList($('#editList_dialog'))
            }
        }, open: function(event) {
            var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
            $buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
            $buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
            initSubmitProgress($(this));
        }
    });

    var startEditList = function($thisRow) {
        var listID = $thisRow.find('.list_id').text();
        $('#editList_dialog').attr('data-list-id', listID);

        $thisRow.find('.index_new_label').remove();
        var listName = $thisRow.find('.list_name').text();
        $('#editList_dialog .list_name').val(listName);

        var listData = JSON.parse($thisRow.find('.list_json').text());

        var menuHTML = '';
        for (var j = 0; j < sortedKeys.length; ++j) {
            menuHTML += 
                '<li> \
                    <div class="dropdown-item columns-menu-row" href="#"> \
                        <input class="columns-checkbox" type="checkbox" data-key="' + sortedKeys[j].key + '" /> \
                        <label class="columns-label">' + sortedKeys[j].title + '</label> \
                    </div> \
                </li>';
        }

        var $columnsMenu = $('#editList_dialog').find('.columns-menu');
        $columnsMenu.html(menuHTML);

        for (var i = 0; i < listData.length; ++i) {
            for (var key in listData[i]) {
                $columnsMenu.find('.columns-checkbox[data-key="' + key + '"]').prop('checked', true);
            }
        }

        $('#editList_dialog').dialog('open');
        drawClassList(listData, $('#edit_list_edit'));        
    }

    $("#editList_button").on('click', function(e) {
        e.preventDefault();

        var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
			}
		});

        var $thisRow = $selectedCheckbox.closest('tr');
        startEditList($thisRow);

        $('#editList_dialog').dialog('option', 'title', 'Edit class list');
    });

    $(document).on('dblclick', '.selection_table tbody tr', function(e) {
		e.preventDefault();

		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked == 0) startEditList($(this));
		else if (numChecked == 1) {
			var $selectedRow = $selectedCheckbox.closest('tr');
			if ($selectedRow[0] == $(this)[0]) startEditList($(this));
		}        

        $('#editList_dialog').dialog('option', 'title', 'Edit class list');
    });

    // Handle "Delete" button

    $(document).bind('keydown', function (e) {
		if ($('.ui-dialog:visible').length == 0) {
	    	if (((e.key === 'Backspace') || (e.key === 'Delete')) && !$(e.target).is("input:not([readonly]), textarea")) {
				$('#deleteList_button').trigger("click");
			}
		}
	});

    $('#deleteList_button').click(function(e){
		e.preventDefault();
		if ($(this).attr('disabled')) return;
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked > 0) {
			if (numChecked == 1) {
				$('#deleteList_text').html('Delete the selected class list?').show();
			} else {
				$('#deleteList_text').html('Delete the ' + numChecked + ' selected class lists?').show();
			}

			$('#deleteList_dialog').dialog('open');
		}
	});

    var deleteList = function() {
		var $dialog = $(this);

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var selectedItems = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var listID = $(this).closest('tr').find('.list_id').text();
				selectedItems.push(listID);
			}
		});

        ajaxFetch('/ClassLists/delete', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                list_ids: selectedItems
            })
        }).then(async response => {
            pendingAJAXPost = false;
            const responseData = await response.json();
        
			if (responseData.success) {
				var message = responseData.count + " class list" + ((responseData.count == 1) ? "" : "s") + " deleted.";

                initialize().then(function() {    
                    $dialog.dialog('close');

                    $.gritter.add({
                        title: "Success",
                        text: message,
                        image: "/img/success.svg"
                    });	            
                });

			} else showSubmitError($dialog);
		});
    }

    $('#deleteList_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteList
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
		   	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});

    // Creating new class lists

    function updateSubmitEnabled() {
    	if ($('#list_text').val().length > 0) $("#list_step_next").removeClass('disabled');
    	else $("#list_step_next").addClass('disabled');
    }

    updateSubmitEnabled();
    $('#list_text').on('keyup', updateSubmitEnabled);
    $('#list_text').on('change', updateSubmitEnabled);
    $('#list_text').on('paste', function() {
        setTimeout(updateSubmitEnabled, 0);
    });
    
	$(document).bind('dragenter', function(e) {
    	e.preventDefault();

        dragTarget = e.target;
        $('#fileDropTarget').show();
        $("#fileDropTarget").fadeTo('fast', 0.7);

        return false;
    });

    $(document).bind('dragover', function(e) {
    	e.preventDefault();
    });

    $(document).bind('dragleave', function(e) {
        if (e.target == dragTarget) {
            dragTarget = false;
            $("#fileDropTarget").fadeTo('fast', 0.0, function() {
                $(this).hide();
            });
        }
	});

    $('#fileDropTarget').bind('drop', function(e) {
    	e.preventDefault();
        var file = e.originalEvent.dataTransfer.files[0];
        if (file) readFile(file);
    });

    $(document).bind('drop', function(e) {
    	e.preventDefault();
        $("#fileDropTarget").fadeTo('fast', 0.0, function() {
            $(this).hide();
        });
        return false;
    });

    $('#fileupload').on('change', function(e) {
        var file = this.files[0];
        if (file) readFile(file);
    });

    function parseCSVLine(line) {
        var fields = [];
        var field = '';
        var inQuotes = false;
        var i = 0;
    
        while (i < line.length) {
            var char = line[i];
    
            if (inQuotes) {
                if (char === '"') {
                    // Check for escaped quote
                    if (i + 1 < line.length && line[i + 1] === '"') {
                        field += '"';
                        i += 1; // Skip the next quote
                    } else {
                        inQuotes = false; // Closing quote
                    }
                } else {
                    field += char;
                }
            } else {
                if (char === '"') {
                    inQuotes = true; // Start of a quoted field
                } else if (char === ',') {
                    fields.push(field.trim());
                    field = '';
                } else {
                    field += char;
                }
            }
            i += 1;
        }
    
        // Add the last field
        fields.push(field.trim());
    
        return fields;
    }
    
    function readFile(file) {
        var reader = new FileReader();
        reader.onload = function(e) {
            var fileContent = e.target.result;
            var lines = fileContent.split(/\r\n|\n/); // Split file content into lines
            var processedLines = lines.map(function(line) {
                var fields = parseCSVLine(line); // Parse the CSV line into fields
                return fields.join('\t'); // Join fields with tabs
            });
            var newContent = processedLines.join('\n'); // Combine all processed lines
            $('#list_text').val(newContent).trigger('change'); // Update the DOM element
        };
        reader.readAsText(file);
    }
    
    $('#createList_dialog').dialog({
		autoOpen: false,
		width: 600,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		},
        open: function(event) {
            initSubmitProgress($(this));
        }, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
    });

    $('#list_cancel').on('click', function() {
        $('#createList_dialog').dialog('close');
    });

    $('#list_step_next').on('click', function() {
        if ($(this).hasClass('disabled')) return;

        var $dialog = $('#createList_dialog');

        if (listStep == 1) {

            saveList($dialog);
    
        } else {

            // Move to next panel

            ++listStep;

            if (listStep == 1) {
                parseClassList();
                $('#list_step_next').html('Save').addClass('btn-save');
            }
    
            var translation = -listStep * 600;
            $('.step_wrapper').css('transform', 'translateX(' + translation + 'px)');

            if (listStep == 1) {
                setTimeout(function() {
                    $dialog.find('.list_name').focus();
                }, 700);
            }

        }
    });

    function getAutomaticParts(listRow) {
        var response = {};
        var workingText = listRow;

        var parts = [
            { key: 'email', regex: /[a-zA-Z0-9\._%+!$&*=^|~#%'`?{}/\-]+@([a-z0-9\-]+\.){1,}([a-z]{2,16})/g },
            { key: 'name', regex: /([\p{L}\p{M}]+(?:[-' ,]+[\p{L}\p{M}]+)*)/gu },
            { key: 'id', regex: /\p{Nd}+/gu }
        ];

        // Remove any text in parentheses

        var parenthesisText = '';
        workingText = workingText.replace(/\([^)]*\)/g, (match) => {
            if (parenthesisText.length > 0) parenthesisText += ' ';
            parenthesisText += match;
            return '';
        });

        // Match each part in sequence, deleting it from the text if found
    
        for (var i = 0; i < parts.length; ++i) {
            const matches = workingText.matchAll(parts[i].regex);

            var best = '';
            for (const match of matches) {
                if (match[0].length > best.length) best = match[0];
            }
        
            if (best.length > 0) {
                workingText = workingText.replace(best, '');
                response[parts[i].key] = best;
            }
        }

        // Put names into name case

        if ('name' in response) {
            response.name = response.name.toLowerCase().replace(/(?<![\p{L}\p{M}])([\p{L}\p{M}])/gu, char => char.toUpperCase());
        }

        // Retain the original text as a proposed response form label if necessary

        var omittedText = parenthesisText + ' ' + workingText;
        if (/[a-zA-Z0-9]/.test(omittedText)) response.label = listRow;

        return response;
    }

    function parseClassList() {
        var listRows = $('#list_text').val().split("\n");

        var listData = [];
        var omitLabels = true;
        for (var i = 0; i < listRows.length; ++i) {
            var text = listRows[i].replace(/[\x00-\x1F\s]/g, ' ').trim();
            if (text.length == 0) continue;
            var parts = getAutomaticParts(text);
            if ('label' in parts) omitLabels = false;
            else parts.label = text;
            listData.push(parts);
        }

        var menuHTML = '';
        for (var j = 0; j < sortedKeys.length; ++j) {
            menuHTML += 
                '<li> \
                    <div class="dropdown-item columns-menu-row" href="#"> \
                        <input class="columns-checkbox" type="checkbox" data-key="' + sortedKeys[j].key + '" /> \
                        <label class="columns-label">' + sortedKeys[j].title + '</label> \
                    </div> \
                </li>';
        }

        var $columnsMenu = $('#create_list_edit').find('.columns-menu');
        $columnsMenu.html(menuHTML);

        for (var i = 0; i < listData.length; ++i) {
            for (var key in listData[i]) {
                $columnsMenu.find('.columns-checkbox[data-key="' + key + '"]').prop('checked', true);
            }
        }
        if (omitLabels) {
            $columnsMenu.find('.columns-checkbox[data-key="label"]').prop('checked', false);
        }

        drawClassList(listData, $('#create_list_edit'));
    }

    // Set up form validation

    $('form').validationEngine({
        validationEventTrigger: 'submit',
        promptPosition : "bottomLeft",
       focusFirstField: false
    }).submit(function(e){ e.preventDefault() });

    // Handle "send copy" button

	$('#sendCopy_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Send': function() {
		  		$('#send_selection_type').html("Choose from list");
				$('#send_email_checklist').hide();
				$('#send_email_list').show();
				
          		$('#sendCopy_form').submit(); 
          	}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Send")').addClass('btn btn-save btn_font');
	      	$('#send_email_list').focus();
		  	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		
	
    $('#sendCopy_form').validationEngine('detach');
    $('#sendCopy_form').validationEngine('attach', {
		validationEventTrigger: 'submit',
		promptPosition : "bottomLeft",
		scroll: false,
		ajaxFormValidation: true,
		ajaxFormValidationURL: '/Users/validateTeacherList',
		onAjaxFormComplete: sendValidated,
		onFieldError: function() {
			hideSubmitProgress($('#sendCopy_dialog'));
  		},
  		onBeforeAjaxFormValidation : function() {
  			showSubmitProgress($('#sendCopy_dialog'));
  		}
    });
    
    $('#sendCopy_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		$('#send_email_list').val("");
		$('.send_email_checkbox').prop('checked', false);
		$('#send_selection_type').html("Choose from list");
		$('#send_email_checklist').hide();
		$('#send_email_list').show();

		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		var sendCopyText = "";
		if (numChecked == 1) {
			var $itemName = $selectedCheckbox.closest('tr').find('.list_name').html();
			sendCopyText = '<p>Sending a copy of "' + $itemName + '".';
		} else sendCopyText = '<p>Sending a copy of ' + numChecked + ' selected items.';
		sendCopyText += " Changes made by the recipient will not be reflected in the original.";
		$('#sendCopy_text').html(sendCopyText);

		$('#sendCopy_dialog').dialog('open');
	});

    $(document).on('click', '.send_email_checkbox', function(event) {
		event.stopPropagation();
		var email = $(this).data('email');
		
		var email_list = $('#send_email_list').val().trim();
		var email_array;
		if (email_list.length == 0) email_array = [];
		else email_array = email_list.split(/,|;| |\n|\t/);
		
		var index = email_array.indexOf(email);
		if ($(this).prop('checked')) {
			if (index == -1) email_array.push(email);
		} else if (index > -1) email_array.splice(index, 1);
				
		$('#send_email_list').val(email_array.join("\n"));
	});
	
	$('#send_selection_type').on('click', function(e) {
		e.preventDefault();
		
		if ($('#send_email_list').is(':visible')) {
			$('#send_selection_type').html("Enter emails");
			
			var email_list = $('#send_email_list').val().trim();
			var email_array;
			if (email_list.length == 0) email_array = [];
			else email_array = email_list.split(/,|;| |\n|\t/);

			$('.send_email_checkbox').each(function() {
				var email = $(this).data('email');
				$(this).prop('checked', email_array.indexOf(email) > -1);
			});
			
			$('#send_email_list').hide();
			$('#send_email_checklist').show().focus();
		} else {
			$('#send_selection_type').html("Choose from list");
			$('#send_email_checklist').hide();
			$('#send_email_list').show().focus();
		}
	});

	function sendValidated(status, form, json_response_from_server, options)  {
        var $dialog = $('#sendCopy_dialog');

		if (status === true) {

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			var selectedItems = [];
			$('.item_select').each(function() {
				if ($(this).prop('checked')) {
					var itemID = parseInt($(this).closest('tr').find('.list_id').html());
					selectedItems.push(itemID);
				}
			});
	
			var emailList = $('#send_email_list').val().split(/,|;| |\n|\t/);

            ajaxFetch('/ClassLists/send', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    email_list: emailList,
                    list_ids: selectedItems
                })
            }).then(async response => {
                pendingAJAXPost = false;
                const responseData = await response.json();
            
				if (responseData.success) {
					var message = responseData.numSent + " item" + ((responseData.numSent == 1) ? "" : "s") + " sent.";

                    initialize().then(function() {
                        $dialog.dialog('close');

                        $.gritter.add({
                            title: "Success",
                            text: message,
                            image: "/img/success.svg"
                        });
                    });
                    
                } else {

                    throw new Error("Unable to send class list.");

                }

			}).catch(function() {
				showSubmitError($dialog);
			});
		} else hideSubmitProgress($dialog);
	}

    // Set up sorting

    function sortIndex(type, direction) {
        var $table = $('#class_lists');
        var $rows = $table.find('tr');

        $rows.sort(function(a, b) {
            var valA = $(a).find('.list_' + type).text();
            if (/^\d+$/.test(valA)) valA = parseInt(valA);

            var valB = $(b).find('.list_' + type).text();
            if (/^\d+$/.test(valB)) valB = parseInt(valB);
    
            if (direction === "asc") {
                return valA > valB ? 1 : valA < valB ? -1 : 0;
            } else {
                return valA < valB ? 1 : valA > valB ? -1 : 0;
            }
        });
    
        $rows.each(function() {
            $table.append($(this));
        })
    }
    
    $('.header_sort').on('click', function() {
        sortIndex($('#sort_type').val(), $(this).attr('data-direction'));
    });

    $('#sort_type').on('change', function() {
        sortIndex($('#sort_type').val(), $('#sort_type option:selected').attr('data-default-direction'));
    });

    // Show received items
	
	var queuedDialogs = [];
	
	function showNextDialog() {
		if (queuedDialogs.length > 0) {
			queuedDialogs.pop().dialog('open');
		}
	}

	$('#receiveCopy_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Thanks!': function() {
               	$(this).dialog("close");
               	showNextDialog();
			}
		}, open: function(event) {
		   	$('.ui-dialog-buttonpane').find('button:contains("Thanks!")').addClass('btn btn-save btn_font');
	        $('.ui-dialog-buttonpane').find('button').blur();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});
	
	if (receivedCount > 0) {
		$('#receiveCopy_text').html('<p><b>You have received ' + receivedCount + ' item' + ((receivedCount == 1) ? '' : 's') + ' from other users.</b></p><p>Received items will be indicated with a "New" tag</p>');
		queuedDialogs.push($('#receiveCopy_dialog'));
	}

    // Set up "New Class List" button

    $('#newItem_button').on('click', function() {
        listStep = 0;
        $('#list_step_next').html('Next').removeClass('btn-save');
        $('#list_step_next').addClass('disabled');

        $('.step_wrapper').css('transition', 'none');
        $('.step_wrapper').css('transform', 'translateX(0px)');
        setTimeout(function() {
            $('.step_wrapper').css('transition', 'transform 0.5s ease');
        }, 0);

        $('#list_text').val('');
        $('#create_list_edit .list_name').val('');

        $('#createList_dialog').dialog('open');

        $('#list_text').focus();
    });

    // Set up "Duplicate" button

    $('#duplicateList_button').on('click', function(e) {
        var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
			}
		});

        var $thisRow = $selectedCheckbox.closest('tr');

        startEditList($thisRow);

        $('#editList_dialog').removeAttr('data-list-id');

        var listName = $('#editList_dialog .list_name').val() + ' - Copy';
        $('#editList_dialog .list_name').val(listName);

        $('#editList_dialog').dialog('option', 'title', 'Duplicate class list');
        $('#edit_list_edit .list_name').focus().select();
    });

    // Set up "Merge Lists" button

    $('#mergeLists_button').on('click', function(e) {
        var listData = [];

		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
                var $thisRow = $(this).closest('tr');
                var thisData = JSON.parse($thisRow.find('.list_json').text());
                listData = listData.concat(thisData);
			}
		});

        $('#editList_dialog').removeAttr('data-list-id');
        $('#editList_dialog .list_name').val('');

        var menuHTML = '';
        for (var j = 0; j < sortedKeys.length; ++j) {
            menuHTML += 
                '<li> \
                    <div class="dropdown-item columns-menu-row" href="#"> \
                        <input class="columns-checkbox" type="checkbox" data-key="' + sortedKeys[j].key + '" /> \
                        <label class="columns-label">' + sortedKeys[j].title + '</label> \
                    </div> \
                </li>';
        }

        var $columnsMenu = $('#editList_dialog').find('.columns-menu');
        $columnsMenu.html(menuHTML);

        for (var i = 0; i < listData.length; ++i) {
            for (var key in listData[i]) {
                $columnsMenu.find('.columns-checkbox[data-key="' + key + '"]').prop('checked', true);
            }
        }

        $('#editList_dialog').dialog('open');
        drawClassList(listData, $('#edit_list_edit'));        

        $('#editList_dialog').dialog('option', 'title', 'Merge class lists');
        $('#edit_list_edit .list_name').focus();
    });
    
    $(document).on('modifyItemActions', function(event) {
        if ($('.item_select:checked').length > 1) {
            $('#mergeLists_button').removeClass('disabled-link');
        } else $('#mergeLists_button').addClass('disabled-link');
	});

    $('#newItem_button .tab_icon').css('background-image', 'url("/img/tab_icons/add_item_icon.svg")');
    $('#newItem_button .tab_text').html('New<br />Class List');
    $('#newItem_button').show();
    	
	// Load class lists

	initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});
		window.location.href = '/Documents';
	});

});

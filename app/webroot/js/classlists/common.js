/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var classListCommonInitializing = false;
var classListCommonInitialized = false;

var lastNeedsEmail = false;
var classListDidDraw = false;
var classListDidSort = false;

function initializeClassLists(options) {
    var needsEmail;
    if ('needsEmail' in options) {
        needsEmail = options.email;
        lastNeedsEmail = needsEmail;
    } else needsEmail = lastNeedsEmail;

    if ('classListDidDraw' in options) {
        classListDidDraw = options.classListDidDraw;
    } else classListDidDraw = false;

    if ('classListDidSort' in options) {
        classListDidSort = options.classListDidSort;
    } else classListDidSort = false;

    if (needsEmail) {
        for (var i = 0; i < classLists.length; ++i) {
            var classList = classLists[i];

            var data = JSON.parse(classList.json_data);
            data = data.filter(function(student) {
                return 'email' in student;
            });

            classList.json_data = JSON.stringify(data);
            classLists[i] = classList;
        }
    }

    if (classLists.length > 0) {

        var selectHTML = "<option value='{}' selected disabled>Select a class list</option>";
        for (var i = 0; i < classLists.length; ++i) {
            selectHTML += "<option value='" + i + "'>" + classLists[i].name + "</option>";
        }
        $('#class_list_select').html(selectHTML);
        $('#class_list_select').removeAttr('disabled');

        $('#class_list_info_wrapper').hide();
        $('#class_list_edit_wrapper').show();
            
    } else {

        var selectHTML = "<option value='{}' selected disabled>No class lists to show here</option>";

        $('#class_list_select').html(selectHTML);
        $('#class_list_select').attr('disabled', true);

        $('#class_list_edit_wrapper').hide();
        $('#class_list_info_wrapper').show();

    }

    var wrapperHTML = '<div class="list_row disabled" style="color:#aaa">'
    wrapperHTML += '<i>Select a class list above or click "Go to class lists" to create one.</i>';
    wrapperHTML += '</div>';
    $('#class_list_wrapper').html(wrapperHTML);

    $('.check_list_button').attr('disabled', true);
    $('#class_list_continue').attr('disabled', true);

    document.activeElement.blur();
}

function initialize() {
    if (classListCommonInitializing) return;
    classListCommonInitializing = true;

    $('#class_list_select').on('change', function() {
        var index = $('#class_list_select').val();
        var classList = JSON.parse(classLists[index].json_data);

        var html = '';
        if (classList.length > 0) {

            for (var i = 0; i < classList.length; ++i) {
                var studentName = '';
                if ('label' in classList[i]) {
                    studentName = classList[i].label;
                } else if (('name' in classList[i]) || ('id' in classList[i])) {
                    if ('name' in classList[i]) studentName += classList[i].name + ' ';
                    if ('id' in classList[i]) studentName += classList[i].id;
                    studentName = studentName.trim();
                } else if ('email' in classList[i]) {
                    studentName = classList[i].email;
                }

                html += "<div class='list_row' data-json-data='" + htmlSingleQuotes(JSON.stringify(classList[i])) + "'>";
                html += "<div style='flex:none; width:20px;'><input type='checkbox' class='class_list_checkbox' /></div>";
                html += "<div style='flex:1 1 100%;'>" + studentName + "</div>";
                html += "<div title='Move' class='menu_button move_icon' style='flex:none; width:12px; height:12px; margin:4px;'>&nbsp;</div>";
                html += "</div>";
            }
    
            $('.check_list_button').removeAttr('disabled');

        } else {

            html += '<div class="list_row disabled" style="color:#aaa">'
            html += '<i>This list contains no students' + (lastNeedsEmail ? ' with email addresses' : '') + '.</i>';
            html += '</div>';
            
            $('.check_list_button').attr('disabled', true);

        }

        $('#class_list_wrapper').html(html);
        $('#class_list_wrapper').sortable({
            handle: ".move_icon"
        }); 

        if (classListDidSort !== false) {
            $('#class_list_wrapper').sortable('option', 'update', classListDidSort);
        }

        $('#class_list_wrapper .class_list_checkbox').prop('checked', true);

        var hasChecked = ($('#class_list_wrapper .class_list_checkbox:checked').length > 0);
        if (hasChecked) $('#class_list_continue').removeAttr('disabled');
        else $('#class_list_continue').attr('disabled', true);

        if (classListDidDraw !== false) classListDidDraw();
    });

    var updateHasChecked = function() {
        var hasChecked = ($('#class_list_wrapper .class_list_checkbox:checked').length > 0);
        if (hasChecked) $('#class_list_continue').removeAttr('disabled');
        else $('#class_list_continue').attr('disabled', true);
    }

    $('#class_list_wrapper').on('click', '.class_list_checkbox', function(e) {
        e.stopPropagation();
        updateHasChecked();
    });

    $('#class_list_wrapper').on('click', '.list_row', function(e) {
        var isChecked = $(this).find('.class_list_checkbox').prop('checked');
        $(this).find('.class_list_checkbox').prop('checked', !isChecked);
        updateHasChecked();
    });

    $('#class_list_check_all').on('click', function(e) {
        e.preventDefault();
        $('#class_list_wrapper .class_list_checkbox').prop('checked', true);

        var hasChecked = ($('#class_list_wrapper .class_list_checkbox:checked').length > 0);
        if (hasChecked) $('#class_list_continue').removeAttr('disabled');
        else $('#class_list_continue').attr('disabled', true);
    });

    $('#class_list_clear_all').on('click', function(e) {
        e.preventDefault();
        $('#class_list_wrapper .class_list_checkbox').prop('checked', false);
        $('#class_list_continue').attr('disabled', true);
    });

    $('#class_list_shuffle').on('click', function(e) {
        e.preventDefault();

        var $listRows = $('#class_list_wrapper .list_row');
    
        if ($listRows.length > 0) {
            for (var i = $listRows.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var $i = $listRows.eq(i);
                var $j = $listRows.eq(j);
                if (i !== j) {
                    var $temp = $('<span>').hide(); // Temporary placeholder
                    $i.before($temp);               // Place $temp before $i
                    $j.before($i);                  // Move $j before $i
                    $temp.replaceWith($j);          // Replace $temp with $j
                }
            }
        }
    });

    $('#class_list_index').on('click', function(e) {
        e.preventDefault();
        window.open('/ClassLists/index', '_blank');
    });

    window.classListCallback = function(data) {
        classLists = data;
        initializeClassLists(lastNeedsEmail);
    }
 
    classListCommonInitialized = true;
}

export { initialize, initializeClassLists };
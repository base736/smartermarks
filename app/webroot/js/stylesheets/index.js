/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as TableSelection from '/js/common/table_selection.js?7.0';

$(document).ready(function() {

    TableSelection.initialize();

	var initialize = async function() { 
		const response = await ajaxFetch('/StyleSheets/get');
		const responseData = await response.json();

		if (responseData.success) {
			var html = '<tbody>';
			if (('results' in responseData) && (responseData.results.length > 0)) {

				// Draw index

				for (var i = 0; i < responseData.results.length; ++i) {
					var data = responseData.results[i];

					html += "<tr>";

					html += '<td style="width:15px; text-align:center;">' + 
					'<input type="checkbox" class="item_select" style="margin:1px 0px;" autocomplete="off" />' + 
					'</td>';
			
					html += '<td class="sheet_id" style="display:none;">' + data.id + '</td>';
					html += '<td class="sheet_json" style="display:none;">' + data.json_data + '</td>';

					var date = dayjs.utc(data.created).local();
					html += '<td class="sheet_created" style="display:none;">' + date.unix() + '</td>';
					html += '<td style="width:120px;">' + date.format('YYYY-MM-DD HH:mm') + '</td>';    

					if (data.last_used == null) {
						html += '<td class="sheet_last_used" style="display:none;">' + dayjs().unix() + '</td>';
						html += '<td style="width:120px;"><i>Unused</i></td>';    
					} else {
						var date = dayjs.utc(data.last_used).local();
						html += '<td class="sheet_last_used" style="display:none;">' + date.unix() + '</td>';
						html += '<td style="width:120px;">' + date.format('YYYY-MM-DD HH:mm') + '</td>';    
					}

					html += '<td class="sheet_name">';
					if (data.is_new > 0) html += "<div class='index_new_label label label-info'>New</div> ";
					html += data.name;
					html += '</td>';

					html += '</tr>';
				}    

			} else {

				html += '<tr><td colspan="2"><div style="padding-left:10px;">';
				html += '<i>No style sheets to show here.</i>';
				html += '</div></td></tr>';

			}
			html += '</tbody>';

			$('#style_sheets').html(html);
			updateItemActions();

			// Show received copy dialog

			showNextDialog();

			// Sort index

			$('#sort_type').trigger('change');

			return;

		} else {

			throw new Error('Failed to fetch style sheet data.');

        }
    }

    // Creating and editing style sheets

    var saveSheet = function() {
        var $dialog = $('#editSheet_dialog');
		var $form = $dialog.find('form');

        // Save style sheet

        var sheetData = {};

        if ($form.validationEngine('validate')) {
    
            var postData = {
                name: $('#edit_sheet_name').val(),
                json_data: JSON.stringify(sheetData)
            };

            if ($dialog[0].hasAttribute('data-sheet-id')) {
                postData.id = $dialog.attr('data-sheet-id');
            }
    
            if (pendingAJAXPost) return false;
            showSubmitProgress($dialog);
            pendingAJAXPost = true;

			ajaxFetch('/StyleSheets/save/', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(postData)
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
			
                if (responseData.success) {
                    initialize().then(function() {
                        $dialog.dialog('close');

                        $.gritter.add({
                            title: "Success",
                            text: "Style sheet saved",
                            image: "/img/success.svg"
                        });	            
                    });
                }            

            }).catch(function() {
                showSubmitError($dialog);			
            });

        }
    }

    $('#editSheet_dialog').dialog({
		autoOpen: false,
		width: 600,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() {
                $(this).dialog("close");
            },
            'Save': function() {
                saveSheet()
            }
        }, open: function(event) {
            var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
            $buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
            $buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
            initSubmitProgress($(this));
        }
    });

    $("#editSheet_button").on('click', function(e) {
        e.preventDefault();

        var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
			}
		});

        var $thisRow = $selectedCheckbox.closest('tr');

        var sheetID = $thisRow.find('.sheet_id').text();
        $('#editSheet_dialog').attr('data-sheet-id', sheetID);

        var sheetName = $thisRow.find('.sheet_name').text();
        $('#edit_sheet_name').val(sheetName);

        var sheetData = JSON.parse($thisRow.find('.sheet_json').text());
        /** Draw style sheet here **/

        $('#editSheet_dialog').dialog({ title: "Edit style sheet" });
        $('#editSheet_dialog').dialog('open');
    });

    // Handle "Delete" button

    $(document).bind('keydown', function (e) {
		if ($('.ui-dialog:visible').length == 0) {
	    	if (((e.key === 'Backspace') || (e.key === 'Delete')) && !$(e.target).is("input:not([readonly]), textarea")) {
				$('#deleteSheet_button').trigger("click");
			}
		}
	});

    $('#deleteSheet_button').click(function(e){
		e.preventDefault();
		if ($(this).attr('disabled')) return;
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked > 0) {
			if (numChecked == 1) {
				$('#deleteSheet_text').html('Delete the selected style sheet?').show();
			} else {
				$('#deleteSheet_text').html('Delete the ' + numChecked + ' selected style sheets?').show();
			}

			$('#deleteSheet_dialog').dialog('open');
		}
	});

    var deleteSheet = function() {
		var $dialog = $(this);

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var selectedItems = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var listID = $(this).closest('tr').find('.sheet_id').text();
				selectedItems.push(listID);
			}
		});

		ajaxFetch('/StyleSheets/delete', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				sheet_ids: selectedItems
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				var message = responseData.count + " style sheet" + ((responseData.count == 1) ? "" : "s") + " deleted.";

                initialize().then(function() {
                    $dialog.dialog('close');

                    $.gritter.add({
                        title: "Success",
                        text: message,
                        image: "/img/success.svg"
                    });	            
                });

			} else showSubmitError($dialog);
		});
    }

    $('#deleteSheet_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteSheet
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
		   	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});

    // Set up form validation

    $('form').validationEngine({
        validationEventTrigger: 'submit',
        promptPosition : "bottomLeft",
       focusFirstField: false
    }).submit(function(e){ e.preventDefault() });

    // Handle "send copy" button

	$('#sendCopy_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Send': function() {
		  		$('#send_selection_type').html("Choose from list");
				$('#send_email_checklist').hide();
				$('#send_email_list').show();
				
          		$('#sendCopy_form').submit(); 
          	}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Send")').addClass('btn btn-save btn_font');
	      	$('#send_email_list').focus();
		  	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		
	
    $('#sendCopy_form').validationEngine('detach');
    $('#sendCopy_form').validationEngine('attach', {
		validationEventTrigger: 'submit',
		promptPosition : "bottomLeft",
		scroll: false,
		ajaxFormValidation: true,
		ajaxFormValidationURL: '/Users/validateTeacherList',
		onAjaxFormComplete: sendValidated,
		onFieldError: function() {
			hideSubmitProgress($('#sendCopy_dialog'));
  		},
  		onBeforeAjaxFormValidation : function() {
  			showSubmitProgress($('#sendCopy_dialog'));
  		}
    });
    
    $('#sendCopy_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		$('#send_email_list').val("");
		$('.send_email_checkbox').prop('checked', false);
		$('#send_selection_type').html("Choose from list");
		$('#send_email_checklist').hide();
		$('#send_email_list').show();

		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		var sendCopyText = "";
		if (numChecked == 1) {
			var itemName = $selectedCheckbox.closest('tr').find('.sheet_name').html();
			sendCopyText = '<p>Sending a copy of "' + itemName + '".';
		} else sendCopyText = '<p>Sending a copy of ' + numChecked + ' selected items.';
		sendCopyText += " Changes made by the recipient will not be reflected in the original.";
		$('#sendCopy_text').html(sendCopyText);

		$('#sendCopy_dialog').dialog('open');
	});

    $(document).on('click', '.send_email_checkbox', function(event) {
		event.stopPropagation();
		var email = $(this).data('email');
		
		var email_list = $('#send_email_list').val().trim();
		var email_array;
		if (email_list.length == 0) email_array = [];
		else email_array = email_list.split(/,|;| |\n|\t/);
		
		var index = email_array.indexOf(email);
		if ($(this).prop('checked')) {
			if (index == -1) email_array.push(email);
		} else if (index > -1) email_array.splice(index, 1);
				
		$('#send_email_list').val(email_array.join("\n"));
	});
	
	$('#send_selection_type').on('click', function(e) {
		e.preventDefault();
		
		if ($('#send_email_list').is(':visible')) {
			$('#send_selection_type').html("Enter emails");
			
			var email_list = $('#send_email_list').val().trim();
			var email_array;
			if (email_list.length == 0) email_array = [];
			else email_array = email_list.split(/,|;| |\n|\t/);

			$('.send_email_checkbox').each(function() {
				var email = $(this).data('email');
				$(this).prop('checked', email_array.indexOf(email) > -1);
			});
			
			$('#send_email_list').hide();
			$('#send_email_checklist').show().focus();
		} else {
			$('#send_selection_type').html("Choose from list");
			$('#send_email_checklist').hide();
			$('#send_email_list').show().focus();
		}
	});

	function sendValidated(status, form, json_response_from_server, options)  {
        var $dialog = $('#sendCopy_dialog');

		if (status === true) {

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			var selectedItems = [];
			$('.item_select').each(function() {
				if ($(this).prop('checked')) {
					var itemID = parseInt($(this).closest('tr').find('.sheet_id').html());
					selectedItems.push(itemID);
				}
			});
	
			var emailList = $('#send_email_list').val().split(/,|;| |\n|\t/);

			ajaxFetch('/StyleSheets/send', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					email_list: emailList,
					sheet_ids: selectedItems
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();

				if (responseData.success) {
					var message = responseData.numSent + " item" + ((responseData.numSent == 1) ? "" : "s") + " sent.";

                    initialize().then(function() {
                        $dialog.dialog('close');

                        $.gritter.add({
                            title: "Success",
                            text: message,
                            image: "/img/success.svg"
                        });
                    });

				} else {

					throw new Error("Unable to send style sheet.");

				}

			}).catch(function() {
				showSubmitError($dialog);
			});
		} else hideSubmitProgress($dialog);
	}

    // Set up sorting

    function sortIndex(type, direction) {
        var $table = $('#style_sheets');
        var $rows = $table.find('tr');

        $rows.sort(function(a, b) {
            var valA = $(a).find('.sheet_' + type).text();
            if (/^\d+$/.test(valA)) valA = parseInt(valA);

            var valB = $(b).find('.sheet_' + type).text();
            if (/^\d+$/.test(valB)) valB = parseInt(valB);
    
            if (direction === "asc") {
                return valA > valB ? 1 : valA < valB ? -1 : 0;
            } else {
                return valA < valB ? 1 : valA > valB ? -1 : 0;
            }
        });
    
        $rows.each(function() {
            $table.append($(this));
        })
    }
    
    $('.header_sort').on('click', function() {
        sortIndex($('#sort_type').val(), $(this).attr('data-direction'));
    });

    $('#sort_type').on('change', function() {
        sortIndex($('#sort_type').val(), $('#sort_type option:selected').attr('data-default-direction'));
    });

    // Show received items
	
	var queuedDialogs = [];
	
	function showNextDialog() {
		if (queuedDialogs.length > 0) {
			queuedDialogs.pop().dialog('open');
		}
	}

	$('#receiveCopy_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Thanks!': function() {
               	$(this).dialog("close");
               	showNextDialog();
			}
		}, open: function(event) {
		   	$('.ui-dialog-buttonpane').find('button:contains("Thanks!")').addClass('btn btn-save btn_font');
	        $('.ui-dialog-buttonpane').find('button').blur();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});
	
	if (receivedCount > 0) {
		$('#receiveCopy_text').html('<p><b>You have received ' + receivedCount + ' item' + ((receivedCount == 1) ? '' : 's') + ' from other users.</b></p><p>Received items will be indicated with a "New" tag.</p>');
		queuedDialogs.push($('#receiveCopy_dialog'));
	}

    // Set up "New Style Sheet" button

    $('#newItem_button').on('click', function() {
        $('#editSheet_dialog').removeAttr('data-sheet-id');

        $('#editSheet_dialog').dialog({ title: "Create style sheet" });
        $('#editSheet_dialog').dialog('open');
    });

    $('#newItem_button .tab_icon').css('background-image', 'url("/img/tab_icons/add_item_icon.svg")');
    $('#newItem_button .tab_text').html('New<br />Style Sheet');
    $('#newItem_button').show();
    	
	// Load style sheets

	initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});
		window.location.href = '/Documents';
	});

});

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as PDFLib from '/js/lib/pdf-lib/pdf-lib.bundle.mjs';

import { DFDocument } from '/js/FormEngine/DFDocument.js?7.0';
import { PSForm } from '/js/FormEngine/PSForm.js?7.0';

async function buildForm(options) {

	// Get individual pdfDocs for forms

	var pdfDocs = [];

    if ('format' in options) {

        var xmlText = options.format;
        const format = new DFDocument(xmlText);
        var version = format.getIntPropertyForName('version', 1);

        var thisData = [];
        if (version == 3) {
            thisData.push({
                qrText: '123456789012345'
            });
        }

        const psForm = new PSForm(format, thisData);

        await psForm.initForm();
		pdfDocs.push(psForm.pdfDoc);

    } else {

		const response = await ajaxFetch('/Documents/get_student_data', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(options)
		});

		const responseData = await response.json();

		if (responseData.success) {
			const studentData = responseData.student_data;

			for (const [documentID, thisData] of Object.entries(studentData)) {

				const response = await ajaxFetch('/Documents/get_xml/' + documentID);
				const responseData = await response.json(); 

				if (responseData.success) {

					const xmlText = responseData.formatXML;

					const format = new DFDocument(xmlText);
					const psForm = new PSForm(format, thisData);
	
					await psForm.initForm();
					pdfDocs.push(psForm.pdfDoc);

				} else {

					throw new Error('Ajax calls not successful.');
	
				}
			}

		} else {

			throw new Error("Invalid student data");

		}
    }

	// Get merged PDF

    const mergedPDF = await PDFLib.PDFDocument.create();

	for (let i = 0; i < pdfDocs.length; i++) {
		const copiedPages = await mergedPDF.copyPages(pdfDocs[i], pdfDocs[i].getPageIndices());
		copiedPages.forEach(page => mergedPDF.addPage(page));
	}    

	return await mergedPDF.save();
}

var initialize = async function() {

	$(document).ready(function() {
		$( "#progressbar" ).progressbar();
		$("#progressbar").progressbar("value", false);
	});
	
	var pdfBytes;
	
	if (data.pdfType == 'response_form') {
		data.target = 'response_form.pdf';

		if ('documentListsJSON' in data) {

			const documentListsJSON = data.documentListsJSON;
			pdfBytes = await buildForm({
				documentLists: documentListsJSON
			});	

		} else if ('formatXML' in data) {

			const formatXML = data.formatXML;
			pdfBytes = await buildForm({
				format: formatXML
			});	

		} else {

			throw new Error('Missing input data for response form PDF.');

		}

	} else if (data.pdfType == 'question_book') {
		data.target = 'assessment.pdf';

		var documentID = data.documentID;
		var formLocation = data.formLocation;

		var formPromise = false;
		if (formLocation != 'none') {
			var documentLists = {};
			var documentID = documentID;
			documentLists[documentID] = false;

			formPromise = buildForm({
				documentLists: JSON.stringify(documentLists)
			});
		}

		const questionsResponse = await ajaxFetch('/Documents/print_questions/' + documentID, {
			method: 'POST'
		});

		const contentType = questionsResponse.headers.get('content-type') || '';
		if (!contentType.includes('application/pdf')) {
			throw new Error('Unable to build PDF.');
		}

		var questionsBytes = await questionsResponse.arrayBuffer();

		if (formLocation != 'none') {
			const formBytes = await formPromise;

			const formPdf = await PDFLib.PDFDocument.load(formBytes);
			const questionsPdf = await PDFLib.PDFDocument.load(questionsBytes);

			const mergedPdf = await PDFLib.PDFDocument.create();

			if (formLocation === 'front') {

				const formPages = await mergedPdf.copyPages(formPdf, formPdf.getPageIndices());
				formPages.forEach(page => mergedPdf.addPage(page));
				if (formPdf.getPageCount() % 2 !== 0) {
					const lastFormPage = formPdf.getPage(formPdf.getPageCount() - 1);
					const { width, height } = lastFormPage.getSize();
					mergedPdf.addPage([width, height]);
				}	
				const questionPages = await mergedPdf.copyPages(questionsPdf, questionsPdf.getPageIndices());
				questionPages.forEach(page => mergedPdf.addPage(page));

			} else if (formLocation === 'back') {

				const questionPages = await mergedPdf.copyPages(questionsPdf, questionsPdf.getPageIndices());
				questionPages.forEach(page => mergedPdf.addPage(page));
				if (questionsPdf.getPageCount() % 2 !== 0) {
					const lastQuestionPage = questionsPdf.getPage(questionsPdf.getPageCount() - 1);
					const { width, height } = lastQuestionPage.getSize();
					mergedPdf.addPage([width, height]);
				}	
				const formPages = await mergedPdf.copyPages(formPdf, formPdf.getPageIndices());
				formPages.forEach(page => mergedPdf.addPage(page));

			}

			pdfBytes = await mergedPdf.save();

		} else pdfBytes = questionsBytes;

	} else if (data.pdfType == 'pdf_from_url') {

		var postData = { method: 'POST' };
		if ('postDataJSON' in data) {
			postData.headers = { 'Content-Type': 'application/json' };
			postData.body = data.postDataJSON;
		}
		const response = await ajaxFetch(data.url, postData);

		const contentType = response.headers.get('content-type') || '';
		if (!contentType.includes('application/pdf')) {
			throw new Error('Unable to build PDF.');
		}

		pdfBytes = await response.arrayBuffer();

	} else {

		throw new Error(`Unexpected PDF type '${data.pdfType}'.`);

	}

	// Save generated PDF to S3 and forward to the link

	const formData = new FormData();
	formData.append('data[pdf]', new Blob(
		[pdfBytes], 
		{ type: 'application/pdf' }
	), data.target);

	$('#progresstext').html('Saving PDF...');

	const saveResponse = await ajaxFetch('/DefaultPages/save_pdf', {
		method: 'POST',
		body: formData
	});

	const responseData = await saveResponse.json();	

	if (responseData.success) {
		window.location.replace(responseData.url);
	} else {
		throw new Error('Failed to save PDF.');
	};

}

document.addEventListener('DOMContentLoaded', async function() {
    try {

        await initialize();

    } catch (error) {

		if (window.opener && !window.opener.closed) {
			if (window.opener.jQuery && window.opener.jQuery.gritter) {
				window.opener.jQuery.gritter.add({
					title: "Error",
					text: error.message,
					image: "/img/error.svg"
				});
			}
		}
		window.close();

	}
});
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export function getSimpleProperty(node) {
    const keys = Object.keys(node);
    if (keys.length === 1 && Array.isArray(node[keys[0]])) {
        if (node[keys[0]].length === 1 && '#text' in node[keys[0]][0]) {
            return { key: keys[0], value: String(node[keys[0]][0]['#text']) };
        } else if (node[keys[0]].length === 0) {
            return { key: keys[0], value: '' };
        } else return false;
    }
    return false;
}

export class DFElement {
	constructor() {
		this.valid = true;
		this.properties = new Map();
	}

	isValid() {
		return this.valid;
	}

	hasProperty(name) {
		return this.properties.has(name);
	}

	setPropertyForName(property, name) {
		this.properties.set(name, property);
	}

	getStringPropertyForName(name, defaultValue = '') {
		if (!this.properties.has(name)) return defaultValue;
		else return this.properties.get(name);
	}

	getIntPropertyForName(name, defaultValue = false) {
		if (!this.properties.has(name)) return defaultValue;
		else return parseInt(this.properties.get(name), 10);
	}

	getDoublePropertyForName(name, defaultValue = false) {
		if (!this.properties.has(name)) return defaultValue;
		else return parseFloat(this.properties.get(name));
	}
}

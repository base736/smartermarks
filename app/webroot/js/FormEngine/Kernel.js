/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export const KE__ = 0;
export const KE_0 = 1;
export const KE_1 = 2;

export class Kernel {
    constructor(numEntries, ...entries) {
        this.data = Array.from({ length: 4 }, () => new Array(9).fill(KE__));

        for (let i = 0; i < 9; ++i) {
            this.data[0][i] = entries[i];
        }

        for (let rotation = 1; rotation < 4; ++rotation) {
            for (let i = 0; i < 3; ++i) {
                this.data[rotation][0 + i] = this.data[rotation - 1][6 - 3 * i];
                this.data[rotation][3 + i] = this.data[rotation - 1][7 - 3 * i];
                this.data[rotation][6 + i] = this.data[rotation - 1][8 - 3 * i];
            }
        }
    }

    dataForRotation(rotation) {
        if (rotation < 0 || rotation >= 4) return null;
        return this.data[rotation];
    }
}

export function kernelSkeletonizeA() {
    return new Kernel(9, KE_0, KE_0, KE_0, KE__, KE_1, KE__, KE_1, KE_1, KE_1);
}

export function kernelSkeletonizeB() {
    return new Kernel(9, KE__, KE_0, KE_0, KE_1, KE_1, KE_0, KE_1, KE_1, KE__);
}

export function kernelPruneA() {
    return new Kernel(9, KE_0, KE_0, KE_0, KE_0, KE_1, KE_0, KE_0, KE__, KE__);
}

export function kernelPruneB() {
    return new Kernel(9, KE_0, KE_0, KE_0, KE_0, KE_1, KE_0, KE__, KE__, KE_0);
}

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { DFElement, getSimpleProperty } from './DFElement.js?7.0';
import { DFPage } from './DFPage.js?7.0';
import { localizationInstance } from './Localization.js?7.0';

export class DFDocument extends DFElement {

    static parseXML(xmlString) {

        let index = 0;

        function skipWhitespace() {
            while (xmlString[index] && /\s/.test(xmlString[index])) index++;
        }

        function parseProcessingInstruction() {
            while (xmlString[index] && xmlString[index] !== '>') {
                index++;
            }
            if (xmlString[index] === '>') index++; // Skip '>'

            return null;
        }

        function parseTag() {
            skipWhitespace();
            if (xmlString[index] !== '<') return null;
            index++; // Skip '<'

            // Parse tag name
            let tagName = '';
            while (xmlString[index] && /[^\s/>]/.test(xmlString[index])) {
                tagName += xmlString[index++];
            }
            tagName = tagName.trim();
            
            // Parse attributes
            let attributes = {};
            while (xmlString[index] && xmlString[index] !== '>') {
                skipWhitespace();

                // Parse attribute name
                let attrName = '';
                while (xmlString[index] && /[^\s=>]/.test(xmlString[index])) {
                    attrName += xmlString[index++];
                }
                attrName = attrName.trim();

                skipWhitespace();

                if (xmlString[index] === '=') {
                    index++; // Skip '='
                    skipWhitespace();

                    // Parse attribute value
                    let quote = xmlString[index];
                    if (quote === '"' || quote === "'") {
                        index++; // Skip opening quote
                        let attrValue = '';
                        while (xmlString[index] && xmlString[index] !== quote) {
                            attrValue += xmlString[index++];
                        }
                        index++; // Skip closing quote
                        attributes[attrName] = attrValue;
                    }
                }
            }

            // Check for self-closing tag
            let selfClosing = (xmlString[index - 1] === '/');

            if (xmlString[index] === '>') index++; // Skip '>'

            let children = [];

            if (!selfClosing) {
                while (true) {
                    skipWhitespace();
                    if (xmlString[index] === '<' && xmlString[index + 1] === '/') {

                        // Found closing tag. Read it and leave the loop.

                        index += 2; // Skip '</'
                        while (xmlString[index] && xmlString[index] !== '>') {
                            index++;
                        }
                        if (xmlString[index] === '>') index++; // Skip '>'
                        break;
                        
                    } else {

                        // Parse child node

                        let child = parseNode();
                        if (child !== null) children.push(child);

                    }
                }
            }

            // Include attributes if present
            if (Object.keys(attributes).length > 0) {
                children.push({ '#attributes': attributes });
            }

            return { [tagName]: children };
        }

        function parseText() {
            let text = '';
            while (xmlString[index] && xmlString[index] !== '<') {
                text += xmlString[index++];
            }
            text = text.trim();
            if (text.length > 0) {
                return { '#text': text };
            } else {
                return null;
            }
        }

        function parseNode() {
            skipWhitespace();
            if (xmlString[index] === '<') {
                if (xmlString[index + 1] === '?') {
                    return parseProcessingInstruction();
                } else {
                    return parseTag();
                }
            } else {
                return parseText();
            }
        }

        let rootNode = null;
        while (index < xmlString.length) {
            let node = parseNode();
            if (node !== null) {
                rootNode = node;
                break;
            }
        }

        return rootNode;
    }
    
	constructor(xmlString) {
		super();
    
		this.pages = [];
		this.hasManualNumbering = false;

        // Parse XML string

        var rootNode = false;
        try {
            rootNode = DFDocument.parseXML(xmlString);
        } catch (error) {
            throw new Error("Error parsing XML");
        }
        
        // Read document data

		if ('document' in rootNode) {
            let documentNode = rootNode.document;

			let sectionNum = 1;
            documentNode.forEach(thisElement => {
                if ('page' in thisElement) {

                    const childNode = thisElement.page;
                    const newPage = new DFPage(childNode, this, sectionNum);
                    if (newPage.isValid()) {
                        this.pages.push(newPage);
                        sectionNum += newPage.getNumSections();
                    } else this.valid = false;

                } else {

                    const property = getSimpleProperty(thisElement);
                    if (property !== false) {
                        if (property.key === 'language') {
                            localizationInstance.setLanguage(property.value);
                        } else {
                            this.setPropertyForName(property.value, property.key);
                        }
                    } else this.valid = false;

                }
			});

			const questions = [];
			this.pages.forEach(page => {
				const numSections = page.getNumSections();
				for (let j = 0; j < numSections; j++) {
					const section = page.sectionAtIndex(j);
					const numQuestions = section.getNumQuestions();
					for (let k = 0; k < numQuestions; k++) {
						const question = section.questionAtIndex(k);
						questions.push(question);
					}
				}
			});

			const questionNumbers = [];
			let hasDuplicateNumber = false;
			questions.forEach(question => {
				const thisNumber = question.getQuestionNumber();
				if (questionNumbers.includes(thisNumber)) {
					hasDuplicateNumber = true;
				} else {
					questionNumbers.push(thisNumber);
				}
			});

			if (hasDuplicateNumber) {
				questions.forEach(question => {
					const thisNumber = question.getQuestionNumber();
					const section = question.getSection();
					const buffer = `${section.getSectionNum()}.${thisNumber}`;
					question.setQuestionNumber(buffer);
				});
			}
		} else {
			console.warn(`WARNING: No document tag in XML.`);
		}
	}

	getNumPages() {
		return this.pages.length;
	}

	pageAtIndex(index) {
		return index < this.pages.length ? this.pages[index] : null;
	}

	setHasManualNumbering(newValue) {
		this.hasManualNumbering = newValue;
	}

	getHasManualNumbering() {
		return this.hasManualNumbering;
	}
}

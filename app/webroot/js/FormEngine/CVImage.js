/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { KE_0, KE_1 } from './Kernel.js?7.0';

export class CVImage {

    static cannyMaxDepth = 100;
    static edgeXOffset = [1, 1, 0, -1, -1, -1, 0, 1];
    static edgeYOffset = [0, 1, 1, 1, 0, -1, -1, -1];
    static NO_EDGE = 0.0;
    static POSSIBLE_EDGE = 0.5;
    static EDGE = 1.0;
  
    constructor(initWidth = false, initHeight = false) {
        this.resize(initWidth, initHeight);
    }
    
    copy() {
        const newImage = new CVImage(this.width, this.height);
        newImage.data.set(this.data);
        return newImage;
    }

    resize(newWidth, newHeight) {
        this.width = newWidth;
        this.height = newHeight;
        if (newWidth && newHeight) {
            this.data = new Float32Array(newWidth * newHeight);
            this.line = new Float32Array(Math.max(newWidth, newHeight));    
        } else {
            this.data = false;
            this.line = false;
        }
    }

    readFromContext(context) {
        var width = context.canvas.width;
        var height = context.canvas.height;

        const imageData = context.getImageData(0, 0, width, height);
        this.resize(width, height);
        
        for (let i = 0; i < imageData.data.length; i += 4) {
            const r = imageData.data[i];
            const g = imageData.data[i + 1];
            const b = imageData.data[i + 2];
            // Convert to grayscale using luminance
            const grayscale = 0.299 * r + 0.587 * g + 0.114 * b;
            this.data[i / 4] = grayscale / 255.0;
        }
    }

    writeToContext(context) {
        const imageData = context.createImageData(this.width, this.height);
        
        for (let i = 0; i < this.data.length; i++) {
            const value = Math.floor(this.data[i] * 255);
            imageData.data[i * 4] = value;     // R
            imageData.data[i * 4 + 1] = value; // G
            imageData.data[i * 4 + 2] = value; // B
            imageData.data[i * 4 + 3] = 255;   // A
        }
        
        context.putImageData(imageData, 0, 0);
    }

    fillWith(value) {
        this.data.fill(value);
    }

    multiplyValuesBy(r) {
        this.data = this.data.map(value => value * r);
    }

    transformValues(transform) {
        this.data = this.data.map(transform);
    }

    addTo(target) {
        if (target.width !== this.width || target.height !== this.height) return;
        this.data.forEach((value, index) => {
            target.data[index] += value;
        });
    }

    reduceByFactor(factor) {
        const newWidth = Math.floor(this.width / factor);
        const newHeight = Math.floor(this.height / factor);
        const scale = 1.0 / (factor * factor);
        const newData = new Float32Array(newWidth * newHeight);

        for (let j = 0; j < newHeight; ++j) {
            for (let i = 0; i < newWidth; ++i) {
                let newValue = 0.0;
                for (let p = 0; p < factor; ++p) {
                    for (let q = 0; q < factor; ++q) {
                        newValue += this.data[(factor * j + p) * this.width + (factor * i + q)];
                    }
                }
                newData[j * newWidth + i] = newValue * scale;
            }
        }

        this.width = newWidth;
        this.height = newHeight;
        this.data = newData;
        return true;
    }

    enlargeByFactor(factor, target, a) {
        const newWidth = (this.width - 2 * a - 1) * factor + 1;
        const newHeight = (this.height - 2 * a - 1) * factor + 1;
        target.resize(newWidth, newHeight);
        target.fillWith(0.0);

        const sincSize = factor * a + 1;
        const sinc = new Float32Array(sincSize);
        sinc[0] = 1.0;
        for (let i = 1; i < sincSize; ++i) {
            const x = Math.PI * i / factor;
            sinc[i] = (Math.sin(x) / x) * (Math.sin(x / a) / (x / a));
        }

        for (let q = 0; q < this.height; ++q) {
            const minJ = Math.max((q - 2 * a) * factor, 0);
            const maxJ = Math.min(q * factor, newHeight - 1);

            for (let p = 0; p < this.width; ++p) {
                const minI = Math.max((p - 2 * a) * factor, 0);
                const maxI = Math.min(p * factor, newWidth - 1);

                for (let j = minJ; j <= maxJ; ++j) {
                    const yIndex = Math.abs((q - a) * factor - j);
                    if (yIndex >= sincSize) throw new Error('yIndex out of bounds');
                    for (let i = minI; i <= maxI; ++i) {
                        const xIndex = Math.abs((p - a) * factor - i);
                        if (xIndex >= sincSize) throw new Error('xIndex out of bounds');
                        target.data[j * newWidth + i] += sinc[xIndex] * sinc[yIndex] * this.data[q * this.width + p];
                    }
                }
            }
        }

        return true;
    }

    rotateRight() {
        const newWidth = this.height;
        const newHeight = this.width;
        const rotatedData = new Float32Array(newWidth * newHeight);

        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                const srcIndex = y * this.width + x;
                const destX = this.height - 1 - y;
                const destY = x;
                const destIndex = destY * newWidth + destX;
                rotatedData[destIndex] = this.data[srcIndex];
            }
        }

        // Update the image properties
        this.width = newWidth;
        this.height = newHeight;
        this.data = rotatedData;
    }

    rotateLeft() {
        const newWidth = this.height;
        const newHeight = this.width;
        const rotatedData = new Float32Array(newWidth * newHeight);

        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                const srcIndex = y * this.width + x;
                const destX = y;
                const destY = this.width - 1 - x;
                const destIndex = destY * newWidth + destX;
                rotatedData[destIndex] = this.data[srcIndex];
            }
        }

        // Update the image properties
        this.width = newWidth;
        this.height = newHeight;
        this.data = rotatedData;
    }

    rotate180() {
        const imageBins = this.width * this.height;
        let a = 0;
        let b = imageBins - 1;
        while (a < b) {
            const temp = this.data[a];
            this.data[a] = this.data[b];
            this.data[b] = temp;
            a++;
            b--;
        }
    }
    
    clipToRange(min, max) {
        for (let i = 0; i < this.data.length; i++) {
            if (this.data[i] > max) this.data[i] = max;
            if (this.data[i] < min) this.data[i] = min;
        }
    }
    
    setValueRange(min, max) {
        const imageBins = this.width * this.height;
        let currentMin = this.data[0];
        let currentMax = this.data[0];
    
        for (let i = 1; i < imageBins; i++) {
            if (this.data[i] > currentMax) currentMax = this.data[i];
            if (this.data[i] < currentMin) currentMin = this.data[i];
        }
    
        const scale = currentMax === currentMin ? 0 : (max - min) / (currentMax - currentMin);
    
        for (let i = 0; i < imageBins; i++) {
            this.data[i] = (this.data[i] - currentMin) * scale + min;
        }
    }
    
    scaleToMax(max) {
        let currentMax = this.data[0];
    
        for (let i = 1; i < this.data.length; i++) {
            if (this.data[i] > currentMax) currentMax = this.data[i];
        }
    
        const scale = max / currentMax;
    
        for (let i = 0; i < this.data.length; i++) {
            this.data[i] *= scale;
        }
    }
    
    thresholdAtValue(threshold) {
        for (let i = 0; i < this.data.length; i++) {
            this.data[i] = this.data[i] < threshold ? 0.0 : 1.0;
        }
    }
    
    invert() {
        for (let i = 0; i < this.data.length; i++) {
            this.data[i] = 1.0 - this.data[i];
        }
    }

    thinWithKernels(kernels, numKernels, maxIterations) {
        // Ensure the image is black and white
        let numWhite = 0;
        for (let j = 1; j < this.height - 1; ++j) {
            for (let i = 1; i < this.width - 1; ++i) {
                const value = this.data[j * this.width + i];
                if (value !== 0.0 && value !== 1.0) {
                    throw new Error("ERROR: Image input to thinWithKernels is not black and white.");
                } else if (value === 1.0) {
                    numWhite++;
                }
            }
        }
    
        // Build a list of white pixels in the image
        var whitePixels = new Int32Array(numWhite);
        let w = 0;
        for (let j = 1; j < this.height - 1; ++j) {
            for (let i = 1; i < this.width - 1; ++i) {
                if (this.data[j * this.width + i] === 1.0) {
                whitePixels[w++] = j * this.width + i;
                }
            }
        }
    
        // Shuffle the list to avoid bias in the thinning
        for (let i = numWhite - 1; i > 0; --i) {
            const j = Math.floor(Math.random() * i);
            [whitePixels[i], whitePixels[j]] = [whitePixels[j], whitePixels[i]];
        }
    
        // Build an array of kernel data
        const numElements = 4 * numKernels;
        const seData = new Array(numElements * 9);
        let seIndex = 0;
        for (let i = 0; i < numKernels; ++i) {
            for (let j = 0; j < 4; ++j) {
                const thisData = kernels[i].dataForRotation(j);
                for (let k = 0; k < 9; ++k) {
                    seData[seIndex++] = thisData[k];
                }
            }
        }
    
        // Apply the kernels until convergence
        const imageBuffer = new Array(9);
        let iterationCount = -1;
        let done;
        do {
            done = true;
            if (++iterationCount === maxIterations) break;
        
            for (let i = 0; i < numWhite; ++i) {
                const dIndex = whitePixels[i];
                if (this.data[dIndex] === 1.0) {
                    let bIndex = 0;
                    for (let deltaJ = -1; deltaJ <= 1; ++deltaJ) {
                        for (let deltaI = -1; deltaI <= 1; ++deltaI) {
                            imageBuffer[bIndex++] = Math.abs(this.data[dIndex + deltaJ * this.width + deltaI]);
                        }
                    }
            
                    for (let k = 0; k < numElements; ++k) {
                        bIndex = 0;
                        let seMatched;
                        for (seMatched = 0; seMatched < 9; ++seMatched) {
                            const seValue = seData[k * 9 + seMatched];
                            const imageValue = imageBuffer[bIndex++];
                            if ((seValue === KE_0 && imageValue !== 0.0) || (seValue === KE_1 && imageValue !== 1.0)) break;
                        }
            
                        if (seMatched === 9) {
                            done = false;
                            this.data[dIndex] = 0.0;
                            whitePixels[i] = -1;
                            break;
                        }
                    }
            
                    if (this.data[dIndex] === 1.0) {
                        this.data[dIndex] = -1.0;
                    } else {
                        for (let deltaJ = -1; deltaJ <= 1; ++deltaJ) {
                            for (let deltaI = -1; deltaI <= 1; ++deltaI) {
                                const sIndex = dIndex + deltaJ * this.width + deltaI;
                                if (this.data[sIndex] === -1.0) this.data[sIndex] = 1.0;
                            }
                        }
                    }
                }
            }
        
            // Remove pixels from the white list that are now black

            let newNumWhite = 0;
            for (let i = 0; i < numWhite; ++i) {
                if (whitePixels[i] > 0) ++newNumWhite;
            }

            let newWhitePixels = new Int32Array(newNumWhite);
            let offset = 0;
            for (let i = 0; i < numWhite; ++i) {
                if (whitePixels[i] > 0) {
                    newWhitePixels[offset++] = whitePixels[i];
                }
            }

            whitePixels = newWhitePixels;
            numWhite = newNumWhite;
    
        } while (!done);
    
        // Restore inactive pixels to their original color
        
        for (let i = 0; i < numWhite; ++i) {
            if (this.data[whitePixels[i]] === -1.0) this.data[whitePixels[i]] = 1.0;
        }
    }

    convolve(mask, transform) {
        transform.resize(this.width, this.height);
        transform.fillWith(0.0);
    
        const radius = Math.floor((mask.width - 1) / 2);
    
        let m = mask.data
        let d = transform.data

        for (let j = radius; j < this.height - radius; ++j) {
            for (let i = radius; i < this.width - radius; ++i) {
                const thisPixel = this.data[j * this.width + i];
                if (thisPixel === 0.0) continue;
        
                let mOffset = 0;
                for (let deltaJ = -radius; deltaJ <= radius; ++deltaJ) {
                    let dOffset = (j + deltaJ) * this.width + i - radius;
                    for (let deltaI = -radius; deltaI <= radius; ++deltaI) {
                        d[dOffset++] += m[mOffset++] * thisPixel;
                    }
                }
            }
        }
    }
    
    linearHoughTransform(minAngle, maxAngle, aResolution, minDistance, maxDistance, dResolution) {
        const cosTable = new Array(aResolution);
        const sinTable = new Array(aResolution);
        for (let a = 0; a < aResolution; ++a) {
            const angle = a * (maxAngle - minAngle) / aResolution + minAngle;
            cosTable[a] = Math.cos(angle);
            sinTable[a] = Math.sin(angle);
        }
    
        const transform = new CVImage(aResolution, dResolution);
        const d = transform.data
    
        const transformBins = aResolution * dResolution;
        for (let i = 0; i < transformBins; ++i) d[i] = 0.0;
    
        const dScale = dResolution / (maxDistance - minDistance);
    
        for (let j = 0; j < this.height; ++j) {
            for (let i = 0; i < this.width; ++i) {
                const thisPixel = this.data[j * this.width + i];
                if (thisPixel === 0.0) continue;
        
                let lastIndex = 0;
                for (let a = 0; a < aResolution; ++a) {
                    const thisValue = i * cosTable[a] + j * sinTable[a];
                    let thisIndex = Math.floor((thisValue - minDistance) * dScale);
                    thisIndex = Math.max(0, Math.min(dResolution - 1, thisIndex));
            
                    if (a === 0) lastIndex = thisIndex;
                    const startIndex = Math.min(lastIndex, thisIndex);
                    const endIndex = Math.max(lastIndex, thisIndex);
            
                    for (let k = startIndex; k <= endIndex; ++k) {
                        d[k * aResolution + a] += thisPixel;
                    }
            
                    lastIndex = thisIndex;
                }
            }
        }
    
        return transform;
    }

    applyGaussian(sigma) {
        const windowSize = 1 + 2 * Math.ceil(2.5 * sigma);
        const kernel = this.makeGaussianKernel(sigma, windowSize);
        const center = Math.floor(windowSize / 2);

        for (let r = 0; r < this.height; ++r) {
            this.line.fill(0.0);

            for (let c = center; c < this.width - center; ++c) {
                if (this.data[r * this.width + c] === 0.0) continue;
                for (let cc = -center; cc <= center; ++cc) {
                    this.line[c + cc] += this.data[r * this.width + c] * kernel[center + cc];
                }
            }

            for (let c = 0; c < this.width; ++c) {
                this.data[r * this.width + c] = this.line[c];
            }
        }

        for (let c = 0; c < this.width; ++c) {
            this.line.fill(0.0);

            for (let r = center; r < this.height - center; ++r) {
                if (this.data[r * this.width + c] === 0.0) continue;
                for (let rr = -center; rr <= center; ++rr) {
                    this.line[r + rr] += this.data[r * this.width + c] * kernel[center + rr];
                }
            }

            for (let r = 0; r < this.height; ++r) {
                this.data[r * this.width + c] = this.line[r];
            }
        }
    }

    makeGaussianKernel(sigma, windowSize) {
        const kernel = new Float32Array(windowSize);
        const center = Math.floor(windowSize / 2);
        let sum = 0.0;

        for (let i = 0; i < windowSize; ++i) {
            const x = i - center;
            kernel[i] = Math.exp(-0.5 * (x / sigma) ** 2);
            sum += kernel[i];
        }

        for (let i = 0; i < windowSize; ++i) {
            kernel[i] /= sum;
        }

        return kernel;
    }

    getDerivatives(image, delta_x, delta_y, magnitude) {
        const rows = image.height;
        const cols = image.width;
    
        delta_x.resize(cols, rows);
        delta_y.resize(cols, rows);
        magnitude.resize(cols, rows);
    
        // Compute the x derivative
        for (let r = 0; r < rows; r++) {
            const d = delta_x.data
            const s = image.data
        
            d[r * cols] = s[r * cols + 1] - s[r * cols];
            for (let c = 1; c < cols - 1; c++) {
                d[r * cols + c] = 0.5 * (s[r * cols + c + 1] - s[r * cols + c - 1]);
            }
            d[r * cols + cols - 1] = s[r * cols + cols - 1] - s[r * cols + cols - 2];
        }
    
        // Compute the y derivative
        for (let c = 0; c < cols; c++) {
            const d = delta_y.data
            const s = image.data
        
            d[c] = s[cols + c] - s[c];
            for (let r = 1; r < rows - 1; r++) {
                d[r * cols + c] = 0.5 * (s[(r + 1) * cols + c] - s[(r - 1) * cols + c]);
            }
            d[(rows - 1) * cols + c] = s[(rows - 1) * cols + c] - s[(rows - 2) * cols + c];
        }
    
        // Get magnitude
        const x = delta_x.data
        const y = delta_y.data
        const m = magnitude.data
        const size = rows * cols;
        for (let i = 0; i < size; i++) {
            const x2 = Math.pow(x[i], 2.0);
            const y2 = Math.pow(y[i], 2.0);
            m[i] = Math.sqrt(x2 + y2);
        }
    }
    
    canny(tLow, tHigh) {
        const delta_x = new CVImage();
        const delta_y = new CVImage();
        const magnitude = new CVImage();
        this.getDerivatives(this, delta_x, delta_y, magnitude);
    
        const edge = new CVImage();
        non_max_supp(edge, magnitude, delta_x, delta_y);
        apply_hysteresis(edge, magnitude, tLow, tHigh);
    
        return edge;
    }
}

function follow_edges(nms, mag, pos, lowval, cols, depth) {
    if (depth >= CVImage.cannyMaxDepth) return;

    const nmsData = nms.data;
    const magData = mag.data;

    for (let i = 0; i < 8; i++) {
        const offset = pos - CVImage.edgeYOffset[i] * cols + CVImage.edgeXOffset[i];
        if (nmsData[offset] == CVImage.POSSIBLE_EDGE && magData[offset] > lowval) {
            nmsData[offset] = CVImage.EDGE;
            follow_edges(nms, mag, offset, lowval, cols, depth + 1);
        }
    }
}

export function apply_hysteresis(nms, mag, tLow, tHigh) {
    const rows = mag.height;
    const cols = mag.width;
    const nmsData = nms.data
    const magData = mag.data

    for (let r = 0, pos = 0; r < rows; r++, pos += cols) {
        nmsData[pos] = CVImage.NO_EDGE;
        nmsData[pos + cols - 1] = CVImage.NO_EDGE;
    }
    for (let c = 0, pos = (rows - 1) * cols; c < cols; c++, pos++) {
        nmsData[c] = CVImage.NO_EDGE;
        nmsData[pos] = CVImage.NO_EDGE;
    }

    for (let r = 0, pos = 0; r < rows; r++) {
        for (let c = 0; c < cols; c++, pos++) {
            if (nmsData[pos] == CVImage.POSSIBLE_EDGE && magData[pos] >= tHigh) {
                nmsData[pos] = CVImage.EDGE;
                follow_edges(nms, mag, pos, tLow, cols, 0);
            }
        }
    }

    for (let r = 0, pos = 0; r < rows; r++) {
        for (let c = 0; c < cols; c++, pos++) {
            if (nmsData[pos] != CVImage.EDGE) nmsData[pos] = CVImage.NO_EDGE;
        }
    }
}

export function non_max_supp(mag, gradx, grady) {
    const nrows = mag.height;
    const ncols = mag.width;
    let result = new CVImage(ncols, nrows);

    const magData = mag.data
    const gradxData = gradx.data
    const gradyData = grady.data
    const resultData = result.data

    // Zero the edges of the result image
    for (let count = 0; count < ncols; count++) {
        resultData[count] = resultData[count + ncols * (nrows - 1)] = 0.0;
    }
    for (let count = 0; count < nrows; count++) {
        resultData[count * ncols] = resultData[(count + 1) * ncols - 1] = 0.0;
    }

    // Suppress non-maximum points
    for (let rowcount = 1; rowcount < nrows - 2; rowcount++) {
        for (let colcount = 1; colcount < ncols - 2; colcount++) {
            const pos = rowcount * ncols + colcount;
            const m00 = magData[pos];
            if (m00 === 0) {
                resultData[pos] = CVImage.NO_EDGE;
                continue;
            }
    
            const gx = gradxData[pos];
            const gy = gradyData[pos];
            const xperp = -gx / m00;
            const yperp = gy / m00;
    
            let mag1, mag2, z1, z2;
    
            if (gx >= 0) {
                if (gy >= 0) {
                    if (gx >= gy) {
                        // 111
                        z1 = magData[pos - 1];
                        z2 = magData[pos - ncols - 1];
                        mag1 = (m00 - z1) * xperp + (z2 - z1) * yperp;
                        z1 = magData[pos + 1];
                        z2 = magData[pos + ncols + 1];
                        mag2 = (m00 - z1) * xperp + (z2 - z1) * yperp;
                    } else {
                        // 110
                        z1 = magData[pos - ncols];
                        z2 = magData[pos - ncols - 1];
                        mag1 = (z1 - z2) * xperp + (z1 - m00) * yperp;
                        z1 = magData[pos + ncols];
                        z2 = magData[pos + ncols + 1];
                        mag2 = (z1 - z2) * xperp + (z1 - m00) * yperp;
                    }
                } else {
                    if (gx >= -gy) {
                        // 101
                        z1 = magData[pos - 1];
                        z2 = magData[pos + ncols - 1];
                        mag1 = (m00 - z1) * xperp + (z1 - z2) * yperp;
                        z1 = magData[pos + 1];
                        z2 = magData[pos - ncols + 1];
                        mag2 = (m00 - z1) * xperp + (z1 - z2) * yperp;
                    } else {
                        // 100
                        z1 = magData[pos + ncols];
                        z2 = magData[pos + ncols - 1];
                        mag1 = (z1 - z2) * xperp + (m00 - z1) * yperp;
                        z1 = magData[pos - ncols];
                        z2 = magData[pos - ncols + 1];
                        mag2 = (z1 - z2) * xperp + (m00 - z1) * yperp;
                    }
                }
            } else {
                if (gy >= 0) {
                    if (-gx >= gy) {
                        // 011
                        z1 = magData[pos + 1];
                        z2 = magData[pos - ncols + 1];
                        mag1 = (z1 - m00) * xperp + (z2 - z1) * yperp;
                        z1 = magData[pos - 1];
                        z2 = magData[pos + ncols - 1];
                        mag2 = (z1 - m00) * xperp + (z2 - z1) * yperp;
                    } else {
                        // 010
                        z1 = magData[pos - ncols];
                        z2 = magData[pos - ncols + 1];
                        mag1 = (z2 - z1) * xperp + (z1 - m00) * yperp;
                        z1 = magData[pos + ncols];
                        z2 = magData[pos + ncols - 1];
                        mag2 = (z2 - z1) * xperp + (z1 - m00) * yperp;
                    }
                } else {
                    if (-gx > -gy) {
                        // 001
                        z1 = magData[pos + 1];
                        z2 = magData[pos + ncols + 1];
                        mag1 = (z1 - m00) * xperp + (z1 - z2) * yperp;
                        z1 = magData[pos - 1];
                        z2 = magData[pos - ncols - 1];
                        mag2 = (z1 - m00) * xperp + (z1 - z2) * yperp;
                    } else {
                        // 000
                        z1 = magData[pos + ncols];
                        z2 = magData[pos + ncols + 1];
                        mag1 = (z2 - z1) * xperp + (m00 - z1) * yperp;
                        z1 = magData[pos - ncols];
                        z2 = magData[pos - ncols - 1];
                        mag2 = (z2 - z1) * xperp + (m00 - z1) * yperp;
                    }
                }
            }
    
            // Determine if the current point is a maximum point
            if (mag1 > 0.0 || mag2 > 0.0) {
                resultData[pos] = CVImage.NO_EDGE;
            } else {
                resultData[pos] = mag2 === 0.0 ? CVImage.NO_EDGE : CVImage.POSSIBLE_EDGE;
            }
        }
    }

    return result;
}
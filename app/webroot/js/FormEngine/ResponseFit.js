/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { DFDocument } from './DFDocument.js?7.0';
import { PSForm } from './PSForm.js?7.0';
import { OMREngine, MessageEntry, OMRMessage } from './OMREngine.js?7.0';

export class ResponseFit {
    static parameters = false;

    async setFormatXML(formatXML) {
        const format = new DFDocument(formatXML);
        if (!format.isValid()) {
            console.error("ERROR: Unable to create DFDocument.");
            throw new Error("Unable to create DFDocument");
        }

        const studentData = [];
        let version = format.getIntPropertyForName('version', 1);

        if (version === 3) {
            studentData.push({ qrText: "312345678912345" });
        }

        const psForm = new PSForm(format, studentData);
        await psForm.initForm();

        // Set up questions, page offsets, section names, and section offsets
        const questions = [];
        const pageOffsets = [];
        const sectionNames = [];
        const sectionOffsets = [];

        for (let i = 0; i < format.getNumPages(); i++) {
            const pageFormat = format.pageAtIndex(i);

            const title = pageFormat.getTitle();
            if (title) {
                if (title.hasIDField()) sectionNames.push(title.idLabel());
                if (title.hasNameField()) sectionNames.push(title.nameLabel());
            }

            for (let j = 0; j < pageFormat.getNumSections(); j++) {
                const section = pageFormat.sectionAtIndex(j);
                sectionNames.push(section.getStringPropertyForName('header', ''));

                for (let k = 0; k < section.getNumQuestions(); k++) {
                    const question = section.questionAtIndex(k);
                    questions.push(question);
                }
            }
        }

        let pagePoints = 0;
        let blockModels = [];
        for (let i = 0; i < psForm.getNumPages(); i++) {
            const formModel = psForm.getBlockModel(i);
            if (formModel.length == 0) continue;          // Skip blank pages

            blockModels.push(formModel);

            for (let j = 0; j < formModel.length; j++) {
                pagePoints += formModel[j].indices.length;
            }
            pageOffsets.push(pagePoints);

            sectionOffsets.push(psForm.getSectionOffset(i));
        }

        this.formData = {
            formatXML,
            version,
            format,
            blockModels,
            questions,
            pageOffsets,
            sectionNames,
            sectionOffsets
        };
        this.reset();
    }

    async readPage(image, scanPage) {
        if (this.abortMessage !== false) return false;
        if (this.allPagesFitted()) return false;

        let requestedPage = -1;
        for (let i = 0; i < this.pageOrder.length; ++i) {
            if (this.pageOrder[i] === -1) {
                if (requestedPage === -1) requestedPage = i;
            }
        }
        if (requestedPage === -1) requestedPage = 0;

        let fittedPage = requestedPage;

        const omrResults = OMREngine.findBubbles(this.formData.blockModels, fittedPage, image);
        fittedPage = omrResults.fittedPage;
        const omrPoints = omrResults.points;
        this.badBubblesCount += omrResults.badBubblesCount;
        const messages = omrResults.messages;

        let success = true;

        if (fittedPage === -1) {
            if (++this.badPages === 3) {
                const startPage = scanPage - this.badPages + 1;
                this.abortMessage = `Too many bad pages starting at page ${startPage}. Check uploaded PDF.`;
            }
            success = false;
        } else {
            this.badPages = 0;

            if (Math.abs(fittedPage - requestedPage) > 1) {
                this.abortMessage = `Unrecoverable page order error at page ${scanPage}`;
                success = false;
            } else if (this.pageOrder[fittedPage] !== -1) {
                let oddOnly = this.isOddOnly();
                if ((this.formPages > 1) && (scanPage <= this.formPages) && oddOnly) {
                    this.abortMessage = "Possible single-sided copy of two-sided form";
                } else {
                    this.abortMessage = `Unrecoverable page order error at page ${scanPage}`;
                }
                success = false;
            }
        }

        if (success) {
            this.pagePixelValues[fittedPage] = omrPoints.map(point => OMREngine.readBubble(point, image));

            if (fittedPage !== requestedPage) {
                const message = new MessageEntry(OMRMessage.OMRWarningBadPageOrder);
                const messageJSON = JSON.stringify(message);
                if (!(messageJSON in this.warnings)) this.warnings[messageJSON] = new Set();
                this.warnings[messageJSON].add(scanPage);
            }

            this.pageOrder[fittedPage] = scanPage;

            messages.forEach(entry => {
                if (entry.sectionIndex !== -1) {
                    const sectionOffset = this.formData.sectionOffsets[fittedPage];
                    if (sectionOffset !== -1) entry.sectionIndex += sectionOffset;
                    else entry.sectionIndex = -1;
                }

                if (entry.message >= OMRMessage.OMRError) {
                    let entryJSON = JSON.stringify(entry);
                    if (!(entryJSON in this.errors)) this.errors[entryJSON] = new Set();
                    this.errors[entryJSON].add(scanPage);
                } else if (entry.message >= OMRMessage.OMRWarning) {
                    let entryJSON = JSON.stringify(entry);
                    if (!(entryJSON in this.warnings)) this.warnings[entryJSON] = new Set();
                    this.warnings[entryJSON].add(scanPage);
                }
            });
        }

        return success;
    }

    allPagesFitted() {
        return this.pageOrder.every(page => page !== -1);
    }

    isOddOnly() {
        var oddOnly = true;
        for (let i = 1; i < this.pageOrder.length; i += 2) {
            if (this.pageOrder[i] !== -1) oddOnly = false;
        }
        return oddOnly;
    }

    static async loadParameters() {
        try {

            const response = await fetch('/js/FormEngine/parameters.json');
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
            const parameterRows = await response.json();

            const offsetsKeys = parameterRows[0];
            const parameters = parameterRows.slice(1).map(row => {
                const bubbleParameters = {
                    blankThreshold: parseFloat(row[0]),
                    medianFilled: parseFloat(row[1]),
                    filledWeight: parseFloat(row[2]),
                    nextWeight: parseFloat(row[3]),
                    minNextDifference: parseFloat(row[4]),
                    gamma: parseFloat(row[5]),
                    offsets: {}
                };
                for (let i = 6; i < row.length; i++) {
                    const key = offsetsKeys[i];
                    if (key.length === 1) {
                        bubbleParameters.offsets[key] = parseFloat(row[i]);
                    } else {
                        throw new Error('Unexpected key size in Assessment');
                    }
                }
                return bubbleParameters;
            });

            ResponseFit.parameters = parameters;

        } catch (error) {
            console.error('There was a problem with the fetch operation:', error);
        }
    }
    
    async getResponses() {
        if (!this.allPagesFitted()) return false;

        if (ResponseFit.parameters == false) await ResponseFit.loadParameters();
    
        let allPixelValues = [];
        for (let i = 0; i < this.pagePixelValues.length; ++i) {
            allPixelValues = allPixelValues.concat(this.pagePixelValues[i]);
        }
    
        const numQuestions = this.formData.questions.length;
    
        let messages = new Set();
        let allResponses = [];
        for (let i = 0; i < ResponseFit.parameters.length; ++i) {
            const omrBrightnesses = OMREngine.getBrightnesses(ResponseFit.parameters[i], this.formData.format, allPixelValues);

            const responseResults = OMREngine.getResponses(ResponseFit.parameters[i], this.formData.format, omrBrightnesses);
            let responses = responseResults.responses;
            let studentData = responseResults.studentData;
            if (i == 0) {
                for (var j = 0; j < responseResults.messages.length; ++j) {
                    messages.add(responseResults.messages[j]);
                }
            }
    
            let responseVector = [];
            for (let j = 0; j < numQuestions; ++j) {
                const dfQuestion = this.formData.questions[j];
                const section = dfQuestion.getSection();
    
                if (responses[j] !== false) {
                    if (section.getType() === 'MCSectionType') {
                        responses[j] = responses[j].split('').sort().join('');
                    }
                    responseVector.push(responses[j]);
                } else if (section.getType() === 'MCSectionType') {
                    responseVector.push("!");
                } else if (section.getType() === 'NRSectionType') {
                    const digitCount = dfQuestion.getIntPropertyForName('digitCount', 4);
                    responseVector.push("!".repeat(digitCount));
                } else if (section.getType() === 'WRSectionType') {
                    const criterionCount = dfQuestion.getCriteria().length;
                    responseVector.push("!".repeat(2 * criterionCount));
                } else {
                    console.error("ERROR: Unexpected section type in Assessment.");
                    throw new Error("Unexpected section type in Assessment");
                }
            }
    
            if ('name' in studentData) responseVector.push(studentData.name);
            else responseVector.push("");
    
            if ('id' in studentData) responseVector.push(studentData.id);
            else responseVector.push("");
    
            allResponses.push(responseVector);
        }
    
        let votedResponses = [];
        let voteCounts = [];
        for (let j = 0; j < numQuestions + 2; ++j) {
            let bestCount = 0;
            let thisVoted = "";
            for (let p = 0; p < ResponseFit.parameters.length; ++p) {
                let thisCount = 1;
                for (let q = p + 1; q < ResponseFit.parameters.length; ++q) {
                    if (allResponses[p][j] === allResponses[q][j]) {
                        if (q === p + 1) p++;
                        thisCount++;
                    }
                }
                if (thisCount > bestCount) {
                    bestCount = thisCount;
                    thisVoted = allResponses[p][j];
                }
            }
            votedResponses.push(thisVoted);
            voteCounts.push(bestCount);
        }
    
        const numPages = this.formData.format.getNumPages();
        let startIndex = 0;
        let responseIndex = 0;
        let sectionIndex = 0;
        for (let pageNum = 0; pageNum < numPages; ++pageNum) {
            const pageFormat = this.formData.format.pageAtIndex(pageNum);
    
            const title = pageFormat.getTitle();
            let idDigits = -1;
            let nameLetters = -1;
            if (title) {
                if (title.hasIDField()) idDigits = title.idDigits();
                if (title.hasNameField()) nameLetters = title.nameLetters();
            }
            if (idDigits > 0) {
                if (voteCounts[numQuestions + 1] < ResponseFit.parameters.length) {
                    if (!messages.has(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex))) {
                        messages.add(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                    }
                }
                if (votedResponses[numQuestions + 1].trim() === "") {
                    messages.add(new MessageEntry(OMRMessage.OMRWarningEmptySection, sectionIndex, startIndex));
                }
                startIndex += idDigits * 10;
                sectionIndex++;
            }
            if (nameLetters > 0) {
                if (voteCounts[numQuestions] < ResponseFit.parameters.length) {
                    if (!messages.has(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex))) {
                        messages.add(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                    }
                }
                if (votedResponses[numQuestions].trim() === "") {
                    messages.add(new MessageEntry(OMRMessage.OMRWarningEmptySection, sectionIndex, startIndex));
                }
                startIndex += nameLetters * 27;
                sectionIndex++;
            }
            for (let i = 0; i < pageFormat.getNumSections(); ++i) {
                const section = pageFormat.sectionAtIndex(i);
                const questionCount = section.getNumQuestions();
                if (questionCount === 0) continue;
    
                const sectionStartIndex = startIndex;
    
                let hasValue = false;
                let hasMissingWR = false;
                if (section.getType() === 'MCSectionType') {
                    for (let j = 0; j < questionCount; ++j) {
                        if (voteCounts[responseIndex] < ResponseFit.parameters.length) {
                            if (!messages.has(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex))) {
                                messages.add(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                            }
                        }
    
                        const dfQuestion = section.questionAtIndex(j);
                        const choiceCount = dfQuestion.getIntPropertyForName('choiceCount', 4);
                        if (votedResponses[responseIndex].trim() !== "") hasValue = true;
                        startIndex += choiceCount;
                        responseIndex++;
                    }
                } else if (section.getType() === 'NRSectionType') {
                    for (let j = 0; j < questionCount; ++j) {
                        if (voteCounts[responseIndex] < ResponseFit.parameters.length) {
                            if (!messages.has(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex))) {
                                messages.add(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                            }
                        }
    
                        const dfQuestion = section.questionAtIndex(j);
                        const digitCount = dfQuestion.getIntPropertyForName('digitCount', 4);
                        if (votedResponses[responseIndex].trim() !== "") {
                            hasValue = true;
                            const trimmedResponse = votedResponses[responseIndex].trim();
                            if (trimmedResponse.includes(' ')) {
                                messages.add(new MessageEntry(OMRMessage.OMRWarningBadResponse, sectionIndex, startIndex));
                            }
                        }
                        startIndex += 11 * digitCount - (digitCount < 2 ? digitCount : 2);
                        responseIndex++;
                    }
                } else if (section.getType() === 'WRSectionType') {
                    for (let j = 0; j < questionCount; ++j) {
                        if (voteCounts[responseIndex] < ResponseFit.parameters.length) {
                            if (!messages.has(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex))) {
                                messages.add(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                            }
                        }
    
                        const dfQuestion = section.questionAtIndex(j);
                        const numCriteria = dfQuestion.getCriteria().length;
                        for (let p = 0; p < numCriteria; ++p) {
                            const maxScore = dfQuestion.getCriteria()[p].maxScore;
                            const thisPart = votedResponses[responseIndex].substr(2 * p, 2);
                            if (thisPart.trim() !== "") {
                                hasValue = true;
                                if (thisPart[0] === '0' + maxScore && thisPart[1] === '+') {
                                    messages.add(new MessageEntry(OMRMessage.OMRWarningBadWRScore, sectionIndex, startIndex));
                                }
                            } else {
                                hasMissingWR = true;
                            }
                            startIndex += maxScore + 2;
                        }
                        responseIndex++;
                    }
                }
    
                if (!hasValue) {
                    messages.add(new MessageEntry(OMRMessage.OMRWarningEmptySection, sectionIndex, sectionStartIndex));
                } else if (hasMissingWR) {
                    messages.add(new MessageEntry(OMRMessage.OMRWarningMissingWRScore, sectionIndex, sectionStartIndex));
                }
    
                sectionIndex++;
            }
        }
    
        var responseData = {
            details: {},
            pages: this.pageOrder,
            responses: votedResponses.slice(0, numQuestions)
        };
    
        if (votedResponses[numQuestions].length > 0) {
            responseData.details.name = votedResponses[numQuestions];
        }
        if (votedResponses[numQuestions + 1].length > 0) {
            responseData.details.id = votedResponses[numQuestions + 1];
        }
    
        for (let entry of messages) {
            let formPageNum;
            for (formPageNum = 0; formPageNum < this.formData.pageOffsets.length; ++formPageNum) {
                if (entry.bubbleIndex < this.formData.pageOffsets[formPageNum]) break;
            }
            const scanPage = this.pageOrder[formPageNum];
    
            entry.bubbleIndex = -1;
    
            if (entry.message >= OMRMessage.OMRError) {
                let entryJSON = JSON.stringify(entry);
                if (!(entryJSON in this.errors)) this.errors[entryJSON] = new Set();
                this.errors[entryJSON].add(scanPage);
            } else if (entry.message >= OMRMessage.OMRWarning) {
                let entryJSON = JSON.stringify(entry);
                if (!(entryJSON in this.warnings)) this.warnings[entryJSON] = new Set();
                this.warnings[entryJSON].add(scanPage);
            }
        }
    
        return responseData;
    }    

    reset() {
        this.pagePixelValues = [];

        this.formPages = this.formData.blockModels.length;
        this.pageOrder = new Array(this.formPages).fill(-1);

        this.badPages = 0;

        this.badBubblesCount = 0;

        this.warnings = {};
        this.errors = {};
        this.abortMessage = false;
    }

    getWarnings() {
        return this.warnings;
    }

    getErrors() {
        return this.errors;
    }

    getAbortMessage() {
        return this.abortMessage;
    }

    getFormPages() {
        return this.formPages;
    }
    
    getFormData() {
        return this.formData;
    }

    getHashWithText() {
        return this.formData.format.getStringPropertyForName('hashWithText', '');
    }

    getHashNoText() {
        return this.formData.format.getStringPropertyForName('hashNoText', '');
    }

}
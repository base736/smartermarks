/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var renderWorker = false;
var formWorker = false;

// Polyfill for Promise.withResolvers

if (typeof Promise.withResolvers === 'undefined') {
    window.Promise.withResolvers = function () {
        let resolve, reject;
        const promise = new Promise((res, rej) => {
            resolve = res;
            reject = rej;
        });
        return { promise, resolve, reject };
    }
}

export async function readForm(pdfArrayBuffer, xmlFormats, progressCallback) {

    const readPromise = Promise.withResolvers();
    var readData = false;

    // Set up render worker

    renderWorker = new Worker('/js/FormEngine/RenderWorker.js?7.0', { type: 'module' });

    renderWorker.onerror = function(error) {
        console.error("RenderWorker error:", error);
        readPromise.reject(error);
    };

    renderWorker.onmessage = function(event) {
        const data = event.data;
        if (data.type === 'pageCount') {

            scanPageCount = data.value;

        } else if (data.type === 'setArrayBufferProgress') {

            data.stage = 'reading';
            progressCallback(data);

        } else if (data.type === 'renderResponse') {

            if (data.status == 'rendered') {
                formWorker.postMessage(data, [data.imageArrayBuffer]);
            } else {
                formWorker.postMessage(data);
            }

        } else if (data.type === 'shutdownComplete') {

            renderWorker = false;
            if (formWorker === false) {
                console.log('Done processing');
                readPromise.resolve(readData);
            }

        }
    };

    renderWorker.postMessage({
        type: 'setArrayBuffer',
        pdfArrayBuffer: pdfArrayBuffer
    }, [pdfArrayBuffer]);

    var scanPageCount = false;
    renderWorker.postMessage({
        type: 'getPageCount'
    });

    // Set up form worker

    formWorker = new Worker('/js/FormEngine/FormWorker.js?7.0', { type: 'module' });

    formWorker.onerror = function(error) {
        console.error("FormWorker error:", error);
        readPromise.reject(error);
    };

    formWorker.onmessage = function(event) {
        const data = event.data;
        if (data.type === 'progress') {

            delete data.type;
            data.stage = 'processing';
            data.scanPageCount = scanPageCount;
            progressCallback(data);

        } else if (data.type === 'renderRequest') {

            renderWorker.postMessage(data);

        } else if (data.type === 'formatRequest') {

            const documentID = data.documentID;
            
            fetch(`/Scores/get_xml?document_id=${documentID}`)
            .then(async response => {
                pendingAJAXPost = false;
                const responseData = await response.json();
            
                formWorker.postMessage({
                    type: 'formatResponse',
                    documentID: documentID,
                    responseData: responseData
                });
            });

        } else if (data.type === 'result') {

            readData = data;

            renderWorker.postMessage({
                type: 'shutdown'
            });

            formWorker.postMessage({
                type: 'shutdown'
            });

        } else if (data.type === 'shutdownComplete') {

            formWorker = false;
            if (renderWorker === false) {
                console.log('Done processing');
                readPromise.resolve(readData);
            }

        }
    };
    
    // Start job

    console.log('Started processing');

    formWorker.postMessage({
        type: 'getResponses',
        formats: xmlFormats
    });

    return readPromise.promise;
}

export function cancelReadForm() {
    formWorker.postMessage({
        type: 'cancel'
    });
}
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

const dictionaryURL = '/Localization/dictionary.json';

class Localization {
    constructor() {
        this.language = 'English';
        this.dictionary = {};
        this.dictionaryLoadingPromise = this.loadDictionary(dictionaryURL);
    }

    async loadDictionary(url) {
        return fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('ERROR: Bad network response in Localization');
            }
            return response.json();
        })
        .then(data => {
            this.dictionary = data;
        })
        .catch(error => {
            console.error('ERROR: Unable to load localization file.', error);
        });
    }

    async getString(key) {
        await this.dictionaryLoadingPromise;

        if (!this.dictionary[this.language] || !this.dictionary[this.language][key]) {
            return '[ERROR: Bad localization]';
        }
        return this.dictionary[this.language][key];
    }

    setLanguage(newLanguage) {
        this.language = newLanguage;
    }

    setDictionaryURL(url) {
        this.dictionaryLoadingPromise = this.loadDictionary(url);
    }
}

// Export a singleton instance
export const localizationInstance = new Localization();

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { DFElement, getSimpleProperty } from './DFElement.js?7.0';
import { DFQuestion } from './DFQuestion.js?7.0';

function parseMCSection(section, document, rootNode) {
    let questionCount = -1;
	section.setPropertyForName('Letter', 'defaultType');
	let hasQuestionTypes = false;

    rootNode.forEach(thisElement => {
        const property = getSimpleProperty(thisElement);
        if (property !== false) {
            if (property.key === 'questionCount') {

                if (questionCount !== -1) {
                    console.error('ERROR: Question count defined twice in MC section.');
                    return false;
                }
    
                questionCount = parseInt(property.value, 10);
                if (!questionCount) {
                    console.error('ERROR: Poorly formed or illegal questionCount tag in MC section.');
                    return false;
                }
    
                for (let i = 0; i < questionCount; i++) section.newQuestion();
    
            } else if (property.key == 'answers') {

                if (questionCount === -1) {
                    console.error('ERROR: Answers defined before question count in MC section.');
                    return false;
                }
    
                let answerString = property.value.toLowerCase().replace(/\s/g, '');
                let questionNum = 0;
                for (let i = 0; i < answerString.length; i++) {
                    if (questionNum >= section.getNumQuestions()) {
                        console.error(`ERROR: ${questionNum} answers given in MC section; expected ${section.getNumQuestions()}.`);
                        return false;
                    }
    
                    const question = section.questionAtIndex(questionNum);
                    question.setPropertyForName('NewNumber', 'numberingType');
    
                    if (answerString[i] === '(') {
                        let end = answerString.indexOf(')', i);
                        if (end === -1) {
                            console.error('ERROR: Unmatched parentheses in MC answers tag.');
                            return false;
                        }
    
                        let chunk = answerString.slice(i + 1, end);
                        if (chunk[0] === '#') {
                            question.setIsOmitted(true);
                        } else {
                            if (chunk[0] === '@') {
                                question.setIsBonus(true);
                                chunk = chunk.slice(1);
                            }
    
                            let answers = chunk.split(';').map(part => part.trim());
                            answers.forEach(answer => {
                                let [thisAnswer, valueString = '1.0'] = answer.split('=');
                                thisAnswer = thisAnswer.split('').sort().join('');
                                question.addAnswer(thisAnswer);
                            });
                        }
    
                        i = end;
                        questionNum++;
                    } else if (/[a-z]/.test(answerString[i])) {
                        question.addAnswer(answerString[i]);
                        questionNum++;
                    } else if (answerString[i] === '_' || answerString[i] === '#') {
                        question.setIsOmitted(true);
                        questionNum++;
                    } else {
                        console.error('ERROR: Illegal character in MC answers tag.');
                        return false;
                    }
                }
    
                if (questionNum < section.getNumQuestions()) {
                    console.error(`ERROR: ${questionNum} answers given in MC section; expected ${section.getNumQuestions()}.`);
                    return false;
                }

            } else if (property.key == 'numberingTypes') {

                if (questionCount === -1) {
                    console.error('ERROR: Numbering types defined before question count in MC section.');
                    return false;
                }
    
                let typeString = property.value.toLowerCase();
    
                if (/\d/.test(typeString[0])) {
                    document.setHasManualNumbering(true);
                    let numberList = typeString.split(',').map(part => part.trim());
                    if (numberList.length !== section.getNumQuestions()) {
                        console.error(`ERROR: ${numberList.length} types given in MC section; expected ${section.getNumQuestions()}.`);
                        return false;
                    }
    
                    numberList.forEach((number, index) => {
                        const question = section.questionAtIndex(index);
                        question.setPropertyForName(number, 'numberingType');
                    });
                } else {
                    if (typeString.length !== section.getNumQuestions()) {
                        console.error(`ERROR: ${typeString.length} types given in MC section; expected ${section.getNumQuestions()}.`);
                        return false;
                    }
    
                    typeString.split('').forEach((typeChar, index) => {
                        const question = section.questionAtIndex(index);
                        if (typeChar === 'n') question.setPropertyForName('NewNumber', 'numberingType');
                        else if (typeChar === 'a') question.setPropertyForName('NewAlpha', 'numberingType');
                        else if (typeChar === 'c') question.setPropertyForName('ContinueAlpha', 'numberingType');
                        else {
                            console.error(`ERROR: Unexpected letter '${typeChar}' in numbering types.`);
                            return false;
                        }
                    });
                }

            } else if (property.key == 'questionTypes') {

                hasQuestionTypes = true;

                let typeString = property.value;
                if (typeString.length !== section.getNumQuestions()) {
                    console.error(`ERROR: ${typeString.length} types given in MC section; expected ${section.getNumQuestions()}.`);
                    return false;
                }
    
                typeString.split('').forEach((typeChar, index) => {
                    const question = section.questionAtIndex(index);
                    if (typeChar === 'T') {
                        question.setPropertyForName('TrueFalse', 'questionType');
                        question.setPropertyForName('2', 'choiceCount');
                    } else if (/[a-z]/.test(typeChar)) {
                        question.setPropertyForName('Letter', 'questionType');
                        question.setPropertyForName(`${typeChar.charCodeAt(0) - 'a'.charCodeAt(0) + 1}`, 'choiceCount');
                    } else {
                        console.error(`ERROR: Unexpected letter '${typeChar}' in question types.`);
                        return false;
                    }
                });

            } else {
              
                section.setPropertyForName(property.value, property.key);

            }
            
        } else if ('category' in thisElement) {

            // No longer necessary since we're not scoring here

        } else {

            console.error('ERROR: Unexpected property in MC question.');
            return false;

        }
	});

	if (questionCount === -1) {
		console.error('ERROR: No question count in MC section');
		return false;
	}

	if (!hasQuestionTypes) {
		let defaultType = section.getStringPropertyForName('defaultType');
		let choiceCount = section.getIntPropertyForName('choiceCount');
		for (let i = 0; i < section.getNumQuestions(); i++) {
			const question = section.questionAtIndex(i);
			question.setPropertyForName(defaultType, 'questionType');
			question.setPropertyForName(choiceCount, 'choiceCount');
		}
	}

	return true;
}

function parseNRSection(section, document, rootNode) {
    rootNode.forEach(thisElement => {
        if ('question' in thisElement) {

            const question = section.newQuestion();
            question.setPropertyForName('NewNumber', 'numberingType');

            const questionNode = thisElement.question;
            for (const childElement of questionNode) {
                const property = getSimpleProperty(childElement);
                if (property !== false) {
                    if (property.key === 'answer') {

                        let answerString = property.value;
                        if (answerString[0] === '#') {
                            question.setIsOmitted(true);
                        } else {
                            if (answerString[0] === '@') {
                                question.setIsBonus(true);
                                answerString = answerString.slice(1);
                            }

                            let answers = answerString.split(';').map(part => part.trim());
                            if (answers.length === 0) question.setIsOmitted(true);

                            answers.forEach(answer => {
                                let [thisAnswer, valueString = '1.0'] = answer.split('=');
                                question.addAnswer(thisAnswer);
                            });
                        }

                    } else if (property.key === 'numberingType') {

                        const numberingType = property.value;
                        if (/\d/.test(numberingType[0])) document.setHasManualNumbering(true);
                        question.setPropertyForName(numberingType, 'numberingType');

                    } else {
              
                        const text = property.value;
                        question.setPropertyForName(text, property.key);
        
                    }
                }
            }

            if (!question.hasProperty('digitCount')) {
                let digitCount = section.getIntPropertyForName('digitCount', 4);
                question.setPropertyForName(digitCount, 'digitCount');
            }

        } else if ('category' in thisElement) {

            // No longer necessary since we're not scoring here

        } else {

            const property = getSimpleProperty(thisElement);
            if (property !== false) {
				section.setPropertyForName(property.value, property.key);
            }    

        }
    });

	if (!section.hasProperty('digitCount')) {
		section.setPropertyForName('4', 'digitCount');
	}

	return true;
}

function parseWRSection(section, document, rootNode) {
    rootNode.forEach(thisElement => {
        if ('question' in thisElement) {

            const question = section.newQuestion();
            question.setPropertyForName('NewNumber', 'numberingType');
            question.setIsOmitted(false);

            const questionNode = thisElement.question;
            for (const childElement of questionNode) {
                const property = getSimpleProperty(childElement);
                if (property !== false) {
                    if (property.key === 'criteria') {

                        let criteriaString = property.value;
                        if (criteriaString[0] === '#') {
                            question.setIsOmitted(true);
                            criteriaString = criteriaString.slice(1);
                        } else if (criteriaString[0] === '@') {
                            question.setIsBonus(true);
                            criteriaString = criteriaString.slice(1);
                        }

                        // Split at semicolons unless they're part of an HTML entity, then process
                        
                        let criteria = criteriaString.split(/(?<!&[^;]*);/);
                        criteria.forEach(criterion => {
                            let name;
                            let maxScore = 5;
                            
                            const lastEqualIndex = criterion.lastIndexOf('=');
                            
                            if (lastEqualIndex === -1) {

                                // Case 1: No '=' found

                                name = criterion;

                            } else {

                                const part1 = criterion.substring(0, lastEqualIndex);
                                const part2 = criterion.substring(lastEqualIndex + 1);
                            
                                if (/^\d+$/.test(part2)) {

                                    // Case 2: Second part is a valid score

                                    name = part1;
                                    maxScore = parseInt(part2, 10);

                                } else {

                                    // Case 3: Second part is not a valid score

                                    name = criterion;
                                }
                            }
                            
                            question.addCriterion(name, maxScore);
                        });

                    } else if (property.key == 'numberingType') {
                         
                        const numberingType = property.value;
                        if (/\d/.test(numberingType[0])) document.setHasManualNumbering(true);
                        question.setPropertyForName(numberingType, 'numberingType');

                    } else if (property.key == 'text') {

                        const text = property.value;
                        question.setPropertyForName('Text', 'promptType');
                        question.setPropertyForName(text, 'promptText');

                    } else {
              
                        question.setPropertyForName(property.value, property.key);
        
                    }
                }
            }

        } else if ('category' in thisElement) {

            // No longer necessary since we're not scoring here

        } else {

            const property = getSimpleProperty(thisElement);
            if (property !== false) {
				section.setPropertyForName(property.value, property.key);
            }    

        }
    });

	return true;
}

export class DFSection extends DFElement {
	constructor(rootNode, rootKey, document, initNum) {
		super();
        
		this.sectionNum = initNum;
		this.questions = [];

        if (rootKey === 'mcSection') {
            this.type = 'MCSectionType';
            this.valid = parseMCSection(this, document, rootNode);
        } else if (rootKey === 'nrSection') {
            this.type = 'NRSectionType';
            this.valid = parseNRSection(this, document, rootNode);
        } else if (rootKey === 'wrSection') {
            this.type = 'WRSectionType';
            this.valid = parseWRSection(this, document, rootNode);
        } else {
            console.warn(`WARNING: Unrecognized tag '${rootKey}' at section root.`);
        }

		if (this.valid) {
			let currentNumber = this.getIntPropertyForName('startNumber', 1) - 1;

			let currentLetter = 'a';
			for (let i = 0; i < this.getNumQuestions(); i++) {
				const question = this.questionAtIndex(i);
				const numberingType = question.getStringPropertyForName('numberingType');
				if (/\d/.test(numberingType[0])) {
					question.setQuestionNumber(numberingType);
				} else if (numberingType === 'NewNumber') {
					currentNumber++;
					currentLetter = 'a';
					question.setQuestionNumber(`${currentNumber}`);
				} else {
					if (numberingType === 'NewAlpha') {
						currentNumber++;
						currentLetter = 'a';
					} else {
						currentLetter = String.fromCharCode(currentLetter.charCodeAt(0) + 1);
					}
					question.setQuestionNumber(`${currentNumber}${currentLetter}`);
				}
			}
		}
	}

	getSectionNum() {
		return this.sectionNum;
	}

	getType() {
		return this.type;
	}

	getNumQuestions() {
		return this.questions.length;
	}

	questionAtIndex(index) {
		return index < this.questions.length ? this.questions[index] : null;
	}

	newQuestion() {
		const question = new DFQuestion(this);
		this.questions.push(question);
		return question;
	}
}

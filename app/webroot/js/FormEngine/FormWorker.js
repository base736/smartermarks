/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { CVImage } from './CVImage.js?7.0';
import { ResponseFit } from './ResponseFit.js?7.0';
import { MessageEntry, OMRMessage } from './OMREngine.js?7.0';

const formatRequests = {};
const renderRequests = {};

// Polyfill for Promise.withResolvers in Web Workers

if (typeof Promise.withResolvers === 'undefined') {
    self.Promise.withResolvers = function () {
        let resolve, reject;
        const promise = new Promise((res, rej) => {
            resolve = res;
            reject = rej;
        });
        return { promise, resolve, reject };
    };
}

function requestPage(pageNum) {
    postMessage({
        type: 'renderRequest',
        pageNum: pageNum
    });

    renderRequests[pageNum] = Promise.withResolvers();
}

var cancelProcessing = false;

const  qrPattern = /^3\d{14}$/;

// Process responses

async function getResponses(xmlFormats) {
    cancelProcessing = false;

    let responseFits = {};
    let formData = {};
    let results = {};

    let hasOldVersion = false;
    for (const [documentID, xmlFormat] of Object.entries(xmlFormats)) {
        const responseFit = new ResponseFit();
        await responseFit.setFormatXML(xmlFormat);
        responseFits[documentID] = responseFit;

        formData[documentID] = responseFit.getFormData();
        if (formData[documentID].version !== 3) hasOldVersion = true;

        results[documentID] = {
            type: "expected",
            responses: [],
            warnings: {},
            errors: {}
        };
    }

    if (hasOldVersion && (Object.keys(responseFits).length > 1)) {
        return {
            status: 'failed',
            message: 'Multiple forms given with incompatible version'
        };
    }

    var documentID = false;

    formData[documentID] = false;

    results[documentID] = {
        type: "unknown",
        responses: [],
        warnings: {},
        errors: {}
    };

    var doneResponse = true;

    let qrCodeString = false;
    let abortMessage = false;
    let badPages = [];

    let runningFits = [];
    let fittedPages = 0;

    let nextRenderedPageObj = false;
    requestPage(1);

    let scanPage = 0;
    while (true) {
        if (cancelProcessing) break;
        ++scanPage;

        let renderedPageObj;
        if (nextRenderedPageObj !== false) {
            renderedPageObj = nextRenderedPageObj;
            nextRenderedPageObj = false;
        } else if (scanPage in renderRequests) {
            renderedPageObj = await renderRequests[scanPage].promise;
            delete renderRequests[scanPage];
        } else {
            throw new Error(`Unrequested page in getResponses (page ${scanPage})`)
        }

        if (scanPage == 1) {
            postMessage({
                type: 'progress',
                status: "running",
                pagesDone: 0,
                warnings: 0,
                errors: 0
            });    
        }

        if (renderedPageObj.status === 'failed') {

            // Last page has been reached

            break;
        }

        requestPage(scanPage + 1);

        /**************************************************************************************************/
        /* If we have many forms defined and one hasn't been chosen for this response, find a QR code,    */
        /* read it and initialize a new response                                                          */
        /**************************************************************************************************/

        const thisQRCode = renderedPageObj.qr;

        if (fittedPages === 0) {

            qrCodeString = thisQRCode;
            if (qrCodeString === false) {

                // No QR code on first page -- check second page in case the page was backward

                nextRenderedPageObj = await renderRequests[scanPage + 1].promise;
                delete renderRequests[scanPage + 1];

                const nextQRCode = nextRenderedPageObj?.qr;
                if (nextQRCode && qrPattern.test(nextQRCode)) {
                    let testDocumentID = parseInt(nextQRCode.substring(1, 10));
                    if (testDocumentID in responseFits) {
                        if (responseFits[testDocumentID].getFormPages() > 1) {
                            qrCodeString = nextQRCode;
                        }    
                    }
                }
            }

            if (hasOldVersion) {
                documentID = parseInt(Object.keys(responseFits)[0]);
            } else if (qrCodeString && qrPattern.test(qrCodeString)) {
                documentID = parseInt(qrCodeString.substring(1, 10));
            }

        }

        if ((documentID !== false) && !(documentID in responseFits)) {

            if (!(documentID in formatRequests)) {
                postMessage({
                    type: 'formatRequest',
                    documentID: documentID
                });

                formatRequests[documentID] = Promise.withResolvers();
            }

            const responseData = await formatRequests[documentID].promise;
            if (responseData.success) {

                let xmlFormat = responseData.formatXML;

                const responseFit = new ResponseFit();
                await responseFit.setFormatXML(xmlFormat);
                responseFits[documentID] = responseFit;

                formData[documentID] = responseFit.getFormData();

                results[documentID] = {
                    type: "loaded",
                    name: responseData.name,
                    responses: [],
                    warnings: {},
                    errors: {}
                };
                
            } else {

                abortMessage = `Your PDF contains forms for "${responseData.name}" which were printed from another teacher's folders. ` + 
                    `You can score these by selecting your copy in the Forms and Versions index, or have it scored by the teacher ` + 
                    `who printed it.`;
                break;

            }

        }

        /**************************************************************************************************/
        /* Start a new response fit if needed                                                             */
        /**************************************************************************************************/

        if (runningFits.length === 0) {
            if (documentID === false) {

                // We don't know what form this is, so just look for anything we expect

                for (let key in results) {
                    if (results[key].type === "expected") {
                        responseFits[key].reset();
                        runningFits.push(responseFits[key]);
                    }
                }

            } else if (documentID in responseFits) {

                // We found a QR code, so initialize the running fits with just that form

                responseFits[documentID].reset();
                runningFits.push(responseFits[documentID]);

            } else {

                // We found a QR code, but we weren't able to retrieve the format for it

                abortMessage = "Unexpected document ID";
                break;

            }
        }

        /**************************************************************************************************/
        /* Read page                                                                                      */
        /**************************************************************************************************/

        const renderedPage = renderedPageObj.image;

        let pageRead = false;
        if (renderedPage) {
            for (let i = 0; i < runningFits.length; ++i) {
                if (await runningFits[i].readPage(renderedPage, scanPage)) {
                    pageRead = true;
                } else {
                    abortMessage = runningFits[i].getAbortMessage();
                    if (abortMessage) runningFits[i] = false;
                }
            }

            runningFits = runningFits.filter(fit => fit !== false);
        }

        /**************************************************************************************************/
        /* Get responses if all pages are fitted                                                          */
        /**************************************************************************************************/

        if (!pageRead) {

            if (runningFits.length > 0) {
                badPages.push(scanPage);
                if (fittedPages === 0) runningFits = [];
                abortMessage = false;
            } else break;

        } else {

            doneResponse = false;
            fittedPages++;

            // Save student data if we have a complete assessment

            let bestFinished = false;
            let bestErrorsCount = 0;
            let bestBadBubblesCount = 0;

            for (let i = 0; i < runningFits.length; ++i) {
                if (runningFits[i].allPagesFitted()) {
                    let thisErrorsCount = 0;
                    let errors = runningFits[i].getErrors();
                    Object.values(errors).forEach(typeErrors => thisErrorsCount += typeErrors.length);

                    const thisBadBubblesCount = runningFits[i].badBubblesCount;

                    var isBest = false;
                    if (!bestFinished) isBest = true;
                    else if (thisErrorsCount < bestErrorsCount) isBest = true;
                    else if (thisErrorsCount == bestErrorsCount) {
                        if (thisBadBubblesCount < bestBadBubblesCount) isBest = true;
                    }

                    if (isBest) {
                        bestFinished = runningFits[i];
                        bestErrorsCount = thisErrorsCount;
                        bestBadBubblesCount = thisBadBubblesCount;
                    }
                }
            }

            if (bestFinished) {

                // Get voted responses

                let thisData = await bestFinished.getResponses();
                if (qrCodeString) thisData.details.qr = qrCodeString;
                results[documentID].responses.push(thisData);

                // Copy warnings and errors into ResponseData entry

                let warnings = bestFinished.getWarnings();
                Object.entries(warnings).forEach(([key, val]) => {
                    if (!(key in results[documentID].warnings)) {
                        results[documentID].warnings[key] = new Set();
                    }
                    val.forEach(pageNum => results[documentID].warnings[key].add(pageNum));
                });

                let errors = bestFinished.getErrors();
                Object.entries(errors).forEach(([key, val]) => {
                    if (!(key in results[documentID].errors)) {
                        results[documentID].errors[key] = new Set();
                    }
                    val.forEach(pageNum => results[documentID].errors[key].add(pageNum));
                });

                // Get ready for next responses

                doneResponse = true;

                documentID = false;
                runningFits = [];
                fittedPages = 0;
                qrCodeString = false;

            }
        }

        let warningsPages = new Set();
        let errorsPages = new Set();

        for (const { warnings, errors } of Object.values(results)) {
            for (const warningSet of Object.values(warnings)) {
                warningSet.forEach(warning => warningsPages.add(warning));
            }
            for (const errorSet of Object.values(errors)) {
                errorSet.forEach(error => errorsPages.add(error));
            }
        }

        badPages.forEach(error => errorsPages.add(error));

        postMessage({
            type: 'progress',
            status: "running",
            pagesDone: scanPage,
            warnings: warningsPages.size,
            errors: errorsPages.size
        });
    }

    if (!doneResponse && !cancelProcessing && !abortMessage) {

        // Last response did not finish before the end of the PDF

        let message = new MessageEntry(OMRMessage.OMRErrorMissingPage);
        let messageJSON = JSON.stringify(message);
        
        if (!(messageJSON in results[documentID].errors)) {
            results[documentID].errors[messageJSON] = new Set();
        }
        results[documentID].errors[messageJSON].add(scanPage);           
        
    }

    // Add errors for bad pages

    badPages.forEach((page) => {
        let message = new MessageEntry(OMRMessage.OMRErrorBadPage);
        let messageJSON = JSON.stringify(message);

        for (const documentID in results) {
            if (documentID === 'false') continue;

            if (!(messageJSON in results[documentID].errors)) {
                results[documentID].errors[messageJSON] = new Set();
            }
            results[documentID].errors[messageJSON].add(page);
        }
    });

    // Map loaded results back to expected forms as required
    
    let documentID_map = {};
    for (const [documentID, result] of Object.entries(results)) {
        if (documentID === 'false') continue;
        if (result.type === "expected") continue;

        let hashWithText = responseFits[documentID].getHashWithText();
        let matchesWithText = [];
    
        let hashNoText = responseFits[documentID].getHashNoText();
        let matchesNoText = [];
    
        for (const [key, fit] of Object.entries(responseFits)) {
            if (results[key].type !== "expected") continue;
            if (results[key].responses.length > 0) continue;
    
            if (hashWithText.length > 0 && fit.getHashWithText() === hashWithText) {
                matchesWithText.push(key);
            }
            if (hashNoText.length > 0 && fit.getHashNoText() === hashNoText) {
                matchesNoText.push(key);
            }
        }
    
        if (matchesWithText.length === 1) {
            documentID_map[documentID] = matchesWithText[0];
        } else if (matchesNoText.length === 1) {
            documentID_map[documentID] = matchesNoText[0];
        }
    }
    
    // Remove keys where more than one key maps to the same value

    let reverse_map = {};
    for (const [key, value] of Object.entries(documentID_map)) {
        if (!(value in reverse_map)) reverse_map[value] = [];
        reverse_map[value].push(key);
    }
    
    for (const [value, keys] of Object.entries(reverse_map)) {
        if (keys.length > 1) {
            for (const key of keys) {
                delete documentID_map[key];
            }
        }
    }
    
    // Map data as required

    for (const [documentID, mappedID] of Object.entries(documentID_map)) {
        results[mappedID].responses = results[documentID].responses;
        results[mappedID].warnings = results[documentID].warnings;
        results[mappedID].errors = results[documentID].errors;
        delete results[documentID];
    }
    
    // Build response

    var response;
    if (abortMessage === false) {
        const data = [];

        for (const [documentID, documentResults] of Object.entries(results)) {

            if (documentID === 'false') {

                for (const response of documentResults.responses) {
                    data.push({
                        type: documentResults.type,
                        details: {
                            ...response.details,
                            pages: response.pages
                        },
                        results: response.responses
                    });
                }

            } else {

                const docID = parseInt(documentID);

                // Build responses array

                const responsesArray = documentResults.responses.map(response => ({
                    details: {
                        ...response.details,
                        pages: response.pages
                    },
                    results: response.responses
                }));

                // Build warnings array

                const warningsArray = [];
                Object.entries(documentResults.warnings).forEach(([key, val]) => {
                    const message = JSON.parse(key);
                    const pages = val;

                    let output;
                    switch (message.message) {
                        case OMRMessage.OMRWarningEmptySection: output = "No responses"; break;
                        case OMRMessage.OMRWarningExtraMarks: output = "Too many bubbles filled"; break;
                        case OMRMessage.OMRWarningAmbiguousFill: output = "Ambiguously filled bubble"; break;
                        case OMRMessage.OMRWarningBadWRScore: output = "WR score exceeds maximum"; break;
                        case OMRMessage.OMRWarningMissingWRScore: output = "Missing WR score (assumed zero)"; break;
                        case OMRMessage.OMRWarningBadResponse: output = "Bad response"; break;
                        case OMRMessage.OMRWarningBadPageOrder: output = "Unexpected page order"; break;
                        case OMRMessage.OMRWarningExtraBubbles: output = "Unexpected bubbles"; break;
                        default: return;
                    }

                    if (message.sectionIndex !== -1) {
                        const sectionName = formData[docID].sectionNames[message.sectionIndex] || "unnamed section";
                        output += ` in "${sectionName}" section`;
                    }

                    warningsArray.push({ message: output, pages: Array.from(pages) });
                });

                // Build errors array

                const errorsArray = [];
                Object.entries(documentResults.errors).forEach(([key, val]) => {
                    const message = JSON.parse(key);
                    const pages = val;

                    let output;
                    switch (message.message) {
                        case OMRMessage.OMRErrorBadPage: output = "Unable to read page"; break;
                        case OMRMessage.OMRErrorBadSection: output = "Unable to find expected bubbles"; break;
                        case OMRMessage.OMRErrorExtraBubbles: output = "Unexpected bubbles"; break;
                        case OMRMessage.OMRErrorMissingPage: output = "Missing page in response"; break;
                        default: return;
                    }

                    if (message.sectionIndex !== -1) {
                        const sectionName = formData[docID].sectionNames[message.sectionIndex] || "unnamed section";
                        output += ` in "${sectionName}" section`;
                    }

                    errorsArray.push({ message: output, pages: Array.from(pages) });
                });

                // Add results to the returned data

                data.push({
                    type: documentResults.type,
                    documentID: docID,
                    formatXML: formData[docID].formatXML,
                    responses: responsesArray,
                    warnings: warningsArray,
                    errors: errorsArray
                });
            }
        }

        response = {
            status: cancelProcessing ? 'canceled' : 'complete',
            pageCount: scanPage - 1,
            data: data
        };      

    } else {

        response = {
            status: 'failed',
            pageCount: scanPage,
            message: abortMessage
        };
        
    }

    return response;
}

// Handle messages from the main thread

addEventListener('message', async function(event) {
    const data = event.data;
    if (data.type === 'getResponses') {

        const {formats} = data;
        const responses = await getResponses(formats);
        postMessage({ type: 'result', ...responses });

    } else if (data.type === 'formatResponse') {

        const { documentID, responseData } = data;
        if (documentID in formatRequests) {
            formatRequests[documentID].resolve(responseData);
            delete formatRequests[documentID];
        }

    } else if (data.type === 'renderResponse') {

        const pageNum = data.pageNum;
        const status = data.status;

        if (pageNum in renderRequests) {
            if (status  == 'rendered' ) {

                const image = new CVImage(data.imageWidth, data.imageHeight);
                image.data = new Float32Array(data.imageArrayBuffer);
    
                const qr = data.qr;
    
                renderRequests[pageNum].resolve({ status, image, qr });
    
            } else {

                renderRequests[pageNum].resolve({ status });

            }
        }

    } else if (data.type === 'cancel') {

        cancelProcessing = true;

    } else if (data.type === 'shutdown') {

        postMessage({
            type: 'shutdownComplete'
        });

        self.close();

    }
});

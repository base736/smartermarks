/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { DFElement } from './DFElement.js?7.0';
import { DFTitle } from './DFTitle.js?7.0';
import { DFSection } from './DFSection.js?7.0';

export class DFPage extends DFElement {
	constructor(rootNode, document, sectionStart) {
		super();
        
		this.title = false;
		this.sections = [];
		this.valid = true;

        var sectionNum = sectionStart;
        rootNode.forEach(thisElement => {
            if ('title' in thisElement) {

                const childNode = thisElement.title;
                if (this.title) {
                    this.valid = false;
                    console.error('ERROR: Duplicate title in DFPage.');
                } else {
                    const newTitle = new DFTitle(childNode);
                    if (newTitle.isValid()) this.title = newTitle;
                    else this.valid = false;
                }

            } else {

                const sectionKeys = ['mcSection', 'nrSection', 'wrSection'];
                var sectionKey = false;
                var sectionNode = false;
                for (const key of sectionKeys) {
                    if (key in thisElement) {
                        sectionKey = key;
                        sectionNode = thisElement[key];
                    }
                }

                if (sectionNode !== false) {
                    const newSection = new DFSection(sectionNode, sectionKey, document, sectionNum++);
                    if (newSection.isValid()) this.sections.push(newSection);
                    else this.valid = false;
                }

            }
        });
	}

	getTitle() {
		return this.title;
	}

	getNumSections() {
		return this.sections.length;
	}

	sectionAtIndex(index) {
		return index < this.sections.length ? this.sections[index] : null;
	}
}

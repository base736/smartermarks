/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as PDFLib from '/js/lib/pdf-lib/pdf-lib.bundle.mjs';
import fontkit from '/js/lib/pdf-lib/fontkit.bundle.mjs';

import { BlockModel } from './BlockModel.js?7.0';
import { QRCode, QRErrorCorrectLevel } from './QRCode.js?7.0';
import { localizationInstance } from './Localization.js?7.0';

const fontURLs = {
    roman: '/fonts/cmunrm.ttf',
    bold: '/fonts/cmunbx.ttf'
};

const userDataPrefix = 'https://userdata.smartermarks.com/';

const moduleSize = 2.0;

export const pointsPerInch = 72.0;
const chunkMinRows = 3;

const defaultMarginInches = 0.75;
const middleGap = 0.25 * pointsPerInch;

const minSlimHeader = 58.0;

const minGapWidth = 0.20 * pointsPerInch;

const circleSize = 10.0;
const circleSpacing = 6.0;
const boxpadding = 5.0;
const boxmargin = 10.0;
const itemIndent = 0.43 * pointsPerInch;
const itemIncrement = 0.3 * pointsPerInch;
const wrColumnSpacing = 0.2 * pointsPerInch;
const nameLineHeight = 25.0;
const columnExtra = 10.0;
const headerGap = 10.0;

const incrementAdjacentCircles = circleSize + circleSpacing;
const boxWidth = incrementAdjacentCircles;
const boxHeight = boxWidth * 1.7;

let entities = null;

async function htmlToUTF8(input) {

    // Load entities if not already loaded

    if (entities === null) {

        // Fetch the entities.csv file
        
        let response;
        try {
            response = await fetch('/js/FormEngine/entities.csv');
        } catch (error) {
            console.error("ERROR: Unable to open 'entities.csv'.", error);
            throw new Error("Unable to open 'entities.csv'.");
        }

        if (!response.ok) {
            console.error("ERROR: Unable to open 'entities.csv'.");
            throw new Error("Unable to open 'entities.csv'.");
        }

        const text = await response.text();
        entities = {};
        const lines = text.split('\n');
        for (let line of lines) {
            line = line.trim();
            if (line.length === 0) continue;
            const bits = line.split(',');
            if (bits.length >= 2) {
                entities[bits[0]] = bits[1];
            }
        }
    }

    // Swap HTML entities for unicode entities

    for (const [entity, value] of Object.entries(entities)) {
        input = input.split(entity).join(value);
    }

    // Translate unicode entities

    input = input.replace(/&#(x[0-9a-fA-F]+|\d+);/g, (match, numStr) => {
        let codePoint;

        if (numStr.startsWith('x') || numStr.startsWith('X')) {
            // Hexadecimal numeric character reference
            codePoint = parseInt(numStr.slice(1), 16);
        } else {
            // Decimal numeric character reference
            codePoint = parseInt(numStr, 10);
        }

        if (isNaN(codePoint) || codePoint > 0x10FFFF) {
            // Invalid code point; return the original match
            return match;
        }

        return String.fromCodePoint(codePoint);
    });

    return input;
}

function fontHeight(font, points) {

    // Replacment for heightOfFontAtSize, which doesn't work properly for CM fonts

    const { ascent, lineGap } = font.embedder.font;
    return (ascent - lineGap) * points / 1000;
}

function isLetterChar(character) {
    if (/[a-zA-Z]/.test(character)) return true;
    else return false;
}

function isNameChar(character) {
    if (/[a-zA-Z]/.test(character)) return true;
    else if (character === '-') return true;
    else if (character === '\'') return true;
    else if (character === ' ') return true;
    else if (character === ',') return true;
    else return false;
}

function emptyParentheses(text) {
    let position = 0;
    let open;
    let output = "";
    while ((open = text.indexOf("(", position)) !== -1) {
        output += text.slice(position, open);
        position = text.indexOf(")", open);
        if (position === -1) break;
        else position = position + 1;
    }
    if (position !== -1) output += text.slice(position);
    return output;
}

async function getNamePart(text) {
    let filteredText = emptyParentheses(text);

    // Find the largest block of text in the string that contains no numbers
    let name = "";
    let nextStart = 0;
    while (nextStart < filteredText.length) {
        while (nextStart < filteredText.length && !isNameChar(filteredText[nextStart])) {
            nextStart++;
        }
        if (nextStart < filteredText.length) {
            let nextEnd = nextStart;
            while (nextEnd < filteredText.length && isNameChar(filteredText[nextEnd])) {
                nextEnd++;
            }
            let thisBlock = filteredText.slice(nextStart, nextEnd);

            // Strip off any leading or trailing characters that are not letters
            let nameStart = 0;
            while (nameStart < thisBlock.length && !isLetterChar(thisBlock[nameStart])) {
                nameStart++;
            }
            let nameEnd = thisBlock.length - 1;
            while (nameEnd >= 0 && !isLetterChar(thisBlock[nameEnd])) {
                nameEnd--;
            }
            if (nameStart <= nameEnd) {
                thisBlock = thisBlock.slice(nameStart, nameEnd + 1);
            } else {
                thisBlock = "";
            }

            // Keep it if it's the largest
            if (thisBlock.length > name.length) name = thisBlock;
            nextStart = nextEnd;
        }
    }

    return name;
}

function getIDPart(text) {
    let filteredText = emptyParentheses(text);

    // Find the largest block of text in the string that contains only numbers
    let idNumber = "";
    let nextStart = 0;
    while (nextStart < filteredText.length) {
        while (nextStart < filteredText.length && !/\d/.test(filteredText[nextStart])) {
            nextStart++;
        }
        if (nextStart < filteredText.length) {
            let nextEnd = nextStart;
            while (nextEnd < filteredText.length && /\d/.test(filteredText[nextEnd])) {
                nextEnd++;
            }
            let thisID = filteredText.slice(nextStart, nextEnd);
            if (thisID.length > idNumber.length) idNumber = thisID;
            nextStart = nextEnd;
        }
    }

    return idNumber;
}

export class PSForm {
	constructor(format, studentData) {
		this.format = format;
		this.studentData = studentData;
		this.pages = [];
		this.images = {};
		this.sectionOffsets = [];
		this.blockModels = [];

		this.version = format.getIntPropertyForName('version', 1);
		this.copiesPerPage = format.getIntPropertyForName('copiesPerPage', 1);
		this.pageWidth = format.getDoublePropertyForName('pageWidth', 8.5) * pointsPerInch;
		this.pageHeight = format.getDoublePropertyForName('pageHeight', 11) * pointsPerInch;
		this.topMarginSize = format.getDoublePropertyForName('topMarginSize', defaultMarginInches) * pointsPerInch;
		this.bottomMarginSize = format.getDoublePropertyForName('bottomMarginSize', defaultMarginInches) * pointsPerInch;
		this.leftMarginSize = format.getDoublePropertyForName('leftMarginSize', defaultMarginInches) * pointsPerInch;
		this.rightMarginSize = format.getDoublePropertyForName('rightMarginSize', defaultMarginInches) * pointsPerInch;
		this.logoHeight = format.getDoublePropertyForName('logoHeight', 0.75) * pointsPerInch;
		this.pageY = 0;
		this.updateModels = true;

		this.pdfDoc = null;
        this.formPartTitle = false;
	}

	async initForm() {
		this.pdfDoc = await PDFLib.PDFDocument.create();
        this.pdfDoc.registerFontkit(fontkit);

        this.fonts = {};
        for (const key in fontURLs) {
            const fontBytes = await fetch(fontURLs[key]).then(res => res.arrayBuffer());
            this.fonts[key] = await this.pdfDoc.embedFont(fontBytes);
        }

		let numCopies;
        if (this.studentData.length === 0) {
            numCopies = this.copiesPerPage;
        } else if ((this.studentData.length === 1) && !('nameText' in this.studentData[0])) {
            numCopies = this.copiesPerPage;
        } else {
            numCopies = this.studentData.length;
        }

		this.updateModels = true;
		for (let copyIndex = 0; copyIndex < numCopies; ++copyIndex) {
			let pageCopy = copyIndex % this.copiesPerPage;
			this.pageY = this.pageHeight - (this.pageHeight * pageCopy) / this.copiesPerPage - this.topMarginSize;
			this.startY = this.pageY;

			let nameText, qrText;
            if (this.studentData.length > 0) {
                let dataIndex = Math.min(copyIndex, this.studentData.length - 1);
                nameText = ('nameText' in this.studentData[dataIndex]) ? this.studentData[dataIndex]['nameText'] : '';
                qrText = ('qrText' in this.studentData[dataIndex]) ? this.studentData[dataIndex]['qrText'] : '';
            } else {
                nameText = '';
                qrText = '';
            }

			let sectionOffset = 0;
			for (let i = 0; i < this.format.getNumPages(); ++i) {
				this.pointDataPage = -1;

				let pageFormat = this.format.pageAtIndex(i);

				this.formPartTitle = pageFormat.getTitle();
				if (pageCopy === 0) await this.addPage(0, nameText, qrText);
				else if (this.formPartTitle) await this.drawHeaders(nameText, qrText);

				this.startY = this.pageY;

				for (let j = 0; j < pageFormat.getNumSections(); ++j) {
					let section = pageFormat.sectionAtIndex(j);
					let numQuestions = section.getNumQuestions();
					let questionNumbers = this.extractQuestionNumbers(section, numQuestions);
					if (j !== 0 || i === 0) this.drawVerticalSpace(0.1);
					await this.drawSection(section, sectionOffset, nameText, questionNumbers, numQuestions);
					++sectionOffset;
				}
			}

			if (copyIndex === 0) {
                if (this.blockModels.length > 1) this.copiesPerPage = 1;
                else if (this.copiesPerPage > 1) {
                    let allowedPerPage = Math.floor(this.pageHeight / (this.pageHeight - (this.pageY - this.bottomMarginSize)));
                    if (allowedPerPage < 1) allowedPerPage = 1;
                    if (this.copiesPerPage > allowedPerPage) this.copiesPerPage = allowedPerPage;
                }
                if (this.studentData.length === 0) numCopies = this.copiesPerPage;        
            }

			this.updateModels = false;
		}
	}

    getNumPages() {
        return this.blockModels.length;
    }

    getSectionOffset(pageNum) {
        if (pageNum >= 0 && pageNum < this.sectionOffsets.length) {
            return this.sectionOffsets[pageNum];
        } else return -1;
    }

    getBlockModel(pageNum) {
        if (pageNum >= this.blockModels.length) return [];
        else return this.blockModels[pageNum];
    }

	async drawSection(section, sectionOffset, nameText, questionNumbers, numQuestions) {
		if (section.getType() === 'MCSectionType') {
			await this.splitMultipleChoiceSection(section, sectionOffset, nameText, questionNumbers, numQuestions);
		} else if (section.getType() === 'NRSectionType') {
			await this.splitNumericalResponseSection(section, sectionOffset, nameText, questionNumbers, numQuestions);
		} else if (section.getType() === 'WRSectionType') {
			await this.splitWrittenResponseSection(section, sectionOffset, nameText, questionNumbers, numQuestions);
		}
	}

	async splitMultipleChoiceSection(section, sectionOffset, nameText, questionNumbers, numQuestions) {
		let columnCount = section.getIntPropertyForName('columnCount', 5);
		let defaultChoices = section.getIntPropertyForName('choiceCount', 4);
		let choices = this.extractChoices(section, numQuestions);

        let firstColumns = columnCount;
        if (firstColumns == -1) {

            // Get automatic number of columns for first page

            const maxColumns = (numQuestions < 5) ? 5 : 10;
            const maxRows = Math.floor((this.pageY - this.bottomMarginSize + itemIncrement - circleSize / 2.0) / itemIncrement);
    
            firstColumns = 1;
            for (let thisColumns = 2; thisColumns <= maxColumns; ++thisColumns) {
                let { gapWidth: thisGapWidth } = 
                    this.calculateMCLayout(numQuestions, thisColumns, 0, maxRows, choices, defaultChoices, this.version < 3);        
                if (thisGapWidth >= minGapWidth) firstColumns = thisColumns;
            }
        }

		let numRows = Math.ceil(numQuestions / firstColumns);
        let minHeight = Math.min(numRows, chunkMinRows) * itemIncrement;
		if (section.hasProperty('header')) minHeight += 18.0 + headerGap;

        let extra = numRows * itemIncrement - (this.pageY - this.bottomMarginSize);
		if (section.hasProperty('header')) extra += 18.0 + headerGap;

        let needsNewPage = false;
        if (this.pageY < minHeight + this.bottomMarginSize) needsNewPage = true;
        if (extra > 0.0 && extra < chunkMinRows * itemIncrement && numRows < 2 * chunkMinRows) needsNewPage = true;
		if (needsNewPage) await this.addPage(sectionOffset, nameText, '');

		if (section.hasProperty('header')) await this.drawHeader(section.getStringPropertyForName('header'));

		let startIndex = 0;
		do {
			startIndex = await this.drawMultipleChoiceSection(questionNumbers, startIndex, columnCount, choices, defaultChoices);
			if (startIndex < questionNumbers.length) {
				await this.addPage(sectionOffset, nameText, '');
				this.pageY -= 15.0;
			}
		} while (startIndex < questionNumbers.length);
	}

	async splitNumericalResponseSection(section, sectionOffset, nameText, questionNumbers, numQuestions) {
		let nrStyle = this.extractNRStyle(section);

		let jMax = (nrStyle === 'Fractions') ? 12 : 11;
        let minHeight = numQuestions > 0 ? (boxHeight + jMax * (circleSize + circleSpacing) + itemIncrement - (circleSize + circleSpacing)) : 0.0;
		if (section.hasProperty('header')) minHeight += 18.0 + headerGap;

        if (this.pageY < minHeight + this.bottomMarginSize && this.blockModels[this.pointDataPage].length > 0) {
			await this.addPage(sectionOffset, nameText, '');
		}

		if (section.hasProperty('header')) await this.drawHeader(section.getStringPropertyForName('header'));

		let columnCount = section.getIntPropertyForName('columnCount', 5);
		let defaultDigits = section.getIntPropertyForName('digitCount', 4);
		let digitCounts = this.extractDigitCounts(section, numQuestions);

		let startIndex = 0;
		do {
			startIndex = await this.drawNumericalResponseSection(questionNumbers, nrStyle, startIndex, columnCount, digitCounts, defaultDigits);
			if (startIndex < questionNumbers.length) {
				await this.addPage(sectionOffset, nameText, '');
				this.pageY -= 15.0;
			}
		} while (startIndex < questionNumbers.length);
	}

	async splitWrittenResponseSection(section, sectionOffset, nameText, questionNumbers, numQuestions) {
		let includeSpace = section.getStringPropertyForName('includeSpace', 'YES') === 'YES';

		if (includeSpace) {
			await this.splitFullWRSection(section, sectionOffset, nameText, questionNumbers, numQuestions);
		} else {
			await this.splitCompressedWRSection(section, sectionOffset, nameText, questionNumbers, numQuestions);
		}
	}

	async splitFullWRSection(section, sectionOffset, nameText, questionNumbers, numQuestions) {
		let minHeightInches = numQuestions === 0 ? 0.0 : section.questionAtIndex(0).getDoublePropertyForName('height', 1.0);
		let minHeight = minHeightInches * pointsPerInch;

		if (section.hasProperty('header')) minHeight += 18.0 + headerGap;
		if (this.pageY < minHeight + this.bottomMarginSize && this.blockModels[this.pointDataPage].length > 0) {
			await this.addPage(sectionOffset, nameText, '');
		}

		if (section.hasProperty('header')) await this.drawHeader(section.getStringPropertyForName('header'));

		let startIndex = 0;
		do {
			startIndex = await this.drawFullWRSection(questionNumbers, startIndex, section);
			if (startIndex < questionNumbers.length) {
				await this.addPage(sectionOffset, nameText, '');
				this.pageY -= 15.0;
			}
		} while (startIndex < questionNumbers.length);
	}

	async splitCompressedWRSection(section, sectionOffset, nameText, questionNumbers, numQuestions) {
		let columnCount = section.getIntPropertyForName('columnCount', 2);
		let criteria = this.extractCriteria(section, numQuestions);

        let requiredRows = 0;
		let requiredQuestions = Math.min(questionNumbers.length, columnCount);
		for (let i = 0; i < requiredQuestions; ++i) if (criteria[i].length > requiredRows) requiredRows = criteria[i].length;
		let minHeight = requiredRows * (circleSize + circleSpacing) + itemIncrement;

		if (section.hasProperty('header')) minHeight += 18.0 + headerGap;
		if (this.pageY < minHeight + this.bottomMarginSize && this.blockModels[this.pointDataPage].length > 0) {
			await this.addPage(sectionOffset, nameText, '');
		}

        if (section.hasProperty('header')) this.drawHeader(section.getStringPropertyForName('header'));

        let startIndex = 0;
		do {
			startIndex = await this.drawCompressedWRSection(questionNumbers, startIndex, columnCount, criteria);
			if (startIndex < questionNumbers.length) {
				await this.addPage(sectionOffset, nameText, '');
				this.pageY -= 15.0;
			}
		} while (startIndex < questionNumbers.length);
	}

	extractQuestionNumbers(section, numQuestions) {
		let questionNumbers = [];
		for (let q = 0; q < numQuestions; ++q) {
			let question = section.questionAtIndex(q);
			let rawQuestionNumber = question.getQuestionNumber();
			let decimalPos = rawQuestionNumber.indexOf('.');
			let questionNumber = decimalPos === -1 ? rawQuestionNumber : rawQuestionNumber.substring(decimalPos + 1);
			questionNumbers.push(questionNumber);
		}
		return questionNumbers;
	}

	extractChoices(section, numQuestions) {
		let choices = [];
		for (let q = 0; q < numQuestions; ++q) {
			let question = section.questionAtIndex(q);
			let type = question.getStringPropertyForName('questionType');
			let choiceCount = question.getIntPropertyForName('choiceCount', 4);
			let thisChoices = '';
			if (type === 'Letter') {
				for (let r = 0; r < choiceCount; ++r) thisChoices += String.fromCharCode('A'.charCodeAt(0) + r);
			} else if (type === 'TrueFalse') {
				thisChoices = 'TF';
			}
			choices.push(thisChoices);
		}
		return choices;
	}

	extractNRStyle(section) {
		let nrStyle = this.format.getStringPropertyForName('nrStyle', 'Default');
		if (section.hasProperty('nrStyle')) nrStyle = section.getStringPropertyForName('nrStyle');
		return nrStyle;
	}

	extractDigitCounts(section, numQuestions) {
		let digitCounts = [];
		for (let q = 0; q < numQuestions; ++q) {
			let question = section.questionAtIndex(q);
			digitCounts.push(question.getIntPropertyForName('digitCount', 4));
		}
		return digitCounts;
	}

	extractCriteria(section, numQuestions) {
		let criteria = [];
		for (let q = 0; q < numQuestions; ++q) criteria.push(section.questionAtIndex(q).getCriteria());
		return criteria;
	}

	async addPage(sectionOffset, nameText, qrText) {
		this.pdfDoc.addPage([this.pageWidth, this.pageHeight]);

		this.pageY = this.pageHeight - this.topMarginSize;
		this.startY = this.pageY;

		if (this.updateModels) {
			this.sectionOffsets.push(sectionOffset);
			this.blockModels.push([]);
			this.pointDataPage = this.blockModels.length - 1;
		} else {
			this.pointDataPage++;
		}

		await this.drawHeaders(nameText, qrText);
        this.formPartTitle = false;
	}

    async embedImage(imageHash) {
        const imageUrl = userDataPrefix + imageHash;
        const imageResponse = await fetch(imageUrl);

        const imageBuffer = await imageResponse.arrayBuffer();

        let image;
        const bytes = new Uint8Array(imageBuffer);
        if (bytes[0] === 0xFF && bytes[1] === 0xD8 && bytes[2] === 0xFF) {
            image = await this.pdfDoc.embedJpg(imageBuffer);
        } else if (bytes[0] === 0x89 && bytes[1] === 0x50 && bytes[2] === 0x4E && bytes[3] === 0x47) {
            image = await this.pdfDoc.embedPng(imageBuffer);
        } else {
            throw new Error('Unsupported image type');
        }

        return image;
    }

	async drawHeaders(nameText, qrText) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

		if (this.formPartTitle) {
            let title = this.formPartTitle;

			if (title.hasProperty('logoHash')) {
                const logoHash = title.getStringPropertyForName('logoHash');
                const image = await this.embedImage(logoHash);

                const logoWidth = this.logoHeight * (image.width / image.height);

                page.drawImage(image, {
                    x: this.leftMarginSize,
                    y: this.pageY - this.logoHeight,
                    width: logoWidth,
                    height: this.logoHeight,
                });

                this.pageY -= this.logoHeight + 15;
			}

			if (!title.hasIDField()) {
				if (this.version === 3) {
					await this.drawQRTitle(title.getLines(), title.getNameLines(), nameText, qrText);
				} else if (title.hasNameField()) {
					await this.drawSlimTitle(title.getLines(), title.getNameLines(), nameText);
				} else if (nameText.length === 0 && this.version === 1) {
                    const nameString = await localizationInstance.getString('form_name_field');
					await this.drawSlimTitle(title.getLines(), [nameString], nameText);
				} else if (this.version === 2) {
					await this.drawOldTitle(title.getLines(), title.getNameLines(), nameText, qrText);
				} else {
					await this.drawQRTitle(title.getLines(), title.getNameLines(), nameText, qrText);
				}
			} else {
				if (this.version === 3) {
					await this.drawIDTitle(title.idDigits(), title.idLabel(), title.getLines(), title.getNameLines(), nameText, qrText);
				} else {
					await this.drawIDTitle(title.idDigits(), title.idLabel(), title.getLines(), title.getNameLines(), nameText, '');
				}
			}

			if (title.hasNameField()) {
                await this.drawNameField(title.nameLetters(), title.nameLabel(), nameText);
            }
		} else {
            const nameFieldString = await localizationInstance.getString('form_name_field');
			await this.drawSlimTitle([], [nameFieldString], nameText);
		}
	}

	drawVerticalSpace(spaceInches) {
		this.pageY -= spaceInches * pointsPerInch;
	}

	async drawCircledCharacter(character, point) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

        page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(point.x, point.y)
        );

        if (character === '/') {
            page.drawLine({
                start: { x: -circleSize * 0.1, y: -circleSize * 0.3 },
                end: { x: circleSize * 0.1, y: circleSize * 0.3 },
                thickness: 0.75,
                color: PDFLib.rgb(0.33, 0.33, 0.33),
            });
        } else if (character === '-') {
            page.drawLine({
                start: { x: -circleSize / 4.0, y: 0.0 },
                end: { x: circleSize / 4.0, y: 0.0 },
                thickness: 0.75,
                color: PDFLib.rgb(0.33, 0.33, 0.33),
            });
        } else if (character === '.') {
            page.drawCircle({
                x: 0.0,
                y: 0.0,
                size: 0.375,
                color: PDFLib.rgb(0.33, 0.33, 0.33),
            });
        } else if (character === ',') {
            page.drawText(',', {
                x: 0.0,
                y: 2.0,
                font: this.fonts.roman,
                size: 8,
                color: PDFLib.rgb(0.33, 0.33, 0.33),
            });
        } else {
            let text;
            let fontSize;
            if (character === '+') {
                fontSize = 6;
                const decimalString = await localizationInstance.getString('form_decimal');
                text = '0' + decimalString + '5';
            } else {
                fontSize = 8;
                text = character;
            }
        
            page.drawText(text, {
                x: -0.5 * this.fonts.roman.widthOfTextAtSize(text, fontSize),
                y: -0.5 * fontHeight(this.fonts.roman, fontSize),
                font: this.fonts.roman,
                size: fontSize,
                color: PDFLib.rgb(0.33, 0.33, 0.33),
            });
        }
    
        page.drawCircle({
            x: 0.0,
            y: 0.0,
            size: circleSize / 2.0,
            borderColor: PDFLib.rgb(0.0, 0.0, 0.0),
            borderWidth: 0.5,
        });

        page.pushOperators(PDFLib.popGraphicsState());
	}

	async drawFilledCircle(point) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

        page.drawCircle({
            x: point.x,
            y: point.y,
            size: circleSize / 2.0,
            color: PDFLib.rgb(0.0, 0.0, 0.0),
            borderWidth: 0.5,
            borderColor: PDFLib.rgb(0.0, 0.0, 0.0),
        });
	}

	async drawNameField(numLetters, label, nameText) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

		let columnHeight = boxWidth * numLetters;
		let name = await getNamePart(nameText);
		
		this.pageY -= itemIncrement;
        page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(this.leftMarginSize, this.pageY),
            PDFLib.rotateDegrees(90),
            PDFLib.translate(-columnHeight + 0.5 * boxWidth, 0.0)
        );

        const labelDecoded = await htmlToUTF8(label);
		page.drawText(labelDecoded, { 
            x: 0.5 * boxWidth * (numLetters - 1) - 0.5 * this.fonts.roman.widthOfTextAtSize(labelDecoded, 12),
            y: -fontHeight(this.fonts.roman, 12),
            font: this.fonts.roman,
            size: 12, 
            color: PDFLib.rgb(0, 0, 0)
        });

		let workingPoint = { x: 0.0, y: -17 - boxHeight / 2.0 };

        page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(workingPoint.x, workingPoint.y)
        );

		for (let k = 0; k < numLetters; ++k) {
			if (k > 0) {
                page.pushOperators(
                    PDFLib.translate(boxWidth, 0.0)
                );
            }

			page.drawRectangle({
				x: -boxWidth / 2.0,
				y: -boxHeight / 2.0,
				width: boxWidth,
				height: boxHeight,
				borderColor: PDFLib.rgb(0, 0, 0),
				borderWidth: 1.0
			});

			if (k < name.length && /^[a-zA-Z]+$/.test(name[k])) {
				page.drawText(name[k], {
                    x: -0.5 * this.fonts.roman.widthOfTextAtSize(name[k], 12),
                    y: -0.5 * fontHeight(this.fonts.roman, 12),
                    font: this.fonts.roman,
                    size: 12, 
                    color: PDFLib.rgb(0, 0, 0)
                });
			}
		}

        page.pushOperators(PDFLib.popGraphicsState());

        workingPoint.y -= boxHeight / 2.0 + circleSpacing + circleSize / 2.0;

        const xGrid = {
            offset: {
                class: 'classNoClass',
                value: this.leftMarginSize - workingPoint.y
            },
            indices: [{
                class: 'classAdjacentCircles', 
                increment: incrementAdjacentCircles
            }]
        };

		const yGrid = {
            offset: {
                class: 'classNoClass',
                value: this.pageHeight - this.pageY + 0.5 * boxWidth
            },
            indices: [{
                class: 'classAdjacentCircles', 
                increment: incrementAdjacentCircles
            }]
        }
    
        let namePointData = new BlockModel(xGrid, yGrid);

        let topY = workingPoint.y;
		for (let k = 0; k < numLetters; ++k) {
            namePointData.addChunk(27, 1);

			for (let j = 0; j <= 26; ++j) {
                const newIndices = namePointData.getNewPoint();
                newIndices.xIndices[0] = j;
                newIndices.yIndices[0] = k;
                namePointData.addPoint(newIndices);

                let char = (j === 0) ? ' ' : String.fromCharCode('A'.charCodeAt(0) + j - 1);
				if (k < name.length) {
					let currentChar = name[k].toUpperCase();
					if (currentChar === char || (currentChar === ' ' && j === 0)) {
						await this.drawFilledCircle(workingPoint);
					} else {
						await this.drawCircledCharacter(char, workingPoint);
					}
				} else {
					await this.drawCircledCharacter(char, workingPoint);
				}
				workingPoint.y -= circleSize + circleSpacing;
			}
			workingPoint.x += boxWidth;
			workingPoint.y = topY;
		}

        page.pushOperators(PDFLib.popGraphicsState());
        this.pageY -= columnHeight;

		if (this.updateModels) this.blockModels[this.pointDataPage].push(namePointData);
	}

	async drawIDTitle(numDigits, label, textLines, nameLines, nameText, qrText) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

		let nameLineCount = nameLines.length;
		let textHeight = (textLines.length + 2) * 15.0;
		if (nameLineCount > 1) textHeight += (nameLineCount - 1) * nameLineHeight;
		
		let columnWidth = (this.pageWidth - this.rightMarginSize - this.leftMarginSize - middleGap) / 2.0;
		let columnHeight = boxWidth * numDigits;
		if (columnHeight < textHeight) columnHeight = textHeight;

		// Draw QR code

		if (qrText.length > 0) {
            const qrCode = new QRCode({ 
                correctLevel: QRErrorCorrectLevel.L,
                text: qrText 
            });
            let barcodeHeight = moduleSize * qrCode.getModuleCount();

            page.pushOperators(
                PDFLib.pushGraphicsState(),
                PDFLib.translate(this.pageWidth - this.rightMarginSize - barcodeHeight, this.startY - barcodeHeight),
                PDFLib.scale(moduleSize, moduleSize)
            );
    
            qrCode.drawToPage(page);

            page.pushOperators(PDFLib.popGraphicsState());

			if (columnHeight < barcodeHeight) columnHeight = barcodeHeight;
			columnWidth -= (barcodeHeight + middleGap) / 2.0;
		}

		// Draw ID section in left column
        
		let idNumber = getIDPart(nameText);
		page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(this.leftMarginSize, this.pageY),
		    PDFLib.rotateDegrees(90),
		    PDFLib.translate(-columnHeight + 0.5 * boxWidth, 0.0)
        );

        const labelDecoded = await htmlToUTF8(label);
		page.drawText(labelDecoded, {
            x: 0.5 * boxWidth * (numDigits - 1) - 0.5 * this.fonts.roman.widthOfTextAtSize(labelDecoded, 12),
            y: -fontHeight(this.fonts.roman, 12),
            font: this.fonts.roman,
            size: 12, 
            color: PDFLib.rgb(0, 0, 0)
        });

		let workingPoint = { x: 0.0, y: -17 - boxHeight / 2.0 };
		page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(workingPoint.x, workingPoint.y)
        );

		for (let k = 0; k < numDigits; ++k) {
			if (k > 0) {
                page.pushOperators(
                    PDFLib.translate(boxWidth, 0.0)
                );
            }

			page.drawRectangle({
				x: -boxWidth / 2.0,
				y: -boxHeight / 2.0,
				width: boxWidth,
				height: boxHeight,
				borderColor: PDFLib.rgb(0, 0, 0),
				borderWidth: 1.0
			});

			if (k < idNumber.length && /^\d+$/.test(idNumber[k])) {
				page.drawText(idNumber[k], {
                    x: -0.5 * this.fonts.roman.widthOfTextAtSize(idNumber[k], 12),
                    y: -0.5 * fontHeight(this.fonts.roman, 12),
                    font: this.fonts.roman,
                    size: 12, 
                    color: PDFLib.rgb(0, 0, 0)
                });
			}
		}

        page.pushOperators(PDFLib.popGraphicsState());
		workingPoint.y -= boxHeight / 2.0 + circleSpacing + circleSize / 2.0;

        const xGrid = {
            offset: {
                class: 'classNoClass',
                value: this.leftMarginSize - workingPoint.y
            },
            indices: [{
                class: 'classAdjacentCircles', 
                increment: incrementAdjacentCircles
            }]
        }
        
		const yGrid = {
            offset: {
                class: 'classNoClass',
                value: this.pageHeight - this.pageY + columnHeight - (numDigits - 0.5) * boxWidth
            },
            indices: [{
                class: 'classAdjacentCircles', 
                increment: incrementAdjacentCircles
            }]
        }
		    
        let idPointData = new BlockModel(xGrid, yGrid);

        let topY = workingPoint.y;
		for (let k = 0; k < numDigits; ++k) {
            idPointData.addChunk(10, 1);
            for (let j = 0; j < 10; ++j) {
                const newIndices = idPointData.getNewPoint();
                newIndices.xIndices[0] = j;
                newIndices.yIndices[0] = k;
                idPointData.addPoint(newIndices);
    
                let char = String.fromCharCode('0'.charCodeAt(0) + j);
				if (k < idNumber.length && idNumber[k] === char) {
					await this.drawFilledCircle(workingPoint);
				} else {
					await this.drawCircledCharacter(char, workingPoint);
				}
				workingPoint.y -= circleSize + circleSpacing;
			}
			workingPoint.x += boxWidth;
			workingPoint.y = topY;
		}

        page.pushOperators(PDFLib.popGraphicsState());

		// Draw name line and titles in right column

		let textStartY = this.startY + await this.drawNameLines(nameLines, nameText, 
            this.leftMarginSize + columnWidth + middleGap, this.startY, columnWidth);
		if (textStartY > this.pageY) textStartY = this.pageY;

        page.pushOperators(
            PDFLib.pushGraphicsState()
        );
/*
		page.drawRectangle({
			x: this.leftMarginSize + columnWidth + middleGap,
			y: textStartY - textHeight - columnExtra,
			width: columnWidth,
			height: textHeight + 2.0 * columnExtra
		});
		page.clip();
		page.endPath();
*/
		for (let i = 0; i < textLines.length; ++i) {
			let textBaseline = textStartY - i * 15.0;

            const textLineDecoded = await htmlToUTF8(textLines[i]);
            page.drawText(textLineDecoded, {
				x: this.leftMarginSize + columnWidth + middleGap,
				y: textBaseline,
                font: this.fonts.roman,
				size: 12,
				color: PDFLib.rgb(0, 0, 0)
			});
		}

        page.pushOperators(PDFLib.popGraphicsState());
		this.pageY -= columnHeight;

		if (this.updateModels) this.blockModels[this.pointDataPage].push(idPointData);
	}

    dataUrlToArrayBuffer(dataUrl) {
        const base64 = dataUrl.split(',')[1];
        const binaryString = atob(base64);
        const len = binaryString.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = binaryString.charCodeAt(i);
        }
        return bytes.buffer;
    }

	async drawOldTitle(textLines, nameLines, nameText, qrText) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

		let nameLineCount = nameLines.length;
		let textHeight = textLines.length * 15.0;
		let nameHeight = nameLineCount * nameLineHeight;
		
		let columnHeight = nameHeight + textHeight;
		if (columnHeight < minSlimHeader) columnHeight = minSlimHeader;

		let offset = 0.0;
		if (qrText.length > 0) {
            const qrCode = new QRCode({ 
                correctLevel: QRErrorCorrectLevel.L,
                text: qrText 
            });
            let barcodeHeight = moduleSize * qrCode.getModuleCount();

            page.pushOperators(
                PDFLib.pushGraphicsState(),
                PDFLib.translate(this.leftMarginSize, this.pageY - columnHeight),
                PDFLib.scale(moduleSize, moduleSize)
            );
    
            qrCode.drawToPage(page);

            page.pushOperators(PDFLib.popGraphicsState());

            offset = barcodeHeight + 15.0;    
		}

		// Draw title in left column

		let columnWidth = (this.pageWidth - this.rightMarginSize - this.leftMarginSize - offset - middleGap) / 2.0;

        page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(this.leftMarginSize + offset, this.pageY - columnHeight)
        );
/*
		page.drawRectangle({
			x: 0.0,
			y: -columnExtra,
			width: columnWidth,
			height: columnHeight + 2.0 * columnExtra
		});
		page.clip();
		page.endPath();
*/
		for (let i = 0; i < textLines.length; ++i) {
            const textLineDecoded = await htmlToUTF8(textLines[i]);
			page.drawText(textLineDecoded, {
				x: 0.0,
				y: 15.0 * (textLines.length - i - 1),
                font: this.fonts.roman,
				size: 12,
				color: PDFLib.rgb(0, 0, 0)
			});
		}

        page.pushOperators(PDFLib.popGraphicsState());

		// Draw name line in right column

		await this.drawNameLines(nameLines, nameText, this.leftMarginSize + offset + columnWidth + middleGap, this.startY, columnWidth);
		this.pageY -= columnHeight;
	}

	async drawQRTitle(textLines, nameLines, nameText, qrText) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

		let nameLineCount = nameLines.length;
		let textHeight = textLines.length * 15.0;
		let nameHeight = nameLineCount * nameLineHeight;
		
		let columnHeight = nameHeight + textHeight;
		if (columnHeight < minSlimHeader) columnHeight = minSlimHeader;

		let columnWidth = (this.pageWidth - this.rightMarginSize - this.leftMarginSize - middleGap) / 2.0;
		if (qrText.length > 0) {

            const qrCode = new QRCode({ 
                correctLevel: QRErrorCorrectLevel.L,
                text: qrText 
            });

            let barcodeHeight = moduleSize * qrCode.getModuleCount();

            page.pushOperators(
                PDFLib.pushGraphicsState(),
                PDFLib.translate(this.pageWidth - this.rightMarginSize - barcodeHeight, this.startY - barcodeHeight),
                PDFLib.scale(moduleSize, moduleSize)
            );
    
            qrCode.drawToPage(page);

            page.pushOperators(PDFLib.popGraphicsState());

			columnWidth -= (barcodeHeight + middleGap) / 2.0;
		}

		// Draw title in left column

        page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(this.leftMarginSize, this.pageY - textHeight)
        );
/*
		page.drawRectangle({
			x: 0.0,
			y: -columnExtra,
			width: columnWidth,
			height: textHeight + 2.0 * columnExtra
		});
		page.clip();
		page.endPath();
*/
		for (let i = 0; i < textLines.length; ++i) {
            const textLineDecoded = await htmlToUTF8(textLines[i]);
			page.drawText(textLineDecoded, {
				x: 0.0,
				y: 15.0 * (textLines.length - i - 1),
                font: this.fonts.roman,
				size: 12,
				color: PDFLib.rgb(0, 0, 0)
			});
		}

        page.pushOperators(PDFLib.popGraphicsState());

		// Draw name line in right column

		await this.drawNameLines(nameLines, nameText, this.leftMarginSize + columnWidth + middleGap, this.startY, columnWidth);
		this.pageY = (this.pageY - textHeight < this.startY - columnHeight) ? this.pageY - textHeight : this.startY - columnHeight;
	}

	async drawSlimTitle(textLines, nameLines, nameText) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

		let nameLineCount = nameLines.length;
		let textHeight = textLines.length * 15.0;
		let nameHeight = 15.0 + (nameLineCount - 1) * nameLineHeight;

		let columnWidth = (this.pageWidth - this.rightMarginSize - this.leftMarginSize - middleGap) / 2.0;
		let columnHeight = (nameHeight > textHeight) ? nameHeight : textHeight;

		// Draw title text in left column

        page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(this.leftMarginSize, this.pageY - columnHeight)
        );
/*
		page.drawRectangle({
			x: 0.0,
			y: -columnExtra,
			width: columnWidth,
			height: columnHeight + 2.0 * columnExtra
		});
		page.clip();
		page.endPath();
*/

        let maxWidth = 0.0;
		for (let i = 0; i < textLines.length; ++i) {
            const textLineDecoded = await htmlToUTF8(textLines[i]);
			page.drawText(textLineDecoded, {
				x: 0.0,
				y: columnHeight - 15.0 * i - fontHeight(this.fonts.roman, 12),
                font: this.fonts.roman,
				size: 12,
				color: PDFLib.rgb(0, 0, 0)
			});

			let textSize = this.fonts.roman.widthOfTextAtSize(textLines[i], 12);
			let thisWidth = textSize + 20.0;
			if (thisWidth > columnWidth + middleGap) thisWidth = columnWidth + middleGap;
			if (thisWidth > maxWidth) maxWidth = thisWidth;
		}

        page.pushOperators(PDFLib.popGraphicsState());

		// Draw name lines in the right column

		await this.drawNameLines(nameLines, nameText, this.leftMarginSize + columnWidth + middleGap, this.startY, 
            columnWidth);
		this.pageY -= columnHeight;
	}

	async drawNameLines(nameLines, nameText, x, y, width) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

        page.pushOperators(
            PDFLib.pushGraphicsState(),
            PDFLib.translate(x, y)
        );

		// Draw name line in right column
		
		let lineY = 0.0;

		if (nameText.length > 0) {
            const nameTextDecoded = await htmlToUTF8(nameText);
            page.drawText(nameTextDecoded, {
				x: width - this.fonts.roman.widthOfTextAtSize(nameTextDecoded, 18),
				y: lineY - fontHeight(this.fonts.roman, 18),
                font: this.fonts.roman,
				size: 18,
				color: PDFLib.rgb(0, 0, 0)
			});
			lineY -= nameLineHeight;
		}
        
        lineY -= 15.0;

		const nameString = await localizationInstance.getString('form_name_field');
		let nameLineIndex = nameLines.indexOf(nameString);

		for (let i = 0; i < nameLines.length; ++i) {
			if (nameText.length > 0 && i === nameLineIndex) continue;

            const nameLabelDecoded = await htmlToUTF8(nameLines[i] + ': ');
			page.drawText(nameLabelDecoded, {
				x: 0.0,
				y: lineY,
                font: this.fonts.roman,
				size: 12,
				color: PDFLib.rgb(0, 0, 0)
			});

			page.drawLine({
				start: { 
                    x: this.fonts.roman.widthOfTextAtSize(nameLabelDecoded, 12), 
                    y: lineY
                },
				end: { 
                    x: width, 
                    y: lineY
                },
				thickness: 1.0,
				color: PDFLib.rgb(0, 0, 0)
			});

			lineY -= nameLineHeight;
		}

        page.pushOperators(PDFLib.popGraphicsState());
        
        return lineY;
	}

	async drawHeader(text) {
		const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

		this.pageY -= 18.0;
        const textDecoded = await htmlToUTF8(text);
		page.drawText(textDecoded, {
			x: this.leftMarginSize,
			y: this.pageY,
            font: this.fonts.bold,
			size: 14,
			color: PDFLib.rgb(0, 0, 0)
		});

		this.pageY -= headerGap;
	}

    calculateMCLayout(numQuestions, numColumns, startIndex, maxRows, choices, defaultChoices, bodge) {
        let questionsOnPage = numQuestions;

        let numRows = Math.ceil(questionsOnPage / numColumns);
        if (numRows > maxRows) {
            if (numRows - chunkMinRows < maxRows) {
                maxRows = numRows - chunkMinRows;
            }
            numRows = maxRows;
        }

        if (questionsOnPage > numColumns * numRows) {
            questionsOnPage = numColumns * numRows;
        }

        let columnChoices = Array(numColumns).fill(-1);

        if (bodge) {

            // For compatibility with old results

            for (let i = 0; i < startIndex + numQuestions; ++i) {
                const columnNum = i % numColumns;
                columnChoices[columnNum] = Math.max(columnChoices[columnNum], choices[i].length);
            }

        } else {

            for (let i = startIndex; i < startIndex + questionsOnPage; ++i) {
                const columnNum = Math.floor((i - startIndex) / numRows);
                columnChoices[columnNum] = Math.max(columnChoices[columnNum], choices[i].length);
            }
        }

        columnChoices = columnChoices.map(c => (c === -1) ? defaultChoices : c);
    
        let gapWidth = 0.0;
        if (numColumns > 1) {
            const usedWidth = columnChoices.reduce((sum, choice) => sum + itemIndent + (choice - 1) * (circleSize + circleSpacing) + circleSize / 2.0, 0);
            gapWidth = (this.pageWidth - this.leftMarginSize - this.rightMarginSize - usedWidth) / (numColumns - 1);
        }

        return { numRows, questionsOnPage, columnChoices, gapWidth };
    }

    async drawMultipleChoiceSection(questionNumbers, startIndex, numColumns, choices, defaultChoices) {
        if (choices.length !== questionNumbers.length) {
            throw new Error("ERROR: Question count mismatch in drawMultipleChoiceSection.");
        }

        const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);
        let topY = this.pageY;
        let numQuestions = questionNumbers.length - startIndex;
        if (numQuestions === 0) return startIndex;

        var maxRows = Math.floor((topY - this.bottomMarginSize + itemIncrement - circleSize / 2.0) / itemIncrement);

        if (numColumns == -1) {

            // Get automatic number of columns for this page

            const maxColumns = (numQuestions < 5) ? 5 : 10;
    
            numColumns = 1;
            for (let thisColumns = 2; thisColumns <= maxColumns; ++thisColumns) {
                let { gapWidth: thisGapWidth } = 
                    this.calculateMCLayout(numQuestions, thisColumns, startIndex, maxRows, choices, defaultChoices, this.version < 3);        
                if (thisGapWidth >= minGapWidth) numColumns = thisColumns;
            }
        }

        // Get number of rows, questions on page, column choices, and gap width

        let { numRows, questionsOnPage, columnChoices, gapWidth } = 
            this.calculateMCLayout(numQuestions, numColumns, startIndex, maxRows, choices, defaultChoices, false);

        // Render section

        const meanColumn = (this.pageWidth - this.leftMarginSize - this.rightMarginSize - (numColumns - 1) * gapWidth) / numColumns;

        var gridColumns = numColumns;
        const filledColumns = Math.ceil(questionsOnPage / numRows);
        if (filledColumns < numColumns) gridColumns = filledColumns;
        
        this.pageY -= 0.5 * itemIncrement;

        const xGrid = {
            offset: {
                class: 'classNRMCLeftMargin',
                value: this.leftMarginSize + itemIndent
            },
            indices: [{
                class: 'classAdjacentCircles', 
                increment: incrementAdjacentCircles    
            }]
        };
        if (gridColumns > 1) {
            xGrid.indices.push({
                class: 'classNoClass',
                increment: meanColumn
            });
        }

        const yGrid = {
            offset: {
                class: 'classNoClass',
                value: this.pageHeight - this.pageY
            },
            indices: []
        };
        if (numRows > 1) {
            yGrid.indices.push({
                class: 'classNoClass',
                increment: itemIncrement
            });
        }

        let mcPointData = new BlockModel(xGrid, yGrid);

        for (let i = startIndex; i < startIndex + questionsOnPage; ++i) {
            const columnNum = Math.floor((i - startIndex) / numRows);
            let columnOffset = 0.0;
            for (let j = 0; j < columnNum; ++j) {
                columnOffset += itemIndent + (columnChoices[j] - 1) * (circleSize + circleSpacing) + circleSize / 2.0 + gapWidth;
            }
            const columnX = columnOffset + this.leftMarginSize;
            columnOffset /= meanColumn;

            const workingPoint = { x: columnX, y: this.pageY - ((i - startIndex) % numRows) * itemIncrement };

            const questionLabel = questionNumbers[i] + '.';
            page.drawText(questionLabel, {
                x: workingPoint.x,
                y: workingPoint.y - 0.5 * fontHeight(this.fonts.roman, 12),
                font: this.fonts.roman,
                size: 12,
                color: PDFLib.rgb(0, 0, 0)
            });

            workingPoint.x += itemIndent;

            mcPointData.addChunk(choices[i].length, 1);
            for (let j = 0; j < choices[i].length; ++j) {
                const newIndices = mcPointData.getNewPoint();
                newIndices.xIndices[0] = j;
                if (gridColumns > 1) newIndices.xIndices[1] = columnOffset;
                if (numRows > 1) newIndices.yIndices[0] = (i - startIndex) % numRows;
                mcPointData.addPoint(newIndices);

                await this.drawCircledCharacter(choices[i][j], workingPoint);
                workingPoint.x += circleSize + circleSpacing;
            }
        }

        this.pageY -= (numRows - 0.5) * itemIncrement;

        if (this.updateModels) {
            this.blockModels[this.pointDataPage].push(mcPointData);
        }

        return startIndex + questionsOnPage;
    }
    
    calculateNRLayout(numQuestions, numColumns, startIndex, maxRows, digitCounts, defaultDigits) {
        let questionsOnPage = numQuestions;

        let numRows = Math.ceil(questionsOnPage / numColumns);
        if (numRows > maxRows) {
            questionsOnPage = maxRows * numColumns;
            numRows = maxRows;
        }

        let columnDigits = Array(numColumns).fill(-1);
        let maxOffset = (this.version === 3) ? questionsOnPage : numQuestions;  // For compatibility with old results
        for (let i = startIndex; i < startIndex + maxOffset; ++i) {
            let columnNum = (i - startIndex) % numColumns;
            columnDigits[columnNum] = Math.max(columnDigits[columnNum], digitCounts[i]);
        }
        columnDigits = columnDigits.map(d => d === -1 ? defaultDigits : d);

        let gapWidth = 0.0;
        if (numColumns > 1) {
            let usedWidth = columnDigits.reduce((sum, d) => sum + itemIndent + (d - 1) * (circleSize + circleSpacing) + circleSize / 2.0, 0);
            gapWidth = (this.pageWidth - this.leftMarginSize - this.rightMarginSize - usedWidth) / (numColumns - 1);    
        }

        return { numRows, questionsOnPage, columnDigits, gapWidth };
    }

    async drawNumericalResponseSection(questionNumbers, nrStyle, startIndex, numColumns, digitCounts, defaultDigits) {
        const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

        if (digitCounts.length !== questionNumbers.length) {
            throw new Error("ERROR: Question count mismatch in drawNumericalResponseSection.");
        }
            
        let topY = this.pageY;
        let numQuestions = questionNumbers.length - startIndex;
        if (numQuestions === 0) return startIndex;

        let jMax = (nrStyle === "Fractions") ? 12 : 11;
        const rowIncrement = boxHeight + jMax * (circleSize + circleSpacing) + itemIncrement;
        let maxRows = Math.floor((topY - this.bottomMarginSize + (circleSize + circleSpacing) + itemIncrement) / rowIncrement);

        if (numColumns == -1) {

            // Get automatic number of columns for this page

            let maxColumns = (numQuestions < 5) ? 5 : 10;
            for (let thisColumns = 2; thisColumns <= maxColumns; ++thisColumns) {
                let { gapWidth: thisGapWidth } = 
                    this.calculateNRLayout(numQuestions, thisColumns, startIndex, maxRows, digitCounts, defaultDigits);        
                if (thisGapWidth >= minGapWidth) numColumns = thisColumns;
            }
        }
    
        // Get number of rows, questions on page, column digits, and gap width

        let { numRows, questionsOnPage, columnDigits, gapWidth } = 
            this.calculateNRLayout(numQuestions, numColumns, startIndex, maxRows, digitCounts, defaultDigits);

        // Render section

        const meanColumn = (this.pageWidth - this.leftMarginSize - this.rightMarginSize - (numColumns - 1) * gapWidth) / numColumns;
        
        let gridColumns = numColumns;
        if (questionsOnPage < numColumns) gridColumns = questionsOnPage;
    
        let maxDigits = Math.max(...digitCounts.slice(startIndex, startIndex + questionsOnPage));
    
        this.pageY -= boxHeight / 2.0;
    
        const xGrid = {
            offset: {
                class: 'classNRMCLeftMargin',
                value: this.leftMarginSize + itemIndent
            },
            indices: []
        };
        if (maxDigits > 1) {
            xGrid.indices.push({
                class: 'classAdjacentCircles',
                increment: incrementAdjacentCircles
            });
        }
        if (gridColumns > 1) {
            xGrid.indices.push({
                class: 'classNoClass',
                increment: meanColumn
            });
        }
    
        const yGrid = {
            offset: {
                class: 'classNoClass',
                value: this.pageHeight - this.pageY + boxHeight / 2.0 + circleSpacing + circleSize / 2.0
            },
            indices: [{
                class: 'classAdjacentCircles',
                increment: incrementAdjacentCircles    
            }]
        };
        if (numRows > 1) {
            yGrid.indices.push({
                class: 'classNoClass',
                increment: rowIncrement

            });
        }
    
        let nrPointData = new BlockModel(xGrid, yGrid);

        const decimalString = await localizationInstance.getString("form_decimal");

        for (let i = startIndex; i < startIndex + questionsOnPage; ++i) {
            let columnNum = (i - startIndex) % numColumns;
            let columnOffset = columnDigits.slice(0, columnNum).reduce((sum, d) => sum + itemIndent + (d - 1) * (circleSize + circleSpacing) + circleSize / 2.0 + gapWidth, 0);
            let columnX = columnOffset + this.leftMarginSize;
            columnOffset /= meanColumn;

            const workingPoint = { x: columnX, y: this.pageY - Math.floor((i - startIndex) / numColumns) * rowIncrement };
    
            const questionLabel = questionNumbers[i] + '.';
            page.drawText(questionLabel, {
                x: workingPoint.x,
                y: workingPoint.y - 0.5 * fontHeight(this.fonts.roman, 12),
                font: this.fonts.roman,
                size: 12,
                color: PDFLib.rgb(0, 0, 0)
            });
    
            workingPoint.x += itemIndent;
    
            page.pushOperators(
                PDFLib.pushGraphicsState(),
                PDFLib.translate(workingPoint.x, workingPoint.y)
            );
    
            const numDigits = digitCounts[i];
            for (let k = 0; k < numDigits; ++k) {
                if (k > 0) {
                    page.pushOperators(
                        PDFLib.translate(boxWidth, 0.0)
                    );
                }
    
                page.drawRectangle({
                    x: -boxWidth / 2.0,
                    y: -boxHeight / 2.0,
                    width: boxWidth,
                    height: boxHeight,
                    borderColor: PDFLib.rgb(0, 0, 0),
                    borderWidth: 1.0
                });
            }

            page.pushOperators(PDFLib.popGraphicsState());

            workingPoint.y -= boxHeight / 2.0 + circleSpacing + circleSize / 2.0;
    
            let topY = workingPoint.y;
            for (let k = 0; k < numDigits; ++k) {
                let jMin = 0;
                if (nrStyle === "Negatives") {
                    if (k === numDigits - 1) jMin = 1;
                } else if (nrStyle === "Fractions") {
                    if (k === 0) jMin = 1;
                    else if (k === numDigits - 1) jMin = 2;
                } else {
                    if (k === 0 || k === numDigits - 1) jMin = 1;
                }
    
                workingPoint.y = topY - jMin * (circleSize + circleSpacing);
                nrPointData.addChunk(jMax - jMin, 1);
                for (let j = jMin; j < jMax; ++j) {
                    const newIndices = nrPointData.getNewPoint();
                    let xIndex = 0;
                    if (maxDigits > 1) newIndices.xIndices[xIndex++] = k;
                    if (gridColumns > 1) newIndices.xIndices[xIndex++] = columnOffset;
                    newIndices.yIndices[0] = j;
                    if (numRows > 1) newIndices.yIndices[1] = Math.floor((i - startIndex) / numColumns);
                    nrPointData.addPoint(newIndices);
    
                    let character;
                    if (nrStyle === "Negatives") {
                        if (j === 0 && k === 0) character = '-';
                        else if (j === 0) character = decimalString;
                        else character = String.fromCharCode('0'.charCodeAt(0) + j - 1);
                    } else if (nrStyle === "Fractions") {
                        if (j === 0) character = '/';
                        else if (j === 1 && k === 0) character = '-';
                        else if (j === 1) character = decimalString;
                        else character = String.fromCharCode('0'.charCodeAt(0) + j - 2);
                    } else {
                        if (j === 0) character = decimalString;
                        else character = String.fromCharCode('0'.charCodeAt(0) + j - 1);
                    }

                    await this.drawCircledCharacter(character, workingPoint);
                    workingPoint.y -= circleSize + circleSpacing;
                }
                workingPoint.x += circleSize + circleSpacing;
            }
        }
    
        this.pageY -= numRows * rowIncrement - boxHeight / 2.0 - (circleSize + circleSpacing);
    
        if (this.updateModels) this.blockModels[this.pointDataPage].push(nrPointData);
    
        return startIndex + questionsOnPage;
    }    

    async drawFullWRSection(questionNumbers, startIndex, section) {
        const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);

        let numQuestions = 0;
        let totalHeight = 0.0;
        for (numQuestions = 0; numQuestions < section.getNumQuestions() - startIndex; ++numQuestions) {
            const question = section.questionAtIndex(startIndex + numQuestions);
            let heightInches = question.getDoublePropertyForName("height", 1.0);
            totalHeight += heightInches * pointsPerInch;
            if (this.pageY - totalHeight < this.bottomMarginSize) break;
        }

        if (numQuestions === 0) {
            if (startIndex < questionNumbers.length) numQuestions = 1;
            else return startIndex;
        }

        let hasMultipleCriteria = false;
        let totalCriteria = 0;
        for (let q = startIndex; q < startIndex + numQuestions; ++q) {
            const numCriteria = section.questionAtIndex(q).getCriteria().length;
            totalCriteria += numCriteria;
            if (numCriteria > 1) hasMultipleCriteria = true;
        }

        const xGrid = {
            offset: {
                class: 'classWRRightMarigin',
                value: this.pageWidth - this.rightMarginSize - boxpadding - circleSize / 2.0
            },
            indices: [{
                class: 'classAdjacentCircles',
                increment: incrementAdjacentCircles
            }]
        };

        const yGrid = {
            offset: {
                class: 'classNoClass',
                value: this.pageHeight - this.pageY + boxpadding + circleSize / 2.0 + 0.5 * itemIncrement
            },
            indices: []
        };
        if (hasMultipleCriteria) {
            yGrid.indices.push({
                class: 'classAdjacentCircles',
                increment: incrementAdjacentCircles
            });
        }
        if (numQuestions > 1) {
            yGrid.indices.push({
                class: 'classNoClass',
                increment: pointsPerInch
            });
        }

        const wrPointData = new BlockModel(xGrid, yGrid);

        let questionOffset = 0.0;
        for (let q = startIndex; q < startIndex + numQuestions; ++q) {
            const question = section.questionAtIndex(q);

            const heightInches = question.getDoublePropertyForName("height", 1.0);

            const promptType = question.getStringPropertyForName('promptType', 'None');

            let text = '';
            let imageHash = '';
            let imageHeight = 0.0;
            if (promptType === "Text") {
                text = question.getStringPropertyForName('promptText');
            } else if (promptType === "Image") {
                imageHash = question.getStringPropertyForName('promptImage');
                imageHeight = question.getDoublePropertyForName("promptHeight", 0.0) * pointsPerInch;
            }

            this.pageY -= 0.5 * itemIncrement;

            const questionLabel = questionNumbers[q] + '.';
            page.drawText(questionLabel, {
                x: this.leftMarginSize,
                y: this.pageY - fontHeight(this.fonts.roman, 12),
                font: this.fonts.roman,
                size: 12,
                color: PDFLib.rgb(0, 0, 0)
            });

            let maxValue = Math.max(...question.getCriteria().map(c => c.maxScore));

            let criteriaBoxWidth = 0.0;
            question.getCriteria().forEach(criterion => {
                const bubbleRight = this.pageWidth - this.rightMarginSize - boxpadding - 0.5 * circleSize;
                const textRight = bubbleRight - (maxValue + 2) * (circleSize + circleSpacing) - 2 * boxpadding - 0.5 * circleSize;

                const thisName = criterion.name;

                const textSize = this.fonts.roman.widthOfTextAtSize(thisName, 12);
                const textWidth = textSize + 20.0;
                const thisWidth = this.pageWidth - this.rightMarginSize - (textRight - textWidth - boxpadding);
                if (thisWidth > criteriaBoxWidth) criteriaBoxWidth = thisWidth;
            });

            const criteriaBoxHeight = question.getCriteria().length * circleSize + (question.getCriteria().length - 1) * circleSpacing + 2.0 * boxpadding;

            page.pushOperators(
                PDFLib.pushGraphicsState()
            );
/*
            page.drawRectangle({
                x: this.leftMarginSize + itemIndent,
                y: this.pageY,
                width: this.pageWidth - this.rightMarginSize - this.leftMarginSize - itemIndent,
                height: -heightInches * pointsPerInch
            });
            page.clip();
            page.endPath();
*/
            if (promptType === "Text") {

                const textDecoded = await htmlToUTF8(text);
                let textTop = this.pageY - fontHeight(this.fonts.roman, 12);

                const wrappedLines = [];

                const lines = textDecoded.split('\n');
                let lineTop = textTop;
                for (const line of lines) {
                    const words = line.split(' ');
                    let currentLine = '';
              
                    for (const word of words) {
                        let availableSpace = this.pageWidth - this.leftMarginSize - itemIndent - this.rightMarginSize;
                        if (lineTop > this.pageY - criteriaBoxHeight - boxmargin) availableSpace -= criteriaBoxWidth + boxmargin;
                        if (availableSpace <= 0.0) break;
        
                        const testLine = currentLine + (currentLine.length == 0 ? '' : ' ') + word;
                        const testLineWidth = this.fonts.roman.widthOfTextAtSize(testLine, 12);
                        if (testLineWidth > availableSpace) {
                            wrappedLines.push(currentLine);
                            currentLine = word;
                            lineTop -= 15;
                        } else {
                            currentLine = testLine;
                        }
                    }
                    wrappedLines.push(currentLine);              
                    lineTop -= 15;
                }

                wrappedLines.forEach(async wrappedLine => {
                    page.drawText(wrappedLine, {
                        x: this.leftMarginSize + itemIndent,
                        y: textTop,
                        font: this.fonts.roman,
                        size: 12,
                        color: PDFLib.rgb(0, 0, 0),
                    });
                    textTop -= 15;
                });

            } else if (promptType === "Image") {

                const image = await this.embedImage(imageHash);
                const imageWidth = imageHeight * image.width / image.height;

                page.drawImage(image, {
                    x: this.leftMarginSize + itemIndent,
                    y: this.pageY - imageHeight,
                    width: imageWidth,
                    height: imageHeight,
                });

            }

            page.pushOperators(PDFLib.popGraphicsState());

            page.drawRectangle({
                x: this.pageWidth - this.rightMarginSize - criteriaBoxWidth - boxmargin,
                y: this.pageY + boxmargin,
                width: criteriaBoxWidth + 2.0 * boxmargin,
                height: -criteriaBoxHeight - 2.0 * boxmargin,
				color: PDFLib.rgb(1.0, 1.0, 1.0)
            });

            const teacherOnlyString = await localizationInstance.getString("form_teacher_only");

            page.drawText(teacherOnlyString, {
                x: this.pageWidth - this.rightMarginSize - this.fonts.roman.widthOfTextAtSize(teacherOnlyString, 8),
                y: this.pageY + 4,
                font: this.fonts.roman,
                size: 8,
                color: PDFLib.rgb(0, 0, 0)
            });

            const criteria = question.getCriteria();
            for (let i = 0; i < criteria.length; ++i) {
                const pointY = this.pageY - 0.5 * circleSize - boxpadding - i * (circleSize + circleSpacing);
                const bubbleRight = this.pageWidth - this.rightMarginSize - boxpadding - 0.5 * circleSize;

                wrPointData.addChunk(criteria[i].maxScore + 2, 2);
                for (let j = 0; j <= criteria[i].maxScore; ++j) {
                    const newIndices = wrPointData.getNewPoint();
                    newIndices.xIndices[0] = j - maxValue - 2;
                    if (hasMultipleCriteria && numQuestions > 1) {
                        newIndices.yIndices[0] = i;
                        newIndices.yIndices[1] = questionOffset;
                    } else if (hasMultipleCriteria) newIndices.yIndices[0] = i;
                    else if (numQuestions > 1) newIndices.yIndices[0] = questionOffset;
                    wrPointData.addPoint(newIndices);

                    const pointX = bubbleRight - (maxValue + 2 - j) * (circleSize + circleSpacing);
                    await this.drawCircledCharacter(String.fromCharCode('0'.charCodeAt(0) + j), { x: pointX, y: pointY });
                }

                const halfIndices = wrPointData.getNewPoint();
                halfIndices.xIndices[0] = 0;
                if (hasMultipleCriteria && numQuestions > 1) {
                    halfIndices.yIndices[0] = i;
                    halfIndices.yIndices[1] = questionOffset;
                } else if (hasMultipleCriteria) halfIndices.yIndices[0] = i;
                else if (numQuestions > 1) halfIndices.yIndices[0] = questionOffset;
                wrPointData.addPoint(halfIndices);

                await this.drawCircledCharacter('+', { x: bubbleRight, y: pointY });

                const textRight = bubbleRight - (maxValue + 2) * (circleSize + circleSpacing) - 2 * boxpadding - 0.5 * circleSize;
                const nameDecoded = await htmlToUTF8(criteria[i].name);
                
                page.drawText(nameDecoded, {
                    x: textRight - this.fonts.roman.widthOfTextAtSize(nameDecoded, 12),
                    y: pointY - 0.5 * fontHeight(this.fonts.roman, 12),
                    font: this.fonts.roman,
                    size: 12,
                    color: PDFLib.rgb(0, 0, 0)
                });
            }

            page.drawRectangle({
                x: this.pageWidth - this.rightMarginSize - criteriaBoxWidth,
                y: this.pageY,
                width: criteriaBoxWidth,
                height: -criteriaBoxHeight,
                borderColor: PDFLib.rgb(0, 0, 0),
                borderWidth: 1.0
            });

            this.pageY -= heightInches * pointsPerInch - 0.5 * itemIncrement;
            questionOffset += heightInches;
        }

        if (this.updateModels) this.blockModels[this.pointDataPage].push(wrPointData);

        return startIndex + numQuestions;
    }

    async drawCompressedWRSection(questionNumbers, startIndex, numColumns, criteria) {
        if (questionNumbers.length !== criteria.length) {
            throw new Error("ERROR: Question count mismatch in drawCompressedWRSection.");
        }

        const page = this.pdfDoc.getPage(this.pdfDoc.getPageCount() - 1);
        const topY = this.pageY;

        let numQuestions;
        let maxCriteria, maxScore, totalBubbleRows;
        let numRows, gridColumns;
        for (numQuestions = questionNumbers.length - startIndex; numQuestions > 0; numQuestions--) {
            maxCriteria = 0;
            maxScore = 0;
            totalBubbleRows = 0;

            for (let i = startIndex; i < startIndex + numQuestions; ++i) {
                totalBubbleRows += criteria[i].length;

                if (criteria[i].length > maxCriteria) maxCriteria = criteria[i].length;
                criteria[i].forEach(c => {
                    if (c.maxScore > maxScore) maxScore = c.maxScore;
                });
            }

            numRows = Math.ceil(totalBubbleRows / numColumns);
            if (numRows < maxCriteria) numRows = maxCriteria;
            let columnNum;
            do {
                let rowNum = 0;
                columnNum = 0;
                for (let i = startIndex; i < startIndex + numQuestions; ++i) {
                    if (rowNum + criteria[i].length > numRows) {
                        rowNum = 0;
                        columnNum++;
                    }
                    rowNum += criteria[i].length + 1;
                }

                if (columnNum >= numColumns) numRows++;
            } while (columnNum >= numColumns);
            gridColumns = columnNum + 1;

            if (numRows * (circleSize + circleSpacing) + itemIncrement <= topY - this.bottomMarginSize) break;
        }

        if (numQuestions === 0) {
            if (startIndex < questionNumbers.length) numQuestions = 1;
            else return startIndex;
        }

        const columnIncrement = (this.pageWidth - this.leftMarginSize - this.rightMarginSize - (numColumns - 1) * wrColumnSpacing) / numColumns + wrColumnSpacing;

        let teacherOnlyString = await localizationInstance.getString("form_teacher_only");
        teacherOnlyString = '(' + teacherOnlyString + ')';

        page.drawText(teacherOnlyString, {
            x: this.pageWidth - this.rightMarginSize - this.fonts.roman.widthOfTextAtSize(teacherOnlyString, 10),
            y: this.pageY + headerGap,
            font: this.fonts.roman,
            size: 10,
            color: PDFLib.rgb(0, 0, 0)
        });

        this.pageY -= 0.5 * itemIncrement;

        const xGrid = {
            offset: {
                class: 'classNoClass',
                value: this.leftMarginSize + columnIncrement - wrColumnSpacing - boxpadding - circleSize / 2.0 - 
                    (maxScore + 2) * (circleSize + circleSpacing)
            },
            indices: [{
                class: 'classAdjacentCircles',
                increment: incrementAdjacentCircles
            }]
        };
        if (gridColumns > 1) {
            xGrid.indices.push({
                class: 'classNoClass',
                increment: columnIncrement
            });
        }

        const yGrid = {
            offset: {
                class: 'classNoClass',
                value: this.pageHeight - this.pageY + boxpadding + circleSize / 2.0
            },
            indices: []
        };
        if (numRows > 1) {
            yGrid.indices.push({
                class: 'classAdjacentCircles',
                increment: incrementAdjacentCircles
            });
        }

        const wrPointData = new BlockModel(xGrid, yGrid);

        let rowNum = 0;
        let columnNum = 0;
        for (let i = startIndex; i < startIndex + numQuestions; ++i) {
            if (rowNum + criteria[i].length > numRows) {
                rowNum = 0;
                columnNum++;
            }

            const workingPoint = {
                x: this.leftMarginSize + columnNum * columnIncrement,
                y: this.pageY - rowNum * (circleSize + circleSpacing)
            };

            const questionLabel = questionNumbers[i] + '.';
            page.drawText(questionLabel, {
                x: workingPoint.x,
                y: workingPoint.y - circleSize / 2.0 - boxpadding - 0.5 * fontHeight(this.fonts.roman, 12),
                font: this.fonts.roman,
                size: 12,
                color: PDFLib.rgb(0, 0, 0)
            });

            const numCriteria = criteria[i].length;
            for (let j = 0; j < numCriteria; ++j) {
                const pointY = workingPoint.y - 0.5 * circleSize - boxpadding - j * (circleSize + circleSpacing);
                const rightX = workingPoint.x + columnIncrement - wrColumnSpacing - boxpadding - 0.5 * circleSize;

                wrPointData.addChunk(criteria[i][j].maxScore + 2, 2);
                for (let k = 0; k <= criteria[i][j].maxScore; ++k) {
                    const newIndices = wrPointData.getNewPoint();
                    newIndices.xIndices[0] = k;
                    if (gridColumns > 1) newIndices.xIndices[1] = columnNum;
                    if (numRows > 1) newIndices.yIndices[0] = rowNum + j;
                    wrPointData.addPoint(newIndices);

                    const pointX = rightX + (k - maxScore - 2) * (circleSize + circleSpacing);
                    await this.drawCircledCharacter(String.fromCharCode('0'.charCodeAt(0) + k), { x: pointX, y: pointY });
                }

                const halfIndices = wrPointData.getNewPoint();
                halfIndices.xIndices[0] = maxScore + 2;
                if (gridColumns > 1) halfIndices.xIndices[1] = columnNum;
                if (numRows > 1) halfIndices.yIndices[0] = rowNum + j;
                wrPointData.addPoint(halfIndices);

                await this.drawCircledCharacter('+', { x: rightX, y: pointY });

                const textRight = rightX - (maxScore + 2) * (circleSize + circleSpacing) - 2 * boxpadding - 0.5 * circleSize;
                const nameDecoded = await htmlToUTF8(criteria[i][j].name);

                page.drawText(nameDecoded, {
                    x: textRight - this.fonts.roman.widthOfTextAtSize(nameDecoded, 12),
                    y: pointY - 0.5 * fontHeight(this.fonts.roman, 12),
                    font: this.fonts.roman,
                    size: 12,
                    color: PDFLib.rgb(0, 0, 0)
                });
            }

            page.drawRectangle({
                x: workingPoint.x + columnIncrement - wrColumnSpacing,
                y: workingPoint.y,
                width: -columnIncrement + wrColumnSpacing - boxpadding + itemIndent,
                height: -(criteria[i].length * circleSize + (criteria[i].length - 1) * circleSpacing + 2.0 * boxpadding),
				borderColor: PDFLib.rgb(0, 0, 0),
				borderWidth: 1.0
            });

            rowNum += criteria[i].length + 1;
        }

        this.pageY -= numRows * (circleSize + circleSpacing) + 0.5 * itemIncrement;

        if (this.updateModels) this.blockModels[this.pointDataPage].push(wrPointData);

        return startIndex + numQuestions;
    }
}

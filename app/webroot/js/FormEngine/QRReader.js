/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import {
    LuminanceSource,
    GlobalHistogramBinarizer,
    BinaryBitmap,
    DecodeHintType,
    QRCodeReader,
} from '/js/lib/zxing/zxing-library.mjs';

import { CVImage } from './CVImage.js?7.0';

// Helper Classes and Functions

class BufferBitmapSource extends LuminanceSource {
    constructor(width, height, buffer) {
        super(width, height);
        this.buffer = buffer;
    }
  
    getRow(y, row) {
        if (!row || row.length !== this.width) {
            row = new Uint8ClampedArray(this.width);
        }
        const start = y * this.width;
        const end = start + this.width;
        row.set(this.buffer.slice(start, end));
        return row;
    }
  
    getMatrix() {
        return this.buffer;
    }
}

class Point2D {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    static subtract(p1, p2) {
        return new Point2D(p1.x - p2.x, p1.y - p2.y);
    }

    static add(p1, p2) {
        return new Point2D(p1.x + p2.x, p1.y + p2.y);
    }

    static scale(p, scalar) {
        return new Point2D(p.x * scalar, p.y * scalar);
    }

    static length(p) {
        return Math.sqrt(p.x * p.x + p.y * p.y);
    }
}

class FinderPattern {
    constructor(center = new Point2D(), spans = [0, 0, 0, 0], totalError = 0) {
        this.center = center;
        this.spans = spans;
        this.totalError = totalError;
    }
}

// Constants and Parameters

const QR_OVERSAMPLING = 3;
const QR_GRID_MIN_SIZE = 17; // Smallest possible QR grid
const QR_MAX_SIZE = 500; // Maximum size for QR code in pixels
const QR_MIN_SIZE = 20; // Minimum size for QR code in pixels (x < 40)

const NUM_SLICES = 10;
const SLICE_WIDTH = 0.5;

const QR_GRID_MINIMUM = 2.0e-4; // 1.0e-4 < x < 3.1e-4
const QR_GRID_SIGMA = 1.4; // 1.1 < x < 2.1
const QR_GAP_BIAS = 0.1; // 0.0 < x < 0.2 *** MAYBE NOT NEEDED LATER ***

const FINDER_MIN_DISTANCE = 5.0;
const FINDER_MAX_ERROR = 0.10; // 0.05 < x < ???
const QR_HYSTERESIS = 0.28; // 0.26 < x < 0.30

const SPAN_VALUES = [1.0/7, 1.0/7, 3.0/7, 1.0/7, 1.0/7];

const FINDER_SIZE = 7;
const FINDER_CROSSINGS = 6;

class QRReader {
    constructor() {
        // Initialize any required properties if needed
    }

    getInterpolated(image, a, x, y) {
        const imageWidth = image.width;
        const imageHeight = image.height;
        const imageData = image.data;

        let sum = 0.0;
        let totalWeight = 0.0;

        for (let p = Math.ceil(x - a); p <= Math.floor(x + a); p++) {
            if (p < 0 || p >= imageWidth) continue;
            const deltaX = Math.PI * (x - p);
            const sincX = (deltaX === 0.0) ? 1.0 : (Math.sin(deltaX) / deltaX) * (Math.sin(deltaX / a) / (deltaX / a));

            for (let q = Math.ceil(y - a); q <= Math.floor(y + a); q++) {
                if (q < 0 || q >= imageHeight) continue;
                const deltaY = Math.PI * (y - q);
                const sincY = (deltaY === 0.0) ? 1.0 : (Math.sin(deltaY) / deltaY) * (Math.sin(deltaY / a) / (deltaY / a));

                const weight = sincX * sincY;
                const pixelValue = imageData[q * imageWidth + p];
                sum += weight * pixelValue;
                totalWeight += weight;
            }
        }

        return sum / totalWeight;
    }

    checkDirection(image, x0, y0, deltaX, deltaY) {
        let meanError = FINDER_MAX_ERROR;

        const deltaLength = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

        // Check that this looks like a finder in the direction given by (deltaX, deltaY)

        const imageWidth = image.width;
        const imageHeight = image.height;
        const imageData = image.data;

        if (x0 < 0 || x0 >= imageWidth) return { isGood: false };
        if (y0 < 0 || y0 >= imageHeight) return { isGood: false };

        // Initialize extremes

        const extremeOffsets = new Array(FINDER_SIZE).fill(0);
        const extremeValues = new Array(FINDER_SIZE).fill(false);

        // Search in both directions

        for (let dir = -1; dir <= 1; dir += 2) {
            let x = x0;
            let y = y0;

            let value = imageData[y * imageWidth + x];

            let state = 'StateDark'; // Possible states: 'StateDark', 'StateLight'

            let offset = 0;
            let index = 3;

            if (extremeValues[index] === false) {
                extremeValues[index] = value;
                extremeOffsets[index] = offset;
            }

            while ((x >= 0) && (x < imageWidth) && (y >= 0) && (y < imageHeight)) {
                value = imageData[y * imageWidth + x];

                if (state === 'StateDark') {

                    if (value < extremeValues[index]) {
                        extremeOffsets[index] = offset;
                        extremeValues[index] = value;
                    } else if (value - extremeValues[index] > QR_HYSTERESIS) {
                        index += dir;
                        extremeOffsets[index] = offset;
                        extremeValues[index] = value;
                        state = 'StateLight';
                    }

                } else {
                        
                    if (value > extremeValues[index]) {
                        extremeOffsets[index] = offset;
                        extremeValues[index] = value;
                    } else if ((index === 0) || (index === FINDER_SIZE - 1)) {
                        break;
                    } else if (extremeValues[index] - value > QR_HYSTERESIS) {
                        index += dir;
                        extremeOffsets[index] = offset;
                        extremeValues[index] = value;
                        state = 'StateDark';
                    }
                }

                x += dir * deltaX;
                y += dir * deltaY;
                offset += dir;
            }
        }

        // Check extremes and find range for value threshold

        let maxLow = -Infinity;
        let minHigh = Infinity;

        for (let i = 0; i < FINDER_SIZE; i++) {
            if (extremeValues[i] === false) return { isGood: false };
            if (i % 2 === 0) {
                if (extremeValues[i] < minHigh) minHigh = extremeValues[i];
            } else {
                if (extremeValues[i] > maxLow) maxLow = extremeValues[i];
            }
        }

        const window = new Array(FINDER_CROSSINGS).fill(false);

        let foundSlice = false;
        const sliceOffset = 0.5 - SLICE_WIDTH / 2.0;

        for (let sliceIndex = 0; sliceIndex <= NUM_SLICES; sliceIndex++) {
            const transitionValue = maxLow + (minHigh - maxLow) * (sliceOffset + SLICE_WIDTH * sliceIndex / NUM_SLICES);

            // Build window

            const testWindow = new Array(FINDER_CROSSINGS).fill(false);
            for (let index = 0; index < FINDER_SIZE - 1; index++) {
                let value, lastValue;
                for (let offset = extremeOffsets[index]; offset <= extremeOffsets[index + 1]; offset++) {
                    const x = Math.round(x0 + offset * deltaX);
                    const y = Math.round(y0 + offset * deltaY);

                    lastValue = value;
                    value = imageData[y * image.width + x];

                    let isTransition = false;
                    if ((index % 2 === 1) && (value > transitionValue)) isTransition = true;
                    if ((index % 2 === 0) && (value < transitionValue)) isTransition = true;

                    if (isTransition) {
                        testWindow[index] = deltaLength * (offset - (value - transitionValue) / (value - lastValue));
                        break;
                    }
                }
            }

            // Check if window is good

            let goodWindow = true;
            for (let j = 0; j < FINDER_CROSSINGS; j++) {
                if (testWindow[j] === false) goodWindow = false;
            }
            if (!goodWindow) continue;
            
            let errorSum = 0.0;
            const ySpan = testWindow[FINDER_CROSSINGS - 1] - testWindow[0];
            for (let j = 0; j < FINDER_CROSSINGS - 1; j++) {
                const thisSpan = (testWindow[j + 1] - testWindow[j]) / ySpan;
                const error = Math.pow(Math.log(thisSpan / SPAN_VALUES[j]), 2.0);
                errorSum += error;
            }

            const thisError = errorSum / (FINDER_CROSSINGS - 1);
            if (thisError < meanError) {
                for (let j = 0; j < FINDER_CROSSINGS; j++) window[j] = testWindow[j];
                meanError = thisError;
                foundSlice = true;
            }
        }

        if (foundSlice) {
            return {
                isGood: true,
                centerOffset: (window[0] + window[FINDER_CROSSINGS - 1]) / 2.0,
                span: window[FINDER_CROSSINGS - 1] - window[0],
                error: meanError
            }
        } else {
            return { 
                isGood: false 
            };            
        }
    }

    readQRCode(image) {
        let qrText = '';

        const imageWidth = image.width;
        const imageHeight = image.height;
        const imageData = image.data;

        let startOffset, extremeValue;

        // Find the QR code finders by searching horizontally first, then confirming vertically.

        const finders = [];

        for (let y = 0; y < imageHeight; y += 2) {
            let state = 'StateInit'; // Possible states: 'StateInit', 'StateDark', 'StateLight'

            for (let x = 0; x < imageWidth; x++) {
                const value = imageData[y * imageWidth + x];

                if (state === 'StateInit') {

                    // Check for our first white point

                    extremeValue = value;
                    state = 'StateLight';

                } else if (state === 'StateDark') {

                    if (value < extremeValue) {

                        extremeValue = value;

                    } else if (value - extremeValue > QR_HYSTERESIS) {

                        let centerX;
                        let intCenterX;

                        let isGood = true;
                        let totalError = 0.0;
                        let spans = [];

                        intCenterX = Math.round((startOffset + x) / 2);
                        let response = this.checkDirection(image, intCenterX, y, 1, 0);
                        if (response.isGood) {
                            spans.push(response.span);
                            totalError += response.error;
                        } else isGood = false;

                        if (isGood) {
                            centerX = intCenterX + response.centerOffset;
                            intCenterX = Math.round(centerX);

                            response = this.checkDirection(image, intCenterX, y, 0, 1);
                            if (response.isGood) {
                                spans.push(response.span);
                                totalError += response.error;
                            } else isGood = false;
                        }

                        let centerY, intCenterY;

                        if (isGood) {
                            centerY = y + response.centerOffset;
                            intCenterY = Math.round(centerY);

                            response = this.checkDirection(image, intCenterX, intCenterY, -1, 1);
                            if (response.isGood) {
                                spans.push(response.span);
                                totalError += response.error;
                            } else isGood = false;
                        }

                        if (isGood) {
                            response = this.checkDirection(image, intCenterX, intCenterY, 1, 1);
                            if (response.isGood) {
                                spans.push(response.span);
                                totalError += response.error;
                            } else isGood = false;
                        }

                        if (isGood) {

                            // Save this finder if it's new or better than one it overlaps
                                
                            const thisCenter = new Point2D(centerX, centerY);
                            const thisFinder = new FinderPattern(thisCenter, spans, totalError);

                            let otherIndex = -1;
                            let otherError = 0;
                            for (let i = 0; i < finders.length; i++) {
                                const distance = Point2D.length(Point2D.subtract(finders[i].center, thisCenter));
                                if (distance < FINDER_MIN_DISTANCE) {
                                    otherIndex = i;
                                    otherError = finders[i].totalError;
                                    break;
                                }
                            }

                            if (otherIndex === -1) {
                                finders.push(thisFinder);
                                if (finders.length > 3) {
                                    finders.sort((a, b) => a.totalError - b.totalError);
                                    finders.pop();
                                }
                            } else if (totalError < otherError) {
                                finders[otherIndex] = thisFinder;
                            }
                        }

                        // Advance state machine

                        startOffset = x;
                        extremeValue = value;
                        state = 'StateLight';
                    }

                } else {

                    if (value > extremeValue) {

                        extremeValue = value;

                    } else if (extremeValue - value > QR_HYSTERESIS) {

                        // Advance state machine

                        startOffset = x;
                        extremeValue = value;
                        state = 'StateDark';
                    }

                }
            }
        }

        let topLeft, topRight, bottomLeft;
        if (finders.length === 3) {
            
            // Find top left finder, which is closest to the others

            const abDistance = Point2D.length(Point2D.subtract(finders[0].center, finders[1].center));
            const acDistance = Point2D.length(Point2D.subtract(finders[0].center, finders[2].center));
            const bcDistance = Point2D.length(Point2D.subtract(finders[1].center, finders[2].center));

            if (bcDistance >= abDistance && bcDistance >= acDistance) {
                topLeft = finders[0].center;
                topRight = finders[1].center;
                bottomLeft = finders[2].center;
            } else if (acDistance >= bcDistance && acDistance >= abDistance) {
                topLeft = finders[1].center;
                topRight = finders[0].center;
                bottomLeft = finders[2].center;
            } else {
                topLeft = finders[2].center;
                topRight = finders[0].center;
                bottomLeft = finders[1].center;
            }

            // Sort top right and bottom left finders using the cross product

            if ((bottomLeft.y - topLeft.y) * (topRight.x - topLeft.x) < (bottomLeft.x - topLeft.x) * (topRight.y - topLeft.y)) {
                [topRight, bottomLeft] = [bottomLeft, topRight];
            }

            // Get vector for the top and left edges of the QR code

            var topEdge = Point2D.subtract(topRight, topLeft);
            var leftEdge = Point2D.subtract(bottomLeft, topLeft);

            // Get basis vectors and scale them so that they go from module space to pixel space

            let qrBasis = [topEdge, leftEdge];

            let lengthAverage = 0.0;
            for (let i = 0; i < 3; i++) {
                lengthAverage += Math.abs(finders[i].spans[1] / (qrBasis[1].y - (qrBasis[0].y * qrBasis[1].x) / qrBasis[0].x));
                lengthAverage += Math.abs(finders[i].spans[0] / (qrBasis[0].x - (qrBasis[1].x * qrBasis[0].y) / qrBasis[1].y));
            }
            lengthAverage /= 6;

            // Get basis vectors normalized to one module length

            qrBasis[0] = Point2D.scale(qrBasis[0], lengthAverage / 7);
            qrBasis[1] = Point2D.scale(qrBasis[1], lengthAverage / 7);

            // Move top left corner to compensate for finder width

            topLeft = Point2D.subtract(topLeft, Point2D.scale(qrBasis[0], 4));
            topLeft = Point2D.subtract(topLeft, Point2D.scale(qrBasis[1], 4));

            // Get the maximum edge length in pixels

            topEdge = Point2D.add(topEdge, Point2D.scale(qrBasis[0], 8));
            const topLength = Math.ceil(Point2D.length(topEdge));

            leftEdge = Point2D.add(leftEdge, Point2D.scale(qrBasis[1], 8));
            const leftLength = Math.ceil(Point2D.length(leftEdge));

            if (Point2D.length(topEdge) > QR_MAX_SIZE) return false;
            if (Point2D.length(leftEdge) > QR_MAX_SIZE) return false;
            if (Point2D.length(topEdge) < QR_MIN_SIZE) return false;
            if (Point2D.length(leftEdge) < QR_MIN_SIZE) return false;

            const maxEdgeLength = Math.max(topLength, leftLength);

            // Get an image of just the QR code, transformed to rectilinear coordinates

            const a = 3;
            const oversampling = QR_OVERSAMPLING;
            const patchSize = oversampling * maxEdgeLength;

            const qrImage = new CVImage(patchSize, patchSize);
            const qrData = qrImage.data;

            let patchIndex = 0;
            for (let j = 0; j < patchSize; j++) {
                for (let i = 0; i < patchSize; i++) {
                    const offset = Point2D.add(
                        topLeft,
                        Point2D.add(
                            Point2D.scale(topEdge, i / (patchSize - 1)),
                            Point2D.scale(leftEdge, j / (patchSize - 1))
                        )
                    );
                    qrData[patchIndex++] = this.getInterpolated(image, a, offset.x, offset.y);
                }
            }

            // Get the position of each row and column in the module grid
            
            const kernel = [
                0.0, -1.0, 0.0,
                -1.0, 4.0, -1.0,
                0.0, -1.0, 0.0,
            ];
            const kernelR = 1;

            const transformed = new CVImage(patchSize, patchSize);
            const transformedData = transformed.data;

            transformed.fillWith(0.0);
            for (let patchJ = kernelR; patchJ < patchSize - kernelR; patchJ++) {
                for (let patchI = kernelR; patchI < patchSize - kernelR; patchI++) {
                    let sum = 0.0;
                    for (let kernelJ = -kernelR; kernelJ <= kernelR; kernelJ++) {
                        for (let kernelI = -kernelR; kernelI <= kernelR; kernelI++) {
                            sum += kernel[(kernelJ + kernelR) * 3 + (kernelI + kernelR)] *
                                qrData[(patchJ + kernelJ) * patchSize + (patchI + kernelI)];
                        }
                    }
                    transformedData[patchJ * patchSize + patchI] = sum;
                }
            }

            transformed.applyGaussian(QR_GRID_SIGMA);

            const values = [new Array(patchSize).fill(0.0), new Array(patchSize).fill(0.0)];

            for (let j = 0; j < patchSize; j++) {
                for (let i = 0; i < patchSize; i++) {
                    const thisValue = Math.pow(transformedData[j * patchSize + i], 2.0);
                    values[0][i] += thisValue;
                    values[1][j] += thisValue;
                }
            }

            for (let i = 0; i < 2; i++) {
                for (let j = 0; j < patchSize; j++) {
                    values[i][j] /= patchSize;
                }
            }

            const gridOffsets = [[], []];
            for (let i = 0; i < 2; i++) {

                // Pick out peaks that pass a threshold size

                const rawOffsets = [];
                for (let j = kernelR + 1; j < patchSize - (kernelR + 1); j++) {
                    if ((values[i][j] > values[i][j - 1]) && (values[i][j] > values[i][j + 1]) && (values[i][j] > QR_GRID_MINIMUM)) {
                        rawOffsets.push(j);
                    }
                }

                if (rawOffsets.length > QR_GRID_MIN_SIZE) {

                    // Fill in gaps as needed
                        
                    const gaps = [];
                    for (let j = 1; j < rawOffsets.length; j++) {
                        gaps.push(rawOffsets[j] - rawOffsets[j - 1]);
                    }
                    gaps.sort((a, b) => a - b);
                    const medianGap = gaps[Math.floor(gaps.length / 2)];

                    gridOffsets[i].push(rawOffsets[0]);
                    for (let j = 1; j < rawOffsets.length; j++) {
                        const thisGap = rawOffsets[j] - rawOffsets[j - 1];
                        const gapSize = Math.round(thisGap / medianGap - QR_GAP_BIAS);
                        for (let k = 1; k <= gapSize; k++) {
                            gridOffsets[i].push(rawOffsets[j - 1] + (thisGap * k) / gapSize);
                        }
                    }

                    // Add grid lines just outside of the QR code

                    const leadingOffset = 2.0 * gridOffsets[i][0] - gridOffsets[i][1];
                    gridOffsets[i].unshift(leadingOffset < 0.0 ? 0.0 : leadingOffset);

                    const trailingOffset = 2.0 * gridOffsets[i][gridOffsets[i].length - 1] - gridOffsets[i][gridOffsets[i].length - 2];
                    gridOffsets[i].push(trailingOffset > patchSize - 1 ? patchSize - 1 : trailingOffset);
                }
            }

            if (gridOffsets[0].length === gridOffsets[1].length && gridOffsets[0].length > QR_GRID_MIN_SIZE) {

                // Build raw pattern from grid points

                const gridSize = gridOffsets[0].length;
                const patternRaw = new CVImage(gridSize, gridSize);
                const patternRawData = patternRaw.data;

                for (let j = 0; j < gridSize; j++) {
                    for (let i = 0; i < gridSize; i++) {
                        const x = Math.round(gridOffsets[0][i]);
                        const y = Math.round(gridOffsets[1][j]);
                        patternRawData[j * gridSize + i] = qrData[y * patchSize + x];
                    }
                }

                patternRaw.setValueRange(0.0, 1.0);

                // Fit filter coefficients using the finders

                const offsets = [-1, +1, -gridSize, +gridSize];
                const patternSize = gridSize - 2;

                let ccSum = 0.0, ooSum = 0.0, coSum = 0.0, ceSum = 0.0, oeSum = 0.0;

                for (let n = 0; n < 3; n++) {
                    const cornerI = (n % 2) * (patternSize - FINDER_SIZE) + (FINDER_SIZE - 1) / 2;
                    const cornerJ = Math.floor(n / 2) * (patternSize - FINDER_SIZE) + (FINDER_SIZE - 1) / 2;
                    for (let j = -3; j <= 3; j++) {
                        for (let i = -3; i <= 3; i++) {
                            const imageI = cornerI + i + 1;
                            const imageJ = cornerJ + j + 1;

                            const centerTerm = patternRawData[imageJ * gridSize + imageI];

                            let offsetTerm = 0.0;
                            for (let q = 0; q < 4; q++) {
                                offsetTerm += patternRawData[imageJ * gridSize + imageI + offsets[q]];
                            }

                            let expectedValue;
                            const k = Math.max(Math.abs(i), Math.abs(j));
                            if (k === 0) expectedValue = 0.0;
                            else expectedValue = 1 - (k % 2);

                            ccSum += centerTerm * centerTerm;
                            ooSum += offsetTerm * offsetTerm;
                            coSum += centerTerm * offsetTerm;
                            ceSum += centerTerm * expectedValue;
                            oeSum += offsetTerm * expectedValue;
                        }
                    }
                }

                const centerWeight = (ceSum * ooSum - oeSum * coSum) / (ccSum * ooSum - coSum * coSum);
                const offsetWeight = (ceSum * coSum - oeSum * ccSum) / (coSum * coSum - ooSum * ccSum);
    
                // Apply filter

                const patternInferred = new CVImage(patternSize, patternSize);
                const patternInferredData = patternInferred.data;

                let tIdx = 0;
                for (let j = 0; j < patternSize; j++) {
                    for (let i = 0; i < patternSize; i++) {
                        const s = patternRawData[(j + 1) * gridSize + (i + 1)];
                        let sum = centerWeight * s;
                        for (let q = 0; q < 4; q++) {
                            sum += offsetWeight * patternRawData[(j + 1) * gridSize + (i + 1) + offsets[q]];
                        }
                        patternInferredData[tIdx++] = sum;
                    }
                }
                patternInferred.thresholdAtValue(0.5);

                // Read QR text with ZXing
                
                const dataSize = patternSize * patternSize;
                const zxingBuffer = new Uint8ClampedArray(dataSize);

                for (let i = 0; i < dataSize; i++) {
                    zxingBuffer[i] = patternInferredData[i] === 1.0 ? 255 : 0;
                }

                try {

                    const source = new BufferBitmapSource(
                        patternSize,
                        patternSize,
                        zxingBuffer
                    );

                    const binarizer = new GlobalHistogramBinarizer(source);
                    const binaryBitmap = new BinaryBitmap(binarizer);

                    const hints = new Map();
                    hints.set(DecodeHintType.TRY_HARDER, true);

                    const reader = new QRCodeReader();
                    const result = reader.decode(binaryBitmap, hints);

                    qrText = result.getText();

                } catch (e) {

                    return false;

                }
            }

        }

        if (qrText.length > 0) {

            return {
                data: qrText,
                location: {
                    topLeftCorner: topLeft,
                    topRightCorner: topRight,
                    bottomLeftCorner: bottomLeft
                }
            };
            
        } else return false;
    }
}

export default QRReader;

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { DFElement } from './DFElement.js?7.0';

export class DFQuestion extends DFElement {
	constructor(section) {
		super();
		this.section = section;
		this.isOmitted = true;
		this.isBonus = false;
		this.answers = [];
		this.criteria = [];
	}

	setQuestionNumber(value) {
		this.questionNumber = value;
	}

	getQuestionNumber() {
		return this.questionNumber;
	}

    addAnswer(newAnswer) {
        if (newAnswer.length === 0) return;
        this.isOmitted = false;    
        this.answers.push(newAnswer);
    }

	getNumAnswers() {
		return this.answers.length;
	}

	getAnswer(answerNum) {
		if (answerNum < 0 || answerNum >= this.answers.length) return 'ERR';
		return this.answers[answerNum];
	}

	getSection() {
		return this.section;
	}

	addCriterion(name, maxScore) {
		this.criteria.push({ 
            name: name, 
            maxScore: maxScore
        });
	}

	getCriteria() {
		return this.criteria;
	}

	setIsOmitted(value) {
		this.isOmitted = value;
	}

	getIsOmitted() {
		return this.isOmitted;
	}

	setIsBonus(value) {
		this.isBonus = value;
	}

	getIsBonus() {
		return this.isBonus;
	}
}

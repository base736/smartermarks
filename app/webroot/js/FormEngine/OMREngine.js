/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as math from '/js/lib/mathjs/mathjs.mjs';

import { CVImage, non_max_supp, apply_hysteresis } from './CVImage.js?7.0';
import { kernelSkeletonizeA, kernelSkeletonizeB, kernelPruneA, kernelPruneB } from './Kernel.js?7.0';
import { CGSolver } from './CGSolver.js?7.0';
import { pointsPerInch } from './PSForm.js?7.0';

// Input DPI for which parameters have been optimized

const scanDPI = 100;

// Parameters for the identification of response bubbles in assessments

const blurSigma = 1.0;
const excludedFraction = 0.5;
const excludedMargin = 10;

const quadraticPatchRadius = 2;
const lowThreshold = 0.01;
const highThreshold = 0.35;

const bubbleRadius = 6.7;              // 6.5 < x < 6.7
const bubbleThickness = 2.6;           // 2.6 < x < 2.7
const circularHoughMinValue = 0.034;
const circularHoughOffGrid = 0.045;

const epsilon = 1.0E-5;

const solverSigmaSmall = bubbleRadius;
const solverSigmaMiddleMax = 50.0;
const solverSigmaBig = 170.0;        // Too small and I lose the "cut and paste" forms. Too big and
                                     // imperfect forms with a WR section go.

const maxMissingFraction = 0.1;      // Relative (expected number missing depends on how many are there)
const otherCountWarning = 8;         // Absolute (expected extras depends on page size)
const otherCountError = 50;

const colinearTolerance = 5.0;       // Below 4.0 starts to miss legitimate lines of bubbles
const minColinear = 4;

const minModelScale = 0.5;
const maxModelScale = 2.0;

// Parameters for determining whether or not response bubbles have been filled in

const focusRadius = 5;               // About 80% of bubbleRadius
var bubbleBufferSize = false;
var maxDeltaI = false;

const ambiguousMin = -0.07;
const ambiguousMax = 0.07;

const tooManyRatio = 0.5;            // More than 0.5 misses some very double-filled looking bubbles

// Shared resources

var emptyBubbleImage = false;
var filledBubbleImage = false;
var patchInverse = false;

export const OMRMessage = {
    OMRMessageNone: 0,
    
    OMRWarning: 1,
    OMRWarningEmptySection: 2,
    OMRWarningBadWRScore: 3,
    OMRWarningMissingWRScore: 4,
    OMRWarningAmbiguousFill: 5,
    OMRWarningExtraMarks: 6,
    OMRWarningExtraBubbles: 7,
    OMRWarningBadPageOrder: 8,
    OMRWarningBadResponse: 9,
    
    OMRError: 10,
    OMRErrorBadSection: 11,
    OMRErrorExtraBubbles: 12,
    OMRErrorBadPage: 13,
    OMRErrorMissingPage: 14
};

export class MessageEntry {
    constructor(message = OMRMessage.OMRMessageNone, sectionIndex = -1, bubbleIndex = -1) {
        this.message = message;
        this.sectionIndex = sectionIndex;
        this.bubbleIndex = bubbleIndex;
    }

    compareTo(other) {
        if (this.message !== other.message) {
            return this.message - other.message;
        } else if (this.sectionIndex !== other.sectionIndex) {
            return this.sectionIndex - other.sectionIndex;
        } else {
            return this.bubbleIndex - other.bubbleIndex;
        }
    }
}

export class OMREngine {

    static normalize = function(image) {
        image.invert();
        image.applyGaussian(blurSigma);
        image.invert();
        image.setValueRange(0.0, 1.0);

        const count = new Array(256).fill(0);
        const size = image.width * image.height;

        const imageData = image.data;
        for (let i = 0; i < size; ++i) {
            const floatValue = imageData[i];
            const intValue = Math.floor(floatValue * 255);
            count[intValue]++;
        }

        let numExcluded = 0;
        let lastIncluded = 255;
        while (numExcluded < excludedFraction * size) {
            numExcluded += count[lastIncluded--];
        }
        lastIncluded -= excludedMargin;

        if (lastIncluded > 0) {
            image.clipToRange(0.0, lastIncluded / 255.0);
            image.setValueRange(0.0, 1.0);
        } else {
            image.fillWith(1.0);
        }
    };

    static getCurvatures = function(image, i, j) {
        const imageWidth = image.width;
        const imageHeight = image.height;
        const imageData = image.data;

        if (patchInverse === false) patchInverse = OMREngine.getPatchInverse();

        let goodPoint = true;
        if (i < quadraticPatchRadius) goodPoint = false;
        else if (i > imageWidth - quadraticPatchRadius) goodPoint = false;
        else if (j < quadraticPatchRadius) goodPoint = false;
        else if (j > imageHeight - quadraticPatchRadius) goodPoint = false;
        if (!goodPoint) return {curvatureX: 0.0, curvatureY: 0.0};

        const terms2 = new Array(6).fill(0);
        for (let deltaJ = -quadraticPatchRadius; deltaJ <= quadraticPatchRadius; ++deltaJ) {
            let offset = (j + deltaJ) * imageWidth + i - quadraticPatchRadius;
            for (let deltaI = -quadraticPatchRadius; deltaI <= quadraticPatchRadius; ++deltaI) {
                let value = imageData[offset++];
                if (value === 0.0) continue;

                terms2[0] += value;
                const valueI = value * deltaI;
                terms2[1] += valueI;
                const valueJ = value * deltaJ;
                terms2[2] += valueJ;
                const valueII = valueI * deltaI;
                terms2[3] += valueII;
                const valueIJ = valueI * deltaJ;
                terms2[4] += valueIJ;
                const valueJJ = valueJ * deltaJ;
                terms2[5] += valueJJ;
            }
        }

        const coefficients = new Array(3).fill(0);
        for (let p = 0; p < 3; ++p) {
            coefficients[p] = 0.0;
            for (let q = 0; q < 6; ++q) {
                coefficients[p] += patchInverse.get([p, q]) * terms2[q];
            }
        }

        const angle = 0.5 * Math.atan2(coefficients[1], coefficients[0] - coefficients[2]);
        const c = Math.cos(angle);
        const s = Math.sin(angle);

        const curvatureX = coefficients[0] * c * c + coefficients[1] * c * s + coefficients[2] * s * s;
        const curvatureY = coefficients[2] * c * c - coefficients[1] * c * s + coefficients[0] * s * s;

        return {
            sin: s,
            cos: c,
            x: curvatureX,
            y: curvatureY
        }
    };

    static getBubbleImage = function(fillBubble = false) {
        const maskSize = 2 * Math.ceil(bubbleRadius + bubbleThickness) + 1;

        var bubbleImage = new CVImage(maskSize, maskSize);
        var bubbleData = bubbleImage.data;

        let index = 0;
        for (let i = 0; i < maskSize; ++i) {
            const x = i - Math.floor(maskSize / 2);
            for (let j = 0; j < maskSize; ++j) {
                const y = j - Math.floor(maskSize / 2);
                
                const r = Math.sqrt(x * x + y * y);
                const deltaR = r - bubbleRadius;

                if (fillBubble && (r < bubbleRadius)) {
                    bubbleData[index] = 1.0;
                } else if (Math.abs(deltaR) < bubbleThickness) {
                    bubbleData[index] = (1.0 + Math.cos(deltaR * Math.PI / bubbleThickness)) / 2.0;
                } else {
                    bubbleData[index] = 0.0;
                }
                index++;
            }
        }

        return bubbleImage;
    }

    static getPatchInverse = function() {
        var fitMatrix = math.zeros(6, 6);
        for (let deltaI = -quadraticPatchRadius; deltaI <= quadraticPatchRadius; ++deltaI) {
            for (let deltaJ = -quadraticPatchRadius; deltaJ <= quadraticPatchRadius; ++deltaJ) {
                const terms = math.matrix([[1.0, deltaI, deltaJ, deltaI * deltaI, deltaI * deltaJ, deltaJ * deltaJ]]);
                const outerProduct = math.multiply(math.transpose(terms), terms);
                fitMatrix = math.add(fitMatrix, outerProduct);
            }
        }

        const solutionMatrix = math.concat(fitMatrix, math.identity(6), 1);

        for (let p = 0; p < 6; ++p) {
            const diagonalValue = solutionMatrix.get([p, p]);
            for (let q = 0; q < 12; ++q) {
                solutionMatrix.set([p, q], solutionMatrix.get([p, q]) / diagonalValue);
            }
            for (let r = 0; r < 6; ++r) {
                if (r === p) continue;
                const eliminatedValue = solutionMatrix.get([r, p]);
                for (let q = 0; q < 12; ++q) {
                    solutionMatrix.set([r, q], solutionMatrix.get([r, q]) - eliminatedValue * solutionMatrix.get([p, q]));
                }
            }
        }

        const rowRange = math.range(3, 6);
        const colRange = math.range(6, 12);
        return math.subset(solutionMatrix, math.index(rowRange, colRange));        
    }

    static getPatch = function(source, point, patch, a) {

        // Ensure patch is square and has an odd size

        if (patch.width !== patch.height) return false;
        if ((patch.width - 1) % 2 !== 0) return false;
    
        const patchSize = patch.width;
        const patchRadius = (patchSize - 1) / 2;
        
        // Check if the point is within the valid range

        if (point.x < patchRadius + (a || 0) || point.x >= source.width - patchRadius - (a || 0)) return false;
        if (point.y < patchRadius + (a || 0) || point.y >= source.height - patchRadius - (a || 0)) return false;
        
        const centerI = Math.floor(point.x);
        const centerJ = Math.floor(point.y);
        const offsetI = point.x - centerI;
        const offsetJ = point.y - centerJ;
        
        const imageWidth = source.width;
        const imageHeight = source.height;
        const imageData = source.data;
        const patchData = patch.data;

        if (typeof a === 'undefined') {

            // Simple patch extraction

            for (let j = -patchRadius; j <= patchRadius; ++j) {
                for (let i = -patchRadius; i <= patchRadius; ++i) {
                    const value = imageData[(centerJ + j) * imageWidth + (centerI + i)];
                    patchData[(j + patchRadius) * patchSize + (i + patchRadius)] = value;
                }
            }
    
        } else {

            // Lanczos patch extraction

            const sincSize = 2 * a + 1;
            const sincX = new Float32Array(sincSize);
            const sincY = new Float32Array(sincSize);
    
            for (let i = -a; i <= a; ++i) {
                const x = Math.PI * (i - offsetI);
                sincX[i + a] = x === 0 ? 1.0 : (Math.sin(x) / x) * (Math.sin(x / a) / (x / a));
    
                const y = Math.PI * (i - offsetJ);
                sincY[i + a] = y === 0 ? 1.0 : (Math.sin(y) / y) * (Math.sin(y / a) / (y / a));
            }

            // Horizontal convolution

            const tempHeight = patchSize + 2 * a;
            const tempData = new Float32Array(tempHeight * patchSize);

            // Temp data is transposed so that the vertical convolution has small steps

            let tempIndex = 0;
            for (let i = -patchRadius; i <= patchRadius; ++i) {
                for (let j = -patchRadius - a; j <= patchRadius + a; ++j) {
                    let value = 0.0;
                    let imageIndex = (centerJ + j) * imageWidth + (centerI + i) - a;
                    for (let k = 0; k < sincSize; ++k) {
                        const sourceValue = imageData[imageIndex++];
                        value += sincX[k] * sourceValue;
                    }
                    tempData[tempIndex++] = value;
                }
            }

            // Vertical convolution
            
            let patchIndex = 0;
            for (let j = -patchRadius; j <= patchRadius; ++j) {
                for (let i = -patchRadius; i <= patchRadius; ++i) {
                    let value = 0.0;
                    let tempIndex = (j + patchRadius) + (i + patchRadius) * tempHeight;
                    for (let k = 0; k < sincSize; ++k) {
                        const tempValue = tempData[tempIndex++];
                        value += sincY[k] * tempValue;
                    }
                    patchData[patchIndex++] = value;
                }
            }
        }
    
        return true;
    }
    
    static refineBubblePoint = function(point, image) {

        if (filledBubbleImage === false) filledBubbleImage = OMREngine.getBubbleImage(true);
        const maskData = filledBubbleImage.data;

        const maskEdgeSize = filledBubbleImage.width;
        const maskSize = maskEdgeSize * maskEdgeSize;
    
        // Walk the center of the bubble in to a position which gives the best correlation with the bubble mask

        let patch = new CVImage(maskEdgeSize, maskEdgeSize);
        const patchData = patch.data;
    
        point.x = Math.round(point.x);
        point.y = Math.round(point.y);
    
        let maxSum = 0.0;
        let neighborhoodRadius = 2;
        let stepSize = 1.0;
    
        for (let stage = 0; stage <= 3; ++stage) {
            let done;
            do {
                done = true;
    
                for (let deltaI = -neighborhoodRadius; deltaI <= neighborhoodRadius; ++deltaI) {
                    for (let deltaJ = -neighborhoodRadius; deltaJ <= neighborhoodRadius; ++deltaJ) {
                        let translated = { x: point.x + deltaI * stepSize, y: point.y + deltaJ * stepSize };
    
                        let success;
                        if (stage === 0) success = OMREngine.getPatch(image, translated, patch);
                        else success = OMREngine.getPatch(image, translated, patch, 2);
                        if (!success) continue;
    
                        let sum = 0.0;
                        for (let n = 0; n < maskSize; ++n) {
                            sum += maskData[n] * (1.0 - patchData[n]);
                        }
    
                        if (sum > maxSum) {
                            if ((stage === 0) && ((deltaI !== 0.0) || (deltaJ !== 0.0))) done = false;
                            maxSum = sum;
                            point = translated;
                        }
                    }
                }
            } while (!done);
    
            neighborhoodRadius = 1;
            stepSize /= 2.0;
        }

        return point;
    }
    
    static findBubbles = function(blockModels, expectedPage, image) {
    
        /**************************************************************************************************/
        /* Find bubble centers in image                                                                   */
        /**************************************************************************************************/

        // Get bubble images

        if (emptyBubbleImage === false) emptyBubbleImage = OMREngine.getBubbleImage(false);
    
        // Transform image

        var transformedImage = image.copy();
        transformedImage.transformValues((value) => Math.sqrt(1.0 - value));

        var imageWidth = transformedImage.width;
        var imageHeight = transformedImage.height;
        var imageData = transformedImage.data;

        // Get image curvature and gradient direction

        var gradx = new CVImage();
        var grady = new CVImage();
        var curvature = new CVImage();

        gradx.resize(imageWidth, imageHeight);
        const gradxData = gradx.data;
        gradx.fillWith(0.0);

        grady.resize(imageWidth, imageHeight);
        const gradyData = grady.data;
        grady.fillWith(0.0);

        curvature.resize(imageWidth, imageHeight);
        const curvatureData = curvature.data;
        curvature.fillWith(0.0);

        for (let j = quadraticPatchRadius; j < imageHeight - quadraticPatchRadius; ++j) {
            var offset = j * imageWidth + quadraticPatchRadius;
            for (let i = quadraticPatchRadius; i < imageWidth - quadraticPatchRadius; ++i) {
                if (imageData[offset] != 0.0) {
                    var curvatures = OMREngine.getCurvatures(transformedImage, i, j);

                    if ((Math.abs(curvatures.x) > Math.abs(curvatures.y)) && (curvatures.x < 0.0)) {
                        curvatureData[offset] = Math.abs(curvatures.x);
                    } else if ((Math.abs(curvatures.y) > Math.abs(curvatures.x)) && (curvatures.y < 0.0)) {
                        curvatureData[offset] = Math.abs(curvatures.y);
                    }
    
                    gradxData[offset] = -curvatures.sin;
                    gradyData[offset] = curvatures.cos;
                }
                ++offset;
            }
        }
                
        curvature.setValueRange(0.0, 1.0);

        // Get circular hough of ridges

        let edgeImage = non_max_supp(curvature, gradx, grady);
        apply_hysteresis(edgeImage, curvature, lowThreshold, highThreshold);

        let kernels = [kernelSkeletonizeA(), kernelSkeletonizeB()];
        edgeImage.thinWithKernels(kernels, 2, -1);
    
        kernels = [kernelPruneA(), kernelPruneB()];
        edgeImage.thinWithKernels(kernels, 2, 2);
    
        const circularHough = new CVImage();
        edgeImage.convolve(emptyBubbleImage, circularHough);
        circularHough.setValueRange(0.0, 1.0);
    
        // Compute offsets

        const offsets = [];
        for (let deltaJ = -1; deltaJ <= 1; ++deltaJ) {
            for (let deltaI = -1; deltaI <= 1; ++deltaI) {
                if ((deltaI !== 0) || (deltaJ !== 0)) {
                    offsets.push(deltaJ * imageWidth + deltaI);
                }
            }
        }
    
        // Iterate through the image and find local maxima

        const circularHoughData = circularHough.data;

        var imagePoints = [];
        for (let j = quadraticPatchRadius; j < imageHeight - quadraticPatchRadius; ++j) {
            for (let i = quadraticPatchRadius; i < imageWidth - quadraticPatchRadius; ++i) {
                const cIndex = j * imageWidth + i;
                const c = circularHoughData[cIndex];
                if (c === 0.0) continue;
    
                let isLocalMaximum = true;
                for (let offsetIndex = 0; offsetIndex < 8; ++offsetIndex) {
                    if (circularHoughData[cIndex + offsets[offsetIndex]] > c) {
                        isLocalMaximum = false;
                        break;
                    }
                }
                if (!isLocalMaximum) continue;
    
                var curvatures = OMREngine.getCurvatures(circularHough, i, j);
                const value = ((curvatures.x >= 0.0) || (curvatures.y >= 0.0)) ? 0.0 : Math.sqrt(curvatures.x * curvatures.y);
    
                if (value > circularHoughMinValue) {
                    imagePoints.push({ x: i, y: j , weight: value});
                }
    
                ++i; // This is a local maximum, so the next one can't be.
            }
        }

        /**************************************************************************************************/
        /* Find best model fit to bubble centers in image                                                 */
        /**************************************************************************************************/
    
        let rawModelScale = 100 / pointsPerInch;

        let solver = new CGSolver(imagePoints);
        
        let fittedPage = -1;

        let bubblePoints = false;
        let bestStrategy = -1;
        let bestBadCount;
        let bestMessage = new MessageEntry(OMRMessage.OMRMessageNone);
    
        const numStrategies = 2 + blockModels.length;
        for (let strategy = 0; strategy < numStrategies; ++strategy) {
            let thisPoints = [];

            let thisPage;
            if (strategy < 3) thisPage = expectedPage;
            else
            {
                thisPage = strategy - 3;
                if (thisPage >= expectedPage) thisPage++;
            }
        
            if (strategy == 2) {
                image.rotate180();
                imagePoints.forEach(point => {
                    point.x = imageWidth - point.x;
                    point.y = imageHeight - point.y;
                });
            }
    
            let pageBlockModels = blockModels[thisPage].map(blockModel => blockModel.copy());
    
            // Copy initial model parameters to a vector

            let rawParameters = solver.readVectorFromModels(pageBlockModels, 0.0, 0.0);
            let workingPoint = math.multiply(rawParameters, rawModelScale);
            let initialPoint = workingPoint;

            let modelPoints = solver.getModelPoints(pageBlockModels, -1, workingPoint);
        
            let solverSigmaMiddle = 0.0;
            pageBlockModels.forEach((model, n) => {
                let minYIncrement = -1;
                let minXIncrement = -1;
    
                let blockPoints = solver.getModelPoints(pageBlockModels, n, workingPoint);
                blockPoints.forEach((point1, i) => {
                    blockPoints.forEach((point2, j) => {
                        if (i !== j) {
                            let xDistance = Math.abs(point1.x - point2.x);
                            if (xDistance > epsilon && (minXIncrement === -1 || xDistance < minXIncrement))
                                minXIncrement = xDistance;
    
                            let yDistance = Math.abs(point1.y - point2.y);
                            if (yDistance > epsilon && (minYIncrement === -1 || yDistance < minYIncrement))
                                minYIncrement = yDistance;
                        }
                    });
                });

                if (minXIncrement < solverSigmaMiddleMax && minXIncrement > solverSigmaMiddle) solverSigmaMiddle = minXIncrement;
                if (minYIncrement < solverSigmaMiddleMax && minYIncrement > solverSigmaMiddle) solverSigmaMiddle = minYIncrement;
            });

            let minImagePoints = Math.floor(modelPoints.length * (1.0 - maxMissingFraction));
            pageBlockModels.forEach(model => {
                model.chunks.forEach(chunk => {
                    minImagePoints -= chunk.numFilled;
                });
            });
 
            let newEntry = new MessageEntry(OMRMessage.OMRMessageNone);
            let numOtherGrid = imagePoints.length;

            if (imagePoints.length < minImagePoints) {

                newEntry = new MessageEntry(OMRMessage.OMRErrorBadPage);

            } else {
 
                let pointReference = {};
                if (pageBlockModels.length > 0) {
                    pointReference.x = workingPoint.get([2]);
                    pointReference.y = workingPoint.get([3 + pageBlockModels[0].xGrid.indices.length]);
                }
    
                // Build an approximate Hessian

                let numParameters = workingPoint.size()[0];

                let fakeHessian = math.zeros(numParameters, numParameters);
                let pointOffset = 0;
                let parameterOffset = 2;
                pageBlockModels.forEach((model, i) => {
                    for (let n = 0; n < model.indices.length; ++n) {
                        let pointDelta = solver.subtractPoints(modelPoints[n + pointOffset], pointReference);
                        let pointRadius = solver.lengthPoint(pointDelta);
                        fakeHessian.set([0, 0], fakeHessian.get([0, 0]) + pointRadius * pointRadius);
                        fakeHessian.set([1, 1], fakeHessian.get([1, 1]) + pointRadius * pointRadius);
                    }
            
                    let numXParameters = model.xGrid.indices.length;
                    model.indices.forEach((index, n) => {
                        let pointDelta = solver.subtractPoints(modelPoints[n + pointOffset], pointReference);
                        for (let j = 0; j < numXParameters + 1; ++j) {
                            let jValue = (j === 0) ? 1 : index.xIndices[j - 1];
                            fakeHessian.set([parameterOffset + j, 1], fakeHessian.get([parameterOffset + j, 1]) - jValue * pointDelta.y);
                            fakeHessian.set([1, parameterOffset + j], fakeHessian.get([1, parameterOffset + j]) - jValue * pointDelta.y);
                            for (let k = 0; k < numXParameters + 1; ++k) {
                                let kValue = (k === 0) ? 1 : index.xIndices[k - 1];
                                fakeHessian.set([parameterOffset + j, parameterOffset + k], 
                                    fakeHessian.get([parameterOffset + j, parameterOffset + k]) + jValue * kValue);
                            }
                        }
                    });
                    parameterOffset += numXParameters + 1;
            
                    let numYParameters = model.yGrid.indices.length;
                    model.indices.forEach((index, n) => {
                        let pointDelta = solver.subtractPoints(modelPoints[n + pointOffset], pointReference);
                        for (let j = 0; j < numYParameters + 1; ++j) {
                            let jValue = (j === 0) ? 1 : index.yIndices[j - 1];
                            fakeHessian.set([parameterOffset + j, 0], fakeHessian.get([parameterOffset + j, 0]) + jValue * pointDelta.x);
                            fakeHessian.set([0, parameterOffset + j], fakeHessian.get([0, parameterOffset + j]) + jValue * pointDelta.x);
                            for (let k = 0; k < numYParameters + 1; ++k) {
                                let kValue = (k === 0) ? 1 : index.yIndices[k - 1];
                                fakeHessian.set([parameterOffset + j, parameterOffset + k], 
                                    fakeHessian.get([parameterOffset + j, parameterOffset + k]) + jValue * kValue);
                            }
                        }
                    });
                    parameterOffset += numYParameters + 1;
                    pointOffset += model.indices.length;
                });
            
                // Get a projection that allows only translation and rotation of all models together

                let xRotation = math.zeros(numParameters);
                xRotation.set([0], 1.0);
            
                let yRotation = math.zeros(numParameters);
                yRotation.set([1], 1.0);
            
                let blockXTranslations = [];
                let blockYTranslations = [];
                let blockZeroScalings = [];
                let blockScalings = [];
                let parameterIndex = 2;
                pageBlockModels.forEach((model, i) => {
                    let thisXTranslation = math.zeros(numParameters);
                    thisXTranslation.set([parameterIndex], 1.0);
                    blockXTranslations.push(thisXTranslation);
            
                    let thisZeroScaling = math.zeros(numParameters);
                    thisZeroScaling.set([parameterIndex], workingPoint.get([parameterIndex]));
                    parameterIndex++;
            
                    let thisScaling = math.zeros(numParameters);
                    for (let j = 0; j < model.xGrid.indices.length; ++j) {
                        thisScaling.set([parameterIndex + j], workingPoint.get([parameterIndex + j]));
                    }
                    parameterIndex += model.xGrid.indices.length;
            
                    let thisYTranslation = math.zeros(numParameters);
                    thisYTranslation.set([parameterIndex], 1.0);
                    blockYTranslations.push(thisYTranslation);
            
                    thisZeroScaling.set([parameterIndex], workingPoint.get([parameterIndex]));
                    blockZeroScalings.push(thisZeroScaling);
                    parameterIndex++;
            
                    for (let j = 0; j < model.yGrid.indices.length; ++j) {
                        thisScaling.set([parameterIndex + j], workingPoint.get([parameterIndex + j]));
                    }
                    parameterIndex += model.yGrid.indices.length;
                    blockScalings.push(thisScaling);
                });
            
                let xTranslation = math.zeros(numParameters);
                blockXTranslations.forEach(translation => {
                    xTranslation = math.add(xTranslation, translation);
                });
            
                let yTranslation = math.zeros(numParameters);
                blockYTranslations.forEach(translation => {
                    yTranslation = math.add(yTranslation, translation);
                });
            
                let allScaling = math.zeros(numParameters);
                blockScalings.forEach(scaling => {
                    allScaling = math.add(allScaling, scaling);
                });
                blockZeroScalings.forEach(zeroScaling => {
                    allScaling = math.add(allScaling, zeroScaling);
                });
            
                xTranslation = math.divide(xTranslation, math.norm(xTranslation));
                yTranslation = math.divide(yTranslation, math.norm(yTranslation));
                allScaling = math.divide(allScaling, math.norm(allScaling));
            
                let basisA = [xTranslation, yTranslation];
                let basisB = [xTranslation, yTranslation, xRotation, yRotation];
                let basisC = [xTranslation, yTranslation, xRotation, yRotation, allScaling];
            
                // Get a projection that respects increment names. Make x and y classes unique to allow for differences
                // between photocopier scaling in the x and y directions.

                let indexClasses = [];
                pageBlockModels.forEach((model, i) => {
                    indexClasses.push('x' + model.xGrid.offset.class);
                    for (let j = 0; j < model.xGrid.indices.length; ++j) {
                        indexClasses.push('x' + model.xGrid.indices[j].class);
                    }
                    indexClasses.push('y' + model.yGrid.offset.class);
                    for (let j = 0; j < model.yGrid.indices.length; ++j) {
                        indexClasses.push('y' + model.yGrid.indices[j].class);
                    }
                });
                let numModelIndices = indexClasses.length;

                let basisD = [xRotation, yRotation];
                indexClasses.forEach((indexClass, i) => {
                    if (indexClass.substring(1) !== 'classNoClass') {
                        let done = false;
                        for (let j = 0; j < i; ++j) {
                            if (indexClasses[i] === indexClasses[j]) done = true;
                        }
                        if (done) return;
                    }
        
                    let thisVector = math.zeros(numParameters);
                    thisVector.set([i + 2], 1.0);
        
                    if (indexClass.substring(1) !== 'classNoClass') {
                        for (let j = i + 1; j < numModelIndices; ++j) {
                            if (indexClasses[i] === indexClasses[j]) thisVector.set([j + 2], 1.0);
                        }
                    }
        
                    basisD.push(thisVector);
                });

                if (modelPoints.length > 0) {
            
                    if (strategy == 1) {

                        // Casting a wide net

                        solver.setSigma(solverSigmaBig);
                        workingPoint = solver.optimize(workingPoint, pageBlockModels, fakeHessian, basisA);
                        solver.setSigma(Math.sqrt(solverSigmaBig * solverSigmaMiddle));
                        workingPoint = solver.optimize(workingPoint, pageBlockModels, fakeHessian, basisC);
                        solver.setSigma(solverSigmaMiddle);
                        workingPoint = solver.optimize(workingPoint, pageBlockModels, fakeHessian, basisD);
                        
                    } else {

                        // Assuming that the page is very near what we expect

                        solver.setSigma(solverSigmaMiddle);
                        workingPoint = solver.optimize(workingPoint, pageBlockModels, fakeHessian, basisA);
                        workingPoint = solver.optimize(workingPoint, pageBlockModels, fakeHessian, basisB);            
                        solver.setSigma(solverSigmaSmall);
                        workingPoint = solver.optimize(workingPoint, pageBlockModels, fakeHessian, basisC);
            
                    }

                    // Finish the optimization, allowing full freedom

                    solver.setSigma(solverSigmaSmall);
                    workingPoint = solver.optimize(workingPoint, pageBlockModels, fakeHessian, basisD);
                }

                var basis = [
                    { x: Math.cos(workingPoint.get([0])), y: Math.sin(workingPoint.get([0])) },
                    { x: -Math.sin(workingPoint.get([1])), y: Math.cos(workingPoint.get([1])) }
                ];
    
                var pointTypes = Array(imagePoints.length).fill("PointOffGrid");
                var closestIndex = Array(imagePoints.length).fill(-1);
                var badSections = [];
                var pageMatched = 0;
    
                pageBlockModels.forEach((model, n) => {
                    modelPoints = solver.getModelPoints(pageBlockModels, n, workingPoint);
    
                    imagePoints.forEach((imagePoint, i) => {
                        let closestDistance = Infinity;
                        modelPoints.forEach((modelPoint, j) => {
                            let distance = solver.lengthPoint(solver.subtractPoints(modelPoint, imagePoint));
                            if (distance < closestDistance) {
                                closestDistance = distance;
                                closestIndex[i] = j;
                            }
                        });
                    });
    
                    var extraMissing = 0;
                    var sectionMatched = 0;
                    var modelPointIndex = 0;
    
                    model.chunks.forEach(chunk => {
                        let allowedMissing = chunk.numFilled;
                        let actualMissing = 0;
    
                        for (let j = 0; j < chunk.numPoints; ++j) {
                            let isMatched = false;
                            imagePoints.forEach((imagePoint, i) => {
                                let distance = solver.lengthPoint(solver.subtractPoints(modelPoints[modelPointIndex], imagePoint));
                                if (distance < bubbleRadius) {
                                    pointTypes[i] = "PointMainGrid";
                                    if (closestIndex[i] == modelPointIndex) isMatched = true;
                                }
                            });
                            if (isMatched) sectionMatched++;
                            else actualMissing++;
                            modelPointIndex++;
                        }
                        if (actualMissing > allowedMissing) extraMissing += actualMissing - allowedMissing;
                    });
    
                    if (extraMissing > maxMissingFraction * modelPoints.length) badSections.push(n);
                    pageMatched += sectionMatched;
                });
    
                if (badSections.length == 1 && pageBlockModels.length > 1) {
                    newEntry = new MessageEntry(OMRMessage.OMRErrorBadSection, badSections[0]);
                } else if (badSections.length > 0) {
                    newEntry = new MessageEntry(OMRMessage.OMRErrorBadPage);
                } else {
                    let offGridThreshold = circularHoughOffGrid;
    
                    numOtherGrid = 0;
                    imagePoints.forEach((imagePoint, i) => {
                        if (pointTypes[i] != "PointOffGrid") return;
                        if (imagePoint.weight < offGridThreshold) return;
    
                        for (let basisDirection = 0; basisDirection < 2; ++basisDirection) {
                            let matchIndices = [];
                            imagePoints.forEach((otherPoint, j) => {
                                if (otherPoint.weight < offGridThreshold) return;
                                let difference = solver.subtractPoints(otherPoint, imagePoint);
                                if (Math.abs(solver.dotPoints(difference, basis[basisDirection])) < colinearTolerance) {
                                    matchIndices.push(j);
                                }
                            });
    
                            if (matchIndices.length >= minColinear) {
                                matchIndices.forEach(j => {
                                    if (pointTypes[j] == "PointOffGrid") {
                                        pointTypes[j] = "PointOtherGrid";
                                        numOtherGrid++;
                                    }
                                });
                            }
                        }
                    });
    
                    if (numOtherGrid > otherCountError) {
                        newEntry = new MessageEntry(OMRMessage.OMRErrorExtraBubbles);
                    } else if (numOtherGrid > otherCountWarning) {
                        newEntry = new MessageEntry(OMRMessage.OMRWarningExtraBubbles);
                    }
                }
    
                // Update and check model scales

                modelPoints = solver.getModelPoints(pageBlockModels, -1, workingPoint);
                let scales = [];
    
                if (modelPoints.length > 0) {
                    parameterIndex = 2;
                    pageBlockModels.forEach(model => {
                        parameterIndex++;
                        for (let j = 0; j < model.xGrid.indices.length; ++j) {
                            scales.push(workingPoint.get([parameterIndex]) / initialPoint.get([parameterIndex]));
                            parameterIndex++;
                        }
                        parameterIndex++;
                        for (let j = 0; j < model.yGrid.indices.length; ++j) {
                            scales.push(workingPoint.get([parameterIndex]) / initialPoint.get([parameterIndex]));
                            parameterIndex++;
                        }
                    });
                    scales.sort();
                    if (scales[0] < minModelScale || scales[scales.length - 1] > maxModelScale) {
                        newEntry = new MessageEntry(OMRMessage.OMRErrorBadPage);
                    }
                }
    
                if (newEntry.message !== OMRMessage.OMRErrorBadPage) {
                    let badSection = newEntry.message === OMRMessage.OMRErrorBadSection ? newEntry.sectionIndex : -1;
                    let pointNum = 0;
    
                    pageBlockModels.forEach((model, n) => {
                        let blockPoints = model.indices.length;
                        if (n === badSection) {
                            for (let i = 0; i < blockPoints; ++i) {
                                thisPoints.push({ x: -1, y: -1 });
                                pointNum++;
                            }
                        } else {
                            for (let i = 0; i < blockPoints; ++i) {
                                let originalPoint = modelPoints[pointNum++];
                                let point = { ...originalPoint };
                                let isValid = true;
    
                                if (point.x - bubbleRadius < 0 || point.x + bubbleRadius >= image.width ||
                                    point.y - bubbleRadius < 0 || point.y + bubbleRadius >= image.height) {
                                    isValid = false;
                                }
                                if (!isValid) {
                                    newEntry = new MessageEntry(OMRMessage.OMRErrorBadPage);
                                }
    
                                point = OMREngine.refineBubblePoint(point, image);
                                if (point.x - bubbleRadius < 0 || point.x + bubbleRadius >= image.width ||
                                    point.y - bubbleRadius < 0 || point.y + bubbleRadius >= image.height ||
                                    solver.lengthPoint(solver.subtractPoints(point, originalPoint)) > bubbleRadius) {
                                    point = originalPoint;
                                }
                                thisPoints.push(point);
                            }
                        }
                    });

                }

            }

            // Update best strategy

            const thisBadCount = (modelPoints.length - pageMatched) + numOtherGrid;
            
            let isBest = false;
            if (strategy === 0) {
                isBest = true;
            } else if (newEntry.compareTo(bestMessage) < 0) {
                isBest = true;
            } else if (newEntry.compareTo(bestMessage) === 0) {
                if (thisBadCount < bestBadCount) isBest = true;
            }

            if (isBest) {
                if (newEntry.message !== OMRMessage.OMRErrorBadPage) {
                    fittedPage = thisPage;
                    bubblePoints = thisPoints;
                    bestStrategy = strategy;
                    bestBadCount = thisBadCount;
                }
                bestMessage = newEntry;
            }

            if (strategy === 2) {
                image.rotate180();
                imagePoints.forEach(point => {
                    point.x = imageWidth - point.x;
                    point.y = imageHeight - point.y;
                });
            }

            if (bestMessage.message === OMRMessage.OMRMessageNone && strategy !== 1) {
                break;
            }            
        }
    
        if (bestStrategy === 2) {
            image.rotate180();
            imagePoints.forEach(point => {
                point.x = imageWidth - point.x;
                point.y = imageHeight - point.y;
            });
        }

        var messages = [];
        if (bestMessage.message !== OMRMessage.OMRMessageNone) {
            messages.push(bestMessage);
        }

        return {
            fittedPage: fittedPage,
            points: bubblePoints,
            badBubblesCount: bestBadCount,
            messages: messages
        };
    }    

    static defineBubbleBuffer() {
        maxDeltaI = new Int16Array(2 * focusRadius + 1);
        bubbleBufferSize = 0;
        for (let deltaJ = -focusRadius; deltaJ <= focusRadius; deltaJ++) {
            maxDeltaI[deltaJ + focusRadius] = Math.sqrt(focusRadius * focusRadius - deltaJ * deltaJ);
            bubbleBufferSize += 2.0 * maxDeltaI[deltaJ + focusRadius] + 1;
        }
    }

    static readBubble(point, image) {
        let isValid = true;
        if ((point.x - focusRadius < 0.0) || (point.x + focusRadius >= image.width)) isValid = false;
        if ((point.y - focusRadius < 0.0) || (point.y + focusRadius >= image.height)) isValid = false;
        if (!isValid) return null;

        if (maxDeltaI === false) OMREngine.defineBubbleBuffer();

        let data = new Float32Array(bubbleBufferSize);
        
        const patchSize = 2 * focusRadius + 1;
        const patch = new CVImage(patchSize, patchSize);
        const patchData = patch.data;

        OMREngine.getPatch(image, point, patch, 2);
        
        let dataOffset = 0;
        for (let deltaJ = -focusRadius; deltaJ <= focusRadius; deltaJ++) {
            let rowRadius = maxDeltaI[deltaJ + focusRadius];
            let sourceI = focusRadius - rowRadius;
            let sourceJ = focusRadius + deltaJ;

            let patchOffset = sourceJ * patchSize + sourceI;
            for (let deltaI = -rowRadius; deltaI <= rowRadius; deltaI++)
                data[dataOffset++] = patchData[patchOffset++];
        }
        
        return data;
    }
    
    static getBubbleData(key, gamma, pixelValues) {
        let data = { key: key };
        
        let sum = 0.0;
        for (let j = 0; j < bubbleBufferSize; ++j) {
            let value = pixelValues[j];
            if (value <= 0.0) value = 0.0;
            else if (value >= 1.0) value = 1.0;
            else value = Math.pow(value, gamma);
            
            sum += value;
        }
        data.value = Math.pow(sum / bubbleBufferSize, 1.0 / gamma);
                
        return data;
    }
    
    static getMedians(sortedBrightness, threshold) {
        let firstBlank, medianFilled, medianBlank;
        
        let minIndex = 0;
        let maxIndex = sortedBrightness.length - 1;
        if (sortedBrightness[minIndex] >= threshold) {
            firstBlank = 0;
            medianFilled = 0.0;
            medianBlank = sortedBrightness[Math.floor(maxIndex / 2)];
        } else if (sortedBrightness[maxIndex] < threshold) {
            firstBlank = sortedBrightness.length;
            medianFilled = sortedBrightness[Math.floor(maxIndex / 2)];
            medianBlank = 1.0;
        } else {
            while (maxIndex - minIndex > 1) {
                let midIndex = Math.floor((maxIndex + minIndex) / 2);
                if (sortedBrightness[midIndex] < threshold) minIndex = midIndex;
                else maxIndex = midIndex;
            }
            
            firstBlank = maxIndex;
            medianFilled = sortedBrightness[Math.floor((firstBlank - 1) / 2)];
            medianBlank = sortedBrightness[Math.floor((firstBlank + sortedBrightness.length) / 2)];
        }
        
        return [firstBlank, medianFilled, medianBlank];
    }
    
    static getBrightnesses(parameters, format, pixelData) {
        let pointIndex = 0;
        let bubbleInfo = [];
    
        let numPages = format.getNumPages();
        for (let pageNum = 0; pageNum < numPages; ++pageNum) {
            let pageFormat = format.pageAtIndex(pageNum);
            let title = pageFormat.getTitle();
            let idDigits = -1;
            let nameLetters = -1;
            if (title) {
                if (title.hasIDField()) idDigits = title.idDigits();
                if (title.hasNameField()) nameLetters = title.nameLetters();
            }
            if (idDigits > 0) {
                let newBubbles = [];
                for (let j = 0; j < idDigits; ++j) {
                    for (let k = 0; k < 10; ++k) {
                        let key = String.fromCharCode('0'.charCodeAt(0) + k);
                        let pixelValues = pixelData[pointIndex++];
                        if (pixelValues) newBubbles.push(OMREngine.getBubbleData(key, parameters.gamma, pixelValues));
                    }
                }
                bubbleInfo.push(newBubbles);
            }
            if (nameLetters > 0) {
                let newBubbles = [];
                for (let j = 0; j < nameLetters; ++j) {
                    for (let k = 0; k < 27; ++k) {
                        let key = (k === 0) ? ' ' : String.fromCharCode('a'.charCodeAt(0) + k - 1);
                        let pixelValues = pixelData[pointIndex++];
                        if (pixelValues) newBubbles.push(OMREngine.getBubbleData(key, parameters.gamma, pixelValues));
                    }
                }
                bubbleInfo.push(newBubbles);
            }
            for (let i = 0; i < pageFormat.getNumSections(); ++i) {
                let section = pageFormat.sectionAtIndex(i);
                let questionCount = section.getNumQuestions();
                if (questionCount === 0) continue;
    
                let newBubbles = [];
                if (section.getType() === 'MCSectionType') {
                    for (let j = 0; j < questionCount; ++j) {
                        let dfQuestion = section.questionAtIndex(j);
                        let type = dfQuestion.getStringPropertyForName('questionType');
                        let choiceCount = dfQuestion.getIntPropertyForName('choiceCount', 4);
                        for (let k = 0; k < choiceCount; ++k) {
                            let key;
                            if (type === "Letter") key = String.fromCharCode('a'.charCodeAt(0) + k);
                            else key = (k === 0) ? 't' : 'f';
                            let pixelValues = pixelData[pointIndex++];
                            if (pixelValues) newBubbles.push(OMREngine.getBubbleData(key, parameters.gamma, pixelValues));
                        }
                    }
                } else if (section.getType() === 'NRSectionType') {
                    let nrStyle = format.getStringPropertyForName('nrStyle', 'Default');
                    if (section.hasProperty('nrStyle')) nrStyle = section.getStringPropertyForName('nrStyle');
    
                    let nrRows = (nrStyle === "Fractions") ? 12 : 11;
    
                    for (let j = 0; j < questionCount; ++j) {
                        let dfQuestion = section.questionAtIndex(j);
                        let digitCount = dfQuestion.getIntPropertyForName('digitCount', 4);
                        for (let p = 0; p < digitCount; ++p) {
                            let qMin = 0;
                            if (nrStyle === "Negatives") {
                                if (p === digitCount - 1) qMin = 1;
                            } else if (nrStyle === "Fractions") {
                                if (p === 0) qMin = 1;
                                else if (p === digitCount - 1) qMin = 2;
                            } else {
                                if ((p === 0) || (p === digitCount - 1)) qMin = 1;
                            }
    
                            for (let q = qMin; q < nrRows; ++q) {
                                let key;
                                if (nrStyle === "Negatives") {
                                    if ((q === 0) && (p === 0)) key = '-';
                                    else if (q === 0) key = '.';
                                    else key = String.fromCharCode('0'.charCodeAt(0) + q - 1);
                                } else if (nrStyle === "Fractions") {
                                    if (q === 0) key = '/';
                                    else if ((q === 1) && (p === 0)) key = '-';
                                    else if (q === 1) key = '.';
                                    else key = String.fromCharCode('0'.charCodeAt(0) + q - 2);
                                } else {
                                    if (q === 0) key = '.';
                                    else key = String.fromCharCode('0'.charCodeAt(0) + q - 1);
                                }
                                let pixelValues = pixelData[pointIndex++];
                                if (pixelValues) newBubbles.push(OMREngine.getBubbleData(key, parameters.gamma, pixelValues));
                            }
                        }
                    }
                } else if (section.getType() === 'WRSectionType') {
                    for (let j = 0; j < questionCount; ++j) {
                        let dfQuestion = section.questionAtIndex(j);
                        let numCriteria = dfQuestion.getCriteria().length;
                        for (let p = 0; p < numCriteria; ++p) {
                            let maxScore = dfQuestion.getCriteria()[p].maxScore;
                            for (let q = 0; q <= maxScore; ++q) {
                                let key = String.fromCharCode('0'.charCodeAt(0) + q);
                                let pixelValues = pixelData[pointIndex++];
                                if (pixelValues) newBubbles.push(OMREngine.getBubbleData(key, parameters.gamma, pixelValues));
                            }
                            let pixelValues = pixelData[pointIndex++];
                            if (pixelValues) newBubbles.push(OMREngine.getBubbleData('+', parameters.gamma, pixelValues));
                        }
                    }
                }
                bubbleInfo.push(newBubbles);
            }
        }
    
        let brightnesses = [];
        let pixelIndex = 0;
        for (let sectionIndex = 0; sectionIndex < bubbleInfo.length; ++sectionIndex) {
            if (bubbleInfo[sectionIndex].length > 0) {
                let offsets = [];
                let sumXA = 0.0;
                let sumAA = 0.0;
                for (let n = 0; n < bubbleInfo[sectionIndex].length; ++n) {
                    let value = bubbleInfo[sectionIndex][n].value;
                    let offset = parameters.offsets[bubbleInfo[sectionIndex][n].key];
                    if (value > parameters.blankThreshold) {
                        sumXA += (1.0 - value) * (1.0 - offset);
                        sumAA += (1.0 - offset) * (1.0 - offset);
                    }
                    offsets.push(offset);
                }
    
                let medianFilled = parameters.medianFilled;
                let medianBlank = 1.0;
    
                let c = (sumAA === 0.0) ? 1.0 : (sumXA / sumAA);
                for (let n = 0; n < bubbleInfo[sectionIndex].length; ++n) {
                    bubbleInfo[sectionIndex][n].value /= (1.0 - c * (1.0 - offsets[n]));
                }
    
                let sortedBrightness = bubbleInfo[sectionIndex].map(b => b.value).sort((a, b) => a - b);
    
                let brightnessIterations = 3;
                for (let i = 0; i < brightnessIterations; ++i) {
                    let brightnessThreshold = medianFilled * parameters.filledWeight + medianBlank * (1.0 - parameters.filledWeight);
                    let [firstBlank, thisMedianFilled, thisMedianBlank] = OMREngine.getMedians(sortedBrightness, brightnessThreshold);
                    
                    medianBlank = thisMedianBlank;
                    if (firstBlank > 0) medianFilled = sortedBrightness[Math.floor((firstBlank - 1) / 2)];
                    else medianFilled = 0.0;
                }
    
                for (let n = 0; n < bubbleInfo[sectionIndex].length; ++n) {
                    while (!pixelData[pixelIndex]) {
                        brightnesses.push(false);
                        pixelIndex++;
                    }
    
                    let thisBrightness = (medianBlank - bubbleInfo[sectionIndex][n].value) / (medianBlank - medianFilled);
                    brightnesses.push(thisBrightness);
                    pixelIndex++;
                }
            }
        }
    
        while (pixelIndex < pixelData.length) {
            brightnesses.push(false);
            pixelIndex++;
        }

        return brightnesses;
    }

    static readPoints(brightnesses, index, count) {
        let results = [];
        for (let q = 0; q < count; ++q) {
            results.push({
                index: q,
                value: brightnesses[index]
            });
            ++index;
        }

        return results;
    }
    
    static getResponses(parameters, format, brightnesses) {
        let sectionIndex = 0;
        let brightnessIndex = 0;

        let responses = [];
        let studentData = {};
        let messages = [];
    
        let numPages = format.getNumPages();
        for (let pageNum = 0; pageNum < numPages; ++pageNum) {
            let pageFormat = format.pageAtIndex(pageNum);
    
            let title = pageFormat.getTitle();
            let idDigits = -1;
            let nameLetters = -1;
            if (title) {
                if (title.hasIDField()) idDigits = title.idDigits();
                if (title.hasNameField()) nameLetters = title.nameLetters();
            }

            if (idDigits > 0) {
                let isValid = true;
                let sumBuffers = []
                let startIndex = brightnessIndex;
                for (let p = 0; p < idDigits; ++p) {
                    sumBuffers.push(OMREngine.readPoints(brightnesses, brightnessIndex, 10));
                    brightnessIndex += 10;

                    if (sumBuffers[p][0].value === false) isValid = false;
                }

                if (isValid) {
                    let darkThreshold = parameters.filledWeight;
                    for (let p = 0; p < idDigits; ++p) {
                        sumBuffers[p].sort((a, b) => b.value - a.value);
                        if (sumBuffers[p][0].value > darkThreshold) darkThreshold = 0.0;
                    }
                    for (let p = 0; p < idDigits; ++p) {
                        if (sumBuffers[p][0].value > darkThreshold) {
                            let nextIndex;
                            for (nextIndex = 1; nextIndex < 10; ++nextIndex)
                                if ((sumBuffers[p][0].value - sumBuffers[p][nextIndex].value) / sumBuffers[p][0].value >= parameters.minNextDifference) break;
                            if (nextIndex < 10) {
                                let midpoint = sumBuffers[p][nextIndex].value * parameters.nextWeight + sumBuffers[p][0].value * (1.0 - parameters.nextWeight);
                                if (midpoint > darkThreshold) darkThreshold = midpoint;
                            }
                        }
                    }
    
                    let responseString = "";
                    for (let p = 0; p < idDigits; ++p) {
                        let margin = sumBuffers[p][0].value - darkThreshold;
                        if ((margin > ambiguousMin) && (margin < ambiguousMax)) {
                            messages.push(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                        }
    
                        if (sumBuffers[p][0].value > darkThreshold) {
                            if ((sumBuffers[p][1].value > darkThreshold) && (sumBuffers[p][1].value > tooManyRatio * sumBuffers[p][0].value)) {
                                messages.push(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex));
                            }
    
                            let thisCharacter = String.fromCharCode('0'.charCodeAt(0) + sumBuffers[p][0].index);
                            responseString = thisCharacter + responseString;
                        } else {
                            responseString = ' ' + responseString;
                        }
                    }
    
                    studentData.id = responseString;
                }
                sectionIndex++;
            }

            if (nameLetters > 0) {
                let isValid = true;
                let sumBuffers = [];
                let startIndex = brightnessIndex;
                for (let p = 0; p < nameLetters; ++p) {
                    sumBuffers.push(OMREngine.readPoints(brightnesses, brightnessIndex, 27));
                    brightnessIndex += 27;

                    if (sumBuffers[p][0].value === false) isValid = false;
                }
                if (isValid) {
                    let darkThreshold = parameters.filledWeight;
                    for (let p = 0; p < nameLetters; ++p) {
                        sumBuffers[p].sort((a, b) => b.value - a.value);
                        if (sumBuffers[p][0].value > darkThreshold) darkThreshold = 0.0;
                    }
                    for (let p = 0; p < nameLetters; ++p) {
                        if (sumBuffers[p][0].value > darkThreshold) {
                            let nextIndex;
                            for (nextIndex = 1; nextIndex < 27; ++nextIndex)
                                if ((sumBuffers[p][0].value - sumBuffers[p][nextIndex].value) / sumBuffers[p][0].value >= parameters.minNextDifference) break;
                            if (nextIndex < 27) {
                                let midpoint = sumBuffers[p][nextIndex].value * parameters.nextWeight + sumBuffers[p][0].value * (1.0 - parameters.nextWeight);
                                if (midpoint > darkThreshold) darkThreshold = midpoint;
                            }
                        }
                    }
    
                    let responseString = "";
                    for (let p = 0; p < nameLetters; ++p) {
                        let margin = sumBuffers[p][0].value - darkThreshold;
                        if ((margin > ambiguousMin) && (margin < ambiguousMax)) {
                            messages.push(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                        }
    
                        if (sumBuffers[p][0].value > darkThreshold) {
                            if ((sumBuffers[p][1].value > darkThreshold) && (sumBuffers[p][1].value > tooManyRatio * sumBuffers[p][0].value)) {
                                messages.push(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex));
                            }
    
                            let thisCharacter = (sumBuffers[p][0].index === 0) ? '_' : 
                                String.fromCharCode('a'.charCodeAt(0) + sumBuffers[p][0].index - 1)
                            responseString = thisCharacter + responseString;
                        } else {
                            responseString = ' ' + responseString;
                        }
                    }
    
                    studentData.name = responseString;
                }
                sectionIndex++;
            }

            for (let i = 0; i < pageFormat.getNumSections(); ++i) {
                let section = pageFormat.sectionAtIndex(i);
                let questionCount = section.getNumQuestions();
                if (questionCount === 0) continue;
    
                if (section.getType() === 'MCSectionType') {
                    let hasLongAnswers = false;
                    for (let j = 0; j < questionCount; ++j) {
                        let dfQuestion = section.questionAtIndex(j);
                        let numAnswers = dfQuestion.getNumAnswers();
                        for (let answerNum = 0; answerNum < numAnswers; answerNum++)
                            if (dfQuestion.getAnswer(answerNum).length > 1) hasLongAnswers = true;
                    }
    
                    for (let j = 0; j < questionCount; ++j) {
                        let dfQuestion = section.questionAtIndex(j);
                        let numAnswers = dfQuestion.getNumAnswers();
                        let type = dfQuestion.getStringPropertyForName('questionType');
                        let choiceCount = dfQuestion.getIntPropertyForName('choiceCount', 4);
                        let maxAnswerSize = 1;
                        for (let answerNum = 0; answerNum < numAnswers; answerNum++) {
                            let answer = dfQuestion.getAnswer(answerNum);
                            if (answer.length > maxAnswerSize) maxAnswerSize = answer.length;
                        }
    
                        let darkThreshold = parameters.filledWeight;
    
                        let startIndex = brightnessIndex;

                        let sumBuffer = OMREngine.readPoints(brightnesses, brightnessIndex, choiceCount);
                        brightnessIndex += choiceCount;

                        if (sumBuffer[0].value !== false) {
                            sumBuffer.sort((a, b) => b.value - a.value);
    
                            if (numAnswers > 0) {
                                for (let s = 0; s < maxAnswerSize; ++s) {
                                    let margin = sumBuffer[s].value - darkThreshold;
                                    if ((margin > ambiguousMin) && (margin < ambiguousMax)) {
                                        messages.push(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                                    }
                                }
                            }
    
                            if (hasLongAnswers && (sumBuffer[0].value > darkThreshold)) {
                                let nextIndex;
                                for (nextIndex = 1; nextIndex < choiceCount; ++nextIndex)
                                    if ((sumBuffer[0].value - sumBuffer[nextIndex].value) / sumBuffer[0].value >= parameters.minNextDifference) break;
                                if (nextIndex < choiceCount) {
                                    let midpoint = sumBuffer[nextIndex].value * parameters.nextWeight + sumBuffer[0].value * (1.0 - parameters.nextWeight);
                                    if (midpoint > darkThreshold) darkThreshold = midpoint;
                                }
                            }
    
                            let responseString = "";
                            if (sumBuffer[0].value > darkThreshold) {
                                let rank = 0;
                                while (sumBuffer[rank].value > darkThreshold) {
                                    let thisLetter = (type === "Letter") ? String.fromCharCode('a'.charCodeAt(0) + sumBuffer[rank].index) : (sumBuffer[rank].index === 0) ? 't' : 'f';
                                    responseString += thisLetter;
                                    rank++;
    
                                    if (!hasLongAnswers) break;
                                    if (rank == choiceCount) break;
                                }
    
                                if ((rank < choiceCount) && (sumBuffer[rank].value > darkThreshold) &&
                                    (sumBuffer[rank].value > tooManyRatio * sumBuffer[0].value)) {
                                    messages.push(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex));
                                }
                            } else {
                                responseString = ' ';
                            }
    
                            responses.push(responseString);
                        } else responses.push(false);
                    }
                } else if (section.getType() === 'NRSectionType') {
                    let nrStyle = format.getStringPropertyForName('nrStyle', 'Default');
                    if (section.hasProperty('nrStyle')) nrStyle = section.getStringPropertyForName('nrStyle');
    
                    let nrRows = (nrStyle === "Fractions") ? 12 : 11;
    
                    for (let j = 0; j < questionCount; ++j) {
                        let dfQuestion = section.questionAtIndex(j);
                        let digitCount = dfQuestion.getIntPropertyForName('digitCount', 4);
    
                        let sumBuffers = [];
                        let isValid = true;
                        let startIndex = brightnessIndex;
                        for (let p = 0; p < digitCount; ++p) {
                            let numBubbles = nrRows;
                            if (nrStyle === "Negatives") {
                                if (p === digitCount - 1) numBubbles = nrRows - 1;
                            } else if (nrStyle === "Fractions") {
                                if (p === 0) numBubbles = nrRows - 1;
                                else if (p === digitCount - 1) numBubbles = nrRows - 2;
                            } else {
                                if ((p === 0) || (p === digitCount - 1)) numBubbles = nrRows - 1;
                            }
    
                            sumBuffers.push(OMREngine.readPoints(brightnesses, brightnessIndex, numBubbles));
                            brightnessIndex += numBubbles;

                            if (sumBuffers[p][0].value === false) isValid = false;
                            if (numBubbles < nrRows) {
                                for (let s = 0; s < sumBuffers[p].length; ++s)
                                    sumBuffers[p][s].index += nrRows - numBubbles;
                            }
                        }
                        if (isValid) {
                            let darkThreshold = parameters.filledWeight;
                            for (let p = 0; p < digitCount; ++p) {
                                sumBuffers[p].sort((a, b) => b.value - a.value);
                                if (sumBuffers[p][0].value > darkThreshold) darkThreshold = 0.0;
                            }
    
                            let responseString = "";
                            for (let p = 0; p < digitCount; ++p) {
                                let numBubbles = nrRows;
                                if (nrStyle === "Negatives") {
                                    if (p === digitCount - 1) numBubbles = nrRows - 1;
                                } else if (nrStyle === "Fractions") {
                                    if (p === 0) numBubbles = nrRows - 1;
                                    else if (p === digitCount - 1) numBubbles = nrRows - 2;
                                } else {
                                    if ((p === 0) || (p === digitCount - 1)) numBubbles = nrRows - 1;
                                }
    
                                if (sumBuffers[p][0].value > darkThreshold) {
                                    let nextIndex;
                                    for (nextIndex = 1; nextIndex < numBubbles; ++nextIndex)
                                        if ((sumBuffers[p][0].value - sumBuffers[p][nextIndex].value) / sumBuffers[p][0].value >= parameters.minNextDifference) break;
                                    if (nextIndex < numBubbles) {
                                        let midpoint = sumBuffers[p][nextIndex].value * parameters.nextWeight + sumBuffers[p][0].value * (1.0 - parameters.nextWeight);
                                        if (midpoint > darkThreshold) darkThreshold = midpoint;
                                    }
                                }
                            }
    
                            for (let p = 0; p < digitCount; ++p) {
                                let margin = sumBuffers[p][0].value - darkThreshold;
                                if ((margin > ambiguousMin) && (margin < ambiguousMax)) {
                                    messages.push(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                                }
    
                                if (sumBuffers[p][0].value > darkThreshold) {
                                    if ((sumBuffers[p][1].value > darkThreshold) &&
                                        (sumBuffers[p][1].value > tooManyRatio * sumBuffers[p][0].value)) {
                                        messages.push(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex));
                                    }
    
                                    let q = sumBuffers[p][0].index;
                                    let thisCharacter;
                                    if (nrStyle === "Negatives") {
                                        if ((q === 0) && (p === 0)) thisCharacter = '-';
                                        else if (q === 0) thisCharacter = '.';
                                        else thisCharacter = String.fromCharCode('0'.charCodeAt(0) + q - 1);
                                    } else if (nrStyle === "Fractions") {
                                        if (q === 0) thisCharacter = '/';
                                        else if ((q === 1) && (p === 0)) thisCharacter = '-';
                                        else if (q === 1) thisCharacter = '.';
                                        else thisCharacter = String.fromCharCode('0'.charCodeAt(0) + q - 2);
                                    } else {
                                        if (q === 0) thisCharacter = '.';
                                        else thisCharacter = String.fromCharCode('0'.charCodeAt(0) + q - 1);
                                    }
    
                                    responseString += thisCharacter;
                                } else {
                                    responseString += ' ';
                                }
                            }
    
                            responses.push(responseString);
                        } else responses.push(false);
                    }
                } else if (section.getType() === 'WRSectionType') {
                    let emptyResponses = new Set();
                    for (let j = 0; j < questionCount; ++j) {
                        let dfQuestion = section.questionAtIndex(j);
                        let responseString = "";
                        let numCriteria = dfQuestion.getCriteria().length;
                        let isValid = true;
                        let startIndex = brightnessIndex;
                        for (let p = 0; p < numCriteria; ++p) {
                            let maxScore = dfQuestion.getCriteria()[p].maxScore;
    
                            let sumBuffers = [];
                            let darkThreshold = parameters.filledWeight;
    
                            sumBuffers.push(OMREngine.readPoints(brightnesses, brightnessIndex, maxScore + 1));
                            brightnessIndex += maxScore + 1;

                            let halfValue = brightnesses[brightnessIndex++];
    
                            if (sumBuffers[0][0].value === false) isValid = false;
                            else {
                                sumBuffers[0].sort((a, b) => b.value - a.value);
    
                                sumBuffers.push([...sumBuffers[0], { index: -1, value: halfValue }]);
                                sumBuffers[1].sort((a, b) => b.value - a.value);
    
                                if (sumBuffers[1][0].value > darkThreshold) {
                                    let nextIndex;
                                    for (nextIndex = 1; nextIndex < sumBuffers[1].length; ++nextIndex)
                                        if ((sumBuffers[1][0].value - sumBuffers[1][nextIndex].value) / sumBuffers[1][0].value >= parameters.minNextDifference) break;
                                    if (nextIndex < sumBuffers[1].length) {
                                        let midpoint = sumBuffers[1][nextIndex].value * parameters.nextWeight + sumBuffers[1][0].value * (1.0 - parameters.nextWeight);
                                        if (midpoint > darkThreshold) darkThreshold = midpoint;
                                    }
                                }
    
                                let margin = sumBuffers[0][0].value - darkThreshold;
                                if ((margin > ambiguousMin) && (margin < ambiguousMax)) {
                                    messages.push(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                                }

                                if (sumBuffers[0][0].value > darkThreshold) {
                                    responseString += String.fromCharCode('0'.charCodeAt(0) + sumBuffers[0][0].index);
    
                                    if ((sumBuffers[0][1].value > darkThreshold) &&
                                        (sumBuffers[0][1].value > tooManyRatio * sumBuffers[0][0].value)) {
                                        messages.push(new MessageEntry(OMRMessage.OMRWarningExtraMarks, sectionIndex, startIndex));
                                    }
    
                                    let nextIndex;
                                    for (nextIndex = 1; nextIndex < maxScore + 1; ++nextIndex)
                                        if ((sumBuffers[0][0].value - sumBuffers[0][nextIndex].value) / sumBuffers[0][0].value >= parameters.minNextDifference) break;
                                    if (nextIndex < maxScore + 1) {
                                        let midpoint = sumBuffers[0][nextIndex].value * parameters.nextWeight + sumBuffers[0][0].value * (1.0 - parameters.nextWeight);
                                        if (midpoint > darkThreshold) darkThreshold = midpoint;
                                    }
                                } else {
                                    responseString += ' ';
                                }
                            }
    
                            if (halfValue === false) isValid = false;
                            else {
                                if (halfValue > darkThreshold) {
                                    let margin = halfValue - darkThreshold;
                                    if ((margin > ambiguousMin) && (margin < ambiguousMax)) {
                                        messages.push(new MessageEntry(OMRMessage.OMRWarningAmbiguousFill, sectionIndex, startIndex));
                                    }
                                    responseString += '+';
                                } else {
                                    responseString += ' ';
                                }
                            }
    
                            if (isValid && (sumBuffers[0][0].value <= darkThreshold) && (halfValue <= darkThreshold) && !dfQuestion.getIsOmitted())
                                emptyResponses.add(new MessageEntry(OMRMessage.OMRWarningMissingWRScore, sectionIndex, startIndex));
                        }
    
                        if (isValid) responses.push(responseString);
                        else responses.push(false);
                    }
                }
    
                sectionIndex++;
            }
        }

        return {
            responses: responses,
            studentData: studentData,
            messages: messages
        };
    }
    
}

export { scanDPI };
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { DFElement, getSimpleProperty } from './DFElement.js?7.0';

export class DFTitle extends DFElement {
	constructor(rootNode) {
		super();
        
		this.lines = [];
		this.nameLines = [];
		this.valid = true;

        rootNode.forEach(thisElement => {
            const property = getSimpleProperty(thisElement);
            if (property !== false) {
                if (property.key === 'text') {

                    this.lines.push(property.value);

                } else if (property.key === 'nameLines') {

                    this.nameLines = property.value.split(',').map(part => part.trim());

                } else {

                    this.setPropertyForName(property.value, property.key);

                }
            } else this.valid = false;
        });
	}

	getNumLines() {
		return this.lines.length;
	}

	lineAtIndex(index) {
		return index < this.lines.length ? this.lines[index] : '';
	}

	getLines() {
		return this.lines;
	}

	getNameLines() {
		return this.nameLines;
	}

	hasIDField() {
		return this.getStringPropertyForName('showIDField') === 'YES';
	}

	idDigits() {
		return this.hasIDField() ? this.getIntPropertyForName('idDigits', 5) : -1;
	}

	idLabel() {
		return this.hasIDField() ? this.getStringPropertyForName('idLabel', 'Student ID') : '';
	}

	hasNameField() {
		return this.getStringPropertyForName('showNameField') === 'YES';
	}

	nameLetters() {
		return this.hasNameField() ? this.getIntPropertyForName('nameLetters', 10) : -1;
	}

	nameLabel() {
		return this.hasNameField() ? this.getStringPropertyForName('nameLabel', 'Name') : '';
	}
}

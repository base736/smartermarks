/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { getDocument, GlobalWorkerOptions } from '/js/lib/pdfjs/pdf.min.mjs';
GlobalWorkerOptions.workerSrc = '/js/lib/pdfjs/pdf.worker.min.mjs';

import { CVImage } from './CVImage.js?7.0';
import { scanDPI, OMREngine } from './OMREngine.js?7.0';
import QRReader from './QRReader.js?7.0';

var pdfDoc = false;
var pdfDocPromise = false;

const renderCanvas = new OffscreenCanvas(0, 0);
const renderContext = renderCanvas.getContext('2d', { willReadFrequently: true });

class OffscreenCanvasFactory {
    create(width, height) {
        const canvas = new OffscreenCanvas(width, height);
        const context = canvas.getContext('2d');
        return { canvas, context };
    }

    reset(canvasAndContext, width, height) {
        canvasAndContext.canvas.width = width;
        canvasAndContext.canvas.height = height;
    }

    destroy(canvasAndContext) {
        canvasAndContext.canvas.width = 0;
        canvasAndContext.canvas.height = 0;
        canvasAndContext.canvas = null;
        canvasAndContext.context = null;
    }
}

const canvasFactory = new OffscreenCanvasFactory();

async function renderPage(pageNum) {

    const supportedFormats = ['qr_code'];
    let barcodeDetector = false;
    try {
        barcodeDetector = new BarcodeDetector({ formats: supportedFormats });
    } catch (e) {
        barcodeDetector = false;
    }

    try {
        
        if (!pdfDoc) {
            if (pdfDocPromise !== false) pdfDoc = await pdfDocPromise;
            else throw new Error("RenderWorker render requested before setArrayBuffer called");
        }
        
        const scanPageCount = await pdfDoc.numPages;
        if ((pageNum < 1) || (pageNum > scanPageCount)) {
            return {
                status: 'failed',
                pageNum: pageNum,
                error: 'Invalid page number'
            };
        }
    
        const qrOversampling = (barcodeDetector === false) ? 1 : 2;
        const renderDPI = qrOversampling * scanDPI;
    
        const page = await pdfDoc.getPage(pageNum);
        const scale = renderDPI / 72;
        const viewport = page.getViewport({ scale: scale });
        renderCanvas.height = viewport.height;
        renderCanvas.width = viewport.width;
        const renderOptions = {
            canvasContext: renderContext,
            canvasFactory: canvasFactory,
            viewport: viewport
        };
        await page.render(renderOptions).promise;
        
        const renderedPage = new CVImage();
        renderedPage.readFromContext(renderContext);
    
        let qrCodeDetails;
        if (barcodeDetector !== false) {
    
            // Read QR code using BarcodeDetector 
    
            const barcodes = await barcodeDetector.detect(renderCanvas);
    
            if (barcodes.length == 1) {
                qrCodeDetails = {
                    data: barcodes[0].rawValue,
                    location: {
                        topLeftCorner: barcodes[0].cornerPoints[0],
                        topRightCorner: barcodes[0].cornerPoints[1],
                        bottomLeftCorner: barcodes[0].cornerPoints[3]
                    }
                }
            } else qrCodeDetails = false;
    
            // Reduce and normalize page image after QR code is read
            
            renderedPage.reduceByFactor(qrOversampling);
            OMREngine.normalize(renderedPage);
    
        } else {
    
            // Reduce and normalize page image before QR code is read
    
            renderedPage.reduceByFactor(qrOversampling);
            OMREngine.normalize(renderedPage);
    
            // Read QR code using our reader
    
            let qrReader = new QRReader();
            qrCodeDetails = qrReader.readQRCode(renderedPage);  
            
        }
    
        // Rotate pages to properly align QR code if it exists
    
        let qrText;
        if (qrCodeDetails !== false) {
            qrText = qrCodeDetails.data;
    
            const topEdgeX = qrCodeDetails.location.topRightCorner.x - qrCodeDetails.location.topLeftCorner.x;
            const topEdgeY = qrCodeDetails.location.topRightCorner.y - qrCodeDetails.location.topLeftCorner.y;
            const leftEdgeX = qrCodeDetails.location.bottomLeftCorner.x - qrCodeDetails.location.topLeftCorner.x;
            const leftEdgeY = qrCodeDetails.location.bottomLeftCorner.y - qrCodeDetails.location.topLeftCorner.y;
    
            const averageY = topEdgeY - leftEdgeX;
            const averageX = topEdgeX + leftEdgeY;
            const angle = Math.atan2(averageY, averageX);
    
            if ((angle >= -Math.PI / 4) && (angle < Math.PI / 4)) {
                // Image is straight; do nothing
            } else if ((angle >= -3 * Math.PI / 4) && (angle < -Math.PI / 4)) {
                renderedPage.rotateRight();
            } else if ((angle >= Math.PI / 4) && (angle < 3 * Math.PI / 4)) {
                renderedPage.rotateLeft();
            } else {
                renderedPage.rotate180();
            }
        } else qrText = false;
    
        return {
            status: 'rendered',
            pageNum: pageNum,
            imageArrayBuffer: renderedPage.data.buffer,
            imageWidth: renderedPage.width,
            imageHeight: renderedPage.height,
            qr: qrText
        };

    } catch (error) {

        return {
            status: 'failed',
            pageNum: pageNum,
            error: error.message
        };

    }
}

// Handle messages from the main thread

addEventListener('message', async function(event) {
    const data = event.data;
    if (data.type === 'getPageCount') {

        if (pdfDocPromise !== false) pdfDoc = await pdfDocPromise;
        else throw new Error("RenderWorker render requested before getPageCount called");

        const scanPageCount = await pdfDoc.numPages;
        postMessage({
            type: 'pageCount',
            value: scanPageCount
        });

    } else if (data.type === 'setArrayBuffer') {

        const pdfArrayBuffer = new Uint8Array(data.pdfArrayBuffer);
        const loadingTask = getDocument({ 
            data: pdfArrayBuffer,
            canvasFactory: canvasFactory
        });

        loadingTask.onProgress = function (progressData) {
            if (progressData.total) {
                postMessage({
                    type: 'setArrayBufferProgress',
                    loaded: progressData.loaded,
                    total: progressData.total
                });
            }
        };

        pdfDocPromise = loadingTask.promise;

    } else if (data.type === 'renderRequest') {

        const responses = await renderPage(data.pageNum);
        const outData = { type: 'renderResponse', ...responses };

        if (responses.status === 'rendered') {
            postMessage(outData, [outData.imageArrayBuffer]);
        } else {
            postMessage(outData);
        }

    } else if (data.type === 'shutdown') {

        if (pdfDoc) {
            await pdfDoc.destroy();
            pdfDoc = false;
            pdfDocPromise = false;
        }

        postMessage({
            type: 'shutdownComplete'
        });

        self.close();

    }
});

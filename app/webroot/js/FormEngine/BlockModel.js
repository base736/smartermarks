/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class BlockModel {
    constructor(xGrid, yGrid) {

        // Grids contain an offset, which has a class and a value, and an array of indices, iteration 
        // over which generates the grid, and each of which has a class and an increment. "copyGrid"
        // does a deep copy of each grid.

        const copyGrid = (grid) => ({
            offset: { ...grid.offset },
            indices: grid.indices.map(item => ({ ...item }))
        });
    
        this.xGrid = copyGrid(xGrid);
        this.yGrid = copyGrid(yGrid);

        // "indices" contains the indices, applied to the grid above, that generate this block. Each
        // entry is an object including the keys "xIndices" and "yIndices" for a single bubble.

        this.indices = [];
        this.chunks = [];
    }

	copy() {
		const newBlockModel = new BlockModel(this.xGrid, this.yGrid);
		newBlockModel.indices = this.indices.map(index => ({
			xIndices: [ ...index.xIndices ],
			yIndices: [ ...index.yIndices ]
		}));
		newBlockModel.chunks = this.chunks.map(chunk => ({ ...chunk }));

		return newBlockModel;
	}

    static valueForIndices(indices, grid) {
		let value = grid.offset.value;
		for (let i = 0; i < grid.indices.length; i++) {
			value += indices[i] * grid.indices[i].increment;
		}
		return value;
	}

	getNewPoint() {
        return {
            xIndices: Array(this.xGrid.indices.length).fill(0),
            yIndices: Array(this.yGrid.indices.length).fill(0)
        };    
	}

	addPoint(newIndices) {
		this.indices.push(newIndices);
	}

	addChunk(numPoints, numFilled) {
		this.chunks.push({ numPoints, numFilled });
	}
}

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as math from '/js/lib/mathjs/mathjs.mjs';

import { BlockModel } from './BlockModel.js?7.0';

const maxIterations = 200;
const motionThreshold = 2.0;
const uScaledMaximum = 10.0;
const anglePressure = 1.0;

export class CGSolver {
    constructor(imagePoints) {
        this.sigma = 0;
        this.imagePoints = imagePoints;
        this.iiIntegral = 0;
        this.imIntegral = 0;
        this.mmIntegral = 0;
    }

    setSigma(value) {
        this.sigma = value;
        const scale = 1.0 / (4.0 * this.sigma * this.sigma);

        this.iiIntegral = 0.0;
        for (let i = 0; i < this.imagePoints.length; ++i) {
            this.iiIntegral += 1.0;
            for (let j = 0; j < i; ++j) {
                const delta = this.subtractPoints(this.imagePoints[i], this.imagePoints[j]);
                const u = this.dotPoints(delta, delta) * scale;
                const exponential = Math.exp(-u);
                this.iiIntegral += 2.0 * exponential * this.imagePoints[i].weight * this.imagePoints[j].weight;
            }
        }
        this.iiIntegral *= Math.PI * this.sigma * this.sigma;
    }

    readVectorFromModels(blockModels, angleX, angleY) {
        let numModelIndices = 0;
        blockModels.forEach(model => {
            numModelIndices += model.xGrid.indices.length + 1;
            numModelIndices += model.yGrid.indices.length + 1;
        });

        const vector = math.zeros(numModelIndices + 2);
        vector.set([0], angleX);
        vector.set([1], angleY);

        let parameterIndex = 2;
        blockModels.forEach(model => {
            vector.set([parameterIndex++], model.xGrid.offset.value);
            for (let j = 0; j < model.xGrid.indices.length; ++j) {
                vector.set([parameterIndex++], model.xGrid.indices[j].increment);
            }
            vector.set([parameterIndex++], model.yGrid.offset.value);
            for (let j = 0; j < model.yGrid.indices.length; ++j) {
                vector.set([parameterIndex++], model.yGrid.indices[j].increment);
            }
        });

        return vector;
    }

    writeVectorToModels(blockModels, vector) {
        let parameterIndex = 2;
        blockModels.forEach(model => {
            model.xGrid.offset.value = vector.get([parameterIndex++]);
            for (let j = 0; j < model.xGrid.indices.length; ++j) {
                model.xGrid.indices[j].increment = vector.get([parameterIndex++]);
            }
            model.yGrid.offset.value = vector.get([parameterIndex++]);
            for (let j = 0; j < model.yGrid.indices.length; ++j) {
                model.yGrid.indices[j].increment = vector.get([parameterIndex++]);
            }
        });
    }

    getModelPoints(blockModels, modelNum, parameters) {
        let angleX = parameters.get([0]);
        let angleY = parameters.get([1]);

        this.writeVectorToModels(blockModels, parameters);

        const cosX = Math.cos(angleX);
        const sinX = Math.sin(angleX);
        const cosY = Math.cos(angleY);
        const sinY = Math.sin(angleY);

        let pointReference = { 
            x: parameters.get([2]), 
            y: parameters.get([3 + blockModels[0].xGrid.indices.length])
        };

        let modelPoints = [];
        blockModels.forEach((model, j) => {
            if ((modelNum >= 0) && (j !== modelNum)) return;

            model.indices.forEach(point => {
                let xOffset = BlockModel.valueForIndices(point.xIndices, model.xGrid);
                let yOffset = BlockModel.valueForIndices(point.yIndices, model.yGrid);

                xOffset -= pointReference.x;
                yOffset -= pointReference.y;

                const modelPoint = {
                    x: xOffset * cosX - yOffset * sinY + pointReference.x,
                    y: xOffset * sinX + yOffset * cosY + pointReference.y
                };
                modelPoints.push(modelPoint);
            });
        });

        return modelPoints;
    }

    getMaxPointMotion(blockModels, parameters, deltaParameters) {
        const initPoints = this.getModelPoints(blockModels, -1, parameters);
        const finalPoints = this.getModelPoints(blockModels, -1, math.add(parameters, deltaParameters));

        let maxPointMotion = 0.0;
        initPoints.forEach((point, i) => {
            const deltaPoint = this.subtractPoints(finalPoints[i], point);
            const pointMotion = this.lengthPoint(deltaPoint);
            if (pointMotion > maxPointMotion) maxPointMotion = pointMotion;
        });
        return maxPointMotion;
    }

    optimize(parameters, blockModels, hessian, basis) {
        const basisTranspose = math.matrix(basis);
        const basisMatrix = math.transpose(basisTranspose);

        const projectedHessian = math.multiply(math.multiply(basisTranspose, hessian), basisMatrix);
        let mInverse = math.inv(projectedHessian);

        const n = basisMatrix.size()[1];

        let initParameters = parameters;
        let x = math.zeros(n);
        let objective, slope, rawSlope;
        [objective, rawSlope] = this.objective(blockModels, math.add(math.multiply(basisMatrix, x), initParameters));
        slope = math.multiply(basisTranspose, rawSlope);

        let d = math.zeros(n);
        let beta = 0.0;
        let numIterations = 0;
        let passCount = 0;
        let maxPointMotion;

        do {
            let r = slope;
            let s = math.multiply(mInverse, r);
            d = (beta === 0.0) ? s : math.add(s, math.multiply(beta, d));

            const deltaOld = math.dot(r, s);

            const rawRMS = this.getMaxPointMotion(blockModels, math.add(math.multiply(basisMatrix, x), initParameters), math.multiply(basisMatrix, d));
            if (rawRMS === 0.0) break;

            let maxStepSize = motionThreshold / rawRMS;
            let maxProjection, minStepSize = 0.0, minProjection = math.dot(slope, d);

            let projection;
            do {
                const stepX = math.add(x, math.multiply(maxStepSize, d));
                [objective, rawSlope] = this.objective(blockModels, math.add(math.multiply(basisMatrix, stepX), initParameters));
                slope = math.multiply(basisTranspose, rawSlope);
                projection = math.dot(slope, d);
                if (projection > 0.0) {
                    minStepSize = maxStepSize;
                    maxStepSize *= 2.0;
                    minProjection = projection;
                } else {
                    maxProjection = projection;
                }
            } while (projection > 0.0);

            const alpha = (minStepSize * maxProjection - maxStepSize * minProjection) / (maxProjection - minProjection);
            const deltaX = math.multiply(alpha, d);
            x = math.add(x, deltaX);

            maxPointMotion = this.getMaxPointMotion(blockModels, math.add(math.multiply(basisMatrix, x), initParameters), math.multiply(basisMatrix, deltaX));
            if (maxPointMotion < motionThreshold) passCount++;
            else passCount = 0;

            [objective, rawSlope] = this.objective(blockModels, math.add(math.multiply(basisMatrix, x), initParameters));
            slope = math.multiply(basisTranspose, rawSlope);

            let stepR = slope;
            const deltaMid = math.dot(stepR, s);
            s = math.multiply(mInverse, stepR);
            const deltaNew = math.dot(stepR, s);
            beta = (deltaNew - deltaMid) / deltaOld;

            if (beta < 0.0) beta = 0.0;
            else if (numIterations % 10 === 0) beta = 0.0;
            if (numIterations > maxIterations) break;

            numIterations++;
        } while (passCount < 3);

        parameters = math.add(math.multiply(basisMatrix, x), initParameters);

        return parameters;
    }

    objective(blockModels, parameters) {

        const scale = 1.0 / (4.0 * this.sigma * this.sigma);
        const numParameters = parameters.size()[0];

        const modelPoints = this.getModelPoints(blockModels, -1, parameters);

        const numImagePoints = this.imagePoints.length;
        const numModelPoints = modelPoints.length;

        const angleXIndex = 0;
        const angleYIndex = 1;
        const xOffsetIndex = 2;
        const yOffsetIndex = 3 + blockModels[0].xGrid.indices.length;

        const angleX = parameters.get([angleXIndex]);
        const cosX = Math.cos(angleX);
        const sinX = Math.sin(angleX);

        const angleY = parameters.get([angleYIndex]);
        const cosY = Math.cos(angleY);
        const sinY = Math.sin(angleY);

        const pointReference = { 
            x: parameters.get([xOffsetIndex]), 
            y: parameters.get([yOffsetIndex])
        };

        let minU = Infinity;
        for (let i = 0; i < numModelPoints; ++i) {
            for (let j = 0; j < numImagePoints; ++j) {
                const delta = this.subtractPoints(modelPoints[i], this.imagePoints[j]);
                const u = this.dotPoints(delta, delta) * scale;
                if (u < minU) minU = u;
            }
        }

        let scaled_imIntegral = 0.0;
        let scaled_imSlope = new Float32Array(numParameters);

        let parameterOffset = 2;
        let modelPointNum = 0;
        blockModels.forEach(model => {
            model.indices.forEach(point => {
                const xComponent = BlockModel.valueForIndices(point.xIndices, model.xGrid) - pointReference.x;
                const yComponent = BlockModel.valueForIndices(point.yIndices, model.yGrid) - pointReference.y;
                const modelPoint = modelPoints[modelPointNum++];

                let xSlope = 0.0;
                let ySlope = 0.0;

                for (let k = 0; k < this.imagePoints.length; ++k) {
                    const imagePoint = this.imagePoints[k]; 
                    const u = scale * (Math.pow(modelPoint.x - imagePoint.x, 2.0) + Math.pow(modelPoint.y - imagePoint.y, 2.0));
                    if (u > uScaledMaximum) continue;

                    const exponential = Math.exp(minU - u) * imagePoint.weight;

                    scaled_imIntegral += exponential;
                    xSlope -= (modelPoint.x - imagePoint.x) * exponential;
                    ySlope -= (modelPoint.y - imagePoint.y) * exponential;
                }
        
                xSlope *= Math.PI / 2.0;
                ySlope *= Math.PI / 2.0;

                const modelXSlope = cosX * xSlope + sinX * ySlope;
                const modelYSlope = -sinY * xSlope + cosY * ySlope;

                scaled_imSlope[angleXIndex] += xComponent * (-sinX * xSlope + cosX * ySlope);
                scaled_imSlope[angleYIndex] += yComponent * (-cosY * xSlope - sinY * ySlope);
                scaled_imSlope[xOffsetIndex] += xSlope;
                scaled_imSlope[yOffsetIndex] += ySlope;
                if (modelPointNum > 0) {
                    scaled_imSlope[xOffsetIndex] -= modelXSlope;
                    scaled_imSlope[yOffsetIndex] -= modelYSlope;
                }

                let parameterIndex = parameterOffset;
                if (modelPointNum > 0) scaled_imSlope[parameterIndex] += modelXSlope;
                parameterIndex++;
                for (let k = 0; k < model.xGrid.indices.length; ++k) {
                    scaled_imSlope[parameterIndex++] += point.xIndices[k] * modelXSlope;
                }

                if (modelPointNum > 0) scaled_imSlope[parameterIndex] += modelYSlope;
                parameterIndex++;
                for (let k = 0; k < model.yGrid.indices.length; ++k) {
                    scaled_imSlope[parameterIndex++] += point.yIndices[k] * modelYSlope;
                }
            });

            parameterOffset += model.xGrid.indices.length;
            parameterOffset += model.yGrid.indices.length;
            parameterOffset += 2;
        });

        scaled_imIntegral *= Math.PI * this.sigma * this.sigma;
        const log_imIntegral = -minU + Math.log(scaled_imIntegral);
        this.imIntegral = Math.exp(log_imIntegral);

        let mmIntegral = 0.0;
        let mmSlope = new Float32Array(numParameters);

        parameterOffset = 2;
        let aPointNum = 0;
        blockModels.forEach(model => {
            model.indices.forEach(point => {
                const xComponent = BlockModel.valueForIndices(point.xIndices, model.xGrid) - pointReference.x;
                const yComponent = BlockModel.valueForIndices(point.yIndices, model.yGrid) - pointReference.y;
                const modelPoint = modelPoints[aPointNum];

                let xSlope = 0.0;
                let ySlope = 0.0;

                for (let k = 0; k < modelPoints.length; ++k) {
                    const otherPoint = modelPoints[k];
                    const u = scale * (Math.pow(modelPoint.x - otherPoint.x, 2.0) + Math.pow(modelPoint.y - otherPoint.y, 2.0));
                    if (u > uScaledMaximum) continue;

                    var exponential = Math.exp(-u);

                    mmIntegral += exponential;
                    if (k !== aPointNum) exponential *= 2.0;
                    xSlope -= (modelPoint.x - otherPoint.x) * exponential;
                    ySlope -= (modelPoint.y - otherPoint.y) * exponential;
                }

                xSlope *= Math.PI / 2.0;
                ySlope *= Math.PI / 2.0;

                const modelXSlope = cosX * xSlope + sinX * ySlope;
                const modelYSlope = -sinY * xSlope + cosY * ySlope;

                mmSlope[angleXIndex] += xComponent * (-sinX * xSlope + cosX * ySlope);
                mmSlope[angleYIndex] += yComponent * (-cosY * xSlope - sinY * ySlope);

                let parameterIndex = parameterOffset;
                mmSlope[parameterIndex++] += modelXSlope;
                for (let k = 0; k < model.xGrid.indices.length; ++k) {
                    mmSlope[parameterIndex++] += point.xIndices[k] * modelXSlope;
                }

                mmSlope[parameterIndex++] += modelYSlope;
                for (let k = 0; k < model.yGrid.indices.length; ++k) {
                    mmSlope[parameterIndex++] += point.yIndices[k] * modelYSlope;
                }

                aPointNum++;
            });

            parameterOffset += model.xGrid.indices.length;
            parameterOffset += model.yGrid.indices.length;
            parameterOffset += 2;
        });

        mmIntegral *= Math.PI * this.sigma * this.sigma;

        let objective = log_imIntegral - 0.5 * Math.log(this.iiIntegral * mmIntegral);
        objective -= anglePressure * (angleX * angleX + angleY * angleY);

        let slope = math.zeros(numParameters);
        for (let i = 0; i < numParameters; ++i) {
            slope.set([i], scaled_imSlope[i] / scaled_imIntegral - 0.5 * mmSlope[i] / mmIntegral);
        }        
        slope.set([0], slope.get([0]) - 2.0 * anglePressure * angleX);
        slope.set([1], slope.get([1]) - 2.0 * anglePressure * angleY);

        return [objective, slope];
    }

    // Utility functions for Point2D operations

    subtractPoints(p1, p2) {
        return { x: p1.x - p2.x, y: p1.y - p2.y };
    }

    dotPoints(p1, p2) {
        return p1.x * p2.x + p1.y * p2.y;
    }

    lengthPoint(p) {
        return Math.sqrt(this.dotPoints(p, p));
    }
}

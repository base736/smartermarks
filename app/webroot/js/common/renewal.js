/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var stripe = null;
var paymentID = null;
var clientSecret = null;
var elements = null;
var card = null;

var bufferedResult = null;

var finishAddCallback = false;

var startPayment = function(purchase, callback) {

	ajaxFetch('/Payments/create', {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			purchase: purchase
		})
	}).then(async response => {
		const responseData = await response.json();
	
		if (responseData.success) {
            var nextStep = 'finish';

			if (responseData.needs_payment) {

                nextStep = 'getPayment';
				paymentID = responseData.paymentID;

				if ('clientSecret' in responseData) {
					clientSecret = responseData.clientSecret;
					$('#card_wrapper').show();
				} else {
					clientSecret = null;
					$('#card_wrapper').hide();
				}
	
				$('#invoice_currency').html(responseData.invoice.currency.toUpperCase());
	
				var invoiceHTML = "";
				var total = 0.0;
				for (var i = 0; i < responseData.invoice.line_items.length; ++i) {
					var thisLine = responseData.invoice.line_items[i];
					var thisAmount = thisLine.count * thisLine.price;
					total += thisAmount;
	
					invoiceHTML += "<tr>" +
						"<td>" + thisLine.description + "</td>" +
						"<td>" + thisLine.count.toFixed(2) + "</td>" + 
						"<td>$" + thisLine.price.toFixed(2) + "</td>" + 
						"<td>$" + thisAmount.toFixed(2) + "</td>" + 
						"</tr>";
				}
	
				var tax = total * responseData.invoice.tax_rate;
				total += tax;
	
				invoiceHTML += "<tr>" + 
					"<td colspan='3' style='text-align:right;'>" + responseData.invoice.tax_name + " (" +
						(responseData.invoice.tax_rate * 100).toFixed(0) + "%)</td>" +
					"<td>$" + tax.toFixed(2) + "</td>" + 
					"</tr>";
	
				invoiceHTML += "<tr>" +
					"<td colspan='3' style='text-align:right; font-weight:bold;'>Total</td>" +
					"<td>$" + (responseData.invoice.total / 100).toFixed(2) + "</td>" +
					"</tr>";
	
				$('.invoice_view').html(invoiceHTML);

			} else if ('prepay_used' in responseData) {
				
                paymentID = responseData.paymentID;
                nextStep = 'getConfirmation';

                $('#confirmation_prepaid').html(responseData.prepay_used + " renewal" + 
                    ((responseData.prepay_used == 1) ? '' : 's'));

            }

            callback(nextStep);

		} else {
			$.gritter.add({
				title: "Error",
				text: "Unable to process payment.",
				image: "/img/error.svg"
			});
		}

		$('.wait_icon').hide();
		$('.payment_button').prop('disabled', false);

	}).catch(function() {

		$('.wait_icon').hide();
		$('.payment_button').prop('disabled', false);

		$.gritter.add({
			title: "Error",
			text: "Unable to process payment.",
			image: "/img/error.svg"
		});
	});
}

var finishPayment = function() {
	var $dialog = $(this);
	
	showSubmitProgress($dialog);

	stripe.confirmCardPayment(clientSecret, {
		payment_method: {
			card: card
		}
	}).then(function(result) {
		if (result.error) {
			var $buttonPane = $('#payment_dialog').siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Pay now")').prop('disabled', false);
			hideSubmitProgress($dialog);

			$('#card_element').validationEngine('showPrompt', '* ' + result.error.message, 'error', 'bottomLeft', true);
		} else {
			window.onbeforeunload = null;
			window.location.href = '/Payments/finish/' + paymentID;
		}
	});
}

function initialize(callbacks = {}) {

    finishAddCallback = callbacks.finishAdd;

	stripe = Stripe(stripePublicKey);
	clientSecret = null;

	elements = stripe.elements();
	card = elements.create("card", {
		hidePostalCode: true,
		style: {
			base: {
				color: "#222",
				fontFamily: 'Arial',
				fontSmoothing: "antialiased",
				fontSize: "12px",
				"::placeholder": {
					color: "#888"
				}
			},
			invalid: {
				fontFamily: 'Arial',
				color: "#222",
				iconColor: "#fa755a"
			}
		}
	});
	card.mount("#card_element");

	card.on('change', function (event) {
		setTimeout(updatePayButton, 0); // To make sure the class is set before we call updatePayButton
	});

	var updatePayButton = function() {
		var newDisabled = (!$('#card_element').hasClass('StripeElement--complete')) || ($('#card_name').val().length == 0);
		$('#renewal_pay').prop('disabled', newDisabled);
	}

	$('#admin_confirmation_dialog').dialog({
        autoOpen: false,
		width: 425,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				$(this).dialog("close");
		 	},
			'Create': function() {
                showSubmitProgress($('#admin_confirmation_dialog'));
                window.onbeforeunload = null;
                window.location.href = '/Payments/finish_invoice/' + paymentID;        
            }
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Create")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

    $('#user_confirmation_dialog').dialog({
        autoOpen: false,
		width: 425,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				$(this).dialog("close");
		 	},
			'Finish': function() {
                showSubmitProgress($('#user_confirmation_dialog'));
                window.onbeforeunload = null;
                window.location.href = '/Payments/finish/' + paymentID;        
            }
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Finish")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$('#payment_dialog').dialog({
        autoOpen: false,
		width: 425,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			$('#card_name').val('');
			card.clear();

			initSubmitProgress($(this));
	        $('#renew_buttons').find('button').blur();
			updatePayButton();
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$('#renewal_cancel').on('click', function() {
		$('#payment_dialog').dialog('close');
	});

	$('#renewal_pay').on('click', function() {
		var contextedPayment = $.proxy(finishPayment, $('#payment_dialog'));
		contextedPayment();
	});

	function getEmailArray(email_list) {
		var email_array;
		if (email_list.length > 0) {
			email_array = email_list.split(/[,;\s]+/);
			if (email_array[email_array.length - 1].length == 0) {
				email_array.pop();
			}
		} else email_array = [];
		return email_array;
	}

	var processUserDetails = function(response) {
		hideSubmitProgress($('#addUsers_dialog'));

		var finishImmediately = true;

		if ('student' in response) {
			finishImmediately = false;

			var newHTML = '';
			for (var i = 0; i < response.student.length; ++i) {
				newHTML += '<div>' + response.student[i] + '</div>';
			}
			$('#bad_email_list').html(newHTML);
			$('#studentEmails_dialog').dialog('open');	

		} else if ('no_user' in response) {
            finishImmediately = false;

            var newHTML = '';
            for (var i = 0; i < response.no_user.length; ++i) {
                newHTML += '<tr>';
                newHTML += '<td><input class="new_email_name validate[required] defaultTextBox2" style="width:170px; margin:0px;" type="text" /></td>';
                newHTML += '<td class="new_email_email">' + response.no_user[i] + '</td>';
                newHTML += '</tr>';
            }
            $('#new_email_rows').html(newHTML);

            $('#new_email_rows tr').each(function() {
                var emailParts = $(this).find('.new_email_email').text().split('@');
                var nameParts = emailParts[0].split('.');
                if (nameParts.length > 1) {
                    for (var i = 0; i < nameParts.length; ++i) {
                        nameParts[i] = nameParts[i].charAt(0).toUpperCase() + nameParts[i].substring(1);									
                    }
                    $(this).find('.new_email_name').val(nameParts.join(' '));
                }
            });

            $('#addAccounts_dialog').dialog('open');		

		}

		if (finishImmediately) {
			if (finishAddCallback !== false) finishAddCallback(bufferedResult);
			$('#addAccounts_dialog').dialog('close');
			$('#addUsers_dialog').dialog('close');
		}
	}

	var getUserDetails = function() {
		var $dialog = $(this);

        var emailRegex = /^[^@]+@([^.@]+\.)+[^.@]+$/;

        var invalidEmail = false;
		var enteredEmails = getEmailArray($('#add_email_list').val());
		var uniqueEmails = [];
		for (var i = 0; i < enteredEmails.length; ++i) {
            if (!enteredEmails[i].match(emailRegex)) {
                invalidEmail = enteredEmails[i];
            } else if (uniqueEmails.indexOf(enteredEmails[i]) == -1) {
				uniqueEmails.push(enteredEmails[i]);				
			}
		}

        if (invalidEmail !== false) {

            $('#add_email_list').validationEngine('showPrompt', '* Invalid email address ' + invalidEmail, 'error', 'bottomLeft', true);

        } else {

            if (pendingAJAXPost) return false;
            showSubmitProgress($dialog);
            pendingAJAXPost = true;
    
			ajaxFetch('/Users/get_renewal_details', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					emails: uniqueEmails
				})
			}).then(async response => {

				pendingAJAXPost = false;
				const responseData = await response.json();
    
                if (responseData.success) {
                    bufferedResult = responseData;
                    processUserDetails(bufferedResult);
                } else showSubmitError($dialog);

            }).catch(function() {
                showSubmitError($dialog);
            });

        }
	}

	$('#addUsers_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Add': getUserDetails
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Add")').addClass('btn btn-save btn_font');
		  	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});
	
	$('#studentEmails_dialog').dialog({
        autoOpen: false,
		width: 500,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Go back': function() {
				$(this).dialog('close');
				$('#addUsers_dialog').dialog('open');
			},
			'Skip': function() {
				$(this).dialog('close');

				delete bufferedResult.student;
				processUserDetails(bufferedResult);
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Go back")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Skip")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button').blur();
		  	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

    var addAccounts = function() {
		var $dialog = $('#addAccounts_dialog');
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')){
            var new_users = [];
            $('#new_email_rows tr').each(function() {
                new_users.push({
                    name: $(this).find('.new_email_name').val(),
                    email: $(this).find('.new_email_email').text()
                });
            });

            bufferedResult.new_users = new_users;
            delete bufferedResult.no_user;

            processUserDetails(bufferedResult);
        }
    }

	$('#addAccounts_dialog').dialog({
        autoOpen: false,
		width: 500,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Go back': function() {
				$(this).dialog('close');
				$('#addUsers_dialog').dialog('open');
			},
			'Continue': addAccounts
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Go back")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Continue")').addClass('btn btn-save btn_font');
			$buttonPane.find('button').blur();
		  	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$('#addUsers_button').on('click', function() {
		$('#add_email_list').val('');
		$('#add_email_checklist').find('input').prop('checked', false);
		bufferedResult = null;

		$('#addUsers_dialog').dialog('open');
		$('#add_email_list').focus().trigger('input');
	});

	$('#add_email_list').on('input', function() {
		var isEmpty = ($('#add_email_list').val().length == 0);
		var $buttonPane = $('#addUsers_dialog').siblings('.ui-dialog-buttonpane');
		$buttonPane.find('button:contains("Add")').prop('disabled', isEmpty);
	});

	$(document).on('click', '.add_email_checkbox', function(event) {
		event.stopPropagation();
		var email = $(this).data('email');
		
		var email_array = getEmailArray($('#add_email_list').val());
		
		var index = $.inArray(email, email_array);
		if ($(this).prop('checked')) {
			if (index == -1) email_array.push(email);
		} else if (index > -1) email_array.splice(index, 1);
				
		$('#add_email_list').val(email_array.join("\n")).trigger('input');
	});
	
	$('#add_selection_type').on('click', function(e) {
		e.preventDefault();

		$('#add_email_list').validationEngine('hideAll');

		if ($('#add_email_list').is(':visible')) {
			$('#add_selection_type').html("Enter emails");
			
			var email_array = getEmailArray($('#add_email_list').val());

			$('.add_email_checkbox').each(function() {
				var email = $(this).data('email');
				var index = $.inArray(email, email_array);
				$(this).prop('checked', (index > -1));
			});
			
			$('#add_email_list').hide();
			$('#add_email_checklist').show().focus();
		} else {
			$('#add_selection_type').html("Choose from list");
			$('#add_email_checklist').hide();
			$('#add_email_list').show().focus();
		}
	});	
	
	$('.intSpinner').spinner({
		step: 1,
		min: 1
	});

    $('form').validationEngine({
        validationEventTrigger: 'submit',
        promptPosition : "bottomLeft",
       focusFirstField: false
   }).submit(function(e){ e.preventDefault() });
}

export { initialize, startPayment };
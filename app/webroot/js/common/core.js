var Core = new function() {
    
    this.FormatNone = -1;
    this.FormatFloat = 0;
    this.FormatScientific = 1;

    this.FormatScientificE = 2;

    this._epsilon = null;

    this.getEpsilon = function() {
        if (this._epsilon === null) {
            this._epsilon = 1.0;
            while ((1.0 + this._epsilon) !== 1.0) {
                this._epsilon *= 0.5;
            }
            this._epsilon *= 16; // Leave a bit of room
        }
        return this._epsilon;
    };

    this.postNotification = function(message) {
        throw {
            message: message
        };
    };

    // Sine function, taking an angle in degrees

    this.sd = function(angle) {
        return Math.sin(angle*Math.PI/180);
    };

    // Cosine function, taking an angle in degrees

    this.cd = function(angle) {
        return Math.cos(angle*Math.PI/180);
    };

    // Tangent function, taking an angle in degrees

    this.td = function(angle) {
        return Math.tan(angle*Math.PI/180);
    };

    // Inverse sine function, returning an angle in degrees

    this.as = function(value) {
        if ((value > 1.0) && (value < 1.0 + this.getEpsilon())) value = 1.0;
        else if ((value < -1.0) && (value > -1.0 - this.getEpsilon())) value = -1.0;
        if ((value < -1.0) || (value > 1.0)) {
            throw {message: "Called asin(x) with x<-1 or x>1"};
        }
        return Math.asin(value)*180/Math.PI;
    };

    // Inverse cosine function, returning an angle in degrees

    this.ac = function(value) {
        if ((value > 1.0) && (value < 1.0 + this.getEpsilon())) value = 1.0;
        else if ((value < -1.0) && (value > -1.0 - this.getEpsilon())) value = -1.0;
        if ((value < -1.0) || (value > 1.0)) {
            throw {message: "Called acos(x) with x<-1 or x>1"};
        }
        return Math.acos(value)*180/Math.PI;
    };

    // Inverse tangent function, returning an angle in degrees

    this.at = function(value) {
        return Math.atan(value)*180/Math.PI;
    };

    // Exponentiation, returning a^b

    this.pw = function(a, b) {
        if ((a < 0.0) && (a > -this.getEpsilon()) && (b < 0.0)) a = 0.0;
        if ((a < 0.0) && (b != Math.floor(b))) {
            throw {message: "Exponent with negative base"};
        } else if ((a == 0.0) && (b == 0)) {
            throw {message: "Cannot evaluate 0^0"};
        }
        return Math.pow(a, b);
    };

    // Square root

    this.sr = function(value) {
        if ((value < 0.0) && (value > -this.getEpsilon())) value = 0.0;
        if (value < 0.0) {
            throw {message: "Square root with negative argument"};
        }
        return Math.sqrt(value);
    };

    // Natural logarithm

    this.l1 = function(value) {
        if (value <= 0.0) {
            throw {message: "Logarithm with negative argument"};
        }
        return Math.log(value);
    };

    // Logarithm base 10

    this.l2 = function(value) {
        if (value <= 0) {
            throw {message: "Logarithm with negative argument"};
        }
        return Math.log(value) / Math.LN10;
    };

    // Factorial

    this.fc = function(value) {
        if (!Number.isInteger(value)) {
            throw {message: "Factorial with non-integer argument"};
        } else if (value < 0) {
            throw {message: "Factorial with negative argument"};
        } else {
            var result = 1;
            for (var i = 1; i <= value; ++i) result *= i;
            return result;
        }
    };

    // Returns a string for the given value and format, to the requested number of significant digits

    this.stringForValueFormat = function(value, sigDigs, format) {

        if (sigDigs <= 0) {
            throw {message: "Significant digits must be greater than zero"};
        } else {
            if (value === 0.0) return value.toFixed(sigDigs - 1);
            if (isNaN(value)) return 'NaN';
        
	        var maxFraction = Math.log(10 - 5.0 * Math.pow(10.0, -sigDigs)) / Math.LN10;
            var logValue = Math.log(Math.abs(value)) / Math.LN10 + this.getEpsilon();
            var exponent = Math.floor(logValue);
            if (logValue - exponent >= maxFraction) exponent++;

            var precision = sigDigs - exponent - 1;
            var divisor = Math.pow(10.0, -precision);
            value = Math.sign(value) * Math.pow(10.0, logValue);

            if (format == this.FormatNone) {
                if ((precision < 0) || (exponent < -2)) format = this.FormatScientific;
                else format = this.FormatFloat;
            }
        
            if (format == this.FormatFloat) {
                return value.toFixed(precision);
            } else if (format == this.FormatScientific) {
                value = value * Math.pow(10.0, -exponent);
                return value.toFixed(sigDigs - 1) + "&times;10<sup>" + exponent + "</sup>";
            } else if (format == this.FormatScientificE) {
                value = value * Math.pow(10.0, -exponent);
                return value.toFixed(sigDigs - 1) + "e" + exponent;
            }
        }
    };

    // Return the given value as a string, using decimal or scientific notation as required

    this.sv = function(value, sigDigs, sdType) {
		sdType = (typeof sdType !== 'undefined') ? sdType : 'SigDigs';
		if (sdType == 'Scientific') {
            return this.stringForValueFormat(value, sigDigs, this.FormatScientific);
        } else if (sdType == 'Decimals') {
            return value.toFixed(sigDigs);
        } else return this.stringForValueFormat(value, sigDigs, this.FormatNone);
    };

    // Return the given value as a decimal string

    this.fv = function(value, sigDigs) {
		return this.stringForValueFormat(value, sigDigs, this.FormatFloat);  
    };

    // Return the given value as a string in scientific notation

    this.cv = function(value, sigDigs) {
		return this.stringForValueFormat(value, sigDigs, this.FormatScientific);  
    };

    // Return the given value as a fraction

    this.rv = function(value) {
        var error = this.getEpsilon() * 1.0e3;

        var signSymbol;
        if (value < 0.0) {
            value = Math.abs(value);
            signSymbol = '-';
        } else signSymbol = '';

        var n = Math.floor(value);
        value -= n;

        if (value < error) return signSymbol + n;
        else if (1.0 - error < value) return signSymbol + (n + 1);

        // Start lower fraction at 0/1

        var lower_n = 0;
        var lower_d = 1;

        // Start upper fraction at 1/1

        var upper_n = 1;
        var upper_d = 1;

        for (var i = 0; i < 50; ++i) {
            var middle_n = lower_n + upper_n;
            var middle_d = lower_d + upper_d;
            if (middle_d * (value + error) < middle_n) {
                upper_n = middle_n;
                upper_d = middle_d;
            } else if (middle_n < (value - error) * middle_d) {
                lower_n = middle_n;
                lower_d = middle_d;
            } else break;
        }

        return signSymbol + (n * middle_d + middle_n) + '/' + middle_d;
    };

    // Return the numerical value of the given string

    this.vs = function(string) {
        var fractionRegex = /^-?([1-9]\d*)\/\d+$/;
        if (fractionRegex.test(string)) {
            var parts = string.split('/');
            if (parts.length == 2) return parseFloat(parts[0]) / parseFloat(parts[1]);
            else return NaN;
        } else {
            var stringCopy = string.replace("&times;10<sup>", "E");
            stringCopy = stringCopy.replace("</sup>", "");
            return parseFloat(stringCopy);    
        }
    };

    // Render variables of the given name to a value

    this.st = function(context, name, value) {

        $(context).find('.block_variable[data-variable-name="' + name + '"]').html(value).addClass('rendered');

        // To support hardcoded questions
        $(context).find('.block_variable[data-name="' + name + '"]').html(value).addClass('rendered');

        // Replace '-' with unicode 2212 in equations. Doing this earlier breaks checks for negative NR answers, for example.
        value = value.replace('-', '−');

        var findRegex = new RegExp('\\{"type":"Variable",([^\\}]*?)"value":"'+ name + '"(.*?)\\}','g');
        var replaceText = '{"type":"Variable","displayValue":"' + value + '","value":"' + name + '"}';
        $(context).find('.equation').each(function() {
			var jsonText = $(this).attr('data-json');
			if (jsonText.length > 0) {
				var newText = jsonText.replace(findRegex, replaceText);
				if (newText != jsonText) $(this).attr('data-json', newText);
			}
        });
        
        return value;
    };

    // Return a random integer from 0 to count - 1

    this.randomIndex = function(count) {
        return Math.floor(Math.random() * count);
    };
    
    // Return a random number between min and max inclusive, with the given step size

    this.rn = function(min, max, step) {
        var steps = Math.round((max - min) / step);
        var int = this.randomIndex(steps + 1);
        return int * step + min;
    };

    // Return a random value from a list

    this.rl = function(list) {
        var index = this.randomIndex(list.length);
        return list[index];
    };
};

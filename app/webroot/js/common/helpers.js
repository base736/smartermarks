/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

function navigateWithPost(url, data) {
	// Create a form element
    const form = document.createElement('form');
    form.method = 'POST';
    form.action = url;
    
    // Let the form submission open a new window automatically.
    form.target = '_blank';

    // Add data as hidden input fields
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            const input = document.createElement('input');
            input.type = 'hidden';
            input.name = key;
            input.value = data[key];
            form.appendChild(input);
        }
    }

    // Append the form to the body, submit it, then remove it
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function checkES6Support() {
    let hasImport = false;

	// Test ES6 `import` dynamically

	try {
        new Function('import("")');
        hasImport = true;
    } catch (e) {
        hasImport = false;
    }

	Cookies.set('has_import', hasImport ? 'true' : 'false', { expires: 7 });
}

var usedIDs = [];
function getNewID(length) {
	var idChars = "0123456789abcdef";
	var newID;
	do {
		newID = "";
		for (var i = 0; i < 2 * length; ++i) {
			newID += idChars.charAt(Math.floor(Math.random() * idChars.length));
		}
	} while (usedIDs.indexOf(newID) >= 0);
	usedIDs.push(newID);
	return newID;
}

function uniqid(prefix, more_entropy) {
	
	//  discuss at: http://phpjs.org/functions/uniqid/
	//  original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//  revised by: Kankrelune (http://www.webfaktory.info/)
	//        note: Uses an internal counter (in php_js global) to avoid collision
	//        test: skip
	//  example 1: uniqid();
	//  returns 1: 'a30285b160c14'
	//  example 2: uniqid('foo');
	//  returns 2: 'fooa30285b1cd361'
	//  example 3: uniqid('bar', true);
	//  returns 3: 'bara20285b23dfd1.31879087'

	if (typeof prefix === 'undefined') {
		prefix = '';
	}

	var retId;
	var formatSeed = function (seed, reqWidth) {
		seed = parseInt(seed, 10).toString(16); // to hex str
		if (reqWidth < seed.length) { // so long we split
			return seed.slice(seed.length - reqWidth);
		}
		if (reqWidth > seed.length) { // so short we pad
			return Array(1 + (reqWidth - seed.length)).join('0') + seed;
		}
		return seed;
	};

	if (!this.php_js) this.php_js = {};

	if (!this.php_js.uniqidSeed) {
		// init seed with big random int
		this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
	}
	this.php_js.uniqidSeed++;

	// start with prefix, add current milliseconds hex string
	retId = prefix;
	retId += formatSeed(parseInt(new Date().getTime() / 1000, 10), 8);
	// add seed hex string
	retId += formatSeed(this.php_js.uniqidSeed, 5);
	if (more_entropy) {
		// for more entropy we add a float lower to 10
		retId += (Math.random() * 10).toFixed(8).toString();
	}

	return retId;
}

// Functions to manage query parameters in URL

function getUrlVars() {
    var vars = {};
    if (window.location.href.indexOf('?') != -1) {
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for (var i = 0; i < hashes.length; i++)
	    {
	        var hash = hashes[i].split('=');
			if (hash.length == 2) {
				vars[hash[0]] = decodeURIComponent(hash[1].replace(/\+/g, '%20'));
			}
	    }
    }
    return vars;
}

function setUrlVars(urlVars) {
	var newURL = window.location.origin + window.location.pathname;

	var newQuery = jQuery.param(urlVars);
	if (newQuery.length > 0) newURL += '?' + newQuery;

	window.history.replaceState({}, '', newURL);
}

// Functions to replace single quotes with an HTML entity

function escapeHTML(string)
{
    var pre = document.createElement('pre');
    var text = document.createTextNode(string);
    pre.appendChild(text);
    return pre.innerHTML;
}

function htmlSingleQuotes(string) {
	return string.replace(/'/g, '&#39;');
}

// Function for copying text to clipboard

var copyText = function(text, type) {
	if ('clipboard' in navigator) {
		navigator.clipboard.writeText(text)
		.then(function() {
			$.gritter.add({
				title: "Success",
				text: "Copied " + type + ".",
				image: "/img/success.svg"
			});
		}).catch(function(err) {
			$.gritter.add({
				title: "Error",
				text: "Unable to copy " + type + ".",
				image: "/img/error.svg"
			});
		});	
	} else {
		$.gritter.add({
			title: "Error",
			text: "Unable to copy " + type + ".",
			image: "/img/error.svg"
		});
	}	
}

// Functions to add and display flash from the client

var addClientFlash = function(newEntry) {
	var flashEntries = [];
	var cookieValue = Cookies.get('client_flash');
	if (typeof cookieValue !== 'undefined') {
		flashEntries = JSON.parse(cookieValue);
	}

	flashEntries.push(newEntry);
	Cookies.set('client_flash', JSON.stringify(flashEntries));
}

var showClientFlash = function() {
	var flashEntries = [];
	var cookieValue = Cookies.get('client_flash');
	if (typeof cookieValue !== 'undefined') {
		flashEntries = JSON.parse(cookieValue);
		Cookies.remove('client_flash');
	}

	for (var i = 0; i < flashEntries.length; ++i) {
		$.gritter.add(flashEntries[i]);		
	}
}

// Functions for managing submit progress indicator and dialog buttons

var showSubmitProgress = function($dialog) {
	var $buttonPane = $dialog.siblings('.ui-dialog-buttonpane');
	if ($buttonPane.length == 0) {
		if ($dialog.is('.custom-buttonpane')) $buttonPane = $dialog;
		else $buttonPane = $dialog.find('.custom-buttonpane');
	}
	$buttonPane.find('button').prop('disabled', true);
	$buttonPane.find('.wait_icon').show();
}

var hideSubmitProgress = function($dialog) {
	var $buttonPane = $dialog.siblings('.ui-dialog-buttonpane');
	if ($buttonPane.length == 0) {
		if ($dialog.is('.custom-buttonpane')) $buttonPane = $dialog;
		else $buttonPane = $dialog.find('.custom-buttonpane');
	}
	$buttonPane.find('button').prop('disabled', false);
	$buttonPane.find('.wait_icon').hide();
}

var initSubmitProgress = function($dialog) {
	var $buttonPane = $dialog.siblings('.ui-dialog-buttonpane');
	if ($buttonPane.length == 0) {
		if ($dialog.is('.custom-buttonpane')) $buttonPane = $dialog;
		else $buttonPane = $dialog.find('.custom-buttonpane');
	}
	if ($buttonPane.find('.wait_icon').length == 0) {
		$buttonPane.find('button, .btn-group').first().before("<div class='wait_icon'></div>");
	}
	$buttonPane.find('button').prop('disabled', false);
	$buttonPane.find('.wait_icon').hide();
}

// Turn an array of numbers into a string representation

function numbersToList(numbers) {
    var list = '';
    if (numbers.length == 0) return list;

    // Sort numerically

    numbers = numbers.sort((a, b) => (a - b));

    // Create a string representation of this list

    var startNumber = numbers[0];
    for (var i = 1; i <= numbers.length; ++i) {
        if ((i == numbers.length) || (numbers[i] != numbers[i - 1] + 1)) {
            var endNumber = numbers[i - 1];

            if (list.length > 0) list += ', ';
            if (endNumber == startNumber) {
                list += startNumber;
            } else if (endNumber == startNumber + 1) {
                list += startNumber + ', ' + endNumber;
            } else {
                list += startNumber + '&ndash;' + endNumber;
            }

            if (i < numbers.length) startNumber = numbers[i];
        }
    }

    return list;
}

// Show submit error and reset submit progress on dialog

var showSubmitError = function($target = null) {
	$.gritter.add({
		title: "Error",
		text: "There was a problem processing your request.",
		image: "/img/error.svg"
	});
	if ($target !== null) hideSubmitProgress($target);
}

// Custom fetch to set AJAX header and redirect to login if needed

async function ajaxFetch(url, options = {}) {
	options.headers = options.headers || {};
    options.headers['X-Requested-With'] = 'XMLHttpRequest';
	const response = await fetch(url, options);
	if (response.status == 403) redirectFailedAJAX();
	else return response;
}
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var isItemFolder = function(treeData, node_id) {
	var ancestors = getAncestors(treeData, node_id);

	if (ancestors[ancestors.length - 1].text != 'Communities') return true;

	for (var i = 0; i < ancestors.length; ++i) {
		if (ancestors[i].type == 'community') return true;
	}
	return false;
}

function getNode(treeData, node_id) {
	for (var i = 0; i < treeData.length; ++i) {
		if (treeData[i].id == node_id) {
			return treeData[i];
		} else if ('children' in treeData[i]) {
			var childValue = getNode(treeData[i].children, node_id);
			if (childValue) return childValue;
		}
	}
	return false;
}

function getNodeId(treeData, folder_id) {
	for (var i = 0; i < treeData.length; ++i) {
		if (treeData[i].data.folder_id == folder_id) {
			return treeData[i].id;
		} else if ('children' in treeData[i]) {
			var childValue = getNodeId(treeData[i].children, folder_id);
			if (childValue) return childValue;
		}
	}
	return false;
}

var getDescendants = function(node) {
	var descendants = [node];
	if ('children' in node) {
		for (var i = 0; i < node.children.length; ++i) {
			descendants = descendants.concat(getDescendants(node.children[i]));
		}
	}
	return descendants;
}

function getAncestors(branchData, node_id) {
	for (var i = 0; i < branchData.length; ++i) {
		if (branchData[i].id == node_id) {
			return [branchData[i]];
		} else if ('children' in branchData[i]) {
			var result = getAncestors(branchData[i].children, node_id);
			if (result !== false) {
				result.push(branchData[i]);
				return result;
			}
		}
	}
	
	return false;
}

function stopClick(event) {
	event.stopPropagation();
	return false;
}

function hasEnabledNodes($parent) {
	var hasEnabled = false;
	$parent.find('.jstree-node').each(function() {
		if (!($parent.jstree().is_disabled(this.id))) hasEnabled = true;
	});
	return hasEnabled;
}

function disableNodes($parent, $nodes) {
	$nodes.each(function() {
		$parent.jstree().disable_node(this.id);
		$(this).children('.jstree-anchor').addClass('disabled-link');
		$(this).children('.jstree-ocl').each(function () {
			$(this).unbind('click', stopClick).on('click', stopClick);
		});
	});
}

function disableAll($parent) {
	disableNodes($parent, $parent.find('.jstree-node'));
}

function disableSubtree($parent, node_id) {
	for (var i = 0; i < $parent.length; ++i) {
		var prefix = $parent.eq(i).data('prefix');
		disableNodes($parent.eq(i), $('#' + prefix + node_id).find('.jstree-node').addBack());
	}
}

function disableConditional($parent, nodes, condition) {
	for (var i = 0; i < nodes.length; ++i) {
		if (condition(nodes[i].data)) disableSubtree($parent, nodes[i].id);
		else if ('children' in nodes[i]) disableConditional($parent, nodes[i].children, condition);
	}
}

function enableNodes($parent, $nodes) {
	$nodes.each(function() {
		$parent.jstree().enable_node(this.id);
		$(this).children('.jstree-anchor').removeClass('disabled-link');
		$(this).children('.jstree-ocl').each(function () {
			$(this).unbind('click', stopClick);
		});
	});
}

function enableAll($parent) {
	enableNodes($parent, $parent.find('.jstree-node'));
}

function enableSubtree($parent, node_id) {
	for (var i = 0; i < $parent.length; ++i) {
		var prefix = $parent.eq(i).data('prefix');
		enableNodes($parent.eq(i), $('#' + prefix + node_id).find('.jstree-node').addBack());
	}
}

function enableConditional($parent, nodes, condition) {
	for (var i = 0; i < nodes.length; ++i) {
		if (condition(nodes[i].data)) enableSubtree($parent, nodes[i].id);
		else if ('children' in nodes[i]) enableConditional($parent, nodes[i].children, condition);
	}
}

function getSelectedNode(treeID) {
	return $("#" + treeID).jstree('get_json', $("#" + treeID).jstree('get_selected'));
}

function specializeTree(data, prefix) {
	for (var i = 0; i < data.length; ++i) {
		data[i].state = { 'opened':true };
		data[i].data.raw_id = data[i].id;
		data[i].id = prefix + data[i].id;
		if ('children' in data[i]) specializeTree(data[i].children, prefix);
	}
}

function openToNode($tree, node_id) {
	var selectId = $tree.data('prefix') + node_id;
	$tree.jstree('close_all');
	$tree.jstree('open_node', selectId);
	$tree.jstree('deselect_all');
	$tree.jstree('select_node', selectId);
}

function initFolderTree($tree, jsonData) {
	var rootOrder = { 'Home':1, 'Archive':2, 'Communities':3, 'Open bank':4, 'Trash':5 };

	var thisTree = $.extend(true, [], jsonData);
	specializeTree(thisTree, $tree.data('prefix'));
	$tree.jstree({
		core: {
			animation: 0,
			multiple: false,
			expand_selected_onload: true,
			themes: { 
				url: "/css/themes/default/style.css",
				dots: false
			},
			data: thisTree
		},
		sort: function (a, b) {
			if (this.get_parent(a) == '#') {
				return rootOrder[this.get_text(a)] > rootOrder[this.get_text(b)] ? 1 : -1;
			} else {
				return this.get_text(a).toUpperCase() > this.get_text(b).toUpperCase() ? 1 : -1;
			}
		},
		types: {
			folder: {
				icon: "/img/icons/folder.svg"
			},
			community: {
				icon: "/img/icons/community.svg"
			},
			open: {
				icon: "/img/icons/globe.svg"
			},
			trash: {
				icon: "/img/icons/trash.svg"
			}
		},
		plugins: [ "wholerow", "sort", "types" ]
	});
}

export { initFolderTree, 
    enableAll, enableSubtree, enableConditional, 
    disableAll, disableSubtree, disableConditional, 
    hasEnabledNodes, isItemFolder,
    openToNode,
    getNode, getNodeId, getSelectedNode,
    getAncestors, getDescendants };
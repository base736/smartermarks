/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var tableSelectionInitializing = false;
var tableSelectionInitialized = false;

var updateItemActions = function() {
	var numChecked = 0;
	$('.item_select').each(function() {
		if ($(this).prop('checked')) numChecked++;
	});

	if (numChecked == 0) {
		$('.item_action_none').removeAttr('disabled');
		$('a.item_action_none').removeClass('disabled-link');

		$('.item_action_many').attr("disabled", true);
		$('a.item_action_many').addClass('disabled-link');
	} else {
		$('.item_action_none').attr("disabled", true);
		$('a.item_action_none').addClass('disabled-link');

		$('.item_action_many').removeAttr('disabled');
		$('a.item_action_many').removeClass('disabled-link');
	}
	
	if (numChecked != 1) {
		$('.item_action_one').attr("disabled", true);
		$('a.item_action_one').addClass('disabled-link');
	} else {
		$('.item_action_one').removeAttr('disabled');
		$('a.item_action_one').removeClass('disabled-link');
	}
}

function initialize() {
    if (tableSelectionInitializing) return;
    tableSelectionInitializing = true;

    var shiftPressed = false;
    var $lastRowClicked = false;

    $(document).on('keydown', function(e) {
        if (e.key === 'Shift') shiftPressed = true;
    });
    
    $(document).on('keyup', function(e) {
        if (e.key === 'Shift') shiftPressed = false;
    });
    
	$(document).on('change, click', '.item_select', function(event) {
        var $thisRow = $(this).closest('tr');

        if (($lastRowClicked === false) || !shiftPressed) {

            if ($(this).prop('checked')) $thisRow.addClass('doc-clicked');
            else $thisRow.removeClass('doc-clicked');    

        } else {

            var $thisParent = $thisRow.parent();
            var $lastParent = $lastRowClicked.parent();

            if ($thisParent.is($lastParent)) {
                var newChecked = $lastRowClicked.find('.item_select').prop('checked');
                var $first = ($thisRow.index() < $lastRowClicked.index()) ? $thisRow : $lastRowClicked;
                var $last = ($thisRow.index() < $lastRowClicked.index()) ? $lastRowClicked : $thisRow;
                var $rows = $first.nextUntil($last).addBack().add($last);
                $rows.each(function() {
                    $(this).find('.item_select').prop('checked', newChecked);
                    if (newChecked) $(this).addClass('doc-clicked');
                    else $(this).removeClass('doc-clicked');
                });
            }

        }
		
        $lastRowClicked = $thisRow;

		$(document).trigger('willModifyItemActions');
		updateItemActions();
		$(document).trigger('modifyItemActions');
		
		event.stopPropagation();
	});
	
	$(document).on('click', '.selection_table tbody tr', function(e) {
		$(this).find('.item_select').trigger('click');
	});
	
    $(document).on('mousemove', '.selection_table tbody tr', function(e) {
        $(this).siblings().removeClass('doc-hover');
        $(this).addClass('doc-hover');
    });
    
	$('#select_all').click(function(e) {
		$('.item_select').prop('checked', true);
		$('.item_select').closest('tr').addClass('doc-clicked');
		updateItemActions();
	});

	$('#select_none').click(function(e) {
		$('.item_select').prop('checked', false);
		$('.item_select').closest('tr').removeClass('doc-clicked');
		updateItemActions();
	});

	updateItemActions();

    tableSelectionInitialized = true;
}

export { initialize, updateItemActions };
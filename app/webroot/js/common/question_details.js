/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { ImageUpload } from '/js/ImageUpload/ImageUpload.js?7.0';

var questionDetailsInitializing = false;
var questionDetailsInitialized = false;

var activeItem = false;

function setActiveItem(newValue) {
    activeItem = newValue;
}

/**************************************************************************************************/
/* Functions to support editing MC questions                                                      */
/**************************************************************************************************/

var rebuildMCDetailsKey = function() {

	// Build a single row for the key

	var rowHTML = "<div class='scored_responses_row'>";

	rowHTML += "<div style='flex:1 1 100%; margin-top:4px;'>";
	for (var i = 0; i < activeItem.Answers.length; ++i) {
		var thisLetter = activeItem.Answers[i].response;
		if (activeItem.type == 'mc_truefalse') thisLetter = thisLetter.toUpperCase();
		var thisID = activeItem.Answers[i].id;
		rowHTML += 
			"<div class='mcq_detail_response'> \
				<label>" +  thisLetter + ".</label><input type='checkbox' class='dmcq_response' data-answer-id=" + thisID + "> \
			</div>";
	}
	rowHTML += '</div>';

	rowHTML += 
		'<div' + ((activeItem.type == 'mc_truefalse') ? ' style="display:none;"' : ' style="flex:none; width:auto; display:flex; align-items:center;"') + '>' +
			'<label style="display:inline-block; width:auto; margin:0px;">Marks:&nbsp</label>' +
			'<input class="defaultTextBox2 dmcq_value" style="width:30px; margin:0px;" type="text" />' +
			'<button title="Add" class="menu_button add_icon mcQuestionAdd" style="margin-left:5px;">&nbsp;</button>' +
			'<button title="Delete" class="menu_button delete_icon mcQuestionDelete">&nbsp;</button>' +
		'</div>';

	rowHTML += '</div>';

	// Create the full key

	if (activeItem.ScoredResponses.length == 0) {

		$('#dmcq_answer_list').html(rowHTML);
		$('#dmcq_answer_list .dmcq_value').val('1');
		$('#dmcq_answer_list .mcQuestionDelete').attr('disabled', 'disabled');

	} else {

		$('#dmcq_answer_list').empty();
		for (var i = 0; i < activeItem.ScoredResponses.length; ++i) {
			$('#dmcq_answer_list').append(rowHTML);
		}
	
		// Fill the key
	
		for (var i = 0; i < activeItem.ScoredResponses.length; ++i) {
			var $thisRow = $('#dmcq_answer_list .scored_responses_row').eq(i);
	
			var answerIDs = [];
			if ('id' in activeItem.ScoredResponses[i]) answerIDs = [activeItem.ScoredResponses[i].id];
			else if ('ids' in activeItem.ScoredResponses[i]) answerIDs = activeItem.ScoredResponses[i].ids;
			for (var j = 0; j < answerIDs.length; ++j) {
				$thisRow.find('.dmcq_response').each(function() {
					if ($(this).attr('data-answer-id') == answerIDs[j]) $(this).prop('checked', true);
				});
			}
			
			if ('value' in activeItem.ScoredResponses[i]) {
				$thisRow.find('.dmcq_value').val(activeItem.ScoredResponses[i].value);
			} else $thisRow.find('.dmcq_value').val('');
		}	
	
	}

	rebuildMCDetailsDescription();
}

var rebuildMCDetailsDescription = function() {

	// Update the description

	var numValid = 0;
	var needsDescription = false;
	var descriptionText = "<b>Note</b>: Students will recieve ";
	for (var i = 0; i < activeItem.ScoredResponses.length; ++i) {
		var isValid = true;

		var ids;
		if (!('value' in activeItem.ScoredResponses[i]) || (activeItem.ScoredResponses[i].value <= 0)) isValid = false;
		if ('id' in activeItem.ScoredResponses[i]) ids = [activeItem.ScoredResponses[i].id];
		else if ('ids' in activeItem.ScoredResponses[i]) ids = activeItem.ScoredResponses[i].ids;
		else isValid = false;

		if (isValid) {
			numValid++;

			var answerLetters = '';
			for (var j = 0; j < activeItem.Answers.length; ++j) {
				if (ids.indexOf(activeItem.Answers[j].id) >= 0) {
					answerLetters += activeItem.Answers[j].response;
				}
			}

			var answerValue = activeItem.ScoredResponses[i].value;
			
			if (answerLetters.length > 1) needsDescription = true;
			if (i > 0) descriptionText += ", or ";
			if (answerValue.length == 0) descriptionText += "unspecified marks";
			else descriptionText += answerValue + " mark" + ((answerValue == '1') ? '' : 's')
			descriptionText += " if they fill in ";
			if (answerLetters.length > 1) {
				needsDescription = true;
				if (answerLetters.length == 2) descriptionText += "both ";
			}
			for (var j = 0; j < answerLetters.length; ++j) {
				if (j > 0) descriptionText += " and "
				descriptionText += "'" + answerLetters.charAt(j) + "'";
			}
		}
	}
	if (numValid > 1) needsDescription = true;

	descriptionText += ".";
	if (needsDescription && (numValid == 1)) {
		descriptionText += " To score alternative responses instead, click '+' and enter a second response.";
	}
	
	if (needsDescription) $('#dmcq_description').html(descriptionText);
	else $('#dmcq_description').html("");
}

var rebuildMCDetails = function() {

	// Set question type selector

	if (activeItem.type == 'mc_truefalse') $('#dmcq_type').val('TrueFalse');
	else $('#dmcq_type').val('Letter');

	// Set mc choices count selector

	$('#dmcq_choices').val(activeItem.Answers.length);
	
	// Set omitted / bonus / default selector

	var isOmitted = false;
	if ('scoring' in activeItem) {
		if (activeItem['scoring'] == 'omit') {
			$('#dmcq_scoring').val('Omit');
			isOmitted = true;
		} else if (activeItem['scoring'] == 'bonus') {
			$('#dmcq_scoring').val('Bonus');
		}
	} else {
		$('#dmcq_scoring').val('Default');
	}

	// Clear disabled elements

	$('#dmcq_detailWrapper *').removeAttr('disabled');
	$('#dmcq_detailWrapper *').removeClass('disabled');
	$('#dmcq_detailWrapper .intSpinner').spinner('enable');
	
	// Rebuild key

	rebuildMCDetailsKey();

	// Enable or disable controls as required

	if (isOmitted) {
		$('#dmcq_detailWrapper input').attr('disabled', 'disabled');
		$('#dmcq_detailWrapper label').addClass('disabled');
		$('#dmcq_detailWrapper .mcQuestionAdd').attr('disabled', 'disabled');
		$('#dmcq_detailWrapper .mcQuestionDelete').attr('disabled', 'disabled');	
	}
	
	if (isGenerated || isOnline || (activeItem.type == 'mc_truefalse')) {
		$('#dmcq_choices').spinner('disable');
		$('#dmcq_choices_label').addClass("disabled");
	} else {
		$('#dmcq_choices').spinner('enable');
		$('#dmcq_choices_label').removeClass("disabled");
	}

	if (isGenerated || isOnline) {
		$('#dmcq_type').attr('disabled', 'disabled');
		$('#dmcq_type_label').addClass("disabled");
		$('#dmcq_numberingType').attr('disabled', 'disabled');
		$('#dmcq_numberingType_label').addClass("disabled");
	}
}

function validateMCQuestion() {
	var validated = true;

	var valueRegex = /^([1-9]\d*|0)(\.\d+)?$/;
	
	var responses = [];
	for (var i = 0; i < activeItem.ScoredResponses.length; ++i) {
		var $thisRow = $('#dmcq_answer_list .scored_responses_row').eq(i);
		var rowValidated = true;

		var thisResponse = false;
		if ('id' in activeItem.ScoredResponses[i]) thisResponse = activeItem.ScoredResponses[i].id;
		else if ('ids' in activeItem.ScoredResponses[i]) thisResponse = JSON.stringify(activeItem.ScoredResponses[i].ids);
		else {
			$thisRow.validationEngine('showPrompt', '* Scored response must not be blank', 'error', 'bottomLeft', true);
			rowValidated = false;
		}

		if (rowValidated !== false) {
			if (!('value' in activeItem.ScoredResponses[i]) || !valueRegex.test(activeItem.ScoredResponses[i].value)) {
				$thisRow.validationEngine('showPrompt', '* Marks must be a nonnegative decimal number', 'error', 'bottomLeft', true);
				rowValidated = false;	
			}
		}

		if (rowValidated !== false) {
			if (responses.indexOf(thisResponse) >= 0) {
				$thisRow.validationEngine('showPrompt', '* Scored responses must be unique.', 'error', 'bottomLeft', true);
				rowValidated = false;
			} else responses.push(thisResponse);	
		}

		validated &= rowValidated;
	}

	return validated;
}

/**************************************************************************************************/
/* Functions to support editing NR questions                                                      */
/**************************************************************************************************/

var getNRDetailsData = function() {
	if (activeItem.ScoredResponses.length > 0) {

		// Remove last scored response if it's a placeholder

		var lastScored = activeItem.ScoredResponses[activeItem.ScoredResponses.length - 1];
		if (!('id' in lastScored) && !('ids' in lastScored)) {
			activeItem.ScoredResponses.pop();
		}
	}
	
	return activeItem;
}

var setNRDetailsData = function(activeItem) {
	$('#dnq_item_data').val(JSON.stringify(activeItem));
}

function rebuildNRDetails() {

	// Set question type and digit count selectors

	$('#dnq_digitCount').val(activeItem.TypeDetails.nrDigits);
	$('#dnq_answerFormat').val(activeItem.type);

	// Set omitted / bonus / default selector

	var isOmitted = false;
	if ('scoring' in activeItem) {
		if (activeItem['scoring'] == 'omit') {
			$('#dnq_scoring').val('Omit');
			isOmitted = true;
		} else if (activeItem['scoring'] == 'bonus') {
			$('#dnq_scoring').val('Bonus');
		}
	} else {
		$('#dnq_scoring').val('Default');
	}

	// Draw options

	if (activeItem.type == 'nr') {

   		$('#nrOptions').hide();

	} else if (activeItem.type == 'nr_fraction') {

   		$('#nrOptions').show();
   		$('#numberOptions').hide();
		$('#selectionOptions').hide();

	} else if (activeItem.type == 'nr_selection') {

   		$('#nrOptions').show();
   		$('#numberOptions').hide();
		$('#selectionOptions').show();

		var partialValue = activeItem.TypeDetails.partialValue;
		$('#dnq_partialValue').val(partialValue);

		if (partialValue == 0.0) {
			$('#dnq_allowPartialMatch').prop('checked', false);
			$('#dnq_partialValueWrapper').hide();
		} else {
			$('#dnq_allowPartialMatch').prop('checked', true);
			$('#dnq_partialValueWrapper').show();
		}

		$('#dnq_ignoreOrder').prop('checked', activeItem.TypeDetails.ignoreOrder);
		$('#dnq_markByDigits').prop('checked', activeItem.TypeDetails.markByDigits);

	} else {

   		$('#nrOptions').show();
		$('#selectionOptions').hide();
   		$('#numberOptions').show();

		var fudgeFactor = activeItem.TypeDetails.fudgeFactor;
		if (fudgeFactor < 1.0) fudgeFactor *= 100;
		$('#dnq_fudgeFactor').val(fudgeFactor);

		if (fudgeFactor == 0.0) {
			$('#dnq_allowSmallErrors').prop('checked', false);
			$('#dnq_fudgeFactorWrapper').hide();
		} else {
			$('#dnq_allowSmallErrors').prop('checked', true);
			$('#dnq_fudgeFactorWrapper').show();
		}
		
		var tensValue = activeItem.TypeDetails.tensValue;
		$('#dnq_tensValue').val(tensValue);

		if (tensValue == 0.0) {
			$('#dnq_allowFactorOfTen').prop('checked', false);
			$('#dnq_tensValueWrapper').hide();
		} else {
			$('#dnq_allowFactorOfTen').prop('checked', true);
			$('#dnq_tensValueWrapper').show();
		}

		var sigDigsBehaviour = activeItem.TypeDetails.sigDigsBehaviour;
		if ((sigDigsBehaviour == 'Zeros') || (sigDigsBehaviour == 'Round')) {
			$('#dnq_sigDigsValue').val(activeItem.TypeDetails.sigDigsValue);
			$('#dnq_sigDigsValueWrapper').show();
		} else {
			$('#dnq_sigDigsValueWrapper').hide();
		}
		
	}

	// Clear disabled elements

	$('#dnq_detailWrapper *').removeAttr('disabled');
	$('#dnq_detailWrapper *').removeClass('disabled');
	$('#dnq_detailWrapper .intSpinner').spinner('enable');

	// Draw answers

	rebuildNRAnswers();

	// Show and hide elements as required

	if (isGenerated || isOnline) {
		$('#dnq_digitCount_label').addClass("disabled");
		$('#dnq_digitCount').attr('disabled', 'disabled');
		$('#dnq_digitCount').spinner('disable');

		$('#dnq_numberingType_label').addClass("disabled");
		$('#dnq_numberingType').attr("disabled", "disabled");
		
		$('#dnq_answerFormat_label').addClass("disabled");
		$('#dnq_answerFormat').attr("disabled", "disabled");
	}

	if (isOmitted) {
		
		$('#dnq_detailWrapper input').attr('disabled', 'disabled');
		$('#dnq_detailWrapper select').attr('disabled', 'disabled');
		$('#dnq_detailWrapper label').addClass("disabled");
		$('#dnq_detailWrapper span').addClass("disabled");
		$('#dnq_detailWrapper .nrAnswerAdd').attr('disabled', 'disabled');
		$('#dnq_detailWrapper .nrAnswerDelete').attr('disabled', 'disabled');

	}
}

var rebuildNRAnswers = function() {

	// Build a single row for the key

	var rowHTML = 
		'<div class="options_row scored_responses_row"> \
			<label style="display:inline-block; text-align:right;">Answer:&nbsp</label> \
			<input class="nrAnswerField defaultTextBox2" style="width:90px;" type="text" /> \
			<label style="text-align:right; margin-left:15px;">Marks:&nbsp</label> \
			<input class="validate[required,custom[isPosDecimal]] nrValue defaultTextBox2" style="width:30px;" type="text" /> \
			<button title="Add" class="menu_button add_icon nrAnswerAdd" style="margin-left:15px;">&nbsp;</button> \
			<button title="Delete" class="menu_button delete_icon nrAnswerDelete">&nbsp;</button> \
		</div>';

	// Apply the row repeatedly to build all scored responses

	$('#dnq_answerWrapper').empty();
	for (var i = 0; i < activeItem.ScoredResponses.length; ++i) {
		var $keyRow = $(rowHTML).appendTo($("#dnq_answerWrapper"));

		if ('value' in activeItem.ScoredResponses[i]) {
			$keyRow.find('.nrValue').val(activeItem.ScoredResponses[i].value);
		}

		var answer = false;
		if ('id' in activeItem.ScoredResponses[i]) {
			for (var j = 0; j < activeItem.Answers.length; ++j) {
				if (activeItem.Answers[j].id == activeItem.ScoredResponses[i].id) {
					answer = activeItem.Answers[j].response;
				}
			}
		}
		if (answer !== false) $keyRow.find('.nrAnswerField').val(answer);

		$keyRow.attr('data-answer-id', activeItem.ScoredResponses[i].id);
	}

	if (($('#dnq_has_calculated').length > 0) && ($('#dnq_has_calculated').val() == 1)) {
		$('#dnq_answerWrapper .nrAnswerField').attr('disabled', 'disabled');
		$('#dnq_answerWrapper .nrAnswerField').prev().addClass('disabled');
		$('#dnq_answerWrapper .nrAnswerAdd').attr('disabled', 'disabled');
		$('#dnq_answerWrapper .nrAnswerDelete').attr('disabled', 'disabled');
	} else {
		$('#dnq_answerWrapper .nrAnswerField').removeAttr('disabled');
		$('#dnq_answerWrapper .nrAnswerField').prev().removeClass('disabled');
		$('#dnq_answerWrapper .nrAnswerAdd').removeAttr('disabled');
		$('#dnq_answerWrapper .nrAnswerDelete').removeAttr('disabled');
	}

	if (activeItem.ScoredResponses.length == 1) {
		$('#dnq_answerWrapper .nrAnswerDelete').attr('disabled', 'disabled');
	}
	
	rebuildNRValidations();
}

var rebuildNRValidations = function() {
	var digitCount = activeItem.TypeDetails.nrDigits;

	var validationClasses = 'nrAnswerField defaultTextBox2';
	if (activeItem.type == 'nr') {

	} else if (activeItem.type == 'nr_scientific') {
		validationClasses += ' validate[required,custom[isScientific]]';
	} else if (activeItem.type == 'nr_selection') {
		validationClasses += ' validate[required,custom[isSelection],maxSize[' + digitCount + ']';
	} else if (activeItem.type == 'nr_fraction') {
		validationClasses += ' validate[required,custom[isFraction],maxSize[' + digitCount + ']';
	} else {
		validationClasses += ' validate[required,custom[isDecimal],maxSize[' + digitCount + ']';
	}

	$('#dnq_answerWrapper .nrAnswerField').each(function() {
		$(this).attr('class', validationClasses);
	});
}

var init_nr_typeDetails = function(itemData, digitCount) {
	if ((itemData.type == 'nr_decimal') || (itemData.type == 'nr_scientific')) {
	
		itemData.TypeDetails = {
			nrDigits: digitCount,
			fudgeFactor: userDefaults.Question.TypeDetails.nr.fudgeFactor,
			tensValue: userDefaults.Question.TypeDetails.nr.tensValue,
			sigDigsBehaviour: userDefaults.Question.TypeDetails.nr.sigDigsBehaviour,
			sigDigsValue: userDefaults.Question.TypeDetails.nr.sigDigsValue
		};

	} else if (itemData.type == 'nr_selection') {

		itemData.TypeDetails = {
			nrDigits: digitCount,
			markByDigits: userDefaults.Question.TypeDetails.nr.markByDigits,
			partialValue: userDefaults.Question.TypeDetails.nr.partialValue,
			ignoreOrder: userDefaults.Question.TypeDetails.nr.ignoreOrder
		};

	} else if (itemData.type.substr(0, 2) == 'nr') {

		itemData.TypeDetails = {
			nrDigits: digitCount
		}

	}
}

function validateNRQuestion() {
	var validated = true;

	var responses = [];
	$('#editNRQuestion_dialog .nrAnswerField').each(function() {
		var response = $(this).val();
		if ($('#dnq_answerFormat').val() == 'nr_scientific') {
			response = response.replace(/\D/g, '');
		}

		for (var i = 0; i < responses.length; ++i) {
			if (response == responses[i]) {
				$(this).validationEngine('showPrompt', '* Scored responses must be unique.', 'error', 'bottomLeft', true);
				validated = false;
			}
		}
		responses.push(response);
	});
	
	return validated;
}

/**************************************************************************************************/
/* Functions to support editing WR questions                                                      */
/**************************************************************************************************/

function resizeWREditor(fixTopLeft) {

	// Resize as required

	var top, left
	if (fixTopLeft) {
		top = parseFloat($("#editWRQuestion_dialog").closest('.ui-dialog').css('top'));
		left = parseFloat($("#editWRQuestion_dialog").closest('.ui-dialog').css('left'));
	}

	var originalHeight = $("#editWRQuestion_dialog").dialog("option", "height");
	var oldWrapperHeight = $("#editWRQuestion_dialog .dialog_wrapper").height();
	$("#editWRQuestion_dialog .dialog_wrapper").css('height', 'auto');
	var newWrapperHeight = $("#editWRQuestion_dialog .dialog_wrapper").height();
	$("#editWRQuestion_dialog .dialog_wrapper").css('height', '100%');
	var minHeight = originalHeight + newWrapperHeight - oldWrapperHeight;

	$("#editWRQuestion_dialog").dialog("option", "height", minHeight);
	$("#editWRQuestion_dialog").dialog("option", "minHeight", minHeight);
	
	if (fixTopLeft) {
		$("#editWRQuestion_dialog").closest('.ui-dialog').css('top', top);
		$("#editWRQuestion_dialog").closest('.ui-dialog').css('left', left);
	}	
}

function rebuildWRDetails(fixTopLeft) {

	// Set question height input

	$('#dwq_height').val(activeItem.TypeDetails.height);

	// Set omitted / bonus / default selector

	var isOmitted = false;
	if ('scoring' in activeItem) {
		if (activeItem['scoring'] == 'omit') {
			$('#dwq_scoring').val('Omit');
			isOmitted = true;
		} else if (activeItem['scoring'] == 'bonus') {
			$('#dwq_scoring').val('Bonus');
		}
	} else {
		$('#dwq_scoring').val('Default');
	}

	// Clear disabled elements

	$('#dwq_detailWrapper textarea').removeAttr('disabled');
	$('#dwq_criteriaWrapper *').removeAttr('disabled');
	$('#dwq_criteriaWrapper *').removeClass('disabled');
	$('#dwq_criteriaWrapper .intSpinner').spinner('enable');
	$('#editRubric').removeAttr('disabled');

	// Rebuild criteria

	rebuildWRCriteria();
	
	// Show and hide elements as required

	if (isGenerated) {
		$('#dwq_criteriaWrapper input').attr('disabled', 'disabled');
		$('#dwq_criteriaWrapper .intSpinner').spinner('disable');
		$('#dwq_criteriaWrapper label').addClass("disabled");
		$('#dwq_criteriaWrapper .wrCriteriaAdd').attr('disabled', 'disabled');
		$('#dwq_criteriaWrapper .wrCriteriaDelete').attr('disabled', 'disabled');
	}

	if (isGenerated || isOnline) {
		$('#dwq_numberingType').attr("disabled", "disabled");
		$('#dwq_numberingType_label').addClass("disabled");
	}

	if (isOmitted) {
		$('#dwq_detailWrapper textarea').attr('disabled', 'disabled');
		$('#dwq_detailWrapper textarea').attr('disabled', 'disabled');
		$('#dwq_criteriaWrapper input').attr('disabled', 'disabled');
		$('#dwq_criteriaWrapper .intSpinner').spinner('disable');
		$('#dwq_criteriaWrapper label').addClass("disabled");
		$('#dwq_criteriaWrapper .wrCriteriaAdd').attr('disabled', 'disabled');
		$('#dwq_criteriaWrapper .wrCriteriaDelete').attr('disabled', 'disabled');
		$('#editRubric').attr('disabled', 'disabled');
	}
	
	if (!('prompt' in activeItem.TypeDetails)) {

		$('#dwq_prompt_type').val('none');
		$('#dwq_prompt_text').val('');

		$('#dwq_text_wrapper').hide();
		$('#dwq_image_wrapper').hide();
		$('#dwq_spaceWrapper').css('flex', 'none');

	} else {

		$('#dwq_prompt_type').val(activeItem.TypeDetails.prompt.type);

		if (activeItem.TypeDetails.prompt.type == 'text') {

			$('#dwq_prompt_text').val(activeItem.TypeDetails.prompt.text);

			$('#dwq_text_wrapper').show();
			$('#dwq_image_wrapper').hide();
			$('#dwq_spaceWrapper').css('flex', '1 0 auto');

		} else if (activeItem.TypeDetails.prompt.type == 'image') {

			if ('imageHash' in activeItem.TypeDetails.prompt) {
				setPromptImagePreview(activeItem.TypeDetails.prompt.imageHash);
			} else {
				$('#dwq_prompt_image_name').val('');
				$('#dwq_prompt_image img').remove();
				$('#dwq_prompt_image_blank').show();
			}
			$('#dwq_prompt_image_height').val(activeItem.TypeDetails.prompt.height);

			$('#prompt_image_extension').val('');
			$("#prompt_filename").html('No file selected');

			$('#dwq_text_wrapper').hide();
			$('#dwq_image_wrapper').show();
			$('#dwq_spaceWrapper').css('flex', '1 0 auto');
		}
	}

	// Resize to fit

	resizeWREditor(fixTopLeft);

}

function rebuildWRCriteria() {
	var criteria = activeItem.TypeDetails.criteria;

	var add_to = "";
	for (var j = 0; j < criteria.length; ++j) {
		add_to += '<div class="options_row criteria_row" style="display:flex; flex-direction:row; align-items:center;">'
		add_to += '<label style="flex:none; display:inline-block; text-align:right;" for="dwq_label_' + j + '">Label:&nbsp</label><input class="wr_criterion defaultTextBox2 validate[required,custom[noPound]]';
		add_to += ']" style="flex:1 1 145px;" type="text" id="dwq_label_' + j + '" value="' + criteria[j].label + '"/>';
		add_to += '<label style="flex:none; text-align:right; margin-left:10px;" for="dwq_value_' + j + '">Out of:&nbsp</label><input class="wrValueField validate[required,custom[isPosInteger],min[1],max[9]] defaultTextBox2 intSpinner" style="flex:none; width:30px;" type="text" id="dwq_value_' + j + '" value="' + criteria[j].value + '"/>';
		add_to += '<button title="Add" class="menu_button add_icon wrCriteriaAdd" style="margin-left:15px;" data-index=' + j + '>&nbsp;</button>';
		add_to += '<button title="Delete" class="menu_button delete_icon wrCriteriaDelete" ' + ((criteria.length == 1) ? 'disabled ' : '') + ' data-index=' + j + '>&nbsp;</button>';
		add_to += '</div>';
	}
	$('#dwq_criteriaWrapper').html(add_to);

	$('#dwq_criteriaWrapper .intSpinner').spinner({
		step: 1,
		min: 1,
		stop: function(event, ui) {
			$(this).trigger('change');
		}
	});
}

function setPromptImagePreview(imageName) {
	$('#dwq_prompt_image_blank').hide();

	$('#dwq_prompt_image_name').val(imageName);
	var $newImage = $('<img style="display:block; margin:auto;" src="' + 
		userdataPrefix + imageName + '" />');
	$('#dwq_prompt_image img').remove();
	$('#dwq_prompt_image').append($newImage);

	$newImage.on('load', resizeImagePrompt);
}

function resizeImagePrompt() {
	var $image = $('#dwq_prompt_image img');
	if ($image.length > 0) {
		var image_ratio = $image[0].naturalWidth / $image[0].naturalHeight;
		var padding = parseInt($('#dwq_prompt_image').css('padding'));
		var parentWidth = $('#dwq_prompt_image').width() - padding;
		var parentHeight = $('#dwq_prompt_image').height() - padding;
		var parent_ratio = parentWidth / parentHeight;
		if (image_ratio < parent_ratio) {
			$image.css({
				width: parentHeight * image_ratio, 
				height: parentHeight
			});
		} else {
			$image.css({
				width: parentWidth, 
				height: parentWidth / image_ratio
			});
		}
	}
    
    resizeWREditor(true);
}

function validateWRQuestion() {
	var validated = true;

	var labels = [];
	$('#dwq_criteriaWrapper .wr_criterion').each(function() {
		var label = $(this).val().trim();
		if (labels.indexOf(label) >= 0) {
			$(this).validationEngine('showPrompt', '* Criteria must be unique.', 'error', 'bottomLeft', true);
			validated = false;
		}
		labels.push(label);
	});

	if (('prompt' in activeItem.TypeDetails) && (activeItem.TypeDetails.prompt.type == 'image')) {
		if (!('imageHash' in activeItem.TypeDetails.prompt)) {
			validated = false;
			$('#dwq_prompt_image').validationEngine('showPrompt', '* Image required.', 'error', 'bottomLeft', true);
		}
	}
	
	return validated;
}

function initialize() {
    if (questionDetailsInitializing) return;
    questionDetailsInitializing = true;
	
	/**************************************************************************************************/
	/* Functions to support editing MC questions                                                      */
	/**************************************************************************************************/

	var apply_mc_choiceCount = function() {
		var numChoices = parseInt($('#dmcq_choices').val());

		while (activeItem.Answers.length < numChoices) {
			var numAnswers = activeItem.Answers.length;
			activeItem.Answers.push({
				id: getNewID(4),
				response: String.fromCharCode(97 + numAnswers)
			});
		}

		while (activeItem.Answers.length > numChoices) {
			var removedAnswer = activeItem.Answers.pop();
			var scoredResponses = activeItem.ScoredResponses;

			var responseIndex = 0;
			while (responseIndex < scoredResponses.length) {
				var thisResponse = scoredResponses[responseIndex];

				var isGood = true;
				if ('ids' in thisResponse) {
					for (var j = 0; j < thisResponse.ids.length; ++j) {
						if (thisResponse.ids[j] == removedAnswer.id) isGood = false;
					}
				} else if (thisResponse.id == removedAnswer.id) isGood = false;

				if (isGood) responseIndex++;
				else scoredResponses.splice(responseIndex, 1);
			}

			activeItem.ScoredResponses = scoredResponses;
		}

		rebuildMCDetails();
	}

	$('#dmcq_choices').on('keyup', apply_mc_choiceCount);
    $('#dmcq_choices').spinner({ stop: apply_mc_choiceCount });

	$('#dmcq_type').on('change', function() {
		var new_type, fillLetters;
		if ($('#dmcq_type').val() == 'Letter') {

			new_type = 'mc';

			fillLetters = '';
			var numChoices = userDefaults.Document.Sections.mc_choiceCount;
			for (var i = 0; i < numChoices; ++i) fillLetters += String.fromCharCode(97 + i);

		} else {

			new_type = 'mc_truefalse';
			fillLetters = 'tf';

		}

		$('#dmcq_choices').val(fillLetters.length);

		activeItem.type = new_type;

		activeItem.Answers = [];
		for (var j = 0; j < fillLetters.length; ++j) {
			activeItem.Answers.push({
				id: getNewID(4),
				response: fillLetters[j]
			});
		}

		activeItem.ScoredResponses = [];

		rebuildMCDetails();
	});

	$('#dmcq_numberingType').on('change', function() {
		activeItem.number = $('#dmcq_numberingType').val();
	});

	$('#dmcq_scoring').on('mousedown', function(e) {
		if (!validateMCQuestion()) e.preventDefault();
	});

	$('#dmcq_scoring').on('change', function() {
		if ($('#dmcq_scoring').val() != 'Default') {
			activeItem.scoring = $('#dmcq_scoring').val().toLowerCase();
		} else delete activeItem.scoring;

		rebuildMCDetails();
	});

	var apply_mc_keyChange = function() {
		activeItem.ScoredResponses = [];
		$('#dmcq_answer_list .scored_responses_row').each(function() {
			var ids = [];
			$(this).find('.dmcq_response').each(function() {
				if ($(this).prop('checked')) ids.push($(this).attr('data-answer-id'));
			});

			var thisEntry = {};
			if (ids.length == 1) thisEntry.id = ids[0];
			else if (ids.length > 1) thisEntry.ids = ids;

			var value = parseFloat($(this).find('.dmcq_value').val());
			if (value > 0) thisEntry.value = value;

			activeItem.ScoredResponses.push(thisEntry);
		});

		$('#dmcq_answer_list .mcQuestionDelete').removeAttr('disabled');
		rebuildMCDetailsDescription();
	}

	$('#dmcq_answer_list').on('change', '.dmcq_response', apply_mc_keyChange);
	$('#dmcq_answer_list').on('keyup', '.dmcq_value', apply_mc_keyChange);

	$('#dmcq_answer_list').on('click', '.mcQuestionAdd', function() {
		activeItem.ScoredResponses.push({});
		rebuildMCDetailsKey();
		$('#dmcq_answer_list .dmcq_value').last().focus();
	});
	
	$(document).on('click', '.mcQuestionDelete', function() {
		var index = $(this).closest('.scored_responses_row').index();
		activeItem.ScoredResponses.splice(index, 1);
		rebuildMCDetailsKey();
	});

	/**************************************************************************************************/
	/* Functions to support editing NR questions                                                      */
	/**************************************************************************************************/

	var apply_nr_digitCount = function() {
		activeItem.TypeDetails.nrDigits = parseInt($(this).val());
		rebuildNRValidations();
	}

	$('#dnq_digitCount').on('keyup', apply_nr_digitCount);
    $('#dnq_digitCount').spinner({ stop: apply_nr_digitCount });

	$('#dnq_numberingType').on('change', function() {
		activeItem.number = $(this).val();
	});
	
	$('#dnq_answerFormat').on('change', function() {
		activeItem.type = $(this).val();

		// Update answers and scored responses as required

		var focusAnswer = false;
		if (activeItem.type == 'nr') {

			activeItem.Answers = [];
			activeItem.ScoredResponses = [];
			
		} else if (activeItem.ScoredResponses.length == 0) {

			var answerID = getNewID(4);
			activeItem.Answers = [{ 
				id: answerID,
				response: ''
			}];
			activeItem.ScoredResponses = [{ 
				id: answerID,
				value: 1.0
			}];
			focusAnswer = true;

            delete activeItem.scoring;
		}

		// Update type details as required

		var digitCount = parseInt($('#dnq_digitCount').val());
		init_nr_typeDetails(activeItem, digitCount);

		// Update interface
	
		rebuildNRDetails();

		if (focusAnswer) {
			$('#dnq_answerWrapper .nrAnswerField').first().focus();
		}
	});
	
	$('#dnq_scoring').on('change', function() {
        var isValid = true;

		if ($('#dnq_scoring').val() == 'Omit') {

            $('#dnq_answerWrapper .nrAnswerField').each(function() {
                if ($(this).validationEngine('validate')) isValid = false;
            });
            $('#dnq_answerWrapper .nrValue').each(function() {
                if ($(this).validationEngine('validate')) isValid = false;
            });

        } else if (activeItem.type != 'nr') {

		    if (activeItem.ScoredResponses.length == 0) {
                var answerID = getNewID(4);
                activeItem.Answers = [{ 
                    id: answerID,
                    response: ''
                }];
                activeItem.ScoredResponses = [{ 
                    id: answerID,
                    value: 1.0
                }];
            }          

        }

        if (isValid) {

            if ($('#dnq_scoring').val() != 'Default') {
                activeItem.scoring = $('#dnq_scoring').val().toLowerCase();
            } else delete activeItem.scoring;
    
            rebuildNRDetails();

        } else {

            if ('scoring' in activeItem) {
                var oldValue = activeItem.scoring;
                oldValue = oldValue.charAt(0).toUpperCase() + oldValue.slice(1);
                $('#dnq_scoring').val(oldValue);
            } else $('#dnq_scoring').val('Default');
        }
	});
	
	$('#dnq_answerWrapper').on('change', '.nrAnswerField', function() {
		var answerID = $(this).closest('.scored_responses_row').attr('data-answer-id');
		for (var i = 0; i < activeItem.Answers.length; ++i) {
			if (activeItem.Answers[i].id == answerID) {
				activeItem.Answers[i].response = $(this).val();
			}
		}
	});
	
	$('#dnq_answerWrapper').on('change', '.nrValue', function() {
		var answerID = $(this).closest('.scored_responses_row').attr('data-answer-id');
		for (var i = 0; i < activeItem.ScoredResponses.length; ++i) {
			if (activeItem.ScoredResponses[i].id == answerID) {
				activeItem.ScoredResponses[i].value = parseFloat($(this).val());
			}
		}
	});

	$('#dnq_answerWrapper').on('click', '.nrAnswerAdd', function() {
		var index = $(this).closest('.scored_responses_row').index() + 1;

		var answerID = getNewID(4);
		activeItem.Answers.splice(index, 0, { id: answerID, response: '' });
		activeItem.ScoredResponses.splice(index, 0, { id: answerID });

		rebuildNRAnswers();
		$('#dnq_answerWrapper .nrAnswerField').eq(index).focus();
	});
	
	$('#dnq_answerWrapper').on('click', '.nrAnswerDelete', function() {
		var index = $(this).closest('.scored_responses_row').index();
		activeItem.Answers.splice(index, 1);
		activeItem.ScoredResponses.splice(index, 1);
		rebuildNRAnswers();
	});

	$('#dnq_allowSmallErrors').on('change', function() {
		if ($('#dnq_allowSmallErrors').prop('checked') == true) {
			activeItem.TypeDetails.fudgeFactor = 1.0;
		} else activeItem.TypeDetails.fudgeFactor = 0.0;
		rebuildNRDetails();
	});

	$('#dnq_fudgeFactor').on('keyup', function() {
		activeItem.TypeDetails.fudgeFactor = parseFloat($('#dnq_fudgeFactor').val());
	});

	$('#dnq_sigDigsBehaviour').on('change', function() {
		var newBehaviour = $('#dnq_sigDigsBehaviour').val();

		activeItem.TypeDetails.sigDigsBehaviour = newBehaviour;
		if ((newBehaviour == 'Zeros') || (newBehaviour == 'Round')) {
			activeItem.TypeDetails.sigDigsValue = 1.0;
		} else activeItem.TypeDetails.sigDigsValue = 0.0;

		rebuildNRDetails();
	});

	$('#dnq_sigDigsValue').on('keyup', function() {
		activeItem.TypeDetails.sigDigsValue = parseFloat($('#dnq_sigDigsValue').val());
	});

	$('#dnq_allowFactorOfTen').on('change', function() {
		if ($('#dnq_allowFactorOfTen').prop('checked') == true) {
			activeItem.TypeDetails.tensValue = 0.5;
		} else activeItem.TypeDetails.tensValue = 0.0;
		rebuildNRDetails();
	});

	$('#dnq_tensValue').on('keyup', function() {
		activeItem.TypeDetails.tensValue = parseFloat($('#dnq_tensValue').val());
	});

	$('#dnq_allowPartialMatch').on('change', function() {
		if ($('#dnq_allowPartialMatch').prop('checked') == true) {
			activeItem.TypeDetails.partialValue = 0.5;
		} else activeItem.TypeDetails.partialValue = 0.0;
		rebuildNRDetails();
	});

	$('#dnq_partialValue').on('keyup', function() {
		activeItem.TypeDetails.partialValue = parseFloat($('#dnq_partialValue').val());
	});

	$('#dnq_ignoreOrder').on('change', function() {
		activeItem.TypeDetails.ignoreOrder = $('#dnq_ignoreOrder').prop('checked');
		rebuildNRDetails();
	});

	$('#dnq_markByDigits').on('change', function() {
		activeItem.TypeDetails.markByDigits = $('#dnq_markByDigits').prop('checked');
		rebuildNRDetails();
	});

	/**************************************************************************************************/
	/* Functions to support editing WR questions                                                      */
	/**************************************************************************************************/

	$('#editRubric').on('click', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		$('#dwq_rubric_edit').htmleditor('hideElement', 'edit_button_variable');

        if ('Rubric' in activeItem) {
            $('#dwq_rubric_edit').htmleditor('setValue', activeItem.Rubric.html);
        } else $('#dwq_rubric_edit').htmleditor('setValue', '');

        $('#dwq_rubric_edit').htmleditor('initSelection');

		$('#editRubric_form').dialog('open');
	});

	$('#editRubric_form').dialog({
		autoOpen: false,
		width:700,
		height:535,
		modal: true,
		resizable: true,
		minWidth:700,
		minHeight:400,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
			'Cancel': function() { 
				$(this).dialog("close");
			},
			'Done': function() {
                var newHTML = $('#dwq_rubric_edit').htmleditor('getValue');
                if (newHTML.length > 0) {
                    activeItem.Rubric = {
                        html: newHTML
                    };
                } else {
                    delete activeItem.Rubric;
                }
				$(this).dialog("close");
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
			$buttonPane.find('button:contains("Done")').addClass('btn btn-primary btn_font');
		}, close:function(event) {
		}
	});

	$('#dwq_rubric_edit').htmleditor();

	$('#dwq_prompt_type').on('change', function() {
        var focusText = false;
		if ($('#dwq_prompt_type').val() == 'none') delete activeItem.TypeDetails.prompt;
		else {
			activeItem.TypeDetails.prompt = {
				type: $('#dwq_prompt_type').val()
			};
			if (activeItem.TypeDetails.prompt.type == 'text') {
				activeItem.TypeDetails.prompt.text = '';
                focusText = true;
			} else {
				activeItem.TypeDetails.prompt.height = activeItem.TypeDetails.height
			}
		}

		rebuildWRDetails(true);

		if (focusText) $('#dwq_prompt_text').focus();
	});

	$('#dwq_prompt_text').on('input', function() {
		activeItem.TypeDetails.prompt.text = $(this).val();
	});

	var apply_prompt_height = function() {
		activeItem.TypeDetails.prompt.height = parseFloat($('#dwq_prompt_image_height').val());
	}

	$('#dwq_prompt_image_height').on('change', apply_prompt_height);
    $('#dwq_prompt_image_height').spinner({ stop: apply_prompt_height });

	$("#prompt_progressbar").progressbar();

	let promptImageUpload = null;
	let promptFile = null;

	$('#prompt_fileupload').on('change', function(e) {
		const files = e.target.files;
		if (!files || !files[0]) {

			// No file chosen
			promptFile = null;
			$('#prompt_filename').text('No file selected');
			$('#prompt_upload_button').attr('disabled', 'disabled');

		} else {

			// Store the file, enable “Upload” button
			promptFile = files[0];
			$('#prompt_filename').text(promptFile.name);
			$('#prompt_upload_button').removeAttr('disabled');

		}
	});

	$('#prompt_upload_button').click(function() {
		if (promptImageUpload) {

			// Cancel the upload
			promptImageUpload.abort();
			promptImageUpload = null;

			$('#prompt_fileupload').removeAttr('disabled');
			$('#prompt_select_button').removeAttr('disabled');
			$('#prompt_title').removeAttr('disabled');
			$('#prompt_progressbar').hide();
			$(this).removeClass('btn-danger');
			$(this).text('Upload');

		} else if (promptFile) {

			// Check file size
			if (promptFile.size > 10000000) {
				$('#prompt_filename').validationEngine(
					'showPrompt',
					'* Images must be smaller than 10 MB',
					'error',
					'bottomLeft',
					true
				);
				return;
			}

			// Start the upload
			$('#prompt_progressbar').progressbar('value', 0).show();
			$('#prompt_fileupload').attr('disabled', 'disabled');
			$('#prompt_select_button').attr('disabled', true);
			$('#prompt_title').attr('disabled', true);
			$(this).addClass('btn-danger');
			$(this).text('Cancel');

			promptImageUpload = new ImageUpload(promptFile, (percent) => {
				$('#prompt_progressbar').progressbar('value', percent);
			});

			promptImageUpload.start()
			.then((responseData) => {

				$('#prompt_upload_button').attr('disabled', 'disabled');
				$('#prompt_fileupload').removeAttr('disabled');
				$('#prompt_select_button').removeAttr('disabled');

				$('#prompt_progressbar').hide();

				$(this).removeClass('btn-danger');
				$(this).text('Upload');

				promptFile = null;
				promptImageUpload = null;

				activeItem.TypeDetails.prompt.imageHash = responseData.imageHash;
				setPromptImagePreview(responseData.imageHash);

			}).catch((err) => {

				if (err && err.message == 'Upload cancelled') {
					$.gritter.add({
						title: "Success",
						text: "Image upload cancelled.",
						image: "/img/success.svg"
					});	
				} else {
					showSubmitError();
				}

				// Reset UI
				$('#prompt_fileupload').removeAttr('disabled');
				$('#prompt_select_button').removeAttr('disabled');
				$('#prompt_title').removeAttr('disabled');
				$('#prompt_progressbar').hide();
				$(this).removeClass('btn-danger');
				$(this).text('Upload');

				promptImageUpload = null;
			});
		}
	});

	var apply_wr_height = function() {
		activeItem.TypeDetails.height = parseFloat($('#dwq_height').val());
	}

	$('#dwq_height').on('change', apply_wr_height);
    $('#dwq_height').spinner({ stop: apply_wr_height });

	$('#dwq_numberingType').on('change', function() {
		activeItem.number = $(this).val();
	});

	$('#dwq_scoring').on('change', function() {
		if ($('#dwq_scoring').val() != 'Default') {
			activeItem.scoring = $('#dwq_scoring').val().toLowerCase();
		} else delete activeItem.scoring;
		rebuildWRDetails(true);
	});

	$('#dwq_criteriaWrapper').on('click', '.wrCriteriaAdd', function() {
		var index = $(this).closest('.criteria_row').index();
		activeItem.TypeDetails.criteria.splice(index + 1, 0, {label: '', value: 1});
		rebuildWRCriteria();
		$('#dwq_criteriaWrapper .criteria_row').eq(index + 1).find('.wr_criterion').focus();
		resizeWREditor(true);
    });
    
	$('#dwq_criteriaWrapper').on('click', '.wrCriteriaDelete', function() {
		var index = $(this).closest('.criteria_row').index();
		activeItem.TypeDetails.criteria.splice(index, 1);
		rebuildWRCriteria();
		resizeWREditor(true);
    });

	$('#dwq_criteriaWrapper').on('input', '.wr_criterion', function() {
		var index = $(this).closest('.criteria_row').index();
		activeItem.TypeDetails.criteria[index].label = $(this).val();
	});

	$('#dwq_criteriaWrapper').on('change', '.wrValueField', function() {
		var index = $(this).closest('.criteria_row').index();
		activeItem.TypeDetails.criteria[index].value = parseFloat($(this).val());
	});

    questionDetailsInitialized = true;
}

export { initialize, 
    activeItem, setActiveItem,
    validateMCQuestion, rebuildMCDetails, 
    validateNRQuestion, rebuildNRValidations, rebuildNRDetails, init_nr_typeDetails,
    validateWRQuestion, rebuildWRDetails, resizeImagePrompt};
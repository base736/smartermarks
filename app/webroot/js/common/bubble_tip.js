/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var bubbletip_count = 0;

$.widget( "sm.bubbletip", {
 
 	_fadeTimer: null,
 	_windowPadding: 10,
 
    // Default options
    options: {
	    html: ''
    },
 
    _create: function() {

		var $tooltip = $('<div id="bubbletip_' + bubbletip_count + '" class="bubbletip" ><div class="arrow"></div>' + 
			'<div class="bubbletip_text">' + this.options.html + '</div></div>').appendTo("body");
	    this.element.attr('bubbletip_id', bubbletip_count);
		this.element.addClass('item_highlighted');
	    bubbletip_count++;
	    
		var widget = this;

	    this.element.hover(function() {
		    var $tooltip = $('#bubbletip_' + $(this).attr('bubbletip_id'));

			var divPosition = $(this).offset();
			var tipTop = divPosition.top + $(this).outerHeight();
			var tipLeft = divPosition.left;
			var tipLimit = window.innerWidth - $tooltip.outerWidth() - widget._windowPadding;
			if (tipLeft > tipLimit) tipLeft = tipLimit;
			
			$tooltip.css({ top: tipTop, left: tipLeft });
			$tooltip.find('.arrow').css({
				left: (divPosition.left - tipLeft + 20) + 'px'
			});
        
			if (widget._fadeTimer !== null) clearTimeout(widget._fadeTimer);
		    $tooltip.addClass("active").removeClass('out');
	    }, function() {
		    var $tooltip = $('#bubbletip_' + $(this).attr('bubbletip_id'));
		    $tooltip.addClass('out');
			widget._fadeTimer = setTimeout(function() {
				$tooltip.removeClass('active').removeClass('out');
          	}, 300);
   	    });			
    },

	_destroy: function () 
	{
	   	$('#bubbletip_' + $(this).attr('bubbletip_id')).remove();
		this.element.removeClass('item_highlighted');
	},
	    	
    setHTML: function(newHTML) {
	    var $tooltip = $('#bubbletip_' + $(this).attr('bubbletip_id'));
	    $tooltip.find('.bubbletip_text').html(newHTML);
    }
});

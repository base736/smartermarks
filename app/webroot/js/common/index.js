/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as Folders from '/js/common/folders.js?7.0';
import * as TableSelection from '/js/common/table_selection.js?7.0';

var drawIndexRow, finishIndex;

var containsCommunity = function(node) {
	var descendants = Folders.getDescendants(node);
	for (var i = 0; i < descendants.length; ++i) {
		if ('community_id' in descendants[i].data) return true;
	}
	return false;
}

var roots = {};

var itemIdTag;
var activeNode = false;
var activeCommunity = null;
var inCommunityHead = false;
var inCommunities = false;
var canExport = false;
var isCommunityAdmin = false;
var canAddUsers = false;

function findCommunityNodeID(thisTreeData, community_id) {
	for (var i = 0; i < thisTreeData.length; ++i) {
		if ('community_id' in thisTreeData[i].data) {
			if (thisTreeData[i].data.community_id == community_id) return thisTreeData[i].id;
		} else if ('children' in thisTreeData[i]) {
			var childValue = findCommunityNodeID(thisTreeData[i].children, community_id);
			if (childValue) return childValue;
		}
	}
	return false;
}

var fixNodeID = function(thisTreeData, nodeID) {
	if (nodeID !== false) {
		if (Folders.getNode(thisTreeData, nodeID) === false) nodeID = false;
	}
	
	if (!nodeID) {
		var communityCookieName = userID + '_lastCommunityID';
		var lastCommunityID = Cookies.get(communityCookieName);
		if (typeof lastCommunityID == 'undefined') lastCommunityID = -1;
		else lastCommunityID = parseInt(lastCommunityID);
	
		nodeID = findCommunityNodeID(thisTreeData, lastCommunityID);
	}

	if (!nodeID) nodeID = roots['Home'].id;

	return nodeID;
}

function updateRightSide(urlVars) {
	return new Promise((resolve, reject) => {
		var oldJSON = $('.index_table').attr('data-url-vars');
		var newJSON = JSON.stringify(urlVars);
		if (oldJSON != newJSON) {
			$('.index_table').attr('data-url-vars', newJSON);

			if (('Open bank' in roots) && (activeNode.id == roots['Open bank'].id)) {
                $('#created_header').hide();
				$('#search_type').hide();
			} else {
                $('#created_header').show();
				$('#search_type').show();
				if ('where' in urlVars) $('#search_type').val(urlVars.where);
				else $('#search_type').val('folder');
			}

			if ('search' in urlVars) {
				$('#search_wrapper .search_text').val(urlVars.search.replace(/\+/g, ' '));
			} else $('#search_wrapper .search_text').val('');

            if ('limit' in urlVars) {
				$('#show_limit').val(urlVars.limit);
			} else $('#show_limit').val('10');

			setUrlVars(urlVars);
		
			var pageNum = ('page' in urlVars) ? parseInt(urlVars.page) : 1;
		
			// Show loading in index list
		
			$('.index_table').html('<tbody><tr class="disabled"><td colspan="5"><div style="padding-left:10px;">' +
				'<div class="wait_icon"></div><i>Loading&hellip;</i></div></td></tr></tbody>');
			$('#footer_wrapper').hide();

			// Get parameters to pass to server

			var passedKeys = ['sort', 'direction', 'limit', 'page', 'search'];

            var getParams = {};
			for (var i = 0; i < passedKeys.length; ++i) {
				if (urlVars.hasOwnProperty(passedKeys[i])) getParams[passedKeys[i]] = urlVars[passedKeys[i]];
			}

			// Add a list of folders to be included

			var folderIDs = [];
			if (activeNode !== false) {
				if (activeNode.type == 'open') {

					getParams['folder_ids'] = 'open';

				} else {

					if (('where' in urlVars) && (urlVars.where != 'folder')) {
						var searchNodes;
						if (urlVars.where == 'subfolders') searchNodes = [activeNode];
						else if (urlVars.where == 'all') searchNodes = treeData;

						for (var j = 0; j < searchNodes.length; ++j) {
							var descendants = Folders.getDescendants(searchNodes[j]);
							for (var i = 0; i < descendants.length; ++i) {
								var folder_id = descendants[i].data.folder_id;
								if (!(folder_id in folderIDs)) folderIDs.push(folder_id);
							}
						}
					} else folderIDs = [activeNode.data.folder_id];
					getParams['folder_ids'] = folderIDs;
				}
			}

			// Load items

			const queryString = new URLSearchParams(getParams).toString();
			ajaxFetch(`/${itemController}/get_index?${queryString}`)
			.then(async response => {
				const responseData = await response.json();

				if (responseData.success) {

					// Fill in index and update action buttons

					$('.index_table').empty();
					for (var i = 0; i < responseData.results.length; ++i) {
						drawIndexRow(responseData.results[i]);
					}
					finishIndex();
					TableSelection.updateItemActions();

					// Complete folder links if needed; leave them hidden otherwise

					if (folderIDs.length > 1) {
						var folderDetails = {};
						$('.index_folder_link').each(function() {
							var folder_id = parseInt($(this).attr('data-folder-id'));
							if (!(folder_id in folderDetails)) {
								var node_id = Folders.getNodeId(treeData, folder_id);
								var ancestors = Folders.getAncestors(treeData, node_id);
								var path = "";
								while (ancestors.length > 0) {
									var thisNode = ancestors.pop();
									if (path.length > 0) path = path + ' > ';
									path = path + thisNode.text;
								}
								folderDetails[folder_id] = {
									node_id: node_id,
									title: path
								};
							}

							$(this).attr('title', folderDetails[folder_id].title);
							$(this).attr('data-node-id', folderDetails[folder_id].node_id);
							$(this).css('display', 'inline-block');
						});
					}
				
					// Show footer

					var urlVars = getUrlVars();

                    if ('count_total' in responseData) {
                        var html = '';
                        if ('search' in urlVars) html += 'Searching ';
                        html += responseData.count_total.toLocaleString() + ' item';
                        if (responseData.count_total != 1) html += 's';
                        if (!('search' in urlVars)) {
                            if (activeNode === false) {
                                html += ' in database';
                            } else if (activeNode.id == roots['Trash'].id) {
                                html += ' in trash';
                            } else if (('Open bank' in roots) && (activeNode.id == roots['Open bank'].id)) {
                                html += ' in open bank';
                            } else {
                                html += ' in folder';
                            }
                        }
                        $('#count_wrapper').html(html);    
                    }

                    var numPages;
                    if ('count_matching' in responseData) {
                        numPages = ('limit' in responseData) ? Math.ceil(responseData.count_matching / responseData.limit) : 1;
                    } else numPages = 5;

                    if (responseData.results.length > 0) {
                        html = '<span class="previous pag_button">&lt;</span>';
                        if (numPages > 1) {
                            var minPage, maxPage;
                            if (numPages > 5) {
                                minPage = pageNum - 2;
                                maxPage = pageNum + 2;
                                if (minPage < 1) {
                                    minPage = 1;
                                    maxPage = 5;
                                } else if (maxPage > numPages) {
                                    minPage = numPages - 4;
                                    maxPage = numPages;
                                }
                            } else {
                                minPage = 1;
                                maxPage = numPages;	
                            }
                            for (var pageLabel = minPage; pageLabel <= maxPage; ++pageLabel) {
                                html += '<span class="pag_button';
                                if (pageLabel == pageNum) html += ' current';
                                html += '">' + pageLabel + '</span>';
                            }
                        }
                        html += '<span class="next pag_button">&gt;</span>';

                        $('#pagination_wrapper').html(html);
                        $('#pagination_wrapper').show();
                        
                    } else $('#pagination_wrapper').hide();

					if (pageNum == 1) {
						$('.index_table tr:first').addClass('first_row');
						$('.pag_button.previous').addClass('disabled');
					}
					if (pageNum == numPages) {
						$('.index_table tr:last').addClass('last_row');
						$('.pag_button.next').addClass('disabled');
					}

					$('#footer_wrapper').show();
					resolve();
				} else reject();
			});
		}
	});
}

var openNode = function(nodeID) {
	var selectId = $("#treePanel").data('prefix') + nodeID;

	$("#treePanel").jstree('deselect_all');
	$("#treePanel").jstree('select_node', selectId, false);
	$("#treePanel").jstree('open_node', selectId, false);

	/***********************************************************************************************
	* Use the requested node to find a valid node to show, and update the URL accordingly          *
	************************************************************************************************/

	nodeID = fixNodeID(treeData, nodeID);
	activeNode = Folders.getNode(treeData, nodeID);

	var nodeCookieName = userID + '_lastNodeID';
	Cookies.set(nodeCookieName, nodeID);

	var ancestors = Folders.getAncestors(treeData, nodeID);
	activeCommunity = null;
	for (var i = 0; i < ancestors.length; ++i) {
		if (ancestors[i].type == 'community') activeCommunity = ancestors[i];
	}

	var communityCookieName = userID + '_lastCommunityID';
	var lastCommunityID = (activeCommunity == null) ? -1 : activeCommunity.data.community_id;
	Cookies.set(communityCookieName, lastCommunityID);

	/***********************************************************************************************
	* Get properties and permissions for the displayed node                                        *
	************************************************************************************************/

	inCommunityHead = (activeNode.type == 'community');
	inCommunities = false;
	if ('Communities' in roots) {
		inCommunities = (ancestors[ancestors.length - 1].id == roots['Communities'].id);
	}

	if (activeCommunity == null) canExport = true;
	else if (activeCommunity.data.content_permissions.all.indexOf('can_export') >= 0) canExport = true;
	else if (activeCommunity.data.is_admin) canExport = true;
	else canExport = false;
	
	if (activeCommunity == null) isCommunityAdmin = false;
	else isCommunityAdmin = (activeCommunity.data.is_admin == 1);

	if (isCommunityAdmin) canAddUsers = true;
	else if ((activeCommunity != null) && (activeCommunity.data.user_add_policy != 'admin')) canAddUsers = true;
	else canAddUsers = false;

	/***********************************************************************************************
	* Update main panel elements                                                                   *
	************************************************************************************************/

	var folderPath = '';
	for (var i = 0; i < ancestors.length; ++i) {
		if (i > 0) folderPath = ' &gt; ' + folderPath;
		folderPath = ancestors[i].text + folderPath;
	}
	$('.folder_path').html(folderPath);

	$('.index_actions').hide();

	$('.folder_dropdown').removeAttr('disabled');
	$('.folder_create').removeClass('always-disabled-link');
	$('.folder_edit').removeClass('always-disabled-link');
	$('.folder_move').removeClass('always-disabled-link');
	$('.folder_copy').removeClass('always-disabled-link');
	$('.folder_delete').removeClass('always-disabled-link');

	$('#duplicateItem_button').removeClass('always-disabled-link');

	$('.moveItem_button').removeClass('always-disabled-link');
	$('.copyItem_button').removeClass('always-disabled-link');
		
	$('.open_actions').hide();
	$('.trash_actions').hide();
	$('.default_actions').hide();

	$('#search_options').show();

	if (('Open bank' in roots) && (activeNode.id == roots['Open bank'].id)) {
		$('.open_actions').show();
		$('.folder_dropdown').attr('disabled', 'disabled');
		$('#search_options').hide();
	} else if (activeNode.id == roots['Trash'].id) {
		$('.trash_actions').show();
		$('.folder_dropdown').attr('disabled', 'disabled');
	} else $('.default_actions').show();

	if (activeNode.data.content_permissions.all.indexOf('can_add') == -1) {
		$('.folder_create').addClass('always-disabled-link');
	}
	
	if (activeNode.data.folder_permissions.indexOf('can_delete') == -1) {
		$('.folder_delete').addClass('always-disabled-link');
		if (!inCommunityHead) $('.folder_move').addClass('always-disabled-link');
	}

	if (activeNode.data.folder_permissions.indexOf('can_edit') == -1) {
		$('.folder_edit').addClass('always-disabled-link');
	}

	if (inCommunityHead || (ancestors.length == 1)) {
		$('.folder_copy').addClass('always-disabled-link');
	}

	if (activeNode.type == 'community') {
		$('.community_unsubscribe').removeClass('always-disabled-link');
	} else $('.community_unsubscribe').addClass('always-disabled-link');

	if (activeCommunity == null) {
		$('.community_create').removeClass('always-disabled-link');
		$('.community_edit').text('View settings');
		$('.community_edit').addClass('always-disabled-link');
	} else {
		$('.community_create').addClass('always-disabled-link');
		$('.community_edit').text(canAddUsers ? 'Edit community' : 'View settings');
		$('.community_edit').removeClass('always-disabled-link');		
	}

	// Disable folder delete if this folder contains a community

	if (containsCommunity(activeNode)) {
		$('.folder_delete').addClass('always-disabled-link');
	}

	/***********************************************************************************************
	* Set up folder tree for "move folder" dialog                                                  *
	************************************************************************************************/

	if (inCommunityHead) {

		Folders.disableAll($('#moveFolderParent'));
		Folders.enableSubtree($("#moveFolderParent"), roots['Communities'].id);
		Folders.disableConditional($('#moveFolderParent'), treeData, function(data) { 
			return ('community_id' in data);
		});

	} else if ((activeCommunity == null) && inCommunities) {

		Folders.disableAll($('#moveFolderParent'));
        Folders.enableSubtree($("#moveFolderParent"), roots['Communities'].id);
		Folders.disableSubtree($("#moveFolderParent"), activeNode.id);
		Folders.disableConditional($('#moveFolderParent'), treeData, function(data) { 
			if ('community_id' in data) return true;
		});

    } else {

		Folders.enableAll($('#moveFolderParent'));
		Folders.disableConditional($('#moveFolderParent'), treeData, function(data) { 
			if (!canExport && (data.community_id != activeCommunity.data.community_id)) return true;
			else if (data.content_permissions.all.indexOf('can_add') == -1) return true;
			else return false;
		});
	
		if ('Open bank' in roots) Folders.disableSubtree($("#moveFolderParent"), roots['Open bank'].id);
		Folders.disableSubtree($("#moveFolderParent"), roots['Trash'].id);
		Folders.disableSubtree($("#moveFolderParent"), activeNode.id);
		
	}

	if (!Folders.hasEnabledNodes($('#moveFolderParent'))) {
		$('.folder_move').addClass('always-disabled-link');
	}

	/***********************************************************************************************
	* Set up folder tree for "copy folder" dialog                                                  *
	************************************************************************************************/

	Folders.enableAll($('#copyFolderParent'));
	Folders.disableConditional($('#copyFolderParent'), treeData, function(data) { 
		if (!canExport && (data.community_id != activeCommunity.data.community_id)) return true;
		else if (data.content_permissions.all.indexOf('can_add') == -1) return true;
		else return false;
	});

	if ('Open bank' in roots) Folders.disableSubtree($("#copyFolderParent"), roots['Open bank'].id);
	Folders.disableSubtree($("#copyFolderParent"), roots['Trash'].id);
	Folders.disableSubtree($("#copyFolderParent"), activeNode.id);
		
	if (!Folders.hasEnabledNodes($('#copyFolderParent'))) {
		$('.folder_copy').addClass('always-disabled-link');
	}

	/***********************************************************************************************
	* Set up folder tree for "move item" dialog                                                    *
	************************************************************************************************/

	Folders.enableAll($('#moveTargetPanel'));
	Folders.disableConditional($('#moveTargetPanel'), treeData, function(data) { 
		if (!canExport && (data.community_id != activeCommunity.data.community_id)) return true;
		else if (data.content_permissions.all.indexOf('can_add') == -1) return true;
		else return false;
	});

	if ('Open bank' in roots) Folders.disableSubtree($("#moveTargetPanel"), roots['Open bank'].id);
	Folders.disableSubtree($("#moveTargetPanel"), roots['Trash'].id);

	if (!Folders.hasEnabledNodes($('#moveTargetPanel'))) {
		$('.moveItem_button').addClass('always-disabled-link');
	}

	/***********************************************************************************************
	* Set up folder tree for "copy item" dialog                                                    *
	************************************************************************************************/

	Folders.enableAll($('#copyTargetPanel'));
	Folders.disableConditional($('#copyTargetPanel'), treeData, function(data) { 
		if (!canExport && (data.community_id != activeCommunity.data.community_id)) return true;
		else if (data.content_permissions.all.indexOf('can_add') == -1) return true;
		else return false;
	});

	if ('Open bank' in roots) Folders.disableSubtree($("#copyTargetPanel"), roots['Open bank'].id);
	Folders.disableSubtree($("#copyTargetPanel"), roots['Trash'].id);

	if (!Folders.hasEnabledNodes($('#copyTargetPanel'))) {
		$('.copyItem_button').addClass('always-disabled-link');
	}

	/***********************************************************************************************
	* Set up folder tree for "duplicate item" dialog                                               *
	************************************************************************************************/

	// Prevent items being duplicated without add or export permissions

	Folders.enableAll($('#duplicateTargetPanel'));
	Folders.disableConditional($('#duplicateTargetPanel'), treeData, function(data) { 
		if (!canExport && (data.community_id != activeCommunity.data.community_id)) return true;
		else if (data.content_permissions.all.indexOf('can_add') == -1) return true;
		else return false;
	});
	
	if ('Open bank' in roots) Folders.disableSubtree($("#duplicateTargetPanel"), roots['Open bank'].id);
	Folders.disableSubtree($('#duplicateTargetPanel'), roots['Trash'].id);	

	if (!Folders.hasEnabledNodes($('#duplicateTargetPanel'))) {
		$('#duplicateItem_button').addClass('always-disabled-link');
	}

	// Update right side data

	var urlVars = getUrlVars();

	delete urlVars.limit;
	delete urlVars.page;
	delete urlVars.where;
	delete urlVars.search;

	urlVars.n = nodeID;
	
	updateRightSide(urlVars);

	return nodeID;
}

var highlightPrevious = function($activeRow) {
    if ($activeRow.prev().length == 1) {
        $activeRow = $activeRow.prev();

        $activeRow.siblings().removeClass('doc-hover');
        $activeRow.addClass('doc-hover');    
        return Promise.resolve();

    } else if (!$activeRow.is('.first_row')) {
        var urlVars = getUrlVars();
        var pageNum = ('page' in urlVars) ? parseInt(urlVars.page) : 1;

        urlVars.page = pageNum - 1;

        return updateRightSide(urlVars)
        .then(function() {
            $activeRow = $('.item_select').last().closest('tr');
            $activeRow.addClass('doc-hover');
        });

    } else return Promise.reject();
}

var highlightNext = function($activeRow) {
    if ($activeRow.next().length == 1) {

        $activeRow = $activeRow.next();

        $activeRow.siblings().removeClass('doc-hover');
        $activeRow.addClass('doc-hover');
        return Promise.resolve();

    } else if (!$activeRow.is('.last_row')) {

        var urlVars = getUrlVars();
        var pageNum = ('page' in urlVars) ? parseInt(urlVars.page) : 1;
        urlVars.page = pageNum + 1;

        return updateRightSide(urlVars)
        .then(function() {
            $activeRow = $('.item_select').first().closest('tr');
            $activeRow.addClass('doc-hover');
        });

    } else return Promise.reject();
}

function getDuplicateTarget(actionName) {
    return new Promise((resolve, reject) => {
        var buttonDefinition = {
            'Cancel': function() {
                $(this).dialog("close");

                $.gritter.add({
                    title: "Error",
                    text: "Save cancelled.",
                    image: "/img/error.svg"
                });

                reject();
            }
        };
        buttonDefinition[actionName] = function() {
            var json = Folders.getSelectedNode("duplicateTargetPanel");
            resolve({
                folder_raw_id: json.data.raw_id,
                folder_id: json.data.folder_id
            });
        }

        $('#duplicateItem_dialog').dialog(
            'option', 
            'buttons', buttonDefinition
        );

        $('#duplicateItem_dialog').dialog('open');

        var $buttonPane = $('#duplicateItem_dialog').siblings('.ui-dialog-buttonpane');
        $buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
        $buttonPane.find('button:contains("' + actionName + '")').addClass('btn btn-save btn_font');

    });
}

function initialize(callbacks = {}) {

    TableSelection.initialize();
	
    drawIndexRow = callbacks.drawIndexRow;
    finishIndex = callbacks.finishIndex;
    
    $.extend($.ui.dialog.prototype.options, {
        resizable: false,
        create: function() {
            var $this = $(this);
            
            $this.keypress(function(e) {
                if( e.key === 'Enter') {
                    if ($this.attr('id') == 'newVersion_dialog') {
                        var $nextEntry = $(".versionInput:focus").closest(".version_entry").nextAll('.version_entry');
                        if ($nextEntry.length > 0) $nextEntry.first().find(".versionInput").focus();
                    } else if (($this.attr('id') != 'sendCopy_dialog') && ($this.attr('id') != 'addMember_dialog')) {
                        $this.parent().find('.ui-dialog-buttonpane .btn-save').click();
                        return false;
                    }
                }
            });
        }
    });
    
    window.onload = function() {
    
    // To reload items if we got here by hitting the back button while editing one 
    
        var e = document.getElementById("refreshed");
        if (e.value=="no") e.value="yes";
        else {
            e.value="no";
            location.reload();
        }
    };
    
    if (itemType == 'Document') {
		itemIdTag = 'data-document-id';
	} else if (itemType == 'Assessment') {
		itemIdTag = 'data-assessment-id';
	} else if (itemType == 'Question') {
		itemIdTag = 'data-userquestion-id';
	} else if (itemType == 'SittingStudent') {
		itemIdTag = 'data-sitting-student-id';
	} else if (itemType == 'Sitting') {
		itemIdTag = 'data-sitting-id';
	}

    var resizeInitialStyle = false;

	var sortType = 'Created';
	if (('Indices' in userDefaults) && ('sort' in userDefaults.Indices)) {
		if (userDefaults.Indices.sort == 'name') sortType = 'Name';
	}

	$('#sort_type option').each(function() {
		$(this).prop('selected', ($(this).text() == sortType));
	});

	$('form').validationEngine({
	 	promptPosition : "bottomLeft",
	 	binded: false
    });

	if (typeof(treeData) != 'undefined') {

/***********************************************************************************************
* Set up folder tree for left panel                                                            *
************************************************************************************************/

		for (var i = 0; i < treeData.length; ++i) {
			roots[treeData[i].text] = {
				id: treeData[i].id,
				folder_id: treeData[i].data.folder_id
			};
		}

		$('.folderTree').each(function() {
			Folders.initFolderTree($(this), treeData);
		});

		// Read node ID from query and rebuild URL as necessary to be valid

		var urlVars = getUrlVars();

		var nodeCookieName = userID + '_lastNodeID';
		var lastNodeID = Cookies.get(nodeCookieName);

		var nodeID;
		if ('n' in urlVars) nodeID = urlVars.n;
		else if (typeof lastNodeID != 'undefined') {
			if (lastNodeID == roots['Trash'].id) nodeID = roots['Home'].id;
			else nodeID = lastNodeID;
		} else nodeID = false;
		
		nodeID = fixNodeID(treeData, nodeID);

		// Set up folder tree click handler

		$("#treePanel").bind("activate_node.jstree", function (event, data) {
			openNode(data.node.data.raw_id);
		});

		$("#treePanel").jstree('close_all');
		openNode(nodeID);

	} else {

        $('.open_actions').hide();
        $('.trash_actions').hide();
            
		updateRightSide([]);

	}

    $(document).on('keydown', function(e) {
        if (($('.ui-dialog:visible').length == 0) && !$(e.target).is("input:not([readonly]), textarea")) {
            var $activeRow = $('tr.doc-hover');
            if ($activeRow.length == 1) {
                if ((e.key === 'ArrowLeft') || (e.key === 'ArrowUp')) {

                    // Up arrow or left arrow

                    e.preventDefault();
                    highlightPrevious($activeRow);
        
                } else if ((e.key === 'ArrowRight') || (e.key === 'ArrowDown')) {
        
                    // Down arrow or right arrow

                    e.preventDefault();
                    highlightNext($activeRow);
        
                } else if (e.key === 'Enter') {
        
                    // Enter
        
                    e.preventDefault();
                    $activeRow.find('.item_select').trigger('click');
        
                }
            }
        }
    });

	$(document).on('click', '.pag_button:not(.disabled, .current)', function() {
		var urlVars = getUrlVars();
		var pageNum = ('page' in urlVars) ? parseInt(urlVars.page) : 1;

		if ($(this).is('.previous')) pageNum = pageNum - 1;
		else if ($(this).is('.next')) pageNum = pageNum + 1;
		else pageNum = parseInt($(this).text());

		urlVars.page = pageNum;
		updateRightSide(urlVars);
	});
		
	var saveFolder = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')) {
	    	if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
	
			ajaxFetch('/Folders/save', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					id: $('#dfolder_id').val(),
					parent_id: $('#dfolder_parent_id').val(),
					name: $('#dfolder_name').val()
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
			
				if (responseData.success) {

					addClientFlash({
						title: "Success",
						text: "Folder saved",
						image: "/img/success.svg"
					});	

                    window.location.reload();

				} else showSubmitError($dialog);
			});
		}
	}
	
	var moveFolder = function() {
		var json = Folders.getSelectedNode('moveFolderParent');
		if (!Folders.isItemFolder(treeData, json.data.raw_id) && (activeNode.type != 'community')) {

			$('#moveFolderParent').validationEngine('showPrompt', "* Cannot place items in this folder", 'error', 'bottomLeft', true);

		} else {

			var $dialog = $(this);

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
						
			var targetId = json.data.folder_id;

			ajaxFetch('/Folders/move/' + $('#moveFolder_id').val(), {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					target_id: targetId
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
							
				if (responseData.success) {
					var newID = SparkMD5.hash(json.data.raw_id + activeNode.data.folder_id);

					var urlVars = getUrlVars();
					urlVars.n = newID;
					var newQuery = jQuery.param(urlVars);

					addClientFlash({
						title: "Success",
						text: "Folder moved",
						image: "/img/success.svg"
					});	

					window.location.href = window.location.origin + window.location.pathname + '?' + newQuery;
				} else {
					showSubmitError($dialog);
				}
			});
		}
	}

	var copyFolder = function() {
		var json = Folders.getSelectedNode('copyFolderParent');
		if (!Folders.isItemFolder(treeData, json.data.raw_id)) {

			$('#copyFolderParent').validationEngine('showPrompt', "* Cannot place items in this folder", 'error', 'bottomLeft', true);

		} else {
			
			$('#copyFolder_dialog').dialog('close');
			$('#copyWaiting_dialog').dialog('open');
	
			if (pendingAJAXPost) return false;
			pendingAJAXPost = true;
						
			var targetId = json.data.folder_id;

			ajaxFetch('/Folders/copy/' + $('#copyFolder_id').val(), {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					target_id: targetId,
					copy_type: $('#copyFolder_type').val()
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
				
				if (responseData.success) {
					var newID = SparkMD5.hash(json.data.raw_id + responseData.folder_id);

					var urlVars = getUrlVars();
					urlVars.n = newID;
					var newQuery = jQuery.param(urlVars);

					addClientFlash({
						title: "Success",
						text: "Folder copied",
						image: "/img/success.svg"
					});	

					window.location.href = window.location.origin + window.location.pathname + '?' + newQuery;

				} else {

					$.gritter.add({
						title: "Error",
						text: "Unable to copy folder",
						image: "/img/error.svg"
					});
					$('#copyWaiting_dialog').dialog('close');
					
				}
				
			}).catch(function() {

				$.gritter.add({
					title: "Error",
					text: "Unable to copy folder",
					image: "/img/error.svg"
				});
				$('#copyWaiting_dialog').dialog('close');

			});
	
		}
	}

	var deleteFolder = function() {
		var $dialog = $(this);

		var json = Folders.getSelectedNode("treePanel");
		var folderID = json.data.folder_id;

    	if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/Folders/delete/' + folderID, {
			method: 'POST'
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				var ancestors = Folders.getAncestors(treeData, activeNode.id);

				var urlVars = getUrlVars();
				urlVars.n = ancestors[1].id;

				var newQuery = jQuery.param(urlVars);

				addClientFlash({
					title: "Success",
					text: "Folder deleted",
					image: "/img/success.svg"
				});	

				window.location.href = window.location.origin + window.location.pathname + '?' + newQuery;
			} else {
				showSubmitError($dialog);
			}

		}).catch(function() {
			showSubmitError($dialog);
		});
	}
	
	$('.folder_delete').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var json = Folders.getSelectedNode("treePanel");
		var folderID = json.data.folder_id;
		var $name = json.text;

		if (typeof(folderID) != 'undefined') {
			$('#deleteFolder_text').html('<p>Are you sure you want to delete the folder "' + $name + '"? Items in this folder will be moved to the trash.</p>');
			$('#deleteFolder_dialog').dialog('open');
		}
	});
	
	$(document).on('click', '.folder_link', function(event) {
		openNode($(this).attr('data-node-id'));
		event.stopPropagation();
	});

	$(document).on('mousedown', '.index_table tbody tr', function(e) {
		e.preventDefault();
	});

	$(document).on('click', '.send_email_checkbox', function(event) {
		event.stopPropagation();
		var email = $(this).data('email');
		
		var email_list = $('#send_email_list').val().trim();
		var email_array;
		if (email_list.length == 0) email_array = [];
		else email_array = email_list.split(/,|;| |\n|\t/);
		
		var index = email_array.indexOf(email);
		if ($(this).prop('checked')) {
			if (index == -1) email_array.push(email);
		} else if (index > -1) email_array.splice(index, 1);
				
		$('#send_email_list').val(email_array.join("\n"));
	});
	
	$('#send_selection_type').on('click', function(e) {
		e.preventDefault();
		
		if ($('#send_email_list').is(':visible')) {
			$('#send_selection_type').html("Enter emails");
			
			var email_list = $('#send_email_list').val().trim();
			var email_array;
			if (email_list.length == 0) email_array = [];
			else email_array = email_list.split(/,|;| |\n|\t/);

			$('.send_email_checkbox').each(function() {
				var email = $(this).data('email');
				$(this).prop('checked', email_array.indexOf(email) > -1);
			});
			
			$('#send_email_list').hide();
			$('#send_email_checklist').show().focus();
		} else {
			$('#send_selection_type').html("Choose from list");
			$('#send_email_checklist').hide();
			$('#send_email_list').show().focus();
		}
	});
	
	$('.folder_create').click(function(e) {
		e.preventDefault();
		
		var json = Folders.getSelectedNode("treePanel");

		$('#dfolder_id').val('');
		$('#dfolder_name').val('');

		$('#dfolder_parent_id').val(json.data.folder_id);

		$('#editFolder_dialog').dialog('open');
	});

	$('.folder_edit').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var json = Folders.getSelectedNode("treePanel");

		$('#dfolder_id').val(json.data.folder_id);
		$('#dfolder_name').val(json.text);
		$('#dfolder_parent_id').val('');
		
		$('#editFolder_dialog').dialog('open');
	});

	$('.folder_move').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var json = Folders.getSelectedNode("treePanel");
		$('#moveFolder_id').val(json.data.folder_id);
		
		// Select this folder's parent on the tree

		var ancestors = Folders.getAncestors(treeData, json.data.raw_id);
		Folders.openToNode($("#moveFolderParent"), ancestors[1].id);
		
		$('#moveFolder_dialog').dialog('open');
	});

	$('.folder_copy').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var json = Folders.getSelectedNode("treePanel");
		$('#copyFolder_id').val(json.data.folder_id);
		
		$('#copyFolder_text').html('<p>Choose a destination for copying the folder "' + json.text + '", then click "Copy".</p>');

		// Select starting parent on the tree

		var ancestors = Folders.getAncestors(treeData, json.data.raw_id);
		Folders.openToNode($("#copyFolderParent"), ancestors[1].id);

		$('#copyFolder_type').val('folder');

		$('#copyFolder_dialog').dialog('open');
	});

	$('#sendCopy_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		$('#send_email_list').val("");
		$('.send_email_checkbox').prop('checked', false);
		$('#send_selection_type').html("Choose from list");
		$('#send_email_checklist').hide();
		$('#send_email_list').show();

		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		var sendCopyText = "";
		if (numChecked == 1) {
			var $itemName = $selectedCheckbox.closest('tr').find('.index_item_name').html();
			sendCopyText = '<p>Sending a copy of "' + $itemName + '".';
		} else sendCopyText = '<p>Sending a copy of ' + numChecked + ' selected items.';
		sendCopyText += " Changes made by the recipient will not be reflected in the original.";
		$('#sendCopy_text').html(sendCopyText);

		$('#sendCopy_dialog').dialog('open');
	});

	function sendValidated(status, form, json_response_from_server, options)  {
        var $dialog = $('#sendCopy_dialog');

		if (status === true) {

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			var selectedItems = [];
			$('.item_select').each(function() {
				if ($(this).prop('checked')) {
					var itemID = $(this).closest('tr').attr(itemIdTag);
					selectedItems.push(itemID);
				}
			});
	
			var actionController = itemController;
			if (itemType == 'Question') actionController = 'UserQuestions';

			var emailList = $('#send_email_list').val().split(/,|;| |\n|\t/);
			var userIndex = emailList.findIndex(element => {
				return element.toLowerCase() === userEmail.toLowerCase();
			});

			ajaxFetch('/' + actionController + '/send', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					email_list: emailList,
					item_ids: selectedItems
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
			
				if (responseData.success) {
					$dialog.dialog('close');

					var message = responseData.numSent + " item" + ((responseData.numSent == 1) ? "" : "s") + " sent.";
					$.gritter.add({
						title: "Success",
						text: message,
						image: "/img/success.svg"
					});	
	
					if (userIndex >= 0) {
						var urlVars = getUrlVars();
						delete urlVars.search;
						$('.index_table').attr('data-url-vars', '');
						updateRightSide(urlVars);	
					} else {
						$('.item_select').prop('checked', false);
						$('.item_select').closest('tr').removeClass('doc-clicked');
						TableSelection.updateItemActions();	
					}
				} else showSubmitError($dialog);
			}).catch(function() {
				showSubmitError($dialog);
			});
		} else hideSubmitProgress($dialog);
	}

	$('#editFolder_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': saveFolder
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#moveFolder_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Move': moveFolder
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Move")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
		   	initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		
		
	$('#copyFolder_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Copy': copyFolder
		}, open: function(event) {
		   	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$('.ui-dialog-buttonpane').find('button:contains("Copy")').addClass('btn btn-save btn_font');
	        $('.ui-dialog-buttonpane').find('button').blur();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});		
		
	$('#copyWaiting_dialog').dialog({
        autoOpen: false,
		closeOnEscape: false,
		width: 350,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}
	});		

	$("#copyFolder_progress").progressbar({
		value: false
	});
		
	$('#deleteFolder_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteFolder
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		
	
	$(document).on('willModifyItemActions', function(event) {
		$('.needs_copy').removeClass('disabled-link');
		$('.needs_delete').removeClass('disabled-link');
	});
	
	$(document).on('modifyItemActions', function(event) {
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				if ($(this).closest('tr').data('can-copy') != 1) $('.needs_copy').addClass('disabled-link');
				if ($(this).closest('tr').data('can-delete') != 1) $('.needs_delete').addClass('disabled-link');
			}
		});
	});
		
	$('#sendCopy_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Send': function() {
		  		$('#send_selection_type').html("Choose from list");
				$('#send_email_checklist').hide();
				$('#send_email_list').show();
				
          		$('#sendCopy_form').submit(); 
          	}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Send")').addClass('btn btn-save btn_font');
	      	$('#send_email_list').focus();
		  	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		
	
    $('#sendCopy_form').validationEngine('detach');
    $('#sendCopy_form').validationEngine('attach', {
		validationEventTrigger: 'submit',
		promptPosition : "bottomLeft",
		scroll: false,
		ajaxFormValidation: true,
		ajaxFormValidationURL: '/Users/validateTeacherList',
		onAjaxFormComplete: sendValidated,
		onFieldError: function() {
			hideSubmitProgress($('#sendCopy_dialog'));
  		},
  		onBeforeAjaxFormValidation : function() {
  			showSubmitProgress($('#sendCopy_dialog'));
  		}
    });

// Community dialogs

	$('#editCommunity_dialog').dialog({
        autoOpen: false,
		width: 650,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
		   	'Done': function() {
               	$(this).dialog("close");
			},
          	'Save': saveCommunity
		}, 
		open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
		   	$buttonPane.find('button:contains("Done")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});
	
	var enforceValidPolicies = function() {
		$('#policy_edit option').removeAttr('disabled');
		if ($('#policy_add_delete').val() == 'owner') {
			if ($('#policy_edit').val() == 'admin') $('#policy_edit').val('owner');
			$('#policy_edit option[value="admin"]').attr('disabled', 'disabled');
		} else if ($('#policy_add_delete').val() == 'all') {
			$('#policy_edit').val('all');			
			$('#policy_edit option[value="admin"]').attr('disabled', 'disabled');
			$('#policy_edit option[value="owner"]').attr('disabled', 'disabled');			
		}
	}
	
	$('#policy_add_delete').on('change', enforceValidPolicies);
	
	$('.community_create').click(function(e) {
		e.preventDefault();
		
		$('#community_id').val('');

		var parent_folder_id = roots['Communities'].folder_id;
		if (inCommunities) parent_folder_id = $('#folders').data('folder-id');
		$('#community_folder_id').val(parent_folder_id);

		$('#community_name').val('New Community');
		$('#policy_edit').val('owner');
		$('#policy_add_delete').val('owner');
		$('#policy_export').val('open');
		$('#policy_scores').val('owner');
		$('#policy_user_add').val('admin');

		$('#addMembers_button').show();
		$('#community_name').removeAttr('disabled');
		$('#community_members_list').removeClass('disabled');
		$('#editCommunity_dialog select').removeAttr('disabled');
	   	$('#editCommunity_dialog').parent().find('button:contains("Save")').show();
	   	$('#editCommunity_dialog').parent().find('button:contains("Done")').hide();

		var initMembers = {
			admin: [userEmail],
			other: []
		};
		$('#community_members_string').val(JSON.stringify(initMembers));
		rebuildMembersList();

		enforceValidPolicies();
		
		$('#editCommunity_dialog').dialog('open');
	});
	
	$('#community_members_list').on('click', '.arrow_up_icon', function() {
		var email = $(this).closest('.members_row').find('.member_email').html();
		var members = JSON.parse($('#community_members_string').val());
		var emailIndex = members.other.indexOf(email);
		if (emailIndex >= 0) {
			members.other.splice(emailIndex, 1);
			members.admin.push(email);
			members.admin.sort();
		}
		$('#community_members_string').val(JSON.stringify(members));
		rebuildMembersList();
	});
	
	$('#community_members_list').on('click', '.arrow_down_icon', function() {
		var email = $(this).closest('.members_row').find('.member_email').html();
		var members = JSON.parse($('#community_members_string').val());
		var emailIndex = members.admin.indexOf(email);
		if (emailIndex >= 0) {
			members.admin.splice(emailIndex, 1);
			members.other.push(email);
			members.other.sort();
		}
		$('#community_members_string').val(JSON.stringify(members));
		rebuildMembersList();
	});
	
	$('#community_members_list').on('click', '.delete_icon', function() {
		var email = $(this).closest('.members_row').find('.member_email').html();
		var members = JSON.parse($('#community_members_string').val());
		var adminIndex = members.admin.indexOf(email);
		if (adminIndex >= 0) {
			members.admin.splice(adminIndex, 1);
		} else {
			var otherIndex = members.other.indexOf(email);
			if (otherIndex >= 0) {
				members.other.splice(otherIndex, 1);
			}
		}
		$('#community_members_string').val(JSON.stringify(members));
		rebuildMembersList();
	});
	
	function rebuildMembersList() {
		var members = JSON.parse($('#community_members_string').val());
		
		var newHTML = "";
		
		var dialogAdmin = isCommunityAdmin;
		if ($('#community_id').val() == '') dialogAdmin = true;
		var dialogCanAdd = canAddUsers;
		if ($('#community_id').val() == '') dialogCanAdd = true;
		
		newHTML += "<div class='members_header'>Administrators</div>";
		for (var i = 0; i < members.admin.length; ++i) {
			newHTML += "<div class='members_row'><div class='member_email'>" + members.admin[i] + "</div><div>";
			if (dialogCanAdd && dialogAdmin) {
				var canMove = (members.admin.length > 1);
				newHTML += "<button title='Remove admin' class='menu_button arrow_down_icon'" + (canMove ? '' : 'disabled') + ">&nbsp;</button>";

				var canDelete = (members.admin.length > 1);
				if (members.admin[i] == userEmail) canDelete = false;
				newHTML += "<button title='Remove' class='menu_button delete_icon'" + (canDelete ? '' : 'disabled') + ">&nbsp;</button>";
			}
			newHTML += "</div></div>";
		}
		
		if (dialogCanAdd) {
			newHTML += "<div class='members_header'>Other members</div>";
			if (members.other.length == 0) {
				newHTML += "<div class='members_row' style='font-style:italic;'>Click 'Add new members' to add members</div>"
			} else {
				for (var i = 0; i < members.other.length; ++i) {
					newHTML += "<div class='members_row'><div class='member_email'>" + members.other[i] + "</div><div>";

					if (dialogAdmin) {
						newHTML += "<button title='Add admin' class='menu_button arrow_up_icon'>&nbsp;</button>";
					}

					var canDelete = (members.other[i] != userEmail);
					newHTML += "<button title='Remove' class='menu_button delete_icon'" + (canDelete ? '' : 'disabled') + ">&nbsp;</button>";

					newHTML += "</div></div>"
				}
				if (!dialogAdmin) {
					newHTML += "<div class='members_row' style='font-style:italic;'>Other members not shown</div>"
				}
			}
		}
		
		$('#community_members_list').html(newHTML);
	}
	
	function saveCommunity() {
		var $dialog = $(this);
		var $form = $dialog.find('form');
		
		if ($form.validationEngine('validate')) {
	    	if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			ajaxFetch('/Communities/save', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					community: {
						id: $('#community_id').val(),
						name: $('#community_name').val(),
						add_delete_policy: $('#policy_add_delete').val(),
						edit_policy: $('#policy_edit').val(),
						export_policy: $('#policy_export').val(),
						scores_policy: $('#policy_scores').val(),
						user_add_policy: $('#policy_user_add').val()
					},
					members_json: $('#community_members_string').val(),
					folder_id: $('#community_folder_id').val(),
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
			
				if (responseData.success) {

					addClientFlash({
						title: "Success",
						text: "Community saved",
						image: "/img/success.svg"
					});	

                    window.location.reload();

				} else showSubmitError($dialog);
			});
		}
	}
	
	$('#addMember_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Add': function() {
		  		$('#member_selection_type').html("Choose from list");
				$('#member_email_checklist').hide();
				$('#member_email_list').show();
				
          		$('#addMember_form').submit(); 
          	}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Add")').addClass('btn btn-save btn_font');
	        initSubmitProgress($(this));
	      	$('#member_email_list').focus();
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});
	
    $('#addMember_form').validationEngine('detach');
    $('#addMember_form').validationEngine('attach', {
		validationEventTrigger: 'submit',
		promptPosition : "bottomLeft",
		scroll: false,
		ajaxFormValidation: true,
		ajaxFormValidationURL: '/Users/validateTeacherList',
		onAjaxFormComplete: membersValidated,
		onFieldError: function() {
	        hideSubmitProgress($('#addMember_dialog'));
  		},
  		onBeforeAjaxFormValidation : function() {
	        showSubmitProgress($('#addMember_dialog'));
  		}
    });

	$('#addMembers_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		$('#member_email_list').val("");
		$('.member_email_checkbox').prop('checked', false);
		$('#member_selection_type').html("Choose from list");
		$('#member_email_checklist').hide();
		$('#member_email_list').show();
		
		$('.member_mail_row').each(function() {
			var email = $(this).children('.member_email_checkbox').attr('data-email');
			var members = JSON.parse($('#community_members_string').val());
			
			var isMember = false;
			if (members.other.indexOf(email) >= 0) isMember = true;
			else if (members.admin.indexOf(email) >= 0) isMember = true;
			if (isMember) $(this).hide();
			else $(this).show();
		});

		$('#addMember_dialog').dialog('open');
	});

	function getEmailArray(email_list) {
		var email_array;
		if (email_list.length > 0) {
			email_array = email_list.split(/[,;\s]+/);
			if (email_array[email_array.length - 1].length == 0) {
				email_array.pop();
			}
		} else email_array = [];
		return email_array;
	}

	$('#member_selection_type').on('click', function(e) {
		e.preventDefault();
		
		if ($('#member_email_list').is(':visible')) {
			$('#member_selection_type').html("Enter emails");
			
			var email_array = getEmailArray($('#member_email_list').val());
	
			$('.member_email_checkbox').each(function() {
				var email = $(this).data('email');
				$(this).prop('checked', email_array.indexOf(email) > -1);
			});
			
			$('#member_email_list').hide();
			$('#member_email_checklist').show().focus();
		} else {
			$('#member_selection_type').html("Choose from list");
			$('#member_email_checklist').hide();
			$('#member_email_list').show().focus();
		}
	});
	
	$(document).on('click', '.member_email_checkbox', function(event) {
		event.stopPropagation();
		var email = $(this).data('email');
		
		var email_array = getEmailArray($('#member_email_list').val());
		
		var index = email_array.indexOf(email);
		if ($(this).prop('checked')) {
			if (index == -1) email_array.push(email);
		} else if (index > -1) email_array.splice(index, 1);
				
		$('#member_email_list').val(email_array.join("\n"));
	});
	
	function membersValidated(status, form, json_response_from_server, options)  {
		if (status === true) {
			var members = JSON.parse($('#community_members_string').val());
			var newMembers = getEmailArray($('#member_email_list').val());
			for (var i = 0; i < newMembers.length; ++i) {
				var isMember = false;
				if (members.other.indexOf(newMembers[i]) >= 0) isMember = true;
				else if (members.admin.indexOf(newMembers[i]) >= 0) isMember = true;
				if (!isMember) members.other.push(newMembers[i]);
			}
			members.other.sort();
			$('#community_members_string').val(JSON.stringify(members));
			rebuildMembersList();
			
			$('#addMember_dialog').dialog('close');
		} else hideSubmitProgress($('#addMember_dialog'));
	}
	
	$('.community_edit').click(function(e){
		e.preventDefault();

		if (activeCommunity == null) return false;
    	if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		if (isCommunityAdmin) {
			$('#community_name').removeAttr('disabled');
			$('#editCommunity_dialog select').removeAttr('disabled');
		} else {
			$('#community_name').attr('disabled', 'disabled');
			$('#editCommunity_dialog select').attr('disabled', 'disabled');
		}
		
		if (canAddUsers) {
			$('#addMembers_button').show();
			$('#community_members_list').removeClass('disabled');
		   	$('#editCommunity_dialog').parent().find('button:contains("Save")').show();
		   	$('#editCommunity_dialog').parent().find('button:contains("Done")').hide();
		} else {
			$('#addMembers_button').hide();
			$('#community_members_list').addClass('disabled');
		   	$('#editCommunity_dialog').parent().find('button:contains("Save")').hide();
		   	$('#editCommunity_dialog').parent().find('button:contains("Done")').show();
		}
		
		ajaxFetch('/Communities/get_data/' + activeCommunity.data.community_id)
		.then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			$('#community_id').val(responseData.Community.id);
			$('#community_folder_id').val('');
			
			$('#community_name').val(responseData.Community.name);
			$('#policy_edit').val(responseData.Community.edit_policy);
			$('#policy_add_delete').val(responseData.Community.add_delete_policy);
			$('#policy_export').val(responseData.Community.export_policy);
			$('#policy_scores').val(responseData.Community.scores_policy);
			$('#policy_user_add').val(responseData.Community.user_add_policy);
		
			var members = {
				admin: [],
				other: []
			};
			
			for (var i = 0; i < responseData.CommunityUser.length; ++i) {
				var memberEmail = responseData.CommunityUser[i].User.email;
				if (responseData.CommunityUser[i].is_admin == 1) members.admin.push(memberEmail);
				else members.other.push(memberEmail);
			}
			
			$('#community_members_string').val(JSON.stringify(members));
			rebuildMembersList();

			enforceValidPolicies();
				
			$('#editCommunity_dialog').dialog('open');

		});
	});

// Prepare community folder trees

	function disableCommunities($parent, treeNodes) {
		for (var i = 0; i < treeNodes.length; ++i) {
			if (treeNodes[i].type == 'community') {
				Folders.disableSubtree($parent, treeNodes[i].id);
			} else if ('children' in treeNodes[i]) {
				disableCommunities($parent, treeNodes[i].children);
			}
		}
	}
	
	$('.communitiesFolderTree').each(function() {

		// Disable all folders except the communities folder, and close all folders

		Folders.disableSubtree($(this), roots['Home'].id);
		Folders.disableSubtree($(this), roots['Archive'].id);
		if ('Open bank' in roots) Folders.disableSubtree($(this), roots['Open bank'].id);
		Folders.disableSubtree($(this), roots['Trash'].id);

		// Disable all communities

		disableCommunities($(this), treeData);
	})

	$('#newItem_button').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		ajaxFetch('/' + itemController + '/create', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				folder_id: activeNode.data.folder_id
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				var target = '/' + itemType + 's/build/' + responseData.id;
				if (itemType == 'Question') target += "?u=" + responseData.user_question_id;
				window.location = target;
			} else {
				var message = "Unable to create " + itemType.toLowerCase() + ".";
				if ('message' in responseData) message = responseData.message;

				$.gritter.add({
					title: "Error",
					text: message,
					image: "/img/error.svg"
				});			
			}
		});
	});

	var emptyTrash = function() {
		var $dialog = $(this);

    	if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/' + itemController + '/empty_trash', {
			method: 'POST'
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				$dialog.dialog('close');

				$.gritter.add({
					title: "Success",
					text: "Trash emptied.",
					image: "/img/success.svg"
				});

				$('.index_table').attr('data-url-vars', '');
				openNode(roots['Trash'].id);
			} else showSubmitError($dialog);
		}).catch(function() {
			showSubmitError($dialog);
		});
	}
	
	$('#emptyTrash_button').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;

		$('#emptyTrash_dialog').dialog('open');
	});
	
	$('#emptyTrash_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': emptyTrash
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	var moveItem = function() {
		var targetJSON = Folders.getSelectedNode("moveTargetPanel");
		if (!Folders.isItemFolder(treeData, targetJSON.data.raw_id)) {

			$('#moveTargetPanel').validationEngine('showPrompt', "* Cannot place items in this folder", 'error', 'bottomLeft', true);

		} else {

			var $dialog = $(this);

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			var selectedItems = [];
			$('.item_select').each(function() {
				if ($(this).prop('checked')) {
					var itemID = $(this).closest('tr').attr(itemIdTag);
					selectedItems.push(itemID);
				}
			});

			var actionController = itemController;
			if (itemType == 'Question') actionController = 'UserQuestions';

			var targetId = targetJSON.data.folder_id;

			ajaxFetch('/' + actionController + '/move', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({ 
					folder_id: targetId,
					item_ids: selectedItems
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
		
				if (responseData.success) {
					$dialog.dialog('close');

					var message = responseData.count + " item" + ((responseData.count == 1) ? "" : "s") + " moved.";

					$.gritter.add({
						title: "Success",
						text: message,
						image: "/img/success.svg"
					});	

					var urlVars = getUrlVars();
					delete urlVars.search;
					$('.index_table').attr('data-url-vars', '');

					openNode(targetJSON.data.raw_id);
				} else showSubmitError($dialog);		
			});
		}
	}

	$('.moveItem_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked == 1) {
			var $itemName = $selectedCheckbox.closest('tr').find('.index_item_name').html();
			$('#moveItem_text').html('<p>Choose a destination folder for the item "' + $itemName + '", then click "Move".</p>');
		} else {
			$('#moveItem_text').html('<p>Choose a destination folder for the ' + numChecked + ' selected items, then click "Move".</p>');
		}

		Folders.openToNode($("#moveTargetPanel"), activeNode.id);

		$('#moveItem_dialog').dialog('open');
	});

	$('#moveItem_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Move': moveItem
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Move")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		
	
	var copyItem = function() {
		var targetJSON = Folders.getSelectedNode("copyTargetPanel");
		if (!Folders.isItemFolder(treeData, targetJSON.data.raw_id)) {

			$('#copyTargetPanel').validationEngine('showPrompt', "* Cannot place items in this folder", 'error', 'bottomLeft', true);

		} else {

			var $dialog = $(this);

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			var selectedItems = [];
			$('.item_select').each(function() {
				if ($(this).prop('checked')) {
					if (itemType == 'Question') {
                        var $row = $(this).closest('tr');
                        if ($row[0].hasAttribute('data-userquestion-id')) {
                            selectedItems.push({
                                user_question_id: $(this).closest('tr').attr('data-userquestion-id')
                            });    
                        } else {
                            selectedItems.push({
                                question_id: $(this).closest('tr').attr('data-question-id')
                            });    
                        }
					} else {
						var itemID = $(this).closest('tr').attr(itemIdTag);
						selectedItems.push(itemID);
					}
				}
			});

			var actionController = itemController;
			if (itemType == 'Question') actionController = 'UserQuestions';

			var targetId = targetJSON.data.folder_id;

			ajaxFetch('/' + actionController + '/copy', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({ 
					folder_id: targetId,
					item_ids: selectedItems
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
					
				if (responseData.success) {
					$dialog.dialog('close');

					var message = responseData.count + " item" + ((responseData.count == 1) ? "" : "s") + " copied.";

					$.gritter.add({
						title: "Success",
						text: message,
						image: "/img/success.svg"
					});	

					var urlVars = getUrlVars();
					delete urlVars.search;
					$('.index_table').attr('data-url-vars', '');

					openNode(targetJSON.data.raw_id);
				} else showSubmitError($dialog);
			}).catch(function() {
				showSubmitError($dialog);
			});
		}
	}

	$('.copyItem_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked == 1) {
			var $itemName = $selectedCheckbox.closest('tr').find('.index_item_name').html();
			$('#copyItem_text').html('<p>Choose a destination folder for the item "' + $itemName + '", then click "Copy".</p>');
		} else {
			$('#copyItem_text').html('<p>Choose a destination folder for the ' + numChecked + ' selected items, then click "Copy".</p>');
		}

		if (activeNode.data.folder_id != 'open') {
			Folders.openToNode($("#copyTargetPanel"), activeNode.id);
		}

		$('#copyItem_dialog').dialog('open');
	});

	$('#copyItem_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Copy': copyItem
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Copy")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		
	
	var deleteItem = function() {

		var $dialog = $(this);

    	if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var selectedItems = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var itemID = $(this).closest('tr').attr(itemIdTag);
				selectedItems.push(itemID);
			}
		});

		var actionController = itemController;
		if (itemType == 'Question') actionController = 'UserQuestions';

		ajaxFetch('/' + actionController + '/move', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				folder_id: roots['Trash'].folder_id,
				item_ids: selectedItems,
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				$dialog.dialog('close');

				var message = responseData.count + " item" + ((responseData.count == 1) ? "" : "s") + " moved to trash.";

				if (message.length > 0) {
					$.gritter.add({
						title: "Success",
						text: message,
						image: "/img/success.svg"
					});	
				}

				var urlVars = getUrlVars();
				delete urlVars.search;
				$('.index_table').attr('data-url-vars', '');
				updateRightSide(urlVars);
			} else showSubmitError($dialog);
		}).catch(function() {
			showSubmitError($dialog);
		});
	}
	
	$(document).bind('keydown', function (e) {
		if ($('.ui-dialog:visible').length == 0) {
	    	if (((e.key === 'Backspace') || (e.key === 'Delete')) && !$(e.target).is("input:not([readonly]), textarea")) {
				$('#deleteItem_button').trigger("click");
			}
		}
	});

	$('#deleteItem_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
				
		if (numChecked == 1) {
			var $itemName = $selectedCheckbox.closest('tr').find('.index_item_name').html();
			if (canExport) {
                $('#deleteItem_dialog').dialog('option', 'title', 'Move item to trash');
				$('#deleteItem_text').html('<p>Move the item "' + $itemName + '" to the trash?</p>');
                $('#deleteItem_confirm').text("Move to trash");
			} else {
                $('#deleteItem_dialog').dialog('option', 'title', 'Delete item');
                $('#deleteItem_text').html('<p>Delete the item "' + $itemName + '"?</p>');
                $('#deleteItem_confirm').text("Delete");
            }
		} else {
			if (canExport) {
                $('#deleteItem_dialog').dialog('option', 'title', 'Move item to trash');
				$('#deleteItem_text').html('<p>Move the ' + numChecked + ' selected items to the trash?</p>');
                $('#deleteItem_confirm').text("Move to trash");
			} else {
                $('#deleteItem_dialog').dialog('option', 'title', 'Delete item');
                $('#deleteItem_text').html('<p>Delete the ' + numChecked + ' selected items?</p>');
                $('#deleteItem_confirm').text("Delete");
            }
		}
		
		if (canExport) $('#delete_warning').hide();
		else $('#delete_warning').show();

		if (numChecked > 0) {
			$('#deleteItem_dialog').dialog('open');
		}
	});

	$('#deleteItem_dialog').dialog({
        autoOpen: false,
		width: 400,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': {
                id: 'deleteItem_confirm',
                click: deleteItem
            }
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$('#deleteItem_confirm').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		
	
	var duplicateItem = function(data) {
		if (!Folders.isItemFolder(treeData, data.folder_raw_id)) {

			$('#duplicateTargetPanel').validationEngine('showPrompt', "* Cannot place items in this folder", 'error', 'bottomLeft', true);

		} else {
	
			var $dialog = $('#duplicateItem_dialog');

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			var selectedItems = [];
			$('.item_select').each(function() {
				if ($(this).prop('checked')) {
					if (itemType == 'Question') {
                        var $row = $(this).closest('tr');
                        if ($row[0].hasAttribute('data-userquestion-id')) {
                            selectedItems.push({
                                user_question_id: $(this).closest('tr').attr('data-userquestion-id')
                            });    
                        } else {
                            selectedItems.push({
                                question_id: $(this).closest('tr').attr('data-question-id')
                            });    
                        }
					} else {
						var itemID = $(this).closest('tr').attr(itemIdTag);
						selectedItems.push(itemID);
					}
				}
			});

			var actionController = itemController;
			if (itemType == 'Question') actionController = 'UserQuestions';

			var targetId = data.folder_id;

			ajaxFetch('/' + actionController + '/copy', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					item_ids: selectedItems,
					folder_id: targetId,
					force_copy: true,
					is_new: 1
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();

				if (responseData.success) {
					if ('id' in responseData) {
						var target;
						if (itemType == 'Sitting') {
							target = '/Sittings/view/' + responseData.id;
						} else if (itemType == 'Question') {
							target = '/Questions/build/' + responseData.id + "?u=" + responseData.user_question_id;
						} else {
							target = '/' + itemType + 's/build/' + responseData.id;
						}
						window.location = target;
					} else if ('folder_id' in responseData) {
						$dialog.dialog('close');

						var urlVars = getUrlVars();
						delete urlVars.search;
						$('.index_table').attr('data-url-vars', '');
		
						var nodeID = Folders.getNodeId(treeData, responseData.folder_id);
						openNode(nodeID);
					}
				} else showSubmitError($dialog);
			}).catch(function() {
				showSubmitError($dialog);
			});
		}
	}
	
	$('#duplicateItem_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});

        $('#duplicateItem_dialog').dialog('option', 'title', 'Duplicate Item');

		if (numChecked == 1) {
			var $itemName = $selectedCheckbox.closest('tr').find('.index_item_name').html();
			$('#duplicateItem_text').html('<p>Choose a destination folder for duplicating item "' + $itemName + '", then click "Duplicate".</p>');
		} else {
			$('#duplicateItem_text').html('<p>Choose a destination folder for duplicating the ' + numChecked + ' selected items, then click "Duplicate".</p>');
		}
		
		Folders.openToNode($("#duplicateTargetPanel"), activeNode.id);

        getDuplicateTarget('Duplicate').then(duplicateItem);
	});

	$('#duplicateItem_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});
	
	var unsubscribe = function() {
		var $dialog = $(this);

    	if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/CommunityUsers/unsubscribe/' + activeCommunity.data.community_user_id, {
			method: 'POST'
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {

                addClientFlash({
                    title: "Success",
                    text: "Unsubscribed from community",
                    image: "/img/success.svg"
                });	

                window.location.reload();

			} else showSubmitError($dialog);
		}).catch(function() {
			showSubmitError($dialog);
		});
	}
	
	$('.community_unsubscribe').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;
		
		if (activeCommunity == null) return false;
		else if (isCommunityAdmin && (activeCommunity.data.admin_count == 1)) {
			if (activeCommunity.data.other_count > 0) {
				$('#unsubscribe_fail_text').html('You are the <b>last administrator</b> for the community "' + 
					activeCommunity.text + '". To unsubscribe, first choose "Edit community" and select a new \
					administrator or remove all other users.');
				$('#unsubscribe_fail_dialog').dialog('open');
			} else {
				$('#delete_community_text').html('You are the last member of the community "' + 
					activeCommunity.text + '". Are you sure you want to delete this community and its contents?');
				$('#delete_community_dialog').dialog('open');
			}
		} else {
			$('#unsubscribe_main_text').html('Unsubscribe from the community "' + activeCommunity.text + '"? \
				You will not be able to access content shared in this community after unsubscribing.');
			$('#unsubscribe_main_dialog').dialog('open');
		}
	});

	$('#unsubscribe_main_dialog').dialog({
        autoOpen: false,
		width: 500,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Unsubscribe': unsubscribe
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Unsubscribe")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});			

	$('#delete_community_dialog').dialog({
        autoOpen: false,
		width: 500,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': unsubscribe
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});			

	$('#unsubscribe_fail_dialog').dialog({
        autoOpen: false,
		width: 500,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Thanks!': function() {
               	$(this).dialog("close");
			}
		}, open: function(event) {
		   	$('.ui-dialog-buttonpane').find('button:contains("Thanks!")').addClass('btn btn-save btn_font');
	        $('.ui-dialog-buttonpane').find('button').blur();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});			

	$(document).on('click', '.header_sort', function() {
		var urlVars = getUrlVars();
		if (!('search' in urlVars)) {
			delete urlVars.page;
			urlVars.sort = $('#sort_type').val();
			urlVars.direction = $(this).attr('data-direction');
			updateRightSide(urlVars);
		}
	});
	
	$(document).on('change', '#sort_type', function() {
		var urlVars = getUrlVars();
		delete urlVars.page;
		urlVars.sort = $('#sort_type').val();
		var $selectedOption = $('#sort_type option:selected');
		urlVars.direction = $selectedOption.attr('data-default-direction');
		updateRightSide(urlVars);
	});
	
	$(document).on('change', '#show_limit', function() {
		var urlVars = getUrlVars();
		delete urlVars.page;
		urlVars.limit = $('#show_limit').val();
		updateRightSide(urlVars);
	});
	
	$('.search_dropdown').on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var $menu = $(this).parent().find('.search_menu');
		if (!$menu.is(":visible")) {
			$menu.show();
			$menu.find('input').focus();
			$menu.find('input').select();
		} else $menu.hide();
	});
	
	function updateSearch() {
		$('.search_menu').hide();
		var urlVars = getUrlVars();
		delete urlVars.sort;
		delete urlVars.direction;
		delete urlVars.page;
		if ((activeNode !== false) && (activeNode.data.folder_id != 'open')) {
			urlVars.where = $('#search_type').val();
		} else delete urlVars.where;
		urlVars.search = $('#search_wrapper .search_text').val();
		$('.index_table').attr('data-url-vars', '');
		updateRightSide(urlVars);
	}

	$('#search_type').on('change', function() {
		$('#search_wrapper .search_text').focus();
	});
	$('#search_wrapper .search_text').on('keypress', function(e) {
		if (e.key === 'Enter') updateSearch();
	});
	$('#search_wrapper .search_icon').on('click', updateSearch);
	
	$(document).on('click', function(e) {
		var $target = $(e.target);
		$('.search_menu').each(function() {
			if (!$target.is($(this)) && ($(this).find($target).length == 0)) $(this).hide();
		});
   	});
		
	$(document).on('click', '.index_folder_link', function(e) {
		e.stopPropagation();
		openNode($(this).attr('data-node-id'));
	});

	// Show received items
	
	var queuedDialogs = [];
	
	function showNextDialog() {
		if (queuedDialogs.length > 0) {
			queuedDialogs.pop().dialog('open');
		}
	}

	$('#receiveCopy_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Thanks!': function() {
               	$(this).dialog("close");
               	showNextDialog();
			}
		}, open: function(event) {
		   	$('.ui-dialog-buttonpane').find('button:contains("Thanks!")').addClass('btn btn-save btn_font');
	        $('.ui-dialog-buttonpane').find('button').blur();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});
	
	if (receivedCount > 0) {
		var json = Folders.getSelectedNode("treePanel");
		if (json.id != roots['Home'].id) {
			openNode(roots['Home'].id);
		}

		$('#receiveCopy_text').html('<p><b>You have received ' + receivedCount + ' item' + ((receivedCount == 1) ? '' : 's') + ' from other users.</b></p><p>Received items will be indicated with a "New" tag in your Home folder.</p>');
		queuedDialogs.push($('#receiveCopy_dialog'));
	}

    $('#index_resizer').on('mousedown', function(e) {
        $('body').addClass('noselect');

        // Store the initial CSS for grid-template-columns, replacing the last column with the uncomputed value

        var columnElements = $('#index_wrapper').css('grid-template-columns').split(' ');
        columnElements[columnElements.length - 1] = 'auto';
        resizeInitialStyle = columnElements;
    });

    function cancelResize() {
        var columnElements = resizeInitialStyle;
        $('#index_wrapper').css('grid-template-columns', columnElements.join(' '));
    }

    function stopResize() {
        resizeInitialStyle = false;
        $('body').removeClass('noselect');
    }
      
    $(document).on('mouseup', function(e) {
        if (resizeInitialStyle === false) return;
        stopResize();
    });

    $(document).on('mouseleave', function(e) {
        if (resizeInitialStyle === false) return;
        cancelResize();
        stopResize();
    });

    $(window).on('blur', function(e) {
        if (resizeInitialStyle === false) return;
        cancelResize();
        stopResize();
    });

    $(document).on('mousemove', function(e) {
        if (resizeInitialStyle === false) return;

        // Set the new width of the left column

        var newWidth = e.clientX - $('#index_wrapper').offset().left;
        if ((newWidth > minFolderWidth) && (newWidth < maxFolderWidth)) {
            var columnElements = resizeInitialStyle;
            columnElements[1] = newWidth + 'px';
            $('#index_wrapper').css('grid-template-columns', columnElements.join(' '));
        }
    });

	var urlVars = getUrlVars();
	if ('sort' in urlVars) {
		$('#sort_type').val(urlVars.sort);
	}

	showNextDialog();

}

export { initialize, 
    findCommunityNodeID,
    roots, activeNode, activeCommunity,
    highlightPrevious, highlightNext };
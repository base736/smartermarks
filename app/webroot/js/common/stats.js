/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as ScoringCommon from '/js/common/scoring.js?7.0';

var epsilon = 1.0e-5;

var fullStatistics = {};
var filteredStatistics = {};

const difficultyBins = 7;
const difficultyLowerLimit = 0.05;
const difficultyUpperLimit = 0.95;
const difficultyDangerFraction = 1.0;
const difficultyDangerBins = 1;
const difficultyWarningFraction = 0.5;
const difficultyWarningBins = 1;

const discriminationBins = 7;
const discriminationDanger = 0.1;
const discriminationDangerBins = 1;
const discriminationWarning = 0.3;
const discriminationWarningBins = 2;

const correlationDanger = 0.1;
const correlationWarning = 0.3;

const neutralColour = 'hsl(0, 0%, 75%)';
const incorrectColour = 'hsl(0, 85%, 45%)';
const partialColour= 'hsl(40, 94%, 70%)';
const correctColour = 'hsl(120, 47%, 45%)';

const dangerColour = 'hsl(0, 84%, 85%)';
const warningColour = 'hsl(40, 95%, 85%)';
const goodColour = 'hsl(120, 47%, 85%)';

const variablesRegex = /<(span)[^>]*?\bclass\s*=\s*"[^"]*\bblock_variable\b[^"]*"[^>]*>(.*?)<\/\1>/;

function hasVariables(questionEntry) {
    var hasVariables = false;
    if ('Answers' in questionEntry) {
        for (let questionAnswer of questionEntry.Answers) {
            if (variablesRegex.test(questionAnswer.template)) hasVariables = true;
        }            
    }
    
    return hasVariables;
}

function getAverageScore(partStatistics, partData) {
    var xTotal = 0.0;
    var nTotal = 0;

    for (var resultsIndex = 0; resultsIndex < partStatistics.length; ++resultsIndex) {
        if (partData.type == 'wr') {
	
            var thisSums = partStatistics[resultsIndex];

            nTotal += thisSums.n;
            xTotal += thisSums.sumX;
    
        } else {
    
            for (var responseIndex = 0; responseIndex < partStatistics[resultsIndex].length; ++responseIndex) {
                var thisSums = partStatistics[resultsIndex][responseIndex];

                var x = 0.0;
                if ('ids_matched' in thisSums) {
                
                    var sums_json = JSON.stringify(thisSums.ids_matched);

                    for (var j = 0; j < partData.ScoredResponses.length; ++j) {
                        var thisScore = partData.ScoredResponses[j].value;
    
                        var scoredIDs = false;
                        if ('id' in partData.ScoredResponses[j]) {
                            scoredIDs = [partData.ScoredResponses[j].id];
                        } else if ('ids' in partData.ScoredResponses[j]) {
                            scoredIDs = partData.ScoredResponses[j].ids;
                            scoredIDs.sort();
                        }
    
                        if (scoredIDs !== false) {
                            var scored_json = JSON.stringify(scoredIDs);
                            if (scored_json == sums_json) x = thisScore;
                        }
                    }
                    
                } else if ('response' in thisSums) {

                    var partStyle = partData.type.substr(0, 2);

                    var thisHasVariables;
                    if ('hasVariables' in partData) thisHasVariables = partData.hasVariables;
                    else thisHasVariables = hasVariables(partData);

                    if ((partStyle == 'nr') && !thisHasVariables) {
                        var response = thisSums.response;
                        var result = ScoringCommon.scoreNumericResponse(response, partData);
                        x = (result === false) ? false : result.score;
                    }

                }

                if (x !== false) {
                    var maxScore = ScoringCommon.getMaxScore(partData);
                    if (maxScore > 0.0) x /= maxScore;
    
                    xTotal += thisSums.n * x;
                    nTotal += thisSums.n;    
                }
            }
        }
    }

    if (nTotal > 0) return xTotal / nTotal;
    else return false;
}

function getDiscrimination(partStatistics, partData) {
    var discriminationSum = 0.0;
    var nTotal = 0;
    
    for (var resultsIndex = 0; resultsIndex < partStatistics.length; ++resultsIndex) {
        var n, sumX, sumY, sumX2, sumXY, sumY2;

        if (partData.type == 'wr') {
	
            var thisSums = partStatistics[resultsIndex];

            n = thisSums.n;
            sumX = thisSums.sumX;
            sumY = thisSums.sumY;
            sumX2 = thisSums.sumX2;
            sumXY = thisSums.sumXY;
            sumY2 = thisSums.sumY2;

        } else {
    
            n = 0;
            sumX = 0.0;
            sumY = 0.0;
            sumX2 = 0.0;
            sumXY = 0.0;
            sumY2 = 0.0;

            for (var responseIndex = 0; responseIndex < partStatistics[resultsIndex].length; ++responseIndex) {
                var thisSums = partStatistics[resultsIndex][responseIndex];

                var x = 0.0;
                if ('ids_matched' in thisSums) {
                
                    var sums_json = JSON.stringify(thisSums.ids_matched);

                    for (var j = 0; j < partData.ScoredResponses.length; ++j) {
                        var thisScore = partData.ScoredResponses[j].value;
    
                        var scoredIDs = false;
                        if ('id' in partData.ScoredResponses[j]) {
                            scoredIDs = [partData.ScoredResponses[j].id];
                        } else if ('ids' in partData.ScoredResponses[j]) {
                            scoredIDs = partData.ScoredResponses[j].ids;
                            scoredIDs.sort();
                        }
    
                        if (scoredIDs !== false) {
                            var scored_json = JSON.stringify(scoredIDs);
                            if (scored_json == sums_json) x = thisScore;
                        }
                    }
                    
                } else if ('response' in thisSums) {

                    var partStyle = partData.type.substr(0, 2);

                    var thisHasVariables;
                    if ('hasVariables' in partData) thisHasVariables = partData.hasVariables;
                    else thisHasVariables = hasVariables(partData);

                    if ((partStyle == 'nr') && !thisHasVariables) {
                        var response = thisSums.response;
                        var result = ScoringCommon.scoreNumericResponse(response, partData);
                        x = (result === false) ? false : result.score;   
                    }

                }

                if (x !== false) {
                    var maxScore = ScoringCommon.getMaxScore(partData);
                    if (maxScore > 0.0) x /= maxScore;
    
                    sumX += thisSums.n * x;
                    sumY += thisSums.sumY;
                    sumX2 += thisSums.n * x * x;
                    sumXY += x * thisSums.sumY;
                    sumY2 += thisSums.sumY2;
                    n += thisSums.n;
                }    
            }
        }

        if (n > 0) {
            var numerator = (sumXY / n) - (sumX / n) * (sumY / n);
            var sigmaX = Math.sqrt((sumX2 / n) - Math.pow(sumX / n, 2.0));
            var sigmaY = Math.sqrt((sumY2 / n) - Math.pow(sumY / n, 2.0));
            if ((sigmaX >= epsilon) && (sigmaY >= epsilon)) {
                discriminationSum += n * numerator / (sigmaX * sigmaY);
                nTotal += n;
            }
        }    
    }

    if (nTotal > 0) return discriminationSum / nTotal;
    else return 0.0;
}

function getStatsBlocks($wrapper, questionStatistics, questionData, renderAsVersion = false) {
    var statsData = JSON.parse(questionStatistics.Question.stats_json);

	var blocks = {};
	for (var partIndex = 0; partIndex < questionData.Parts.length; ++partIndex) {
		var partData = JSON.parse(JSON.stringify(questionData.Parts[partIndex]));
		if (partData.type == 'context') continue;
		
        var partAnswers;
        if ('Common' in questionData) partAnswers = questionData.Common.Answers;
        else partAnswers = partData.Answers;

        var $newElement = $("<div class='part_statistics_wrapper'></div>");

		if (!(partData.id in questionStatistics.QuestionParts)) {
            $newElement.append("<div style='font-style:italic;'>No statistics available</div>");
		} else {
			var partStatistics = JSON.parse(JSON.stringify(questionStatistics.QuestionParts[partData.id]));

            $newElement.append(
                "<div class='stats_count_wrapper'>" +
                    "<div class='part_statistics_label'>Responses:</div>" +
                    "<div class='part_statistics_value'></div>" + 
                "</div>")
			
			var difficulty = getAverageScore(partStatistics, partData) * 100;

            var lowerLimit, upperLimit;
            if (partData.type.substr(0, 2) == 'mc') {
                lowerLimit = 1.0 / partAnswers.length;
                upperLimit = 1.0 - 0.03 * (partAnswers.length - 1);
            } else {
                lowerLimit = difficultyLowerLimit;
                upperLimit = difficultyUpperLimit;
            }
            var idealAverage = (lowerLimit + 1.0) / 2.0 + 0.1;

            var lowerWarning = idealAverage - (idealAverage - lowerLimit) * difficultyWarningFraction;
            var upperWarning = idealAverage + (upperLimit - idealAverage) * difficultyWarningFraction;
            
            var lowerDanger = idealAverage - (idealAverage - lowerLimit) * difficultyDangerFraction;
            var upperDanger = idealAverage + (upperLimit - idealAverage) * difficultyDangerFraction;

			var widthA = lowerDanger * 100;
			var widthB = (lowerWarning - lowerDanger) * 100;
			var widthC = (upperWarning - lowerWarning) * 100;
			var widthD = (upperDanger - upperWarning) * 100;

            $newElement.append(
				"<div class='stats_difficulty_wrapper' style='margin-top:10px;'>" + 
					"<div class='part_statistics_label' style='vertical-align:middle;'>Average score:</div>" + 
                    "<div style='display:inline-block; vertical-align:middle; width:100px; height:13px; border:1px solid black; background-color:" + dangerColour + "; position:relative;'>" +
                        "<div class='stats_indicator' style='left:" + difficulty + "%;'>&#9660</div>" +
                        "<div style='display:inline-block; height:100%; width:" + widthA + "%; background-color:" + dangerColour + "'></div>" +
                        "<div style='display:inline-block; height:100%; width:" + widthB + "%; background-color:" + warningColour + "'></div>" +
                        "<div style='display:inline-block; height:100%; width:" + widthC + "%; background-color:" + goodColour + "'></div>" +
                        "<div style='display:inline-block; height:100%; width:" + widthD + "%; background-color:" + warningColour + "'></div>" +
                    "</div>" +
                    "<div class='part_statistics_value' style='vertical-align:middle; margin-left:5px;'>" + difficulty.toFixed(0) + "%</div>" + 
                    "<div class='part_score_distribution' style='display:inline-block; vertical-align:middle; margin-left:20px;'></div>" +
				"</div>");
			
			var discrimination = getDiscrimination(partStatistics, partData);
			var discriminationString = discrimination.toFixed(2);

			var barDiscrimination = Math.max(discrimination, 0) * 100;

            $newElement.append(
				"<div class='stats_discrimination_wrapper' style='margin-top:10px; margin-bottom:10px;'>" + 
					"<div class='part_statistics_label' style='vertical-align:middle;'>Discrimination:</div>" + 
                    "<div style='display:inline-block; vertical-align:middle; width:100px; height:13px; border:1px solid black; background-color:" + goodColour + "; position:relative'>" +
                        "<div class='stats_indicator' style='left:" + barDiscrimination + "%;'>&#9660</div>" +
                        "<div style='display:inline-block; height:100%; width:" + (discriminationDanger * 100).toFixed(2) + "%; background-color:" + dangerColour + "'></div>" +
                        "<div style='display:inline-block; height:100%; width:" + ((discriminationWarning - discriminationDanger) * 100).toFixed(2) + "%; background-color:" + warningColour + "'></div>" +
                    "</div>" +
                    "<div class='part_statistics_value' style='vertical-align:middle; margin-left:5px;'>" + discriminationString + "</div>" + 
				"</div>");

            if (showCorrelation && (partData.id in statsData) && ('correlation' in statsData[partData.id])) {

                var correlation = statsData[partData.id].correlation;
                var correlationString = correlation.toFixed(2);
    
                var barCorrelation = Math.max(correlation, 0) * 100;

                $newElement.append(
                    "<div class='stats_correlation_wrapper' style='margin-top:10px; margin-bottom:10px;'>" + 
                        "<div class='part_statistics_label' style='vertical-align:middle;'>Correlation:</div>" + 
                        "<div style='display:inline-block; vertical-align:middle; width:100px; height:13px; border:1px solid black; background-color:" + goodColour + "; position:relative'>" +
                            "<div class='stats_indicator' style='left:" + barCorrelation + "%;'>&#9660</div>" +
                            "<div style='display:inline-block; height:100%; width:" + (correlationDanger * 100).toFixed(2) + "%; background-color:" + dangerColour + "'></div>" +
                            "<div style='display:inline-block; height:100%; width:" + ((correlationWarning - correlationDanger) * 100).toFixed(2) + "%; background-color:" + warningColour + "'></div>" +
                        "</div>" +
                        "<div class='part_statistics_value' style='vertical-align:middle; margin-left:5px;'>" + correlationString + "</div>" + 
                    "</div>");
    
            }
        
			if (partData.type == 'wr') {

                var allMarksCount = 0;
				for (var i = 0; i < partStatistics.length; ++i) {
                    allMarksCount += partStatistics[i].n;
                }

                $newElement.find('.stats_count_wrapper .part_statistics_value').html(allMarksCount);

            } else {

				var partStyle = partData.type.substr(0, 2);

				// Get bar data

				var rowData = [];

                var maxIDCount = 1;
				if (partStyle == 'mc') {
					for (var i = 0; i < partAnswers.length; ++i) {
						var answerIDs = [partAnswers[i].id];
						rowData.push({
							ids_matched: JSON.stringify(answerIDs),
							n: 0,
                            sumU: 0.0,
                            sumU2: 0.0
						});
					}
					for (var i = 0; i < partData.ScoredResponses.length; ++i) {
						if ('ids' in partData.ScoredResponses[i]) {
							var answerIDs = partData.ScoredResponses[i].ids;
                            if (answerIDs.length > maxIDCount) maxIDCount = answerIDs.length;

							answerIDs.sort();
							rowData.push({
								ids_matched: JSON.stringify(answerIDs),
								n: 0,
                                sumU: 0.0,
                                sumU2: 0.0
							});	
						}
					}
				}

                var allMarksCount = 0;
				for (var i = 0; i < partStatistics.length; ++i) {

                    // Get mean and standard deviation for this question in this assessment

                    var n = 0;
                    var sumY = 0.0;
                    var sumY2 = 0.0;

                    for (var j = 0; j < partStatistics[i].length; ++j) {
                        n += partStatistics[i][j].n;
                        sumY += partStatistics[i][j].sumY;
                        sumY2 += partStatistics[i][j].sumY2;
                    }

                    allMarksCount += n;

                    var testMean = sumY / n;
                    var testStandardDeviation = Math.sqrt(sumY2 / n - testMean * testMean);

                    // Get row data for each response

                    for (var j = 0; j < partStatistics[i].length; ++j) {
                        var sumU, sumU2;
                        if ((partStatistics[i][j].n > 0) && (testStandardDeviation > epsilon)) {
                            sumU = (partStatistics[i][j].sumY - testMean * partStatistics[i][j].n) / testStandardDeviation;
                            sumU2 = (partStatistics[i][j].sumY2 - 2 * partStatistics[i][j].sumY * testMean + 
                                testMean * testMean * partStatistics[i][j].n) / (testStandardDeviation * testStandardDeviation);
                        } else {
                            sumU = 0.0;
                            sumU2 = 0.0;
                        }

                        if ('ids_matched' in partStatistics[i][j]) {

                            var showRow;
                            if (partStatistics[i][j].ids_matched.length > maxIDCount) showRow = false;
                            else showRow = true;

                            if (showRow) {
                                var idString = JSON.stringify(partStatistics[i][j].ids_matched);

                                var index = 0;
                                for (index = 0; index < rowData.length; ++index) {
                                    if (idString == rowData[index].ids_matched) break;
                                }
                                if (index < rowData.length) {
                                    rowData[index].n += partStatistics[i][j].n;
                                    rowData[index].sumU += sumU;
                                    rowData[index].sumU2 += sumU2;
                                } else  {
                                    rowData.push({
                                        ids_matched: idString,
                                        n: partStatistics[i][j].n,
                                        sumU: sumU,
                                        sumU2: sumU2
                                    });	
                                }
                            }

                        } else if ('response' in partStatistics[i][j]) {

                            var thisHasVariables;
                            if ('hasVariables' in partData) thisHasVariables = partData.hasVariables;
                            else thisHasVariables = hasVariables(partData);

                            var showRow;
                            if (partStyle != 'nr') showRow = true;
                            else if (!thisHasVariables) showRow = true;
                            else showRow = renderAsVersion;

                            if (showRow) {
                                var response = partStatistics[i][j].response;

                                if (partData.type == 'nr_selection') {
                                    if (partData.TypeDetails.ignoreOrder) {
    
                                        var responseParts = response.split('');
                                        responseParts.sort();
                                        response = responseParts.join('').trim();
    
                                    } else {
    
                                        var fullResult = ScoringCommon.scoreNumericResponse(response, partData);
    
                                        var trimmedResponse = response.trim();
                                        var trimmedResult = ScoringCommon.scoreNumericResponse(trimmedResponse, partData);

                                        if ((fullResult !== false) && (trimmedResult !== false)) {
                                            if (fullResult.score == trimmedResult.score) response = trimmedResponse;
                                            else response = response.replace(' ', '_');    
                                        } else response = '';
                                                                                
                                    }
                                } else response = response.trim();
    
                                if (response.length > 0) {

                                    var matchingRow = 0;
                                    for (matchingRow = 0; matchingRow < rowData.length; ++matchingRow) {
                                        if (response == rowData[matchingRow].response) break;
                                    }

                                    if (matchingRow == rowData.length) {
                                        rowData.push({
                                            response: response,
                                            n: partStatistics[i][j].n,
                                            sumU: sumU,
                                            sumU2: sumU2
                                        });
                                    } else {
                                        rowData[matchingRow].n += partStatistics[i][j].n;
                                        rowData[matchingRow].sumU += sumU;
                                        rowData[matchingRow].sumU2 += sumU2;
                                    }
                                }

                            }

                        }
                    }
                }

                $newElement.find('.stats_count_wrapper .part_statistics_value').html(allMarksCount);

                // Get max score and max decimals for this part

                var maxScore = ScoringCommon.getMaxScore(partData);

                var maxDecimals = 1;
                for (var j = 0; j < partData.ScoredResponses.length; ++j) {
                    var thisValue = partData.ScoredResponses[j].value;

                    var valueString = thisValue.toFixed(3);
                    while (valueString[valueString.length - 1] == '0') {
                        valueString = valueString.substr(0, valueString.length - 1);
                    }    
                    var numDecimals = valueString.length - valueString.indexOf('.') - 1;
                    if (numDecimals > maxDecimals) maxDecimals = numDecimals;
                }

                var fullMarksCount = 0;
                var partialMarksCount = 0;
				for (var i = 0; i < rowData.length; ++i) {

                    // Get response text for ids_matched entries

                    if ('ids_matched' in rowData[i]) {

                        var answerIDs = JSON.parse(rowData[i].ids_matched);

                        if (partData.type == 'mc_truefalse') {
                            if (answerIDs.length == 1) {
                                var answerID = answerIDs[0];
                                for (var j = 0; j < partAnswers.length; ++j) {
                                    if (partAnswers[j].id == answerID) {
                                        rowData[i].response = partAnswers[j].template[0];
                                    }
                                }
                            }
                        } else {
                            var responseText = '';
                            for (var j = 0; j < answerIDs.length; ++j) {
                                var $answerWrapper = $wrapper.find(".question_answer[data-answer-id='" + answerIDs[j] + "']");
                                if ($answerWrapper.length == 1) {
                                    if (partStyle == 'mc') {
                                        var mcLabel = $answerWrapper.find('.answer_label').text();
                                        responseText += mcLabel.substr(0, mcLabel.length - 1);
                                    } else responseText += $answerWrapper.text();
                                }	
                            }
                            if (partStyle == 'mc') {
                                var responseLetters = responseText.split('');
                                responseLetters.sort();
                                responseText = responseLetters.join('');
                            }

                            rowData[i].response = responseText;
                        }

                    } else if (partData.type == 'nr_scientific') {

                        var rawText = rowData[i].response;
                        if (/^\d+$/.test(rawText)) {

                            // Format scientific-notation answers as strings like "12.3e4"

                            var bestValue = 0.0;
                            for (var j = 0; j < partData.ScoredResponses.length; ++j) {
                                if (partData.ScoredResponses[j].value > bestValue) bestValue = partData.ScoredResponses[j].value;
                            }

                            var numExponent = 1;
                            var valueIsNegative = false;
                            var exponentIsNegative = false;
                            
                            $wrapper.find(".question_answer").each(function() {
                                var answerID = $(this).attr('data-answer-id');

                                var value = 0.0;
                                for (var j = 0; j < partData.ScoredResponses.length; ++j) {
                                    if (('id' in partData.ScoredResponses[j]) && (partData.ScoredResponses[j].id == answerID)) {
                                        value = partData.ScoredResponses[j].value;
                                        break;
                                    }
                                }

                                if ((value > 0.0) && (value == bestValue)) {
                                    var exponent = null;
                                    var parts = $(this)[0].innerText.split(/[eE]/);
                                    if (parts.length == 2) {
                                        var coefficient = parts[0];
                                        if (coefficient[0] == '-') {
                                            valueIsNegative = true;
                                        }
        
                                        var exponent = parts[1];
                                        if (exponent[0] == '-') {
                                            exponentIsNegative = true;
                                            exponent = exponent.substring(1);
                                        } else if (exponent[0] == '+') {
                                            exponent = exponent.substring(1);
                                        }
                                        if (exponent.length > numExponent) numExponent = exponent.length;
                                    }
                                }
            
                            });

                            var exponentStr = rawText.slice(-numExponent);
                            if (exponentIsNegative) exponentStr = '-' + exponentStr;

                            const coefficientStr = rawText.slice(0, rawText.length - numExponent);
                            var formattedCoefficient = coefficientStr.charAt(0) + '.' + coefficientStr.slice(1);
                            if (valueIsNegative) formattedCoefficient = '-' + formattedCoefficient;

                            rowData[i].displayText = `${formattedCoefficient}e${exponentStr}`;
                            
                        } else {

                            // Leave the raw string as is if the response contains a decimal, for instance.

                            rowData[i].response = rawText;

                        }

                    }

                    // Get score and score type

                    rowData[i].value = 0.0;
                    rowData[i].scoreType = 'incorrect';
                    
                    if ('ids_matched' in rowData[i]) {

                        var ids_matched = rowData[i].ids_matched;
        
                        for (var j = 0; j < partData.ScoredResponses.length; ++j) {
                            var scoredIDs = false;
                            if ('id' in partData.ScoredResponses[j]) {
                                scoredIDs = [partData.ScoredResponses[j].id];
                            } else if ('ids' in partData.ScoredResponses[j]) {
                                scoredIDs = partData.ScoredResponses[j].ids;
                                scoredIDs.sort();
                            }

                            if (scoredIDs !== false) {
                                var scored_json = JSON.stringify(scoredIDs);
                                if (scored_json == ids_matched) {
                                    rowData[i].value = partData.ScoredResponses[j].value;
                                }
                            }
                        }

                    } else if ((partStyle == 'nr') && ('response' in rowData[i])) {

                        var result = ScoringCommon.scoreNumericResponse(rowData[i].response, partData);
                        rowData[i].value = (result === false) ? 0.0 : result.score;
                        
                    }

                    if (rowData[i].value == maxScore) {
                        rowData[i].scoreType = 'correct';
                        fullMarksCount += rowData[i].n;
                    } else if (rowData[i].value > 0.0) {
                        rowData[i].scoreType = 'partial';
                        partialMarksCount += rowData[i].n;
                    }
                }

				// Sort responses

                if ((partStyle == 'mc') && (partData.type != 'mc_truefalse')) {

					rowData.sort(function(a, b) {
						if (a.response.length > b.response.length) return 1;
						else if (a.response.length < b.response.length) return -1;
						if (a.response > b.response) return 1;
						else if (a.response < b.response) return -1;
						else return 0;
					});

				} else if (partStyle == 'nr') {

					rowData.sort(function(a, b) {
						if (a.n > b.n) return -1;
						else if (a.n < b.n) return 1;
                        else if (a.value > b.value) return -1;
                        else if (a.value < b.value) return 1;
						else return 0;
					});

				}

                // Build table

                var hasTable = false;
                for (var i = 0; i < rowData.length; ++i) {
                    var responseText = false;
                    if ('displayText' in rowData[i]) responseText = rowData[i].displayText;
                    else if ('response' in rowData[i]) responseText = rowData[i].response;

                    if (responseText.length > 0) {
                        if (!hasTable) {
                            $newElement.append(
                                "<table class='stats_table'>" +
                                    "<tr>" +
                                        "<td colspan='2'>Response distribution<span class='stats_show_count'></span></td>" + 
                                        "<td style='padding-left:10px;'>Marks</td>" +
                                        "<td></td>" +
                                        "<td>Test score ranges</td>" +
                                    "</tr>" +
                                "</table>");

                            hasTable = true;
                        }

                        if (partStyle == 'mc') responseText += '.';

                        var barColour = neutralColour;
                        if (rowData[i].scoreType == 'incorrect') barColour = incorrectColour;
                        else if (rowData[i].scoreType == 'partial') barColour = partialColour;
                        else if (rowData[i].scoreType == 'correct') barColour = correctColour;
                        
                        var barPercent = (rowData[i].n * 100 / allMarksCount).toFixed(0);
                        
                        var $tableRow = $("<tr class='stats_table_element' data-grid-row='" + (i + 1) + "'></tr>");

                        $tableRow.append("<td style='padding-right:10px;'>" + responseText + "</td>");
                        $tableRow.append(
                            "<td style='position:relative; border:1px solid black; width:200px; height:15px; display:flex; align-items:center;'>" +
                                "<div class='stats_response_bar' style='background-color:" + barColour + "; width:" + barPercent + "%; height:100%; display:flex; justify-content:flex-end; align-items:center;'>" +
                                    "<div class='stats_score_label' style='font-size:12px; color:white; padding:0px 5px;'>" + 
                                        barPercent + "%</div>" +
                                "</div>" +
                            "</td>");
                        $tableRow.append("<td style='width:40px; padding-left:10px;'>" + rowData[i].value.toFixed(maxDecimals) + "</td>");
                        $tableRow.append("<td style='width:40px;'></td>");

                        if (rowData[i].n > 5) {
                            var average = rowData[i].sumU / rowData[i].n;
                            var standardDeviation = Math.sqrt(rowData[i].sumU2 / rowData[i].n - average * average);
                            var left = (average - standardDeviation) * 20 + 50;
                            var width = 2.0 * standardDeviation * 20;

                            $tableRow.append(
                                "<td style='border:1px solid black; width:200px; height:13px;'>" +
                                    "<div style='position:relative; left:" + left + "%; background-color:#ccc; width:" + width + "%; height:100%;'></div>" +
                                "</td>");
                        } else {
                            $tableRow.append("<td>&mdash;</td>");
                        }

                        $newElement.find('.stats_table').append($tableRow);
                    }
                }

                if (partialMarksCount > 0) {
                    $newElement.find('.part_score_distribution').html("(" + (100.0 * fullMarksCount / allMarksCount).toFixed(0) + "% at full marks)");
                }

                if (!hasTable) {
                    $newElement.append("<div style='font-style:italic;'>No statistics available</div>");
                }
			}
		}
		blocks[partData.id] = $newElement[0].outerHTML;
	}

	return blocks;
}

function fixScoreLabels($wrapper) {
    $wrapper.find('.stats_response_bar>.stats_score_label').each(function() {
        var $label = $(this);
        var $bar = $label.parent();
        var $barWrapper = $bar.parent();

        if ($bar.outerWidth() + $label.outerWidth() < $barWrapper.width()) {
            $label.insertAfter($bar);
            $label.css('color', 'black');
        }
    });
}

function getAssessmentStatistics(questionHandleIDs, questionData) {

	var values = {};

	var outOf = 0.0;
	var totalScore = 0.0;
	var numIncluded = 0;
	var numExcluded = 0;

	var difficultyCounts = [];			
	for (var i = 0; i < difficultyBins; ++i) {
		difficultyCounts.push(0);
	}

	var discriminationCounts = [];			
	for (var i = 0; i < discriminationBins; ++i) {
		discriminationCounts.push(0);
	}

	for (var i = 0; i < questionHandleIDs.length; ++i) {
		var binIncrement = 1.0 / questionHandleIDs[i].length;

		for (var j = 0; j < questionHandleIDs[i].length; ++j) {
			var questionHandleID = questionHandleIDs[i][j];

			var questionID = questionData[questionHandleID].id;
			if (questionID in filteredStatistics) {
				var itemStatistics = filteredStatistics[questionID];

				var itemData = questionData[questionHandleID].json_data;
				for (var partIndex = 0; partIndex < itemData.Parts.length; ++partIndex) {
					if (itemData.Parts[partIndex].type == 'context') continue;
	
					if (itemData.Parts[partIndex].id in itemStatistics.QuestionParts) {

						// Get part data, replacing answers with common answers if needed

						var partData = JSON.parse(JSON.stringify(itemData.Parts[partIndex]));
						if ('Common' in itemData) partData.Answers = itemData.Common.Answers;

						// Get the maximum possible score for this part
		
						var partOutOf = ScoringCommon.getMaxScore(partData);

						// Get statistics

						var partStatistics = itemStatistics.QuestionParts[partData.id];
						
						// Add average score to bins

                        var averageScore = getAverageScore(partStatistics, partData);

						outOf += partOutOf * binIncrement;
						totalScore += averageScore * partOutOf * binIncrement;
						numIncluded++;
	
                        var lowerLimit, upperLimit;
                        if (partData.type.substr(0, 2) == 'mc') {
                            lowerLimit = 1.0 / partData.Answers.length;
                            upperLimit = 1.0 - 0.03 * (partData.Answers.length - 1);
                        } else {
                            lowerLimit = difficultyLowerLimit;
                            upperLimit = difficultyUpperLimit;
                        }
                        var idealAverage = (lowerLimit + 1.0) / 2.0 + 0.1;
            
                        var lowerWarning = idealAverage - (idealAverage - lowerLimit) * difficultyWarningFraction;
                        var upperWarning = idealAverage + (upperLimit - idealAverage) * difficultyWarningFraction;
                        
                        var lowerDanger = idealAverage - (idealAverage - lowerLimit) * difficultyDangerFraction;
                        var upperDanger = idealAverage + (upperLimit - idealAverage) * difficultyDangerFraction;
                            
                        var binNum;
                        if (averageScore < lowerDanger) {
                            var binBase = 0;
                            var binSpan = difficultyDangerBins;
                            var fraction = (averageScore - 0) / (lowerWarning - 0);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else if (averageScore < lowerWarning) {
                            var binBase = difficultyDangerBins;
                            var binSpan = difficultyWarningBins;
                            var fraction = (averageScore - lowerDanger) / (lowerWarning - lowerDanger);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else if (averageScore < upperWarning) {
                            var binBase = difficultyDangerBins + difficultyWarningBins;
                            var binSpan = difficultyBins - 2 * difficultyDangerBins - 2 * difficultyWarningBins;
                            var fraction = (averageScore - lowerWarning) / (upperWarning - lowerWarning);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else if (averageScore < upperDanger) {
                            var binBase = difficultyBins - difficultyDangerBins - difficultyWarningBins;
                            var binSpan = difficultyWarningBins;
                            var fraction = (averageScore - upperWarning) / (upperDanger - upperWarning);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else {
                            var binBase = difficultyBins - difficultyDangerBins;
                            var binSpan = difficultyDangerBins;
                            var fraction = (averageScore - upperDanger) / (1.0 - upperDanger);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        }
        
                        difficultyCounts[binNum] += binIncrement;

						// Add discrimination to bins
		
						var discrimination = getDiscrimination(partStatistics, partData);

						if (discrimination !== false) {
                            binNum;
                            if (discrimination < discriminationDanger) {
                                var binBase = 0;
                                var binSpan = discriminationDangerBins;
                                var fraction = (discrimination - 0) / (discriminationWarning - 0);
                                binNum = binBase + Math.floor(fraction * binSpan);
                                if (fraction == 1.0) binNum--;
                            } else if (discrimination < discriminationWarning) {
                                var binBase = discriminationDangerBins;
                                var binSpan = discriminationWarningBins;
                                var fraction = (discrimination - discriminationDanger) / (discriminationWarning - discriminationDanger);
                                binNum = binBase + Math.floor(fraction * binSpan);
                                if (fraction == 1.0) binNum--;
                            } else {
                                var binBase = discriminationDangerBins + discriminationWarningBins;
                                var binSpan = discriminationBins - discriminationDangerBins - discriminationWarningBins;
                                var fraction = (discrimination - discriminationWarning) / (1.0 - discriminationWarning);
                                binNum = binBase + Math.floor(fraction * binSpan);
                                if (fraction == 1.0) binNum--;
                            }
            
                            discriminationCounts[binNum] += binIncrement;
                        }						
					} else numExcluded++;
				}
			}
		}
	}

	values.n = numIncluded;
	values.nExcluded = numExcluded;
	values.average = totalScore * 100.0 / outOf;

    // Build difficulty graph

	var difficultyGraphPoints = [];
    var maxDifficultyCount = 0;
	for (var i = 0; i < difficultyBins; ++i) {
        if (difficultyCounts[i] > maxDifficultyCount) {
            maxDifficultyCount = difficultyCounts[i];
        }

        difficultyGraphPoints.push([100 * i / difficultyBins, difficultyCounts[i]]);
		difficultyGraphPoints.push([100 * (i + 1) / difficultyBins, difficultyCounts[i]]);
	}

    values.difficulties = {
        data: [{
            data: difficultyGraphPoints.slice(0, 2),
            lines: {
                show: true,
                lineWidth: 0,
                fill: 1
            },
            color: jQuery.Color(dangerColour).toHexString()
        }, {
            data: difficultyGraphPoints.slice(2, 4),
            lines: {
                show: true,
                lineWidth: 0,
                fill: 1
            },
            color: jQuery.Color(warningColour).toHexString()
        }, {
            data: difficultyGraphPoints.slice(4, difficultyGraphPoints.length - 4),
            lines: {
                show: true,
                lineWidth: 0,
                fill: 1
            },
            color: jQuery.Color(goodColour).toHexString()
        }, {
            data: difficultyGraphPoints.slice(difficultyGraphPoints.length - 4, difficultyGraphPoints.length - 2),
            lines: {
                show: true,
                lineWidth: 0,
                fill: 1
            },
            color: jQuery.Color(warningColour).toHexString()
        }, {
            data: difficultyGraphPoints.slice(difficultyGraphPoints.length - 2),
            lines: {
                show: true,
                lineWidth: 0,
                fill: 1
            },
            color: jQuery.Color(dangerColour).toHexString()
        }, {
            data: difficultyGraphPoints,
            lines: {
                show: true
            },
            color: '#333333'
        }], 

        options: {
            xaxis: {
                ticks: [
                    [50 / difficultyBins, 'hard'],
                    [50, 'ideal'],
                    [100 - 50 / difficultyBins, 'easy']
                ],
                tickLength: 0
            }, 
            yaxis: {
                min: 0,
                max: maxDifficultyCount * 1.35,
                ticks: false
            },
            colors:["#5D95C4"]
        }
    };

    // Build discrimination graph

	var discriminationGraphPoints = [];
    var maxDiscriminationCount = 0;
	for (var i = 0; i < discriminationBins; ++i) {
        if (discriminationCounts[i] > maxDiscriminationCount) {
            maxDiscriminationCount = discriminationCounts[i];
        }

		discriminationGraphPoints.push([100 * i / discriminationBins, discriminationCounts[i]]);
		discriminationGraphPoints.push([100 * (i + 1) / discriminationBins, discriminationCounts[i]]);
	}
    
    values.discriminations = {
        data: [{
            data: discriminationGraphPoints.slice(0, 2),
            lines: {
                show: true,
                lineWidth: 0,
                fill: 1
            },
            color: jQuery.Color(dangerColour).toHexString()
        }, {
            data: discriminationGraphPoints.slice(2, 6),
            lines: {
                show: true,
                lineWidth: 0,
                fill: 1
            },
            color: jQuery.Color(warningColour).toHexString()
        }, {
            data: discriminationGraphPoints.slice(6),
            lines: {
                show: true,
                lineWidth: 0,
                fill: 1
            },
            color: jQuery.Color(goodColour).toHexString()
        }, {
            data: discriminationGraphPoints,
            lines: {
                show: true
            },
            color: '#333333'
        }],

        options: {
            xaxis: {
                ticks: [
                    [50 / discriminationBins, 'poor'],
                    [50, 'good'],
                    [100 - 50 / discriminationBins, 'great']
                ],
                tickLength: 0
            }, 
            yaxis: {
                min: 0, 
                max: maxDiscriminationCount * 1.35,
                ticks: false
            },
            colors:["#5D95C4"]
        }
    };

	return values;
}

function rebuildItemStatistics(questionID, questionData, numberingType, renderAsVersion = false) {
	if (questionID in filteredStatistics) {
		var statsBlocks = getStatsBlocks($('#statistics_item'), filteredStatistics[questionID], questionData, renderAsVersion);
	
		$('#item_statistics_data').html('');
		$('#statistics_item .part_wrapper').each(function() {
			var thisNumber = $(this).find('.question_number').html().slice(0, -1);
			if (numberingType == 'Alpha') {
				var $contextWrapper = $(this).prevAll('.context_wrapper:first');
				if ($contextWrapper.length > 0) {
					thisNumber = $contextWrapper.find('.question_number').html().slice(0, -1) + thisNumber;
				}
			} else if (numberingType == 'Type') {
				var thisStyle = $(this).attr('data-type').substr(0, 2);
				thisNumber = thisStyle.toUpperCase() + thisNumber;
			}

			var partID = $(this).attr('data-part-id');
			$('#item_statistics_data').append("<div class='question_number' style='font-weight:bold; font-size:1.2em; margin-bottom:5px;'>Question " + 
				thisNumber + "</div>");

			var numShown = 5;

			var $statsContent = $(statsBlocks[partID]);
			var $statsTable = $statsContent.find('.stats_table');
			if ($(this).attr('data-type').substr(0, 2) == 'nr') {
                var numRows = $statsTable.find('.stats_table_element').length;
                $statsTable.find('.stats_table_element').each(function() {
                    if ($(this).index() <= numShown) $(this).show();
                });
				if (numRows > numShown) {
					var moreHTML = "<tr class='stats_show_more' data-num-shown='" + numShown + "'><td colspan=4>" +
						"Show more (<span class='stats_show_count'>" + (numRows - numShown) + 
						"</span> hidden)</td></tr>";
					$statsTable.append(moreHTML);
				}	
			} else $statsTable.find('.stats_table_element').show();
			$('#item_statistics_data').append($statsContent);
		});
		
		$('#item_statistics_data .stats_count_wrapper').hide();
		$('#statistics_n').html($('#item_statistics_data .stats_count_wrapper .part_statistics_value').html());
		$('#item_statistics_wrapper').show();

        fixScoreLabels($('#item_statistics_data'));

	} else $('#item_statistics_wrapper').hide();
}

function rebuildAssessmentStatistics(questionHandleIDs, questionData) {
	var statsValues = getAssessmentStatistics(questionHandleIDs, questionData);
			
	$('#statistics_graphs').html("");
	if (statsValues.n > 0) {
		
// Show number of questions included
		
		var includedString = "Based on " + statsValues.n + " question" + ((statsValues.n == 1) ? '' : 's');
		if (statsValues.nExcluded > 0) includedString += " (excluding " + statsValues.nExcluded + " without statistics)";
		$('#statistics_graphs').append("<div style='margin-bottom:5px; margin-left:10px;'>" + includedString + "</div>");
		
// Show average score
		
		var averageString = "Assessment average: " + statsValues.average.toFixed(2) + "%";
		$('#statistics_graphs').append("<div style='margin-bottom:5px; margin-left:10px;'>" + averageString + "</div>");

// Set up plots

		$('#statistics_graphs').append("<div id='difficultyPlot' style='width:270px; height:170px; display:inline-block;'></div><div id='discriminationPlot' style='width:270px; height:170px; margin-left:10px; display:inline-block;'></div>");

// Fill difficulty plot 
	   
		$.plot($('#difficultyPlot'), statsValues.difficulties.data, 
            statsValues.difficulties.options);
        
		$('#difficultyPlot').append(
			"<span style='position:absolute; left:20px; top:15px;'>Item score</span>"
		);
	
// Fill discrimination plot 
		   
		$.plot($('#discriminationPlot'), statsValues.discriminations.data, 
            statsValues.discriminations.options);

		$('#discriminationPlot').append(
			"<span style='position:absolute; left:20px; top:15px;'>Discrimination</span>"
		);
	} else {
		$('#statistics_graphs').html("<div style='font-style:italic'>No statistics to show here.</div>");		
	}
}

function highlightQuestion($wrapper, questionStatistics, questionData) {
	for (var partIndex = 0; partIndex < questionData.Parts.length; ++partIndex) {
		var partData = JSON.parse(JSON.stringify(questionData.Parts[partIndex]));
		if (partData.type == 'context') continue;
        if (!(partData.id in questionStatistics.QuestionParts)) continue;

		if ((partData.type != 'wr') && !('Answers' in partData)) {
			partData['Answers'] = questionData['Common']['Answers'];
		}

		var partStatistics = questionStatistics.QuestionParts[partData.id];

        var totalCount = 0;
        for (var i = 0; i < partStatistics.length; ++i) {
            if (partData.type == 'wr') {
                totalCount += partStatistics[i].n;
            } else {
                for (var j = 0; j < partStatistics[i].length; ++j) {
                    totalCount += partStatistics[i][j].n;
                }    
            }
        }

        var partAnswers;
        if ('Common' in questionData) partAnswers = questionData.Common.Answers;
        else partAnswers = partData.Answers;

		var discrimination = getDiscrimination(partStatistics, partData);

		var hasStatistics = false;
		var $partWrapper = $wrapper.find('.part_wrapper[data-part-id="' + partData.id + '"]');
		if ((discrimination !== false) && (totalCount > 100)) {
			hasStatistics = true;

			if (discrimination < discriminationDanger) {
				$partWrapper.bubbletip({
					html: 'This question has a low discrimination (' + discrimination.toFixed(2) + ').'
				});
			} else {
				if ($partWrapper.is('.item_highlighted')) $partWrapper.bubbletip('destroy');
				
				if (partData.type.substr(0, 2) == 'mc') {
					var hasMultipleScored = false;
					for (var i = 0; i < partData.ScoredResponses.length; ++i) {
						if ('ids' in partData.ScoredResponses[i]) hasMultipleScored = true;
					}
					
					if (!hasMultipleScored) {
						for (var answerIndex = 0; answerIndex < partAnswers.length; ++answerIndex) {
							var answerData = partAnswers[answerIndex];
							var $answerWrapper = $partWrapper.find('.question_answer[data-answer-id="' + answerData.id + '"]');

                            var thisCount = 0;
                            for (var i = 0; i < partStatistics.length; ++i) {
                                for (var j = 0; j < partStatistics[i].length; ++j) {
                                    if ('ids_matched' in partStatistics[i][j]) {
                                        var ids = partStatistics[i][j].ids_matched;
                                        if ((ids.length == 1) && (ids[0] == answerData.id)) {
                                            thisCount += partStatistics[i][j].n;
                                        }    
                                    }
                                }
                            }
                            var percent = thisCount * 100 / totalCount;

							if (percent == 0.0) {
								$answerWrapper.bubbletip({
									html: 'This is has not been selected by students.'
								});
							} else if (percent < 5.0) {
								$answerWrapper.bubbletip({
									html: 'This answer has been selected by only ' + percent.toFixed(1) + '% of students.'
								});
							} else if ($answerWrapper.is('.item_highlighted')) {
								$answerWrapper.bubbletip('destroy');
							}
						}
					}
				}
			}
		}

		if (!hasStatistics) {
			$partWrapper.find('.item_highlighted').bubbletip('destroy');
		}		
	}
}

function buildStatsHTML(questionHandleIDs, questionData, renderAsVersion = false) {

	var $assessmentCopy = $('#assessment_preview_wrapper').clone();

	// Remove section and question wrappers if they are present

	$assessmentCopy.find('.preview_section_wrapper').each(function() {
		if ($(this).children().length > 0) $(this).children().eq(0).unwrap();
		else $(this).remove();
	});

	$assessmentCopy.find('.question_wrapper').each(function() {
		$(this).children().attr('data-question-handle-id', $(this).attr('data-question-handle-id'));
		if ($(this).children().length > 0) $(this).children().eq(0).unwrap();
		else $(this).remove();
	});

	// Add title

	var titleHTML = $assessmentCopy.find('.assessment_title').html();
	$assessmentCopy.find('.assessment_title').remove();

	var statsHTML = '<div class="assessment_title">';
	if (($('#set_logo_hash').length > 0) && ($('#set_logo_hash').val().length > 0)) {
		statsHTML += '<img src="' + userdataPrefix + $('#set_logo_hash').val() + 
			'" style="float:left; height:' + $('#set_logo_height').val() + 'in; margin-right:0.25in;">'
	}
	statsHTML += titleHTML;
	statsHTML += '<div style="clear:both;"></div>';
	statsHTML += '</div>';
	
	$assessmentCopy.find('.assessment_title').remove();

	// Add stuff for graphs
	
	var statsValues = getAssessmentStatistics(questionHandleIDs, questionData);
	
    var graphsData = [
        {
            name: "Average score", 
            id: "difficultyPlot", 
            labels:["hard", "ideal", "easy"],
            data: statsValues.difficulties.data,
            options: statsValues.difficulties.options
        }, {
            name: "Discrimination", 
            id: "discriminationPlot", 
            labels:["poor", "good", "great"],
            data: statsValues.discriminations.data,
            options: statsValues.discriminations.options
        }
    ];

	statsHTML += '<script type="text/javascript">var graphs=' + JSON.stringify(graphsData) + ';</script>';

	var includedString = "Based on " + statsValues.n + " questions";
	if (statsValues.nExcluded > 0) includedString += " (excluding " + statsValues.nExcluded + " without statistics)";
		
	statsHTML += "<div class='printed_stats_wrapper'><div class='printed_stats_label'>Assessment statistics</div><div style='margin-bottom:5px;'>" + includedString + "</div><div style='margin-bottom:5px;'>Assessment average: " + statsValues.average.toFixed(2) + "%</div><div id='difficultyPlot' style='width:3.25in; height:2in; float:left;'></div><div id='discriminationPlot' style='width:3.25in; height:2in; float:right;'></div><div style='clear:both;'></div></div>";
	
	// Unrender equation elements and remove selection

	RenderTools.unrenderEquations($assessmentCopy);
	$assessmentCopy.find('.part_wrapper').removeClass('question_selected');

	// Transfer elements with stats as appropriate

	var needsPagebreak;
	$assessmentCopy.children().each(function() {
		
		if ($(this).is('.preamble_wrapper')) {

			if ($(this).is(':visible')) statsHTML += this.outerHTML;

		} else if ($(this).is('.preview_header')) {

			needsPagebreak = false;
			if ($(this).is(':visible')) statsHTML += this.outerHTML;
			else if ($(this).hasClass('pagebreak')) needsPagebreak = true;

		} else {

			if (needsPagebreak) $(this).addClass('pagebreak');
			needsPagebreak = false;

			if ($(this).is('.part_wrapper')) {

				$(this).find('.question_answer_mc').css('width', '');						
				$(this).find('.question_answer_mc').css('padding-right', '');

				var questionHandleID = parseInt($(this).attr('data-question-handle-id'));
				var itemData = questionData[questionHandleID].json_data;
				var questionID = questionData[questionHandleID].id;
				var statsBlocks = getStatsBlocks($(this), filteredStatistics[questionID], itemData, renderAsVersion);

				statsHTML += this.outerHTML;

				var partID = $(this).attr('data-part-id');

				var numShown = 10;

				var $statsContent = $(statsBlocks[partID]);
				var $statsTable = $statsContent.find('.stats_table');
				if ($(this).attr('data-type').substr(0, 2) == 'nr') {
                    var numRows = $statsTable.find('.stats_table_element').length;
					$statsTable.find('.stats_table_element').each(function() {
						if ($(this).index() <= numShown) $(this).show();
					});
					if (numRows > numShown) {
						$statsTable.find('.stats_show_count').html(" (" + (numRows - numShown) + " hidden)");
					}	
				} else $statsTable.find('.stats_table_element').show();

				statsHTML += "<div class='printed_stats_wrapper'><div class='printed_stats_label'>Item statistics</div>" + 
					$statsContent.html() + "</div>";

			} else {

				statsHTML += this.outerHTML;

			}
		}			
	});

	return statsHTML;
}

function initFullStatistics(data) {
    fullStatistics = data;
}

function loadFilteredStatistics(requested_ids) {
	var required_ids = [];
	for (var i = 0; i < requested_ids.length; ++i) {
		if (!(requested_ids[i] in filteredStatistics)) {
			required_ids.push(requested_ids[i]);
		}	
	}

	var statsOptions = { question_ids: required_ids };
	var hasFilters = false;

    if ($('#stats_included_sources').length == 1) {
        if ($('#stats_included_sources').val() != 'All') {
            statsOptions.included_sources = $('#stats_included_sources').val();
            hasFilters = true;
        }    
    }

    if ($('#stats_filter_by_date').length == 1) {
        if ($('#stats_filter_by_date').prop('checked')) {
            var minDate = dayjs($('#stats_min_date').val()).utc();
            statsOptions.filter_minDate = minDate.format('YYYY-MM-DD HH:mm:ss');
            hasFilters = true;
        }
    }

    if ($('#stats_filter_by_user').length == 1) {
        if ($('#stats_filter_by_user').prop('checked')) {
            statsOptions.filter_user_id = userID;
            hasFilters = true;
        }
    }

	if ($('#stats_filter_by_version').length == 1) {
		if ($('#stats_filter_by_version').prop('checked')) {
			statsOptions.filter_document_id = documentID;
			hasFilters = true;
		}	
	}
    
    if ($('#stats_filter_by_sitting').length == 1) {
		if ($('#stats_filter_by_sitting').prop('checked')) {
			statsOptions.filter_sitting_id = sittingID;
			hasFilters = true;
		}
	}

	if (!hasFilters) {
		
		filteredStatistics = fullStatistics;
		return Promise.resolve();

	} else {

		if (statsOptions.question_ids.length > 0) {
		
            const queryString = new URLSearchParams(statsOptions).toString();
            return ajaxFetch(`/Questions/get_statistics?${queryString}`)
            .then(async response => {
                const responseData = await response.json();

                for (var question_id in responseData) {
                    filteredStatistics[question_id] = responseData[question_id];
                }

			}).catch(function() {
				$.gritter.add({
					title: "Error",
					text: "Unable to load statistics.",
					image: "/img/error.svg"
				});
			});

		} else return Promise.resolve();
	}
}

function getQuestionHandleIDs(assessmentData) {
	var questionHandleIDs = [];
	for (var i = 0; i < assessmentData.Sections.length; ++i) {
		if (assessmentData.Sections[i].type == 'page_break') continue;

		for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
			var itemData = assessmentData.Sections[i].Content[j];
		
			if (itemData.type == 'item') {
				questionHandleIDs.push([itemData.question_handle_id]);
			} else if (itemData.type == 'item_set') {
				questionHandleIDs.push(itemData.question_handle_ids);
			}
		}
	}

	return questionHandleIDs;
}

function initFilterInterface() {

	// Initialize statistics filters
	
	$('#stats_min_date').datepicker({
		dateFormat: "MM d, yy",
		disabled: true,
		onSelect: function() {
			filteredStatistics = {};
		}
	});

	var minDate = new Date();
	minDate.setMonth(minDate.getMonth() - 3);
	$('#stats_min_date').val($.datepicker.formatDate("MM d, yy", minDate));

	$('.stats_filter_input').on('change', function() {
		filteredStatistics = {};
	});

	$('#stats_filter_by_date').on('click', function() {
		if ($('#stats_filter_by_date').prop('checked')) {
			$('#stats_min_date').datepicker("option", "disabled", false);
		} else {
			$('#stats_min_date').datepicker("option", "disabled", true);
		}
	});
}

export { 
    fullStatistics, initFullStatistics,
    filteredStatistics, loadFilteredStatistics,
    difficultyBins, difficultyLowerLimit, difficultyUpperLimit,
    difficultyDangerFraction, difficultyDangerBins,
    difficultyWarningFraction, difficultyWarningBins,
    discriminationBins,
    discriminationDanger, discriminationDangerBins,
    discriminationWarning, discriminationWarningBins, 
    neutralColour, incorrectColour, partialColour, correctColour,
    dangerColour, warningColour, goodColour,
    getQuestionHandleIDs,
    getDiscrimination,
    fixScoreLabels,
    hasVariables,
    rebuildItemStatistics, rebuildAssessmentStatistics, 
    buildStatsHTML,
    highlightQuestion,
    initFilterInterface
};
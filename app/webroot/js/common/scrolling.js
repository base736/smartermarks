/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var scrollingCommonInitializing = false;
var scrollingCommonInitialized = false;

var scrollDirection = 0;
var lastScrollTop = 0;

var resetLeftPanel = function() {
    scrollDirection = 0;
    $('#left_panel').css('position', 'relative');
    fixLeftPanelPosition();
}

var fixLeftPanelPosition = function() {
    var wrapperHeight = $('#panel_wrapper').height();
    var panelHeight = $('#left_panel').height();

    if (panelHeight < wrapperHeight) {
        
        // Panel is smaller than the wrapper on the right. Scrolling may be needed.
        
        var windowHeight = $(window).height();
        var scrollTop = $(window).scrollTop();
        var deltaScrollTop = scrollTop - lastScrollTop;
        
        var wrapperTop = $('#panel_wrapper').offset().top;
        var wrapperBottom = wrapperTop + wrapperHeight;
        
        var panelTop = $('#left_panel').offset().top;
        var panelBottom = panelTop + panelHeight;
        
        var topMargin = 5;
        var bottomMargin = 5;
        
        if ($('#left_panel').css('position') == 'relative') {
            
            if (panelHeight + bottomMargin < windowHeight) {
                
                // Panel is smaller than the window
                
                if (scrollTop < wrapperTop - topMargin) {
                    
                    // Top of window is above the top of the wrapper
                    
                    $('#left_panel').css('position', 'relative');
                    $('#left_panel').css('top', '0px');
                    
                } else if (scrollTop > wrapperBottom - panelHeight) {
                    
                    // Top of window is too low to leave room for the panel; pin it to the bottom of the wrapper
                    
                    var newTop = wrapperHeight - panelHeight;
                    $('#left_panel').css('position', 'relative');
                    $('#left_panel').css('top', newTop + 'px');
                    
                } else {
                    
                    // Pin the panel to the top of the window
                    
                    $('#left_panel').css('position', 'fixed');
                    $('#left_panel').css('top', topMargin + 'px');
                }
            } else if (scrollTop + windowHeight > panelBottom + bottomMargin) {
                
                // Bottom of the window is below the bottom of the panel. Line them up if we can.
                
                if (scrollTop + windowHeight - bottomMargin < wrapperBottom) {
                    var newTop = windowHeight - bottomMargin - panelHeight;
                    $('#left_panel').css('position', 'fixed');
                    $('#left_panel').css('top', newTop + 'px');
                } else {
                    var newTop = wrapperHeight - panelHeight;
                    $('#left_panel').css('position', 'relative');
                    $('#left_panel').css('top', newTop + 'px');
                }
            } else if (scrollTop < panelTop - topMargin) {
                
                // Top of the window is above the top of the panel. Line them up if we can.
                
                if (scrollTop > wrapperTop - topMargin) {
                    $('#left_panel').css('position', 'fixed');
                    $('#left_panel').css('top', topMargin + 'px');
                } else {
                    $('#left_panel').css('position', 'relative');
                    $('#left_panel').css('top', '0px');
                }
            } else if (wrapperBottom < panelBottom + bottomMargin) {
                var newTop = wrapperHeight - panelHeight;
                $('#left_panel').css('position', 'relative');
                $('#left_panel').css('top', newTop + 'px');
            }
        } else if ($('#left_panel').css('position') == 'fixed') {
            
            if (panelTop < wrapperTop) {
                
                // Top of the panel is above the top of the wrapper. Pin it to the top of the wrapper.
                
                $('#left_panel').css('position', 'relative');
                $('#left_panel').css('top', '0px');
                
            } else if (panelBottom > wrapperBottom) {
                
                // Bottom of the panel is below the bottom of the wrapper. Pin it to the bottom of the wrapper.
                
                var newTop = wrapperHeight - panelHeight;
                $('#left_panel').css('position', 'relative');
                $('#left_panel').css('top', newTop + 'px');
                
            } else if (deltaScrollTop * scrollDirection <= 0) {
                
                // Scroll direction has changed. Pin the panel to its current location in the wrapper.
                
                var currentOffset = $('#left_panel').offset().top - wrapperTop;
                $('#left_panel').css('position', 'relative');
                $('#left_panel').css('top', currentOffset + 'px');
            }
        }
    } else {

        // Panel is larger than the wrapper on the right. Top align the two.

        $('#left_panel').css('position', 'relative');
        $('#left_panel').css('top', '0px');
    }
    
    scrollDirection = (deltaScrollTop > 0) ? 1 : -1;
    lastScrollTop = scrollTop;			
}

function initialize() {
    if (scrollingCommonInitializing) return;
    scrollingCommonInitializing = true;

    $(window).on('scroll', function(e) {
        fixLeftPanelPosition();			
    });

    scrollingCommonInitialized = true;
}

export { initialize, resetLeftPanel, fixLeftPanelPosition };
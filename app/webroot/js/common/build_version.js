/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as StatsCommon from '/js/common/stats.js?7.0';

document.addEventListener('DOMContentLoaded', async function() {

    await RenderTools.initialize();

    // Render graphs. The graphs variable is built in buildStatsHTML, in "stats.js".

    if (typeof(graphs) !== 'undefined') {
		for (var i = 0; i < graphs.length; ++i) {
			$.plot($('#' + graphs[i].id), graphs[i].data, graphs[i].options);
			$('#' + graphs[i].id).append(
	    		"<span style='position:absolute; left:20px; top:15px;'>" + graphs[i].name + "</span>"
			);
		}
	}

	// Load CSS
	
	var renderStyle = 'Numbers';
	if ('numberingType' in pageSettings) renderStyle = pageSettings.numberingType;
	await RenderTools.setRenderStyle(renderStyle);

	// Remove section and question wrappers if they are present

	$('#print_wrapper .preview_section_wrapper').each(function() {
		if ($(this).children().length > 0) $(this).children().eq(0).unwrap();
		else $(this).remove();
	});
	$('#print_wrapper .question_wrapper').each(function() {
		if ($(this).children().length > 0) $(this).children().eq(0).unwrap();
		else $(this).remove();
	});

	// Remove "last_part" classes in old renders, add "context_question", and add separator

	$('#print_wrapper .last_part').each(function() {
		$(this).after("<div class='item_separator'></div>");
		$(this).removeClass('last_part');
		var $target = $(this);
		while ($target.is('.part_wrapper')) {
			$target.addClass('context_question');
			$target = $target.prev();
		}
	});

	// Remove NR answers

	$('#print_wrapper .nr_answer_list').remove();

	if (('wrResponseLocation' in pageSettings) && (pageSettings.wrResponseLocation == 'question')) {
		$('.wr_response_space').show();
	}

	// Render equations

	await RenderTools.renderEquations($('#print_wrapper'));

	// Rebuild multiple choice answer widths

	$('#print_wrapper .part_wrapper').each(function() {
		if (pageSettings.mcFormat == 'Auto') {
			if ($(this).find('.question_answer_mc').length > 0) RenderTools.setMCWidths($(this));
		} else $(this).find('.question_answer_mc').css('width', '100%');

		if ($(this).find('.question_answer_mc, .question_answer_table').length > 0) {
			RenderTools.setMCImageWidths($(this));
		}
	});
	
	RenderTools.setMatchingWidths($('#print_wrapper'));

	// Fix score labels if required

	StatsCommon.fixScoreLabels($('#print_wrapper'));

	// Wrap assessment elements to avoid inconvenient page breaks. Headers and other top-level elements are
	// moved into chunk_wrappers to keep them with the items that follow.

	var $chunk_wrapper = $("<div class='chunk_wrapper'></div>");
	$('#print_wrapper').children().each(function() {
		if ($(this).hasClass('pagebreak')) {
			$chunk_wrapper.css('page-break-before', 'always');
		}
		
		var finishChunk = false;
		if ($(this).is('.part_wrapper') && !$(this).next().is('.item_separator, .printed_stats_wrapper')) {
			finishChunk = true;
		} else if ($(this).is('.printed_stats_wrapper') && !$(this).next().is('.item_separator')) {
			finishChunk = true;
		} else if ($(this).is('.item_separator')) {
			finishChunk = true;
		} else if ($(this).is('.preamble_wrapper')) {
			finishChunk = true;
		}

		$(this).appendTo($chunk_wrapper);

		if (finishChunk) {
			$chunk_wrapper.appendTo($('#print_wrapper'));
			$chunk_wrapper = $("<div class='chunk_wrapper'></div>");
		}
	});
	$chunk_wrapper.remove();
	
	// Flag window as ready to print for Puppeteer
	
	$('body').append('<span class="page_loaded"></span>');
});
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var login_success = null;
var login_failure = null;

var start_login = function(success_callback, failure_callback) {
    if (pendingAJAXPost) return false;
    pendingAJAXPost = true;

    Cookies.set('auth_redirect_action', 'redirect_home');
    Cookies.remove('security_email');
    
    const email = encodeURIComponent($('.login_email').val());
    fetch(`/Users/get_auth_redirect?email=${email}`)
    .then(async response => {
        pendingAJAXPost = false;
        const responseData = await response.json();

        hideSubmitProgress($('.login_button_wrapper'));

        if ('redirect' in responseData) {
            
            if (('needs_email' in responseData) && responseData.needs_email) {
                $('#needsEmail_dialog').attr('data-redirect', responseData.redirect);
                $('#needsEmail_dialog').dialog('open');
            } else window.location.href = responseData.redirect;

        } else if (('needs_password' in responseData) && (responseData.needs_password)) {

            $('#setPassword_email').val($('.login_email').val());
            $('#setPassword_dialog').dialog('open');

        } else if (responseData.success) {

            login_success = success_callback;
            login_failure = failure_callback;

            $('#localLogin_dialog').attr('data-cancel-action', 'close');
            $('#localLogin_email').val($('.login_email').val());
            $('#localLogin_prompt').html('');
            $('#localLogin_dialog').dialog('open');       

        } else {
            
            $.gritter.add({
                title: "Error",
                text: "No user found with this email address. Please try again.",
                image: "/img/error.svg"
            });

            if (failure_callback != null) failure_callback(responseData);
            else $('.login_email').val('').focus();
        }

    }).catch(function() {
        pendingAJAXPost = false;

        $.gritter.add({
            title: "Error",
            text: "Network error encountered. Please try again.",
            image: "/img/error.svg"
        });
    });
}

$(document).ready(function() {

    var login_local = function(success_callback, failure_callback) {
        if (pendingAJAXPost) return false;
        showSubmitProgress($('#localLogin_dialog'))
        pendingAJAXPost = true;

        fetch('/Users/ajax_login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: $('#localLogin_email').val(),
                password: $('.login_password').val()
            })
        }).then(async response => {
            pendingAJAXPost = false;
            const responseData = await response.json();

            if (responseData.success) {
                success_callback(responseData);
            } else {

                $.gritter.add({
                    title: "Error",
                    text: "Incorrect password entered. Please try again.",
                    image: "/img/error.svg"
                });

                if (failure_callback != null) {

                    failure_callback(responseData);

                } else {

                    $('.login_password').val('');
                    $('#localLogin_dialog').dialog('close');
    
                    $('.login_email').val('').focus();  

                }
            }
        }).catch(function() {
            pendingAJAXPost = false;
    
            $.gritter.add({
                title: "Error",
                text: "Network error encountered. Please try again.",
                image: "/img/error.svg"
            });
        });
    }

    $('#localLogin_dialog').dialog({
        autoOpen: false,
		width: 350,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
                if ($(this).attr('data-cancel-action') == 'logout') {
                    window.location.href = "/";
                } else $(this).dialog("close");
		 	},
			'Continue': function() {
                login_local(login_success, login_failure);
            }
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Continue")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();

            $(this).find('.login_password').val('');
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

    $('.login_email').on('keydown', function(e) {
        if ((e.key === 'Enter') || ((e.key === 'Tab') && ($(this).val().length > 0))) {
            $('.login_submit').trigger('click');
        }
    });

    $('.login_password').on('keydown', function(e) {
        if (e.key == 'Enter') {
			var $buttonPane = $('#localLogin_dialog').siblings('.ui-dialog-buttonpane');
            $buttonPane.find('button:contains("Continue")').trigger('click');
        }
    });

    $(document).on('click', '.login_password+.view_icon_grey', function(e) {
        e.preventDefault();
        
        $(this).siblings('.login_password').attr('type', 'text');
        $(this).addClass('view_icon');
        $(this).removeClass('view_icon_grey');
        $(this).attr('title', 'Hide password');
    });

    $(document).on('click', '.login_password+.view_icon', function(e) {
        e.preventDefault();
        
        $(this).siblings('.login_password').attr('type', 'password');
        $(this).addClass('view_icon_grey');
        $(this).removeClass('view_icon');
        $(this).attr('title', 'Show password');
    });

    $('.login_field input').on('keyup blur refresh', function() {
        var $label = $(this).closest('.login_field').find('label');
        if ($(this).val().length != 0) $label.css('color', 'white');
    	else $label.css('color', '#aaa');
    });
    
    $('.login_email').focus();
});

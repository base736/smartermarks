/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as AssessmentsRender from '/js/assessments/render.js?7.0';
import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as StatsCommon from '/js/common/stats.js?7.0';
import * as ScoringCommon from '/js/common/scoring.js?7.0';

var previewCommonInitializing = false;
var previewCommonInitialized = false;

var bankQuestions = {};

var parseQuestionData = function(data) {
    var isValid = true;
    if (!('data_string' in data)) isValid = false;
    else if (!('data_hash' in data)) isValid = false;
    else if (data.data_hash != SparkMD5.hash(data.data_string)) isValid = false;

    if (isValid) {
        data.json_data = JSON.parse(data.data_string);
        for (var i = 0; i < data.json_data.Parts.length; ++i) {
            data.json_data.Parts[i].index = i;
        }

        delete data.data_string;
        delete data.data_hash;

        return data;
    } else return false;
}

function updateQuestionPreview(questionID) {

    if (typeof updateQuestionPreview.renderingQuestionID == 'undefined') {
        updateQuestionPreview.renderingQuestionID = false;
    }

    if (typeof updateQuestionPreview.queuedQuestionID == 'undefined') {
        updateQuestionPreview.queuedQuestionID = false;
    }

    if (updateQuestionPreview.renderingQuestionID === false) {
        if (questionID === false) {
            questionID = updateQuestionPreview.queuedQuestionID;
            updateQuestionPreview.queuedQuestionID = false;
        }
        
        if (questionID !== false) {
            updateQuestionPreview.renderingQuestionID = questionID;

            $('#preview_loading').show();
            $('#preview_stats_wrapper').empty().hide();
            $('#preview_question_wrapper .question_wrapper').empty();

            var stylePromise = RenderTools.setRenderStyle('Numbers');
            var loadPromise = loadQuestionPreview(questionID);
    
            Promise.all([stylePromise, loadPromise])
            .then(function() {
                $('#preview_loading').hide();

                var thisStatistics = StatsCommon.fullStatistics[questionID];
        
                var count = thisStatistics.Question.count;
                var bad_discrimination = false;

                if (count > 0) {

                    // Get difficulty bar

                    var widthA = 0.0;
                    var widthB = 0.0;
                    var widthC = 0.0;
                    var widthD = 0.0;
                    var questionMaxScore = 0.0;
        
                    for (var partIndex = 0; partIndex < bankQuestions[questionID].json_data.Parts.length; ++partIndex) {
                        var partData = bankQuestions[questionID].json_data.Parts[partIndex];
                        if (partData.type == 'context') continue;

                        var lowerLimit, upperLimit;
                        if (partData.type.substr(0, 2) == 'mc') {
                            var answersCount;
                            if ('Answers' in partData) answersCount = partData.Answers.length;
                            else if ('Answers' in bankQuestions[questionID].json_data.Common) {
                                answersCount = bankQuestions[questionID].json_data.Common.Answers.length;
                            }

                            lowerLimit = 1.0 / answersCount;
                            upperLimit = 1.0 - 0.03 * (answersCount - 1);
                        } else {
                            lowerLimit = StatsCommon.difficultyLowerLimit;
                            upperLimit = StatsCommon.difficultyUpperLimit;
                        }
                        var idealAverage = (lowerLimit + 1.0) / 2.0 + 0.1;
            
                        var lowerWarning = idealAverage - (idealAverage - lowerLimit) * StatsCommon.difficultyWarningFraction;
                        var upperWarning = idealAverage + (upperLimit - idealAverage) * StatsCommon.difficultyWarningFraction;
                        
                        var lowerDanger = idealAverage - (idealAverage - lowerLimit) * StatsCommon.difficultyDangerFraction;
                        var upperDanger = idealAverage + (upperLimit - idealAverage) * StatsCommon.difficultyDangerFraction;
            
                        var partOutOf = ScoringCommon.getMaxScore(partData);

                        widthA += lowerDanger * partOutOf;
                        widthB += (lowerWarning - lowerDanger) * partOutOf;
                        widthC += (upperWarning - lowerWarning) * partOutOf;
                        widthD += (upperDanger - upperWarning) * partOutOf;
                        questionMaxScore += partOutOf;
                    }

                    widthA *= 100 / questionMaxScore;
                    widthB *= 100 / questionMaxScore;
                    widthC *= 100 / questionMaxScore;
                    widthD *= 100 / questionMaxScore;

                    var difficultyPercent = parseFloat(thisStatistics.Question.difficulty) * 100;

                    var difficultyBar = 
                    "<div style='display:inline-block; vertical-align:middle; width:100px; height:13px; border:1px solid black; background-color:" + StatsCommon.dangerColour + "; position:relative;'>" +
                        "<div style='font-size:18px; position:absolute; left:" + difficultyPercent + "%; transform:translate(-50%, -60%); -webkit-transform:translate(-50%, -60%);'>&#9660</div>" +
                        "<div style='display:inline-block; height:100%; width:" + widthA + "%; background-color:" + StatsCommon.dangerColour + "'></div>" +
                        "<div style='display:inline-block; height:100%; width:" + widthB + "%; background-color:" + StatsCommon.warningColour + "'></div>" +
                        "<div style='display:inline-block; height:100%; width:" + widthC + "%; background-color:" + StatsCommon.goodColour + "'></div>" +
                        "<div style='display:inline-block; height:100%; width:" + widthD + "%; background-color:" + StatsCommon.warningColour + "'></div>" +
                    "</div>";
    
                    $('#preview_stats_wrapper').append(
                        "<div style='margin-top:10px;'>" + 
                            "<div style='font-weight:bold; display:inline-block; width:120px;'>Average score:</div>" + 
                            difficultyBar + 
                            "<div style='display:inline-block; vertical-align:middle; margin-left:5px;'>" + difficultyPercent.toFixed(0) + "%</div>" + 
                        "</div>"
                    );

                    // Get discrimination bar

                    var discrimination = parseFloat(thisStatistics.Question.discrimination);
                    var discriminationString = discrimination.toFixed(2);
        
                    if ((count > 100) && (discrimination < 0.15)) bad_discrimination = true;

                    var barDiscrimination = Math.max(discrimination, 0) * 100;

                    var disciminationBar = 
                        "<div style='display:inline-block; vertical-align:middle; width:100px; height:13px; border:1px solid black; background-color:" + StatsCommon.goodColour + "; position:relative'>" +
                            "<div style='font-size:18px; position:absolute; left:" + barDiscrimination + "%; transform:translate(-50%, -60%); -webkit-transform:translate(-50%, -60%);'>&#9660</div>" +
                            "<div style='display:inline-block; height:100%; width:15%; background-color:" + StatsCommon.dangerColour + "'></div>" +
                            "<div style='display:inline-block; height:100%; width:25%; background-color:" + StatsCommon.warningColour + "'></div>" +
                        "</div>";
        
                    $('#preview_stats_wrapper').append(
                        "<div style='margin-top:10px; margin-bottom:10px;'>" +
                            "<div style='font-weight:bold; display:inline-block; width:120px;'>Discrimination:</div>" +
                            disciminationBar + 
                            "<div style='display:inline-block; vertical-align:middle; margin-left:5px;'>" + discriminationString + "</div>" + 
                        "</div>"
                    );

                    $('#preview_stats_wrapper').show();
                }
        
                AssessmentsRender.renderItemToDiv(bankQuestions[questionID].json_data, $('#preview_question_wrapper .question_wrapper'))
                .then(async function() {
                    $('#preview_question_wrapper').find('.part_wrapper[data-type="mc"] .question_answer').css('width', '100%');

                    var numberingType = 'Numbers';
                    if (typeof assessmentData !== 'undefined') {
                        numberingType = assessmentData.Settings.numberingType;
                    }
                    await RenderTools.renderStyles[numberingType].renumber($('#preview_question_wrapper'));
    
                    var bad_answers = false;
                    if (count > 100) {
                        for (var partIndex = 0; partIndex < bankQuestions[questionID].json_data.Parts.length; ++partIndex) {
                            var partData = bankQuestions[questionID].json_data.Parts[partIndex];
                            
                            if ((partData.type != 'context') && (partData.type.substr(0, 2) == 'mc')) {
                                if (partData.id in thisStatistics.QuestionParts) {
                                    var partStatistics = thisStatistics.QuestionParts[partData.id];

                                    var totalCount = 0;
                                    for (var i = 0; i < partStatistics.length; ++i) {
                                        for (var j = 0; j < partStatistics[i].length; ++j) {
                                            totalCount += partStatistics[i][j].n;
                                        }
                                    }
                        
                                    var partAnswers;
                                    if ('Common' in bankQuestions[questionID].json_data) {
                                        partAnswers = bankQuestions[questionID].json_data.Common.Answers;
                                    } else partAnswers = partData.Answers;
    
                                    for (var answerIndex = 0; answerIndex < partAnswers.length; ++answerIndex) {
                                        var answerData = partAnswers[answerIndex];
                                        var $answerWrapper = $('#preview_question_wrapper .question_answer[data-answer-id="' + answerData.id + '"]');
                    
                                        var thisCount = 0;
                                        for (var i = 0; i < partStatistics.length; ++i) {
                                            for (var j = 0; j < partStatistics[i].length; ++j) {
                                                if ('ids_matched' in partStatistics[i][j]) {
                                                    var ids = partStatistics[i][j].ids_matched;
                                                    if ((ids.length == 1) && (ids[0] == answerData.id)) {
                                                        thisCount += partStatistics[i][j].n;
                                                    }    
                                                }
                                            }
                                        }
                                        var percent = thisCount * 100 / totalCount;
                    
                                        if (percent < 5.0) {
                                            $answerWrapper.addClass('item_highlighted');
                                            bad_answers = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if (bad_discrimination || bad_answers) {
                        var noticeHTML = "<div class='item_highlighted' style='border:1px solid #ccc; margin-bottom:10px; \
                            padding:5px;'>";
                        if (bad_discrimination) {
                            noticeHTML += "This question has a low discrimination";
                        }
                        if (bad_answers) {
                            if (bad_discrimination) noticeHTML += " and some"
                            else noticeHTML += "This question has";
                            noticeHTML += " answers that are rarely chosen by students"
                        }
                        if (bankQuestions[questionID].open_status != 0) {
                            noticeHTML += ". You can help improve the open question bank by fixing it! "
                        } else noticeHTML += ". ";
                        noticeHTML += "Problems are highlighted below.</div>";
                        $('#preview_stats_wrapper').prepend(noticeHTML);
                    }

                    updateQuestionPreview.renderingQuestionID = false;
                    updateQuestionPreview(false);

                });
            });
        }
    } else if (questionID !== false) {
        updateQuestionPreview.queuedQuestionID = questionID;
    }
}

var loadQuestionPreview = function(questionID) {
    var loadPromise;

    if (questionID in bankQuestions) {

        if ('promise' in bankQuestions[questionID]) {
            loadPromise = bankQuestions[questionID].promise;
        } else loadPromise = Promise.resolve();
        
    } else {
        
        loadPromise = ajaxFetch('/Questions/get/' + questionID)
        .then(async response => {
            const responseData = await response.json();
            
            if (responseData.success) {
                var thisData = parseQuestionData(responseData.Question);
                if (thisData !== false) {

                    bankQuestions[questionID] = thisData;
                    StatsCommon.fullStatistics[questionID] = responseData.Statistics;
                    return;

                } else {

                    throw new Error('Unable to parse question data');

                }   
            } else {

                throw new Error('Failed to fetch question data');

            }
        }).catch(function() {
            delete bankQuestions[questionID];
        });

        bankQuestions[questionID] = { promise: loadPromise };    
    }

    return loadPromise;
}

async function initialize() {
    if (previewCommonInitializing) return;
    previewCommonInitializing = true;

    await AssessmentsRender.initialize();

    previewCommonInitialized = true;
}

export { 
    initialize, 
    bankQuestions, 
    parseQuestionData,
    loadQuestionPreview, updateQuestionPreview
};
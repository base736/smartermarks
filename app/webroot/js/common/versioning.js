/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

function getPartData($assessmentWrapper, assessmentData, questionData) {
	var partEntries = [];

	$assessmentWrapper.find('.question_wrapper:not(.shuffle_break)').each(function() {
		var questionHandleID = parseInt($(this).attr('data-question-handle-id'));
		var thisQuestionData = questionData[questionHandleID].json_data;

		for (var k = 0; k < thisQuestionData.Parts.length; ++k) {
			var partData = JSON.parse(JSON.stringify(thisQuestionData.Parts[k]));
			var $partWrapper = $(this).find('.part_wrapper[data-part-id="' + partData.id + '"]');

			if (partData.type != 'context') {

				var questionNumber = $partWrapper.find('.question_number').html().slice(0, -1);
				if (assessmentData.Settings.numberingType == 'Alpha') {
					var $contextWrapper = $partWrapper.prevAll('.context_wrapper:first');
					if ($contextWrapper.length > 0) {
						questionNumber = $contextWrapper.find('.question_number').html().slice(0, -1) + questionNumber;
					}
				}

				var type = partData.type;
				if (type == 'mc_table') type = 'mc';

				var thisEntry = {
					type: type,
					number: questionNumber,
					Source: {
						question_handle_id: questionHandleID,
						part_id: partData.id
					}
				};

				// Get other data

				var partStyle = partData.type.substr(0, 2);
				if (partStyle == 'mc') {

					var answers = false;
					if ('Common' in thisQuestionData) answers = thisQuestionData.Common.Answers;
					else answers = partData.Answers;

					if (answers !== false) {
						thisEntry.Answers = [];
						var baseLabel = "a".charCodeAt(0);
						for (var p = 0; p < answers.length; ++p) {
							var answerCharacter;
							if (partData.type == 'mc_truefalse') {
								if (answers[p].template == 'True') answerCharacter = 't';
								else if (answers[p].template == 'False') answerCharacter = 'f';
							} else answerCharacter = String.fromCharCode(baseLabel + p);

							thisEntry.Answers.push({
								id: answers[p].id,
								response: answerCharacter
							});
						}	
					}

					if ('ScoredResponses' in partData) {
						thisEntry.ScoredResponses = partData.ScoredResponses;
					}

				} else if (partStyle == 'nr') {

					if ('Answers' in partData) {
						thisEntry.Answers = [];
						for (var p = 0; p < partData.Answers.length; ++p) {
							var answerID = partData.Answers[p].id;
							var $answerWrapper = $partWrapper.find('.question_answer[data-answer-id="' + answerID + '"]');
							if ($answerWrapper.length == 1) {
								thisEntry.Answers.push({
									id: partData.Answers[p].id,
									response: $answerWrapper.text()
								});
							}
						}
					}

					if ('ScoredResponses' in partData) {
						thisEntry.ScoredResponses = partData.ScoredResponses;
					}

					thisEntry.TypeDetails = partData.TypeDetails;
					delete thisEntry.TypeDetails.prompt;

				} else if (partStyle == 'wr') {

					if ('Rubric' in partData) {
						var $rubric = $partWrapper.find('.question_answer');
						if ($rubric.length == 1) {
                            var rubricHTML = $rubric.html();
                            if (rubricHTML.length > 0) {
                                thisEntry.Rubric = {
                                    html: rubricHTML
                                };            
                            }
                        }
					}
            
					thisEntry.TypeDetails = partData.TypeDetails;

				}

				partEntries.push(thisEntry);
			}
		}
	});

	return partEntries;
}

export { getPartData };
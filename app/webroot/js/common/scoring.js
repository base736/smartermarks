/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/
    
function getWeightGroups(scoringData) {
    const scoringType = scoringData.Scoring.type;
    const rawWeights = scoringData.Scoring.weights;

    let groups = [];

    if (scoringType === 'Total') {
        groups = false;
    } else if (scoringType === 'Sections') {
        let questionIndex = 0;
        scoringData.Sections.forEach(section => {
            let indices = [];
            section.Content.forEach(item => {
                indices.push(questionIndex);
                questionIndex++;
            });

            let weight = 0.0;
            rawWeights.forEach(entry => {
                if (section.id === entry.section_id) {
                    weight = entry.weight;
                }
            });

            groups.push({
                section_id: section.id,
                weight: weight,
                indices: indices
            });
        });
    } else if (scoringType === 'Outcomes') {
        let questionIndices = {};
        let questionIndex = 0;
        scoringData.Sections.forEach(section => {
            section.Content.forEach(item => {
                const itemId = item.id;
                questionIndices[itemId] = questionIndex++;
            });
        });

        scoringData.Outcomes.forEach(outcome => {
            let indices = [];
            outcome.item_ids.forEach(itemId => {
                indices.push(questionIndices[itemId]);
            });

            let weight = 0.0;
            rawWeights.forEach(entry => {
                if (outcome.id === entry.outcome_id) {
                    weight = entry.weight;
                }
            });

            groups.push({
                outcome_id: outcome.id,
                weight: weight,
                indices: indices
            });
        });
    } else if (scoringType === 'Custom') {
        let questionIndices = {};
        let questionIndex = 0;
        scoringData.Sections.forEach(section => {
            section.Content.forEach(item => {
                const itemId = item.id;
                questionIndices[itemId] = questionIndex++;
            });
        });

        rawWeights.forEach(entry => {
            let indices = [];
            entry.item_ids.forEach(itemId => {
                indices.push(questionIndices[itemId]);
            });

            groups.push({
                name: entry.name,
                weight: entry.weight,
                indices: indices
            });
        });
    }

    return groups;
}

function getWeights(scoringData) {
    const scoringType = scoringData.Scoring.type;
    let weights = [];

    if (scoringType === 'Total') {
        scoringData.Sections.forEach(section => {
            section.Content.forEach(() => {
                weights.push(1.0);
            });
        });
    } else {
        let questionData = [];
        scoringData.Sections.forEach(section => {
            section.Content.forEach(question => {

                var maxValue = getMaxScore(question);
                if ('scoring' in question) {
                    if (question.scoring === 'omit') maxValue = 0.0;
                    else if (question.scoring === 'bonus') maxValue = 0.0;
                }

                questionData.push({
                    omitted: (('scoring' in question) && (question.scoring === 'omit')),
                    maxValue: maxValue
                });

            });
        });

        const groups = getWeightGroups(scoringData);

        // Initialize weights array with zeros

        weights = Array(questionData.length).fill(0.0);

        // Calculate total weight

        let totalWeight = groups.reduce((sum, group) => sum + group.weight, 0.0);

        groups.forEach(group => {

            // Calculate total value for the group

            let totalValue = 0.0;
            group.indices.forEach(questionIndex => {
                if (!questionData[questionIndex].omitted) {
                    totalValue += questionData[questionIndex].maxValue;
                }
            });

            // Calculate scale factor

            let scale = 0.0;
            if (totalValue !== 0.0 && totalWeight !== 0.0) {
                scale = (group.weight / totalWeight) / totalValue;
            }

            // Update weights for questions in the group
            
            group.indices.forEach(questionIndex => {
                if (!questionData[questionIndex].omitted) {
                    weights[questionIndex] += scale;
                }
            });
        });
    }

    return weights;
}

function significantDigits(value) {
    value = String(value);
    let nonDigitRegex = /[^\d.-]/;
    let match = nonDigitRegex.exec(value);
    let end = match === null ? value.length : match.index;

    let nonZeroRegex = /[^0.-]/;
    match = nonZeroRegex.exec(value);
    let start = match === null ? end : match.index;

    if (start != end) {
        let decimalPos = value.indexOf('.', start);
        if (decimalPos !== -1 && decimalPos < end) {
            end--;
        }
        return end - start;
    } else {
        let zeros = (value.substring(0, end).match(/0/g) || []).length;
        return zeros;
    }
}

function getMaxScore(question) {
    var maxScore = 0.0;

    if (question.type == 'wr') {

        for (let criterion of question.TypeDetails.criteria) {
            maxScore += criterion.value;
        }

    } else if ('ScoredResponses' in question) {

        for (let thisScoredResponse of question.ScoredResponses) {
            var thisScore = thisScoredResponse.value;

            if ((question.type == 'nr_selection') && (question.TypeDetails.markByDigits)) {
                var matchingAnswer = '';
                var answerID = thisScoredResponse.id;
                for (let thisAnswer of question.Answers) {
                    if (thisAnswer.id == answerID) {
                        if ('response' in thisAnswer) matchingAnswer = thisAnswer.response;
                        else if ('template' in thisAnswer) matchingAnswer = thisAnswer.template;        
                    }
                }

                thisScore *= matchingAnswer.length;
            }	

            if (thisScore > maxScore) maxScore = thisScore;
        }

    }

    return maxScore;
}

function bumpValue(value) {

    // Bump numeric string up by a little bit to ensure that 5's round correctly

    const bump = value.includes('.') ? '1' : '.1';

    let eIndex = value.indexOf('e');
    if (eIndex === -1) {
        return value + bump;
    } else {
        let mantissa = value.substring(0, eIndex);
        let exponent = value.substring(eIndex);
        return mantissa + bump + exponent;
    }
}

function scoreNumericResponse(response, question) {
    let responseScore = 0.0;
    let matchedAnswerIDs = false;

    let questionType = question.type;
    if (questionType.substr(0, 2) !== 'nr') return false;
    if (response.includes('!')) return false;

    // Initialize variables based on question type

    let fudgeFactor, factorOfTenValue, sigDigsBehaviour, sigDigsValue;
    let markByDigits, ignoreOrder, partialValue;

    if (['nr_decimal', 'nr_scientific'].includes(questionType)) {

        fudgeFactor = parseFloat(question.TypeDetails.fudgeFactor);
        if (fudgeFactor >= 1.0) fudgeFactor /= 100.0;
        if (fudgeFactor < 1.0e-5) fudgeFactor = 1.0e-5;

        factorOfTenValue = parseFloat(question.TypeDetails.tensValue);
        sigDigsBehaviour = question.TypeDetails.sigDigsBehaviour;
        sigDigsValue = parseFloat(question.TypeDetails.sigDigsValue);

    } else if (questionType === 'nr_selection') {

        markByDigits = question.TypeDetails.markByDigits;
        ignoreOrder = question.TypeDetails.ignoreOrder;
        partialValue = parseFloat(question.TypeDetails.partialValue);

    }

    // Convert nbsp in response to spaces

    response = response.replace(/\s/g, " ");

    // Check keyed responses

    for (let entry of question.ScoredResponses) {
        let answer = false;
        for (let thisAnswer of question.Answers) {
            if (thisAnswer.id == entry.id) {
                if ('response' in thisAnswer) answer = thisAnswer.response;
                else if ('template' in thisAnswer) answer = thisAnswer.template;
                break;
            }
        }
        if (answer === false) continue;

        let value = entry.value;

        var answerIDs;
        if ('ids' in entry) answerIDs = entry.ids;
        else if ('id' in entry) answerIDs = [entry.id];
        else answerIDs = false;

        let thisScore = 1.0;
        let answerMatched = false;

        if (['nr_decimal', 'nr_scientific'].includes(questionType)) {

            // Handle decimal and scientific notation

            let adjustedResponse = response.trim();
            if (/^-?(\d+(\.\d*)?|\.\d+)$/.test(adjustedResponse)) {

                let adjustedAnswer = answer.toLowerCase().trim();
                
                if (questionType === 'nr_scientific') {
                    if (['+', '-'].includes(adjustedAnswer[0])) {
                        adjustedAnswer = adjustedAnswer.substring(1);
                    }
    
                    let eIndex = adjustedAnswer.indexOf('e');
                    let exponentDigits;
                    
                    if (eIndex === -1) {
                        exponentDigits = 0;
                    } else {
                        const nextChar = adjustedAnswer.charAt(eIndex + 1);
                        if (nextChar === '+' || nextChar === '-') {
                            adjustedAnswer = adjustedAnswer.substring(0, eIndex + 1) + 
                                             adjustedAnswer.substring(eIndex + 2);
                        }
                        exponentDigits = adjustedAnswer.length - eIndex - 1;
                    }

                    // Process response
                        
                    if (adjustedResponse.includes('.') || exponentDigits === 0 || adjustedResponse.length <= exponentDigits) {

                        let responseSD = significantDigits(adjustedResponse);
                        if (responseSD === 0) responseSD = 1;
                        adjustedResponse = parseFloat(adjustedResponse).toExponential(responseSD - 1);

                    } else {

                        let beforeDecimal = adjustedResponse[0];
                        let afterDecimal = adjustedResponse.substring(1, adjustedResponse.length - exponentDigits);
                        let exponent = adjustedResponse.substring(adjustedResponse.length - exponentDigits);
                        let temp = `${beforeDecimal}.${afterDecimal}e${exponent}`;

                        let responseValue = parseFloat(temp);
                        let responseSD = significantDigits(temp);
                        if (responseSD === 0) responseSD = 1;
                        adjustedResponse = responseValue.toExponential(responseSD - 1);

                    }
                }

                // Check significant digits
                
                let answerSD = significantDigits(adjustedAnswer);
                let responseSD = significantDigits(adjustedResponse);

                if (answerSD !== responseSD) {
                    if (sigDigsBehaviour === 'Strict') {
                        thisScore = 0.0;
                    } else {
                        if (thisScore > sigDigsValue) thisScore = sigDigsValue;
                        if (sigDigsBehaviour === 'Round') {
                            let minSD = Math.min(answerSD, responseSD) || 1;

                            adjustedAnswer = bumpValue(adjustedAnswer);
                            adjustedAnswer = parseFloat(adjustedAnswer).toExponential(minSD - 1);

                            adjustedResponse = bumpValue(adjustedResponse);
                            adjustedResponse = parseFloat(adjustedResponse).toExponential(minSD - 1);
                        }
                    }
                }

                // Check the answer within tolerance

                let answerValue = parseFloat(adjustedAnswer);
                let responseValue = parseFloat(adjustedResponse);
                let minGood = answerValue / (1.0 + fudgeFactor);
                let maxGood = answerValue * (1.0 + fudgeFactor);
                if (maxGood < minGood) [minGood, maxGood] = [maxGood, minGood];

                if (responseValue >= minGood && responseValue <= maxGood) {
                    answerMatched = true;
                } else if (factorOfTenValue > 0.0) {
                    if (responseValue !== 0.0) {
                        responseValue *= Math.pow(10.0, Math.ceil(Math.log10(Math.abs(minGood / responseValue))));
                    }
                    if (responseValue >= minGood && responseValue <= maxGood) {
                        answerMatched = true;
                        if (thisScore > factorOfTenValue) thisScore = factorOfTenValue;
                    } else {
                        thisScore = 0.0;
                    }
                } else thisScore = 0.0;
            } else thisScore = 0.0;

        } else if (questionType === 'nr_selection') {

            if (/^[\d\s]+$/.test(response)) {

                // Process selection question

                let answerDigits = answer.length;
                let responseDigits = response.length;
                let numMatches = 0;

                if (ignoreOrder) {

                    let numWildcards = 0;
                    let remaining = response.replace(/\s+/g, '').split('');
                    responseDigits = remaining.length;

                    for (let char of answer) {
                        if (char !== '?') {
                            let index = remaining.indexOf(char);
                            if (index !== -1) {
                                remaining.splice(index, 1);
                                numMatches++;
                            }
                        } else numWildcards++;
                    }
                    numMatches += Math.min(numWildcards, remaining.length);

                } else {

                    let trimmedResponse;
                    if (answerDigits !== responseDigits) {
                        trimmedResponse = response.trim();
                        responseDigits = trimmedResponse.length;    
                    } else trimmedResponse = response;

                    let maxOffset = Math.abs(answerDigits - responseDigits);
                    let checkDigits = Math.min(answerDigits, responseDigits);

                    let maxMatches = 0;
                    for (let offset = 0; offset <= maxOffset; ++offset) {
                        let thisAnswer, thisResponse, goodOffset;
                        if (answerDigits > responseDigits) {
                            thisAnswer = answer.substr(offset, checkDigits);
                            thisResponse = trimmedResponse;
                            goodOffset = true;
                        } else {
                            thisAnswer = answer;
                            thisResponse = trimmedResponse.substr(offset, checkDigits);
                            let remainder = trimmedResponse.substr(0, offset) + trimmedResponse.substr(offset + checkDigits);
                            goodOffset = (remainder.replace(/ /g, '').length == 0);
                        }

                        if (goodOffset) {
                            let matches = 0;
                            for (let j = 0; j < checkDigits; ++j) {
                                if (thisAnswer[j] === '?' || thisResponse[j] === thisAnswer[j]) {
                                    matches++;
                                }
                            }
                            if (matches > maxMatches) maxMatches = matches;
                        }
                    }
                    numMatches = maxMatches;
                }

                if (markByDigits) {
                    if (numMatches > 0) answerMatched = true;
                    thisScore = numMatches;
                } else if (numMatches === answerDigits && responseDigits === answerDigits) {
                    answerMatched = true;
                    thisScore = 1.0;
                } else if (partialValue > 0.0) {
                    if (numMatches >= answerDigits / 2) {
                        answerMatched = true;
                        thisScore = partialValue;
                    } else thisScore = 0.0;
                } else thisScore = 0.0;
            } else thisScore = 0.0;

        } else if (questionType === 'nr_fraction') {

            const responseSplit = response.split('/');
            const answerSplit = answer.split('/');
            
            if (responseSplit.length != answerSplit.length) {
                thisScore = 0.0;
            } else {
                answerMatched = true;

                const numParts = responseSplit.length;
                for (let i = 0; i < numParts; ++i) {
                    const responsePart = responseSplit[i].trim();
                    const answerPart = answerSplit[i].trim();
                    if (/^\d+$/.test(answerPart)) {
                        if (/^\d+$/.test(responsePart)) {
                            if (parseInt(responsePart) != parseInt(answerPart)) {
                                answerMatched = false;                                
                            }
                        } else answerMatched = false;
                    } else if (responsePart !== answerPart) {
                        answerMatched = false;
                    }
                }

                thisScore = answerMatched ? 1.0 : 0.0;
            }

        }

        thisScore *= value;
        if (answerMatched && thisScore >= responseScore) {
            matchedAnswerIDs = answerIDs;
            responseScore = thisScore;
        }
    }

    var results = { score: responseScore };
    if (matchedAnswerIDs !== false) {
        results.answerIDs = matchedAnswerIDs;
    }

    return results;
}

function augmentQuestion(question) {

    if (question.ScoredResponses && question.ScoredResponses.length > 0) {
            
        // Add responses to ScoredResponses entries

        for (let pEntry of question.ScoredResponses) {
            let entry_ids = pEntry.id ? [pEntry.id] : pEntry.ids || [];
            let response_array = question.Answers.filter(answer => entry_ids.includes(answer.id)).map(answer => answer.response);

            if (response_array.length > 0) {
                response_array.sort();
                pEntry.response = response_array.join('');
            } else pEntry.response = '';
        }

        // Add unscored answers

        for (let entry of question.Answers) {
            let has_scored = question.ScoredResponses.some(scored => scored.id === entry.id);
            if (!has_scored) {
                question.ScoredResponses.push({
                    id: entry.id,
                    response: entry.response,
                    value: 0.0,
                });
            }
        }
    }

    question.maxValue = getMaxScore(question);

    return question;
}

function scoreResponse(response, question) {
    let questionType = question.type;
    let result = { score: 0.0 };

    if (response.length == 0) return result;
    else if (response.includes('!')) return false;

    if (questionType === 'wr') {

        let criteria = question.TypeDetails.criteria;
        let regexString = criteria.map(entry => `[ 0-${entry.value}][ \\+]`).join('');
        let regex = new RegExp(`^${regexString}$`);

        if (response[0] === '+') response = ' ' + response;
        if (response.length % 2 === 1) response += ' ';

        if (regex.test(response)) {
            for (let j = 0; j < criteria.length; ++j) {
                let this_score = response[2 * j] === ' ' ? 0 : parseInt(response[2 * j], 10);
                if (this_score < criteria[j].value && response[2 * j + 1] === '+') {
                    this_score += 0.5;
                }
                result.score += this_score;
            }
        } else result = false;

    } else if (questionType.startsWith('mc')) {

        // Sort multiple choice responses alphabetically
        
        let characters = response.trim().split('').sort();
        response = characters.join('');

        // Check keyed responses

        let responseScore = 0.0;
        for (let entry of question.ScoredResponses) {
            if (response === entry.response && entry.value >= responseScore) {
                responseScore = entry.value;
            }
        }

        // Get response IDs
    
        let matchedAnswerIDs = [];
        let validResponses = true;
        for (let j = 0; j < response.length; j++) {
            let found = false;
            for (let k = 0; k < question.Answers.length; k++) {
                if (question.Answers[k].response === response[j]) {
                    if ('id' in question.Answers[k]) {
                        matchedAnswerIDs.push(question.Answers[k].id);
                    } else validResponses = false;
                    found = true;
                    break;
                }
            }
            if (!found) validResponses = false;
        }
        if (matchedAnswerIDs.length !== response.length) validResponses = false;

        if (validResponses) {
            result.score = responseScore;
            if (matchedAnswerIDs.length > 0) {
                result.answerIDs = matchedAnswerIDs;
            }
        } else result = false;

    } else if (questionType.startsWith('nr')) {

        result = scoreNumericResponse(response, question);
    
    } else result = false;

    return result;
}

function scoreResults(scoringData, responseData) {

    // Initialize results object

    let results = {};
    results.questions = [];

    // Calculate total number of questions

    let questionCount = 0;
    scoringData.Sections.forEach(section => {
        if (!('Content' in section)) return;
        section.Content.forEach(() => {
            questionCount++;
        });
    });

    // Check if response data matches the number of questions

    if (responseData.length !== questionCount) return false;

    // Get weights for the questions

    const weights = getWeights(scoringData);

    let questions = [];
    let responseScores = [];
    let totalScore = 0.0;

    // Process each question in the scoring data

    scoringData.Sections.forEach(section => {
        if (!('Content' in section)) return;
        section.Content.forEach(question => {

            // Augment question with additional data

            question = augmentQuestion(question);

            // Assign weight to the question

            question.weight = weights[questions.length];

            // Update total score if the question is not omitted or bonus

            if (!('scoring' in question) || question.scoring === 'regular') {
                totalScore += question.maxValue;
            }

            // Prepare new entry for response scores

            let newEntry = {
                item_id: question.id,
                maxValue: question.maxValue,
                weight: question.weight
            };

            // Handle omitted or bonus questions

            if ('scoring' in question && question.scoring !== 'regular') {
                if (question.scoring === 'omit') newEntry.omitted = 1;
                else if (question.scoring === 'bonus') newEntry.bonus = 1;
            }

            responseScores.push(newEntry);
            questions.push(question);
        });
    });

    // Set the total possible score and whether the scoring is weighted

    if (!('outOf' in results)) {
        results.outOf = totalScore;
        results.weighted = (scoringData.Scoring.type !== 'Total');
    }

    // Process each student response

    for (let i = 0; i < responseData.length; i++) {
        let response = responseData[i];
        let question = questions[i];

        // Score the student's response and store results

        let result = scoreResponse(response, question);

        responseScores[i].response = response;
        if (result !== false) {
            responseScores[i].score = result.score;
            if ('answerIDs' in result) {
                responseScores[i].ids = result.answerIDs;
            }    
        } else responseScores[i].score = false;
    }

    // Assign the response scores to the results

    results.questions = responseScores;

    // Return the computed results

    return results;
}

export { 
    significantDigits, 
    getMaxScore, 
    scoreNumericResponse, augmentQuestion, scoreResponse, 
    scoreResults 
};
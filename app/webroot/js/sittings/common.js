/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var sittingsCommonInitializing = false;
var sittingsCommonInitialized = false;

import { localizationInstance } from '/js/FormEngine/Localization.js?7.0';

import { Annotation } from '/js/Annotation/annotation.js?7.0';

import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as AssessmentsRender from '/js/assessments/render.js?7.0';
import * as ScoringCommon from '/js/common/scoring.js?7.0';

var banner_timers = {};

var getInputWidth = function($input) {
	if ($('#matching_temp').length == 0) {
		$('body').append('<span id="matching_temp"></span>');
		$('#matching_temp').css('font-family', $input.css('font-family'));
		$('#matching_temp').css('font-size', $input.css('font-size'));
		$('#matching_temp').css('font-weight', $input.css('font-weight'));
	}
	$('#matching_temp').html($input.val());
	var inputWidth = $('#matching_temp').width() + 9;
	return Math.max(18, inputWidth);
}

var thinValue = function($element) {
	var thisValue = $element.val();
	var lastValue = $element.attr('data-last-value');
	var index;
	for (index = 0; index < lastValue.length; ++index) {
		if (lastValue.charAt(index) != thisValue.charAt(index)) break;
	}
	$element.val(thisValue.charAt(index).toUpperCase());
}

var showAndFade = function($target) {
	var target_id = $target.attr('id');
	if (target_id in banner_timers) {
		clearTimeout(banner_timers[target_id]);
	}

	$target.stop(true);
	$target.css('opacity', '1.0');		
	$target.show();
	
	banner_timers[target_id] = setTimeout(function($banner) {
		$banner.animate({
			opacity: '0.0'
		}, {
			'duration': 500, 
			'complete': function() {
				$banner.hide();
				$banner.css('opacity', '1.0');
			}
		});
	}, 3000, $target);
}

var markAssessment = function($target, questions, showPercent) {
	var $questionWrappers = $target.find('.part_wrapper');

	var commonCount = Math.min($questionWrappers.length, questions.length);

	for (var i = 0; i < commonCount; ++i) {
		var score = questions[i].score;
		var type = $questionWrappers.eq(i).attr('data-type');
		if (('omitted' in questions[i]) || (questions[i].maxValue == 0.0)) {
			var $target = $questionWrappers.eq(i).find('.question_number');
			$target.append('<div class="score_omitted"></div>');
		} else if (type == 'wr') {
			var $target =  $questionWrappers.eq(i).find('.wr_answer_wrapper .wr_input_field');
			var colour = (score == questions[i].maxValue) ? '#20de00' : '#fe0a00';
			if (score == 0.0) {
				$target.append('<div class="score_wrong"></div>');
			} else if (showPercent) {
				var percentage = (100.0 * score / questions[i].maxValue).toFixed(0);
				$target.append('<div class="score_value" style="color:red;">' + percentage + '%</div>');
			} else if (questions[i].maxValue > 1) {
				$target.append('<div class="score_value" style="color:' + colour + ';">' + score + '/' + 
					questions[i].maxValue + '</div>');
			} else {
				$target.append('<div class="score_value" style="color:' + colour + ';">' + score + '</div>');
			}
		} else {
			var $target = null;
			var $table = $questionWrappers.eq(i).find('.mc_answer_table');
			if ($table.length > 0) {
				if ($table.closest('.mc_table_wrapper').length == 0) {
					$table.wrap('<div class="mc_table_wrapper"></div>');
					$target = $table.parent();
				}
			} else $target = $questionWrappers.eq(i).find('.nr_answer_field, .mc_answer_list');
			if ($target.length == 0) $target = $questionWrappers.eq(i);

			if (score == questions[i].maxValue) {
				$target.append('<div class="score_correct"></div>');
			} else if (score == 0.0) {
				$target.append('<div class="score_wrong"></div>');
			} else if (showPercent) {
				var percentage = (100.0 * score / questions[i].maxValue).toFixed(0);
				$target.append('<div class="score_value" style="color:red;">' + percentage + '%</div>');
			} else if (questions[i].maxValue > 1) {
				$target.append('<div class="score_value" style="color:red;">' + score + '/' + 
					questions[i].maxValue + '</div>');
			} else {
				$target.append('<div class="score_value" style="color:red;">' + score + '</div>');
			}
		}
	}
	
	if ($questionWrappers.length != questions.length) {
		$.gritter.add({
			title: "Error",
			text: "Number of responses does not match number of questions.",
			image: "/img/error.svg"
		});					
	}
}

var previewFromHTML = async function($target, html, settings, view_json) {
	$target.html(html);

	// Clean up old versions

	$target.find('.wr_notes_wrapper').remove();

	// Finish rendering

	await RenderTools.setRenderStyle(settings.numberingType);
	updateReasoningFields($target, view_json);
}

var previewConstruct = async function($target, assessmentData, questionData, view_json) {

	// Draw title

	var titleHTML = '<div class="assessment_title"><div style="float:left;">';
	for (var i = 0; i < assessmentData.Title.text.length; ++i) {
		titleHTML += '<p>' + escapeHTML(assessmentData.Title.text[i]) + '</p>';
	}
	titleHTML += '</div><div style="clear:both;"></div>';

	$target.html(titleHTML);

	// Draw preamble if needed

	$target.append('<div class="preamble_wrapper" style="margin:30px 0px;"></div>');
	if ('Preamble' in assessmentData) {
		$target.find('.preamble_wrapper').html(assessmentData.Preamble.html);
		$target.find('.preamble_wrapper').show();
		RenderTools.renderEquations($target.find('.preamble_wrapper'));
		$('#preamble_item').show();
		$('#addPreamble').hide();
	} else {
		$('#preamble_item').hide();
		$target.find('.preamble_wrapper').hide();
		$('#addPreamble').show();
	}

	// Draw remainder of assessment

	AssessmentsRender.shuffleAssessmentQuestions(assessmentData);
	AssessmentsRender.shuffleAssessmentParts(assessmentData, questionData);

	await RenderTools.setRenderStyle(assessmentData.Settings.numberingType);
	await AssessmentsRender.drawAssessment(assessmentData, questionData, $target);

	$('#assessment_preview_wrapper .question_answer').css('width', '100%');

	AssessmentsRender.shuffleAssessmentAnswers(assessmentData, questionData, $target);
	await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($target);
	await applyOnlineStyling($target, questionData, view_json);
}

var applyOnlineStyling = async function($target, questionData, view_json) {

	// Replace MC letters with checkboxes

	$target.find('.mc_answer_list .answer_label').each(function() {
		var oldContent = $(this).html();
		$(this).empty();
		$(this).append("<input type='checkbox' class='response_checkbox online_response' />");
		$(this).append("<span class='response_old_label'>" + oldContent + "</span>");
	});
	
	$target.find('.mc_answer_table .answer_label').each(function() {
		var oldContent = $(this).html();
		$(this).empty();
		$(this).append("<input type='checkbox' class='response_checkbox online_response' />");
		$(this).append("<span class='response_old_label'>" + oldContent + "</span>");
	});

	$target.find('.matching_blank').each(function() {
		$(this).replaceWith('<input class="online_blank online_response" style="margin-right:10px;" type="text" data-last-value="" autocomplete="off" \>');
	});

	// Add input area for NR and WR questions

	var $questionWrappers;
	if ($target.is('.question_wrapper')) $questionWrappers = $target;
	else $questionWrappers = $target.find('.question_wrapper');

    for (let wrapper of $questionWrappers.toArray()) {
		if (!wrapper.hasAttribute('data-question-handle-id')) continue;

        const $wrapper = $(wrapper);
		var questionHandleID = $wrapper.attr('data-question-handle-id');
		var thisData = questionData[questionHandleID].json_data;

		for (var q = 0; q < thisData.Parts.length; ++q) {
			var partData = thisData.Parts[q];
			if (partData.type == 'context') continue;
		
			var $partWrapper = $wrapper.find('.part_wrapper[data-part-id="' + partData.id + '"]');
            $partWrapper.attr('data-part-index', partData.index);

			if (partData.type.substr(0, 2) == 'nr') {
				if ($partWrapper.find('.nr_answer_field').length == 0) {
					var numBlanks = partData.TypeDetails.nrDigits;
                    var questions_nr_label_online = await localizationInstance.getString('questions_nr_label_online');
					var newHTML = '<div class="nr_answer_field">' + questions_nr_label_online + ':<div class="input_wrapper">';
					for (var blankNum = 0; blankNum < numBlanks; ++blankNum) {
						newHTML += '<input class="online_response" type="text" data-last-value="" autocomplete="off" \>';
					}
					newHTML += '</div></div>';
					$partWrapper.find('.question_text').append(newHTML);
				}
			} else if (partData.type == 'wr') {
                var questions_nr_label_online = await localizationInstance.getString('questions_nr_label_online');
				var newHTML = '<div class="wr_answer_wrapper"><div>' + questions_nr_label_online + ':</div>';
				newHTML += '<div class="input_wrapper">';
				newHTML += '<div id="wr_input_' + questionHandleID + '" class="wr_input_field online_response template_view"></div>';
				newHTML += '</div></div>';
				$partWrapper.find('.wr_response_space').replaceWith(newHTML);
			}
		}
	}

    var questions_nr_prompt_online = await localizationInstance.getString('questions_nr_prompt_online');
	$('.response_prompt_location').html(questions_nr_prompt_online);
	
	updateReasoningFields($target, view_json);	
}

var updateReasoningFields = function($target, view_json) {
	var reasoningJSON = view_json.reasoning;

	// Add WR input field for student notes as required

	$target.find('.part_wrapper').each(function() {
		var contentID = $(this).closest('.question_wrapper').attr('data-content-id');
		var partIndex = $(this).attr('data-part-index');

		var hasReasoning = false;
		if (reasoningJSON.location == 'all') {
			if ($(this).attr('data-type') != 'wr') hasReasoning = true;
		} else if (reasoningJSON.location == 'selected') {
			for (var i = 0; i < reasoningJSON.parts.length; ++i) {
				var isMatch = true;
				if (contentID != reasoningJSON.parts[i].content_id) isMatch = false;
				else if (partIndex != reasoningJSON.parts[i].part_index) isMatch = false;
				if (isMatch) hasReasoning = true;
			}
		}

		if (hasReasoning) {
			if ($(this).find('.reasoning_wrapper').length == 0) {
				var newHTML = '<div class="reasoning_wrapper" style="margin-top:10px;"><div class="notes_label"></div>';
				newHTML += '<div class="input_wrapper">';
				newHTML += '<div class="wr_input_field online_response template_view"></div>';
				newHTML += '</div></div>';
				$(this).find('.question_text').append(newHTML);
			}
		} else $(this).find('.reasoning_wrapper').remove();
		$('.notes_label').html(reasoningJSON.label + ":");
	});
}

var fillResponses = function($target, responses) {
	var $questionWrappers = $target.find('.part_wrapper');
	if (responses.length != $questionWrappers.length) return false;

	var responseIndex = 0;
	$questionWrappers.each(function() {
		var thisResponse = responses[responseIndex].value;
		
		var questionHandleID = $(this).closest('.question_wrapper').attr('data-question-handle-id');
		var partID = $(this).attr('data-part-id');

		var $partMenuItem = $('.items_menu .menu_item[data-question-handle-id="' + questionHandleID + '"][data-part-id="' + partID + '"]');
		if (thisResponse.trim().length != 0) {
			$partMenuItem.find('.status_pip').css('background-image', 'url("/img/icons/pip-checked.svg")');
		} else $partMenuItem.find('.status_pip').css('background-image', 'url("/img/icons/pip-empty.svg")');
		
		if ($(this).attr('data-type') != responses[responseIndex].type) {
			$questionWrappers.find('.online_response').val('');
			return false;
		}
		var questionStyle = $(this).attr('data-type').substr(0, 2);

		if (questionStyle == 'mc') {
			var $textInput = $(this).find('input[type="text"].online_response');
			var $checkboxes = $(this).find('input[type="checkbox"].online_response');
			if ($textInput.length == 1) {

				$textInput.attr('data-last-value', thisResponse.toUpperCase());
				$textInput.val(thisResponse.toUpperCase());

				var $sectionWrapper = $(this).closest('.preview_section_wrapper');
				var allowMultiple = $sectionWrapper[0].hasAttribute('data-allow-multiple');

				if (allowMultiple) $textInput.css('width', getInputWidth($textInput));
				
			} else if ($checkboxes.length > 0) {

				var responseCharCode = "a".charCodeAt(0);
				$checkboxes.each(function() {
					var responseLetter = String.fromCharCode(responseCharCode);
					$(this).prop('checked', (thisResponse.indexOf(responseLetter) != -1));
					++responseCharCode;
				});

			}
		} else if (questionStyle == 'nr') {
			$(this).find('.online_response').each(function() {
				if (thisResponse.length > 0) {
					var thisCharacter = thisResponse.charAt(0);
					if (thisCharacter == ' ') thisCharacter = '';
					$(this).attr('data-last-value', thisCharacter);
					$(this).val(thisCharacter);
					thisResponse = thisResponse.substr(1);
				} else $(this).val('');
			});
		} else if (questionStyle == 'wr') {
			var $inputField = $(this).find('.wr_input_field');
			if ($inputField.is('.html_editor')) {
				$inputField.htmleditor('setValue', thisResponse);
			} else {
				$inputField.html(thisResponse);
				RenderTools.renderEquations($inputField);
			}
		}

		if ('notes' in responses[responseIndex]) {
			var thisNotes = responses[responseIndex].notes;
			var $notesField = $(this).find('.reasoning_wrapper .wr_input_field');
			if ($notesField.length > 0) {
				if ($notesField.is('.html_editor')) {
					$notesField.htmleditor('setValue', thisNotes);
				} else {
					$notesField.html(thisNotes);
					RenderTools.renderEquations($notesField);
				}
			}
		}

		if ('comment' in responses[responseIndex]) {
			var thisComment = responses[responseIndex].comment;
			var $commentField = getCommentField($(this));
			$commentField.html(thisComment);
			RenderTools.renderEquations($(this));
		}
			
		responseIndex++;
	});
	
	return true;
}

var drawAnnotations = function($target, annotations) {
	for (const [toolName, paths] of Object.entries(annotations)) {
		if (toolName in Annotation.tools) {
			var $new_wrapper = $(`<svg data-tool-name="${toolName}" class="annotation_wrapper"></svg>`);
			$new_wrapper.html(paths);

			const defaults = Annotation.tools[toolName].defaults;
			$new_wrapper.find('path').each(function() {
				if (!this.hasAttribute('stroke')) {
					$(this).attr('stroke', defaults.stroke);
				}
				if (!this.hasAttribute('stroke-width')) {
					$(this).attr('stroke-width', defaults.strokeWidth);
				}
			});

			$target.append($new_wrapper);
		}
	}	
}

var getCommentField = function($questionWrapper) {
	if ($questionWrapper.find('.wr_comment_wrapper').length == 0) {
		var newHTML = '<div class="wr_comment_wrapper" style="margin-top:10px;">'
		newHTML += '<div>Teacher comment:</div>';
		newHTML += '<div class="input_wrapper">';
		newHTML += '<div class="wr_input_field online_response template_view" disabled></div>';
		newHTML += '</div></div>';
		$questionWrapper.find('.question_text').append(newHTML);				
	}
	return $questionWrapper.find('.wr_comment_wrapper .wr_input_field');
}

var buildOutcomesTable = function($target, outcomes) {
	outcomes.sort(function(a, b) {
		if (a.name < b.name) return -1;
		if (a.name > b.name) return 1;
		return 0;	
	});

	var tableHTML = "";
	for (var i = 0; i < outcomes.length; ++i) {
		tableHTML += '<tr style="vertical-align:top;" data-question-indices="' + 
			outcomes[i].questions.join(',') + '"><td>' + outcomes[i].name + 
			'</td><td style="box-sizing:border-box;">';

		if ('percentage' in outcomes[i]) tableHTML += '<div>' + outcomes[i].percentage + '%</div>';
		else tableHTML += '<div>&mdash;</div>';

		if ('score' in outcomes[i]) tableHTML += '<div>(' + outcomes[i].score + '/' + outcomes[i].outOf + ')</div>';
		tableHTML += '</td><td style="text-align:center;"><button class="show_outcome btn btn-mini"';
        if (outcomes[i].questions.length == 0) tableHTML += ' disabled';
        tableHTML += '>Show</button></td></tr>';
	}
	$target.html(tableHTML);
}

var addNameLines = function($target, nameLinesData) {
	if ($target.find('.namelines_wrapper').length == 0) {
		var nameLinesHTML = "<div class='namelines_wrapper'>";		
		nameLinesHTML += "<div class='namelines_labels'>";
		for (var key in nameLinesData) {
			if (nameLinesData.hasOwnProperty(key)) {
				nameLinesHTML += "<div>" + key + ":</div>";
			}
		}
		nameLinesHTML += "</div>";
		nameLinesHTML += "<div class='namelines_data'>";
		for (var key in nameLinesData) {
			if (nameLinesData.hasOwnProperty(key)) {
				nameLinesHTML += "<div>" + nameLinesData[key] + "</div>";
			}
		}
		nameLinesHTML += "</div></div>";
		$target.find('.assessment_title').prepend(nameLinesHTML);	
	}
}

var getKeyHTML = function(keySections) {
	var keyHTML = '';
	for (var i = 0; i < keySections.length; ++i) {
		if ('header' in keySections[i]) {
			keyHTML += "<div class='key_header'>" + keySections[i].header + "</div>";				
		}
		

		for (var j = 0; j < keySections[i].Content.length; ++j) {
			var question = keySections[i].Content[j];

			keyHTML += '<div class="key_row">';
	
			var isOmitted = ('scoring' in question) && (question['scoring'] == 'omit');
			var isBonus = ('scoring' in question) && (question['scoring'] == 'bonus');

			var extra = "";
			if (isOmitted) extra += ", omitted";
			else if (isBonus) extra += ", bonus";
			else if (keySections[i].type == 'nr') {
				if (question.type == 'nr_selection') {
					if (question.TypeDetails.markByDigits) extra += ", marks are per digit";
					if (question.TypeDetails.ignoreOrder) extra += ", any order";
				} else {
					var fudgeFactor = parseFloat(question.TypeDetails.fudgeFactor);
					if (fudgeFactor != 0.0) {
						if (fudgeFactor < 1.0) fudgeFactor *= 100.0;
						extra += ", within " + fudgeFactor + "%";
					}
				}
			}
			
			var answersHTML = "";
			if (question.type == 'wr') {
				var criteriaHTML = '';
				for (var k = 0; k < question.TypeDetails.criteria.length; ++k) {
					criteriaHTML += '<div>' + question.TypeDetails.criteria[k]['label'] + 
						' (/' + question.TypeDetails.criteria[k]['value'] + ')</div>';
				}
				
				if ('Rubric' in question) {
					answersHTML = "<div>" + question.Rubric.html + "</div>";
				} else answersHTML = "<div>" + criteriaHTML + "</div>";
			} else {
				var responses = {};
				for (var k = 0; k < question.Answers.length; ++k) {
					responses[question.Answers[k].id] = question.Answers[k].response;
				}
				for (var k = 0; k < question.ScoredResponses.length; ++k) {
					var thisAnswer = '';
					if ('id' in question.ScoredResponses[k]) {
						var answerID = question.ScoredResponses[k].id;
						thisAnswer = responses[answerID];
					} else if ('ids' in question.ScoredResponses[k]) {
						for (var p = 0; p < question.ScoredResponses[k].ids.length; ++p) {
							var answerID = question.ScoredResponses[k].ids[p];
							thisAnswer += responses[answerID];	
						}
					}

					var thisScore = question.ScoredResponses[k].value;

					if (question.type == 'nr_selection') {
						if (question.TypeDetails.markByDigits) thisScore *= thisAnswer.length;
					}
					
					answersHTML += "<div><b>" + thisAnswer + "</b>";

					if (isOmitted) answersHTML += " (" + extra.substr(2) + ")";
					else if ((question.ScoredResponses.length > 1) || (thisScore != 1) || (extra.length > 0)) {
						answersHTML += " (" + thisScore.toString() + extra + ")";
					}
					
					answersHTML += "</div>";
				}
			}
					
			keyHTML += "<td valign='top'><div class='key_number'>" + question.number + 
				".</div><div class='key_answer'>" + answersHTML + "</div></div>";

			keyHTML += "</tr>";
		}

		keyHTML += "</table>";
	}

	return keyHTML;
}

function scoreAttempt(attempt) {
	var scores = ScoringCommon.scoreResults(attempt.layout, attempt.results);
	if (scores === false) return;

	// Adjust scoring to build in weights (TEMPORARY)

	var outOf = 0.0;
	for (let question of scores.questions) {
		question.score *= question.weight;
		question.maxValue *= question.weight;
		if (!question?.bonus && !question?.omitted) {
			outOf += question.maxValue;
		}
	}
	scores.outOf = outOf;

	attempt.scores = scores;

	// Check for unscored WR questions

	var wrUnscored = false;
	if (attempt.responses.length == attempt.results.length) {
		for (var i = 0; i < attempt.responses.length; ++i) {
			if ((attempt.responses[i].type == 'wr') && (attempt.responses[i].value.length > 0) && 
				(attempt.results[i].trim().length == 0)) wrUnscored = true;
		}
	}
	if (wrUnscored) attempt.wr_unscored = 1;

	// Add outcomes data

	if (attempt.show_outcomes) {
		var outcomes = [];
		for (let entry of attempt.layout.Outcomes) {
			var itemIDs = entry.item_ids;
	
			var questionIndices = [];
			var score = 0.0;
			var outOf = 0.0;

			var index = 0;
			for (let section of attempt.layout.Sections) {
				for (let question of section.Content) {
					for (let itemID of itemIDs) {
						if (itemID == question.id) {
							questionIndices.push(index);
							if (!scores.questions[index].omitted) {
								score += scores.questions[index].score;
								outOf += scores.questions[index].maxValue;
							}
						}
					}
					++index;
				}
			}

			var newEntry = {
				name: entry.name,
				questions: questionIndices
			}

			if (!scores.weighted) {
				newEntry.score = score;
				newEntry.outOf = outOf;
			}

			if (outOf > 0.0) {
				newEntry.percentage = (100.0 * score / outOf).toFixed(0);
			}

			outcomes.push(newEntry);
		}

		if (outcomes.length > 0) attempt.outcomes = outcomes;
	}
}

async function initialize() {
    if (sittingsCommonInitializing) return;
    sittingsCommonInitializing = true;

    await AssessmentsRender.initialize();

    var blurWaiting = [];
	var focusWaiting = [];

	$(document).on('focus', '.wr_input_field', function(e) {
		e.stopPropagation();
		var blurIndex = blurWaiting.indexOf($(this));
		if (blurIndex >= 0) blurWaiting.splice(blurIndex, 1);
		focusWaiting.push($(this));	
	});
	
	$(document).on('blur', '.wr_input_field', function(e) {
		var focusIndex = focusWaiting.indexOf($(this));
		if (focusIndex >= 0) focusWaiting.splice(focusIndex, 1);
		blurWaiting.push($(this));
	});

	$(document).on('click keyup', function(e) {
		
		// Handle blur and focus for htmleditor fields on click so that we don't (for example)
		// move dialog buttons out of the way when a field shrinks.
		
		setTimeout(function() {
			while (blurWaiting.length > 0) {
				var $element = blurWaiting.pop();
				if ($element.is('.html_editor')) {
					$element.htmleditor('makeInactive');
				}
			}
			while (focusWaiting.length > 0) {
				var $element = focusWaiting.pop();
				if ($element.is('.html_editor')) {
					$element.htmleditor('makeActive');
				}
			}
		}, 0);
	});

	$(document).on('keydown', 'input[type="text"].online_response', function(e) {
		if ((e.key === 'Backspace') || (e.key === 'Delete')) {
			var $testInput = $(this);
			var blankAfter = true;
			do {
				if ($testInput.val() != '') blankAfter = false;
				$testInput = $testInput.next();
			} while ($testInput.is('.online_response'));

			if (blankAfter) {
				var $prevInput = $(this).prev();
				if ($prevInput.is('.online_response')) $prevInput.focus();
			}
		}
	});

	$(document).on('input change', 'input[type="text"].online_response', function(e) {

		var $sectionWrapper = $(this).closest('.preview_section_wrapper');
		var allowNegative = $sectionWrapper[0].hasAttribute('data-allow-negative');
		var allowMultiple = $sectionWrapper[0].hasAttribute('data-allow-multiple');

		var needsSave = false;
		if ($(this).val().length == 0) {
			
			$(this).attr('data-last-value', '');
			needsSave = true;
			
		} else if ($(this).val() != $(this).attr('data-last-value')) {
			
			var $questionWrapper = $(this).closest('.part_wrapper');
			var type = $questionWrapper.attr('data-type');
			
			var regex, allInputs;
			if (type == 'mc') {

				var $itemWrapper = $(this).closest('.question_wrapper');
				var numAnswers = $itemWrapper.find('.context_answer_list .question_answer').length;
				regex = new RegExp('^[A-' + String.fromCharCode('A'.charCodeAt(0) + numAnswers - 1) + ']*$');

				if (allowMultiple) $(this).val($(this).val().toUpperCase());
				else thinValue($(this));

				allInputs = $(this).val();

			} else {

				if (type == 'nr_decimal') {
					regex = allowNegative ? /^[-.\d]*$/ : /^[.\d]*$/;
				} else if (type == 'nr_scientific') {
					regex = /^[\d]*$/;
				} else if (type == 'nr_selection') {
					regex = /^[\d ]*$/;
				} else if (type == 'nr_fraction') {
					regex = /^[-.\/\d]*$/;
				} else if (type == 'mc_truefalse') {
					regex = /^[TF]$/;
				}

				thinValue($(this));
				
				allInputs = '';
				$(this).parent().find('input[type="text"].online_response').each(function() {
					allInputs += $(this).val();
				});
				allInputs = allInputs.trim();
	
			}
			
			if (regex.test(allInputs)) {
				if ($(this).val() != $(this).attr('data-last-value')) {
					$(this).attr('data-last-value', $(this).val());
					needsSave = true;
				}
				
				var $next = $(this).next();
				if ($next.length == 0) $(this).blur();
				else $next.focus();
			} else {
				$(this).val($(this).attr('data-last-value'));
				
				var $errorMessage = $('<div class="answer_field_error">Invalid key pressed</div>');
	
				var $existing = $(this).closest('.nr_answer_field').find('.answer_field_error');
				if ($existing.length == 0) $questionWrapper.find('input[type="text"].online_response').last().after($errorMessage);
				else $existing.replaceWith($errorMessage);
				setTimeout(function() {
					$errorMessage.animate({
						opacity: 0
					}, 1000, function() {
						$errorMessage.remove();
					});
				}, 500);
			}
		}

		if (needsSave) $(this).trigger('saveData');

		if ((type == 'mc') && allowMultiple) {
			$(this).css('width', getInputWidth($(this)));
		}
	});
	
	$(document).on('click', '.question_answer_mc', function() {
		$(this).find('.response_checkbox').trigger('click');
	});

	$(document).on('click', '.mc_answer_table .question_answer', function() {
		$(this).find('.response_checkbox').trigger('click');
	});

	$(document).on('click', '.response_checkbox', function(e) {
		e.stopPropagation();

		var $sectionWrapper = $(this).closest('.preview_section_wrapper');
		var allowMultiple = $sectionWrapper[0].hasAttribute('data-allow-multiple');

		if ($(this).prop('checked') && !allowMultiple) {
			var $clicked = $(this);
			$(this).closest('.part_wrapper').find('.response_checkbox').each(function() {
				if (!$(this).is($clicked)) $(this).prop('checked', false);
			});
		}
	});
	
    sittingsCommonInitialized = true;
}

export { initialize, 
    showAndFade, 
    previewFromHTML, previewConstruct,
    buildOutcomesTable, getKeyHTML,
    applyOnlineStyling, markAssessment, fillResponses, drawAnnotations, updateReasoningFields, addNameLines,
    getCommentField,
	scoreAttempt
};
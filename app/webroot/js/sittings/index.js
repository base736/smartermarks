/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as IndexCommon from '/js/common/index.js?7.0';

function drawIndexRow(data) {

	// Add a row in the index table for this entry

	var canCopy = 1;
	var canDelete = 0;
	if (('Permissions' in data) && (data['Permissions'].indexOf('can_delete') >= 0)) canDelete = 1;

	var html = "<tr data-sitting-id='" + data['Sitting']['id'] + "' " +
		"data-can-copy='" + canCopy + "' " +
		"data-can-delete='" + canDelete + "'>";

	html += '<td style="width:15px; text-align:center;">' + 
		'<input type="checkbox" class="item_select" style="margin:1px 0px;" autocomplete="off" />' + 
		'</td>';

	if ('User' in data) {
		html += '<td style="width:250px;">' + data['User']['email'] + '</td>';
	}

	var date = dayjs.utc(data['Sitting']['moved']).local();
	html += '<td style="width:120px;">' + date.format('YYYY-MM-DD HH:mm') + '</td>';

	html += '<td><div style="display:flex; flex-direction:row; align-items:flex-start; gap:5px;">';
	html += '<div class="index_folder_link" data-folder-id="' + data['Sitting']['folder_id'] + '"></div>';
	if (data['Sitting']['is_new'] >= 2) html += "<div class='index_new_label label label-info'>New</div> ";
	html += '<div class="index_item_name" style="display:inline-block">' + escapeHTML(data['Sitting']['name']) + '</div>';
	html += '</div></td>';

	html += "</tr>";
	
	$('.index_table').append(html);
}

function finishIndex() {
	if (IndexCommon.activeNode === false) {

		$('#share_dropdown').removeAttr('disabled');
		$('#sendCopy_button').removeClass('always-disabled-link');
		$('#newItem_button').hide();

	} else {
				
		// Update button availability

		if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_add') >= 0) {
			$('#newVersion_button').removeClass('always-disabled-link');
		} else $('#newVersion_button').addClass('always-disabled-link');

		if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_export') >= 0) {
			$('#share_dropdown').removeAttr('disabled');
			$('#sendCopy_button').removeClass('always-disabled-link');
		} else {
			$('#share_dropdown').attr('disabled', 'disabled');
			$('#sendCopy_button').addClass('always-disabled-link');
		}

		// Set up right-side tab buttons

		$('.tab_right').hide();
		if (IndexCommon.activeNode.id == IndexCommon.roots['Trash'].id) $('#emptyTrash_button').show();

		// Check for an empty index

		if ($('.index_table tr').length == 0) {
			var html = "";
			html += '<tbody><tr class="disabled"><td colspan="5"><div style="padding-left:10px;">';
			html += '<i>No Sittings to show here.</i></div></td></tr></tbody>';
			$('.index_table').html(html);
		}
	}
}

$(document).ready(function() {

    IndexCommon.initialize({ drawIndexRow, finishIndex });
	
	$('#editSitting_button').click(function(e) {
		e.preventDefault();
		if ($(this).prop('disabled')) return;

		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var $tr = $(this).closest('tr');
				var $sittingID = $tr.data('sitting-id');
				window.location = "/Sittings/view/" + $sittingID;
			}
		});
	});

	$(document).on('dblclick', '.index_table tbody tr', function(e) {
		e.preventDefault();
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		var $sittingID = $(this).data('sitting-id');
		if (numChecked == 0) {
			window.location = "/Sittings/view/" + $sittingID;
		} else if (numChecked == 1) {
			var $selectedID = $selectedCheckbox.closest('tr').attr('data-sitting-id');
			if ($selectedID == $sittingID) {
				window.location = "/Sittings/view/" + $sittingID;
			}
		}
	});
});

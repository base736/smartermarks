/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as SittingsCommon from '/js/sittings/common.js?7.0';
import * as ClassListCommon from '/js/classlists/common.js?7.0';
import * as ScrollingCommon from '/js/common/scrolling.js?7.0';
import * as VersioningCommon from '/js/common/versioning.js?7.0';
import * as HtmlEdit from '/js/HTMLEditor/html_edit.js?7.0';
import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as AssessmentsRender from '/js/assessments/render.js?7.0';
import * as StatsCommon from '/js/common/stats.js?7.0';
import * as ScoringCommon from '/js/common/scoring.js?7.0';
import * as QuestionDetails from '/js/common/question_details.js?7.0';

$.extend($.ui.dialog.prototype.options, {
	resizable: false
});

var sittingData = null;
var joinToken = null;
var needsVersion = true;

var assessmentData = null;
var questionData = null;
var layoutSections = false;

var accountData = null;

document.addEventListener("DOMContentLoaded", async function() {

    await SittingsCommon.initialize();
    ClassListCommon.initialize();
    ScrollingCommon.initialize();
    await AssessmentsRender.initialize();
    await RenderTools.initialize();
    await HtmlEdit.initialize();
    QuestionDetails.initialize();

	StatsCommon.initFilterInterface();

    var has_wr = false;

	var scoringResults = {};
	var wr_allData = null;
	var wr_displayData = null;
	var wr_index = 0;

	var scoringTimeout = null;

	var needsRender = false;

	var studentEvents = null;
	var questionNumbers = null;
	var eventsPlot = null;

	/** start edit sitting */
	
	var startSittingEdit = function() {
		if (!canEdit) return;
				
		$('#dset_name').val($('#document_title').text());

		var start_type = sittingData.Access.start.type;
		$('#start_type').val(start_type);
		if (start_type == 'date') {
			var date = dayjs.utc(sittingData.Access.start.date).local();
			$('#start_date').datepicker('setDate', date.format('MMMM D, YYYY'));
			$('#start_time').val(date.format('HH:mm'));
		} else if (start_type == 'code') {
			$('#start_code').val(sittingData.Access.start.code);
		}
		
		var end_type = sittingData.Access.end.type;
		$('#end_type').val(end_type);
		if (end_type == 'date') {
			var date = dayjs.utc(sittingData.Access.end.date).local();
			$('#end_date').datepicker('setDate', date.format('MMMM D, YYYY'));
			$('#end_time').val(date.format('HH:mm'));
		}

		if (sittingData.Access.duration == 'none') {
			$('#duration_type').val('none');
			$('#duration_number').val(0);
		} else {
			$('#duration_type').val('defined');
			$('#duration_number').val(sittingData.Access.duration);

			if ('warnings' in sittingData.View) {
				$('#warnings_json').val(JSON.stringify(sittingData.View.warnings));
			} else $('#warnings_json').val('[]');
		}
		
		if (sittingData.Access.attempt_limit == 'none') {
			$('#attempts_type').val('unlimited');
			$('#attempts_number').val(1);
		} else {
			$('#attempts_type').val('defined');
			$('#attempts_number').val(sittingData.Access.attempt_limit);
		}

		$('#results_menu').attr('data-json-data', JSON.stringify(sittingData.Results));
		$("#results_menu li").removeClass("ui-state-active");
		$('#results_menu li').eq(0).trigger('click');

		if ('student_note' in sittingData.View) {
			$('#student_note').val(sittingData.View.student_note);
		} else $('#student_note').val('');

		$('#presentation').val(sittingData.View.presentation);

		$('#notes_policy').val(sittingData.View.reasoning.location);
		$('#notes_label').val(sittingData.View.reasoning.label);

		$('#shuffle_attempts').prop('checked', sittingData.View.shuffle_attempts);

        if ('smart_wr' in sittingData.Scoring) {
            $('#smart_wr_policy').val(sittingData.Scoring.smart_wr.location);
        } else $('#smart_wr_policy').val('none');

		updateStartDetails();
		updateEndDetails();
		updateDurationDetails();
		updateAttemptsDetails();
		
		$('#editSettings_form').dialog('open');
	};
	
	$(document).on('click', '.warningAdd', function() {
		rebuildWarningsJSON();
		var warnings = JSON.parse($('#warnings_json').val());

		var index;
		if (warnings.length == 0) index = 0;
		else index = parseInt($(this).attr('data-index')) + 1;
		warnings.splice(index, 0, '');

		$('#warnings_json').val(JSON.stringify(warnings));

   	    rebuildWarningLines();
		resizeSettingsWrappers();

		$('#dset_warning_' + index).focus();
    });
    
	$(document).on('click', '.warningDelete', function() {
		rebuildWarningsJSON();
		var warnings = JSON.parse($('#warnings_json').val());

		var index;
		if (warnings.length == 0) index = 0;
		else index = parseInt($(this).attr('data-index'));

		warnings.splice(index, 1);

		$('#warnings_json').val(JSON.stringify(warnings));

   	    rebuildWarningLines();
		resizeSettingsWrappers();
	});

	var rebuildWarningLines = function() {
		var warnings = JSON.parse($('#warnings_json').val());
		
		var warningsHTML = "";
		var numFields = warnings.length;
		if (numFields == 0) numFields = 1;
		for (var i = 0; i < numFields; ++i) {
			var time = (i < warnings.length) ? warnings[i] : '';
			warningsHTML += '<div class="options_row">';
			warningsHTML += '<div style="width:145px;"><input type="number" id="dset_warning_' + i + '" class="validate[required,custom[isPosInteger]] defaultTextBox2" min="1" style="width:40px; margin:0px;" value="' + time + '"/><label style="display:inline-block; margin:0px 5px;">minute' + ((time == 1) ? '' : 's') + ' left</label></div>';
			warningsHTML += '<button title="Add" class="actions_button add_icon warningAdd" style="margin-left:15px;" data-index=' + i + '>&nbsp;</button>';
			warningsHTML += '<button title="Delete" class="actions_button delete_icon warningDelete" data-index=' + i + '>&nbsp;</button>';
			warningsHTML += '</div>';
		}
		$('#warnings').html(warningsHTML);
		
		if (warnings.length == 0) {
			$('#warnings .deleteButton').attr('disabled', 'disabled');
			$('#warnings input').attr('disabled', 'disabled');
			$('#warnings label').addClass('disabled');
		}
		
		$('#warnings_wrapper').show();
	};
	
	$('#warnings').on('input', 'input', function() {
		var label = ($(this).val() == 1) ? 'minute left' : 'minutes left';
		$(this).siblings('label').html(label);
	});
	
	var rebuildWarningsJSON = function() {
		var warnings = [];
		$('#warnings input').each(function() {
			if (!this.hasAttribute('disabled')) warnings.push($(this).val());
		});
		$('#warnings_json').val(JSON.stringify(warnings));
	};

	var settingsSave = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		var isValid = true;

		if (($('#start_type').val() == 'date') && ($('#end_type').val() == 'date')) {
			var start_date = dayjs($('#start_date').datepicker('getDate'));
			var end_date = dayjs($('#end_date').datepicker('getDate'));
			if (start_date.isAfter(end_date)) {
				$('#end_time').validationEngine('showPrompt', '* Close date/time must be after open date/time', 'error', 'bottomLeft', true);
				isValid = false;
			}	
		}

		if (!checkItemOptionDates()) {
			$('#item_show_time').validationEngine('showPrompt', '* Hide date/time must be after show date/time', 'error', 'bottomLeft', true);
			isValid = false;
		}

		if (isValid && $form.validationEngine('validate')) {

			canCancel = 0;

			if ($('#start_type').val() == 'date') {
				start_date = dayjs($('#start_date').datepicker('getDate'));
				var timeParts = $('#start_time').val().match(/(\d+)/g);
				start_date = start_date.hour(timeParts[0]).minute(timeParts[1]).utc();

				sittingData.Access.start = {
					type: 'date',
					date: start_date.format('YYYY-MM-DD HH:mm:ss')
				};
			} else if ($('#start_type').val() == 'code') {
				sittingData.Access.start = {
					type: 'code',
					code: $('#start_code').val()
				};
			} else {
				sittingData.Access.start = {
					type: 'none'
				};
			}
			
			if ($('#end_type').val() == 'date') {
				end_date = dayjs($('#end_date').datepicker('getDate'));
				var timeParts = $('#end_time').val().match(/(\d+)/g);
				end_date = end_date.hour(timeParts[0]).minute(timeParts[1]).utc();

				sittingData.Access.end = {
					type: 'date',
					date: end_date.format('YYYY-MM-DD HH:mm:ss')
				};
			} else {
				sittingData.Access.end = {
					type: 'none'
				};
			}

			if ($('#duration_type').val() == 'defined') {
				sittingData.Access.duration = parseInt($('#duration_number').val());
	
				rebuildWarningsJSON();
				var warnings = JSON.parse($('#warnings_json').val());
				if (warnings.length > 0) {
					for (var i = 0; i < warnings.length; ++i) warnings[i] = parseInt(warnings[i]);
					sittingData.View.warnings = warnings;
				} else {
					delete sittingData.View.warnings;
				}
			} else {
				sittingData.Access.duration = 'none';
				delete sittingData.View.warnings;
			}
			
			if ($('#attempts_type').val() == 'defined') {
				sittingData.Access.attempt_limit = parseInt($('#attempts_number').val());
			} else sittingData.Access.attempt_limit = 'none';
			
			saveItemOptions();
			sittingData.Results = JSON.parse($('#results_menu').attr('data-json-data'));
		
			if ($('#student_note').val().length > 0) {
				var studentNote = $('#student_note').val().trim();
				if (studentNote.charAt(studentNote.length - 1) != '.') studentNote += '.';
				sittingData.View.student_note = studentNote;
			} else delete sittingData.View.student_note;

			sittingData.View.presentation = $('#presentation').val();

			sittingData.View.reasoning.location = $('#notes_policy').val();
			sittingData.View.reasoning.label = $('#notes_label').val();
            
            if ($('#notes_policy').val() == 'all') {
                delete sittingData.View.reasoning.parts;
            } else if (!('parts' in sittingData.View.reasoning)) {
                sittingData.View.reasoning.parts = [];
            }

			var needsReload = (sittingData.View.shuffle_attempts != $('#shuffle_attempts').prop('checked'));
			sittingData.View.shuffle_attempts = $('#shuffle_attempts').prop('checked');
    
            if (has_wr && ($('#smart_wr_policy').val() != 'none')) {
                sittingData.Scoring.smart_wr = {
                    location: $('#smart_wr_policy').val()
                };
            } else delete sittingData.Scoring.smart_wr;


			// Save new settings

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			var data_string = JSON.stringify(sittingData);

			var postData = {
				save_name: $('#dset_name').val(),
				data_string: data_string,
				data_hash: SparkMD5.hash(data_string)
			};

			ajaxFetch('/Sittings/save/' + sittingID, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(postData)
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
			
				if (responseData.success) {
					if (!needsReload) {

						$('#document_title').text($('#dset_name').val());
						if (!canEdit) $('#document_title').append(' &mdash; <b>READ ONLY</b>');
	
						SittingsCommon.updateReasoningFields($('#assessment_preview_wrapper'), sittingData.View);
	
						if (sittingData.View.shuffle_attempts) $('#version_controls').hide();
						else $('#version_controls').show();
	
						$.gritter.add({
							title: "Success",
							text: "Settings saved.",
							image: "/img/success.svg"
						});		

						$dialog.dialog('close');	
						
					} else {

						initialize().then(function() {
							$dialog.dialog('close');	
						});

					}
				} else showSubmitError($dialog);
			});
		}
	}

	$('#version_shuffle').on('click', function() {

		$('#sections_menu').empty();
		$('.items_panel').remove();

		$('#assessment_preview_wrapper').css('opacity', 0.0);
		$('#version_shuffling').show();

		$('#version_shuffle').attr('disabled', 'disabled');
		$('#version_save').attr('disabled', 'disabled');
		$('#version_print').attr('disabled', 'disabled');

		layoutSections = false;

		SittingsCommon.previewConstruct($('#assessment_preview_wrapper'), assessmentData, questionData, sittingData.View)
		.then(function() {
			finishAssessmentView();
			$('#version_shuffle').removeAttr('disabled');
			$('#version_save').removeAttr('disabled');
		});
	});

	$('#version_print').on('click', function(e) {
		e.preventDefault();

		navigateWithPost('/build_pdf', {
			pdfType: 'pdf_from_url',
			url: '/Sittings/print_questions/' + sittingID,
			postDataJSON: JSON.stringify({ suppressInputs: true }),
			target: 'version.pdf'
		});
	});

	$('#version_save').on('click', function() {
		initSubmitProgress($('#version_wrapper'));
		saveVersion(true);
	});

	var saveVersion = function(showMessages) {

		showSubmitProgress($('#version_wrapper'));

		var $wrapper = $('#assessment_preview_wrapper');

		// Remove reasoning and unrender equations

		$wrapper.find('.reasoning_wrapper').remove();
		RenderTools.unrenderEquations($wrapper);

		// Save version

		var layoutJSON = JSON.stringify(layoutSections);
		var versionHTML = $wrapper.html();

		ajaxFetch('/Sittings/save_version/' + sittingID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				layout_hash: SparkMD5.hash(layoutJSON),
				layout_sections: layoutJSON,
				html_hash: SparkMD5.hash(versionHTML),
				html: versionHTML
			})
		}).then(async response => {
			const responseData = await response.json();

            if (responseData.success) {
				needsVersion = false;

				// Re-render equations and add reasoning as required

				RenderTools.renderEquations($wrapper);
				SittingsCommon.updateReasoningFields($wrapper, sittingData.View);

				// Show notification
		
				if (showMessages) {
					$.gritter.add({
						title: "Success",
						text: "Common version saved.",
						image: "/img/success.svg"
					});	
				}

				hideSubmitProgress($('#version_wrapper'));
				$('#version_save').attr('disabled', 'disabled');
				$('#version_print').removeAttr('disabled');
			} else {
				$.gritter.add({
					title: "Error",
					text: "Unable to save common version.",
					image: "/img/error.svg"
				});
				hideSubmitProgress($('#version_wrapper'));
			}

		}).catch(function() {
			$.gritter.add({
				title: "Error",
				text: "Unable to save common version.",
				image: "/img/error.svg"
			});
			hideSubmitProgress($('#version_wrapper'));
		});
	}

    /* *
     * * initialize dialog box for settings
     * */
     
    $('#editSettings_form').dialog({
        autoOpen: false,
        width: 730,
        modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');

			initSubmitProgress($(this));

			resizeSettingsWrappers();		
			$('.document_settings_tab:first-child').trigger('mousedown');
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });
	
	$('#editSettings_form').find('button:contains("Cancel")').click(function(){
		if (canCancel == 1) {
			if (pendingAJAXPost) return false;
			showSubmitProgress($('#editSettings_form'));
			pendingAJAXPost = true;
	
			ajaxFetch('/Sittings/delete/' + sittingID, {
				method: 'POST'
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();

				if (responseData.success) {

					addClientFlash({
						title: "Success",
						text: "New sitting cancelled.",
						image: "/img/success.svg"
					});
					window.location = '/Sittings/index';	

				} else {

					throw new Error("Unable to cancel sitting");

				}
				
			}).catch(function() {
				showSubmitError();
			});
		} else $('#editSettings_form').dialog("close");
	});
		 	
	$('#editSettings_form').find('button:contains("Save")').click(function(){
		var contextedSave = $.proxy(settingsSave, $('#editSettings_form'));
		contextedSave();
	});

	$('.document_settings_tab').on('mousedown', function() {
		if (!$(this).hasClass('selected')) {
			var activeID = $('.document_settings_tab.selected').attr('data-wrapper-id');
			if ((activeID == 'tab_results_wrapper') && !checkItemOptionDates()) {

				$('#item_show_time').validationEngine('showPrompt', '* Hide date/time must be after show date/time', 'error', 'bottomLeft', true);
	
			} else if ($('#editSettings_form form').validationEngine('validate')) {
				$('.document_settings_tab').removeClass('selected');
				$(this).addClass('selected');
				
				$('.settings_wrapper').hide();
				$('#' + $(this).attr('data-wrapper-id')).show();
				
				resizeSettingsWrappers();
			}
		}
		return false;
	});

	function resizeSettingsWrappers() {
		$('#editDocument_buttons').css('margin-top', '5px');
		$('#editDocument_wrapper_left').css('height', 'auto');
		var leftWrapperHeight = $('#editDocument_wrapper_left').outerHeight();
		$('#editDocument_wrapper_right').css('height', 'auto');
		var rightWrapperHeight = $('#editDocument_wrapper_right').outerHeight();

		var newHeight = Math.max(leftWrapperHeight, rightWrapperHeight);
		$('#editDocument_wrapper_left').outerHeight(newHeight);
		$('#editDocument_wrapper_right').outerHeight(newHeight);
		if (rightWrapperHeight > leftWrapperHeight) {
			var newTopMargin = (rightWrapperHeight - leftWrapperHeight + 5) + 'px';
			$('#editDocument_buttons').css('margin-top', newTopMargin);
		}
	}

	var initialize = async function() { 
		const response = await ajaxFetch('/Sittings/get/' + sittingID);
		const responseData = await response.json();

		$('#document_title').html(responseData.Sitting.name);
		if (!canEdit) $('#document_title').append(' &mdash; <b>READ ONLY</b>');

		sittingData = responseData.Sitting.json_data;
		joinToken = responseData.Sitting.join_token;

		// Load assessment data

		assessmentData = responseData.Assessment;

		// Load question data

		questionData = {};
		for (var k = 0; k < responseData.Questions.length; ++k) {
			var json_data = responseData.Questions[k].json_data;
	
			// Intialize used IDs

			if ('Answers' in json_data) {
				for (var i = 0; i < json_data.Answers.length; ++i) {
					usedIDs.push(json_data.Answers[i].id);
				}	
			}

			for (var i = 0; i < json_data.Parts.length; ++i) {
				if ('Answers' in json_data.Parts[i]) {
					for (var j = 0; j < json_data.Parts[i].Answers.length; ++j) {
						usedIDs.push(json_data.Parts[i].Answers[j].id);
					}	
				}
			}
			
			// Store question data
			
			var questionHandleID = responseData.Questions[k].question_handle_id;
			questionData[questionHandleID] = responseData.Questions[k];
		}

		// Check for WR questions

		has_wr = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var questionHandleIDs;
				if (assessmentData.Sections[i].Content[j].type == 'item') {
					questionHandleIDs = [assessmentData.Sections[i].Content[j].question_handle_id];
				} else if (assessmentData.Sections[i].Content[j].type == 'item_set') {
					questionHandleIDs = assessmentData.Sections[i].Content[j].question_handle_ids;
				} else continue;

				for (var k = 0; k < questionHandleIDs.length; ++k) {
					var thisQuestionData = questionData[questionHandleIDs[k]];
					for (var p = 0; p < thisQuestionData.json_data.Parts.length; ++p) {
						if (thisQuestionData.json_data.Parts[p].type == 'wr') has_wr = true;
					}
				}
			}
		}

		if (has_wr) {
			$('.document_settings_tab[data-wrapper-id="tab_scoring_wrapper"]').show();
		} else $('.document_settings_tab[data-wrapper-id="tab_scoring_wrapper"]').hide();

		// Load other data

		StatsCommon.initFullStatistics(responseData.Statistics);

		accountData = responseData.Accounts;	
		needsVersion = responseData.Sitting.needs_version;
		
		needsRender = true;
		
		if (sittingData.View.shuffle_attempts) {

			layoutSections = false;
			
			await SittingsCommon.previewConstruct($('#assessment_preview_wrapper'), assessmentData, questionData, sittingData.View);

		} else {

			layoutSections = responseData.Version.layoutSections;

			var assessmentHTML = responseData.Version.html;
			await SittingsCommon.previewFromHTML($('#assessment_preview_wrapper'), assessmentHTML, assessmentData.Settings, sittingData.View);

			// Render equations

			await RenderTools.renderEquations($('#assessment_preview_wrapper'));

		}

		finishAssessmentView();
		triggerActiveView();
		
		$('#sitting_options').removeAttr('disabled');
		$('#version_shuffle').removeAttr('disabled');
		$('#version_print').removeAttr('disabled');

		// Open dialogs as required

		if (showSettings > 0) startSittingEdit();
	}

	var triggerActiveView = function() {
	
		// Show the selected view

		var urlVars = getUrlVars();

		if ('view' in urlVars) {
			var $viewButton = $('.view_button[data-view-type="' + urlVars.view + '"]:visible');
			if ($viewButton.length == 1) $viewButton.trigger('click');
			else $('.view_button:visible').first().trigger('click');
		} else $('.view_button:visible').first().trigger('click');
	}

	$(".view_button").click(function(e) {
		e.preventDefault();
		if (!$("#view_menu").menu("option", "disabled")) {
			var urlVars = getUrlVars();
			urlVars.view = $(this).attr('data-view-type');
			setUrlVars(urlVars);

			$(".view_button").removeClass("ui-state-active");
			$(this).addClass("ui-state-active");

			drawView();
		}
	});

	var drawView = function() {
		$('.right_panel').hide();
		$('.left_details').hide();

		var urlVars = getUrlVars();
		if (urlVars.view == 'preview') {
			$('#preview_panel').show();
			drawPreview();
		} else if (urlVars.view == 'access') {
			$('#access_panel').show();
			drawAccess();
		} else if (urlVars.view == 'results') {
			drawResults();
		} else if (urlVars.view == 'statistics') {
			$('#statistics_wrapper').show();
			drawStatistics();
		}
	}

	var drawPreview = function() {
		$('#navigation_wrapper').show();

        ScrollingCommon.resetLeftPanel();

		if (needsRender) {
		
			// Bring equations back and rebuild MC widths

			RenderTools.renderEquations($('#assessment_preview_wrapper')).then(function() {
				$('#assessment_preview_wrapper .question_answer').css('width', '100%');
				$('#assessment_preview_wrapper .part_wrapper').each(function() {
					if ($(this).find('.question_answer_mc, .question_answer_table').length > 0) {
						RenderTools.setMCImageWidths($(this));
					}
				});
				
				needsRender = false;
			});
		}	
	}

	var drawAccess = function() {

		// Show student accounts attached to this sitting

		$(".student_row").remove();

		var accountsBouncedItems = "";
		var accountsNewItems = "";
		var accountsValidatedItems = "";
		for (var i = 0; i < accountData.length; ++i) {
			if ('user_id' in accountData[i]) {
				var rowStart = '<tr class="student_row" id="student_row_' + accountData[i].id + '"' + 
                    ' data-student-user-id="' + accountData[i].user_id + '"' + 
                    ' data-sitting-student-id="' + accountData[i].id + '">' +
                    '<td class="student_email">' + accountData[i].details['Email'] + '</td>';
				
				if (accountData[i].bounced) {
					accountsBouncedItems += rowStart + '<td>' +
						'<button title="Edit email" class="actions_button edit_icon" style="margin-right:3px;">&nbsp;</button>';
					if (canEdit && canViewResults) {
						accountsBouncedItems += '<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
					}
					accountsBouncedItems += '</td></tr>';
				} else if (!accountData[i].validated) {
					accountsNewItems += rowStart + '<td>' +
						'<button title="Re-send verification" class="actions_button email_icon" style="margin-right:3px;">&nbsp;</button>';
					if (canEdit && canViewResults) {
						accountsNewItems += '<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
					}
					accountsNewItems += '</td></tr>';
				} else {
					accountsValidatedItems += rowStart + '<td>';
					if (canEdit && canViewResults) {
						accountsValidatedItems += '<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
					}
					accountsValidatedItems += '</td></tr>';
				}
			}
		}

		$('#accounts_table .header_empty').show();

		if (accountsBouncedItems.length > 0) {
			$('#header_bounced_accounts').show();
			$('#accounts_table .header_empty').hide();
			$('#header_bounced_accounts').after(accountsBouncedItems);
		} else $('#header_bounced_accounts').hide();

		if (accountsNewItems.length > 0) {
			$('#header_new_accounts').show();
			$('#accounts_table .header_empty').hide();
			$('#header_new_accounts').after(accountsNewItems);
		} else $('#header_new_accounts').hide();

		if (accountsValidatedItems.length > 0) {
			$('#header_validated_accounts').show();
			$('#accounts_table .header_empty').hide();
			$('#header_validated_accounts').after(accountsValidatedItems);
		} else $('#header_validated_accounts').hide();

		// Show shareable link information

		$('.tok_link').text('https://smartermarks.com/Sittings/join/' + joinToken);

		$('.identifier_select').empty();
		if ('nameLines' in assessmentData.Title) {
			var namelines = assessmentData.Title.nameLines;
			for (var i = 0; i < namelines.length; ++i) {
				$('.identifier_select').append('<option value="' + namelines[i] + '">' + namelines[i] + '</option>');
			}	
		}
		$('.identifier_select').append('<option value="Custom">Custom</option>');

		if ('token_details' in sittingData.Access) {
			rebuildLinkDisplay('tok', sittingData.Access.token_details);
		} else rebuildLinkDisplay('tok', {});

		// Finish up

        ScrollingCommon.resetLeftPanel();
	}

	var drawResults = function() {
		if ($('.results_view_button.active').length > 0) {
			$('.results_view_button.active').trigger('click');
		} else $('.results_view_button[data-view-type="summary"]').trigger('click');
	}

	$(".results_view_button").click(function(e) {
		e.preventDefault();
		if (!$("#results_view_menu").menu("option", "disabled")) {
			$("#results_view_menu li").removeClass("ui-state-active");
			$(this).addClass("ui-state-active");

			$('.right_panel').hide();
	
			if ($(this).attr('data-view-type') == 'written') {
				$('#written_panel').show();
				drawWritten();
			} else if ($(this).attr('data-view-type') == 'summary') {
				$('#summary_panel').show();
				drawSummary();
			} else if ($(this).attr('data-view-type') == 'student') {
				$('#student_panel').show();
				drawStudent();
			}
		}
	});

	var drawWritten = function() {
		$('.left_details').hide();
		$('#results_views').show();

		$('#written_wrapper').show();

		wr_displayData = null;
		rebuildWrittenViews();
	}

	var drawSummary = function() {
		$('.left_details').hide();
		$('#results_views').show();

		$('#loading_summary').show();   

		$('#sitting_controls_wrapper').hide();
		$('#results_wrapper').hide();
   		
   		buildSummary();
	}

	var drawStudent = function() {
		$('.left_details').hide();
		$('#results_views').show();

		$('.results_status').hide();
		
		if ($('#student_title')[0].hasAttribute('data-sitting-student-id')) {
			$('#students_wrapper').show();

			showSubmitProgress($('#student_reload_wrapper'));
			var sittingStudentID = $('#student_title').attr('data-sitting-student-id');
			$('#student_menu .menu_item[data-sitting-student-id="' + sittingStudentID + '"]').trigger('click');
		} else if ($('#student_menu .menu_item').length > 0) {
			$('#students_wrapper').show();
			$('#student_menu .menu_item').first().trigger('click');
		} else {
			$('#no_students').show();
			$('#attempt_overview_wrapper').hide();
			$('#student_details_window').hide();
		}
	}

	var drawStatistics = function() {
		$('#navigation_wrapper').show();

		$('#statistics_panel').show();

		var question_ids = [];
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				if (assessmentData.Sections[i].Content[j].type == 'item') {
					var questionHandleID = assessmentData.Sections[i].Content[j].question_handle_id;
					question_ids.push(questionData[questionHandleID].id);	
				} else if (assessmentData.Sections[i].Content[j].type == 'item_set') {
					for (var k = 0; k < assessmentData.Sections[i].Content[j].question_handle_ids.length; ++k) {
						var questionHandleID = assessmentData.Sections[i].Content[j].question_handle_ids[k];
						question_ids.push(questionData[questionHandleID].id);		
					}
				}
			}
		}

		$("#stats_print").attr('disabled', 'disabled');

        var renderPromise = Promise.resolve();
        
		var urlVars = getUrlVars();
		if ('c' in urlVars) {
			var questionHandleID = false;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					var itemData = assessmentData.Sections[i].Content[j];
					if (itemData.id == urlVars.c) {
						if (itemData.type == 'item') {
							questionHandleID = itemData.question_handle_id;
						} else if (itemData.type == 'item_set') {
							var renderedIndex = ('rendered_index' in itemData) ? itemData.rendered_index : 0;
							questionHandleID = itemData.question_handle_ids[renderedIndex];
						}
					}
				}
			}

			if (questionHandleID !== false) {
			
				$('#statistics_graphs').html("<div style='font-style:italic'>Loading statistics</div>");
				$('#item_statistics_data').html("<div style='font-style:italic'>Loading statistics</div>");
				$('#statistics_n').html('0');

				$('#statistics_item').attr('data-question-handle-id', questionHandleID);

				var $itemWrapper = $('<div class="question_wrapper" data-content-id="' + urlVars.c + '"></div>');
				$('#statistics_item').empty().append($itemWrapper);

				renderPromise = AssessmentsRender.renderItemToDiv(questionData[questionHandleID].json_data, $itemWrapper)
				.then(async function() {
					await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($('#statistics_item'));
					$itemWrapper.find('.part_wrapper[data-type="mc"] .question_answer').css('width', '100%');
				});

			} else {
	
				$('#statistics_item').html('<i>Error loading question.</i>');
				$('#item_statistics_wrapper').hide();
	
			}

		} else {

			$('#statistics_item').html('<i>No question selected.</i>');
			$('#item_statistics_wrapper').hide();

		}

		renderPromise.then(function() {
			return StatsCommon.loadFilteredStatistics(question_ids);
		})
		.then(function() {
			var urlVars = getUrlVars();
			if ('c' in urlVars) {
				var questionHandleID = parseInt($('#statistics_item').attr('data-question-handle-id'));
				var questionID = questionData[questionHandleID].id;

				StatsCommon.rebuildItemStatistics(questionID, questionData[questionHandleID].json_data, assessmentData.Settings.numberingType);
			}

			var questionHandleIDs = StatsCommon.getQuestionHandleIDs(assessmentData);
			StatsCommon.rebuildAssessmentStatistics(questionHandleIDs, questionData);

            ScrollingCommon.resetLeftPanel();
	
			$("#stats_print").removeAttr('disabled');
		});
	}

	var rebuildLinkDisplay = function(prefix, json_data) {		
		var join_field;
		if ('fieldName' in json_data) join_field = json_data.fieldName;
		else if ($('#' + prefix + '_identifier option').length > 0) {
			join_field = $('#' + prefix + '_identifier option').eq(0).attr('value');
		} else join_field = 'Custom';

		$('#' + prefix + '_custom').val(join_field);
		$('#' + prefix + '_identifier').val('Custom');
		$('#' + prefix + '_identifier option').each(function() {
			if ($(this).attr('value') == join_field) $('#' + prefix + '_identifier').val(join_field);
		});
		$('#' + prefix + '_identifier').trigger('change');

		var join_values = [];
		var list_string = '';

		if ('fieldValues' in json_data) {
			join_values = json_data.fieldValues;
			for (var i = 0; i < join_values.length; ++i) {
				if (i > 0) list_string += "\n";
				list_string += join_values[i];
			}
		}

		$('#' + prefix + '_identifier_list').val(list_string);

		if (prefix == 'tok') {
			if (list_string.length == 0) {
				$('#tok_detail_wrapper').hide();
				$('#tok_empty_wrapper').show();
			} else {
				$('#tok_empty_wrapper').hide();
				$('#tok_detail_wrapper').show();
			}
		} else if (prefix == 'dtok') {
			if (list_string.length == 0) {
				$('#enable_link').prop('checked', false);
				$('#identifier_wrapper').hide();
			} else {
				$('#enable_link').prop('checked', true);
				$('#identifier_wrapper').show();
			}
		}
	}

	var drawLeftBar = function(assessmentData, questionData) {
		$('#sections_menu').empty();
		$('.items_panel').remove();
		
		var activeSectionID = null;
		var activeContentID = null;
	
		var urlVars = getUrlVars();
		if ('s' in urlVars) {
			activeSectionID = urlVars.s;
		} else if ('c' in urlVars) {
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					if (assessmentData.Sections[i].Content[j].id == urlVars.c) {
						activeSectionID = assessmentData.Sections[i].id;
					}	
				}
			}
	
			if (activeSectionID != null) {
				activeContentID = urlVars.c;
			}
		}
	
		var foundSection = false;
	
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			var sectionData = assessmentData.Sections[i];
			if (sectionData.type == 'page_break') continue;
	
			if (sectionData.id == activeSectionID) foundSection = true;
	
			var menuItemHTML = 
				'<li><div class="menu_item" data-section-id="' + sectionData.id + '">' + 
				'<div class="menu_text">' + escapeHTML(sectionData.header) + '</div>';
			menuItemHTML += '</div></li>';	
	
			var $sectionMenuItem = $(menuItemHTML);
			$('#sections_menu').append($sectionMenuItem);
	
			var detailPanelHTML =
				'<div class="items_panel" id="items_panel_' + sectionData.id + '" data-section-id="' + sectionData.id + '" style="display:none;"> \
					<div class="window_bar">' + escapeHTML(sectionData.header) + '</div> \
					<div class="window_content" style="padding:5px; overflow:visible;"> \
						<div class="layout_panel"> \
							<ul class="items_menu menu_wrapper">';
	
			if (sectionData.Content.length > 0) {
				for (var j = 0; j < sectionData.Content.length; ++j) {
					var itemData = sectionData.Content[j];
	
					if (itemData.type == 'item') {
	
						var thisData = questionData[itemData.question_handle_id];
			
						detailPanelHTML += 
							'<li><div class="menu_item question content_item" data-content-id="' + itemData.id + '" data-question-handle-id="' + thisData.question_handle_id + '">' +
							'<div class="menu_text">' + escapeHTML(thisData.name) + '</div>';
						detailPanelHTML += '</div></li>';
			
					} else if (itemData.type == 'item_set') {
	
						detailPanelHTML += 
							'<li><div class="menu_item question_set content_item" data-content-id="' + itemData.id + '">' +
							'<div class="question_set_reveal"></div>' +
							'<div class="menu_text">(' + itemData.question_handle_ids.length + ' items) ' + escapeHTML(itemData.name) + '</div>';
							detailPanelHTML += '</div>';
	
						for (var k = 0; k < itemData.question_handle_ids.length; ++k) {
							var thisData = questionData[itemData.question_handle_ids[k]];
			
							detailPanelHTML += 
								'<div class="menu_item question child_item" style="display:none;" data-question-handle-id="' + thisData.question_handle_id + '">' +
								'<div class="menu_text">' + escapeHTML(thisData.name) + '</div>';
							detailPanelHTML += '</div>';
	
						}
	
						detailPanelHTML += '</li>';
			
					}
				}
			}
			detailPanelHTML += '</ul> \
						</div> \
					</div> \
				</div>';
			
			$('#navigation_wrapper').append(detailPanelHTML);
		}
	
		if (!foundSection) {
			activeSectionID = null;
			activeContentID = null;
		}
		if ((activeSectionID == null) && (assessmentData.Sections.length > 0)) {
			activeSectionID = assessmentData.Sections[0].id;
		}
		if (activeSectionID != null) {
			if (activeContentID != null) selectItem(activeContentID);
			else selectSection(activeSectionID);
		}
	
		$('#sections_panel').show();
		$('#sections_panel').removeAttr('data-needs-render');
	}

    var buildLayoutSections = function($wrapper) {
        RenderTools.unrenderEquations($wrapper);

        var partEntries = VersioningCommon.getPartData($wrapper, assessmentData, questionData);

        layoutSections = [];
        for (var i = 0; i < assessmentData.Sections.length; ++i) {
            if (assessmentData.Sections[i].type == 'page_break') continue;

            var sectionData = assessmentData.Sections[i];

            var sectionEntry = {
                id: sectionData.id,
                Content: []
            };

            for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
                if (assessmentData.Sections[i].Content[j].type == 'shuffle_break') continue;
                var itemData = assessmentData.Sections[i].Content[j];

                var questionHandleID;
                if (itemData.type == 'item') {
                    questionHandleID = itemData.question_handle_id;
                } else if (itemData.type == 'item_set') {
                    var renderedIndex = ('rendered_index' in itemData) ? itemData.rendered_index : 0;
                    questionHandleID = itemData.question_handle_ids[renderedIndex];
                }

                for (var k = 0; k < questionData[questionHandleID].json_data.Parts.length; ++k) {
                    var partID = questionData[questionHandleID].json_data.Parts[k].id;
                    for (var p = 0; p < partEntries.length; ++p) {
                        var isMatch = true;
                        if (partEntries[p].Source.question_handle_id != questionHandleID) isMatch = false;
                        else if (partEntries[p].Source.part_id != partID) isMatch = false;

                        if (isMatch) {
                            partEntries[p].id = getNewID(4);
                            sectionEntry.Content.push(partEntries[p]);
                        }
                    }
                }
            }

            layoutSections.push(sectionEntry);
        }

        RenderTools.renderEquations($wrapper);
    }

	var finishAssessmentView = function() {

		var $wrapper = $('#assessment_preview_wrapper');

		// Get layout
		
		if (layoutSections == false) buildLayoutSections($wrapper);

        // Check for negative answers in calculated responses

		$wrapper.find('.hidden_answer_list .question_answer').each(function() {
            if ($(this).text()[0] == '-') {
				$(this).closest('.preview_section_wrapper').attr('data-allow-negative', '');
			}
		});

        // Remove hidden answers

        $wrapper.find('.hidden_answer_list').remove();

		// Save version if needed
		
		if (needsVersion) saveVersion(false);

		// Create entries for WR scoring menu

		$('#written_menu').empty();
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;

			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var questionHandleIDs = [];
				if (assessmentData.Sections[i].Content[j].type == 'item') {
					questionHandleIDs = [assessmentData.Sections[i].Content[j].question_handle_id];
				} else if (assessmentData.Sections[i].Content[j].type == 'item_set') {
					questionHandleIDs = assessmentData.Sections[i].Content[j].question_handle_ids;
				}

				for (var k = 0; k < questionHandleIDs.length; ++k) {
					var questionHandleID = questionHandleIDs[k];
					for (var p = 0; p < questionData[questionHandleID].json_data.Parts.length; ++p) {
						var partData = questionData[questionHandleID].json_data.Parts[p];
						if (partData.type == 'wr') {
							var $templateDiv = $('<div>' + partData.template + '</div>');
							var itemHTML = 
								"<li><div class='menu_item' \
									data-question-handle-id='" + questionHandleID + "' \
									data-part-id='" + partData.id + "'> \
									<div class='menu_text'>" + $templateDiv.text() + "</div> \
								</div></li>";
								
							$('#written_menu').append(itemHTML);	
						}
					}
				}
			}
		}

		// Attach htmleditor to WR fields

		$('#assessment_preview_wrapper .wr_input_field').each(function() {
			$(this).htmleditor({'height': 'auto'});
			$(this).htmleditor('hideElement', 'edit_button_variable');
			$(this).htmleditor('hideElement', 'edit_button_blank');
		});

		// Build left bar

		drawLeftBar(assessmentData, questionData);
	
		// Show everything

		if ($('#written_menu .menu_item').length > 0) {
			$('.results_view_button[data-view-type="written"]').show();
		}
		
		$('#version_shuffling').hide();
		$('#loading_panel').hide();
		$('#preview_panel').show();

		if (sittingData.View.shuffle_attempts) $('#version_controls').hide();
		else $('#version_controls').show();

		$('.wr_input_field').blur();

		$('#assessment_preview_wrapper').css('opacity', '1.0');

		$('#view_menu').menu('enable');
	}

	var updateLabelOptions = function() {
		var oldValue = false;
		if ($('#student_label_select option:selected').length == 1) {
			oldValue = $('#student_label_select').val();
		}

		var detailCounts = {};
		$('.student_label').each(function() {
			var json_string = $(this).attr('data-details');
			var details = (json_string.length == 0) ? {} : JSON.parse(json_string);
			for (var key in details) {
				if (!(key in detailCounts)) detailCounts[key] = 0;
				detailCounts[key]++;
			}
		});

		$('#student_label_select').empty();

		var allDetails = Object.keys(detailCounts);
		if (allDetails.length == 0) {
			$('#label_select_wrapper').hide();
		} else {
			for (var i = 0; i < allDetails.length; ++i) {
				$('#student_label_select').append('<option value="' + allDetails[i] +
					'"' + ((i == 0) ? ' selected' : '') + '>' + allDetails[i] + '</option>');
			}
			$('#label_select_wrapper').show();
		}

		if ((oldValue !== false) && ($('#student_label_select option[value="' + oldValue + '"]').length > 0)) {
			$('#student_label_select').val(oldValue);
		}
	}

	var getLabel = function(details, requestedLabel) {
		if (requestedLabel in details) {
			return details[requestedLabel];
		} else {
			var matchingLabel = null;
			$('#student_label_select option').each(function() {
				var thisLabel = $(this).attr('value');
				if (thisLabel in details) {
					matchingLabel = thisLabel;
					return false;
				}
			});

			if (matchingLabel != null) return details[matchingLabel];
			else return false;
		}
	}

	var updateStudentLabels = function() {
		if ($('#student_label_select').val() != null) {
			var requestedLabel = $('#student_label_select').val();

			$('.student_label').each(function() {
				var json_string = $(this).attr('data-details');
				var details = (json_string.length == 0) ? {} : JSON.parse(json_string);
				var label = getLabel(details, requestedLabel);
				if (label === false) label = '[No details]';
				$(this).text(label);

				if ($(this).is('td') && $(this).parent()[0].hasAttribute('data-sort-values')) {
					var json_string = $(this).parent().attr('data-sort-values');
					var rowSortValues = JSON.parse(json_string);
					var index = $(this).index();
					rowSortValues[index] = label;
					$(this).parent().attr('data-sort-values', JSON.stringify(rowSortValues));
				}
			});
	
			$('.student_label_upper').text(requestedLabel);
			$('.student_label_lower').text(requestedLabel.toLowerCase());
	
			if ($('#student_title')[0].hasAttribute('data-sitting-student-id')) {
				var selectedID = parseInt($('#student_title').attr('data-sitting-student-id'));
				var $button = $('#student_menu .menu_item[data-sitting-student-id="' + selectedID + '"]');
				$('#student_title').text('Attempts overview for ' + $button.find('.menu_text').text());
			} else $('#student_title').text('Attempts overview');

			if ((wr_displayData != null) && (wr_index >= 0) && (wr_index < wr_displayData.length)) {
				var attempt = wr_displayData[wr_index];
				if ('details' in attempt) {
					var label = getLabel(attempt['details'], requestedLabel);
					$('#student_scoring_title').text((label === false) ? 'selected response' : label);
				} else $('#student_scoring_title').text('selected response');
			} else $('#student_scoring_title').text('selected response');
		}
	}

	$('#student_label_select').on('change', function() {
		updateStudentLabels();
	});

	$('.identifier_select').on('change', function() {
		if ($(this).val() == 'Custom') {
			$(this).next().show().focus();
		} else $(this).next().val('').hide();
	});
	
	$('#addStudentEmails').on('click', function() {
		$('#dstu_id').val(sittingID);
		$('#dstu_emails').val('');
		
		$('#addStudentEmails_form').dialog("open");
	});

	$(document).on('click', '.student_row .delete_icon', function() {
		var student_email = $(this).closest('tr').find('.student_email').text();
		var sitting_student_id = $(this).closest('tr').attr('data-sitting-student-id');
		$('#removeStudent_dialog').attr('data-sitting-student-id', sitting_student_id);
		$('#removeStudent_text').text('Permanently remove "' + student_email + '" from this sitting and delete any associated attempts? This action can not be undone.');
		$('#removeStudent_dialog').dialog('open');
	});
	
	var addStudentEmails = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		var student_emails = $('#dstu_emails').val().split(/[,;\"\s]+/);
		$('#dstu_emails').val(student_emails.join("\n"));

        var studentData = [];

		var emailMessage = false;
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		for (var i = 0; i < student_emails.length; ++i) {
			if ((student_emails[i].length > 0) && !re.test(student_emails[i].toLowerCase())) {
				emailMessage = '* Email ' + student_emails[i] + ' is not valid.';
			} else studentData.push({email: student_emails[i]});
		}
		
		if (emailMessage !== false) {

			$('#dstu_emails').validationEngine('showPrompt', emailMessage, 'error', 'bottomLeft', true);

		} else if ($form.validationEngine('validate')) {

	    	if (pendingAJAXPost) return false;
	    	showSubmitProgress($dialog);
			pendingAJAXPost = true;

			ajaxFetch('/Sittings/add_students/' + sittingID, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					studentData: studentData
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
			
				if (responseData.success) {
					accountData = responseData.account_data;
					drawAccess();

					if (responseData.skipped.length > 0) {

						hideSubmitProgress($dialog);
						$('#dstu_emails').val(responseData.skipped.join("\n"));	
						$('#dstu_emails').validationEngine('showPrompt', '* Skipped emails: not a student account', 
							'error', 'bottomLeft', true);

					} else {

                        $.gritter.add({
                            title: "Success",
                            text: "Added students.",
                            image: "/img/success.svg"
                        });
                        $dialog.dialog('close');

                    }
				} else showSubmitError($dialog);
			});
		}
	}

    $('#addStudentEmails_form').dialog({
        autoOpen: false,
        width: 550,
        modal: true,
		minWidth:400,
		minHeight:300,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$('#addStudentEmails_form').dialog("close");
			},
			"Add": addStudentEmails
        }, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Add")').addClass('btn btn-save btn_font');
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

    var classListCallback = function() {
        $('#class_list_wrapper .move_icon').hide();
    }

    $('#addClassList').click(function(e){
		e.preventDefault();

        ClassListCommon.initializeClassLists({
            needsEmail: true,
            classListDidDraw: classListCallback
        });
        $('#addClassList_dialog').dialog('open');
        $('#class_list_shuffle').hide();
        document.activeElement.blur();
	});

    var addClassList = function() {
		var $dialog = $(this);

        if (pendingAJAXPost) return false;
        showSubmitProgress($dialog);
        pendingAJAXPost = true;

        var index = $('#class_list_select').val();
        var classListID = classLists[index].id;

        var studentData = [];
		var validClassList = true;
        $('#class_list_wrapper .list_row').each(function() {
            if ($(this).find('.class_list_checkbox').prop('checked')) {
				var thisData = JSON.parse($(this).attr('data-json-data'));
				if (!('email' in thisData)) validClassList = false;
                else studentData.push(thisData);
            }
        });

		if (validClassList) {

			ajaxFetch('/Sittings/add_students/' + sittingID, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					studentData: studentData,
					classListID: classListID
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
	
				if (responseData.success) {
					accountData = responseData.account_data;
					drawAccess();
	
					if (responseData.skipped.length > 0) {
	
						var message;
						if (responseData.skipped.length > 1) {
							message = "Skipped " + responseData.skipped.length + " emails that are not student accounts."
						} else message = "Skipped 1 email that is not a student account.";
	
						$.gritter.add({
							title: "Error",
							text: message,
							image: "/img/error.svg"
						});
	
					} else {
	
						$.gritter.add({
							title: "Success",
							text: "Added students.",
							image: "/img/success.svg"
						});
	
					}
					
					$dialog.dialog('close');
				} else showSubmitError($dialog);
			});

		} else {

			$.gritter.add({
				title: "Error",
				text: "No email addresses in selected class list",
				image: "/img/error.svg"
			});

			$dialog.dialog('close');

		}
    }

    $('#addClassList_dialog').dialog({
        autoOpen: false,
        width: 500,
        modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$(this).dialog("close");
			},
			"Add": addClassList
        }, create: function() {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
            $buttonPane.find('button:contains("Add")').attr('id', 'class_list_continue');
        }, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Add")').addClass('btn btn-save btn_font');
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

	$('#accounts_table').on('click', '.edit_icon', function(e) {
		var student_email = $(this).closest('tr').find('.student_email').text();
		var sitting_student_id = $(this).closest('tr').attr('data-sitting-student-id');
		$('#editEmail_dialog').attr('data-sitting-student-id', sitting_student_id);
		$('#new_email').val(student_email);

		$('#editEmail_dialog').dialog('open');
	});
	
	var saveEmail = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')) {
			var sittingStudentId = $('#editEmail_dialog').attr('data-sitting-student-id');
			var newEmail = $('#new_email').val().trim();
		
			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
	
			ajaxFetch('/SittingStudents/update_email/' + sittingStudentId, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					new_email: newEmail
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();

				if (responseData.success) {
					for (var i = 0; i < accountData.length; ++i) {
						if (accountData[i].id == responseData.id) {
							accountData[i].bounced = false;
							accountData[i].details.Email = responseData.email;
						}
					}
					drawAccess();
	
					$.gritter.add({
						title: "Success",
						text: "Email updated.",
						image: "/img/success.svg"
					});
					$dialog.dialog('close');
				} else if ('message' in responseData) {
					$('#new_email').validationEngine('showPrompt', responseData.message, 'error', 'bottomLeft', true);
					hideSubmitProgress($dialog);
				} else {
					showSubmitError($dialog);
				}
			});
		}
	}

	$('#editEmail_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': saveEmail
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#accounts_table').on('click', '.email_icon', function(e) {
		var student_email = $(this).closest('tr').find('.student_email').text();
		$('#verification_email').text(student_email);

        var studentUserID = $(this).closest('tr').attr('data-student-user-id');
		$('#sendVerification_dialog').attr('data-student-user-id', studentUserID);

		$('#sendVerification_dialog').dialog('open');
	});

	var sendVerification = function() {
		var $dialog = $(this);

        var studentUserID = $dialog.attr('data-student-user-id');
		
	    if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/Users/resend_verification/' + studentUserID, {
			method: 'POST'
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				$.gritter.add({
					title: "Success",
					text: "Verification email sent.",
					image: "/img/success.svg"
				});
				$dialog.dialog('close');
			} else {
				showSubmitError($dialog);
			}
		});
	}

	$('#sendVerification_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Send': sendVerification
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Send")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	var removeStudent = function() {
		var $dialog = $(this);

		var sittingStudentId = $('#removeStudent_dialog').attr('data-sitting-student-id');

	    if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/Sittings/remove_student/' + sittingStudentId, {
			method: 'POST'
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				var index = 0;
				while (index < accountData.length) {
					if (accountData[index].id == responseData.id) accountData.splice(index, 1);
					else ++index;
				}

				drawAccess();
				
				$.gritter.add({
					title: "Success",
					text: "Student removed from sitting.",
					image: "/img/success.svg"
				});

				$dialog.dialog('close');
			} else showSubmitError($dialog);

		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$('#removeStudent_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Remove': removeStudent
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Remove")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#editLink_button').on('click', function() {
		if ('token_details' in sittingData.Access) {
			rebuildLinkDisplay('dtok', sittingData.Access.token_details);
		} else rebuildLinkDisplay('dtok', {});

		$('#link_code').text(joinToken);

		$('#editLink_form').dialog("open");
	});

	$('#enable_link').on('change', function() {
		if ($(this).prop('checked')) $('#identifier_wrapper').show();
		else $('#identifier_wrapper').hide();
	});
	
	var saveLink = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');
		
		var isValid = true;

		var join_field = false;
		var join_values = [];

		if ($('#enable_link').prop('checked')) {
			if ($('#dtok_identifier').val() == 'Custom') {
				join_field = $('#dtok_custom').val();
			} else join_field = $('#dtok_identifier').val();

			if ($('#dtok_identifier_list').val().length > 0) {
				var raw_values = $('#dtok_identifier_list').val().split(/[\r\n]+/);
				for (var i = 0; i < raw_values.length; ++i) {
					var thisValue = raw_values[i].trim();
					if (thisValue.length > 0) join_values.push(thisValue);
				}

				if ($('#dtok_identifier').val() == 'Custom') {
					if ($('#dtok_identifier option[value="' + join_field + '"]').length > 0) {
						$('#dtok_custom').validationEngine('showPrompt', '* Value must not be one of the choices above', 
							'error', 'bottomLeft', true);
						isValid = false;
					}
				}
		
				var uniqueValues = true;
				for (var i = 1; i < join_values.length; ++i) {
					for (var j = 0; j < i; ++j) {
						if (join_values[i] == join_values[j]) uniqueValues = false;
					}
				}
				if (!uniqueValues) {
					$('#dtok_identifier_list').validationEngine('showPrompt', '* Values must be unique', 
						'error', 'bottomLeft', true);
					isValid = false;
				}
			} else {
				$('#dtok_identifier_list').validationEngine('showPrompt', '* Must specify one or more allowed values', 
					'error', 'bottomLeft', true);
				isValid = false;
			}
		}

		if (isValid && $form.validationEngine('validate')) {

			if (join_field !== false) {
				sittingData.Access.token_details = {
					fieldName: join_field,
					fieldValues: join_values
				};	
			} else delete sittingData.Access.token_details;
			
			// Save new settings

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			var data_string = JSON.stringify(sittingData);

			var postData = {
				data_string: data_string,
				data_hash: SparkMD5.hash(data_string)
			};

			ajaxFetch('/Sittings/save/' + sittingID, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(postData)
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
			
				if (responseData.success) {
					drawAccess();

					$.gritter.add({
						title: "Success",
						text: "Shareable link saved.",
						image: "/img/success.svg"
					});
	
					$dialog.dialog('close');
				} else showSubmitError($dialog);
			});
		}
	}

    $('#editLink_form').dialog({
        autoOpen: false,
        width: 550,
        modal: true,
		minWidth:400,
		minHeight:300,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$('#editLink_form').dialog("close");
			},
			"Save": saveLink
        }, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
	});
	
	var loadStudentOverview = function() {
		var sittingStudentID = $('#student_title').attr('data-sitting-student-id');
		ajaxFetch('/SittingStudents/get_overview/' + sittingStudentID)
		.then(async response => {
			const responseData = await response.json();

			$('#student_title').attr('data-details', JSON.stringify(responseData.details));

			$('#loading_student_data').hide();
			hideSubmitProgress($('#student_reload_wrapper'));

			if (responseData.success) {
				var $button = $('#student_menu .menu_item[data-sitting-student-id="' + sittingStudentID + '"]');
				$('#student_title').text('Attempts overview for ' + $button.find('.menu_text').text());

				if (('attempts' in responseData) && (responseData.attempts.length > 0)) {
					var tableHTML = "";
					for (var i = 0; i < responseData.attempts.length; ++i) {
						
						var duration;
						if ('duration' in responseData.attempts[i]) {
							var minutes = Math.round(responseData.attempts[i].duration / 60.0);
							duration = minutes.toFixed(0) + " minute" + ((minutes == 1) ? '' : 's');
						} else duration = "<i>In progress</i>";

						var created = dayjs.utc(responseData.attempts[i].created).local();
						var formatted = created.format('MMMM D, YYYY') + ' at ' + created.format('h:mm A');
						tableHTML += "<tr data-response-online-id='" + responseData.attempts[i].id + "'> \
							<td><div class='attempt_number'>" + (i + 1) + "</div></td> \
							<td>" + formatted + "</td> \
							<td>" + duration + "</td>";

						var scoringData = responseData.attempts[i].layout;
						var resultData = responseData.attempts[i].results;
						var scores = ScoringCommon.scoreResults(scoringData, resultData);
						if (scores !== false) {

							// Adjust scoring to build in weights (TEMPORARY)

							var outOf = 0.0;
							for (let question of scores.questions) {
								question.score *= question.weight;
								question.maxValue *= question.weight;
								if (!question?.bonus && !question?.omitted) {
									outOf += question.maxValue;
								}
							}
							scores.outOf = outOf;
						
							var totalScore = 0.0;
							for (var j = 0; j < scores.questions.length; ++j) {
								if (!('omitted' in scores.questions[j])) totalScore += scores.questions[j].score;
							}
							var totalOutOf = scores.outOf;
							var percentage = (100.0 * totalScore / totalOutOf).toFixed(0);

							tableHTML += "<td class='td_score'><div style='margin-bottom:5px;'>" + percentage + "%</div>";
							if (!('weighted' in scores) || (scores['weighted'] == 0)) {
								tableHTML += "<div>(" + totalScore + "/" + totalOutOf + ")</div>";
							}
							tableHTML += "</td>";
							
						} else {

							tableHTML += "<td class='td_score'><i>No scores available</i></td>";

						}

						tableHTML += "<td>" +
							"<button title='Delete' class='actions_button delete_icon' style='margin-right:3px;'>&nbsp;</button>" +
							"<button title='View' class='actions_button view_icon' style='margin-right:3px;'>&nbsp;</button>" +
							"</td>";

						tableHTML += "</tr>";
					}
					$('#attempts_rows').html(tableHTML);
					$('#attempt_overview_wrapper').show();

					if ($('#attempts_rows .view_icon').length > 0) {
						$('#attempts_rows .view_icon').last().trigger('click');
					} else {
						$('#empty_details').show();
					}
				} else {
					$('#empty_student_data').show();
				}
			} else {
				$.gritter.add({
					title: "Error",
					text: "Unable to load student results.",
					image: "/img/error.svg"
				});					
			}
		});

        ScrollingCommon.resetLeftPanel();
	}

	$('#student_reload').on('click', function() {
		showSubmitProgress($('#student_reload_wrapper'));
		loadStudentOverview();
	});

	$('#student_menu').on('click', '.menu_item', function(e) {
		e.preventDefault();
		
		$('#student_menu .menu_item').removeClass('active');
		$(this).addClass('active');

		var sittingStudentID = $(this).attr('data-sitting-student-id');
		$('#student_title').attr('data-sitting-student-id', sittingStudentID);

		$('.results_status').hide();
		$('#loading_student_data').show();

		$('#attempt_overview_wrapper').hide();

		$('#student_details_window').hide();

		loadStudentOverview();	
    });

	var deleteAttempt = function() {
		var $dialog = $(this);

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var sittingStudentID = $('#student_title').attr('data-sitting-student-id');
		var attemptID = $('#deleteAttempt_dialog').attr('data-response-online-id');

		ajaxFetch('/SittingStudents/delete_attempt/' + sittingStudentID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				response_online_id: attemptID
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				$('#attempts_rows tr[data-response-online-id="' + responseData.response_online_id + '"]').remove();

				if ($('#attempts_rows tr').length > 0) $('#attempts_rows .view_icon').last().trigger('click');
				else $('.results_view_button[data-view-type="summary"]').trigger('click');

				$.gritter.add({
					title: "Success",
					text: "Attempt deleted.",
					image: "/img/success.svg"
				});

				$dialog.dialog('close');
			} else {
				showSubmitError($dialog);
			}

		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$('#attempts_rows').on('click', '.delete_icon', function() {
		var $row = $(this).closest('tr');

		$('#deleteAttempt_dialog').attr('data-response-online-id', $row.attr('data-response-online-id'));
		$('#delete_attempt_number').text($row.find('.attempt_number').text());

		$('#deleteAttempt_dialog').dialog('open');
	});

	$('#deleteAttempt_dialog').dialog({
        autoOpen: false,
		width: 380,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteAttempt
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#attempts_rows').on('click', '.view_icon', function() {
		$('#attempts_rows .view_icon').removeAttr('disabled');
		$(this).attr('disabled', 'disabled');

		var $row = $(this).closest('tr');

		var newTitle = 'Attempt ' + $row.find('.attempt_number').text();
		$('.student_attempt_title').text(newTitle);

		var attemptID = $row.attr('data-response-online-id');
		var sittingStudentID = $('#student_title').attr('data-sitting-student-id');

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		$('#student_details_window').attr('data-response-online-id', attemptID);

		const queryString = new URLSearchParams({
			response_online_id: attemptID
		}).toString();

		ajaxFetch(`/SittingStudents/get_attempt/${sittingStudentID}?${queryString}`)
		.then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {

				SittingsCommon.scoreAttempt(responseData);

	            if ('Version' in responseData) {
					await SittingsCommon.previewFromHTML($('#student_assessment_inner'), responseData.Version.html, responseData.Version.settings, sittingData.View);
					if ($('#student_title')[0].hasAttribute('data-details')) {
						var nameLinesString = $('#student_title').attr('data-details');
						var nameLinesData = (nameLinesString.length == 0) ? {} : JSON.parse(nameLinesString);
						SittingsCommon.addNameLines($('#student_assessment_inner'), nameLinesData);
					}
					if ('responses' in responseData) {
						var result = SittingsCommon.fillResponses($('#student_assessment_inner'), responseData.responses);
						if (!result) {
							$.gritter.add({
								title: "Error",
								text: "Unable to read responses.",
								image: "/img/error.svg"
							});					
						}
					}

					$('#student_assessment_inner .online_response').attr('disabled', 'disabled');
						
					if ('scores' in responseData) {
						var isWeighted = false;
						if (!('weighted' in responseData.scores)) isWeighted = false;
						else if (responseData.scores['weighted'] == 0) isWeighted = false;
						else isWeighted = true;
	
						SittingsCommon.markAssessment($('#student_assessment_inner'), responseData.scores.questions, isWeighted);
					}

					await RenderTools.renderEquations($('#student_assessment_inner'));

					if ('annotations' in responseData) {
						const annotations = responseData.annotations;
						$('#student_assessment_inner .question_wrapper').each(function() {
							const contentID = $(this).attr('data-content-id');
							if (contentID in annotations) {
								SittingsCommon.drawAnnotations($(this), annotations[contentID]);
							}
						});
					}
	
					$('#student_assessment_wrapper').attr('data-has-content', 1);
	            } else $('#student_assessment_wrapper').attr('data-has-content', 0);

				if ('key_sections' in responseData) {
					$('#student_key_wrapper').html(SittingsCommon.getKeyHTML(responseData.key_sections));
					RenderTools.renderEquations($('#student_key_wrapper'));
					$('#student_key_wrapper').attr('data-has-content', 1);
				} else $('#student_key_wrapper').attr('data-has-content', 0);

				if (('scores' in responseData) && ('outcomes' in responseData) && (responseData.outcomes.length > 0)) {
					SittingsCommon.buildOutcomesTable($('#teacherOutcomes'), responseData.outcomes);
					$('#outcomes_header_text').text(newTitle);
					$('#student_outcomes_wrapper').attr('data-has-content', 1);
				} else $('#student_outcomes_wrapper').attr('data-has-content', 0);

				if (('events' in responseData) && (responseData.events.length > 0)) {
					studentEvents = responseData.events;

                    for (var i = 0; i < studentEvents.length; ++i) {
                        if (('clientTime' in studentEvents[i]) && ('serverTime' in studentEvents[i])) {
                            studentEvents[i].time = studentEvents[i].serverTime;
                            delete studentEvents[i].clientTime;
                            delete studentEvents[i].serverTime;
                        }
                    }	

					studentEvents.sort(function(a, b) {
						if (a.time < b.time) return -1;
						else if (a.time > b.time) return 1;
						else return 0;
					});

					if ($('#student_events_wrapper').is(':visible')) rebuildEventsPlot();

					var niceNames = {
						'create': 'Attempt created',
						'copy': 'Copied content',
						'paste': 'Pasted content',
						'closed': 'Attempt closed'
					};

					var html = '';
					for (var i = 0; i < studentEvents.length; ++i) {
						var eventName = false;
						if (studentEvents[i].eventName == 'enter') {
							if (('deviceID' in studentEvents[i]) && (studentEvents[i].deviceID >= 0)) {
								var deviceID = studentEvents[i].deviceID + 1;
								eventName = 'Device ' + deviceID + ' login';
							} else eventName = 'New login';
						} else if (studentEvents[i].eventName in niceNames) {
							eventName = niceNames[studentEvents[i].eventName];
						}

						if (eventName !== false) {
							var date = dayjs.utc(studentEvents[i].time).local();
							html += '<tr><td>' + date.format('MMMM D, YYYY h:mm:ss a') + '</td>' +
								'<td>' + eventName + '</td></tr>';	
						}
					}
					if (html.length > 0) {
						$('#events_rows').html(html);
						$('#student_events_other').show();
					} else $('#student_events_other').hide();

					$('#student_events_wrapper').attr('data-has-content', 1);
				} else $('#student_events_wrapper').attr('data-has-content', 0);

				var hasOutcomes = parseInt($('#student_outcomes_wrapper').attr('data-has-content'));
				var hasAssessment = parseInt($('#student_assessment_wrapper').attr('data-has-content'));

				var fallback;
				if (hasOutcomes) fallback = 'outcomes';
				else if (hasAssessment) fallback = 'assessment';
				else fallback = 'events';
				
				$('#student_details_window').show();
				if (($('#student_data_select').val() == 'outcomes') && !hasOutcomes) {
					$('#student_data_select').val(fallback).trigger('change');
				} else if (($('#student_data_select').val() == 'assessment') && !hasAssessment) {
					$('#student_data_select').val(fallback).trigger('change');
				} else $('#student_data_select').trigger('change');
			}
		});
	});
	
	$('#student_assessment_print').on('click', function() {
		navigateWithPost('/build_pdf', {
			pdfType: 'pdf_from_url',
			url: '/Sittings/print_questions/' + sittingID,
			postDataJSON: JSON.stringify({ 
				html: $('#student_assessment_inner').html(),
				suppressInputs: false 
			}),
			target: 'attempt.pdf'
		});
	});
	
	$("#student_events_graph").bind("plotselected", function (event, ranges) {
		$.each(eventsPlot.getXAxes(), function(_, axis) {
			var opts = axis.options;
			opts.min = ranges.xaxis.from;
			opts.max = ranges.xaxis.to;
		});
		eventsPlot.setupGrid();
		eventsPlot.draw();
		eventsPlot.clearSelection();

		$('#student_events_reset').show();
	});

	$('#student_events_reset').on('click', function() {
		rebuildEventsPlot();
	});
	
	var rebuildEventsPlot = function() {

		var tabColor = "rgba(0, 0, 0, 0.0625)";

		var deviceIDs = [];
		var lines = {};
		var points = {};

		var tabMarkings = [];
		var eventMarkings = [];
		var blurStarts = {};

		var lastTime = null;
		var lastIndex = -1;

		var attemptStart = null;
		var attemptEnd = null;
		var attemptClosed = null;

		for (var i = 0; i < studentEvents.length; ++i) {
			var msTime = studentEvents[i].time;
			if ((attemptEnd == null) || (msTime > attemptEnd)) attemptEnd = msTime;

			var deviceID;
			if ('deviceID' in studentEvents[i]) {
				deviceID = studentEvents[i].deviceID;
			} else deviceID = -1;

			if (deviceIDs.indexOf(deviceID) == -1) {
				deviceIDs.push(deviceID);
				lines[deviceID] = [];
				points[deviceID] = [];
			}

			if (studentEvents[i].eventName == 'start') {
				attemptStart = msTime;
				lastTime = attemptStart;
			} else if (studentEvents[i].eventName == 'select') {
				if (lastIndex == -1) lastIndex = studentEvents[i].index;
				if (lastTime != null) {
					lines[deviceID] = lines[deviceID].concat([null, [lastTime, lastIndex + 1]]);
				}

				var dataPoint = [msTime, lastIndex + 1];
				lines[deviceID].push(dataPoint);

				lastTime = msTime;
				lastIndex = studentEvents[i].index;
			} else if (studentEvents[i].eventName == 'response') {
				if (lastTime != null) {
					lines[deviceID] = lines[deviceID].concat([null, [lastTime, studentEvents[i].index + 1]]);
				}

				var dataPoint = [msTime, studentEvents[i].index + 1];
				lines[deviceID].push(dataPoint);

				if ('response' in studentEvents[i]) dataPoint.push(studentEvents[i].response);
				points[deviceID].push(dataPoint);

				lastTime = msTime;
				lastIndex = studentEvents[i].index;
			} else if (studentEvents[i].eventName == 'blur') {
				if (!(deviceID in blurStarts)) blurStarts[deviceID] = msTime;
			} else if (studentEvents[i].eventName == 'focus') {
				if ((attemptClosed == null) && (deviceID in blurStarts)) {
					tabMarkings.push({
						xaxis: {
							from: blurStarts[deviceID],
							to: msTime
						}, color: tabColor
					});
					delete blurStarts[deviceID];	
				}
			} else if (studentEvents[i].eventName == 'closed') {
				attemptClosed = msTime;
			}
		}

		if ((attemptClosed != null) && (attemptClosed != attemptEnd)) {
			eventMarkings.push({
				xaxis: {
					from: attemptClosed,
					to: attemptClosed
				}, color: "#888"
			});
		} else attemptClosed = attemptEnd;

		for (var deviceID in blurStarts) {
			if (blurStarts.hasOwnProperty(deviceID)) {
				tabMarkings.push({
					xaxis: {
						from: blurStarts[deviceID],
						to: attemptClosed
					}, color: tabColor
				});
			}
		}

		eventMarkings = tabMarkings.concat(eventMarkings);

		var i = 0;
		while (i < deviceIDs.length) {
			var deviceID = deviceIDs[i];
			if ((lines[deviceID].length == 0) && (points[deviceID].length == 0)) {
				delete lines[deviceID];
				delete points[deviceID];
				deviceIDs.splice(i, 1);
			} else ++i;
		}

		questionNumbers = [];
		$('#student_assessment_wrapper .part_wrapper').each(function() {
			var thisNumber = $(this).find('.question_number').html().slice(0, -1);
			if ((assessmentData.Settings.numberingType == 'Alpha') && $(this).is('.context_question')) {
				var $contextWrapper = $(this).prevAll('.context_wrapper:first');
				if ($contextWrapper.length > 0) {
					thisNumber = $contextWrapper.find('.question_number').html().slice(0, -1) + thisNumber;
				}
			} else if (assessmentData.Settings.numberingType == 'Type') {
				thisNumber = $(this).attr('data-type').substr(0, 2).toUpperCase() + thisNumber;
			}
			questionNumbers.push([questionNumbers.length + 1, thisNumber]);
		});

		var stride = Math.ceil(questionNumbers.length / 10);
		var ticks = [];
		for (var i = 0; i < questionNumbers.length; i += stride) {
			ticks.push(questionNumbers[i]);
		}
		
		var options = {
			xaxis: {
				mode: "time",
				tickLength: 0,
				minTickSize: [1, "minute"],
				tickFormatter: function (val, axis) {
					var date = dayjs.utc(val).local();
					return date.format('h:mm');
				}
			},
			yaxis: {
				position: "left",
				axisLabel: "bar",
				min: 0,
				max: questionNumbers.length + 1,
				ticks: ticks
			},
			selection: {
				mode: "x"
			},
			grid: {
				markings: eventMarkings,
				hoverable: true
			}
		};		

		if (attemptStart != null) options.xaxis.min = attemptStart;
		if (attemptEnd != null) options.xaxis.max = attemptEnd;

		var colours = ["#deac21", "#0b84a5", "#9dd866", "#6f4e7c", "#ffa056", "#ca472f", "#8dddd0"];

		deviceIDs.sort();

		var plots = [];
		for (var i = 0; i < deviceIDs.length; ++i) {
			var colour = (i < colours.length) ? colours[i] : "#555";
			var deviceID = deviceIDs[i];
			if (lines[deviceID].length > 0) {
				plots.push({
					data: lines[deviceID],
					lines: { show: true },
					color: colour,
					hoverable: false,
					shadowSize: 0	
				});
			}
		}
		for (var i = 0; i < deviceIDs.length; ++i) {
			var colour = (i < colours.length) ? colours[i] : "#555";
			var deviceID = deviceIDs[i];
			if (points[deviceID].length > 0) {
				plots.push({
					data: points[deviceID],
					points: { 
						show: true,
						fill: true,
						fillColor: colour
					},
					color: colour,
					shadowSize: 0	
				});
			}
		}
		if (plots.length == 0) {

			// Make sure plots aren't entirely empty -- otherwise tick labels won't show

			plots.push({data: []});			
		}

		eventsPlot = $.plot("#student_events_graph", plots, options);

		var xMax = eventsPlot.getOptions().xaxis.max;
		var yMin = eventsPlot.getOptions().yaxis.min;
		var yMax = eventsPlot.getOptions().yaxis.max;

		var legendPosition = eventsPlot.pointOffset({ x: xMax, y: yMin});
		$('#student_events_legend').css({
			right: ($('#student_events_graph').width() - legendPosition.left + 3) + 'px',
			bottom: ($('#student_events_graph').height() - legendPosition.top + 3) + 'px'
		});

		var resetPosition = eventsPlot.pointOffset({ x: xMax, y: yMax});
		$('#student_events_reset').css({
			right: ($('#student_events_graph').width() - resetPosition.left + 3) + 'px',
			top: (resetPosition.top + 3) + 'px'
		}).hide();

		var legendHTML = "";
		if (tabMarkings.length > 0) {
			legendHTML += '<div class="legend_block" style="border: 1px solid #888; background:' + tabColor + '"></div>';
			legendHTML += '<div class="legend_text">Student off tab</div>';
		}
		if (deviceIDs.length > 1) {
			for (var i = 0; i < deviceIDs.length; ++i) {
				var colour = (i < colours.length) ? colours[i] : "#555";
				legendHTML += '<svg class="legend_block" style="width:100%; height:100%;" viewBox="0 0 1 1"> \
					<path stroke-width="0.15" fill="none" stroke="' + colour + '" d="M 0 0.5 l 1 0" /> \
					<circle fill="' + colour + '" stroke="none" cx="0.5" cy="0.5" r="0.25" /></svg>';
				if (deviceIDs[i] >= 0) legendHTML += '<div class="legend_text">Device ' + (deviceIDs[i] + 1) + '</div>';
				else legendHTML += '<div class="legend_text">Unknown device</div>';
			}
		}

		$('#student_events_legend').html(legendHTML);			
		if (legendHTML.length > 0) $('#student_events_legend').show();
		else $('#student_events_legend').hide();
	
		$("#student_events_graph").bind("plothover", function (event, pos, item) {
			if (item && pos.x && pos.y) {
				var html = "<b>Question " + questionNumbers[item.series.data[item.dataIndex][1] - 1][1] + "</b><br/>";

                html += "New response: ";
				if (item.series.data[item.dataIndex].length < 3) html += "(WR)";
				else html += item.series.data[item.dataIndex][2];
				html += "<br/>";

                var date = new dayjs(item.series.data[item.dataIndex][0]);
				html += "Date: " + date.format('MMMM D, YYYY') + "<br/>";
				html += "Time: " + date.format('h:mm:ss a') + "<br/>";

                $("#tooltip").html(html);
				$("#tooltip").css({top: item.pageY - 15, left: item.pageX + 10}).fadeIn(200);
			} else $("#tooltip").hide();
		});
	}

	$("<div id='tooltip' class='graph-tooltip'></div>").appendTo("body");

	$('#student_events_legend').on('click', function() {
		$('#student_events_legend').hide();
	});

    $('#student_data_select').on('change', function() {
		$('.student_data_wrapper').hide();

		var selected = $(this).val();
		if (parseInt($('#student_' + selected + '_wrapper').attr('data-has-content'))) {
			$('#student_' + selected + '_wrapper').show();
			if (selected == 'events') rebuildEventsPlot();
			$('#empty_details').hide();
		} else {
			$('#student_' + selected + '_wrapper').hide();
			$('#empty_details').show();
		}
    });
    
    $('#start_type').on('change', function() {
	    updateStartDetails();
	    
	    var type = $('#start_type').val();
	    if (type == 'code') {
			var characters = "abcdefghjkmnpqrstuvwxyz23456789";
			
			var hasLetters, hasNumbers;
			do {
				var code = "";
				hasLetters = false;
				hasNumbers = false;
				for (var i = 0; i < 5; ++i) {
					var c = characters.charAt(Math.floor(Math.random() * characters.length));
					if ((c >= '0') && (c <= '9')) hasNumbers = true;
					else hasLetters = true;
					code = code + c;
				}
			} while (!hasLetters || !hasNumbers);
			
			$('#start_code').val(code.toUpperCase());
	    }
    });
    
    $('#start_code').on('input', function() {
	    $(this).val($(this).val().toUpperCase());
    });
      
    var updateStartDetails = function() {
	    var type = $('#start_type').val();
	    $('.start_details').hide();
	    $('#start_' + type + '_details').show();

		resizeSettingsWrappers();
    };

    $('#end_type').on('change', function() {
	    updateEndDetails();
    });

    var updateEndDetails = function() {
	    var type = $('#end_type').val();
	    $('.end_details').hide();
	    $('#end_' + type + '_details').show();

		resizeSettingsWrappers();
	};

    $('#duration_type').on('change', function() {
	    if ($('#duration_type').val() == 'defined') {
		    $('#duration_number').val(0);
		    $('#warnings_json').val('[]');
	    }
	    updateDurationDetails();
    });

    var updateDurationDetails = function() {
	    var type = $('#duration_type').val();
	    if (type == 'defined') {
			rebuildWarningLines();

		    $('#duration_details').show();
			$('#warnings_wrapper').show();
	    } else {
		    $('#duration_details').hide();
			$('#warnings_wrapper').hide();
		}

		resizeSettingsWrappers();
	};

    $('#attempts_type').on('change', function() {
	    if ($('#attempts_type').val() == 'defined') {
		    $('#attempts_number').val(1);
	    }
	    updateAttemptsDetails();
    });
    
    $("#attempts_number").on('input', function() {
	    updateAttemptsDetails();
    });

    var updateAttemptsDetails = function() {
	    var type = $('#attempts_type').val();
	    if (type == 'defined') $('#attempts_details').show();
	    else $('#attempts_details').hide();

	    if ($('#attempts_number').val() == 1) $('#attempts_label').text('attempt');
	    else $('#attempts_label').text('attempts');

		resizeSettingsWrappers();
	};

	$(document).on('mouseenter', '#assessment_preview_wrapper .part_wrapper', function(e) {
		$(this).addClass('selected');
		if ($('#question_menu_template li').length > 0) {
			$(this).append($('#question_menu_template').html());
			$(this).find('.item_menu_button').dropdown();

			var hasReasoning = false;
			if (sittingData.View.reasoning.location == 'all') {
				$(this).find('.item_menu_reasoning').hide();
				hasReasoning = true;
			} else {
				$(this).find('.item_menu_reasoning').show();

				var contentID = $(this).closest('.question_wrapper').attr('data-content-id');
                var partIndex = parseInt($(this).attr('data-part-index'));

				for (var i = 0; i < sittingData.View.reasoning.parts.length; ++i) {
					var thisPart = sittingData.View.reasoning.parts[i];
					if ((thisPart.content_id == contentID) && (thisPart.part_index == partIndex)) {
						hasReasoning = true;
					}
				}
			}

			if (hasReasoning) $(this).find('.item_menu_reasoning').html('Remove reasoning');
			else $(this).find('.item_menu_reasoning').html('Add reasoning');
		}
	});
	
	$(document).on('mouseleave', '#assessment_preview_wrapper .part_wrapper', function(e) {
		if ($('.item_menu.open').length == 0)  {
			$(this).removeClass('selected');
			$(this).find('.item_menu_wrapper').remove();
		}
	});
	
	$(document).on('mouseenter', '#student_assessment_wrapper .part_wrapper', function(e) {
		$(this).addClass('selected');
		if ($('#attempt_menu_template li').length > 0) {
			$(this).append($('#attempt_menu_template').html());
			$(this).find('.item_menu_button').dropdown();

			if ($(this).find('.wr_comment_wrapper').length == 0) {
				$(this).find('.item_menu_edit_comment').html('Add comment');
				$(this).find('.item_menu_delete_comment').hide();
			} else {
				$(this).find('.item_menu_edit_comment').html('Edit comment');
				$(this).find('.item_menu_delete_comment').show();
			};
		}
	});
	
	$(document).on('mouseleave', '#student_assessment_wrapper .part_wrapper', function(e) {
		if ($('.item_menu.open').length == 0)  {
			$('.part_wrapper.selected').removeClass('selected');
			$(this).find('.item_menu_wrapper').remove();
		}
	});
	
	$(document).on('change', '.student_status', function(e) {
		var sittingStudentID = parseInt($(this).closest('tr').attr('data-sitting-student-id'));
		if ($(this).val() == 'open') {
			$('#openAttempt_dialog').attr('data-sitting-student-id', sittingStudentID);
			$('#openAttempt_dialog').dialog('open');
		} else if (($(this).val() == 'closed') && !pendingAJAXPost) {
			$(this).next('.wait_icon').show();
			pendingAJAXPost = true;

			ajaxFetch('/SittingStudents/close/' + sittingStudentID, {
				method: 'POST'
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
			
				var $row = $('#summary_rows tr[data-sitting-student-id="' + responseData.sitting_student_id + '"]');
				$row.find('.wait_icon').hide();
	
				if (responseData.success) {
					$.gritter.add({
						title: "Success",
						text: "Attempt closed.",
						image: "/img/success.svg"
					});
				} else $row.find('.student_status').val('open');
				
			});
		}
	});
	
	var buildSummary = function() {
		ajaxFetch('/Sittings/get_results/' + sittingID)
		.then(async response => {
			const responseData = await response.json();

			if (responseData.success) {
				if (responseData.Sitting.status == 'open') {
					$('#sitting_status').val('open');
					$('#sitting_closed').hide();
					$('#sitting_open').show();
				} else {
					$('#sitting_status').val('closed');
					$('#sitting_open').hide();
					$('#sitting_closed').show();
				}

				$('#loading_summary').hide();
				$('#sitting_controls_wrapper').show();

				hideSubmitProgress($('#sitting_reload_wrapper'));

				// Build summary table

				if (responseData.SittingStudent.length > 0) {
					var tableHTML = "";
					var rowSortValues = [];
					for (var i = 0; i < responseData.SittingStudent.length; ++i) {
						var rowData = responseData.SittingStudent[i];
						
						var sortValues = [];

						tableHTML += "<tr style='vertical-align:middle;' data-sitting-student-id='" + rowData.id + "'>";
						tableHTML += "<td class='student_label' data-details='" + htmlSingleQuotes(rowData.details) + "'></td>";
						sortValues.push('');

						var numResponses = rowData.ResponseOnline.length;
						if (numResponses == 0) {
							tableHTML += "<td colspan='4' style='text-align:left;'><i>No attempts made</i></td>";
							sortValues = sortValues.concat([0, Date.now(), 0]);
						} else {
							tableHTML += "<td class='td_count' data-export='" + numResponses + "'><div>" + numResponses + "</div></td>";
							sortValues.push(numResponses);

							var created = dayjs.utc(rowData.ResponseOnline[numResponses - 1].created).local();
							var created_simple = created.format('YYYY/MM/DD HH:mm:ss');
							var created_string = created.format('MMMM D, YYYY') + ' at ' + created.format('h:mm A');
	
							tableHTML += "<td class='td_latest' data-export='" + created_simple + "'>";
							tableHTML += "<div>" + created_string + "</div>"
							sortValues.push(created.unix());

							var status = rowData.ResponseOnline[numResponses - 1].status;
							tableHTML += "<div style='vertical-align:middle; margin-top:5px;'><div style='display:inline-block;'>Status:</div><select class='student_status' style='width:80px; height:25px; font-size:13px; font-family:Arial, Helvetica, sans-serif; margin:0px 5px;'><option value='open'" + ((status == 'open') ? ' selected' : '') + ">Open</option><option value='closed'" + ((status == 'closed') ? ' selected' : '') + ">Closed</option></select><div class='wait_icon' style='display:none;'></div></div>"

							tableHTML += "</td>";
							
							var hasScores = false;
							var bestPercentage = 0.0;
							var bestTotalScore = 0.0;
							var bestOutcomes = [];
							var bestWeighted;
							var bestTotalOutOf;
							
							for (var j = 0; j < rowData.ResponseOnline.length; ++j) {
								var scoringData = JSON.parse(rowData.ResponseOnline[j].layout);
								var resultData = JSON.parse(rowData.ResponseOnline[j].results);
								var scores = ScoringCommon.scoreResults(scoringData, resultData);
								if (scores === false) continue;

								// Adjust scoring to build in weights (TEMPORARY)

								var outOf = 0.0;
								for (let question of scores.questions) {
									question.score *= question.weight;
									question.maxValue *= question.weight;
									if (!question?.bonus && !question?.omitted) {
										outOf += question.maxValue;
									}
								}
								scores.outOf = outOf;

								hasScores = true;

								var layout = JSON.parse(rowData.ResponseOnline[j].layout);

								var totalScore = 0.0;
								for (var k = 0; k < scores.questions.length; ++k) {
									if (!('omitted' in scores.questions[k])) totalScore += scores.questions[k].score;
								}
								var totalOutOf = scores.outOf;

								var percentage = 100.0 * totalScore / totalOutOf;
								if (percentage >= bestPercentage) {
									bestPercentage = percentage;
									bestTotalScore = totalScore;
									bestTotalOutOf = totalOutOf;

									bestOutcomes = [];
									for (var p = 0; p < layout.Outcomes.length; ++p) {
										var outcomeScore = 0.0;
										var outcomeOutOf = 0.0;

										var thisEntry = { name: layout.Outcomes[p].name };
										for (var q = 0; q < layout.Outcomes[p].item_ids.length; ++q) {
											var itemID = layout.Outcomes[p].item_ids[q];

											for (var k = 0; k < scores.questions.length; ++k) {
												if (scores.questions[k].item_id != itemID) continue;
												else if ('omitted' in scores.questions[k]) continue;

												outcomeScore += scores.questions[k].score;
												outcomeOutOf += scores.questions[k].maxValue;
											}
										}
										
										if (outcomeOutOf == 0) thisEntry.percentage = false;
										else thisEntry.percentage = 100.0 * outcomeScore / outcomeOutOf;

										bestOutcomes.push(thisEntry);
									}

									if (!('weighted' in scores)) bestWeighted = false;
									else if (scores['weighted'] == 0) bestWeighted = false;
									else bestWeighted = true;
								}
							}

							if (hasScores) {
								var export_data = { percent: bestPercentage.toFixed(1) };
								if (!bestWeighted) {
									export_data.score = bestTotalScore;
									export_data.outOf = bestTotalOutOf;
								}
								if (bestOutcomes.length > 0) {
									export_data.outcomes = bestOutcomes;
								}

								tableHTML += "<td class='td_score' data-export='" + htmlSingleQuotes(JSON.stringify(export_data)) + "'>" + 
									"<div style='margin-bottom:5px;'>" + bestPercentage.toFixed(0) + "%</div>";
								if (!bestWeighted) {
									tableHTML += "<div>(" + bestTotalScore + "/" + bestTotalOutOf + ")</div>";
								}
								tableHTML += "</td>";
								sortValues.push(bestPercentage);
							} else {
								tableHTML += "<td class='td_score'><i>No scores available</i></td>";
								sortValues.push(0);
							}

							tableHTML += "<td>" +
								"<button title='View' class='actions_button view_icon' style='margin-right:3px;'>&nbsp;</button>" +
								"</td>";
						}

						tableHTML += "</tr>";
						rowSortValues.push(sortValues);
					}
					$('#summary_rows').html(tableHTML);

					for (var i = 0; i < rowSortValues.length; ++i) {
						$('#summary_rows tr').eq(i).attr('data-sort-values', JSON.stringify(rowSortValues[i]));
					}

					$('#empty_summary').hide();
					$('#summary_wrapper').show();
				} else {
					$('#empty_summary').show();
					$('#summary_wrapper').hide();
				}
				$('#results_wrapper').show();

				// Build student list in left bar

				if (responseData.SittingStudent.length > 0) {
					var leftBarItems = "";
					for (var i = 0; i < responseData.SittingStudent.length; ++i) {
						var details_json = responseData.SittingStudent[i].details;
						leftBarItems += "<li><div class='menu_item' data-sitting-student-id='" + responseData.SittingStudent[i].id + "'> \
							<div class='menu_text student_label' data-details='" + htmlSingleQuotes(details_json) + "'></div></div></li>";
					}

					$('#students_wrapper .empty_layout').hide();
					$('#student_menu').html(leftBarItems);
				} else {
					$('#students_wrapper .empty_layout').show();
					$('#student_menu').html('');
				}

				// Update student labels and label options
				
				updateLabelOptions();
				updateStudentLabels();

				// Sort table

				sortSittingSummary();
			}

		});

        ScrollingCommon.resetLeftPanel();
	};

	$('#summary_rows').on('click', '.view_icon', function() {
		var sitting_student_id = $(this).closest('tr').attr('data-sitting-student-id');
		$('#student_title').attr('data-sitting-student-id', sitting_student_id);
		$('.results_view_button[data-view-type="student"]').trigger('click');
	});

	$('.column_sort').on('click', function() {
		var sortOrder;
		if ($('#summary_rows')[0].hasAttribute('data-sort-order')) {
			sortOrder = JSON.parse($('#summary_rows').attr('data-sort-order'));
		} else sortOrder = [];

		var index = $(this).closest('td').index();
		var sign = ($(this).attr('data-direction') == 'asc') ? 1 : -1;

		for (var i = 0; i < sortOrder.length; ) {
			if (sortOrder[i].index == index) sortOrder.splice(i, 1);
			else ++i;
		}

		sortOrder.push({
			index: index,
			sign: sign
		});

		$('#summary_rows').attr('data-sort-order', JSON.stringify(sortOrder));
		sortSittingSummary();
	});

	var sortSittingSummary = function() {
		var sortOrder;
		if ($('#summary_rows')[0].hasAttribute('data-sort-order')) {
			sortOrder = JSON.parse($('#summary_rows').attr('data-sort-order'));
		} else sortOrder = [];

		var entries = [];
		$('#summary_rows tr').each(function() {
			var thisEntry = {
				row: $(this),
				values: []
			}
			var thisValues = JSON.parse($(this).attr('data-sort-values'));
			for (var i = 0; i < thisValues.length; ++i) {
				if (typeof thisValues[i] === 'string') {
					thisEntry.values.push(String(thisValues[i]).toLowerCase());
				} else thisEntry.values.push(thisValues[i]);
			}

			entries.push(thisEntry);
		});
		
		for (var i = 0; i < sortOrder.length;++i) {
			var index = sortOrder[i].index;
			var sign = sortOrder[i].sign;
		
			entries.sort(function(a, b) {
				if (a.values[index] < b.values[index]) return -sign;
				else if (a.values[index] > b.values[index]) return sign;
				else return 0;
			});	
		}

		for (var i = 0; i < entries.length; ++i) {
			$('#summary_rows').append(entries[i].row);
		}
	}

	$('#sitting_reload').on('click', function() {
		showSubmitProgress($('#sitting_reload_wrapper'));
		buildSummary();
	})

	$(document).on('change', '#sitting_status', function(e) {
		if ($(this).val() == 'open') {
			$('#openSitting_dialog').dialog('open');
		} else if ($(this).val() == 'closed') {
			$('#closeSitting_dialog').dialog('open');
		}
	});
		
	var openSitting = function() {
		var $dialog = $(this);

		var postData = { close_type: $('#sitting_closeType').val() };
		if ($('#sitting_closeType').val() == 'Specified') {
			var close = dayjs($('#sitting_close_date').val() + ' ' + $('#sitting_close_time').val()).utc();
			postData.close_datetime = close.format('YYYY-MM-DD HH:mm:ss');
		}

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/Sittings/open/' + sittingID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(postData)
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				sittingData = JSON.parse(responseData.json_data);

				$('#sitting_status').val('open');
				$('.student_status').each(function() {
					var sittingStudent_id = $(this).closest('tr').attr('data-sitting-student-id');
					if (sittingStudent_id in responseData.status) {
						$(this).val(responseData.status[sittingStudent_id]);
					}
				});
				$('#sitting_closed').hide();
				$('#sitting_open').show();

				$.gritter.add({
					title: "Success",
					text: "Sitting opened.",
					image: "/img/success.svg"
				});

				$dialog.dialog('close');
			} else {
				showSubmitError($dialog);
			}			
		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$('#sitting_closeType').on('change', function() {
		if ($(this).val() == 'Specified') {
			var currentDate = new Date();
			$("#sitting_close_date").datepicker("setDate", currentDate);			

			var hours = currentDate.getHours();
			hours = (hours < 10 ? "0" : "") + hours;
			var minutes = currentDate.getMinutes();
			minutes = (minutes < 10 ? "0" : "") + minutes;
			$("#sitting_close_time").val(hours + ":" + minutes);

			$('#sitting_close_details').show();

		} else $('#sitting_close_details').hide();
	});

	$('#openSitting_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
				$('#sitting_status').val('closed');
               	$(this).dialog("close");
			},
          	'Open sitting': openSitting
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Open sitting")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});	

	var closeSitting = function() {
		var $dialog = $(this);

	    if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/Sittings/close/' + sittingID, {
			method: 'POST'
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				sittingData = JSON.parse(responseData.json_data);

				$('#sitting_status').val('closed');
				$('.student_status').val('closed');
				$('#sitting_open').hide();
				$('#sitting_closed').show();

				$.gritter.add({
					title: "Success",
					text: "Sitting closed.",
					image: "/img/success.svg"
				});					

				$dialog.dialog('close');
			} else {
				showSubmitError($dialog);
			}	

		}).catch(function() {
			showSubmitError($dialog);
		});
	};

	$('#closeSitting_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
				$('#sitting_status').val('open');
               	$(this).dialog("close");
			},
          	'Close sitting': closeSitting
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Close sitting")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});	
	
	var openAttempt = function() {
		var $dialog = $(this);

		var postData = { close_type: $('#attempt_closeType').val() };
		if ($('#attempt_closeType').val() == 'Specified') {
			var close = dayjs($('#attempt_close_date').val() + ' ' + $('#attempt_close_time').val()).utc();
			postData.close_datetime = close.format('YYYY-MM-DD HH:mm:ss');
		}

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var sittingStudentID = $dialog.attr('data-sitting-student-id');
		ajaxFetch('/SittingStudents/open/' + sittingStudentID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(postData)
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			var $row = $('#summary_rows tr[data-sitting-student-id="' + responseData.sitting_student_id + '"]');
			$row.find('.wait_icon').hide();

			if (responseData.success) {
				if (responseData.status == 'open') {
					$.gritter.add({
						title: "Success",
						text: "Attempt opened.",
						image: "/img/success.svg"
					});
				} else {
					$.gritter.add({
						title: "Success",
						text: "Attempt has closed.",
						image: "/img/success.svg"
					});
				}

				$row.find('.student_status').val(responseData.status);
				$dialog.dialog('close');
			} else {
				showSubmitError($dialog);
			}

		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$('#attempt_closeType').on('change', function() {
		if ($(this).val() == 'Specified') {
			var currentDate = new Date();
			$("#attempt_close_date").datepicker("setDate", currentDate);			

			var hours = currentDate.getHours();
			hours = (hours < 10 ? "0" : "") + hours;
			var minutes = currentDate.getMinutes();
			minutes = (minutes < 10 ? "0" : "") + minutes;
			$("#attempt_close_time").val(hours + ":" + minutes);

			$('#attempt_close_details').show();

		} else $('#attempt_close_details').hide();
	});

	$('#openAttempt_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
				var sittingStudentID = $(this).attr('data-sitting-student-id');
				var $row = $('#summary_rows tr[data-sitting-student-id="' + sittingStudentID + '"]');
				$row.find('.student_status').val('closed');
				$(this).dialog("close");
			},
          	'Open attempt': openAttempt
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Open attempt")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});	

	$(document).on('click', '.item_menu_scoring', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var $wrapper = $(this).closest('.part_wrapper');

        var questionHandleID = $wrapper.closest('.question_wrapper').attr('data-question-handle-id');
        var partID = $wrapper.attr('data-part-id');

//        *** THE BELOW WONT WORK FOR QUESTION SETS ***

		QuestionDetails.setActiveItem(false);
		for (var i = 0; i < layoutSections.length; ++i) {
			for (var j = 0; j < layoutSections[i].Content.length; ++j) {
				var thisData = layoutSections[i].Content[j];
				if ((thisData.Source.question_handle_id == questionHandleID) && (thisData.Source.part_id == partID)) {
					QuestionDetails.setActiveItem(JSON.parse(JSON.stringify(thisData)));
					break;
				}
			}
			if (QuestionDetails.activeItem !== false) break;
		}

		var questionType = $wrapper.attr('data-type');
		var questionStyle = questionType.substr(0, 2);

        if ('tweaks' in sittingData.Scoring) {
            var contentID = $wrapper.closest('.question_wrapper').attr('data-content-id');
            var partIndex = parseInt($wrapper.attr('data-part-index'));

            for (var i = 0; i < sittingData.Scoring.tweaks.length; ++i) {
                var thisEntry = sittingData.Scoring.tweaks[i];
                if ((thisEntry.content_id == contentID) && (thisEntry.part_index == partIndex)) {
                    QuestionDetails.activeItem.scoring = thisEntry.type;
                }
            }    
        }

		var numberingOptions = [QuestionDetails.activeItem.number];
		
		if (questionStyle == 'mc') {
	
			$('#dmcq_numberingType').empty();
			for (var i = 0; i < numberingOptions.length; ++i) {
				var value = numberingOptions[i];
				$('#dmcq_numberingType').append('<option value="' + value + '">' + value + '</option>')
			}
			$('#dmcq_numberingType').val(QuestionDetails.activeItem.number);
	
			// Show remaining details
	
			QuestionDetails.rebuildMCDetails();
	
			$('#mc_scoring_dialog').dialog('open');	
			document.activeElement.blur();      

		} else if (questionStyle == 'nr') {

			$('#dnq_numberingType').empty();
			for (var i = 0; i < numberingOptions.length; ++i) {
				var value = numberingOptions[i];
				$('#dnq_numberingType').append('<option value="' + value + '">' + value + '</option>')
			}
			$('#dnq_numberingType').val(QuestionDetails.activeItem.number);
	
			// Check for calculated responses and disable changes to answers if there are any

			var partData = false;
			for (var i = 0; i < questionData[questionHandleID].json_data.Parts.length; ++i) {
				var thisData = questionData[questionHandleID].json_data.Parts[i];
				if (thisData.id == partID) partData = thisData;
			}
	
			var variableRegex = /<span[^>]+class="block_variable"[^>]*><\/span>/;
			var hasCalculated = false;
			for (var j = 0; j < partData.Answers.length; ++j) {
				if (variableRegex.test(partData.Answers[j].template)) hasCalculated = true;
			}

			$('#dnq_has_calculated').val(hasCalculated ? 1 : 0);

			// Rebuild details and open dialog

			QuestionDetails.rebuildNRDetails();

			$('#nr_scoring_dialog').dialog('open');
			if ($('#dnq_scoring').val() == 'Omit') document.activeElement.blur();
			else $('#dnq_answer_0').focus().val($('#dnq_answer_0').val());

		} else {

			$('#dwq_numberingType').empty();
			for (var i = 0; i < numberingOptions.length; ++i) {
				var value = numberingOptions[i];
				$('#dwq_numberingType').append('<option value="' + value + '">' + value + '</option>')
			}
			$('#dwq_numberingType').val(QuestionDetails.activeItem.number);
	
			// Show remaining details
	
			$('#wr_scoring_dialog').dialog('open');
	
			QuestionDetails.rebuildWRDetails(false);
			document.activeElement.blur();

		}

	});

	$(document).on('click', '.item_menu_reasoning', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var $menuItem = $(this);
		
		var $wrapper = $(this).closest('.part_wrapper');
		if ($wrapper.attr('data-type') != 'wr') {

            var contentID = $wrapper.closest('.question_wrapper').attr('data-content-id');
            var partIndex = parseInt($wrapper.attr('data-part-index'));
            
			var reasoning_parts = sittingData.View.reasoning.parts;

			var hasReasoning = false;
			var index = 0;
			while (index < reasoning_parts.length) {
				var thisPart = reasoning_parts[index];
				if ((thisPart.content_id == contentID) && (thisPart.part_index == partIndex)) {
					reasoning_parts.splice(index, 1);
					hasReasoning = true;
				} else ++index;
			}

			if (!hasReasoning) reasoning_parts.push({
				content_id: contentID,
				part_index: partIndex
			});

			sittingData.View.reasoning.parts = reasoning_parts;

			// Save new settings

			if (pendingAJAXPost) return false;
			pendingAJAXPost = true;

			var data_string = JSON.stringify(sittingData);

			var postData = {
				data_string: data_string,
				data_hash: SparkMD5.hash(data_string)
			};

			ajaxFetch('/Sittings/save/' + sittingID, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(postData)
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();

				if (responseData.success) {

					SittingsCommon.updateReasoningFields($('#assessment_preview_wrapper'), sittingData.View);

					if (hasReasoning) $menuItem.html('Add reasoning');
					else $menuItem.html('Remove reasoning');
		
					$.gritter.add({
						title: "Success",
						text: "Reasoning " + (hasReasoning ? "removed" : "added") + ".",
						image: "/img/success.svg"
					});					

				} else {

					$.gritter.add({
						title: "Error",
						text: "Unable to " + (hasReasoning ? "remove" : "add") + " reasoning.",
						image: "/img/error.svg"
					});					

				}
			});
		}
	});

	var scoringSave = function() {
		var $dialog = $(this);

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		// Update layout sections

		var questionHandleID = QuestionDetails.activeItem.Source.question_handle_id;
		var partID = QuestionDetails.activeItem.Source.part_id;

		for (var i = 0; i < layoutSections.length; ++i) {
			for (var j = 0; j < layoutSections[i].Content.length; ++j) {
				var thisData = layoutSections[i].Content[j];
				if ((thisData.Source.question_handle_id == questionHandleID) && (thisData.Source.part_id == partID)) {
					layoutSections[i].Content[j] = QuestionDetails.activeItem;
				}
			}
		}

		// Get scoring data ready

		var updatedKeys = ['type', 'Source', 'scoring'];
		if (QuestionDetails.activeItem.type.substr(0, 2) == 'mc') {
			updatedKeys.push('ScoredResponses');
		} else if (QuestionDetails.activeItem.type.substr(0, 2) == 'nr') {
			if ($('#dnq_has_calculated').val() == 0) updatedKeys.push('Answers');
			updatedKeys.push('ScoredResponses');
			updatedKeys.push('TypeDetails');
		} else if (QuestionDetails.activeItem.type == 'wr') {
			updatedKeys.push('TypeDetails');
		}

		var json_data = {};
		for (var i = 0; i < updatedKeys.length; ++i) {
			if (updatedKeys[i] in QuestionDetails.activeItem) {
				json_data[updatedKeys[i]] = QuestionDetails.activeItem[updatedKeys[i]];
			}
		}
		var data_string = JSON.stringify(json_data);

		var requestData = {
			data_string: data_string,
			data_hash: SparkMD5.hash(data_string)
		};

		// Update scoring data

		ajaxFetch('/Sittings/update_scoring/' + sittingID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(requestData)
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
                                
                // Add part index to question data

                for (var i = 0; i < responseData.question_data.json_data.Parts.length; ++i) {
                    responseData.question_data.json_data.Parts[i].index = i;
                }

                // Update question data

				questionData[questionHandleID] = responseData.question_data;

                // Save sitting data
                                
				sittingData = responseData.sitting_data;
				
				wr_allData = null;

				$.gritter.add({
					title: "Success",
					text: "Scoring updated.",
					image: "/img/success.svg"
				});		

				$dialog.dialog('close');

			} else showSubmitError($dialog);
		});
	}
	
    $('#mc_scoring_dialog').dialog({
        autoOpen: false,
		width: 450,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': function() {
				if (QuestionDetails.validateMCQuestion()) {
					var contextedSave = $.proxy(scoringSave, $('#mc_scoring_dialog'));
					contextedSave();
				}
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
		 	initSubmitProgress($(this));
		}, close: function() {
			hideSubmitProgress($(this));
        }
    });

    $('#nr_scoring_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$(this).dialog('close');
			},
			"Save": function() {
				if (QuestionDetails.validateNRQuestion() && $('#nr_scoring_dialog form').validationEngine('validate')) {
					var contextedSave = $.proxy(scoringSave, $('#nr_scoring_dialog'));
					contextedSave();
				}
			}
        }, open: function(event) {
		 	var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
        	QuestionDetails.rebuildNRValidations();
		 	$('#dnq_answer_0').focus();
		 	initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
	});

    $('#wr_scoring_dialog').dialog({
		autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$(this).dialog('close');
			},
			"Save": function() {
				if (QuestionDetails.validateWRQuestion() && $('#wr_scoring_dialog form').validationEngine('validate')) {
					var contextedSave = $.proxy(scoringSave, $('#wr_scoring_dialog'));
					contextedSave();
				}
			}
        }, open: function(event) {
		 	var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
		 	initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
	});

	$(document).on('click', '.item_menu_edit_comment', function(e) {
		e.preventDefault();
		if (!canEdit) return;
		
		var $questionWrapper = $(this).closest('.part_wrapper');
		$('#editComment_index').val($('#student_assessment_wrapper .part_wrapper').index($questionWrapper));

		var $commentInput = $questionWrapper.find('.wr_comment_wrapper .wr_input_field');
		if ($commentInput.length == 1) $('#editComment_editor').htmleditor('setValue', $commentInput.html());
		else $('#editComment_editor').htmleditor('setValue', '');
		$('#editComment_editor').htmleditor('initSelection');

		$('#editComment_dialog').dialog('open');
		$('#editComment_editor').focus();
	});

	var saveComment = function() {
		var $dialog = $(this);

		var sittingStudentID = $('#student_title').attr('data-sitting-student-id');
		var attemptID = $('#student_details_window').attr('data-response-online-id');
		var questionIndex = $('#editComment_index').val();
		var html = $('#editComment_editor').htmleditor('getValue');
		
		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/SittingStudents/save_comment/' + sittingStudentID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				response_online_id: attemptID,
				question_index: questionIndex,
				comment: html
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				var questionIndex = $('#editComment_index').val();
				var $questionWrapper = $('#student_assessment_wrapper .part_wrapper').eq(questionIndex);
				SittingsCommon.getCommentField($questionWrapper).html(responseData.html);
				RenderTools.renderEquations($questionWrapper);

				$.gritter.add({
					title: "Success",
					text: "Comment saved.",
					image: "/img/success.svg"
				});					
			} else {
				$.gritter.add({
					title: "Error",
					text: "Unable to save comment.",
					image: "/img/error.svg"
				});					
			}

			$('#editComment_dialog').dialog('close');
		});
	}

	$('#editComment_dialog').dialog({
        autoOpen: false,
		width:700,
		height:535,
		modal: true,
		resizable: true,
		minWidth:700,
		minHeight:300,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': saveComment
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));

			$('#dpre_preamble_edit').htmleditor('initSelection');
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });

    $('#dpre_preamble_edit').on('savedom', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });

	$(document).on('click', '.item_menu_delete_comment', function(e) {
		e.preventDefault();
		if (!canEdit) return;
		
		var $questionWrapper = $(this).closest('.part_wrapper');
		$('#deleteComment_index').val($('#student_assessment_wrapper .part_wrapper').index($questionWrapper));

		$('#deleteComment_dialog').dialog('open');
	});

	var deleteComment = function() {
		var $dialog = $(this);

		var sittingStudentID = $('#student_title').attr('data-sitting-student-id');
		var attemptID = $('#student_details_window').attr('data-response-online-id');
		var questionIndex = $('#deleteComment_index').val();
		
		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/SittingStudents/delete_comment/' + sittingStudentID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				response_online_id: attemptID,
				question_index: questionIndex
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				var questionIndex = $('#deleteComment_index').val();
				var $questionWrapper = $('#student_assessment_wrapper').find('.part_wrapper').eq(questionIndex);
				$questionWrapper.find('.wr_comment_wrapper').remove();

				$.gritter.add({
					title: "Success",
					text: "Deleted comment.",
					image: "/img/success.svg"
				});					
			} else {
				$.gritter.add({
					title: "Error",
					text: "Unable to delete comment.",
					image: "/img/error.svg"
				});					
			}

			$('#deleteComment_dialog').dialog('close');
		});		
	}

	$('#deleteComment_dialog').dialog({
        autoOpen: false,
        width: 400,
        modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				$(this).dialog("close");
		 	},
			'Delete': deleteComment
		 }, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

	var shuffle = function(array) {
		var m = array.length, t, i;
	  
		while (m) {
			i = Math.floor(Math.random() * m--);
	  
			t = array[m];
			array[m] = array[i];
			array[i] = t;
		}
	  
		return array;
	}

	var rebuildWrittenViews = async function() {
		if (wr_allData == null) {

			$('#loading_written_data').hide();

			$('#written_menu .menu_item').removeClass('active');
			$('#no_written_selected').show();

			$('#written_controls_wrapper').hide();
			$('#written_details_window').hide();

		} else {

			if (wr_displayData === null) {
				wr_displayData = [];
				for (var i = 0; i < wr_allData.length; ++i) {
					var attempt = wr_allData[i];
	
					var included = true;
					if (!('responses' in attempt) || !(attempt.index in attempt.responses)) included = false;
					else if (attempt.responses[attempt.index].value.length == 0) {
						included &= !$('#wr_show_completed').prop('checked');
					}
	
					var isScored = true;
					if (!('results' in attempt) || !(attempt.index in attempt.results)) isScored = false;
					else if (attempt.results[attempt.index].length == 0) isScored = false;
					else {
						for (var j = 0; j < attempt.results[attempt.index].length; j += 2) {
							if (attempt.results[attempt.index].substr(j, 2) == '  ') {
								isScored = false;
							}
						}
					}
					if (isScored) included &= !$('#wr_show_unscored').prop('checked');
	
					if (attempt.status == 'open') {
						included &= !$('#wr_show_closed').prop('checked');
					}
	
					if (included) wr_displayData.push(attempt);
				}
				if (wr_index >= wr_displayData.length) {
					wr_index = wr_displayData.length - 1;
				}
	
				if ($('#wr_shuffle').prop('checked')) {
					wr_displayData = shuffle(wr_displayData);
				}
			}
	
			$('#written_controls_wrapper').show();

			if (wr_displayData.length == 0) {
	
				// No responses to show
	
				$('#wr_response_label').hide();
				$('#wr_no_responses').show();
				$('#written_details_window').hide();
	
				$('#wr_previous').attr('disabled', 'disabled');
				$('#wr_next').attr('disabled', 'disabled');
	
				$('#student_scoring_title').html('selected response');
				
			} else {
	
				// Matching responses exist. Show the selected one.
	
				$('#wr_no_responses').hide();
				$('#wr_response_label').show();
				$('#written_details_window').show();
	
				if (wr_index > 0) $('#wr_previous').removeAttr('disabled');
				else $('#wr_previous').attr('disabled', 'disabled');
		
				if (wr_index < wr_displayData.length - 1) $('#wr_next').removeAttr('disabled');
				else $('#wr_next').attr('disabled', 'disabled');
		
				$('#wr_response_index').html(1 + wr_index);
				$('#wr_response_count').html(wr_displayData.length);
		
				var attempt = wr_displayData[wr_index];
		
				var $viewBuffer = $('<div></div>');
				await SittingsCommon.previewFromHTML($viewBuffer, attempt.html, assessmentData.Settings, sittingData.View);
				if ('responses' in attempt) SittingsCommon.fillResponses($viewBuffer, attempt.responses);
				if ('annotations' in attempt) {
					const annotations = attempt.annotations;
					$viewBuffer.find('.question_wrapper').each(function() {
						const contentID = $(this).attr('data-content-id');
						if (contentID in annotations) {
							SittingsCommon.drawAnnotations($(this), annotations[contentID]);
						}
					});
				}
				
				var goodScores = true;
				if ('results' in attempt) {
					$('#item_score_wrapper input').prop('checked', false);
					$('#item_score_wrapper .item_score_element').removeClass('selected');
	
					var results = attempt.results[attempt.index];
					var rowIndex = 0;
					while (results.length >= 2) {
						var thisValue = results.substr(0, 2);
						if (thisValue != '  ') {
							var $input = $('input[name="item_score_' + rowIndex + '"][data-value="' + thisValue + '"]');
							if ($input.length == 1) $input.prop('checked', true);
							else goodScores = false;
						}
						updateScoreRow($('#item_score_wrapper .item_score_row').eq(rowIndex));
						results = results.substr(2);
						rowIndex++;
					}
	
					if (results.length > 0) goodScores = false;
				}
		
				if (!goodScores) {
					$('.item_score_input').prop('checked', false);
					$.gritter.add({
						title: "Error",
						text: "Unable to read scores.",
						image: "/img/error.svg"
					});					
				}
	
				if ('responses' in attempt) {
					var responses = attempt.responses[attempt.index];
					if ('comment' in responses) {
						$('#wr_comment').htmleditor('setValue', responses.comment);
					} else $('#wr_comment').htmleditor('setValue', '');
				}
				$('#wr_comment').blur();
	
				if ($('#wr_show_label').prop('checked')) {
					if ('details' in attempt) {
						var requestedLabel = $('#student_label_select').val();
						var label = getLabel(attempt['details'], requestedLabel);
						$('#student_scoring_title').text((label === false) ? 'selected response' : label);
					} else $('#student_scoring_title').text('selected response');
				} else $('#student_scoring_title').text('selected response');
	
				var $questionWrapper = $viewBuffer.find('.part_wrapper').eq(attempt.index);
				$questionWrapper.find('.wr_input_field').css('background-color', '#e9f4f9');

				var $itemWrapper = $questionWrapper.closest('.question_wrapper');
				$('#item_view_wrapper').empty().append($itemWrapper);
				$('#item_view_wrapper').find('.wr_comment_wrapper').remove();
	
				$('#written_details_select').val('response');
				if (('rubric' in attempt) && (attempt.rubric.length > 0)) {
					$('#item_rubric_wrapper').html(attempt.rubric);
					$('#written_details_select').removeAttr('disabled');
				} else {
					$('#written_details_select').attr('disabled', 'disabled');
				}
		
				await RenderTools.renderEquations($('#item_view_wrapper'));
				await RenderTools.renderEquations($('#item_rubric_wrapper'));
			}

		}
	}

	$('#written_details_select').on('change', function() {
		if ($(this).val() == 'response') {
			$('#item_rubric_wrapper').hide();
			$('#item_view_wrapper').show();
		} else {
			$('#item_view_wrapper').hide();
			$('#item_rubric_wrapper').show();
		}
	});

	$('#written_menu').on('click', '.menu_item', function(e) {
        e.preventDefault();
        
		$('#written_menu .menu_item').removeClass('active');
		$(this).addClass('active');

        // Select the item in the assessment preview
        
        var questionHandleID = $(this).attr('data-question-handle-id');
        var partID = $(this).attr('data-part-id');

		$('#no_written_selected').hide();
		$('#written_controls_wrapper').hide();
		$('#loading_written_data').show();

		$('#written_details_window').hide();

		var criteria = false;
		var selectedData = questionData[questionHandleID].json_data;
		for (var i = 0; i < selectedData.Parts.length; ++i) {
			if (selectedData.Parts[i].id == partID) {
				criteria = selectedData.Parts[i].TypeDetails.criteria;
			}
		}

		var newHTML = '';
		for (var i = 0; i < criteria.length; ++i) {
			newHTML += "<div style='grid-column:1; grid-row:" + (i + 1) + "; margin-right:10px;'>" + criteria[i].label + ":</div>";
			newHTML += "<div class='item_score_row' style='grid-column:2; grid-row:" + (i + 1) + ";'>";

			var row_name = 'item_score_' + i;
			for (var j = 0; j <= 2 * criteria[i].value; ++j) {
				var element_classes = 'item_score_element';
				if (j == 0) element_classes += ' first';
				else if (j == 2 * criteria[i].value) element_classes += ' last';

				var label_text;
				if (j % 2 == 0) {
					label_text = (j / 2).toFixed(0);
				} else {
					element_classes += ' half_mark';
					label_text = '&#xBD;';
				}

				var input_id = 'item_score_' + i + '_' + j;
				var value = Math.floor(0.5 * j).toFixed(0) + ((j % 2 == 0) ? ' ' : '+');
				newHTML += 
					"<div class='" + element_classes + "' style='grid-column:" + (j + 2) + "; grid-row:" + (i + 1) + ";'> \
						<input class='item_score_input' type='radio' name='" + row_name + "' id='" + input_id + "' data-value='" + value + "'/> \
						<label class='item_score_label' for='" + input_id + "'><div>" + label_text + "</div> \
						</label> \
					</div>";
			}

			newHTML += "</div>";
		}
		$('#item_score_wrapper').html(newHTML);

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		const queryString = new URLSearchParams({
			question_handle_id: questionHandleID,
			part_id: partID
		}).toString();

		ajaxFetch(`/Sittings/get_written/${sittingID}?${queryString}`)
		.then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			$('#loading_written_data').hide();

			if (responseData.success) {
				wr_allData = responseData.attempt_data;
				wr_displayData = null;
				wr_index = 0;

				rebuildWrittenViews();
			} else {
				$('#no_written_selected').show();
			}
		});
	});

	var saveResults = function(sendAsBeacon) {
		var resultsJSON = JSON.stringify(scoringResults);

		var url = '/Sittings/save_results/' + sittingID;
		var data = {
			results_hash: SparkMD5.hash(resultsJSON),
			results: resultsJSON
		};

		if (!sendAsBeacon) {

			ajaxFetch(url, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(data)
			}).then(async response => {
				const responseData = await response.json();

				if (responseData.success) {
					SittingsCommon.showAndFade($('#save_success'));
				} else $('#save_warning').show();
			}).catch(function(error) {
				$('#save_warning').show();
			});

		} else if ("sendBeacon" in navigator) {

			var formData = new FormData();
			for (var key in data) formData.append(key, data[key]);
			navigator.sendBeacon(url, formData);

		} else {

			ajaxFetch(url, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(data)
			});
			
		}
	}

	$(document).on('click', '.item_score_label', function(e) {
		e.preventDefault();
		var $input = $(this).siblings('.item_score_input');
		$input.prop('checked', !$input.prop('checked'));
		$input.trigger('change');
	});

	var updateScoreRow = function($row) {
		$row.find('.item_score_element').removeClass('selected');
		$row.find('input:checked').each(function() {
			var $element = $(this).closest('.item_score_element');
			$element.addClass('selected');
			if ($element.hasClass('half_mark')) $element.prev().addClass('selected');
		});
	}

	$(document).on('change', '.item_score_input', function() {
		updateScoreRow($(this).closest('.item_score_row'));
		updateScoringResults();
		queueSave(500);
	});

	$(document).on('savedom', '#wr_comment', function(e) {
		updateScoringResults();
		queueSave(5000);
	});

	var updateScoringResults = function() {

		var attempt = wr_displayData[wr_index];

		var newResult = '';
		var rowIndex = 0;
		var $inputs = null;
		do {
			$inputs = $('input[name="item_score_' + rowIndex++ + '"]');
			if ($inputs.length > 0) {
				var $selected = $inputs.filter(':checked');
				if ($selected.length == 1) newResult += $selected.attr('data-value');
				else newResult += '  ';
			}
		 } while ($inputs.length > 0);

		if (!('results' in wr_displayData[wr_index])) {
			wr_displayData[wr_index].results = {};
		}
		wr_displayData[wr_index].results[attempt.index] = newResult;

		scoringResults[attempt.id] = {
			index: attempt.index,
			result: newResult
		};

		var commentHTML = $('#wr_comment').htmleditor('getValue');
		if (commentHTML.length > 0) {
			wr_displayData[wr_index].responses[attempt.index].comment = commentHTML;
			scoringResults[attempt.id].comment = commentHTML;
		} else delete wr_displayData[wr_index].responses[attempt.index].comment;
	}

	$(document).on('blur', '#wr_comment', function(e) {
		if (scoringTimeout != null) queueSave(500);
	});

	var queueSave = function(delay) {
		$('#save_success').hide();
		$('#save_warning').hide();
		if (scoringTimeout != null) clearTimeout(scoringTimeout);
		scoringTimeout = setTimeout(function() {
			saveResults(false);
			scoringTimeout = null;
		}, delay);
	}

	window.onunload = function() {
		if (scoringTimeout !== null) {
			clearTimeout(scoringTimeout);
			saveResults(true);
		}		
	}

	$('#save_retry').on('click', function() {
		$('#save_warning').hide();
		if (scoringTimeout != null) {
			clearTimeout(scoringTimeout);
			scoringTimeout = null;
		}
		saveResults(false);
	});
	
	$(document).on('keydown', function(e) {
		if ($('#written_controls_wrapper').is(':visible') && !$('#wr_comment').is(':focus')) {
			if (e.key === 'ArrowLeft') {
				e.preventDefault();
				$('#wr_previous').trigger('click');
			} else if (e.key === 'ArrowRight') {
				e.preventDefault();
				$('#wr_next').trigger('click');
			}
		}
	});
	
	$('#wr_previous').on('click', function() {
		if (wr_index > 0) {
			wr_index--;
			rebuildWrittenViews();
		}
		$(this).blur();
	});

	$('#wr_next').on('click', function() {
		if (wr_index < wr_displayData.length - 1) {
			wr_index++;
			rebuildWrittenViews();
		}
		$(this).blur();
	});

	$(".wr_filter_input").on('change', function() {
		wr_displayData = null;
		wr_index = 0;
		rebuildWrittenViews();
	});

	$(document).on('click', '.wr_input_field img', function() {
		$('#image_view_panel').show();

		$('#image_view_inner').html("<img src='" + $(this).attr('src') + "' />");

		var windowWidth = $(window).width();
		var windowHeight = $(window).height();

		var divWidth = $('#image_view_panel').width();
		var divHeight = $('#image_view_panel').height();
		var divLeft = (windowWidth - divWidth) / 2;
		var divTop = (windowHeight - divHeight) / 2;

		$('#image_view_panel').css({
			left: divLeft,
			top: divTop
		});

		var imageWidth = $('#image_view_inner img').width();
		var imageHeight = $('#image_view_inner img').height();
		var scale = divWidth / imageWidth;

		var matrix = [scale, 0.0, 0.0, scale, 0.0, 0.0];
		$('#image_view_panel').attr('data-transform-matrix', JSON.stringify(matrix));
		var inverse = [1.0 / scale, 0.0, 0.0, 1.0 / scale, 0.0, 0.0];
		$('#image_view_panel').attr('data-transform-inverse', JSON.stringify(inverse));

		$('#image_view_panel').before('<div id="image_view_overlay" class="ui-widget-overlay ui-front"></div>');
		$('body').css('overflow', 'hidden');

		rebuildImageView([imageWidth / 2, imageHeight / 2]);
	});

	$(document).on('click', '#image_view_overlay, #image_view_panel .close_image_icon', function() {
		$('body').css('overflow', 'visible');
		$('#image_view_panel').hide();
		$('#image_view_overlay').remove();
	});

	var getImageCenter = function() {
		var windowCenterX = $('#image_view_wrapper').scrollLeft() + $('#image_view_wrapper').width() / 2;
		var windowCenterY = $('#image_view_wrapper').scrollTop() + $('#image_view_wrapper').height() / 2;

		var inverse = JSON.parse($('#image_view_panel').attr('data-transform-inverse'));

		var imageCenter = [];
		imageCenter.push(inverse[0] * windowCenterX + inverse[2] * windowCenterY + inverse[4]);
		imageCenter.push(inverse[1] * windowCenterX + inverse[3] * windowCenterY + inverse[5]);

		return imageCenter;
	}

	var rebuildImageView = function(imageCenter) {
		var matrix = JSON.parse($('#image_view_panel').attr('data-transform-matrix'));

		var imageWidth = $('#image_view_inner img').width();
		var imageHeight = $('#image_view_inner img').height();
		var imageCorners = [[0, 0], [0, imageHeight], [imageWidth, 0], [imageWidth, imageHeight]];

		var min = [null, null];
		var max = [null, null];
		for (var i = 0; i < imageCorners.length; ++i) {
			var x = matrix[0] * imageCorners[i][0] + matrix[2] * imageCorners[i][1] + matrix[4];
			var y = matrix[1] * imageCorners[i][0] + matrix[3] * imageCorners[i][1] + matrix[5];
			if ((min[0] == null) || (x < min[0])) min[0] = x;
			if ((min[1] == null) || (y < min[1])) min[1] = y;
			if ((max[0] == null) || (x > max[0])) max[0] = x;
			if ((max[1] == null) || (y > max[1])) max[1] = y;
		}

		var viewWidth = $('#image_view_wrapper').width();
		var viewHeight = $('#image_view_wrapper').height();

		$('#image_view_inner').width(max[0] - min[0] + 2 * viewWidth);
		$('#image_view_inner').height(max[1] - min[1] + 2 * viewHeight);

		matrix[4] -= min[0] - viewWidth;
		matrix[5] -= min[1] - viewHeight;
		$('#image_view_panel').attr('data-transform-matrix', JSON.stringify(matrix));

		var inverse = JSON.parse($('#image_view_panel').attr('data-transform-inverse'));

		inverse[4] = -inverse[0] * matrix[4] - inverse[2] * matrix[5];
		inverse[5] = -inverse[1] * matrix[4] - inverse[3] * matrix[5];
		$('#image_view_panel').attr('data-transform-inverse', JSON.stringify(inverse));

		var windowX = matrix[0] * imageCenter[0] + matrix[2] * imageCenter[1] + matrix[4];
		var windowY = matrix[1] * imageCenter[0] + matrix[3] * imageCenter[1] + matrix[5];

		$('#image_view_inner img').css({
			'transform-origin': 'top left',
			'transform': 'matrix(' + matrix.join(',') + ')'
		});

		$('#image_view_wrapper').scrollLeft(windowX - viewWidth / 2);
		$('#image_view_wrapper').scrollTop(windowY - viewHeight / 2);
	}

	var scaleImage = function(scale) {
		var imageCenter = getImageCenter();

		var matrix = JSON.parse($('#image_view_panel').attr('data-transform-matrix'));
		for (var i = 0; i < matrix.length; ++i) matrix[i] *= scale;
		$('#image_view_panel').attr('data-transform-matrix', JSON.stringify(matrix));

		var inverse = JSON.parse($('#image_view_panel').attr('data-transform-inverse'));
		for (var i = 0; i < inverse.length; ++i) inverse[i] /= scale;
		$('#image_view_panel').attr('data-transform-inverse', JSON.stringify(inverse));

		rebuildImageView(imageCenter);
	}

	var multiplyMatrices = function(a, b) {
		var product = [];
		product.push(a[0] * b[0] + a[2] * b[1]);
		product.push(a[1] * b[0] + a[3] * b[1]);
		product.push(a[0] * b[2] + a[2] * b[3]);
		product.push(a[1] * b[2] + a[3] * b[3]);
		product.push(a[0] * b[4] + a[2] * b[5] + a[4]);
		product.push(a[1] * b[4] + a[3] * b[5] + a[5]);
		return product;
	}

	var rotateImage = function(angle) {
		var imageCenter = getImageCenter();

		var sin = Math.sin(angle * Math.PI / 180);
		var cos = Math.cos(angle * Math.PI / 180);
		var rotationMatrix = [cos, sin, -sin, cos, 0, 0];
		var rotationInverse = [cos, -sin, sin, cos, 0, 0];

		var matrix = JSON.parse($('#image_view_panel').attr('data-transform-matrix'));
		matrix = multiplyMatrices(rotationMatrix, matrix);		
		$('#image_view_panel').attr('data-transform-matrix', JSON.stringify(matrix));

		var inverse = JSON.parse($('#image_view_panel').attr('data-transform-inverse'));
		inverse = multiplyMatrices(rotationInverse, inverse);		
		$('#image_view_panel').attr('data-transform-inverse', JSON.stringify(inverse));

		rebuildImageView(imageCenter);
	}

	$(document).on('click', '#image_view_panel .zoom_in_icon', function() {
		scaleImage(1.5);
	});

	$(document).on('click', '#image_view_panel .zoom_out_icon', function() {
		scaleImage(1.0 / 1.5);
	});

	$(document).on('click', '#image_view_panel .rotate_cw_icon', function() {
		rotateImage(90);
	});

	$(document).on('click', '#image_view_panel .rotate_ccw_icon', function() {
		rotateImage(-90);
	});

	$('#stats_update').on('mousedown', function(e) {
		e.preventDefault();
		
		$('#statistics_graphs').html("<div style='font-style:italic'>Loading statistics&hellip;</div>");
		$('#item_statistics_data').html("<div style='font-style:italic'>Loading statistics&hellip;</div>");
		$('#statistics_n').html('0');

		var question_ids = [];
		$('#assessment_preview_wrapper .question_wrapper:not(.shuffle_break)').each(function() {
			var this_handle_id = parseInt($(this).attr('data-question-handle-id'));
			question_ids.push(questionData[this_handle_id].id);
		});	

		showSubmitProgress($('#stats_button_wrapper'));
		$("#stats_print").attr('disabled', 'disabled');
	
		StatsCommon.loadFilteredStatistics(question_ids)
		.then(function() {
			var urlVars = getUrlVars();
			if ('c' in urlVars) {
				var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + urlVars.c + '"]');
				var questionHandleID = parseInt($itemWrapper.attr('data-question-handle-id'));
				var questionID = questionData[questionHandleID].id;

				StatsCommon.rebuildItemStatistics(questionID, questionData[questionHandleID].json_data, assessmentData.Settings.numberingType);
			}

			var questionHandleIDs = StatsCommon.getQuestionHandleIDs(assessmentData);
			StatsCommon.rebuildAssessmentStatistics(questionHandleIDs, questionData);

			hideSubmitProgress($('#stats_button_wrapper'));
			$("#stats_print").removeAttr('disabled');
		});
	});

	$('#stats_print').on('mousedown', function(e) {
		e.preventDefault();
	
		var questionHandleIDs = StatsCommon.getQuestionHandleIDs(assessmentData);
		var statsHTML = StatsCommon.buildStatsHTML(questionHandleIDs, questionData);

		navigateWithPost('/build_pdf', {
			pdfType: 'pdf_from_url',
			url: '/Sittings/print_questions/' + sittingID,
			postDataJSON: JSON.stringify({ 
				html: statsHTML,
				suppressInputs: true 
			}),
			target: 'statistics.pdf'
		});

		needsRender = true;
	});

	$('.link_copy').on('click', function() {
		copyText($('.tok_link').eq(0).text(), "link");
	});

	$(document).on('click', '#sections_menu .menu_item', function(e) {
        e.preventDefault();

		var sectionID = $(this).attr('data-section-id');
		selectSection(sectionID);

		var urlVars = getUrlVars();
		if (urlVars.view == 'statistics') drawStatistics();
    });

    $(document).on('click', '.items_panel .menu_item', function(e) {
		e.stopPropagation();

		if (!$(this).is('.active')) {
			var $parent = $(this).closest('li');
			var contentID = $parent.find('.content_item').attr('data-content-id');
			var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"]');
	
			if ($(this).is('.child_item')) {
				var questionHandleID = $(this).attr('data-question-handle-id');
				var rendered_index = $(this).index() - 1;
	
				for (var i = 0; i < assessmentData.Sections.length; ++i) {
					if (assessmentData.Sections[i].type == 'page_break') continue;
					for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
						if (assessmentData.Sections[i].Content[j].id == contentID) {
							assessmentData.Sections[i].Content[j].rendered_index = rendered_index;
						}
					}
				}
	
				$itemWrapper.attr('data-question-handle-id', questionHandleID);
	
				$itemWrapper.empty();
				AssessmentsRender.renderItemToDiv(questionData[questionHandleID].json_data, $itemWrapper)
				.then(async function() {
					await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($('#assessment_preview_wrapper'));
					$itemWrapper.find('.part_wrapper[data-type="mc"]').each(function() { 
						RenderTools.setMCWidths($(this)); 
					});
					SittingsCommon.applyOnlineStyling($itemWrapper, questionData, sittingData.View);
                    buildLayoutSections($('#assessment_preview_wrapper'));
                });
			}

			// Select the item in the assessment preview
			
			selectItem(contentID);

			var urlVars = getUrlVars();
			if (urlVars.view == 'preview') {
				
				$('#assessment_preview_wrapper .question_wrapper').removeClass('question_selected');
				$itemWrapper.addClass('question_selected');

				var itemTop = $itemWrapper.offset().top;
				$("html, body").animate({
					scrollTop: itemTop - 10
				}, 1000);		

			} else if (urlVars.view == 'statistics') {

				drawStatistics();

			}
		}
	});

	var selectSection = function(sectionID) {
		$('#sections_menu .menu_item').removeClass('active');
		$('#sections_menu .menu_item[data-section-id="' + sectionID + '"]').addClass('active');

		$('#assessment_preview_wrapper .question_wrapper').removeClass('question_selected');

		$('.items_panel').hide();
		$('.items_panel[data-section-id="' + sectionID + '"]').show();
	
		$('.items_menu .menu_item').removeClass('active');
	
		var urlVars = getUrlVars();
		urlVars.s = sectionID;
		delete urlVars.c;
		setUrlVars(urlVars);
	}
	
	var selectItem = function(contentID) {

		var urlVars = getUrlVars();
		urlVars.c = contentID;
		delete urlVars.s;
		setUrlVars(urlVars);

		var $menuItem = $('.menu_item[data-content-id="' + contentID + '"]');
		var $items_panel = $menuItem.closest('.items_panel');
		var sectionID = $items_panel.attr('data-section-id');

	   	// Unhighlight sections except for the active one

		$('#sections_menu .menu_item').removeClass('active');
		$('#sections_menu .menu_item[data-section-id="' + sectionID + '"]').addClass('active');

		// Open item panel

		$('.items_panel').hide();
		$items_panel.show();

	   	// Highlight the selected item link and unhighlight others

		   $('.items_menu').find('.menu_item').removeClass('active');
		   $menuItem.addClass('active');
   
		// Open item sets as needed

		if ($menuItem.is('.question_set')) {
			$menuItem.find('.question_set_reveal').addClass('selected');
			var $children = $menuItem.closest('li').find('.child_item');
			$children.show();

			var rendered_index = 0;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					if (assessmentData.Sections[i].Content[j].id == contentID) {
						rendered_index = assessmentData.Sections[i].Content[j].rendered_index;
					}
				}
			}

			$children.eq(rendered_index).addClass('active');
		}

		ScrollingCommon.fixLeftPanelPosition();
    }

	$(document).on('click', '.items_panel .question_set_reveal', function(e) {
        e.stopPropagation();

		var $thisItem = $(this).closest('li');

		if ($(this).is('.selected')) {
			$(this).removeClass('selected');
			$thisItem.find('.child_item').hide();
		} else {
			$(this).addClass('selected');
			$thisItem.find('.child_item').show();
		}
	});

	$('#student_menu').sortable({
		connectWith : '#student_menu'
	});

	$('#view_menu').menu({
		disabled: true
	});

	$('#results_view_menu').menu();

	var checkItemOptionDates = function() {
		if ($('#item_show_date').is(':visible') && $('#item_hide_date').is(':visible')) {
			var show_date = dayjs($('#item_show_date').datepicker('getDate'));
			var timeParts = $('#item_show_time').val().match(/(\d+)/g);
            if (timeParts && (timeParts.length == 2)) {
                show_date = show_date.hour(timeParts[0]).minute(timeParts[1]);
            } else return false;
	
			var hide_date = dayjs($('#item_hide_date').datepicker('getDate'));
			var timeParts = $('#item_hide_time').val().match(/(\d+)/g);
            if (timeParts && (timeParts.length == 2)) {
                hide_date = hide_date.hour(timeParts[0]).minute(timeParts[1]);
            } else return false;
	
			return hide_date.isAfter(show_date);	
		} else return true;
	}

	var showItemOptions = function() {
		var all_options = JSON.parse($('#results_menu').attr('data-json-data'));

		$('#results_menu li').each(function() {
			var key = $(this).attr('data-item-key');

			var index;
			for (index = 0; index < all_options.length; ++index) {
				if (all_options[index].type == key) break;
			}
			if (index < all_options.length) $(this).find('.item_check').html('&check;');
			else $(this).find('.item_check').html('');
		});

		var $selected = $('#results_menu li.ui-state-active');
		if ($selected.length == 1) {
			var key = $selected.eq(0).attr('data-item-key');

			var index;
			for (index = 0; index < all_options.length; ++index) {
				if (all_options[index].type == key) break;
			}
			if (index < all_options.length) {

				$('#item_status').prop('checked', true);
				$('.item_detail_wrapper').show();

				var options = all_options[index];
				$('#item_show_type').val(options.show.type);
				if (options.show.type == 'date') {
					if (options.show.date.length > 0) {
						var date = dayjs.utc(options.show.date).local();
						$('#item_show_date').datepicker('setDate', date.format('MMMM D, YYYY'));
						$('#item_show_time').val(date.format('HH:mm'));
					} else {
						$('#item_show_date').val('');
						$('#item_show_time').val('');
					}
					$('#item_show_details').show();
				} else $('#item_show_details').hide();
		
				$('#item_hide_type').val(options.hide.type);
				if (options.hide.type == 'date') {
					if (options.hide.date.length > 0) {
						var date = dayjs.utc(options.hide.date).local();
						$('#item_hide_date').datepicker('setDate', date.format('MMMM D, YYYY'));
						$('#item_hide_time').val(date.format('HH:mm'));
					} else {
						$('#item_hide_date').val('');
						$('#item_hide_time').val('');
					}
					$('#item_hide_details').show();
				} else $('#item_hide_details').hide();
			
			} else {

				$('#item_status').prop('checked', false);
				$('.item_detail_wrapper').hide();

			}
		}

		resizeSettingsWrappers();
	}

	var saveItemOptions = function() {
		var $selected = $('#results_menu li.ui-state-active');
		if ($selected.length == 1) {
			var all_options = JSON.parse($('#results_menu').attr('data-json-data'));
			var key = $selected.eq(0).attr('data-item-key');

			var index;
			for (index = 0; index < all_options.length; ++index) {
				if (all_options[index].type == key) break;
			}
			if ($('#item_status').prop('checked')) {

				var newEntry = {
					type: key,
					show: { type: $('#item_show_type').val() },
					hide: { type: $('#item_hide_type').val() }
				};

				if ($('#item_show_type').val() == 'date') {
					if (($('#item_show_date').val().length > 0) && ($('#item_show_time').val().length > 0)) {
						var date = dayjs($('#item_show_date').datepicker('getDate'));
						var timeParts = $('#item_show_time').val().match(/(\d+)/g);
						date = date.hour(timeParts[0]).minute(timeParts[1]).utc();
						newEntry.show.date = date.format('YYYY-MM-DD HH:mm:ss');	
					} else newEntry.show.date = '';
				}

				if ($('#item_hide_type').val() == 'date') {
					if (($('#item_hide_date').val().length > 0) && ($('#item_hide_time').val().length > 0)) {
						var date = dayjs($('#item_hide_date').datepicker('getDate'));
						var timeParts = $('#item_hide_time').val().match(/(\d+)/g);
						date = date.hour(timeParts[0]).minute(timeParts[1]).utc();
						newEntry.hide.date = date.format('YYYY-MM-DD HH:mm:ss');	
					} else newEntry.hide.date = '';
				}

				if (index == all_options.length) all_options.push(newEntry);
				else all_options[index] = newEntry;

			} else if (index < all_options.length) {
				all_options.splice(index, 1);
			}

			$('#results_menu').attr('data-json-data', JSON.stringify(all_options));
		}
	}

	$("#results_menu li").click(function(e) {
		e.preventDefault();

		if (!checkItemOptionDates()) {

			$('#item_show_time').validationEngine('showPrompt', '* Hide date/time must be after show date/time', 'error', 'bottomLeft', true);

		} else {

			if ($('#results_menu li.ui-state-active').length > 0) {
				saveItemOptions();
			}
	
			$("#results_menu li").removeClass("ui-state-active");
			$(this).addClass("ui-state-active");
	
			showItemOptions();	
			
		}
	});

	$('#item_status').on('change', function() {
		var $selected = $('#results_menu li.ui-state-active');
		var all_options = JSON.parse($('#results_menu').attr('data-json-data'));
		var key = $selected.eq(0).attr('data-item-key');

		var index;
		for (index = 0; index < all_options.length; ++index) {
			if (all_options[index].type == key) break;
		}
		if ($('#item_status').prop('checked')) {

			var newEntry = {
				type: key,
				show: { type: 'auto' },
				hide: { type: 'none' }
			};

			if (index == all_options.length) all_options.push(newEntry);
			else all_options[index] = newEntry;

		} else if (index < all_options.length) {
			all_options.splice(index, 1);
		}

		$('#results_menu').attr('data-json-data', JSON.stringify(all_options));

		showItemOptions();
		resizeSettingsWrappers();
	});

	$('#item_show_type').on('change', function() {
		var $selected = $('#results_menu li.ui-state-active');

		saveItemOptions();

		var all_options = JSON.parse($('#results_menu').attr('data-json-data'));
		var key = $selected.eq(0).attr('data-item-key');

		var index;
		for (index = 0; index < all_options.length; ++index) {
			if (all_options[index].type == key) break;
		}
		if (index < all_options.length) {

			all_options[index].show = { 
				type: $('#item_show_type').val() 
			};
			if ($('#item_show_type').val() == 'date') {
				all_options[index].show.date = '';
			}
			$('#results_menu').attr('data-json-data', JSON.stringify(all_options));

		}

		showItemOptions();
		resizeSettingsWrappers();
	});

	$('#item_hide_type').on('change', function() {
		var $selected = $('#results_menu li.ui-state-active');

		saveItemOptions();

		var all_options = JSON.parse($('#results_menu').attr('data-json-data'));
		var key = $selected.eq(0).attr('data-item-key');

		var index;
		for (index = 0; index < all_options.length; ++index) {
			if (all_options[index].type == key) break;
		}
		if (index < all_options.length) {

			all_options[index].hide = { 
				type: $('#item_hide_type').val() 
			};
			if ($('#item_hide_type').val() == 'date') {
				all_options[index].hide.date = '';
			}
			$('#results_menu').attr('data-json-data', JSON.stringify(all_options));

		}

		showItemOptions();
		resizeSettingsWrappers();
	});

	$('#export_csv').on('click', function() {
		var html = '';

		var $labelOptions = $('#student_label_select option');
		for (var i = 0; i < $labelOptions.length; ++i) {
			var label = $labelOptions.eq(i).attr('value');
			var details = {type: label, label: label};
			html += 
				"<div class='csv_select_element' style='grid-column:1; grid-row:" + (i + 1) + ";' data-details='" + htmlSingleQuotes(JSON.stringify(details)) + "'>" + 
					"<input id='csv_input_0_" + i + "' type='checkbox' class='csv_select_input' />" +
					"<label for='csv_input_0_" + i + "' class='csv_select_label'>" + label + "</div>" + 
				"</div>";
		}

		var otherFields = [
			{type: 'attempts', label: 'Attempts'}, 
			{type: 'latest_attempt', label: 'Latest attempt'}, 
			{type: 'best_score', label: 'Best score'}, 
			{type: 'best_score_percent', label: 'Best score (%)'}, 
			{type: 'outcomes_placeholder', label: 'Outcomes (%)'}
		];
		for (var i = 0; i < otherFields.length; ++i) {
			html += 
				"<div class='csv_select_element' style='grid-column:2; grid-row:" + (i + 1) + ";' data-details='" + htmlSingleQuotes(JSON.stringify(otherFields[i])) + "'>" + 
					"<input id='csv_input_1_" + i + "' type='checkbox' class='csv_select_input' />" +
					"<label for='csv_input_1_" + i + "' class='csv_select_label'>" + otherFields[i].label + "</div>" + 
				"</div>";
		}

		$('#include_list').html(html);
		$('#include_list').attr('data-fields', '[]');

		$('#exportCSV_dialog').dialog('open');
		rebuildCSVPreview();
	});

	$(document).on('change', '.csv_select_input', function() {
		var fields = JSON.parse($('#include_list').attr('data-fields'));
		var clicked_data = JSON.parse($(this).closest('.csv_select_element').attr('data-details'));
		if ($(this).prop('checked')) {
			fields.push(clicked_data);
		} else {
			var index = 0;
			do {
				if (fields[index].type == clicked_data.type) {
					fields.splice(index, 1);
				}
			} while (++index < fields.length);
		}
		$('#include_list').attr('data-fields', JSON.stringify(fields));

		rebuildCSVPreview();
	});

	var getCSVEntry = function($row, columnDetails) {
		var value;
		if (columnDetails.type == 'outcome') {
			var $scoreCell = $row.find('.td_score');
			if ($scoreCell.length == 1) {
				var json_string = $scoreCell.attr('data-export');
				var json_data = (json_string.length == 0) ? {} : JSON.parse(json_string);
				if ('outcomes' in json_data) {
					value = '';
					for (var k = 0; k < json_data.outcomes.length; ++k) {
						if (json_data.outcomes[k].name == columnDetails.label) {
							if (json_data.outcomes[k].percentage !== false) {
								value = json_data.outcomes[k].percentage.toFixed(0);
							}
						}
					}
				} else value = '';
			} else value = '';
		} else if (columnDetails.type == 'attempts') {
			var $countCell = $row.find('.td_count');
			if ($countCell.length == 1) value = $countCell.attr('data-export');
			else value = '';
		} else if (columnDetails.type == 'latest_attempt') {
			var $latestCell = $row.find('.td_latest');
			if ($latestCell.length == 1) value = $latestCell.attr('data-export');
			else value = '';
		} else if (columnDetails.type == 'best_score') {
			var $scoreCell = $row.find('.td_score');
			if ($scoreCell.length == 1) {
				var json_string = $scoreCell.attr('data-export');
				var json_data = (json_string.length == 0) ? {} : JSON.parse(json_string);
				value = ('score' in json_data) ? json_data.score : '';	
			} else value = '';
		} else if (columnDetails.type == 'best_score_percent') {
			var $scoreCell = $row.find('.td_score');
			if ($scoreCell.length == 1) {
				var json_string = $scoreCell.attr('data-export');
				var json_data = (json_string.length == 0) ? {} : JSON.parse(json_string);
				value = ('percent' in json_data) ? json_data.percent : '';	
			} else value = '';
		} else {
			var details_string = $row.find('.student_label').attr('data-details');
			var details = (details_string.length == 0) ? {} : JSON.parse(details_string);
			value = getLabel(details, columnDetails.label);
		}

		return value;		
	}

	var rebuildCSVPreview = function() {
		var $summaryRows = $('#summary_rows tr');

		var fields = JSON.parse($('#include_list').attr('data-fields'));

		var columns = [];
		for (var i = 0; i < fields.length; ++i) {
			if (fields[i].type == 'outcomes_placeholder') {
				var allOutcomes = [];
				for (var j = 0; j < $summaryRows.length; ++j) {
					var $scoreCell = $summaryRows.eq(j).find('.td_score');
					if ($scoreCell.length == 1) {
						var json_string = $scoreCell.attr('data-export');
						var json_data = (json_string.length == 0) ? {} : JSON.parse(json_string);
						if ('outcomes' in json_data) {
							for (var k = 0; k < json_data.outcomes.length; ++k) {
								var thisName = json_data.outcomes[k].name;
								if (allOutcomes.indexOf(thisName) == -1) allOutcomes.push(thisName);
							}
						}
					}
				}
				allOutcomes.sort();

				for (var j = 0; j < allOutcomes.length; ++j) {
					columns.push({
						type: 'outcome',
						label: allOutcomes[j]
					});
				}
			} else {
				if (fields[i].type == 'best_score') {
					var outOf = false;
					$summaryRows.each(function() {
						var $scoreCell = $(this).find('.td_score');
						if ($scoreCell.length == 1) {
							var json_string = $scoreCell.attr('data-export');
							var json_data = (json_string.length == 0) ? {} : JSON.parse(json_string);
							if ('outOf' in json_data) {
								if (outOf === false) outOf = json_data.outOf;
								else if (json_data.outOf != outOf) {
									outOf = false;
									return false;
								}
							}
						}
					});
	
					if (outOf !== false) {
						fields[i].label = fields[i].label + ' (/' + outOf + ')';
					}
				}
	
				columns.push(fields[i]);
			}
		}

		var $dialog = $('#csv_layout').closest('.ui-dialog');
		var $download_button = $dialog.find('button:contains("Download")');

		var html = '';
		if (columns.length > 0) {
			$download_button.removeAttr('disabled');

			for (var j = 0; j < columns.length; ++j) {
				html += 
					"<div class='csv_layout_column' data-details='" + htmlSingleQuotes(JSON.stringify(columns[j])) + "'>" + 
						"<div class='csv_layout_element' title='" + columns[j].label + "'>" + columns[j].label + "</div>" +
						"<div class='csv_layout_element'>" + getCSVEntry($summaryRows.eq(0), columns[j]) + "</div>" +
					"</div>";
			}
		} else {
			$download_button.attr('disabled', 'disabled');
			html = '<div class="csv_layout_empty">Select fields above to begin.</div>';
		}

		$('#csv_layout').html(html);
		$('#csv_layout').sortable();
	}

    /* *
     * * initialize dialog box for CSV export
     * */

	var exportCSV = function() {
		var columns = [];
		$('.csv_layout_column').each(function() {
			columns.push(JSON.parse($(this).attr('data-details')));
		});

		var csvData = [];

		var rowData = [];
		for (var i = 0; i < columns.length; ++i) {
			rowData.push(columns[i].label);
		}
		csvData.push(rowData);

		$('#summary_rows tr').each(function() {
			rowData = [];
			for (var i = 0; i < columns.length; ++i) {
				rowData.push(getCSVEntry($(this), columns[i]));
			}
			csvData.push(rowData);
		});

		var csvString = '';
		for (var i = 0; i < csvData.length; ++i) {
			for (var j = 0; j < csvData[i].length; ++j) {
				var string = csvData[i][j].toString();
				string = string.replace(/"/g, '""');
				if (string.search(/("|,|\n)/g) >= 0) string = '"' + string + '"';

				if (j > 0) csvString += ',';
				csvString += string;
			}
			csvString += '\n';
		}

		var pom = document.createElement('a');
		pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(csvString));
		pom.setAttribute('download', 'summary.csv');
	
		if (document.createEvent) {
			var event = document.createEvent('MouseEvents');
			event.initEvent('click', true, true);
			pom.dispatchEvent(event);
		}
		else {
			pom.click();
		}
	
		$('#exportCSV_dialog').dialog('close');
	}
     
    $('#exportCSV_dialog').dialog({
        autoOpen: false,
        width: 450,
        modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			"Cancel": function() {
				$('#exportCSV_dialog').dialog("close");
			},
			"Download": exportCSV
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Download")').addClass('btn btn-save btn_font');

			initSubmitProgress($(this));

			resizeSettingsWrappers();		
			$('.document_settings_tab:first-child').trigger('mousedown');
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });
	
	$('#wr_comment').htmleditor({'height': 'auto'});
	$('#wr_comment').htmleditor('hideElement', 'edit_button_variable');
	$('#wr_comment').htmleditor('hideElement', 'edit_button_blank');

	$('#editComment_editor').htmleditor();
	$('#editComment_editor').htmleditor('hideElement', 'edit_button_variable');
	$('#editComment_editor').htmleditor('hideElement', 'edit_button_blank');

	$('#results_menu').menu();
	
	$(".datepicker").datepicker({dateFormat: "MM d, yy"});

    $('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : "bottomLeft"
    }).submit(function(e){ e.preventDefault() });

	initSubmitProgress($('#sitting_reload_wrapper'));
	initSubmitProgress($('#student_reload_wrapper'));

	$('#sitting_options').on('click', startSittingEdit);

	// Load sitting

	initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});
		window.location.href = '/Sittings';
	});
});
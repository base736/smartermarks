/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

$(document).ready(function() {

    var initialize = async function() {
        // Convert paymentID to zero-padded 6 characters
        const paddedPaymentId = String(paymentID).padStart(6, '0');

        // Format the creation date
        const createdLocal = dayjs.utc(createdUTC).local();
        const createdDate = createdLocal.format('MMMM D, YYYY');

        // Decide if we’re showing RECEIPT or INVOICE
        const receiptTitle = (paymentStatus === 'complete') ? 'RECEIPT' : 'INVOICE';

        // Build line items for the table
        let lineItemsHTML = '';
        let runningTotal = 0.0;

        details.invoice.line_items.forEach(lineItem => {
            const { count, price, description, description_full } = lineItem;
            const usedDescription = description_full && description_full.length > 0
                                    ? description_full
                                    : description;
            const thisAmount = count * price;
            runningTotal += thisAmount;

            lineItemsHTML += `
                <tr>
                    <td>${usedDescription}</td>
                    <td>${count}</td>
                    <td>$${price.toFixed(2)}</td>
                    <td>$${thisAmount.toFixed(2)}</td>
                </tr>
            `;
        });

        // Subtotal row
        const subTotal = runningTotal;
        lineItemsHTML += `
            <tr>
            <td colspan="3" style="text-align:right;">Subtotal</td>
            <td>$${subTotal.toFixed(2)}</td>
            </tr>
        `;

        // Tax
        const taxRate = details.invoice.tax_rate;
        const taxName = details.invoice.tax_name || 'Tax';
        const tax = subTotal * taxRate;
        runningTotal += tax;

        lineItemsHTML += `
            <tr>
            <td colspan="3" style="text-align:right;">${taxName} (${(taxRate * 100).toFixed(0)}%)</td>
            <td>$${tax.toFixed(2)}</td>
            </tr>
        `;

        // Final total from details (in your PHP code you use details['invoice']['total']/100)
        const finalTotal = details.invoice.total / 100;

        // TOTAL / TOTAL DUE row
        lineItemsHTML += `
            <tr>
            <td colspan="3" style="text-align:right; font-weight:bold;">
                ${paymentStatus === 'complete' ? 'TOTAL' : 'TOTAL DUE'}
            </td>
            <td>$${finalTotal.toFixed(2)}</td>
            </tr>
        `;

        // If complete, show PAID row
        if (paymentStatus === 'complete') {
            lineItemsHTML += `
                <tr>
                    <td colspan="3" style="text-align:right; font-weight:bold;">PAID</td>
                    <td>$${finalTotal.toFixed(2)}</td>
                </tr>
            `;
        }

        // Billed to (only if complete)
        let billedToHTML = '';
        if (paymentStatus === 'complete') {
            billedToHTML = `
                <div class="receipt_header">
                    Billed to: ${email}
                </div>
            `;
        }

        const html = `
            <div style="width:100%; display:flex; justify-content:space-between; margin:5px 0px 20px;">
            <div style="flex:none; width:auto;">
                <img src="https://images.smartermarks.com/logo.svg" style="width:350px;" />
            </div>
            <div style="flex:none; width:auto; font-size:36px; line-height:auto; height:auto; font-weight:bold;">
                ${receiptTitle}
            </div>
            </div>

            <div style="display:flex; justify-content:space-between;">
            <div class="receipt_header" style="flex:none; width:auto;">
                ${receiptHeader}
            </div>
            <div style="flex:none; width:auto; text-align:right;">
                <p>Payment ID: ${paddedPaymentId}</p>
                <p>Date: ${createdDate}</p>
            </div>
            </div>

            ${billedToHTML}

            <table class="invoice_table" style="width:100%; box-sizing:border-box; margin:20px 0px;">
            <thead>
                <tr>
                <td>Item</td>
                <td width="18%">Quantity</td>
                <td width="18%">Price</td>
                <td width="18%">Amount</td>
                </tr>
            </thead>
            <tbody>
                ${lineItemsHTML}
            </tbody>
            </table>

            <div class="receipt_footer">
            ${receiptFooter}
            </div>
        `;

        document.getElementById('receipt_content').innerHTML = html;        
    }

/**************************************************************************************************/
/* PDF render stuff                                                                               */
/**************************************************************************************************/

	$('#print_div').on('click', function() {
		navigateWithPost('/build_pdf', {
			pdfType: 'pdf_from_url',
            url: '/Payments/print_receipt/' + paymentID,
            target: 'receipt.pdf'
		});
	});

	// Load receipt data

	initialize()
    .then(() => {

        $('body').append('<span class="page_loaded"></span>');

    }).catch((error) => {

        if (isPrint) {

            $('body').html(
                '<div>Error: ' + error.message + '</div> \
                <span class="page_loaded"></span>');
    
        } else {

            addClientFlash({
                title: "Error",
                text: error.message,
                image: "/img/error.svg"
            });
            window.location.href = '/Documents';
    
        }

	});
});

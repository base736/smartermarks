/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { EditTableTool } from './tools/EditTableTool.js?7.0';
import { EditImageTool } from './tools/EditImageTool.js?7.0';
import { EditDrawingTool } from './tools/EditDrawingTool.js?7.0';

function removeComments($element) {
    $element.contents().each(function() {
        if (this.nodeType === Node.COMMENT_NODE) $(this).remove();
        else if (this.nodeType === Node.ELEMENT_NODE) removeComments($(this));
    });
}

function cleanHTML(html) {

    var allowed_tags = [
        'div', 'span',
        'br', 
        'b', 'i', 'u', 'sup', 'sub', 
        'ul', 'ol', 'li', 
        'img', 
        'table', 'tbody', 'tr', 'td',
        'svg', 'path'
    ];

    var delete_selectors = [
        'script', 'style', 
        'textarea', 'button', 'fieldset', 
        'del', 
        'mjx-container', 'c-wiz', 'g-dropdown-menu',
        'svg:not(.annotation_wrapper)'
    ];

    var replace_tags = [
        { regex: /^p$/, target: 'div' },
        { regex: /^h[0-9]$/, target: 'div' },
        { regex: /^center$/, target: 'div' }, 
        { regex: /^blockquote$/, target: 'div' }, 
        { regex: /^header$/, target: 'div' }, 
        { regex: /^footer$/, target: 'div' }, 
        { regex: /^caption$/, target: 'div' }, 
        { regex: /^legend$/, target: 'div' }, 
        { regex: /^var$/, target: 'i' },
        { regex: /^strong$/, target: 'b' },
        { regex: /^em$/, target: 'i' }  
    ];

    var allowed_attributes = {
        'div': ['class', 'style', 'data-json'],
        'span': ['class', 'style', 'data-variable-name'],
        'ul': ['class', 'style'],
        'ol': ['style'],
        'img': ['style', 'src'], 
        'table': ['style'],
        'td': ['class', 'style', 'colspan', 'width', 'valign'],
        'svg':  ['class', 'data-tool-name', 'viewBox', 'preserveAspectRatio'],
        'path': ['stroke', 'stroke-width', 'd']
    };

    var allowed_classes = {
        'div': ['equation', 'code_container', 'html_drawing'],
        'span': ['block_variable', 'block_bigBlank'],
        'ul': ['ul_checklist'],
        'td': ['td_top', 'td_middle', 'td_bottom'],
        'svg': ['annotation_wrapper']
    };

    var allowed_styles = {
        'div': ['text-align', 'font-weight', 'font-style', 'width', 'aspect-ratio'],
        'span': ['font-weight', 'font-style'],
        'ul': ['list-style-type'],
        'ol': ['list-style-type'],
        'img': ['width'],
        'table': ['width', 'margin', 'margin-left', 'margin-right'],
        'td': ['text-align', 'vertical-align', 'font-weight', 'font-style', 'width', 'border-style',
            'border-left-style', 'border-right-style', 'border-top-style', 'border-bottom-style']
    };

    var strip_empty = [
        'div', 'span', 
        'b', 'i', 'u', 'sup', 'sub', 
        'ul', 'ol'
    ];

    var strip_nested = [
        'div', 'span'
    ];

    // Insert HTML into a temporary jquery element

    var $temp = $('<div></div>');
    $temp.append(html);
    $temp[0].normalize();
    
    // Strip comments

    removeComments($temp);

    // Delete disallowed elements as required

    const deleteSelector = delete_selectors.join(',');
    $temp.find(deleteSelector).each(function() {
        $(this).remove();
    });

    // Replace disallowed elements as required

    $temp.find('*').each(function() {
        var nodeName = this.nodeName.toLowerCase();
        for (var i = 0; i < replace_tags.length; ++i) {
            if (nodeName.match(replace_tags[i].regex)) {
                var $newElement = $('<' + replace_tags[i].target + '></' + replace_tags[i].target + '>');

                $.each(this.attributes, function(i, attrib) {
                    $newElement.attr(attrib.name, attrib.value);
                });

                var $targetElement = $newElement;
                if ((nodeName == 'h1') || (nodeName == 'h2') || (nodeName == 'h3')) {
                    $targetElement = $('<b></b>');
                    $newElement.append($targetElement);
                }

                $newElement.append($(this).html());

                $(this).replaceWith($newElement);

                if (nodeName == 'blockquote') {
                    $newElement.after('<div><br/></div>');
                } else if (nodeName == 'center') {
                    $newElement.attr('style', 'text-align:center;');
                }
            }
        }
    });

    // Clean attributes, classes, and styles

    $temp.find('*').each(function() {
        var nodeName = this.nodeName.toLowerCase();

        // Skip elements that we'll be unwrapping later. We have to clean attributes first because this 
        // may create spans with no style or class, which are unwrapped below.

        if (allowed_tags.indexOf(nodeName) == -1) return;

        var removeAttributes = [];
        for (var i = 0; i < this.attributes.length; ++i) {
            var attributeName = this.attributes[i].name.toLowerCase();
            
            if (attributeName == 'align') {
                var style = this.hasAttribute('style') ? $(this).attr('style') : '';
                style += 'text-align:' + this.attributes[i].value + ';';
                $(this).attr('style', style);
            }

            if (!(nodeName in allowed_attributes) || (allowed_attributes[nodeName].indexOf(attributeName) == -1)) {
                removeAttributes.push(attributeName);
            }
        }

        for (var i = 0; i < removeAttributes.length; ++i) {
            $(this).removeAttr(removeAttributes[i]);
        }

        var classes = this.hasAttribute('class') ? $(this).attr('class').split(/\s+/) : [];
        if ((classes.indexOf('block_variable') >= 0) || (classes.indexOf('block_bigBlank') >= 0)) {

            $(this).removeAttr('style');

        } else {

            // Remove disallowed styles

            var newStyles = '';
            if (this.hasAttribute('style')) {
                var styles = $(this).attr('style').split(';');
                for (var i = 0; i < styles.length; ++i) {
                    var elements = styles[i].split(':');
                    var styleName = elements[0].trim();
                    if ((nodeName in allowed_styles) && (allowed_styles[nodeName].indexOf(styleName) >= 0)) {
                        var styleValue = elements[1].trim();
                        newStyles += styleName + ':' + styleValue + ';';
                    }
                }
            }

            if (newStyles.length > 0) $(this).attr('style', newStyles);
            else $(this).removeAttr('style');

            // Remove disallowed classes

            var newClasses = '';
            for (var i = 0; i < classes.length; ++i) {
                var className = classes[i].trim();
                if ((nodeName in allowed_classes) && (allowed_classes[nodeName].indexOf(className) >= 0)) {
                    if (newClasses.length > 0) newClasses += ' ';
                    newClasses += className;
                }
            }

            if (newClasses.length > 0) $(this).attr('class', newClasses);
            else $(this).removeAttr('class');
        }

    });

    // Unwrap disallowed elements as required

    $temp.find('*').each(function() {
        var nodeName = this.nodeName.toLowerCase();
    
        var shouldUnwrap = (allowed_tags.indexOf(nodeName) == -1);
        if ((nodeName == 'span') && !this.hasAttribute('style') && 
            !this.hasAttribute('class')) shouldUnwrap = true;

        if (shouldUnwrap) {
            if (($(this).contents().length == 1) && ($(this).contents()[0].nodeType == Node.TEXT_NODE)) {
                
                var elementText = $(this).contents()[0].nodeValue;

                if (this.previousSibling && (this.previousSibling.nodeType == Node.TEXT_NODE) &&
                    this.previousSibling.nodeValue.match(/[a-zA-Z]$/)) {
                    elementText = ' ' + elementText;
                }

                if (this.nextSibling && (this.nextSibling == Node.TEXT_NODE) &&
                    this.nextSibling.nodeValue.match(/^[a-zA-Z]/)) {
                    elementText = elementText + ' ';
                }

                $(this).replaceWith(elementText);

            } else if ($(this).contents().length > 0) {

                $(this).replaceWith(this.childNodes);

            } else $(this).remove();
        }
    });

    // Remove redundant elements

    var changed;
    do {
        changed = false;

        $temp.find(strip_empty.join(',')).each(function() {
            if (!this.hasAttribute('class') && ($(this).contents().length == 0)) {

                $(this).remove();
                changed = true;

            }
        });

        $temp.find(strip_nested.join(',')).each(function() {
            if (($(this).contents().length == 1) && ($(this).contents()[0].nodeName == this.nodeName) &&
                (!this.hasAttribute('class') || !$(this).contents()[0].hasAttribute('class'))) {

                // Unwrap divs and spans containing only one child, which is of the same type and for which either
                // parent or child has no classes.

                var $child = $(this).contents().eq(0);

                // Merge styles

                var merged_styles = {};

                if (this.hasAttribute('style')) {
                    var this_styles = $(this).attr('style').split(';');
                    for (var i = 0; i < this_styles.length; ++i) {
                        var elements = this_styles[i].split(':');
                        if (elements.length == 2) {
                            merged_styles[elements[0].trim()] = elements[1].trim();
                        }
                    }    
                }

                if ($child[0].hasAttribute('style')) {
                    var child_styles = $child.attr('style').split(';');
                    for (var i = 0; i < child_styles.length; ++i) {
                        var elements = child_styles[i].split(':');
                        if (elements.length == 2) {
                            merged_styles[elements[0].trim()] = elements[1].trim();
                        }
                    }    
                }

                var newStyles = '';
                for (var key in merged_styles) {
                    newStyles += key + ':' + merged_styles[key] + ';';
                }

                if (newStyles.length > 0) $child.attr('style', newStyles);
                else $child.removeAttr('style');

                // Merge classes

                if (this.hasAttribute('class')) {
                    $child.attr('class', $(this).attr('class'));
                }

                // Replace element with child

                $(this).replaceWith($child);
                changed = true;
            }
        });

    } while (changed);

    // Normalize again

    $temp[0].normalize();

    // Return HTML

    return $temp.html();
}

function isOrContainsNode(ancestor, descendant) {
    var node = descendant;
    while (node) {
        if (node === ancestor) return true;
        node = node.parentNode;
    }
    return false;
}

function insertHTML(html, $container) {
    const containerNode = $container[0];

    // Remove placeholder <br> if that's all that's in the containerNode

    if (
        containerNode.childNodes.length === 1 &&
        containerNode.childNodes[0].nodeName === 'BR'
    ) {
        containerNode.removeChild(containerNode.childNodes[0]);
    }

    // Insert the given HTML
  
    var sel = window.getSelection();
    if (sel.getRangeAt && sel.rangeCount) {
        var el = document.createElement("div");
        el.innerHTML = html;
        var frag = document.createDocumentFragment();
        var node, lastNode;
        while ((node = el.firstChild)) {
            lastNode = frag.appendChild(node);
        }

        var range = sel.getRangeAt(0);
        if (isOrContainsNode(containerNode, range.commonAncestorContainer)) {
            range.deleteContents();
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        } else {
            containerNode.appendChild(frag);
        }
    }
}

var uneditable = '.resizer_left, .resizer_right, .block_variable, .block_bigBlank, .html_drawing';
var selectable = '.block_variable, .block_bigBlank, .equation, .img_resize_wrapper';
var needsHighlight = '.block_variable, .block_bigBlank, .equation';  // Must be a subset of selectable

function undecorateContent($target) {
	$target.find(selectable).css('backgroundColor', '');
	$target.find(selectable).each(function() {
		if (this.hasAttribute('style') && ($(this).attr('style').length == 0)) {
			$(this).removeAttr('style');
		}
	});

	$target.find('.edit_resize_wrapper').remove();
    EditTableTool.undecorate($target);
    EditImageTool.undecorate($target);
    EditDrawingTool.undecorate($target);
	
	$target.find(uneditable).removeAttr('contenteditable').removeAttr('unselectable');
	$target.find('.equation').removeAttr('contenteditable');	
}

function decorateContent($target) {
    EditTableTool.decorate($target);	
    EditImageTool.decorate($target);	
    EditDrawingTool.decorate($target);	
	$target.find(uneditable).attr('contenteditable', 'false').attr('unselectable', 'on');
	$target.find('.equation').attr('contenteditable', 'false');
}

function updateSelection() {
    var sel = window.getSelection();
    var range = (sel.rangeCount > 0) ? sel.getRangeAt(0) : null;

    if (range) {
        $(needsHighlight).each(function() {
            var elementRange = document.createRange();
            elementRange.selectNode(this);

            var startCompare = range.compareBoundaryPoints(Range.START_TO_START, elementRange);
            var endCompare   = range.compareBoundaryPoints(Range.END_TO_END,   elementRange);

            if (startCompare <= 0 && endCompare >= 0) {
                $(this).css('backgroundColor', 'Highlight');
            } else {
                $(this).css('backgroundColor', '');
            }
        });
    } else {
        $(needsHighlight).css('backgroundColor', '');
    }
}

function newVariableName(variables) {    

    // Shuffle available characters

    var variableCharacters = "abcdefghijklmnopqrstuvwxyz".split("");
    for (var i = variableCharacters.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = variableCharacters[i];
        variableCharacters[i] = variableCharacters[j];
        variableCharacters[j] = temp;
    }

    // Get existing names

    var variableNames = ['e', 't'];
    for (var type in variables) {
        if (variables.hasOwnProperty(type)) {
            for (var i = 0; i < variables[type].length; ++i) {
                variableNames.push(variables[type][i].variableName);
            }
        }
    }

    // Find a new, valid variable name that's as short as possible

    var index = -1;
    var variableName = null;
    var newVariable = false;
    do {
        var temp = ++index;
        variableName = '';
        do {
            var i = temp % variableCharacters.length;
            variableName = variableName + variableCharacters[i];
            temp = Math.floor(temp / variableCharacters.length);
        } while (temp > 0);

        newVariable = true;
        if (variableNames.indexOf(variableName) >= 0) newVariable = false;
        else {
            try {
                eval('var ' + variableName + ';'); 
            } catch (e) {
                newVariable = false;
            }		
        }
    } while (!newVariable);
    
    return variableName;
}

function getTableWidths($table) {
    if (!$table[0].hasAttribute('data-widths')) {
        // Recreate data-widths from scratch
        let tableWidthPx = parseFloat($table.width());
        let firstRow = true;
        let columnInfo = [];
        $table.find('tr').each(function() {
            let index = 0;
            $(this).find('td').each(function() {
                let colsp = this.hasAttribute('colspan') ? parseInt($(this).attr('colspan')) : 1;
                let w = $(this).outerWidth() / tableWidthPx;
                let thisInfo = { colspan: colsp, width: w };

                if (firstRow) {
                    columnInfo.push(thisInfo);
                } else {
                    while (thisInfo) {
                        if (thisInfo.colspan === columnInfo[index].colspan) {
                            thisInfo = null;
                            index++;
                        } else if (thisInfo.colspan < columnInfo[index].colspan) {
                            columnInfo.splice(index, 0, {
                                colspan: thisInfo.colspan,
                                width: thisInfo.width
                            });
                            columnInfo[index + 1].colspan -= thisInfo.colspan;
                            columnInfo[index + 1].width -= thisInfo.width;
                            thisInfo = null;
                            index++;
                        } else {
                            thisInfo.colspan -= columnInfo[index].colspan;
                            thisInfo.width -= columnInfo[index].width;
                            index++;
                        }
                    }
                }
            });
            firstRow = false;
        });

        let percentWidths = [];
        let totalWidth = 0.0;
        for (let i = 0; i < columnInfo.length - 1; i++) {
            const colsp = columnInfo[i].colspan;
            let w = (100.0 * columnInfo[i].width) / colsp;
            for (let j = 0; j < colsp; j++) {
                let rounded = w.toFixed(3);
                percentWidths.push(rounded);
                totalWidth += parseFloat(rounded);
            }
        }

        // Last one gets the remainder
        percentWidths.push((100.0 - totalWidth).toFixed(3));
        $table.attr('data-widths', JSON.stringify(percentWidths));
    }

    let widths = JSON.parse($table.attr('data-widths'));
    for (let i = 0; i < widths.length; i++) {
        widths[i] = parseFloat(widths[i]) / 100.0;
    }
    return widths;
}

function setTableWidths($table, newWidths) {
    let percentWidths = [];
    let totalWidth = 0.0;
    for (let i = 0; i < newWidths.length - 1; i++) {
        let rounded = (100.0 * newWidths[i]).toFixed(3);
        percentWidths.push(rounded);
        totalWidth += parseFloat(rounded);
    }
    percentWidths.push((100.0 - totalWidth).toFixed(3));
    $table.attr('data-widths', JSON.stringify(percentWidths));

    // Apply to each <td>
    $table.find('tr').each(function() {
        let index = 0;
        $(this).find('td').each(function() {
            let colsp = this.hasAttribute('colspan') ? parseInt($(this).attr('colspan')) : 1;
            let wSum = 0.0;
            for (let i = 0; i < colsp; i++) {
                wSum += newWidths[index++];
            }
            $(this).css('width', (100.0 * wSum).toFixed(3) + '%');
        });
    });
}

function getSelectionDetails() {
    const selection = window.getSelection();
    var rangeDetails = [];
    if (selection) {
        for (let i = 0; i < selection.rangeCount; i++) {
            rangeDetails.push({
                startContainer: selection.getRangeAt(i).startContainer,
                startOffset: selection.getRangeAt(i).startOffset,
                endContainer: selection.getRangeAt(i).endContainer,
                endOffset: selection.getRangeAt(i).endOffset
            });
        }
    }

    return rangeDetails;
}

function restoreSelectionDetails(rangeDetails) {
    const selection = window.getSelection();
    selection.removeAllRanges();

    for (const details of rangeDetails) {
        if (!details.startContainer.isConnected) continue;
        if (!details.endContainer.isConnected) continue;
        
        try {
            const newRange = document.createRange();
            newRange.setStart(details.startContainer, details.startOffset);
            newRange.setEnd(details.endContainer, details.endOffset);
            selection.addRange(newRange);
        } catch (error) {
            // Skip this range
        }    
    }
    return true;
}

export { 
    cleanHTML, 
    insertHTML, 
    selectable, 
    decorateContent, undecorateContent,
    updateSelection,
    newVariableName,
    getTableWidths, setTableWidths,
    getSelectionDetails, restoreSelectionDetails
};
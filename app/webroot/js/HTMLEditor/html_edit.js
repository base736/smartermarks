/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as EqnEdit from '/js/EquationEditor/eqn_edit.js?7.0';
import * as HTMLEditorUtils from './utils.js?7.0';

import { createTools } from './tools/index.js?7.0';
import { createMenus } from './menus/index.js?7.0';

var htmlEditInitializing = false;
var htmlEditInitialized = false;

var internalClipboard = null;

var mouseDown = false;

var commonContext = { variables: {} };
function setCommonContext(newContext) {
    Object.keys(commonContext).forEach((key) => {
        delete commonContext[key];
    });
    Object.keys(newContext).forEach((key) => {
        commonContext[key] = newContext[key];
    });
}

function getEditorVariables() {
	return commonContext.variables;
}

async function initialize() {
    if (htmlEditInitializing) return;
    htmlEditInitializing = true;

    await RenderTools.initialize();
    await EqnEdit.initialize();

    $.widget( "sm.htmleditor", {
    
        _savedStates: [],
        _savedStateIndex: -1,

        _commandMode: false,
        _commandText: "",

        _suppressInactive: false,

        _toolTarget: null,
        _selectedCells: null,

        // Default options

        options: {
            height: 'fixed',
            scrollWidth: 20,
            statusHeight: 20,
            labelTypes: [],
            defaultVariableType: 'randomized'
        },
    
        _create: function() {

            const context = {
                common: commonContext,
                editor: {
                    $element: this.element
                }
            };

            this._tools = createTools(context);
            this._menus = createMenus(context);

            var $parent = $("<div class='edit_parent' style='flex:1 1 100%; width:100%; display:flex; flex-direction:column;'></div>");
            $parent.insertBefore(this.element);
            $parent.tabindex = -1; // Enable focus on parent

            var overflow_y = (this.options.height == 'fixed') ? 'scroll' : 'hidden';

            // Set up toolbar

            $parent.css('margin', this.element.css('margin'));
            this.element.css('margin', '0px');

            var $bar = $("<div class='edit_bar' style='flex:0 1 auto;'></div>");
            $parent.append($bar);
            $parent.append(this.element);
            
            var $status = $("<div class='command_status' style='flex:0 1 " + this.options.statusHeight + "px; background-color:white; font-size:10px; padding:0px 10px;'></div>");
            $parent.append($status);
                        
            this.element.addClass('html_editor');
            this.element.attr('contenteditable', 'true');
            this.element.attr('spellcheck', 'true');
            this.element.css('overflow-x', 'hidden');
            this.element.css('overflow-y', overflow_y);

            $bar.append("<div style='float:left;'></div><div style='float:right;'></div>");
            var $barLeft = $bar.children().eq(0);
            var $barRight = $bar.children().eq(1);
            
            var $section = $("<div class='edit_bar_section edit_bar_section_font'></div>");
            this._addTool($section, 'formatRemove');
            this._addTool($section, 'formatBold');
            this._addTool($section, 'formatItalic');
            this._addTool($section, 'formatUnderline');
            this._addTool($section, 'formatSubscript');
            this._addTool($section, 'formatSuperscript');
            $barLeft.append($section);

            $section = $("<div class='edit_bar_section edit_bar_section_alignment'></div>");
            this._addMenu($section, 'changeAlignment');
            $barLeft.append($section);

            $section = $("<div class='edit_bar_section edit_bar_section_lists'></div>");
            this._addMenu($section, 'addUnorderedList');
            this._addMenu($section, 'addOrderedList');
            $barLeft.append($section);
                
            $section = $("<div class='edit_bar_section edit_bar_section_special'></div>");
            this._addMenu($section, 'insertVariable');
            this._addMenu($section, 'insertBlank');
            this._addMenu($section, 'insertTable');
            this._addTool($section, 'insertImage');
            this._addTool($section, 'insertDrawing');
            this._addTool($section, 'insertEquation');
            this._addMenu($section, 'insertSymbol');
            this._addTool($section, 'insertCode');
            $barLeft.append($section);

            $section = $("<div class='edit_bar_section'></div>");
            this._addTool($section, 'settingsEmpty');
            $barRight.append($section);
                            
            // Initialize menus
            
            var widget = this;

            // Set up shortcuts

            var $menuTargets = $bar.find('.edit_button_symbol');
            $menuTargets.find('.edit_button_menu').each(function() {
                const $menuButton = $(this).closest('.edit_button');
                const menuID = $menuButton.attr('data-menu-id');
                widget._menus[menuID].setMenu($(this));    
            });

            widget._buttonShortcuts = {};
            $bar.find('.edit_button, .edit_menu_button, .symbol_cell').each(function() {
                if (this.hasAttribute('data-shortcut')) {
                    const shortcutText = $(this).attr('data-shortcut').substr(1);
                    widget._buttonShortcuts[shortcutText] = $(this);    
                }
            });

            // Set up keyboard events
            
            $parent.on('keydown', function(e) {
                if ($parent.hasClass('inactive')) return;			
                if (widget._handleKeydown(widget, e)) e.preventDefault();
            });

            $parent.on('keyup', function(e) {
                if ($parent.hasClass('inactive')) return;

                if (!(e.ctrlKey || e.metaKey)) {
                    var isPrintable = 
                        (e.keyCode > 47 && e.keyCode < 58)   || // number keys
                        e.key === ' ' || e.key === 'Enter'   || // spacebar & return key(s) (if you want to allow carriage returns)
                        (e.keyCode > 64 && e.keyCode < 91)   || // letter keys
                        (e.keyCode > 95 && e.keyCode < 112)  || // numpad keys
                        (e.keyCode > 185 && e.keyCode < 193) || // ;=,-./` (in order)
                        (e.keyCode > 218 && e.keyCode < 223);   // [\]' (in order)
                    var isDelete = (e.key === 'Backspace') || (e.key === 'Delete');
                    if (isPrintable || isDelete) widget._pushSavedState();
                }
            });
            
            // Single-click selection for some elements
                
            const click_targets = HTMLEditorUtils.selectable;
            this.element.on('click', click_targets, function(e) {

                // Skip this click if a parent or higher also matches
                if ($(this).closest(click_targets).length > 1) return true;

                const range = document.createRange();
                range.selectNode(this);
                
                const selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
    
                HTMLEditorUtils.updateSelection();    
            });

            // Double-click to open settings for some elements

            const dblclick_targets = 'img, .equation, .block_variable';
            this.element.on('dblclick', dblclick_targets, function(e) {

                const $settingsButton = $parent.find('.edit_button_settings');

                var toolID;
                if ($(this).is('img')) {
                    toolID = 'editImage';
                    widget._toolTarget = $(this);
                } else if ($(this).is('.equation')) {
                    toolID = 'editEquation';
                    widget._toolTarget = $(this);    
                } else if ($(this).is('.block_variable')) {
                    toolID = 'editVariable';
                    widget._toolTarget = $(this);    
                }

                $settingsButton.removeClass('disabled');
                $settingsButton.attr('data-tool-id', toolID);
                widget._runTool($settingsButton);
            });

            // Actions to show tooltip when mouse is over a toolbar button
                
            $parent.on('mouseenter', '.edit_button', function(e) {
                $parent.find('.edit_title').remove();
                
                if ($(this).attr("data-show-title") == 'true') {
                    $parent.append(
                        "<div class='edit_title'> \
                            <div class='edit_titleArrow'></div> \
                            <div class='edit_titleText'>" + $(this).data("text") + "</div> \
                        </div>"
                    );
                    
                    var offset = 0;
                    if (this.hasAttribute('data-title-offset')) {
                        offset = parseFloat($(this).data("title-offset"));
                        var $arrow = $parent.find('.edit_titleArrow');
                        $arrow.css('position', 'relative').css('left', -offset);
                    }
                            
                    var $title = $parent.find('.edit_title');
                    $title.css('left', $(this).position().left + $(this).outerWidth() / 2 - $title.outerWidth() / 2 + offset);
                    $title.css('top', $(this).position().top + $(this).outerHeight() + 2);
                    $title.show();
                }
            });

            $parent.find('.edit_button').mouseleave(function(e) {
                $parent.find('.edit_title').remove();
            });
            
            // Hide all menus when the main view is clicked
            
            this.element.click(function() {
                widget._hideMenu($parent.find('.edit_button'));
            });

            // Actions for toolbar button click

            $parent.on('click', '.edit_button, .edit_menu_button', function(e) {
                if ($(this).hasClass('disabled')) return;

                e.preventDefault();
                e.stopPropagation();

                var $button = $(this);
                    
                // Process the command that was clicked

                if (this.hasAttribute('data-tool-id')) {

                    $parent.find('.edit_button').each(function() {
                        widget._hideMenu($(this));
                    });
    
                    widget._runTool($button);

                } else if (this.hasAttribute('data-menu-id')) {

                    const menuID = this.getAttribute('data-menu-id');

                    if ($button.find('.edit_button_menu').is(":visible")) {

                        widget._hideMenu($button);

                    } else {

                        $parent.find('.edit_button').each(function() {
                            widget._hideMenu($(this));
                        });
        
                        const $menuTarget = $button.find('.edit_button_menu');
                        if (menuID == 'insertVariable') {
                            widget._menus[menuID].setMenu($menuTarget, widget.options.defaultVariableType);
                        } else if (menuID == 'insertBlank') {
                            widget._menus[menuID].setMenu($menuTarget, widget.options.labelTypes);
                        } else {
                            widget._menus[menuID].setMenu($menuTarget);
                        }

                        // Check whether the menu has just one possible option

                        const $menuButtons = $menuTarget.find('.edit_menu_button');
                        if ($menuButtons.length == 1) {
                            widget._runTool($menuButtons.eq(0));
                        } else widget._showMenu($button);
                    }

                }
            });

            this.element.on('copy cut', function(e) {
                var sel = window.getSelection();
                if (sel.rangeCount > 0) {
                    var range = sel.getRangeAt(0);

                    var tempDiv = document.createElement('div');
                    tempDiv.appendChild(range.cloneContents());

                    internalClipboard = tempDiv.innerHTML;

                    // Get the common ancestor container for the selection.
                    // It's often a text node, so if it's not an element node, move up to its parent.
                    var parentElement = range.commonAncestorContainer;
                    if (parentElement.nodeType !== Node.ELEMENT_NODE) {
                        parentElement = parentElement.parentNode;
                    }

                    // Check if this was a partial copy of a list. If so, we need to add the list back in,
                    // otherwise we just get a bunch of naked list elements.
                    if ($(parentElement).is('ol, ul')) {
                        var $parentCopy = $(parentElement).clone();
                        $parentCopy.html(internalClipboard);
                        internalClipboard = $parentCopy[0].outerHTML;
                    }
                }
            });

            this.element.on('paste', function(e) {
                e.preventDefault();
                
                // Get pasted elements
                
                var clipboardData = e.originalEvent.clipboardData || window.clipboardData;
                var types = clipboardData.types;

                var newHTML = "";
                if (internalClipboard !== null) {
                    newHTML = internalClipboard;
                } else if (clipboardData.types) {
                    if (types.includes("text/html") && (clipboardData.getData('text/html').length > 0)) {
                        newHTML = clipboardData.getData('text/html');
                    } else if (types.includes('Files')) {
                        if (clipboardData.files.length === 1) {
                            const file = clipboardData.files[0];
                            if (file.type === 'image/png' || file.type === 'image/jpeg') {
                                e.preventDefault();
                                const insertImageTool = widget._tools['insertImage'];
                                insertImageTool.apply({ imageFile: file });
                                return; 
                            }
                        }
                    } else {
                        var lines = clipboardData.getData('text/plain').split('\n');
                        for (var i = 0; i < lines.length; ++i) {
                            if (lines[i].length == 0) lines[i] = '<br>';
                            newHTML += "<div>" + lines[i] + "</div>";
                        }
                    }
                } else {
                    var lines = clipboardData.getData('Text').split('\n');
                    for (var i = 0; i < lines.length; ++i) {
                        if (lines[i].length == 0) lines[i] = '<br>';
                        newHTML += "<div>" + lines[i] + "</div>";
                    }
                }
                
                // Collapse extra whitespace

                newHTML = newHTML.replace(/\s+/g, " ");

                // Insert content and undecorate

                var $target = widget.element;
                HTMLEditorUtils.insertHTML(newHTML, $target);

                RenderTools.unrenderEquations($target);
                HTMLEditorUtils.undecorateContent($target);
                
                /********************************************************************************************************
                * Next parts have to happen after the content is inserted into the target, so that relative size is     *
                * calculated relative to the target's size.                                                             *
                ********************************************************************************************************/
                
                // Strip extraneous styles from tables and change to relative widths

                $target.find('table').each(function() {
                    
                    var tableMarginLeft = ($(this).css('margin-left').length == 0) ? 0.0 : parseFloat($(this).css('margin-left'));
                    var tableMarginRight = ($(this).css('margin-right').length == 0) ? 0.0 : parseFloat($(this).css('margin-right'));
                    var tableWidth = parseFloat($(this).width());
                    
                    var totalWidth = tableMarginLeft + tableMarginRight + tableWidth;
                    $(this).css('margin-left', (100 * tableMarginLeft / totalWidth).toFixed(3) + '%');
                    $(this).css('margin-right', (100 * tableMarginRight / totalWidth).toFixed(3) + '%');
                    $(this).css('width', (100 * tableWidth / totalWidth).toFixed(3) + '%');
                    
                    $(this).find('tbody').removeAttr('style');
                    
                    var $rows = $(this).find('tr');
                    $rows.removeAttr('style');
                    $rows.find('td').each(function() {
                        // First get border styles
                        let borderLeftStyle   = $(this).css('border-left-style');
                        let borderRightStyle  = $(this).css('border-right-style');
                        let borderTopStyle    = $(this).css('border-top-style');
                        let borderBottomStyle = $(this).css('border-bottom-style');

                        // Then get border widths
                        const borderLeftWidth   = parseFloat($(this).css('border-left-width'))   || 0;
                        const borderRightWidth  = parseFloat($(this).css('border-right-width'))  || 0;
                        const borderTopWidth    = parseFloat($(this).css('border-top-width'))    || 0;
                        const borderBottomWidth = parseFloat($(this).css('border-bottom-width')) || 0;

                        // If width is zero, treat it as "no border"
                        if (borderTopWidth === 0)    borderTopStyle    = 'none';
                        if (borderRightWidth === 0)  borderRightStyle  = 'none';
                        if (borderBottomWidth === 0) borderBottomStyle = 'none';
                        if (borderLeftWidth === 0)   borderLeftStyle   = 'none';
                    
                        // Likewise handle alignment
                        let textAlign    = $(this).css('text-align');
                        let verticalAlign = $(this).css('vertical-align');
                    
                        // Remove style/valign so we can reapply in controlled manner
                        $(this).removeAttr('style width valign');
                    
                        // Map 'vertical-align' to classes
                        if (verticalAlign === 'top') {
                            $(this).addClass('td_top');
                        } else if (verticalAlign === 'middle') {
                            $(this).addClass('td_middle');
                        } else if (verticalAlign === 'bottom') {
                            $(this).addClass('td_bottom');
                        } else {
                            $(this).addClass('td_middle');
                        }
                    
                        // Now set the new style with any 0-width sides forced to "none"
                        $(this).css('border-style',
                            borderTopStyle + ' ' +
                            borderRightStyle + ' ' +
                            borderBottomStyle + ' ' +
                            borderLeftStyle
                        );
                        $(this).css('text-align', textAlign);
                    });
                });

                // Strip extraneous styles from images and change to relative widths

                $target.find('img').each(function() {
        
                    if ($(this).attr('src').indexOf(userdataPrefix) == -1) {
                        $(this).replaceWith("<div>[Local image]</div>");
                    } else {
                        var imageWidth = parseFloat($(this).width());
                        var parentWidth = parseFloat($(this).parent().width());
                        $(this).removeAttr('style');
                        $(this).css('width', (100 * imageWidth / parentWidth).toFixed(3) + '%');
                    }	
                    
                });
                
                // Clean disallowed elements from target HTML

                var newHTML = $target.html();
                newHTML = HTMLEditorUtils.cleanHTML(newHTML);
                $target.html(newHTML);
                
                // Re-render math and add resizing handles for tables back

                HTMLEditorUtils.decorateContent($target);
                RenderTools.updateVariableNames($target, commonContext.variables);
                RenderTools.renderEquations($target)
                .then(function() {
                    widget._pushSavedState();
                });

                $(this).trigger('afterPaste');
            });
        },

        _destroy: function() {
            this.element.off();
            var $parent = this.element.closest('.edit_parent');

            this.element.css('width', $parent.css('width'));
            this.element.css('margin', $parent.css('margin'));

            this.element.removeClass('html_editor');
            this.element.removeAttr('contenteditable', 'true');
            this.element.removeAttr('spellcheck', 'true');
            this.element.css('overflow-x', '');
            this.element.css('overflow-y', '');
            this.element.css('cursor', '');

            this.element.insertBefore($parent);

            $parent.remove();
        },

        _hideMenu: function($button) {
            $button.find('.edit_button_menu').hide();
            $('.edit_button').attr('data-show-title', 'true');
            $button.removeClass('selected');
        },
        
        _showMenu: function($button) {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.edit_title').remove();

            $('.edit_button').attr('data-show-title', 'false');
            $button.addClass('selected');
            $button.find('.edit_button_menu').show();
        },

        _addMenu: function($target, menuID) {
            const menu = this._menus[menuID];
            var menuButtonHTML = "<div style='position:relative' class='edit_button";
            if ('iconClass' in menu) menuButtonHTML += " " + menu.iconClass;
            menuButtonHTML += "'";
            if ('label' in menu) menuButtonHTML += " data-text='" + menu.label + "'";
            if ('titleOffset' in menu) menuButtonHTML += " data-title-offset='" + menu.titleOffset + "'";
            menuButtonHTML += " data-menu-id='" + menuID + "'";
            menuButtonHTML += " data-show-title='true'>";
            menuButtonHTML += "<div class='edit_button_icon'></div>";
            menuButtonHTML += "<div class='edit_button_menu' style='display:none;'></div>";
            menuButtonHTML += "</div>";

            const $newElement = $(menuButtonHTML);
            $target.append($newElement);

            return $newElement;
        },
        
        _addTool: function($target, toolID) {
            const tool = this._tools[toolID];
            var buttonHTML = "<div style='position:relative' class='edit_button";
            if ('iconClass' in tool) buttonHTML += " " + tool.iconClass;
            buttonHTML += "'";
            if ('label' in tool) buttonHTML += " data-text='" + tool.label + "'";
            if ('titleOffset' in tool) buttonHTML += " data-title-offset='" + tool.titleOffset + "'";
            buttonHTML += " data-tool-id='" + toolID + "'";
            buttonHTML += " data-show-title='true'>";
            buttonHTML += "<div class='edit_button_icon'></div>";
            buttonHTML += "</div>";

            const $newElement = $(buttonHTML);
            $target.append($newElement);

            return $newElement;
        },

        _runTool: function($button) {
            const toolID = $button.attr('data-tool-id');
            const toolOptions = JSON.parse($button.attr('data-options') || "{}");
            if (this._toolTarget !== null) toolOptions.target = this._toolTarget;
            if (this._selectedCells !== null) toolOptions.selectedCells = this._selectedCells;

            this._suppressInactive = true;
            this._tools[toolID].apply(toolOptions)
            .then(() => {
                this._suppressInactive = false;
                HTMLEditorUtils.undecorateContent(this.element);
                RenderTools.unrenderEquations(this.element);

                HTMLEditorUtils.decorateContent(this.element);
                RenderTools.updateVariableNames(this.element, commonContext.variables);
                return RenderTools.renderEquations(this.element);
            })
            .then(() => {
                this._pushSavedState();
            });
        },

        // Save DOM state including selection and HTML
        _pushSavedState: function() {

            this.element.trigger('savedom');
            this.element[0].normalize(); 
        
            var selection = window.getSelection();
            var range;
            if (selection.rangeCount > 0) {
                range = selection.getRangeAt(0);
            } else {
                range = document.createRange();
                range.setStart(this.element[0], 0);
                range.setEnd(this.element[0], 0);
            }

            // Insert a start marker for the selection
            const startMarker = document.createElement('span');
            startMarker.setAttribute('data-selection', 'start');
            startMarker.style.display = 'none';
            range.insertNode(startMarker);

            // If the selection is not collapsed, also insert an end marker
            let endMarker = null;
            if (!range.collapsed) {
                const newRange = document.createRange();
                newRange.setStartAfter(startMarker);
                newRange.setEnd(selection.getRangeAt(0).endContainer, selection.getRangeAt(0).endOffset);

                endMarker = document.createElement('span');
                endMarker.setAttribute('data-selection', 'end');
                endMarker.style.display = 'none';
                newRange.insertNode(endMarker);
            }

            // Get entry

            const entry = {
                html: this.element.html(),
                variables: JSON.parse(JSON.stringify(commonContext.variables))
            };

            // Remove start and end markers

            startMarker.remove();
            if (endMarker) endMarker.remove();

            // Save state
        
            if (this._savedStateIndex >= 0 && this._savedStates[this._savedStateIndex].html === entry.html) {
                this._savedStates[this._savedStateIndex].variables = entry.variables;
            } else {
                if (this._savedStateIndex + 1 < this._savedStates.length) {
                    this._savedStates = this._savedStates.slice(0, this._savedStateIndex + 1);
                }
                this._savedStates.push(entry);
                this._savedStateIndex++;
            }
        },
        
        // Restore DOM state to the last saved
        _restoreSavedState: function() {
            this.element.focus();
        
            var entry = this._savedStates[this._savedStateIndex];

            // Restore the HTML (including zero-width marker spans)
            this.element.html(entry.html);

            // Find the markers and set selection
            const startMarker = this.element[0].querySelector('span[data-selection="start"]');
            const endMarker = this.element[0].querySelector('span[data-selection="end"]');

            const selection = window.getSelection();
            selection.removeAllRanges();

            if (startMarker) {
                // Create a new range from markers
                const range = document.createRange();
                range.setStartBefore(startMarker);
                if (endMarker) range.setEndBefore(endMarker);
                else range.collapse(true);

                // Apply selection
                selection.addRange(range);

                // Remove the markers
                startMarker.remove();
                if (endMarker) endMarker.remove();

            } else {
                // No markers => just collapse at start
                selection.collapse(this.element[0], 0);
            }
            
            // Restore variables from the saved state
            Object.keys(commonContext.variables).forEach(key => delete commonContext.variables[key]);
            Object.keys(entry.variables).forEach(key => { commonContext.variables[key] = entry.variables[key]; });
            RenderTools.updateVariableNames($('.html_editor'), commonContext.variables);
        },
        
        makeInactive: function() {
            if (!this._suppressInactive) {
                var $parent = this.element.closest('.edit_parent');
                HTMLEditorUtils.undecorateContent(this.element);
                $parent.addClass('inactive');
                return true;  
            } else return false;
        },

        makeActive: function() {
            var $parent = this.element.closest('.edit_parent');
            $parent.removeClass('inactive');
            HTMLEditorUtils.decorateContent(this.element);
        },

        // Undo and redo functions
        
        _stepUndo: function() {
            if (this._savedStateIndex > 0) {
                this._savedStateIndex--;
                this._restoreSavedState();
            }
        },

        _stepRedo: function() {
            if (this._savedStateIndex + 1 < this._savedStates.length) {
                this._savedStateIndex++;
                this._restoreSavedState();
            }
        },

        // Getter and setter
        
        getValue: function() {
            
            RenderTools.unrenderEquations(this.element);
            HTMLEditorUtils.undecorateContent(this.element);
            
            this.element[0].normalize();

            var html = this.element.html();
            html = html.replace(/(<span (?=[^>]*class="block_variable")(.*?)>)[^<]*(<\/span>)/, '$1$3');
            html = HTMLEditorUtils.cleanHTML(html);

            HTMLEditorUtils.decorateContent(this.element);
            RenderTools.updateVariableNames(this.element, commonContext.variables);
            RenderTools.renderEquations(this.element);

            return html;
        },
        
        setValue: function(newValue) {
            HTMLEditorUtils.undecorateContent(this.element);

            this.element.html(newValue);

            HTMLEditorUtils.decorateContent(this.element);
            RenderTools.updateVariableNames(this.element, commonContext.variables);
            RenderTools.renderEquations(this.element);
            
            this._savedStates = [];
            this._savedStateIndex = -1;
        },
        
        // Hide and show buttons and sections
        
        hideAllButtons: function() {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.edit_button').hide();
            $parent.find('.edit_bar_section').hide();
        },
    
        showAllButtons: function() {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.edit_button').show();
            $parent.find('.edit_bar_section').show();
        },
        
        hideElement: function(elementClass) {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.' + elementClass).each(function() {
                $(this).hide();
                if ($(this).hasClass('edit_bar_section')) $(this).find('.edit_button').hide();
            });
        },

        showElement: function(elementClass) {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.' + elementClass).each(function() {
                $(this).show();
                if ($(this).hasClass('edit_bar_section')) $(this).find('.edit_button').show();
                else if ($(this).hasClass('edit_button')) $(this).closest('.edit_bar_section').show();
            });
        },
        
        // Update button states
        
        updateButtonStates: function() {
            var $parent = this.element.closest('.edit_parent');
            var selection = window.getSelection();
        
            // Clear any references from a previous call
            this._selectedCells = null;
            this._toolTarget = null;
        
            if (selection.rangeCount > 0) {
                var hasSettings = false;
        
                // Loop through each range in the current selection
                for (var rangeIndex = 0; rangeIndex < selection.rangeCount; rangeIndex++) {
                    var range = selection.getRangeAt(rangeIndex);
                    var $startContainer = $(range.startContainer);
                    var $endContainer   = $(range.endContainer);
        
                    // Ensure the selection is within this .html_editor
                    if ($startContainer.closest('.html_editor').length !== 1) {
                        hasSettings = false;
                        break;
                    }
        
                    // Check if the selection is inside a single table
                    var $startTable = $startContainer.closest('table');
                    var $endTable   = $endContainer.closest('table');
                    if ($startTable.length === 1 && 
                        $endTable.length === 1 && 
                        $startTable[0] === $endTable[0]) 
                    {
                        // Identify the start cell
                        var $selectedCellsStart;
                        if (range.startContainer.nodeType === Node.TEXT_NODE) {
                            $selectedCellsStart = $startContainer.closest('td');
                        } else {
                            var $startElement = $startContainer.contents().eq(range.startOffset);
                            $selectedCellsStart = $startElement.closest('td');
                            if ($selectedCellsStart.length === 0) {
                                $selectedCellsStart = $startElement.find('td').first();
                            }
                        }
        
                        // Identify the end cell
                        var $selectedCellsEnd;
                        if (range.endContainer.nodeType === Node.TEXT_NODE) {
                            $selectedCellsEnd = $endContainer.closest('td');
                        } else {
                            var $endElement = $endContainer.contents().eq(range.endOffset - 1);
                            $selectedCellsEnd = $endElement.closest('td');
                            if ($selectedCellsEnd.length === 0) {
                                $selectedCellsEnd = $endElement.find('td').last();
                            }
                        }
        
                        // If the selection spans multiple rows, expand each end to its full row
                        if ($selectedCellsStart.parent()[0] !== $selectedCellsEnd.parent()[0]) {
                            $selectedCellsStart = $selectedCellsStart.parent().children().first();
                            $selectedCellsEnd   = $selectedCellsEnd.parent().children().last();
                        }
        
                        // Get a list of all <td> in the table and figure out the slice we need
                        var $allCells  = $selectedCellsStart.closest('table').find('td');
                        var startIndex = -1;
                        var endIndex   = -1;
        
                        for (var i = 0; i < $allCells.length; i++) {
                            if ($allCells.eq(i)[0] === $selectedCellsStart[0]) startIndex = i;
                            if ($allCells.eq(i)[0] === $selectedCellsEnd[0])   endIndex   = i;
                        }
        
                        // Slice out the selected cells
                        if (startIndex >= 0 && endIndex >= 0) {
                            if (this._selectedCells == null) this._selectedCells = $();
                            this._selectedCells = this._selectedCells.add($allCells.slice(startIndex, endIndex + 1));
                            hasSettings = true;
                        }
                    }
        
                    // Check if the selection exactly matches any <img>, .equation, or .block_variable
                    // by creating a range for each element and comparing range boundaries.
                    $('img, .img_resize_wrapper, .html_drawing, .drawing_resize_wrapper, .equation, .block_variable').each((index, target) => {
                        var elementRange = document.createRange();
                        elementRange.selectNode(target);
        
                        if (range.startContainer === elementRange.startContainer &&
                            range.startOffset === elementRange.startOffset &&
                            range.endContainer === elementRange.endContainer &&
                            range.endOffset === elementRange.endOffset) 
                        {
                            if ($(target).is('.img_resize_wrapper')) {
                                this._toolTarget = $(target).find('img');
                            } else if ($(target).is('.drawing_resize_wrapper')) {
                                this._toolTarget = $(target).find('.html_drawing');
                            } else this._toolTarget = $(target);
                            hasSettings = true;
                        }
                    });
                }
        
                // Update settings button based on whether or not we have an appropriate selection

                const $settingsButton = $parent.find('.edit_button_settings');
                if (hasSettings) {
                    var toolID = false;
                    if (this._toolTarget) {

                        if (this._toolTarget.is('.equation')) {
                            toolID = 'editEquation';
                        } else if (this._toolTarget.is('.block_variable')) {
                            toolID = 'editVariable';
                        } else if (this._toolTarget.is('img')) {
                            toolID = 'editImage';
                        } else if (this._toolTarget.is('.html_drawing')) {
                            toolID = 'editDrawing';
                        } else this._toolTarget = null;

                    } else if (this._selectedCells) {
                        toolID = 'editTable';
                    }

                    if (toolID !== false) {
                        $settingsButton.removeClass('disabled');
                        $settingsButton.attr('data-tool-id', toolID);
                    } else {
                        $settingsButton.addClass('disabled');
                        $settingsButton.attr('data-tool-id', 'settingsEmpty');                            
                    }

                } else {
                    $settingsButton.addClass('disabled');
                    $settingsButton.attr('data-tool-id', 'settingsEmpty');
                }
            }
        },
        
        // Initialize selection
        
        initSelection: function() {
            this.element.focus();

            var selection = window.getSelection();
            selection.collapse(this.element[0], 0);

            if (this._commandMode) {
                this._commandMode = false;
                var $parent = this.element.closest('.edit_parent');
                $parent.find('.command_status').html("");
            }

            this._pushSavedState();
        },
        
        // Handle keyboard shortcuts

        _handleKeydown: function(widget, e) {
            var $parent = widget.element.closest('.edit_parent');
            var $status = $parent.find('.command_status');
            if (widget._commandMode) {
                if ((widget._commandText.length == 0) && (e.key === '\\')) {
                    
                    widget._commandMode = false;
                    $status.html("");
                    return false;

                } else if (!e.altKey && !e.ctrlKey && !e.metaKey) {

                    if (e.key === 'Escape') {
                        widget._commandMode = false;
                        $status.html("");
                    } else if ((e.key === 'Backspace' || e.key === 'Delete')) {
                        if (widget._commandText.length > 0) {
                            widget._commandText = widget._commandText.slice(0, -1);
                            $status.html("<span style='float:left;'>Command: " + widget._commandText + "</span><span style='float:right;'>Type esc to cancel</span>");
                        }
                    } else if (e.key === 'Enter') {
                        widget._commandMode = false;
                        $status.html("");
                    } else if (/^[a-zA-Z]$/.test(e.key)) {
                        widget._commandText += e.key;
                        
                        // Check shortcuts
                        
                        if (widget._commandText in widget._buttonShortcuts) {
                            widget._commandMode = false;
                            const $button = widget._buttonShortcuts[widget._commandText];
                            if ($button.closest('.edit_button').is(':visible')) {
                                widget._runTool($button);
                            }
                        }
                        
                        // Show command
                        
                        $status.html("<span style='float:left;'>Command: " + widget._commandText + "</span><span style='float:right;'>Type esc to cancel</span>");
                        if (!widget._commandMode) {
                            $status.animate({
                                color: '#fff'
                            }, 500, 'swing', function() {
                                $status.html("");
                                $status.css('color', '#222');
                            });
                        }
                    }
        
                    return true;
                }
            } else if (e.key === '\\') {           // Backslash pressed
                var sel = window.getSelection();
                var range = (sel.rangeCount > 0) ? sel.getRangeAt(0) : null;

                if (range) {
                    // Ensure we have an element on which to call .closest().
                    var container = range.startContainer.nodeType === Node.TEXT_NODE
                        ? range.startContainer.parentElement
                        : range.startContainer;
                
                    if (!container.closest('.code_container')) {
                        widget._commandMode = true;
                        widget._commandText = "";
                        $status.html("<span style='float:left;'>Command:</span><span style='float:right;'>Type esc to cancel</span>");
                        return true;
                    }
                }                
            } else if (e.key === 'Tab') {
                e.preventDefault();

                var sel = window.getSelection();
                var range = (sel.rangeCount > 0) ? sel.getRangeAt(0) : null;

                if (range) {
                    // Ensure we have an element on which to call .closest().
                    var container = range.startContainer.nodeType === Node.TEXT_NODE
                        ? range.startContainer.parentElement
                        : range.startContainer;
                
                    if (container.closest('table')) {

                        var cell = container.closest('td');
                        if (cell) {
                            var nextCell = cell.nextElementSibling;
                            
                            // If there is no next sibling, try to get the first cell of the next row
                            if (!nextCell) {
                                var row = cell.parentElement;
                                var nextRow = row.nextElementSibling;
                                if (nextRow) {
                                    nextCell = nextRow.querySelector('td');
                                }
                            }
                            
                            // If a next cell was found, move the cursor to its end
                            if (nextCell) {
                                var range = document.createRange();
                                var emptyText = document.createTextNode('');
                                nextCell.appendChild(emptyText);
                                range.setStart(emptyText, 0);
                                range.collapse(true);
                                var sel = window.getSelection();
                                sel.removeAllRanges();
                                sel.addRange(range);
                            }
                        }
                        return true;

                    } else if (container.closest('.code_container')) {

                        HTMLEditorUtils.insertHTML('&#009;', widget.element);
                        widget._pushSavedState();
                        return true;

                    }
                }                
            } else if (e.ctrlKey || e.metaKey) {
                if (e.key === 'z' && e.shiftKey) {
                    widget._stepRedo();
                    return true;
                } else if (e.key === 'z' && !e.shiftKey) {
                    widget._stepUndo();
                    return true;
                } else if (e.key == 'y' && !e.shiftKey) {
                    widget._stepRedo();
                    return true;
                } else if (e.key == 'b' && !e.shiftKey) {
                    const $actionButton = $parent.find('.edit_button_bold');
                    widget._runTool($actionButton);
                    return true;
                } else if (e.key == 'i' && !e.shiftKey) {
                    const $actionButton = $parent.find('.edit_button_italic');
                    widget._runTool($actionButton);
                    return true;
                } else if (e.key == 'u' && !e.shiftKey) {
                    const $actionButton = $parent.find('.edit_button_underline');
                    widget._runTool($actionButton);
                    return true;
                }
            }

            return false;
        }
    });

	var blurWaiting = [];
    var focusWaiting = [];

	// Handle selection change -- must not save DOM state here
	
    $(document).on('selectionchange', function() {
        var sel = window.getSelection();
        for (var i = 0; i < sel.rangeCount; i++) {
            var range = sel.getRangeAt(i);
            var container = (range.startContainer.nodeType === Node.TEXT_NODE)
                ? range.startContainer.parentElement
                : range.startContainer;
    
            var $parentElement = $(container).closest('.html_editor');
            if ($parentElement.length == 1) {
                $parentElement.htmleditor('updateButtonStates');
            }
        }
    });

    // Expire internal clipboard if user leaves the window
	
	$(window).on('blur', function(e) {
		internalClipboard = null;
	});

	// Highlight selected equations

	$(document).on('mousedown', function(e) {
		mouseDown = true;
	});

	$(document).on('mouseup', function(e) {
		mouseDown = false;
	});

	$(document).on('mousemove', function(e) {
		if (mouseDown) HTMLEditorUtils.updateSelection();
	});

	$(document).on('click', function(e) {
		setTimeout(HTMLEditorUtils.updateSelection, 0);
	});

	$('#equation_text').equationeditor();
    $('#equation_text').equationeditor('hideElement', 'edit_button_sup');
    $('#equation_text').equationeditor('hideElement', 'edit_button_constant');
    $('#equation_text').equationeditor('hideElement', 'edit_button_tentothe');

	$('#editVariable_dialog .variable_equation').each(function() {
		$(this).equationeditor();
		$(this).equationeditor('option', 'restrictEquation', true);
		$(this).equationeditor('option', 'blockedKeys', [].concat(
			'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
			['=', '>', '<', '{', '[', '_', ',']
		));
		$(this).equationeditor('hideElement', 'edit_button_text');
		$(this).equationeditor('hideElement', 'edit_button_symbol');
		$(this).equationeditor('hideElement', 'edit_button_accent');
		$(this).equationeditor('hideElement', 'edit_button_sup_sub');
		$(this).equationeditor('hideElement', 'edit_button_matrix');
		$(this).equationeditor('hideElement', 'edit_button_other');
		$(this).equationeditor('hideElement', 'edit_button_brackets');
	});

	$('#calculated_equation, #conditional_then, #conditional_else').equationeditor('option', 'blockedKeys', [].concat(
		'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
		['=', '>', '<', '{', '[', '_', ',']
	));

	$('#conditional_if').equationeditor('option', 'blockedKeys', [].concat(
		'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
		['{', '[', '_', ',']
	));

	$(document).on('focus', '.variable_conditional .edit_parent', function(e) {
		e.preventDefault();

		var $editor = $(this).find('.variable_equation');
		var blurIndex = blurWaiting.indexOf($editor);
		if (blurIndex >= 0) blurWaiting.splice(blurIndex, 1);
		focusWaiting.push($editor);
	});
	
	$(document).on('blur', '.variable_conditional .edit_parent', function(e) {
		var $editor = $(this).find('.variable_equation');
		var focusIndex = focusWaiting.indexOf($editor);
		if (focusIndex >= 0) focusWaiting.splice(focusIndex, 1);
		blurWaiting.push($editor);
	});
	
	$(document).on('click keyup', function(e) {
		
		// Handle blur and focus for equationeditor fields on click so that we don't (for example)
		// move dialog buttons out of the way when a field shrinks.
		
		setTimeout(function() {
			while (blurWaiting.length > 0) {
				var $element = blurWaiting.pop();
				if ($element.is('.eqn_editor')) {
					$element.equationeditor('makeInactive');
				}
			}
			while (focusWaiting.length > 0) {
				var $element = focusWaiting.pop();
				if ($element.is('.eqn_editor')) {
					$element.equationeditor('makeActive');
				}
			}
		}, 0);
	});

    htmlEditInitialized = true;
}

export { initialize, setCommonContext, getEditorVariables };
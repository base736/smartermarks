/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { FormatRemoveTool } from './FormatRemoveTool.js?7.0';
import { FormatBoldTool } from './FormatBoldTool.js?7.0';
import { FormatItalicTool } from './FormatItalicTool.js?7.0';
import { FormatUnderlineTool } from './FormatUnderlineTool.js?7.0';
import { FormatSubscriptTool } from './FormatSubscriptTool.js?7.0';
import { FormatSuperscriptTool } from './FormatSuperscriptTool.js?7.0';

import { JustifyCenterTool } from './JustifyCenterTool.js?7.0';
import { JustifyLeftTool } from './JustifyLeftTool.js?7.0';
import { JustifyRightTool } from './JustifyRightTool.js?7.0';

import { ListBulletedTool } from './ListBulletedTool.js?7.0';
import { ListChecklistTool } from './ListChecklistTool.js?7.0';

import { ListNumberedTool } from './ListNumberedTool.js?7.0';
import { ListLowercaseLettersTool } from './ListLowercaseLettersTool.js?7.0';
import { ListUppercaseLettersTool } from './ListUppercaseLettersTool.js?7.0';
import { ListLowercaseRomanTool } from './ListLowercaseRomanTool.js?7.0';
import { ListUppercaseRomanTool } from './ListUppercaseRomanTool.js?7.0';

import { InsertVariableTool } from './InsertVariableTool.js?7.0';
import { InsertBlankTool } from './InsertBlankTool.js?7.0';
import { InsertTableTool } from './InsertTableTool.js?7.0';
import { InsertImageTool } from './InsertImageTool.js?7.0';
import { InsertDrawingTool } from './InsertDrawingTool.js?7.0';
import { InsertEquationTool } from './InsertEquationTool.js?7.0';
import { InsertSymbolTool } from './InsertSymbolTool.js?7.0';
import { InsertCodeTool } from './InsertCodeTool.js?7.0';

import { SettingsEmptyTool } from './SettingsEmptyTool.js?7.0';
import { EditImageTool } from './EditImageTool.js?7.0';
import { EditDrawingTool } from './EditDrawingTool.js?7.0';
import { EditTableTool } from './EditTableTool.js?7.0';
import { EditVariableTool } from './EditVariableTool.js?7.0';
import { EditEquationTool } from './EditEquationTool.js?7.0';

export function createTools(context) {
    return {
        formatRemove: new FormatRemoveTool(context),
        formatBold: new FormatBoldTool(context),
        formatItalic: new FormatItalicTool(context),
        formatUnderline: new FormatUnderlineTool(context),
        formatSubscript: new FormatSubscriptTool(context),
        formatSuperscript: new FormatSuperscriptTool(context),
    
        justifyCenter: new JustifyCenterTool(context),
        justifyLeft: new JustifyLeftTool(context),
        justifyRight: new JustifyRightTool(context),
    
        listBulleted: new ListBulletedTool(context),
        listChecklist: new ListChecklistTool(context),
    
        listNumbered: new ListNumberedTool(context),
        listLowercaseLetters: new ListLowercaseLettersTool(context),
        listUppercaseLetters: new ListUppercaseLettersTool(context),
        listLowercaseRoman: new ListLowercaseRomanTool(context),
        listUppercaseRoman: new ListUppercaseRomanTool(context),
    
        insertVariable: new InsertVariableTool(context),
        insertBlank: new InsertBlankTool(context),
        insertTable: new InsertTableTool(context),
        insertImage: new InsertImageTool(context),
        insertDrawing: new InsertDrawingTool(context),
        insertEquation: new InsertEquationTool(context),
        insertSymbol: new InsertSymbolTool(context),
        insertCode: new InsertCodeTool(context),

        settingsEmpty: new SettingsEmptyTool(context),
        editImage: new EditImageTool(context),
        editDrawing: new EditDrawingTool(context),
        editTable: new EditTableTool(context),
        editVariable: new EditVariableTool(context),
        editEquation: new EditEquationTool(context)
    }
};
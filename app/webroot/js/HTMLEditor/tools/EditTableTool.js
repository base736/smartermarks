/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { Tool } from './Tool.js?7.0';
import { EditTableDialog } from '../dialogs/EditTableDialog.js?7.0';
import * as HTMLEditorUtils from '../utils.js?7.0';

export class EditTableTool extends Tool {
    static _activeResizer = null;

    constructor(context) {
        super(context);
        this.id = 'editTable';
        this.label = 'Settings';
        this.iconClass = 'edit_button_settings';
        this.titleOffset = -12;
    }

    static decorate($target) {
        $target.find('table').each(function() {
            EditTableTool._addResizers($(this));
        });

        $target.on('pointerdown.editTable', '.resizer_left, .resizer_right', function(e) {
            const orig = e.originalEvent;
            const pageX = orig.pageX || (orig.clientX + window.scrollX);
            EditTableTool._activeResizer = this;

            var resizerRect = this.getBoundingClientRect();
            const startOffset = pageX - (resizerRect.left + resizerRect.right) / 2.0;
            $(this).attr('data-start-offset', startOffset);

            var resizerType = $(this).attr('data-type');
            if (resizerType !== 'left' && resizerType !== 'right') {
                // It's one of the column-split resizers
                var $table = $(this).closest('table');
                var mouseIndex = parseInt(resizerType, 10);
                var widths = HTMLEditorUtils.getTableWidths($table);
                var tableRect = $table[0].getBoundingClientRect();

                // Calculate the leftX boundary
                let leftX = tableRect.left + window.scrollX - 0.5;
                for (let i = 0; i < mouseIndex; i++) {
                    leftX += widths[i] * tableRect.width;
                }

                // Calculate the rightX boundary
                let rightX = leftX;
                for (let i = mouseIndex; i <= mouseIndex + 1; i++) {
                    rightX += widths[i] * tableRect.width;
                }

                // Store in data attributes
                $(this).attr('data-left-x', leftX);
                $(this).attr('data-right-x', rightX);
            }

            $(document)
                .on('pointermove.editTableResize', EditTableTool._onPointerMove)
                .on('pointerup.editTableResize', EditTableTool._onPointerUp);

            e.preventDefault();
        });
    }

    static _addResizers($table) {
        $table.find('tr').each(function() {
            var index = -1;
            $(this).find('td').each(function() {
                if (!this.hasAttribute('colspan')) {
                    index++;
                } else {
                    index += parseInt($(this).attr('colspan'), 10);
                }

                // Left resizer on the first cell in row
                if ($(this).is(':first-child') && $(this).find('.resizer_left').length === 0) {
                    $(this).prepend("<div class='resizer_left' contenteditable='false' data-type='left'></div>");
                }

                // Right resizer on every cell: if it's last-child -> 'right' type, otherwise -> index
                if ($(this).find('.resizer_right').length === 0) {
                    if ($(this).is(':last-child')) {
                        $(this).prepend("<div class='resizer_right' contenteditable='false' data-type='right'></div>");
                    } else {
                        $(this).prepend("<div class='resizer_right' contenteditable='false' data-type='" + index + "'></div>");
                    }
                }
            });
        });
    }

    static _onPointerMove(e) {
        const $resizer = EditTableTool._activeResizer;
        if (!$resizer) return;

        const orig = e.originalEvent;
        const pageX = orig.pageX || (orig.clientX + window.scrollX);

        var $resizerEl = $(EditTableTool._activeResizer);
        var resizerType = $resizerEl.attr('data-type');
        var $table = $resizerEl.closest('table');

        const startOffset = parseFloat($resizerEl.attr('data-start-offset')) || 0;
        const leftX = parseFloat($resizerEl.attr('data-left-x')) || 0;
        const rightX = parseFloat($resizerEl.attr('data-right-x')) || 0;

        var $editView = $table.closest('.html_editor');
        var editRect = $editView[0].getBoundingClientRect();
        var editLeft = editRect.left + window.scrollX + parseFloat($editView.css('padding-left'));
        var editRight = editRect.right + window.scrollX - parseFloat($editView.css('padding-right'));

        var marginLeft = parseFloat($table.css('margin-left'));
        var marginRight = parseFloat($table.css('margin-right'));

        var mouseX = pageX - startOffset;
        if (mouseX < editLeft) mouseX = editLeft;
        if (mouseX > editRight) mouseX = editRight;

        if (resizerType === 'left') {
            var newMarginLeft = mouseX - editLeft;
            var newWidth = editRight - editLeft - marginRight - newMarginLeft;
            $table.css('margin-left', (100 * newMarginLeft / (editRight - editLeft)).toFixed(3) + '%');
            $table.css('width', (100 * newWidth / (editRight - editLeft)).toFixed(3) + '%');
        } else if (resizerType === 'right') {
            var newMarginRight = editRight - mouseX;
            var newWidth = editRight - editLeft - newMarginRight - marginLeft;
            $table.css('margin-right', (100 * newMarginRight / (editRight - editLeft)).toFixed(3) + '%');
            $table.css('width', (100 * newWidth / (editRight - editLeft)).toFixed(3) + '%');
        } else {
            // Column resizer
            var mouseIndex = parseInt(resizerType, 10);
            var tableRect = $table[0].getBoundingClientRect();
            if (mouseX < leftX + 12) mouseX = leftX + 12;
            if (mouseX > rightX - 12) mouseX = rightX - 12;

            var columnWidths = HTMLEditorUtils.getTableWidths($table);
            columnWidths[mouseIndex] = (mouseX - leftX) / tableRect.width;
            columnWidths[mouseIndex + 1] = (rightX - mouseX) / tableRect.width;
            HTMLEditorUtils.setTableWidths($table, columnWidths);
        }

        // Force table repaint. Without this, borders aren't always redrawn.
        $table.hide().show(0);

        e.preventDefault();
    }

    static _onPointerUp(e) {
        const $resizer = EditTableTool._activeResizer;
        if (!$resizer) return;

        EditTableTool._activeResizer = null;

        $(document).off('.editTableResize');
    }

    static undecorate($target) {
        $target.find('.resizer_left').remove();
        $target.find('.resizer_right').remove();
        $target.find('table').removeAttr('data-widths');

        $target.off('.editTable');
        $(document).off('.editTableResize');
    }

    apply(options = {}) {
        options.mode = 'edit';
        return EditTableDialog.showDialog(this.context, options);
    }
};
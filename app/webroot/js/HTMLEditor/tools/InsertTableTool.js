/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { Tool } from './Tool.js?7.0';
import { EditTableDialog } from '../dialogs/EditTableDialog.js?7.0';

export class InsertTableTool extends Tool {
    constructor(context) {
        super(context);
        this.id = 'table';
    }

    apply(options = {}) {
        options.mode = 'insert';
        if ('rows' in options && 'cols' in options) {
            EditTableDialog.createNewTable(this.context, options.rows, options.cols, 'middle');
            return Promise.resolve('done');
        } else {
            return EditTableDialog.showDialog(this.context, options);
        }
    }
};

/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { Tool } from './Tool.js?7.0';
import { EditVariableDialog } from '../dialogs/EditVariableDialog.js?7.0';

export class EditVariableTool extends Tool {
    constructor(context) {
        super(context);
        this.id = 'editVariable';
        this.label = 'Settings';
        this.iconClass = 'edit_button_settings';
        this.titleOffset = -12;
    }

    apply(options) {
        options.mode = 'edit';
        return EditVariableDialog.showDialog(this.context, options);
    }
}
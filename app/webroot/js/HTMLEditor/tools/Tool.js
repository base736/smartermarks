/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class Tool {
    constructor(context) {
        this.context = context;
        this.id = null;
    }

    apply(options) {
        throw new Error(`Apply not implemented by tool: ${this.id}`);
    }

    applyCommand(command) {
        // Get the user's current selection
        const selection = window.getSelection();
      
        // If there's at least one range, proceed
        if (selection.rangeCount > 0) {
            const range = selection.getRangeAt(0);
        
            // Re-apply the selection range (some browsers require this before execCommand)
            selection.removeAllRanges();
            selection.addRange(range);
        
            // Execute the requested command (e.g. "bold", "italic", etc.)
            document.execCommand(command, false, null);
        }
    }
}
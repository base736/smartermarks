/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { Tool } from './Tool.js?7.0';

export class ListTool extends Tool {
    apply(options) {
        if (this.listSelector == 'ol') this.applyCommand('insertOrderedList');            
        else if (this.listSelector == 'ul') this.applyCommand('insertUnorderedList');
        else return false;
                
        var sel = window.getSelection();
        var range = sel.rangeCount > 0 ? sel.getRangeAt(0) : null;
        
        if (range) {
            // If it's a text node, use its parent so we can call .closest() on an element
            var container = (range.startContainer.nodeType === Node.TEXT_NODE)
                ? range.startContainer.parentElement
                : range.startContainer;
        
            var listParent = container.closest(this.listSelector);
            if (listParent) {
                if (this.listStyleType) {
                    console.log("A");
                    listParent.style.listStyleType = this.listStyleType;
                } else if (this.listClass) {
                    console.log("B");
                    listParent.classList.add(this.listClass);
                }
            }
        }

        return Promise.resolve('done');
    }
};

/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as HTMLEditorUtils from '../utils.js?7.0';

import { Tool } from './Tool.js?7.0';
import { EditDrawingDialog } from '../dialogs/EditDrawingDialog.js?7.0';
import { ColourPicker, Annotation } from '/js/Annotation/annotation.js?7.0';

const resize_handleRadius = 5;

export class EditDrawingTool extends Tool {
    static _activeResizer = null;
    static _activeAnnotation = null;
    static _lastToolName = null;

    constructor(context) {
        super(context);
        this.id = 'editDrawing';
        this.label = 'Settings';
        this.iconClass = 'edit_button_settings';
        this.titleOffset = -12;
    }

    static decorate($target) {

        // Show resize wrapper when mouse is over a drawing
        $target.on('mouseenter.editDrawing', '.html_drawing', function(e) {
            if (!$(this).parent().is('.drawing_resize_wrapper')) {
                $target.find('.drawing_resize_wrapper').each(function() {
                    EditDrawingTool._removeResizer($(this));
                });
                EditDrawingTool._addResizer($(this));
            }
        });

        // Hide resize wrapper when mouse leaves a drawing and wrapper is not locked
        $target.on('mouseleave.editDrawing', '.drawing_resize_wrapper:not(.locked)', function(e) {
            if (!EditDrawingTool._activeResizer) {
                EditDrawingTool._removeResizer($(this));
                $(document).off('.editDrawingResize');
            }
        });

        // When either the inner drawing or the wrapper is clicked, add a "locked" state so that 
        // it stays visible until the user clicks somewhere outside the wrapper.
        $target.on('pointerdown.editDrawingLock', '.html_drawing, .drawing_resize_wrapper', function(e) {
            // Prevent the click from bubbling up so that the document
            // click handler does not immediately undo the lock.
            e.stopPropagation();
            const $wrapper = $(this).closest('.drawing_resize_wrapper');
            EditDrawingTool._lockWrapper($wrapper);
        });

        // Start resize when the resize corner is clicked
        $target.on('pointerdown.editDrawing', '.drawing_resize_corner', function(e) {
            const orig = e.originalEvent;
            const pageX = orig.pageX || (orig.clientX + window.scrollX);
            const pageY = orig.pageY || (orig.clientY + window.scrollY);
                        
            EditDrawingTool._activeResizer = $(this);
            EditDrawingTool._activeResizer.attr('data-offset-x', pageX - $(this).offset().left - $(this).outerWidth());
            EditDrawingTool._activeResizer.attr('data-offset-y', pageY - $(this).offset().top - $(this).outerHeight());
        
            $(document)
                .on('pointermove.editDrawingResize', EditDrawingTool._onPointerMove)
                .on('pointerup.editDrawingResize', EditDrawingTool._onPointerUp);
        });

        // Hide toolbar when drawing, and return afterward
        $target.on('pointerdown.editDrawing', '.html_drawing', function(e) {
            $(this).closest('.drawing_resize_wrapper').find('.drawing_toolbar').hide();
        });

        $target.on('pointerup.editDrawing', '.html_drawing', function(e) {
            $(this).closest('.drawing_resize_wrapper').find('.drawing_toolbar').show();
        });

        // Close colour picker if $target is in a dialog and that dialog closes
        const $dialog = $target.closest('.ui-dialog-content');
        if ($dialog.length === 1) {
            $dialog.off("dialogclose.colourPicker");
            $dialog.on("dialogclose.colourPicker", function() {
                $dialog.off("dialogclose.colourPicker");
                ColourPicker.close();
            });
        }
    }

    static _addResizer($drawing) {
        EditDrawingDialog._activeAnnotation = new Annotation($drawing[0], () => { });
        EditDrawingDialog._activeAnnotation.setTool(this._lastToolName);

        // Capture the existing selection ranges
        const oldSelection = HTMLEditorUtils.getSelectionDetails();

        // Wrap drawing with resize wrapper
        const $resizeWrapper = $(`
            <div class="drawing_resize_wrapper" contenteditable="false"></div>
        `).insertAfter($drawing);

        $resizeWrapper.attr('style', $drawing.attr('style'));
        $drawing.removeAttr('style');
        $resizeWrapper.append($drawing);

        const $resizeBox = $(`
            <div class="drawing_resize_box"></div>
        `).appendTo($resizeWrapper);

        const $resizer = $('<div class="drawing_resize_corner"></div>')
            .css({ 
                bottom: 0, 
                right: 0,
                width: 2 * resize_handleRadius,
                height: 2 * resize_handleRadius
            })
            .appendTo($resizeBox);

        // Build drawing toolbar
        const $drawingToolbar = $('<div class="drawing_toolbar"></div>')
            .appendTo($resizeBox);

        for (const toolName of ['pen', 'eraser']) {
            let classes = 'drawing_tool select_connected';
            if (toolName == this._lastToolName) classes += ' selected';
            const toolData = Annotation.tools[toolName];
            const html = `
                <div class="${classes}" data-type="${toolName}">
                    <img class="annotation_icon" src="${toolData.iconURL}" />
                </div>`;        
            $drawingToolbar.append(html);
        }

        ColourPicker.initialize();

        const pickerOpen = ColourPicker.$dialog.dialog('isOpen');
        const pickerClasses = 'drawing_tool' + (pickerOpen ? ' selected' : '');
        const pickerAction = pickerOpen ? 'close' : 'open';

        const pickerHTML = `
            <div id="colour_picker_toggle" class="${pickerClasses}" data-type="colour_picker" 
                data-action="${pickerAction}">
                <img class="annotation_icon" src="/img/icons/annotation_colour.svg" />
            </div>`;        
        $drawingToolbar.append(pickerHTML);

		ColourPicker.$dialog.on("dialogopen", function() {
            $('#colour_picker_toggle').addClass('selected');
			$('#colour_picker_toggle').attr('data-action', 'close');
        }).on("dialogclose", function() {
            $('#colour_picker_toggle').removeClass('selected');
			$('#colour_picker_toggle').attr('data-action', 'open');
        });

        $drawingToolbar.on('pointerdown', '.drawing_tool', (e) => {
            e.stopPropagation();
            const $clickedTool = $(e.currentTarget);
            const clickedToolName = $clickedTool.attr('data-type');
            if (clickedToolName == 'colour_picker') {
                if ($clickedTool.attr('data-action') == 'open') {
                    const $wrapper = $clickedTool.closest('.drawing_resize_wrapper');
                    EditDrawingTool._lockWrapper($wrapper);
                    ColourPicker.open();    
                } else {
                    ColourPicker.close();
                }
            } else if (Annotation.tools[clickedToolName]) {
                this._lastToolName = clickedToolName;
                $clickedTool.siblings('.select_connected').removeClass('selected');
                $clickedTool.addClass('selected');
                EditDrawingDialog._activeAnnotation.setTool(clickedToolName);
            }
        });

        // Rebuild selections
        HTMLEditorUtils.restoreSelectionDetails(oldSelection);
    }

    static _removeResizer($resizeWrapper) {

        // Clean up active annotation if there is one
        if (EditDrawingDialog._activeAnnotation) {
            EditDrawingDialog._activeAnnotation.destroy();
            EditDrawingDialog._activeAnnotation = null;
        }

        // Capture the existing selection ranges
        const oldSelection = HTMLEditorUtils.getSelectionDetails();

        // Unwrap the drawing element
        const $drawing = $resizeWrapper.find('.html_drawing');
        $drawing.attr('style', $resizeWrapper.attr('style'));
        $drawing.insertAfter($resizeWrapper);
        $resizeWrapper.remove();
        
        // Rebuild selections
        HTMLEditorUtils.restoreSelectionDetails(oldSelection);
    }

    static _refreshResizeWrapper($resizeWrapper, mouseX, mouseY) {
        const $drawing = $resizeWrapper.find('.html_drawing');
        EditDrawingTool._removeResizer($resizeWrapper);

        // Get the drawing's position and size.
        const drawingOffset = $drawing.offset();
        const drawingWidth = $drawing.outerWidth();
        const drawingHeight = $drawing.outerHeight();

        // Check if the mouse (using page coordinates) is over the drawing.
        if (
            mouseX >= drawingOffset.left &&
            mouseX <= drawingOffset.left + drawingWidth &&
            mouseY >= drawingOffset.top &&
            mouseY <= drawingOffset.top + drawingHeight
        ) {
            // Only add the resizer if the mouse is over the drawing.
            EditDrawingTool._addResizer($drawing);
        }
    }

    static _lockWrapper($wrapper) {
        if (($wrapper.length == 1) && !$wrapper.hasClass('locked')) {
            $wrapper.addClass('locked');
            $(document).on('pointerdown.editDrawingUnlock', function(ev) {
                const wrapperClicked = $(ev.target).closest($wrapper).length > 0;                
                const $pickerDialog = $('#colourPicker_dialog').closest('.ui-dialog');
                const pickerClicked = $(ev.target).closest($pickerDialog).length > 0;
                if (!wrapperClicked && !pickerClicked) {
                    EditDrawingTool._removeResizer($wrapper);
                    $(document).off('pointerdown.editDrawingUnlock');
                }
            });
        }
    }

    static _onPointerMove(e) {
        const $resizer = EditDrawingTool._activeResizer;
        if (!$resizer) return;

        const orig = e.originalEvent;
        const pageX = orig.pageX || (orig.clientX + window.scrollX);
        const pageY = orig.pageY || (orig.clientY + window.scrollY);

        const offsetX = $resizer.data('offsetX');
        const offsetY = $resizer.data('offsetY');
        
        // Current mouse position minus the offset
        const mouseX = pageX - offsetX;
        const mouseY = pageY - offsetY;
        
        const $resizeWrapper = $resizer.closest('.drawing_resize_wrapper');

        // Calculate new size
        const wrapperRect = $resizeWrapper[0].getBoundingClientRect();
        let newWidth = mouseX - wrapperRect.left - window.scrollX;
        let newHeight = mouseY - wrapperRect.top - window.scrollY;

        newWidth = Math.max(newWidth, 0);
        newHeight = Math.max(newHeight, 0);

        // Update wrapper and box
        $resizeWrapper.find('.drawing_resize_box')
            .outerWidth(newWidth)
            .outerHeight(newHeight);
    }

    static _onPointerUp(e) {
        const $resizer = EditDrawingTool._activeResizer;
        if (!$resizer) return;

        const orig = e.originalEvent;
        const pageX = orig.pageX || (orig.clientX + window.scrollX);
        const pageY = orig.pageY || (orig.clientY + window.scrollY);

        const $resizeWrapper = $resizer.closest('.drawing_resize_wrapper');

        const newWidthPixels = $resizeWrapper.find('.drawing_resize_box').outerWidth();
        const newHeightPixels = $resizeWrapper.find('.drawing_resize_box').outerHeight();

        const parentWidthPixels = $resizeWrapper.parent().closest('div, .templateView, td').width() || 1;

        let newWidthPercent = (newWidthPixels / parentWidthPixels) * 100;
        newWidthPercent = Math.min(Math.max(newWidthPercent, 0), 100);

        $resizeWrapper
            .css('width', newWidthPercent.toFixed(3) + '%')
            .css('aspect-ratio', `${newWidthPixels}/${newHeightPixels}`);
            
        $(document).off('.editDrawingResize');
        EditDrawingTool._activeResizer = null;
        EditDrawingTool._refreshResizeWrapper($resizeWrapper, pageX, pageY);
    }

    static undecorate($target) {
        $target.find('.drawing_resize_wrapper').each(function() {
            EditDrawingTool._removeResizer($(this));
        });

        $target.off('.editDrawing');
        $(document).off('.editDrawingResize');
        $(document).off('.editDrawingUnlock');
        EditDrawingTool._activeResizer = null;
    }

    apply(options = {}) {
        options.mode = 'edit';
        return EditDrawingDialog.showDialog(this.context, options);
    }
}
/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { Tool } from './Tool.js?7.0';
import * as HTMLEditorUtils from '../utils.js?7.0';

export class InsertCodeTool extends Tool {
    constructor(context) {
        super(context);
        this.id = 'insertCode';
        this.label = 'Code';
        this.iconClass = 'edit_button_code';
    }
    
    apply(options) {
        const blockHTML = "<div class='code_container'><div id='newCodeArea'> </div></div>";
        HTMLEditorUtils.insertHTML(blockHTML, this.context.editor.$element);
    
        var selection = window.getSelection();
        var range = document.createRange();
        
        var newCodeArea = document.getElementById('newCodeArea');
        range.setStart(newCodeArea, 0);
        range.collapse(true);
        
        selection.removeAllRanges();
        selection.addRange(range);

        $('#newCodeArea').removeAttr('id');

        return Promise.resolve('done');
    }
};

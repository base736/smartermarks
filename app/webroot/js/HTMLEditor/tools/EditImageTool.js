/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as HTMLEditorUtils from '../utils.js?7.0';

import { Tool } from './Tool.js?7.0';
import { EditImageDialog } from '../dialogs/EditImageDialog.js?7.0';

const resize_handleRadius = 5;

export class EditImageTool extends Tool {
    static _activeResizer = null;

    constructor(context) {
        super(context);
        this.id = 'editImage';
        this.label = 'Settings';
        this.iconClass = 'edit_button_settings';
        this.titleOffset = -12;
    }

    static decorate($target) {

        $target.find('img').attr('draggable', 'false');

        $target.on('mouseenter.editImage', 'img', function(e) {
            if (!$(this).parent().is('.img_resize_wrapper')) {
                EditImageTool._addResizer($(this));
            }
        });

        $target.on('mouseleave.editImage', '.img_resize_wrapper', function(e) {
            if (!EditImageTool._activeResizer) {
                EditImageTool._removeResizer($(this));
                $(document).off('.editImageResize');
            }
        });

        $target.on('pointerdown.editImage', '.img_resize_corner', function(e) {
            const orig = e.originalEvent;
            const pageX = orig.pageX || (orig.clientX + window.scrollX);
            const pageY = orig.pageY || (orig.clientY + window.scrollY);
                        
            EditImageTool._activeResizer = $(this);
            EditImageTool._activeResizer.attr('data-offset-x', pageX - $(this).offset().left - $(this).outerWidth());
            EditImageTool._activeResizer.attr('data-offset-y', pageY - $(this).offset().top - $(this).outerHeight());
        
            $(document)
                .on('pointermove.editDrawingResize', EditImageTool._onPointerMove)
                .on('pointerup.editDrawingResize', EditImageTool._onPointerUp);
        });        
    }

    static _addResizer($image) {
        
        // Capture the existing selection ranges

        const oldSelection = HTMLEditorUtils.getSelectionDetails();
        
        // Wrap the image

        const $resizeWrapper = $(`
            <div class="img_resize_wrapper" contenteditable="false"></div>
        `).insertAfter($image);
      
        $resizeWrapper.attr('style', $image.attr('style'));
        $resizeWrapper.css('aspect-ratio', `${$image.width()}/${$image.height()}`);
        $image.removeAttr('style');
        $resizeWrapper.append($image);
      
        const $resizeBox = $(`
            <div class="img_resize_box"></div>
        `).appendTo($resizeWrapper);
      
        $('<div class="img_resize_corner"></div>')
        .css({
            bottom: 0,
            right: 0,
            width: 2 * resize_handleRadius,
            height: 2 * resize_handleRadius
        })
        .appendTo($resizeBox);
        
        // Rebuild selections

        HTMLEditorUtils.restoreSelectionDetails(oldSelection);
    }

    static _removeResizer($resizeWrapper) {
        
        // Capture the existing selection ranges

        const oldSelection = HTMLEditorUtils.getSelectionDetails();
        
        // Unwrap the image

        const $image = $resizeWrapper.find('img');
        $image.attr('style', $resizeWrapper.attr('style'));
        $image.css('aspect-ratio', '');
        $image.insertAfter($resizeWrapper);
        $resizeWrapper.remove();
        
        // Rebuild selections

        HTMLEditorUtils.restoreSelectionDetails(oldSelection);
    }

    static _refreshResizeWrapper($resizeWrapper, mouseX, mouseY) {
        const $image = $resizeWrapper.find('img');
        EditImageTool._removeResizer($resizeWrapper);

        // Get the image's position and size.
        const imageOffset = $image.offset();
        const imageWidth = $image.outerWidth();
        const imageHeight = $image.outerHeight();

        // Check if the mouse (using page coordinates) is over the drawing.
        if (
            mouseX >= imageOffset.left &&
            mouseX <= imageOffset.left + imageWidth &&
            mouseY >= imageOffset.top &&
            mouseY <= imageOffset.top + imageHeight
        ) {
            // Only add the resizer if the mouse is over the image.
            EditImageTool._addResizer($image);
        }
    }

    static _onPointerMove(e) {
        const $resizer = EditImageTool._activeResizer;
        if (!$resizer) return;

        const orig = e.originalEvent;
        const pageX = orig.pageX || (orig.clientX + window.scrollX);
        const pageY = orig.pageY || (orig.clientY + window.scrollY);

        const offsetX = $resizer.data('offsetX');
        const offsetY = $resizer.data('offsetY');
        
        // Current mouse position minus the offset
        const mouseX = pageX - offsetX;
        const mouseY = pageY - offsetY;
        
        const $resizeWrapper = $resizer.closest('.img_resize_wrapper');

        // Calculate new size
        const wrapperRect = $resizeWrapper[0].getBoundingClientRect();
        let newWidth = mouseX - wrapperRect.left - window.scrollX;
        let newHeight = mouseY - wrapperRect.top - window.scrollY;

        newWidth = Math.max(newWidth, 0);
        newHeight = Math.max(newHeight, 0);

        // Keep aspect ratio
        const $image = $resizeWrapper.find('img');

        const xRatio = newWidth / $image.width();
        const yRatio = newHeight / $image.height();
        const ratio = Math.max(xRatio, yRatio);
        
        newWidth = $image.width() * ratio;
        newHeight = $image.height() * ratio;

        // Update wrapper and box
        $resizeWrapper.find('.img_resize_box')
            .outerWidth(newWidth)
            .outerHeight(newHeight);
    }

    static _onPointerUp(e) {
        const $resizer = EditImageTool._activeResizer;
        if (!$resizer) return;

        const orig = e.originalEvent;
        const pageX = orig.pageX || (orig.clientX + window.scrollX);
        const pageY = orig.pageY || (orig.clientY + window.scrollY);

        const $resizeWrapper = $resizer.closest('.img_resize_wrapper');

        const newWidthPixels = $resizeWrapper.find('.img_resize_box').outerWidth();
        const parentWidthPixels = $resizeWrapper.parent().closest('div, .templateView, td').width() || 1;

        let newWidthPercent = (newWidthPixels / parentWidthPixels) * 100;
        newWidthPercent = Math.min(Math.max(newWidthPercent, 0), 100);

        $resizeWrapper.css('width', newWidthPercent.toFixed(3) + '%');
                
        $(document).off('.editImageResize');
        EditImageTool._activeResizer = null;
        EditImageTool._refreshResizeWrapper($resizeWrapper, pageX, pageY);
    }

    static undecorate($target) {
        $target.find('img').removeAttr('draggable');

        $target.find('.img_resize_wrapper').each(function() {
            EditImageTool._removeResizer($(this));
        });

        $target.off('.editImage');
        $(document).off('.editImageResize');
        EditImageTool._activeResizer = null;
    }

    apply(options = {}) {
        options.mode = 'edit';
        return EditImageDialog.showDialog(this.context, options);
    }
}
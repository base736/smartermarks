/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class InsertBlankMenu {
    constructor(context) {
        this.id = 'insertBlank';
        this.label = 'Blank';
        this.iconClass = 'edit_button_blank';
    }
    
    setMenu($target, labelTypes) {
        var menuHTML = '<ul style="min-width:50px; width:auto;">';
        if (labelTypes.length == 0) {
            menuHTML += '<li class="edit_menu_button label_select" data-tool-id="insertBlank">&nbsp;</li>';
        } else {
            for (var i = 0; i < labelTypes.length; ++i) {
                const options = { 
                    label: labelTypes[i]
                };
                const optionsString = htmlSingleQuotes(JSON.stringify(options));
                
                menuHTML += `<li class="edit_menu_button label_select" data-tool-id="insertBlank" 
                    data-options='${optionsString}'>${options.label}</li>`;
            }    
        }
        menuHTML += '</ul>';

        $target.html(menuHTML);
    }
};
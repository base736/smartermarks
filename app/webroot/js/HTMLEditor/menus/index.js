/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { ChangeAlignmentMenu } from './ChangeAlignmentMenu.js?7.0';

import { AddUnorderedListMenu } from './AddUnorderedListMenu.js?7.0';
import { AddOrderedListMenu } from './AddOrderedListMenu.js?7.0';

import { InsertVariableMenu } from './InsertVariableMenu.js?7.0';
import { InsertBlankMenu } from './InsertBlankMenu.js?7.0';
import { InsertTableMenu } from './InsertTableMenu.js?7.0';
import { InsertSymbolMenu } from './InsertSymbolMenu.js?7.0';

export function createMenus(context) {
    return {
        changeAlignment: new ChangeAlignmentMenu(context),
        addUnorderedList: new AddUnorderedListMenu(context),
        addOrderedList: new AddOrderedListMenu(context),
        insertVariable: new InsertVariableMenu(context),
        insertBlank: new InsertBlankMenu(context),
        insertTable: new InsertTableMenu(context),
        insertSymbol: new InsertSymbolMenu(context)
    }
};
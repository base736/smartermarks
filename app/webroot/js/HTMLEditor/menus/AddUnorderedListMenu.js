/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { ListBulletedTool } from '../tools/ListBulletedTool.js?7.0';
import { ListChecklistTool } from '../tools/ListChecklistTool.js?7.0';

export class AddUnorderedListMenu {
    constructor(context) {
        this.id = 'addUnorderedList';
        this.label = 'Unordered list';
        this.iconClass = 'edit_button_ulist';
        
        this.tools = [
            new ListBulletedTool(context), 
            new ListChecklistTool(context)
        ]
    }
    setMenu($target) {
        let menuHTML = '<ul style="width:130px;">';
        for (var i = 0; i < this.tools.length; ++i) {
            menuHTML += `
                <li class="edit_menu_button" data-tool-id="${this.tools[i].id}">
                    ${this.tools[i].menuRow}
                </li>
            `;
        }
        menuHTML += '</ul>';
        $target.html(menuHTML);
    }
};

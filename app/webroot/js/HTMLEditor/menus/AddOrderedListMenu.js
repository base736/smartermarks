/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { ListNumberedTool } from '../tools/ListNumberedTool.js?7.0';
import { ListLowercaseLettersTool } from '../tools/ListLowercaseLettersTool.js?7.0';
import { ListUppercaseLettersTool } from '../tools/ListUppercaseLettersTool.js?7.0';
import { ListLowercaseRomanTool } from '../tools/ListLowercaseRomanTool.js?7.0';
import { ListUppercaseRomanTool } from '../tools/ListUppercaseRomanTool.js?7.0';

export class AddOrderedListMenu {
    constructor(context) {
        this.id = 'addOrderedList';
        this.label = 'Ordered list';
        this.iconClass ='edit_button_olist';

        this.tools = [
            new ListNumberedTool(context),
            new ListLowercaseLettersTool(context),
            new ListUppercaseLettersTool(context),
            new ListLowercaseRomanTool(context),
            new ListUppercaseRomanTool(context)
        ];
    }

    setMenu($target) {
        let menuHTML = '<ul style="width:210px;">';
        for (var i = 0; i < this.tools.length; ++i) {
            menuHTML += `
                <li class="edit_menu_button" data-tool-id="${this.tools[i].id}">
                    ${this.tools[i].menuRow}
                </li>
            `;
        }
        menuHTML += '</ul>';
        $target.html(menuHTML);
    }
};

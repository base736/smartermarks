/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class InsertVariableMenu {
    constructor(context) {
        this.context = context;
        this.id = 'insertVariable';
        this.label = 'Variable';
        this.iconClass = 'edit_button_variable';
    }

    setMenu($target, defaultVariableType) {
        const variables = this.context.common.variables;
        
        var variableEntries = [];
        for (var key in variables) {
            if (variables.hasOwnProperty(key)) {
                for (var i = 0; i < variables[key].length; ++i) {
                    if ('displayName' in variables[key][i]) {
                        variableEntries.push({
                            variableName: variables[key][i].variableName,
                            displayName: variables[key][i].displayName
                        });	
                    }
                }
            }
        }
        variableEntries.sort(function(a, b) {
            return (a.displayName > b.displayName ? 1 : -1);
        });

        const newOptions = { variableType: defaultVariableType };
        const newOptionsString = htmlSingleQuotes(JSON.stringify(newOptions));
        
        var menuHTML = `
            <ul style="width:auto;">
                <li class="edit_menu_button variable_select" data-tool-id="insertVariable"
                    data-options='${newOptionsString}'>New variable...</li>
                <div style="height:0px; width:100%; margin:5px 0px; border-top:1px solid #eee;"></div>
        `;

        for (var i = 0; i < variableEntries.length; ++i) {
            const thisOptions = {
                variableName: variableEntries[i].variableName,
                displayName: variableEntries[i].displayName
            };
            const thisOptionsString = htmlSingleQuotes(JSON.stringify(thisOptions));

            menuHTML += `<li class="edit_menu_button variable_select" data-tool-id="insertVariable" 
                data-options='${thisOptionsString}'>${thisOptions.displayName}</li>`;
        }
        menuHTML += '</ul>';

        $target.html(menuHTML);
    }
};
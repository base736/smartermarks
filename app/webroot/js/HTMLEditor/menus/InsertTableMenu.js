/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class InsertTableMenu {
    constructor(context) {
        this.id = 'insertTable';
        this.label = 'Table';
        this.iconClass = 'edit_button_table';
    }

    setMenu($target) {
        var menuHTML = `<div class="edit_table_size">`;

        const tableMaxRows = 6;
        for (var j = 0; j < tableMaxRows; ++j) {
            for (var i = 0; i < tableMaxRows; ++i) {
                const thisOptions = {
                    rows: j + 1,
                    cols: i + 1
                };
                const thisOptionsString = htmlSingleQuotes(JSON.stringify(thisOptions));

                menuHTML += `
                    <div class='edit_table_block edit_menu_button'
                        style='grid-row:${j + 1}; grid-column:${i + 1}'
                        data-row='${j + 1}' data-col='${i + 1}'
                        data-tool-id='insertTable' data-options='${thisOptionsString}'>
                    </div>
                `;
            }
        }

        menuHTML += `
                </div>
                <ul style="width:100%;">
                    <li class="edit_menu_button" data-tool-id='insertTable'
                        style="padding:6px 4px;">More tables&hellip;</li>
                </ul>
            </div>
        `;

        $target.html(menuHTML);

        $target.on('mouseenter', '.edit_table_block', function(e) {
            var numCols = $(this).attr('data-col');
            var numRows = $(this).attr('data-row');
            var $parent = $(this).closest('.edit_table_size');
            $parent.find('.edit_table_block').each(function() {
                var thisCol = $(this).attr('data-col');
                var thisRow = $(this).attr('data-row');
                if ((thisRow <= numRows) && (thisCol <= numCols)) $(this).addClass('selected');
                else $(this).removeClass('selected');
            });            
        });
        
        $target.on('mouseleave', '.edit_table_size', function(e) {
            $target.find('.edit_table_block').removeClass('selected');
        });
    }
};
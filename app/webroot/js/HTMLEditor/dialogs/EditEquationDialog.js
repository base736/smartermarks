/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as HTMLEditorUtils from '../utils.js?7.0';

export class EditEquationDialog {
    static _initialized = false;
    static _activeContext = null;
    static _activeOptions = null;
    static _savedRanges = null;

    static showDialog(context, options) {
        if (this._activeContext) {
            throw new Error('EditEquationDialog: showDialog called again before previous is done');
        }

        this._saveSelection();

        let validOptions = true;
        if ('mode' in options) {
            if (options.mode == 'insert') {
                // No special checks needed for "insert"
            } else if (options.mode == 'edit') {
                if (!('target' in options)) validOptions = false;
            } else validOptions = false;
        } else validOptions = false;

        if (!validOptions) {
            throw new Error('EditEquationDialog: Invalid options in showDialog');
        }

        return new Promise((resolve, reject) => {
            this._activeContext = context;
            this._activeOptions = options;

            this._initializeDialog();
            this._initDialogInterface();

            // Set dialog buttons here so that we can call the promise resolve and reject

            const $dialog = $('#editEquation_dialog');

            $dialog.dialog('option', 'buttons', {
                Cancel: () => {
                    $dialog.dialog('close');
                    this._restoreSelection();
                    this._cleanup();
                    resolve('canceled');
                },
                Save: () => {
                    if (this._finishDialog()) {
                        $dialog.dialog('close');
                        this._cleanup();
                        resolve('done');
                    }
                }
            });

            $dialog.dialog('open');
        });
    }

    static _saveSelection() {
        this._savedRanges = [];
        var sel = window.getSelection();
        for (var i = 0; i < sel.rangeCount; i++) {
            this._savedRanges.push(sel.getRangeAt(i).cloneRange());
        }
    }

    static _restoreSelection() {
        if (this._savedRanges) {
            const sel = window.getSelection();
            sel.removeAllRanges();
            this._savedRanges.forEach(function(range) {
                sel.addRange(range);
            });
        }
    }

    static _initializeDialog() {
        if (this._initialized) return;
        this._initialized = true;

        const $dialog = $('#editEquation_dialog');

        // Set up the jQuery dialog

        $dialog.dialog({
            autoOpen: false,
            width: 430,
            modal: true,
            position: {
                my: "center center",
                at: "center center",
                of: window
            },
            open: function(event) {
                $('.ui-dialog-buttonpane').find('button').blur();
                $('.ui-dialog-buttonpane').find('button:contains("Cancel")')
                    .addClass('btn btn-danger btn_font');
                $('.ui-dialog-buttonpane').find('button:contains("Save")')
                    .addClass('btn btn-primary btn_font');
            }
        });

        $dialog.on('keypress', function(e) {
            if (e.key === 'Enter') {
                $dialog.parent().find('.ui-dialog-buttonpane button:contains("Save")').click();
                return false;
            }
        });
    }

    static _initDialogInterface() {
        $('#equation_text').equationeditor('attachEditor');
        if (this._activeOptions.mode == 'edit') {
            const json_data = this._activeOptions.target.attr('data-json');
            $('#equation_text').equationeditor('setJSON', json_data);
        }
    }

    static _finishDialog() {
        
        this._restoreSelection();

        const equationJSON = $('#equation_text').equationeditor('getJSON');
        if (this._activeOptions.mode == 'edit') {

            this._activeOptions.target.attr('data-json', htmlSingleQuotes(equationJSON));

        } else if (this._activeOptions.mode == 'insert') {

            // Insert HTML, then set JSON -- prevents innerHTML from mangling the JSON

            HTMLEditorUtils.insertHTML("<div id='eqn_temp' class='equation'></div>", 
                this._activeContext.editor.$element);
            $('#eqn_temp').attr('data-json', htmlSingleQuotes(equationJSON));
            $('#eqn_temp').removeAttr('id');

        }

        return true;
    }

    static _cleanup() {
        this._activeContext = null;
        this._activeOptions = null;
    }
}
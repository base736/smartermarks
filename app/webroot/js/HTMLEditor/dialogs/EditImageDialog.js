/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as HTMLEditorUtils from '../utils.js?7.0';
import { ImageUpload } from '/js/ImageUpload/ImageUpload.js?7.0';

export class EditImageDialog {
    static _initialized = false;
    static _activeContext = null;
    static _activeOptions = null;
    static _savedRanges = null;

    static _lastFile = null;
    static _resolve = null;
    static _reject = null;

    static showDialog(context, options) {
        if (this._activeContext) {
            throw new Error('EditImageDialog: showDialog called again before previous is done');
        }

        this._saveSelection();

        let validOptions = true;
        if ('mode' in options) {
            if (options.mode == 'insert') {
                // No special checks needed for "insert"
            } else if (options.mode == 'edit') {
                if (!('target' in options)) validOptions = false;
            } else validOptions = false;
        } else validOptions = false;

        if (!validOptions) {
            throw new Error('EditImageDialog: Invalid options in showDialog');
        }

        return new Promise((resolve, reject) => {
            this._activeContext = context;
            this._activeOptions = options;
            this._resolve = resolve;
            this._reject = reject;

            this._lastFile = null;

            this._initializeDialog();
            this._initDialogInterface();

            // Set dialog buttons here so that we can call the promise resolve and reject

            const $dialog = $('#editImage_dialog');

            if (this._activeOptions.mode == 'edit') {

                // Set up dialog for editing

                $dialog.dialog('option', 'buttons', {
                    Cancel: () => {
                        $dialog.dialog('close');
                        this._restoreSelection();
                        this._cleanup();
                        resolve('canceled');
                    },
                    Done: () => {
                        if (this._finishDialog()) {
                            $dialog.dialog('close');
                            this._cleanup();
                            resolve('done');
                        }
                    }
                });

            } else if (this._activeOptions.mode == 'insert') {

                // Set up dialog for adding a new image

                $dialog.dialog('option', 'buttons', {
                    Cancel: () => {
                        $dialog.dialog('close');
                        this._restoreSelection();
                        this._cleanup();
                        resolve('canceled');
                    },
                    Upload: () => {

                        const $dialog = $('#editImage_dialog');

                        if (!this._lastFile) {

                            $('#image_filename').validationEngine(
                                'showPrompt',
                                '* No image selected',
                                'error',
                                'bottomLeft',
                                true
                            );

                        } else if ('size' in this._lastFile && this._lastFile.size > 10000000) {

                            $('#image_filename').validationEngine(
                                'showPrompt',
                                '* Image files must be smaller than 10 MB',
                                'error',
                                'bottomLeft',
                                true
                            );

                        } else if ($dialog.find('form').validationEngine('validate')) {
                                            
                            this._startUpload(resolve, reject);

                        }
                    }
                });
            }

            $dialog.dialog('open');

            setTimeout(() => {
                if (!this._lastFile) {
                    $dialog.parent().find('button:contains("Upload")').attr('disabled', 'disabled');
                }
            }, 0);
        });
    }

    static _saveSelection() {
        this._savedRanges = [];
        var sel = window.getSelection();
        for (var i = 0; i < sel.rangeCount; i++) {
            this._savedRanges.push(sel.getRangeAt(i).cloneRange());
        }
    }

    static _restoreSelection() {
        if (this._savedRanges) {
            const sel = window.getSelection();
            sel.removeAllRanges();
            this._savedRanges.forEach(function(range) {
                sel.addRange(range);
            });
        }
    }

    static _initializeDialog() {
        if (this._initialized) return;
        this._initialized = true;

        const $dialog = $('#editImage_dialog');

        $dialog.dialog({
            autoOpen: false,
            width: 500,
            modal: true,
            position: {
                my: 'center center',
                at: 'center center',
                of: window
            },
            open: function() {
                $('#image_width').focus();

                $('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
                $('.ui-dialog-buttonpane').find('button:contains("Upload")').addClass('btn btn-primary btn_font');
                $('.ui-dialog-buttonpane').find('button:contains("Done")').addClass('btn btn-primary btn_font');
            },
            close: function() {
                $dialog.find('form').validationEngine('hideAll');
            }
        });

        // Set up validation engine

        $dialog.find('form').validationEngine({
            promptPosition: 'bottomLeft'
        });

        // Set up progress bar for file uploads

        $("#progressbar").progressbar();

        // Catch change event on the file input

        $('#image_upload').on('change', (e) => {
            const $dialogParent = $('#editImage_dialog').parent();
            const files = e.target.files;

            if (files && files[0]) {

                this._lastFile = files[0];
                $('#image_filename').text(this._lastFile.name);
                $dialogParent.find('button:contains("Upload")').removeAttr('disabled');

                $('#image_width').focus();

            } else {

                this._lastFile = null;
                $('#image_filename').text('No file selected');
                $dialogParent.find('button:contains("Upload")').attr('disabled', 'disabled');

            }
        });
    }

    static _initDialogInterface() {
        if (this._activeOptions.mode == 'edit') {

            $('.image_edit_container').show();
            $('.image_insert_container').hide();

            const $target = this._activeOptions.target;
            let imageWidth;
            if ($target.parent().is('.img_resize_wrapper')) {
                imageWidth = parseFloat($target.parent()[0].style.width);
            } else imageWidth = parseFloat($target[0].style.width);

            $('#image_width').val(imageWidth);

        } else if (this._activeOptions.mode == 'insert') {

            $('.image_edit_container').hide();
            $('.image_insert_container').show();

            $('#image_width').val('');
            $("#image_filename").html('No file selected');
            $("#progresswrapper").hide();
            
            if ('imageFile' in this._activeOptions) {
                const $dialogParent = $('#editImage_dialog').parent();

                this._lastFile = this._activeOptions.imageFile;
                $('#image_filename').text(this._lastFile.name);
                $dialogParent.find('button:contains("Upload")').removeAttr('disabled');

                $('#image_width').focus();
            }
        }
    }

    static _finishDialog() {
        const $dialog = $('#editImage_dialog');
        if ($dialog.find('form').validationEngine('validate')) {
            if (this._activeOptions.mode == 'edit') {
                this._restoreSelection();

                const newWidth = $('#image_width').val();
                this._activeOptions.target.css('width', newWidth + '%');
                return true;
            } else return false;
        } else return false;
    }

    static async _startUpload(resolve, reject) {
        $("#progresswrapper").show();
        $('#progressbar').progressbar('value', 0);

        const imageUpload = new ImageUpload(this._lastFile, (percent) => {
            $('#progressbar').progressbar('value', percent);
        });

        imageUpload.start()
        .then((responseData) => {

            this._restoreSelection();

            const widthPercent = $('#image_width').val() || '100';
            HTMLEditorUtils.insertHTML(
                `<img src='${responseData.imageURL}' style='width:${widthPercent}%' />`,
                    this._activeContext.editor.$element
            );

            $('#editImage_dialog').dialog('close');
            this._cleanup();
            resolve('done');

        }).catch((err) => {

            $.gritter.add({
                title: "Error",
                text: "Upload failed.",
                image: "/img/error.svg"
            });

            $("#progresswrapper").hide();
            reject(err);

        });
    }

    static _cleanup() {
        this._activeContext = null;
        this._activeOptions = null;
        this._lastData = null;
        this._resolve = null;
        this._reject = null;
        this._uploadTarget = null;
    }
}
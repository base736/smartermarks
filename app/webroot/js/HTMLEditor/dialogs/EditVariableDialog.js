/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as HTMLEditorUtils from '../utils.js?7.0';
import * as RenderTools from '/js/assessments/render_tools.js?7.0';

export class EditVariableDialog {
    static _initialized = false;    
    static _activeContext = null;
    static _activeOptions = null;
    static _savedRanges = null;

    static showDialog(context, options) {
        if (this._activeContext) {
            throw new Error('EditVariableDialog: showDialog called again before previous is done');
        }

        this._saveSelection();

        let validOptions = true;
        if ('mode' in options) {
            if (options.mode == 'insert') {
                if (!('variableType' in options)) validOptions = false;
            } else if (options.mode == 'edit') {
                if (!('target' in options)) validOptions = false;
            } else validOptions = false;
        } else validOptions = false;

        if (!validOptions) {
            throw new Error('EditVariableDialog: Invalid options in showDialog');
        }

        return new Promise((resolve, reject) => {
            this._activeContext = context;
            this._activeOptions = options;

            this._initializeDialog();
            this._initDialogInterface(options);

            // Set dialog buttons here so that we can call the promise resolve and reject

            const $dialog = $('#editVariable_dialog');

            $dialog.dialog('option', 'buttons', {
                Cancel: () => {
                    $dialog.dialog('close');
                    this._restoreSelection();
                    this._cleanup();
                    resolve('canceled');
                },
                Done: () => {
                    if (this._finishDialog()) {
                        $dialog.dialog('close');
                        this._cleanup();
                        resolve('done');
                    }
                }
            });

            $dialog.dialog('open');
        });
    }

    static _saveSelection() {
        this._savedRanges = [];
        var sel = window.getSelection();
        for (var i = 0; i < sel.rangeCount; i++) {
            this._savedRanges.push(sel.getRangeAt(i).cloneRange());
        }
    }

    static _restoreSelection() {
        if (this._savedRanges) {
            const sel = window.getSelection();
            sel.removeAllRanges();
            this._savedRanges.forEach(function(range) {
                sel.addRange(range);
            });
        }
    }

    static _initializeDialog() {
        if (this._initialized) return;
        this._initialized = true;

        const $dialog = $('#editVariable_dialog');

        // Set up the jQuery dialog

        $dialog.dialog({
            autoOpen: false,
            width: 395,
            modal: true,
            position: {
                my: 'center center',
                at: 'center center',
                of: window
            },
            open: function(event) {
                setTimeout(function() {
                    $('.ui-dialog-buttonpane')
                        .find('button:contains("Cancel")')
                        .addClass('btn btn-danger btn_font');
                    $('.ui-dialog-buttonpane')
                        .find('button:contains("Done")')
                        .addClass('btn btn-primary btn_font');

                    $('.variable_conditional .variable_equation').parent().blur();
                    $('#variable_display_name').focus();
                    $(document).trigger('click');
                }, 0);
            },
            close: function() {
                $dialog.find('form').validationEngine('hideAll');
            }
        });

        // Set up validation engine

        $dialog.find('form').validationEngine({
            promptPosition: 'bottomLeft'
        });

        // Event handlers

        $('#variable_type').on('change', () => {
            this._updateVariableSubtype();

            $('.variable_details').hide();
            $('.variable_' + $('#variable_subtype').val()).show();
            $('#variable_subtype').trigger('change');

            if ($('#variable_type').val() === 'constants') {
                $('#calculated_equation').blur();
                $('#var_constant_value').focus();
            } else if ($('#variable_type').val() === 'randomized') {
                $('#calculated_equation').blur();
                $('#var_randomized_min').focus();
            } else if ($('#variable_type').val() === 'calculated') {
                if ('activeElement' in document) {
                    document.activeElement.blur();
                }
                $('#calculated_equation').parent().focus();
            }
        });

        $('#variable_subtype').on('change', () => {
            console.log("HERE");
            $('.variable_details').hide();
            $('.variable_' + $('#variable_subtype').val()).show();

            const context = this._activeContext;
            if (!context) return;  // Safety check

            const userDefaults = context?.userDefaults;

            if ($('#variable_subtype').val() === 'randomized') {

                $('#var_randomized_min').val('');
                $('#var_randomized_max').val('');
                $('#var_randomized_step').val('');
                $('#var_sdType').val(userDefaults?.Question?.Engine?.sigDigsType);
                $('#var_sd').val(userDefaults?.Question?.Engine?.sigDigs);

                this._updateFormatInputs();
                $('#var_randomized_min').focus();

            } else if ($('#variable_subtype').val() === 'from_list') {

                $('#var_randomized_list').val('');
                $('#var_randomized_list').focus();

            } else if ($('#variable_subtype').val() === 'calculated') {

                $('.variable_calculated .variable_equation').equationeditor('attachEditor');

                this._updateFormatInputs();
                $('#calculated_equation').parent().focus();

            } else if ($('#variable_subtype').val() === 'conditional') {

                $('.variable_conditional .variable_equation').equationeditor('attachEditor');

                this._updateFormatInputs();

                $('.variable_conditional .variable_equation').parent().blur();
                $('#conditional_if').parent().focus();
            }
        });

        $('#var_sdType').on('change', () => {
            this._updateFormatInputs();
            $('#var_sd').focus();
        });

        $('#var_sd').spinner({
            stop: function(e, ui) {
                this._updateFormatInputs();
            }
        });
    }

    static _initDialogInterface(options) {
        const context = this._activeContext;

        let variableType = null;
        let thisVariable = {};

        if (options.mode == 'edit') {

            const variableName = options.target.attr('data-variable-name');
            for (let key in context.common.variables) {
                if (context.common.variables.hasOwnProperty(key)) {
                    for (let i = 0; i < context.common.variables[key].length; i++) {
                        if (context.common.variables[key][i].variableName === variableName) {
                            thisVariable = context.common.variables[key][i];
                            variableType = key;
                        }
                    }
                }
            }

        } else if (options.mode == 'insert') {

            variableType = options.variableType;
            thisVariable = {
                variableName: HTMLEditorUtils.newVariableName(context.common.variables),
                sdType: context.common.userDefaults.Question.Engine.sigDigsType,
                sd: context.common.userDefaults.Question.Engine.sigDigs
            }

        }

        $('#variable_variable_name').val(thisVariable.variableName);
        $('#variable_display_name').val(thisVariable.displayName ?? '');
        $('#variable_old_display_name').val(thisVariable.displayName ?? '');

        if (variableType === 'constant') {
            $('#variable_type').closest('.labeled-input').hide();
        } else {
            $('#variable_type').show();
        }

        $('.variable_details').hide();

        if (variableType == null) {
            $('#editVariable_dialog').dialog('option', 'title', 'Add variable');

        } else if (variableType === 'constants') {
            $('#editVariable_dialog').dialog('option', 'title', 'Edit constant');
            $('.variable_constants').show();

            $('#variable_type').val('constants');
            this._updateVariableSubtype();
            $('#variable_subtype').val('constants');

            $('#var_constant_value').val(thisVariable.value ?? '');

        } else if (variableType === 'randomized') {
            $('#editVariable_dialog').dialog('option', 'title', 'Edit variable');
            $('.variable_randomized').show();

            $('#variable_type').val('randomized');
            this._updateVariableSubtype();
            $('#variable_subtype').val('randomized');

            $('#var_randomized_min').val(thisVariable.min ?? '');
            $('#var_randomized_max').val(thisVariable.max ?? '');
            $('#var_randomized_step').val(thisVariable.step ?? '');
            $('#var_sd').val(thisVariable.sd ?? '');
            $('#var_sdType').val(thisVariable.sdType ?? '');
            this._updateFormatInputs();

        } else if (variableType === 'from_list') {
            $('#editVariable_dialog').dialog('option', 'title', 'Edit variable');
            $('.variable_from_list').show();

            $('#variable_type').val('randomized');
            this._updateVariableSubtype();
            $('#variable_subtype').val('from_list');

            $('#var_randomized_list').val(thisVariable.list ?? '');

        } else if (variableType === 'calculated') {

            $('#editVariable_dialog').dialog('option', 'title', 'Edit variable');
            $('.variable_calculated').show();

            if (thisVariable.json) {
                let json_data = thisVariable.json ?? {};
                for (let key in context.common.variables) {
                    if (context.common.variables.hasOwnProperty(key)) {
                        for (let i = 0; i < context.common.variables[key].length; i++) {
                            const varName = context.common.variables[key][i].variableName;
                            let dispName = (key === 'deleted') ? '<i>deleted</i>' : context.common.variables[key][i].displayName;
                            json_data = RenderTools.updateEquationJSON(json_data, varName, dispName);
                        }
                    }
                }
                thisVariable.json = JSON.stringify(json_data);
            }

            $('#variable_type').val('calculated');
            this._updateVariableSubtype();
            $('#variable_subtype').val('calculated');

            $('#calculated_equation').equationeditor('attachEditor');
            if (thisVariable.json) {
                $('#calculated_equation').equationeditor('setJSON', thisVariable.json);
            }

            $('#var_round').prop('checked', thisVariable.round ?? false);
            $('#var_sd').val(thisVariable.sd ?? '');
            $('#var_sdType').val(thisVariable.sdType ?? '');
            this._updateFormatInputs();

        } else if (variableType === 'conditional') {
            $('#editVariable_dialog').dialog('option', 'title', 'Edit variable');
            $('.variable_conditional').show();

            const json_keys = ['if_json', 'then_json', 'else_json'];
            json_keys.forEach(jsonKey => {
                if (thisVariable[jsonKey]) {
                    let json_data = thisVariable[jsonKey] ?? {};
                    for (let key in context.common.variables) {
                        if (context.common.variables.hasOwnProperty(key)) {
                            for (let i = 0; i < context.common.variables[key].length; i++) {
                                const varName = context.common.variables[key][i].variableName;
                                let dispName = (key === 'deleted') ? '<i>deleted</i>' : context.common.variables[key][i].displayName;
                                json_data = RenderTools.updateEquationJSON(json_data, varName, dispName);
                            }
                        }
                    }
                    thisVariable[jsonKey] = JSON.stringify(json_data);
                }
            });

            $('#variable_type').val('calculated');
            this._updateVariableSubtype();
            $('#variable_subtype').val('conditional');

            $('#conditional_if').equationeditor('attachEditor');
            if (thisVariable.if_json) {
                $('#conditional_if').equationeditor('setJSON', thisVariable.if_json);
            }

            $('#conditional_then').equationeditor('attachEditor');
            if (thisVariable.then_json) {
                $('#conditional_then').equationeditor('setJSON', thisVariable.then_json);
            }

            $('#conditional_else').equationeditor('attachEditor');
            if (thisVariable.else_json) {
                $('#conditional_else').equationeditor('setJSON', thisVariable.else_json);
            }

            $('#var_round').prop('checked', thisVariable.round ?? false);
            $('#var_sd').val(thisVariable.sd ?? '');
            $('#var_sdType').val(thisVariable.sdType ?? '');
            this._updateFormatInputs();
        }
    }

    static _updateVariableSubtype() {
        $('#variable_subtype').empty();
        const typeVal = $('#variable_type').val();
        if (typeVal === 'constants') {
            $('#variable_subtype').append('<option value="constants">Default</option>');
            $('#variable_subtype').val('constants');
            $('#variable_subtype').hide();
        } else if (typeVal === 'randomized') {
            $('#variable_subtype').append('<option value="randomized">From range</option>');
            $('#variable_subtype').append('<option value="from_list">From list</option>');
            $('#variable_subtype').val('randomized');
            $('#variable_subtype').show();
        } else if (typeVal === 'calculated') {
            $('#variable_subtype').append('<option value="calculated">From formula</option>');
            $('#variable_subtype').append('<option value="conditional">Conditional</option>');
            $('#variable_subtype').val('calculated');
            $('#variable_subtype').show();
        }
    }

    static _updateFormatInputs() {
        const $inputField = $('#var_sd');
        const roundingType = $('#var_sdType').val();
        const typeVal = $('#variable_type').val();

        if (roundingType === 'SigDigs' || roundingType === 'Scientific') {
            $inputField.attr('class', 'validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner');
            $inputField.spinner('option', 'min', 1);
            $('#var_sd_wrapper').show();
            if (typeVal === 'calculated') {
                $('#round_wrapper').show();
            } else {
                $('#round_wrapper').hide();
            }
            let label = 'sig dig';
            if ($('#var_sd').val() !== '1') label += 's';
            $('#var_sdLabel').html(label);
        } else if (roundingType === 'Decimals') {
            $inputField.attr('class', 'validate[required,custom[isNonNegativeInteger]] defaultTextBox2 intSpinner');
            $inputField.spinner('option', 'min', 0);
            $('#var_sd_wrapper').show();
            if (typeVal === 'calculated') {
                $('#round_wrapper').show();
            } else {
                $('#round_wrapper').hide();
            }
            let label = 'decimal';
            if ($('#var_sd').val() !== '1') label += 's';
            $('#var_sdLabel').html(label);
        } else {
            $('#var_sd_wrapper').hide();
            $('#var_round_wrapper').hide();
        }
    }

    static _finishDialog() {
        const $dialog = $('#editVariable_dialog');
        const context = this._activeContext;
        if (!context) return false;

        this._restoreSelection();

        const variableName = $('#variable_variable_name').val();
        const newDisplayName = $('#variable_display_name').val();
        const oldDisplayName = $('#variable_old_display_name').val();

        const entryType = $('#variable_subtype').val();
        let isValid = true;

        // Make sure variable name is unique if changed
        if (newDisplayName !== oldDisplayName) {
            let nameUsed = false;
            for (let type in context.common.variables) {
                if (context.common.variables.hasOwnProperty(type)) {
                    for (let i = 0; i < context.common.variables[type].length; i++) {
                        if (context.common.variables[type][i].displayName === newDisplayName) {
                            nameUsed = true;
                            break;
                        }
                    }
                }
            }
            if (nameUsed) {
                isValid = false;
                $('#variable_display_name').validationEngine(
                    'showPrompt',
                    '* Variable names must be unique',
                    'error',
                    'bottomLeft',
                    true
                );
            }
        }

        // If from_list, check the inputs
        let valueList;
        if (entryType === 'from_list') {
            valueList = $('#var_randomized_list').val().split(',');
            const valueRegex = /^-?(([1-9]\d*|0)(\.\d+)?|-?[1-9](\.\d+)?[Ee][+\-]?\d+)$/;
            for (let i = 0; i < valueList.length; i++) {
                valueList[i] = valueList[i].trim();
                if (!valueRegex.test(valueList[i])) {
                    isValid = false;
                }
            }
            if (!isValid) {
                $('#var_randomized_list').validationEngine(
                    'showPrompt',
                    '* Must be a comma-separated list of numbers',
                    'error',
                    'bottomLeft',
                    true
                );
            }
        }

        if (isValid && $dialog.find('form').validationEngine('validate')) {

            $dialog.dialog('close');

            const newEntry = {
                variableName: variableName,
                displayName: escapeHTML(newDisplayName) 
            };

            if (entryType === 'constants') {
                newEntry.value = $('#var_constant_value').val();
            } else if (entryType === 'randomized') {
                newEntry.min = $('#var_randomized_min').val();
                newEntry.max = $('#var_randomized_max').val();
                newEntry.step = $('#var_randomized_step').val();
                newEntry.sd = parseInt($('#var_sd').val());
                newEntry.sdType = $('#var_sdType').val();
            } else if (entryType === 'from_list') {
                newEntry.list = valueList;
            } else if (entryType === 'calculated') {
                const equationJSON = $('#calculated_equation').equationeditor('getJSON');
                const equationObject = JSON.parse(equationJSON);
                newEntry.json = equationObject;

                newEntry.round = $('#var_round').prop('checked');
                newEntry.sd = parseInt($('#var_sd').val());
                newEntry.sdType = $('#var_sdType').val();
            } else if (entryType === 'conditional') {
                let eqJSON = $('#conditional_if').equationeditor('getJSON');
                newEntry.if_json = JSON.parse(eqJSON);

                eqJSON = $('#conditional_then').equationeditor('getJSON');
                newEntry.then_json = JSON.parse(eqJSON);

                eqJSON = $('#conditional_else').equationeditor('getJSON');
                newEntry.else_json = JSON.parse(eqJSON);

                newEntry.round = $('#var_round').prop('checked');
                newEntry.sd = parseInt($('#var_sd').val());
                newEntry.sdType = $('#var_sdType').val();
            }

            if (oldDisplayName == '') {

                if (!(entryType in context.common.variables)) context.common.variables[entryType] = [];
                context.common.variables[entryType].push(newEntry);
    
                HTMLEditorUtils.insertHTML(`<span class='block_variable' contenteditable='false' unselectable='on'
                    data-variable-name='${variableName}'>${newDisplayName}</span>`, context.editor.$element);
    
            }  else {

                for (let type in context.common.variables) {
                    if (context.common.variables.hasOwnProperty(type)) {
                        let index = 0;
                        while (index < context.common.variables[type].length) {
                            if (context.common.variables[type][index].variableName === variableName) {
                                if (type === entryType) {
                                    // Same type, just update in place
                                    context.common.variables[type][index] = newEntry;
                                    index++;
                                } else {
                                    // Different type, remove from old list
                                    context.common.variables[type].splice(index, 1);
                                }
                            } else {
                                index++;
                            }
                        }
                    }
                }

            }

            return true;
        } else return false;
    }

    static _cleanup() {
        this._activeContext = null;
        this._activeOptions = null;
    }
}
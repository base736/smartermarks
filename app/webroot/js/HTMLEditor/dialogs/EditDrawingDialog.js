/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as HTMLEditorUtils from '../utils.js?7.0';

export class EditDrawingDialog {
    static _initialized = false;
    static _activeContext = null;
    static _activeOptions = null;
    static _savedRanges = null;

    static showDialog(context, options) {
        if (this._activeContext) {
            throw new Error('EditDrawingDialog: showDialog called again before previous is done');
        }

        this._saveSelection();

        let validOptions = true;
        if ('mode' in options) {
            if (options.mode == 'insert') {
                // No special checks needed for "insert"
            } else if (options.mode == 'edit') {
                if (!('target' in options)) validOptions = false;
            } else validOptions = false;
        } else validOptions = false;

        if (!validOptions) {
            throw new Error('EditDrawingDialog: Invalid options in showDialog');
        }

        return new Promise((resolve, reject) => {
            this._activeContext = context;
            this._activeOptions = options;

            this._initializeDialog();
            this._initDialogInterface();

            // Set dialog buttons here so that we can call the promise resolve and reject

            const $dialog = $('#editDrawing_dialog');

            $dialog.dialog('option', 'buttons', {
                Cancel: () => {
                    $dialog.dialog('close');
                    this._restoreSelection();
                    this._cleanup();
                    resolve('canceled');
                },
                Done: () => {
                    if (this._finishDialog()) {
                        $dialog.dialog('close');
                        this._cleanup();
                        resolve('done');
                    }
                }
            });

            $dialog.dialog('open');

        });
    }

    static _saveSelection() {
        this._savedRanges = [];
        var sel = window.getSelection();
        for (var i = 0; i < sel.rangeCount; i++) {
            this._savedRanges.push(sel.getRangeAt(i).cloneRange());
        }
    }

    static _restoreSelection() {
        if (this._savedRanges) {
            const sel = window.getSelection();
            sel.removeAllRanges();
            this._savedRanges.forEach(function(range) {
                sel.addRange(range);
            });
        }
    }

    static _initializeDialog() {
        if (this._initialized) return;
        this._initialized = true;

        const $dialog = $('#editDrawing_dialog');

        $('#editDrawing_dialog').dialog({
            autoOpen: false,
            width: 500,
            modal: true,
            position: {
                my: "center center",
                at: "center center",
                of: window
            },
            open: function() {
                $('#drawing_width').focus();

                $('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
                $('.ui-dialog-buttonpane').find('button:contains("Done")').addClass('btn btn-primary btn_font');
            },
            close: function() {
                $dialog.find('form').validationEngine('hideAll');
            }
        });

        // Set up validation engine

        $dialog.find('form').validationEngine({
            promptPosition: 'bottomLeft'
        });        
    }

    static _initDialogInterface() {
        if (this._activeOptions.mode == 'edit') {

            const $target = this._activeOptions.target;
            let drawingWidth;
            if ($target.parent().is('.drawing_resize_wrapper')) {
                drawingWidth = parseFloat($target.parent()[0].style.width);
            } else drawingWidth = parseFloat($target[0].style.width);

            $('#drawing_width').val(drawingWidth);

        } else if (this._activeOptions.mode == 'insert') {

            $('#drawing_width').val('');

        }
    }

    static _finishDialog() {
        this._restoreSelection();

        const $dialog = $('#editDrawing_dialog');
        if ($dialog.find('form').validationEngine('validate')) {
            const newWidth = $('#drawing_width').val();

            if (this._activeOptions.mode == 'edit') {
    
                this._activeOptions.target.css('width', newWidth + '%');
    
            } else if (this._activeOptions.mode == 'insert') {
    
                HTMLEditorUtils.insertHTML(
                    `<div class='html_drawing' style='width:${newWidth}%; aspect-ratio:1/1'
                        contenteditable='false' unselectable='on'>&nbsp;</div>`,
                    this._activeContext.editor.$element);
    
            }
            
            return true;
        } else return false;
    }

    static _cleanup() {
        this._activeContext = null;
        this._activeOptions = null;
        this._savedRanges = null;

    }
}
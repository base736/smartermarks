/**************************************************************************************************/
/* SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as HTMLEditorUtils from '../utils.js?7.0';

export class EditTableDialog {
    static _initialized = false;
    static _activeContext = null;
    static _activeOptions = null;
    static _savedRanges = null;
    static _cellData = [];  // Metadata about selected cells (borders, etc.)

    static showDialog(context, options) {
        if (this._activeContext) {
            throw new Error('EditVariableDialog: showDialog called again before previous is done');
        }

        this._saveSelection();

        let validOptions = true;
        if ('mode' in options) {
            if (options.mode == 'insert') {
                // No special checks needed for "insert"
            } else if (options.mode == 'edit') {
                if (!('selectedCells' in options)) validOptions = false;
            } else validOptions = false;
        } else validOptions = false;

        if (!validOptions) {
            throw new Error('EditDrawingDialog: Invalid options in showDialog');
        }

        return new Promise((resolve, reject) => {
            this._activeContext = context;
            this._activeOptions = options;

            this._initializeDialog();
            this._initDialogInterface();

            // Set dialog buttons here so that we can call the promise resolve and reject

            const $dialog = $('#editTable_dialog');

            $dialog.dialog('option', 'buttons', {
                Cancel: () => {
                    $dialog.dialog('close');
                    this._restoreSelection();
                    this._cleanup();
                    resolve('canceled');
                },
                Done: () => {
                    if (this._finishDialog()) {
                        $dialog.dialog('close');
                        this._cleanup();
                        resolve('done');
                    }
                }
            });

            $dialog.dialog('open');
        });
    }

    static _saveSelection() {
        this._savedRanges = [];
        var sel = window.getSelection();
        for (var i = 0; i < sel.rangeCount; i++) {
            this._savedRanges.push(sel.getRangeAt(i).cloneRange());
        }
    }

    static _restoreSelection() {
        if (this._savedRanges) {
            const sel = window.getSelection();
            sel.removeAllRanges();
            this._savedRanges.forEach(function(range) {
                sel.addRange(range);
            });
        }
    }

    static createNewTable(context, numRows, numCols, alignment) {        
        const colWidth = (100.0 / numCols).toFixed(3);
        const leftover = (100.0 - (numCols - 1) * parseFloat(colWidth)).toFixed(3);

        let tableHTML = "<table style='width:90%; margin:0px 5%;'>";
        for (let r = 0; r < numRows; r++) {
            tableHTML += '<tr>';
            for (let c = 0; c < numCols; c++) {
                tableHTML += `<td class="td_${alignment}" style="border-style:solid;`;
                if (r === 0) {
                    tableHTML += ` width:${(c < numCols - 1) ? colWidth : leftover}%;`;
                }
                tableHTML += '"><br></td>';
            }
            tableHTML += '</tr>';
        }
        tableHTML += '</table>';

        HTMLEditorUtils.insertHTML(tableHTML, context.editor.$element);
    }

    static _initializeDialog() {
        if (this._initialized) return;
        this._initialized = true;

        const $dialog = $('#editTable_dialog');

        $dialog.dialog({
            autoOpen: false,
            width: 500,
            modal: true,
            position: {
                my: "center center",
                at: "center center",
                of: window
            },
            open: function(event) {
                $('#image_width').focus();
                $('.ui-dialog-buttonpane')
                    .find('button:contains("Cancel")')
                    .addClass('btn btn-danger btn_font');
                $('.ui-dialog-buttonpane')
                    .find('button:contains("Done")')
                    .addClass('btn btn-primary btn_font');
            },
            close: function() {
                $dialog.find('form').validationEngine('hideAll');
            }
        });

        // Set up validation engine

        $dialog.find('form').validationEngine({
            promptPosition: 'bottomLeft'
        });

        // Set up the dialog event handlers
        
        $('#settings_table_rows, #settings_table_columns').spinner({
            step: 1,
            min: 1
        });

        $('#merge_cells_checkbox').on('change', () => {
            if (this._activeOptions.selectedCells.length === 0) return;
            const $selectedCells = this._activeOptions.selectedCells;

            let $tableRow = null;
            let sameRow = true;
            $selectedCells.each((index, cell) => {
                const $row = $(cell).closest('tr');
                if ($tableRow == null) $tableRow = $row;
                else if (!$row.is($tableRow)) sameRow = false;
            });

            if ($tableRow && sameRow) {
                const $table = $tableRow.closest('table');
                const widths = HTMLEditorUtils.getTableWidths($table);

                if ($('#merge_cells_checkbox').prop('checked')) {
                    let totalColumns = 0;
                    $selectedCells.each((index, cell) => {
                        totalColumns += cell.hasAttribute('colspan')
                            ? parseInt($(cell).attr('colspan'))
                            : 1;
                    });
                    $selectedCells.eq(0).attr('colspan', totalColumns);

                    const borderRight = $selectedCells.last().css('border-right-style');
                    $selectedCells.eq(0).css('border-right-style', borderRight);

                    for (let i = 1; i < $selectedCells.length; ++i) {
                        $selectedCells.eq(i).remove();
                    }

                } else {

                    $selectedCells.each((index, cell) => {
                        const $cell = $(cell);
                        const colspan = cell.hasAttribute('colspan') ? parseInt($cell.attr('colspan')) : 1;
                        $cell.removeAttr('colspan');

                        const borderStyle = $selectedCells.eq(0).css('border-style');
                        const classes = $selectedCells.eq(0).attr('class');

                        for (let i = 1; i < colspan; i++) {
                            $cell.after(`<td class='${classes} new_cell' style='border-style:${borderStyle};'><br></td>`);
                        }
                    });

                    $selectedCells = $selectedCells.add($tableRow.find('.new_cell'));
                    $selectedCells.removeClass('new_cell');
                }

                // Re-apply column widths
                HTMLEditorUtils.setTableWidths($table, widths);
            }
        });

        // Handle table border changes

        $('#borders_all').on('click', (e) => {
            const $selectedCells = this._activeOptions.selectedCells;
            if ($(e.currentTarget).prop('checked')) {
                $selectedCells.css('border-style', 'solid');
            }
        });

        $('#borders_none').on('click', (e) => {
            const $selectedCells = this._activeOptions.selectedCells;
            if ($(e.currentTarget).prop('checked')) {
                $selectedCells.css('border-style', 'none');
            }
        });

        $('#borders_inside').on('click', (e) => {
            const $selectedCells = this._activeOptions.selectedCells;
            const prop = $(e.currentTarget).prop('checked') ? 'solid' : 'none';
            $selectedCells.each((index, cell) => {
                const cData = this._cellData[index];
                if (!cData.isLeftCell) $(cell).css('border-left-style', prop);
                if (!cData.isRightCell) $(cell).css('border-right-style', prop);
                if (!cData.isTopCell) $(cell).css('border-top-style', prop);
                if (!cData.isBottomCell) $(cell).css('border-bottom-style', prop);
            });
        });

        $('#borders_outside').on('click', (e) => {
            const $selectedCells = this._activeOptions.selectedCells;
            const prop = $(e.currentTarget).prop('checked') ? 'solid' : 'none';
            $selectedCells.each((index, cell) => {
                const cData = this._cellData[index];
                if (cData.isLeftCell) $(cell).css('border-left-style', prop);
                if (cData.isRightCell) $(cell).css('border-right-style', prop);
                if (cData.isTopCell) $(cell).css('border-top-style', prop);
                if (cData.isBottomCell) $(cell).css('border-bottom-style', prop);
            });
        });

        $('.borders_single').on('click', (e) => {
            const $selectedCells = this._activeOptions.selectedCells;

            const borderType = $(e.currentTarget).attr('id'); // e.g. 'borders_top'
            let property = '';
            if (borderType === 'borders_top') {
                property = 'border-top-style';
            } else if (borderType === 'borders_bottom') {
                property = 'border-bottom-style';
            } else if (borderType === 'borders_left') {
                property = 'border-left-style';
            } else if (borderType === 'borders_right') {
                property = 'border-right-style';
            }

            const propValue = $(e.currentTarget).prop('checked') ? 'solid' : 'none';
            $selectedCells.css(property, propValue);

            // Hide appropriate border for adjacent cells if necessary

            if (propValue === 'none') {
                $selectedCells.each((index, cell) => {
                    if (borderType === 'borders_top' || borderType === 'borders_bottom') {
                        const $thisRow = $(cell).closest('tr');
                        const $otherRow = (borderType === 'borders_top') ? $thisRow.prev('tr') : $thisRow.next('tr');
                        if ($otherRow.length > 0) {

                            let selectedIndex = 0;
                            let selectedColspan = 0;
                            const $selectedCell = $(cell);

                            $thisRow.find('td').each(function() {
                                const colspan = this.hasAttribute('colspan')
                                    ? parseInt($(this).attr('colspan')) : 1;
                                if ($(this).is($selectedCell)) {
                                    selectedColspan = colspan;
                                    return false;
                                }
                                selectedIndex += colspan;
                            });

                            let otherIndex = 0;
                            $otherRow.find('td').each(function() {
                                const colspan = this.hasAttribute('colspan') ? parseInt($(this).attr('colspan')) : 1;
                                if ((otherIndex + colspan - 1 >= selectedIndex) &&
                                    (otherIndex < selectedIndex + selectedColspan)) {
                                    if (borderType === 'borders_top') {
                                        $(this).css('border-bottom-style', 'none');
                                    } else {
                                        $(this).css('border-top-style', 'none');
                                    }
                                }
                                otherIndex += colspan;
                            });
                        }
                    } else if (borderType === 'borders_left') {
                        $(cell).prev().css('border-right-style', 'none');
                    } else if (borderType === 'borders_right') {
                        $(cell).next().css('border-left-style', 'none');
                    }
                });
            }
        });

        $('.borders_checkbox').on('click', () => {
            const $selectedCells = this._activeOptions.selectedCells;

            // Force table repaint. Without this, borders aren't always redrawn.

            const $table = $selectedCells.eq(0).closest('table');
            $table.hide().show(0);

            this._updateBorderButtons();
        });

        $('#vertical_alignment').on('change', function() {
            if ($(this).val() !== 'many') {
                $(this).find('.alignment_many').remove();
            }
        });
    }

    static _initDialogInterface() {
        const $selectedCells = this._activeOptions.selectedCells;

        if ($selectedCells && $selectedCells.length > 0) {

            $('#settings_table_border').show();
            $('#settings_table_merge').show();

            // Gather some info to enable/disable merges
            let $tableRow = null;
            let sameRow = true;
            let hasMerged = false;
            let hasSingle = false;
            let numColumns = 0;
            let verticalAlign = '';

            $selectedCells.each(function() {
                const $cell = $(this);
                if ($tableRow === null) {
                    $tableRow = $cell.closest('tr');
                } else if (!$cell.closest('tr').is($tableRow)) {
                    sameRow = false;
                }
                const thisColspan = this.hasAttribute('colspan') ? parseInt($cell.attr('colspan')) : 1;
                numColumns += thisColspan;
                if (thisColspan > 1) hasMerged = true;
                else hasSingle = true;

                let thisAlign = '';
                if ($cell.hasClass('td_top')) thisAlign = 'top';
                else if ($cell.hasClass('td_middle')) thisAlign = 'middle';
                else if ($cell.hasClass('td_bottom')) thisAlign = 'bottom';

                if (thisAlign) {
                    if (!verticalAlign) verticalAlign = thisAlign;
                    else if (thisAlign !== verticalAlign) {
                        verticalAlign = 'many';
                    }
                }
            });

            // Set merge checkbox
            $('#merge_cells_checkbox').prop('checked', hasMerged);
            if (!sameRow || (hasMerged && hasSingle) || (numColumns === 1)) {
                $('#merge_cells_checkbox').attr('disabled', 'disabled');
                $('#merge_cells_checkbox_label').addClass('disabled');
            } else {
                $('#merge_cells_checkbox').removeAttr('disabled');
                $('#merge_cells_checkbox_label').removeClass('disabled');
            }

            // Vertical alignment

            if (verticalAlign === 'many') {
                $('#vertical_alignment').prepend("<option class='alignment_many' value='many'>Many</option>");
            } else {
                $('#vertical_alignment .alignment_many').remove();                
            }
            $('#vertical_alignment').val(verticalAlign);

            this._buildCellData();
            this._updateBorderButtons();

            // Hide or show #borders_inside if only one cell is selected
            if ($selectedCells.length === 1) {
                $('#borders_inside').parent().hide();
            } else {
                $('#borders_inside').parent().show();
            }

            // Update rows/columns spinners to match the table
            const $table = $selectedCells.eq(0).closest('table');
            const columnWidths = HTMLEditorUtils.getTableWidths($table); 
            const initRows = $table.find('tr').length;
            const initCols = columnWidths.length;

            $('#settings_table_rows').val(initRows);
            $('#settings_table_columns').val(initCols);

        } else {

            $('#settings_table_border').hide();
            $('#settings_table_merge').hide();
            
            $('#settings_table_rows').val('3');
            $('#settings_table_columns').val('3');
            $('#vertical_alignment').val('middle');

        }
    }

    static _finishDialog() {
        const context = this._activeContext;
        if (!context) return;

        this._restoreSelection();

        const $selectedCells = this._activeOptions.selectedCells;

        const $dialog = $('#editTable_dialog');
        if ($dialog.find('form').validationEngine('validate')) {

            const numRows = parseInt($('#settings_table_rows').val());
            const numCols = parseInt($('#settings_table_columns').val());
            const selectedAlignment = $('#vertical_alignment').val();
            const newAlignment = (selectedAlignment !== 'many') ? selectedAlignment : 'middle';

            if ($selectedCells && $selectedCells.length > 0) {
                const $table = $selectedCells.eq(0).closest('table');
                let columnWidths = HTMLEditorUtils.getTableWidths($table);
                const initRows = $table.find('tr').length;
                const initCols = columnWidths.length;

                // Possibly remove or add rows

                if (numRows < initRows) {

                    $table.find(`tr:nth-last-child(-n+${initRows - numRows})`).remove();

                } else if (numRows > initRows) {

                    // Insert new rows

                    let rowHTML = '<tr>';
                    for (let i = 0; i < initCols; i++) {
                        rowHTML += `<td class="td_${newAlignment}" style="border-style:solid;"></td>`;
                    }
                    rowHTML += '</tr>';

                    for (let i = initRows; i < numRows; i++) {
                        $table.append(rowHTML);
                    }

                }

                // Possibly remove or add columns

                if (numCols < initCols) {

                    // Remove rightmost columns
                    $table.find(`tr td:nth-last-child(-n+${initCols - numCols})`).remove();

                    // Re-normalize columnWidths so that they sum to 1.0
                    let totalWidth = 0.0;
                    for (let i = 0; i < numCols; i++) {
                        totalWidth += columnWidths[i];
                    }
                    const newW = [];
                    for (let i = 0; i < numCols; i++) {
                        newW.push(columnWidths[i] / totalWidth);
                    }
                    columnWidths = newW;

                } else if (numCols > initCols) {

                    // Add new columns
                    let columnHTML = '';
                    for (let i = initCols; i < numCols; i++) {
                        columnHTML += `<td class="td_${newAlignment}" style="border-style:solid;"></td>`;
                    }
                    $table.find('tr').each(function() {
                        $(this).append(columnHTML);
                    });

                    // Re-normalize widths
                    let totalWidth = 0.0;
                    for (let i = 0; i < initCols; i++) {
                        totalWidth += columnWidths[i];
                    }

                    const newW = [];
                    for (let i = 0; i < initCols; i++) {
                        const oldWidth = columnWidths[i] / totalWidth;
                        const newWidth = oldWidth * initCols / numCols;
                        newW.push(newWidth);
                    }
                    const addWidth = 1.0 / numCols;
                    for (let i = initCols; i < numCols; i++) {
                        newW.push(addWidth);
                    }
                    columnWidths = newW;

                }

                HTMLEditorUtils.setTableWidths($table, columnWidths);

                // Update alignment classes
                if (selectedAlignment !== 'many') {
                    $selectedCells.removeClass('td_top td_middle td_bottom');
                    $selectedCells.addClass('td_' + selectedAlignment);
                }

            } else {

                // If no selected cells => insert a new table

                this.createNewTable(context, numRows, numCols, newAlignment);

            }

            return true;
        } else return false;
    }

    static _buildCellData() {
        const $selectedCells = this._activeOptions.selectedCells;
        this._cellData = [];

        $selectedCells.each((index, cell) => {
            const $cell = $(cell);
            // Check left vs right
            let isLeftCell = false;
            if ($cell.is(":first-child")) {
                isLeftCell = true;
            } else if ($selectedCells.index($cell.prev()) === -1) {
                isLeftCell = true;
            }

            let isRightCell = false;
            if ($cell.is(":last-child")) {
                isRightCell = true;
            } else if ($selectedCells.index($cell.next()) === -1) {
                isRightCell = true;
            }

            // Check top vs bottom
            const $row = $cell.parent();
            let isTopCell = false;
            if ($row.is(":first-child")) {
                isTopCell = true;
            } else if ($selectedCells.index($row.prev().children().first()) === -1) {
                isTopCell = true;
            }

            let isBottomCell = false;
            if ($row.is(":last-child")) {
                isBottomCell = true;
            } else if ($selectedCells.index($row.next().children().first()) === -1) {
                isBottomCell = true;
            }

            this._cellData.push({
                isLeftCell,
                isRightCell,
                isTopCell,
                isBottomCell
            });
        });
    }

    static _updateBorderButtons() {
        const $selectedCells = this._activeOptions.selectedCells;

        $('.borders_checkbox').prop('checked', true);

        $selectedCells.each((index, cell) => {
            const $cell = $(cell);
            const cData = this._cellData[index];

            const hasLeft = ($cell.css('border-left-style') === 'solid');
            const hasRight = ($cell.css('border-right-style') === 'solid');
            const hasTop = ($cell.css('border-top-style') === 'solid');
            const hasBottom = ($cell.css('border-bottom-style') === 'solid');

            if (!hasLeft) $('#borders_left').prop('checked', false);
            if (!hasRight) $('#borders_right').prop('checked', false);
            if (!hasTop) $('#borders_top').prop('checked', false);
            if (!hasBottom) $('#borders_bottom').prop('checked', false);

            if (!hasLeft || !hasRight || !hasTop || !hasBottom) {
                $('#borders_all').prop('checked', false);
            }
            if (hasLeft || hasRight || hasTop || hasBottom) {
                $('#borders_none').prop('checked', false);
            }

            if (!hasLeft && cData.isLeftCell) $('#borders_outside').prop('checked', false);
            if (!hasRight && cData.isRightCell) $('#borders_outside').prop('checked', false);
            if (!hasTop && cData.isTopCell) $('#borders_outside').prop('checked', false);
            if (!hasBottom && cData.isBottomCell) $('#borders_outside').prop('checked', false);

            if (!hasLeft && !cData.isLeftCell) $('#borders_inside').prop('checked', false);
            if (!hasRight && !cData.isRightCell) $('#borders_inside').prop('checked', false);
            if (!hasTop && !cData.isTopCell) $('#borders_inside').prop('checked', false);
            if (!hasBottom && !cData.isBottomCell) $('#borders_inside').prop('checked', false);
        });
    }

    static _cleanup() {
        this._activeContext = null;
        this._activeOptions = null;
    }
}
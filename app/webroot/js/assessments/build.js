/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { localizationInstance } from '/js/FormEngine/Localization.js?7.0';

import * as Folders from '/js/common/folders.js?7.0';
import * as PreviewCommon from '/js/common/preview.js?7.0';
import * as VersioningCommon from '/js/common/versioning.js?7.0';
import * as StatsCommon from '/js/common/stats.js?7.0';
import * as ScrollingCommon from '/js/common/scrolling.js?7.0';
import * as TableSelection from '/js/common/table_selection.js?7.0';
import * as HtmlEdit from '/js/HTMLEditor/html_edit.js?7.0';
import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as AssessmentsRender from '/js/assessments/render.js?7.0';
import * as ClassListsCommon from '/js/classlists/common.js?7.0';

$.extend($.ui.dialog.prototype.options, {
	resizable: false,
    create: function() {
        var $this = $(this);

        $this.keypress(function(e) {
			if (e.key === 'Enter') {
				if ($this.attr('id') == 'manageOutcome_dialog') {
	                $this.parent().find('.ui-dialog-buttonpane .btn-save').click();
					return false;
				}
            }
        });
    }
});

var treeData = false;
var formTreeData = false;
var sittingTreeData = false;

var assessmentData;
var assessmentVersionID;
var questionData;

var previewTimer = null;

var roots = {};

var assessmentNewQuestions = [];
var showingOpen = false;

var needsShuffle = true;

var sortFolderQuestions = function() {
	var rows = $('#folder_questions').children('tbody').children('.question_row').get();
	if (!$('#sort_default').is(':visible')) {
		
	} else if (($('#sort_type').val() == 'last_used') && ($('#sort_type').attr('data-direction') == 'asc')) {
		rows.sort(function(a, b) {
			var aDate = parseInt($(a).attr('data-last-used'));
			var bDate = parseInt($(b).attr('data-last-used'));
			var aId = parseInt($(a).attr('data-question-id'));
			var bId = parseInt($(b).attr('data-question-id'));
			if (aDate > bDate) return 1;
			else if (aDate < bDate) return -1;
			else if (aId > bId) return 1;
			else if (aId < bId) return -1;
		});
	} else if (($('#sort_type').val() == 'last_used') && ($('#sort_type').attr('data-direction') == 'desc')) {
		rows.sort(function(a, b) {
			var aDate = parseInt($(a).attr('data-last-used'));
			var bDate = parseInt($(b).attr('data-last-used'));
			var aId = parseInt($(a).attr('data-question-id'));
			var bId = parseInt($(b).attr('data-question-id'));
			if (aDate < bDate) return 1;
			else if (aDate > bDate) return -1;
			else if (aId > bId) return 1;
			else if (aId < bId) return -1;
		});
	} else if (($('#sort_type').val() == 'name') && ($('#sort_type').attr('data-direction') == 'asc')) {
		rows.sort(function(a, b) {
			var aName = $(a).find('.question_name').text();
			var bName = $(b).find('.question_name').text();
			var aId = parseInt($(a).attr('data-question-id'));
			var bId = parseInt($(b).attr('data-question-id'));
			if (aName > bName) return 1;
			else if (aName < bName) return -1;
			else if (aId > bId) return 1;
			else if (aId < bId) return -1;
		});
	} else if (($('#sort_type').val() == 'name') && ($('#sort_type').attr('data-direction') == 'desc')) {
		rows.sort(function(a, b) {
			var aName = $(a).find('.question_name').text();
			var bName = $(b).find('.question_name').text();
			var aId = parseInt($(a).attr('data-question-id'));
			var bId = parseInt($(b).attr('data-question-id'));
			if (aName < bName) return 1;
			else if (aName > bName) return -1;
			else if (aId > bId) return 1;
			else if (aId < bId) return -1;
		});
	}
	$.each(rows, function(index, row) {
		$('#folder_questions').children('tbody').append(row);
	});
}

var showItems = function(parameters) {
	$('#item_search_menu').hide();
	$('#question_preview').html("");

	if (parameters['node_id'] == 'open') {
		$('#search_type_wrapper').hide();
		$('#search_type').val('folder');
	} else $('#search_type_wrapper').show();

	if ((parameters['node_id'] == 'open') && !('search' in parameters)) {

		var tableRows = '<tbody><tr id="folder_questions_empty" class="disabled"><td style="border:none;">'
		tableRows += '<div style="display:flex;">';
		tableRows += '<div style="flex:0 0 30px; padding:3px;"><img src="/img/info.svg" /></div>';
		tableRows += '<div style="flex:1 1 100%; padding-left:10px; line-height:normal; font-size:14px; font-family:Arial, Helvetica, sans-serif;">';
		tableRows += 'Click "Search" to find questions in the open bank. Questions in the open bank have been contributed by other teachers ';
		tableRows += '&mdash; please use them respectfully.';
		tableRows += '</div>';
		tableRows += '</div>';
		tableRows += '</td></tr></tbody>';
		$('#folder_questions').html(tableRows);
		$('#folder_questions').parent().animate({ scrollTop: 0 }, "fast");
		
		showingOpen = true;
		
	} else {

		if (parameters['node_id'] == 'open') {

			parameters['folder_ids'] = 'open';
            parameters['limit'] = 50;

		} else {

			var nodeID = parameters['node_id'];
			var rootNode = Folders.getNode(treeData, nodeID);

			var folderIDs = [];
			if (('where' in parameters) && (parameters['where'] != 'folder')) {
				var searchNodes;
				if (parameters['where'] == 'subfolders') searchNodes = [rootNode];
				else if (parameters['where'] == 'all') searchNodes = treeData;
	
				for (var j = 0; j < searchNodes.length; ++j) {
					var descendants = Folders.getDescendants(searchNodes[j]);
					for (var i = 0; i < descendants.length; ++i) {
						var folder_id = descendants[i].data.folder_id;
						if (!(folder_id in folderIDs)) folderIDs.push(folder_id);
					}
				}
			} else folderIDs = [rootNode['data']['folder_id']];
	
			parameters['folder_ids'] = folderIDs;
	        parameters['limit'] = 'all';

		}

		delete parameters['node_id'];
		delete parameters['where'];
        
		if (pendingAJAXPost) return;
		pendingAJAXPost = true;

		const queryString = new URLSearchParams(parameters).toString();
		ajaxFetch(`/Questions/get_index?${queryString}`)
		.then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				var tableHTML = "<tr id='folder_questions_empty' class='disabled'><td style='border:none;'><i>No questions to show here</i></td></tr>";
				if (responseData.results.length > 0) {

					var addQuestionData = JSON.parse($('#addItem_dialog').attr('data-selected'));

					for (var i = 0; i < responseData.results.length; ++i) {
						var entry = responseData.results[i];

						var isIncluded = false;
						for (var j = 0; j < addQuestionData.length; ++j) {
							if (addQuestionData[j].question_id == entry.Question.id) isIncluded = true;
						}

						var json_data = JSON.parse(entry.Question.json_data);
		
						var typeIcon = "";
						var questionTypes = [];
						for (var j = 0; j < json_data.Parts.length; ++j) {
							if (json_data.Parts[j].type != 'context') {
								var thisType = json_data.Parts[j].type.substr(0, 2);
								if (typeIcon == "") typeIcon = thisType;
								else if (typeIcon != thisType) typeIcon = "mixed";
								questionTypes.push(thisType);
							}
						}

						var lastUsed, lastUsedString;
						if (entry.Question.last_used.length > 0) {
							var lastUsedData = JSON.parse(entry.Question.last_used);
							if (userID in lastUsedData) {
								var date = dayjs.utc(lastUsedData[userID]).local();
								lastUsed = date.unix();
								lastUsedString = date.format('MMM D, YYYY');
							} else {
								lastUsed = 0;
								lastUsedString = '---';	
							}
						} else {
							lastUsed = 0;
							lastUsedString = '---';
						}

						var questionName = ('QuestionHandle' in entry) ? entry.QuestionHandle.name_alias : entry.Question.name;
						
						tableHTML += 
							"<tr id='question_row_" + entry.Question.id + "' class='question_row'" +
								"data-question-id='" + entry.Question.id + "' " +
								"data-scoring-hash='" + entry.Question.scoring_hash + "' " + 
								"data-question-types='" + JSON.stringify(questionTypes) + "' " + 
								"data-last-used='" + lastUsed + "'> \
								<td style='width:15px; vertical-align:top;'> \
									<input type='checkbox' class='item_select'" + (isIncluded ? ' checked' : '') + "/> \
								</td> \
								<td> \
								<img class='type_icon' style='margin-right:5px; display:inline-block;' src='/img/icons/type_" + typeIcon + ".png' /><span class='question_name'>" + questionName + "</span> \
								</td> \
									<td style='width:85px; text-align:right;'>" + lastUsedString + "</td> \
								</tr>";
					}
				}

				$('#folder_questions').html('<tbody>' + tableHTML + '</tbody>');
				$('#folder_questions').parent().animate({ scrollTop: 0 }, "fast");
				updateAllowedQuestions();
				sortFolderQuestions();

			} else {
				$.gritter.add({
					title: "Error",
					text: "Unable to load index.",
					image: "/img/error.svg"
				});
			}
		}).catch(function() {
			showSubmitError();
		});

		showingOpen = false;
	}
}

var updateAllowedQuestions = function() {
	var $usedQuestions = $();
	var $wrongTypeQuestions = $();
	var $allowedQuestions = $();
	
	var allowedTypes = [];
	$('#addItem_dialog .type_checkbox').each(function() {
		if ($(this).prop('checked')) allowedTypes.push($(this).attr('data-type'));
	});
	
	var usedQuestionIDs = [];
	for (var i = 0; i < assessmentData.Sections.length; ++i) {
		if (assessmentData.Sections[i].type == 'page_break') continue;
		for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
			if (assessmentData.Sections[i].Content[j].type == 'shuffle_break') continue;

			var thisEntry = assessmentData.Sections[i].Content[j];
			if (thisEntry.type == 'item') {
				var questionID = questionData[thisEntry.question_handle_id].id;
				usedQuestionIDs.push(questionID);	
			} else if (thisEntry.type == 'item_set') {
				for (var k = 0; k < thisEntry.question_handle_ids.length; ++k) {
					var questionHandleID = thisEntry.question_handle_ids[k];
					var questionID = questionData[questionHandleID].id;
					usedQuestionIDs.push(questionID);
				}
			} 
		}
	}

	var scoringHash = false;
	if ($('#addItem_dialog')[0].hasAttribute('data-scoring-hash')) {
		scoringHash = $('#addItem_dialog').attr('data-scoring-hash');
	}

	$('.question_row').each(function() {
		var questionId = parseInt($(this).attr('data-question-id'));
		if (usedQuestionIDs.indexOf(questionId) >= 0) {
			$usedQuestions = $usedQuestions.add($(this));
		} else {
			var isAllowed = true;

			if (scoringHash !== false) {
				if (scoringHash != $(this).attr('data-scoring-hash')) isAllowed = false;
			} else {
				var questionTypes = JSON.parse($(this).attr('data-question-types'));
				for (var i = 0; i < questionTypes.length; ++i) {
					if (allowedTypes.indexOf(questionTypes[i]) == -1) isAllowed = false;
				}
			}

			if (!isAllowed) $wrongTypeQuestions = $wrongTypeQuestions.add($(this));
			else $allowedQuestions = $allowedQuestions.add($(this));
		}
	});
	
	$usedQuestions.hide();
	$wrongTypeQuestions.hide();
	$allowedQuestions.show();
	
	var totalHidden = $usedQuestions.length + $wrongTypeQuestions.length;
	if (totalHidden > 0) {
		$('#hidden_questions').html('(Hiding ' + totalHidden.toString() + ')');
		$('#hidden_questions').show();
	} else $('#hidden_questions').hide();

	if ($allowedQuestions.length > 0) $('#folder_questions_empty').hide();
	else $('#folder_questions_empty').show();
}

var initTypeCheckboxes = function() {
	if ($('#folder_questions').attr('data-section-type') == 'mixed') {
		$('#addItem_dialog .type_checkbox').prop('checked', true);
		$('#type_checkbox_wrapper').find('input').removeAttr("disabled");
		$('#type_checkbox_wrapper').find('label').removeClass("disabled");
	} else {
		$('#addItem_dialog .type_checkbox').prop('checked', false);
		$('#show_' + $('#folder_questions').attr('data-section-type').substr(0, 2)).prop('checked', true);
		$('#type_checkbox_wrapper').find('input').attr("disabled", "disabled");
		$('#type_checkbox_wrapper').find('label').addClass("disabled");
	}

	updateAllowedQuestions();
}

document.addEventListener("DOMContentLoaded", async function() {

    await PreviewCommon.initialize();
    ScrollingCommon.initialize();
    await AssessmentsRender.initialize();
    TableSelection.initialize();
    await RenderTools.initialize();
    await HtmlEdit.initialize();
    ClassListsCommon.initialize();

	StatsCommon.initFilterInterface();

	var blurWaiting = [];
    var focusWaiting = [];

	var $draggingSection = null;
	var $draggingItem = null;
	var draggingTimer = null;

    var listStep = 0;

	var initialize = async function() { 
		const response = await ajaxFetch('/Assessments/get/' + assessmentID);
		const responseData = await response.json();

		if (responseData.success) {
			var isValid = true;
			if (!('Assessment' in responseData)) isValid = false;
			else if (!('data_string' in responseData.Assessment)) isValid = false;
			else if (!('data_hash' in responseData.Assessment)) isValid = false;
			else if (responseData.Assessment.data_hash != SparkMD5.hash(responseData.Assessment.data_string)) isValid = false;

			if (isValid) {

				// Load assessment data

				assessmentData = JSON.parse(responseData.Assessment.data_string);
				localizationInstance.setLanguage(assessmentData.Settings.language);

				assessmentVersionID = responseData.Assessment.version_id;

				// Initialize used IDs

				for (var i = 0; i < assessmentData.Sections.length; ++i) {
					usedIDs.push(assessmentData.Sections[i].id);
				}

				for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
					usedIDs.push(assessmentData.Outcomes[i].id);
				}

				// Load question data

				questionData = {};
				for (var id in responseData.Questions) {
					if (responseData.Questions.hasOwnProperty(id)) {
						var thisData = PreviewCommon.parseQuestionData(responseData.Questions[id]);
						if (thisData !== false) {
							var questionHandleID = thisData.question_handle_id;
							questionData[questionHandleID] = thisData;
						}
					}
				}

				StatsCommon.initFullStatistics(responseData.Statistics);

				// Initialize interface

				initFolderTrees();

				$('#document_title').text(responseData.Assessment.save_name);
				if (!canEdit) $('#document_title').append(' &mdash; <b>READ ONLY</b>');

				$('#loading_panel').hide();
				$('#view_menu').menu('enable');			
	
				$('#assessment_preview_wrapper').attr('data-needs-render', 1);
				$('#sections_panel').attr('data-needs-render', 1);
		
				// Shuffle assessment parts

				AssessmentsRender.shuffleAssessmentParts(assessmentData, questionData);

				// Show the selected view

				triggerActiveView();

				// Enable settings button

				$('#document_options').removeAttr('disabled');

				// Open dialogs as required

				if (showIntroNote == 1) $('#intro_note').dialog('open');
				else if (showSettings) $("#document_options").trigger("click");
				return;

			} else {

                throw new Error('Invalid assessment data.');

            }
		} else {

			throw new Error('Failed to fetch assessment data.');
			
		}
	}

	var triggerActiveView = function() {
	
		// Show the selected view

		var urlVars = getUrlVars();

		if ('view' in urlVars) {
			var $viewButton = $('.view_button[data-view-type="' + urlVars.view + '"]:visible');
			if ($viewButton.length == 1) $viewButton.trigger('click');
			else $('.view_button:visible').first().trigger('click');
		} else $('.view_button:visible').first().trigger('click');
	}

	$(".view_button").click(function(e) {
		e.preventDefault();
		if (!$("#view_menu").menu("option", "disabled")) {
			var urlVars = getUrlVars();
			urlVars.view = $(this).attr('data-view-type');
			setUrlVars(urlVars);

			$(".view_button").removeClass("ui-state-active");
			$(this).addClass("ui-state-active");

			drawView();
		}
	});

	var drawView = function() {
		$('.right_panel').hide();

		var urlVars = getUrlVars();
		if (urlVars.view == 'preview') {
			$('.templates_wrapper').show();
			$('#preview_panel').show();
			drawPreview();
		} else if (urlVars.view == 'outcomes') {
			$('#wizard_wrapper').show();
			$('#outcomes_panel').show();
			drawOutcomes();
		} else if (urlVars.view == 'statistics') {
			$('#statistics_wrapper').show();
			$('#statistics_panel').show();
			drawStatistics();
		}
	}

	var drawPreview = async function() {
		await RenderTools.setRenderStyle(assessmentData.Settings.numberingType);

		$('.buildButton').attr('disabled', 'disabled');

		if ($('#assessment_preview_wrapper')[0].hasAttribute('data-needs-render')) {
			await drawAssessmentPreview();

			if (assessmentData.Settings.mcFormat == 'Auto') {
				$('#assessment_preview_wrapper .part_wrapper[data-type="mc"]').each(function() { 
					RenderTools.setMCWidths($(this)); 
				});
			} else $('#assessment_preview_wrapper .question_answer').css('width', '100%');	

			$('#assessment_preview_wrapper .part_wrapper').each(function() {
				if ($(this).find('.question_answer_mc, .question_answer_table').length > 0) {
					RenderTools.setMCImageWidths($(this));
				}
			});

			$('#assessment_preview_wrapper')[0].removeAttribute('data-needs-render');
		}
		
		// Shuffle if required

		if (needsShuffle) {
			AssessmentsRender.shuffleAssessmentAnswers(assessmentData, questionData, $('#assessment_preview_wrapper'));
			needsShuffle = false;
		}

		// Renumber			

		await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($('#assessment_preview_wrapper'));

		$('.buildButton').removeAttr('disabled');

		if ($('#sections_panel')[0].hasAttribute('data-needs-render')) {
			drawLeftBar();
		}

		$('#sections_panel .edit_control').show();
		$('.items_panel .edit_control').show();

		$('#sections_menu, .items_menu').sortable('option', 'disabled', false);

		var urlVars = getUrlVars();
		if ('c' in urlVars) {
	
			// Highlight question
			
			var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + urlVars.c + '"]');
			$itemWrapper.addClass('question_selected');
		}

		ScrollingCommon.resetLeftPanel();
	}
	
	var drawOutcomes = async function() {
		if ($('#sections_panel')[0].hasAttribute('data-needs-render')) {
			drawLeftBar();
		}

        $('#sections_panel .edit_control').hide();
        $('.items_panel .edit_control').hide();

		$('#sections_menu, .items_menu').sortable('option', 'disabled', true);
	
		var urlVars = getUrlVars();

		await RenderTools.setRenderStyle(assessmentData.Settings.numberingType);

		if ('c' in urlVars) {
			var questionHandleID = false;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					var itemData = assessmentData.Sections[i].Content[j];
					if (itemData.id == urlVars.c) {
						if (itemData.type == 'item') {
							questionHandleID = itemData.question_handle_id;
						} else if (itemData.type == 'item_set') {
							var renderedIndex = ('rendered_index' in itemData) ? itemData.rendered_index : 0;
							questionHandleID = itemData.question_handle_ids[renderedIndex];
						}
					}
				}
			}

			if (questionHandleID !== false) {

				var $itemWrapper = $('<div class="question_wrapper" data-content-id="' + urlVars.c + '"' +
					' data-question-handle-id="' + questionHandleID + '"></div>');
				$('#outcomes_item').empty().append($itemWrapper);
	
				await AssessmentsRender.renderItemToDiv(questionData[questionHandleID].json_data, $itemWrapper);
				await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($('#outcomes_item'));

				$itemWrapper.find('.part_wrapper[data-type="mc"] .question_answer').css('width', '100%');

			} else {

				$('#outcomes_item').html('<i>Error loading question.</i>');
	
			}
			
		} else {

			$('#outcomes_item').html('<i>No question selected.</i>');

		}
	
		rebuildOutcomesList();
		ScrollingCommon.resetLeftPanel();
	}

	var drawStatistics = async function() {
		$("#stats_print").attr('disabled', 'disabled');

		if ($('#sections_panel')[0].hasAttribute('data-needs-render')) {
			drawLeftBar();
		}

        $('#sections_panel .edit_control').hide();
        $('.items_panel .edit_control').hide();
        $('.items_panel .edit_icon').show();

		$('#sections_menu, .items_menu').sortable('option', 'disabled', true);

		var question_ids = [];
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				if (assessmentData.Sections[i].Content[j].type == 'item') {
					var questionHandleID = assessmentData.Sections[i].Content[j].question_handle_id;
					question_ids.push(questionData[questionHandleID].id);	
				} else if (assessmentData.Sections[i].Content[j].type == 'item_set') {
					for (var k = 0; k < assessmentData.Sections[i].Content[j].question_handle_ids.length; ++k) {
						var questionHandleID = assessmentData.Sections[i].Content[j].question_handle_ids[k];
						question_ids.push(questionData[questionHandleID].id);		
					}
				}
			}
		}

		$('#statistics_graphs').html("<div style='font-style:italic'>Loading statistics</div>");

		var urlVars = getUrlVars();

		await RenderTools.setRenderStyle(assessmentData.Settings.numberingType);

		if ('c' in urlVars) {
			var questionHandleID = false;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					var itemData = assessmentData.Sections[i].Content[j];
					if (itemData.id == urlVars.c) {
						if (itemData.type == 'item') {
							questionHandleID = itemData.question_handle_id;
						} else if (itemData.type == 'item_set') {
							var renderedIndex = ('rendered_index' in itemData) ? itemData.rendered_index : 0;
							questionHandleID = itemData.question_handle_ids[renderedIndex];
						}
					}
				}
			}

			if (questionHandleID !== false) {

				$('#item_statistics_data').html("<div style='font-style:italic'>Loading statistics</div>");
				$('#statistics_n').html('0');
	
				$('#statistics_item').attr('data-question-handle-id', questionHandleID);
				$('#statistics_item').html('<div class="question_wrapper" data-content-id="' + urlVars.c + '"></div>');
				var $itemWrapper = $('#statistics_item').find('.question_wrapper');
	
				await AssessmentsRender.renderItemToDiv(questionData[questionHandleID].json_data, $itemWrapper);
				await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($('#statistics_item'));

				$itemWrapper.find('.part_wrapper[data-type="mc"] .question_answer').css('width', '100%');
	
				var questionID = questionData[questionHandleID].id;
				if (questionID in StatsCommon.fullStatistics) {
					StatsCommon.highlightQuestion($itemWrapper, StatsCommon.fullStatistics[questionID], questionData[questionHandleID].json_data);
				}

			} else {

				$('#statistics_item').html('<i>Error loading question.</i>');
				$('#item_statistics_wrapper').hide();
	
			}
		
		} else {

			$('#statistics_item').html('<i>No question selected.</i>');
			$('#item_statistics_wrapper').hide();

		}

		await StatsCommon.loadFilteredStatistics(question_ids);

		var urlVars = getUrlVars();
		if ('c' in urlVars) {
			var questionHandleID = parseInt($('#statistics_item').attr('data-question-handle-id'));
			var questionID = questionData[questionHandleID].id;

			StatsCommon.rebuildItemStatistics(questionID, questionData[questionHandleID].json_data, assessmentData.Settings.numberingType);
		}

		var questionHandleIDs = StatsCommon.getQuestionHandleIDs(assessmentData);
		StatsCommon.rebuildAssessmentStatistics(questionHandleIDs, questionData);

		ScrollingCommon.resetLeftPanel();

		$("#stats_print").removeAttr('disabled');
	}

	var drawLeftBar = function() {
		
		$('#sections_menu').empty();
		$('.items_panel').remove();
		
		if ('Preamble' in assessmentData) {
			$('#preamble_menu').show();
			$('#addPreamble').hide();
		} else {
			$('#preamble_menu').hide();
			$('#addPreamble').show();
		}

		var activeSectionID = null;
		var activeContentID = null;

		var urlVars = getUrlVars();
		if ('s' in urlVars) {
			activeSectionID = urlVars.s;
		} else if ('c' in urlVars) {
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					if (assessmentData.Sections[i].Content[j].id == urlVars.c) {
						activeSectionID = assessmentData.Sections[i].id;
					}	
				}
			}

			if (activeSectionID != null) {
				activeContentID = urlVars.c;
			}
		}

		var foundSection = false;

		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			var sectionData = assessmentData.Sections[i];
			if (sectionData.id == activeSectionID) foundSection = true;

			if (sectionData.type == 'page_break') {

				var menuItemHTML = 
					'<li class="formatting_item"><div class="menu_item" data-section-id="' + sectionData.id + '"> \
						<div class="menu_text">Page break</div>';
				if (canEdit) menuItemHTML += '<div title="Delete" class="edit_control menu_button delete_icon"></div>';
				menuItemHTML += '</div></li>';

				var $sectionMenuItem = $(menuItemHTML);
				$('#sections_menu').append($sectionMenuItem);
	
			} else {

				var sectionMenuText = getSectionMenuText(sectionData);
				var menuItemHTML = 
					'<li><div class="menu_item" data-section-id="' + sectionData.id + '">' + 
					'<div class="menu_text">' + escapeHTML(sectionMenuText) + '</div>';
				if (canEdit) {
					menuItemHTML += '<button title="Edit" class="edit_control menu_button edit_icon"></button>';
					menuItemHTML += '<button title="Delete" class="edit_control menu_button delete_icon"></button>';
				}
				menuItemHTML += '</div></li>';	

				var $sectionMenuItem = $(menuItemHTML);
				$('#sections_menu').append($sectionMenuItem);
	
				var detailPanelHTML =
					'<div class="items_panel" id="items_panel_' + sectionData.id + '" data-section-id="' + sectionData.id + '" style="display:none;"> \
						<div class="window_bar">' + escapeHTML(sectionData.header) + '</div> \
						<div class="window_content" style="padding:5px; overflow:visible;"> \
							<div class="layout_panel"> \
								<div class="empty_layout" style="width:255px; padding:10px; color:#eee; font-size:13px; font-style:italic;">No questions in this section. Click "Add Questions" below to begin.</div> \
								<ul class="items_menu menu_wrapper">';
	
				if (sectionData.Content.length > 0) {
					for (var j = 0; j < sectionData.Content.length; ++j) {
						var itemData = sectionData.Content[j];

						if (itemData.type == 'item') {

							var thisData = questionData[itemData.question_handle_id];
				
							detailPanelHTML += 
								'<li><div class="menu_item question content_item" data-content-id="' + itemData.id + '" data-question-handle-id="' + thisData.question_handle_id + '">' +
								'<div class="menu_text" title="' + escapeHTML(thisData.name) + '">' + escapeHTML(thisData.name) + '</div>' +
								'<button title="Edit" class="edit_control menu_button edit_icon"></button>';
							if (canEdit) detailPanelHTML += '<button title="Delete" class="edit_control menu_button delete_icon "></button>';
							detailPanelHTML += '</div></li>';
				
						} else if (itemData.type == 'item_set') {

							detailPanelHTML += 
								'<li><div class="menu_item question_set content_item" data-content-id="' + itemData.id + '">' +
								'<div class="question_set_reveal"></div>' +
								'<div class="menu_text">' +
                                    '<span class="menu_count">(' + itemData.question_handle_ids.length + ' items)</span> ' + 
                                    '<span class="menu_name" title="' + escapeHTML(itemData.name) + '">' + escapeHTML(itemData.name) + '</span>' + 
                                '</div>';
								if (canEdit) {
									detailPanelHTML += '<button title="Edit" class="edit_control menu_button edit_icon "></button>';
									detailPanelHTML += '<button title="Delete" class="edit_control menu_button delete_icon "></button>';
								}
								detailPanelHTML += '</div>';

							detailPanelHTML += '<ul class="menu_wrapper child_menu" style="min-height:0px;">';
							for (var k = 0; k < itemData.question_handle_ids.length; ++k) {
								var thisData = questionData[itemData.question_handle_ids[k]];
				
								detailPanelHTML += 
									'<li><div class="menu_item question child_item" style="display:none;" data-question-handle-id="' + thisData.question_handle_id + '">' +
									'<div class="menu_text" title="' + escapeHTML(thisData.name) + '">' + escapeHTML(thisData.name) + '</div>';
								if (canEdit) {
									detailPanelHTML += 
										'<button title="Edit" class="edit_control menu_button edit_icon"></button>' +
										'<button title="Delete" class="edit_control menu_button delete_icon "></button>';
								}
								detailPanelHTML += '</div></li>';
	
							}
							detailPanelHTML += '</ul>';

							detailPanelHTML += '</li>';
				
						} else if (itemData.type == 'shuffle_break') {
							
							detailPanelHTML += 
								'<li class="formatting_item"><div class="menu_item content_item" data-content-id="' + itemData.id + '"> \
									<div class="menu_text">Shuffle break</div>';
							if (canEdit) detailPanelHTML += '<button title="Delete" class="edit_control menu_button delete_icon"></button>';
							detailPanelHTML += '</div></li>';
						}			
					}
				}
				detailPanelHTML += '</ul>';

				if (canEdit) {
					detailPanelHTML += 
						'<div class="edit_control" style="margin-top:10px;"> \
							<span style="color:#cccccc; font-size:13px; vertical-align:middle; margin-left:4px; margin-right:6px;">Add:</span> \
							<div class="btn-group" style="margin-left:2px;"> \
								<button class="btn btn-small btn-dark dropdown-toggle" data-toggle="dropdown" href="#"> \
									Questions \
									<span class="caret"></span> \
								</button> \
								<ul class="dropdown-menu text-left"> \
									<li><a href="#" class="add_bank_items">Choose from bank</a></li> \
                                    <li><a href="#" class="create_new_item">Create new question</a></li>';

                    if (hasSmartFill) {
                        var fillEnabled = (assessmentData.Outcomes.length > 0) ? '' : ' disabled-link';
                        detailPanelHTML +=
                                    '<li><a href="#" class="smart_fill' + fillEnabled + '">Smart fill</a></li>';
                    }

                    detailPanelHTML +=
								'</ul> \
							</div><div class="btn-group" style="margin-left:2px;"> \
								<button class="btn btn-small btn-dark add_shuffle_break">Shuffle Break</button> \
							</div> \
						</div>';
				}
				detailPanelHTML += 
							'</div> \
						</div> \
					</div>';
				
				var $detailPanel = $(detailPanelHTML);
				$('#sections_panel').after($detailPanel);

				if (sectionData.Content.length > 0) {
					$detailPanel.find('.empty_layout').hide();
					$detailPanel.find('.items_menu').show();
				} else {
					$detailPanel.find('.empty_layout').show();
					$detailPanel.find('.items_menu').hide();
				}
			}			
		}

		if (canAdd) $('.create_new_item').removeClass('disabled-link');
		else $('.create_new_item').addClass('disabled-link');

		if (assessmentData.Sections.length > 0) {
			$('#sections_panel .empty_layout').hide();
			$('#sections_menu').show();
		} else {
			$('#sections_panel .empty_layout').show();
			$('#sections_menu').hide();
		}

		if (canEdit) {
			$('#sections_menu').sortable({
				start: function(event, ui) {
					$draggingSection = ui.item;
				}, 
				stop: onSectionSortStop
			});

			$('.items_panel .items_menu').sortable({
				start: function(event, ui) {
					$draggingItem = ui.item;

					$draggingItem.css('pointer-events', 'none');
					$('.empty_layout').hide();
					$('.items_panel .menu_wrapper').show();
				}, 
				stop: onItemSortStop,
				connectWith: '.items_panel .items_menu'
			});

			$('.items_panel .child_menu').sortable({
				stop: onChildSortStop
			});

		}

		if (!foundSection) {
			activeSectionID = null;
			activeContentID = null;
		}
		if ((activeSectionID == null) && (assessmentData.Sections.length > 0)) {
			activeSectionID = assessmentData.Sections[0].id;
		}
		if (activeSectionID != null) {
			if (activeContentID != null) selectItem(activeContentID);
			else selectSection(activeSectionID);
		}

		$('#sections_panel').show();
		$('#sections_panel').removeAttr('data-needs-render');
	}

	var drawAssessmentPreview = async function() {

		// Wrap old content. We keep this around so that the browser doesn't jump back to the top of the page
		// while we render the assessment.

		$('#assessment_preview_wrapper').wrapInner("<div class='preview_old'></div>");

		// Render the assessment

		var hasText = false;
		if ('Preamble' in assessmentData) hasText = true;
		else {
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type != 'page_break') hasText = true;
			}
		}
		
		if (hasText) {

			$('#assessment_empty_wrapper').hide();
			$('#assessment_preview_wrapper').show();

			// Draw title

			var titleHTML = '<div class="assessment_title"><div style="float:left;">';
			for (var i = 0; i < assessmentData.Title.text.length; ++i) {
				titleHTML += '<p>' + escapeHTML(assessmentData.Title.text[i]) + '</p>';
			}
			titleHTML += '</div>';
	
			if ('nameLines' in assessmentData.Title) {
				titleHTML += '<div style="float:right;">';
				for (var i = 0; i < assessmentData.Title.nameLines.length; ++i) {
					titleHTML += '<div style="float:right;">';
					titleHTML += '<div style="display:inline-block; margin-right:0.1in;">' + assessmentData.Title.nameLines[i] + ':</div>';
					titleHTML += '<div style="display:inline-block; width:1in; border-bottom:1px solid black;"></div>';
					titleHTML += '</div>';
					titleHTML += '<div style="clear:both;"></div>';
				}
				titleHTML += '</div>';
			}
			titleHTML += '<div style="clear:both;"></div></div>';
			
			$('#assessment_preview_wrapper').append(titleHTML);

			// Draw preamble
	
			$('#assessment_preview_wrapper').append('<div class="preamble_wrapper" style="margin:30px 0px;"></div>');
			if ('Preamble' in assessmentData) {
				$('#assessment_preview_wrapper .preamble_wrapper').html(assessmentData.Preamble.html);
				$('#assessment_preview_wrapper .preamble_wrapper').show();
				RenderTools.renderEquations($('#assessment_preview_wrapper .preamble_wrapper'));
			} else {
				$('#assessment_preview_wrapper .preamble_wrapper').hide();
			}
	
			// Draw body of assessment
	
			await AssessmentsRender.drawAssessment(assessmentData, questionData, $('#assessment_preview_wrapper'));

			// Remove old content

			$('.preview_old').remove();

			// Draw notes

			var hasQuestionNote = false;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					if ('notes' in assessmentData.Sections[i].Content[j]) {

						var contentID = assessmentData.Sections[i].Content[j].id;
						var $wrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"]');

						var $newNote = $($('#note_template').html());
						$newNote.attr('data-content-id', contentID);
						$newNote.find('.note_content').html(assessmentData.Sections[i].Content[j].notes);
						$wrapper.prepend($newNote);

						hasQuestionNote = true;
						
					}
				}
			}

			var $domNote = $($('#note_template').html());
			var titleNote = false;
			if ('notes' in assessmentData.Title) {
				titleNote = assessmentData.Title.notes;
			} else if (hasQuestionNote) {
				titleNote = 'This assessment template contains notes for some questions.';
				$domNote.find('.edit_icon').hide();
				$domNote.find('.delete_icon').hide();
			}

			if (titleNote !== false) {
				$domNote.find('.note_content').html(titleNote);
				$('#assessment_preview_wrapper').prepend($domNote);
			}

			if ('notes' in assessmentData.Title) {
				$('#title_add_notes').attr('disabled', true);
			} else {
				$('#title_add_notes').removeAttr('disabled');                        
			}

			var $allNotes = $("#assessment_preview_wrapper .assessment_note");
			RenderTools.renderEquations($allNotes);

			if ($allNotes.length == 1) {
				$allNotes.find('.prev_icon, .next_icon').hide();
			} else {
				$allNotes.first().find('.prev_icon').attr('disabled', true);
				$allNotes.last().find('.next_icon').attr('disabled', true);    
			}

			// Highlight weak questions

			updateHighlighting();

		} else {

			$('#assessment_empty_wrapper').show();
			$('#assessment_preview_wrapper').hide();

		}
	}

	var getSectionMenuText = function(sectionData) {
		var menuSuffix = '';
		if (sectionData.type == 'mixed') menuSuffix = ' (Mixed Section)';
		else menuSuffix = ' (' + sectionData.type.toUpperCase() + ' Section)';

		return sectionData.header + menuSuffix;
	}

	$('#version_conflict_dialog').dialog({
		autoOpen: false,
		width:400,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Go back")').addClass('btn btn_font');
			$buttonPane.find('button:contains("Reload")').addClass('btn btn-primary btn_font');
		}
    });

	async function saveAssessmentData() {

		// Remove rendering information

		var dataCopy = JSON.parse(JSON.stringify(assessmentData));

		for (var i = 0; i < dataCopy.Sections.length; ++i) {
			if (dataCopy.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < dataCopy.Sections[i].Content.length; ++j) {
				if (dataCopy.Sections[i].Content[j].type == 'item_set') {
					delete dataCopy.Sections[i].Content[j].rendered_index;
				}
			}
		}

		// Save assessment

		var data_string = JSON.stringify(dataCopy);

		var postData = {
			data_string: data_string,
			data_hash: SparkMD5.hash(data_string),
			version_id: assessmentVersionID
		};

		if (assessmentNewQuestions.length > 0) {
			postData.new_items = assessmentNewQuestions;
		}

		var newSaveName = $('#dset_save_name').val();
		if ((newSaveName.length > 0) && (newSaveName != $('#document_title').text())) {
			postData.save_name = newSaveName;
		}

		const response = await ajaxFetch('/Assessments/save/' + assessmentID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(postData)
		});

		pendingAJAXPost = false;
		const responseData = await response.json();

		if (responseData.success) {
			var isValid = true;
			if ('data_string' in responseData) {
				if (!('data_hash' in responseData)) isValid = false;
				else if (responseData.data_hash != SparkMD5.hash(responseData.data_string)) isValid = false;	
				if (isValid) assessmentData = JSON.parse(responseData.data_string);
			}

			if (isValid) {
				assessmentNewQuestions = [];

				// Update version

				assessmentVersionID = responseData.version_id;

				// Update question data for new items

				if ('new_items' in responseData) {
					for (var i = 0; i < responseData.new_items.length; ++i) {
						var thisData = PreviewCommon.parseQuestionData(responseData.new_items[i]);
						if (thisData !== false) {
							var questionHandleID = thisData.question_handle_id;
							questionData[questionHandleID] = thisData;
						}
					}
				}

				// Update render style and language

				localizationInstance.setLanguage(assessmentData.Settings.language);

				await RenderTools.setRenderStyle(assessmentData.Settings.numberingType);
				
				$('#assessment_preview_wrapper').attr('data-needs-render', 1);
				$('#sections_panel').attr('data-needs-render', 1);
				drawView();	

				return;

			} else {

				throw new Error('Invalid assessment data in save');

			}
			
		} else if (('error_type' in responseData) && (responseData.error_type == 'version_conflict')) {

			$('#version_conflict_dialog').dialog(
				'option', 
				'buttons', {
					'Go back': function() {
						$(this).dialog("close");
						throw new Error('Unable to save assessment data');
					},
					'Reload': function() {
						window.onbeforeunload = null;
						window.location.reload();
					}
				}
			);

			$('#version_conflict_dialog').dialog('open');

		} else {

			throw new Error('Unable to save assessment data');

		}
	}
	
    $('.marginSpinner').spinner({
	    step: 0.25,
        min: 0
    });

	$('#view_menu').menu({
		disabled: true
	});

	function initFolderTrees() {
		if ($('#treeData').html().length) {
			treeData = JSON.parse($('#treeData').html());
	
			for (var i = 0; i < treeData.length; ++i) {
				roots[treeData[i].text] = {
					id: treeData[i].id,
					folder_id: treeData[i].data.folder_id
				};
			}
	
			formTreeData = JSON.parse($('#formTreeData').html());
			sittingTreeData = JSON.parse($('#sittingTreeData').html());
			$(".folderTree").each(function() {
				if ($(this).is('.formFolders')) Folders.initFolderTree($(this), formTreeData);
				else if ($(this).is('.sittingFolders')) Folders.initFolderTree($(this), sittingTreeData);
				else Folders.initFolderTree($(this), treeData);
				$(this).jstree('close_all');
				$(this).jstree('deselect_all');
			});
			
            $('#folderPanel').off('keydown'); // To prevent arrow keys from navigating this tree

	        // Set up folder trees for form generation
	
			var formNodeId = Folders.getNodeId(formTreeData, formFolderId);
            $('.formFolders').each(function() {
                var selectID = $(this).attr('data-prefix') + formNodeId;
                $(this).jstree('select_node', selectID);
        
                Folders.disableConditional($(this), formTreeData, function(data) { 
                    return (('can_add' in data) && !data.can_add);
                });
            });
			
	        // Set up folder trees for sitting generation
	
			var sittingNodeId = Folders.getNodeId(sittingTreeData, sittingFolderId);
            $('.sittingFolders').each(function() {
                var selectID = $(this).attr('data-prefix') + sittingNodeId;
                $(this).jstree('select_node', selectID);
        
                Folders.disableConditional($(this), sittingTreeData, function(data) { 
                    return (('can_add' in data) && !data.can_add);
                });
            });
			
	        // Finish setting up question folder trees
	
			Folders.disableConditional($('#folderPanel'), treeData, function(data) { 
				if (('community_id' in data) && (data.community_id == communityID)) return false;
				else if (('can_export' in data) && !data.can_export) return true;
				else return false;
			});
	
			var defaultQuestionNodeId;
			if (communityID == null) {
	
				Folders.disableSubtree($('#targetFolderParent'), roots['Communities'].id);
				defaultQuestionNodeId = roots['Home'].id;
	
			} else if (questionFolderId !== false) {
	
				Folders.disableAll($('#targetFolderParent'));
				Folders.enableConditional($('#targetFolderParent'), treeData, function(data) { 
					return (('community_id' in data) && (data.community_id == communityID));
				});
				defaultQuestionNodeId = Folders.getNodeId(treeData, questionFolderId);
	
			}
				
			var selectID = $('#targetFolderParent').attr('data-prefix') + defaultQuestionNodeId;
			$('#targetFolderParent').jstree('deselect_all');
			$('#targetFolderParent').jstree('select_node', selectID);
	
			showItems({ node_id: defaultQuestionNodeId });
	
			var selectID = $('#folderPanel').attr('data-prefix') + defaultQuestionNodeId;
			$('#folderPanel').jstree('select_node', selectID);
	
			Folders.disableSubtree($('#folderPanel'), roots['Trash'].id);
			Folders.disableSubtree($("#targetFolderParent"), roots['Open bank'].id);
			Folders.disableSubtree($('#targetFolderParent'), roots['Trash'].id);
	
			$("#folderPanel").bind("select_node.jstree", function (event, data) {
				showItems({ node_id: data.node.data.raw_id });
				$('#sort_search').hide();
				$('#sort_default').show();
			});
	
			$('#sort_search').hide();
		}	
	}
	
	$(document).on('mouseenter', '.question_row', function(e) {
		var questionID = $(this).attr('data-question-id');
		if (!(questionID in PreviewCommon.bankQuestions)) {
            PreviewCommon.loadQuestionPreview(questionID);
        }
	});

	$(document).on('mouseleave', '.question_row', function(e) {
		clearTimeout(previewTimer);
		previewTimer = null;
	});

	$(document).on('mousemove', '.question_row', function(e) {
		var questionID = $(this).attr('data-question-id');

		if (previewTimer != null) clearTimeout(previewTimer);
		if ($('#question_preview').attr('data-question-id') != questionID) {
			previewTimer = setTimeout(function() {
				return function() {
					PreviewCommon.updateQuestionPreview(questionID);
				}
			}(questionID), 300);
		} else previewTimer = null;
	});

    $(document).on('keydown', function(e) {
        if ($('#addItem_dialog').is(':visible') && !$('#item_search_menu').is(':visible')) {
            var $activeRow = $('#folder_questions tr.doc-hover');

            var needsScroll = false;
            if ((e.key === 'ArrowLeft') || (e.key === 'ArrowUp')) {
            
                // Up arrow or left arrow
                
                e.preventDefault();
                e.stopPropagation();
                
                if (($activeRow.length == 1) && ($activeRow.prev('.question_row').length == 1)) {
                    $activeRow = $activeRow.prev('.question_row');
                    needsScroll = true;
                }
    
            } else if ((e.key === 'ArrowRight') || (e.key === 'ArrowDown')) {
    
                // Down arrow or right arrow
    
                e.preventDefault();
                e.stopPropagation();
    
                if (($activeRow.length == 1) && ($activeRow.next('.question_row').length == 1)) {
                    $activeRow = $activeRow.next('.question_row');
                    needsScroll = true;
                }
    
            } else if ((e.key === 'Enter') || (e.key === ' ')) {
    
                // Enter or space

                e.preventDefault();
                e.stopPropagation();
    
                $activeRow.find('.item_select').trigger('click');
    
            }
    
            if (needsScroll) {
                $activeRow.siblings('.question_row').removeClass('doc-hover');
                $activeRow.addClass('doc-hover');
    
                var $wrapper = $('#folder_questions_wrapper');
                var $table = $activeRow.closest('table');
    
                var wrapperTop = $wrapper.scrollTop();
                var wrapperBottom = wrapperTop + $wrapper.height();
    
                var rowTop = $activeRow.position().top - $table.position().top;
                var rowBottom = rowTop + $activeRow.height();
    
                if (rowTop < wrapperTop) $wrapper.scrollTop(rowTop);
                else if (rowBottom > wrapperBottom) $wrapper.scrollTop(rowTop - $wrapper.height() + $activeRow.height());
                
                var questionID = $activeRow.attr('data-question-id');
                if ($('#question_preview').attr('data-question-id') != questionID) {
                    clearTimeout(previewTimer);
                    previewTimer = null;
    
                    PreviewCommon.updateQuestionPreview(questionID);
                }
            }
        }
    });

  	$('.item_sort').on('click', function() {
	  	$('#sort_type').attr('data-direction', $(this).attr('data-direction'));
		sortFolderQuestions();
	});
	
  	$('#sort_type').on('change', function() {
	  	$('#sort_type').attr('data-direction', $(this).find(':selected').attr('data-default-direction'));
		sortFolderQuestions();
	});
	
    /* *
     * * on section sort stop, send list order to the server
     * */
    var onSectionSortStop = function(ev, ui) {
        var $parent = ui.item.parent();

		var sectionID = $draggingSection.find('.menu_item').attr('data-section-id');
		$draggingSection = null;

		var sectionType = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].id == sectionID) sectionType = assessmentData.Sections[i].type;
		}

		var originalData = JSON.stringify(assessmentData);
		
		var tempSections = {};
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			tempSections[assessmentData.Sections[i].id] = assessmentData.Sections[i];
		}

		assessmentData.Sections = [];
        $parent.find('.menu_item').each(function() {
			var sectionID = $(this).attr('data-section-id');
            assessmentData.Sections.push(tempSections[sectionID]);
		});

		var newData = JSON.stringify(assessmentData);
		if (newData != originalData) {

			// Save assessment

			if (pendingAJAXPost) return false;
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {

				// Finish up

				$.gritter.add({
					title: "Success",
					text: ((sectionType == 'page_break') ? 'Page break' : 'Section') + " moved.",
					image: "/img/success.svg"
				});
			})
			.catch(function(error) {
				showSubmitError();			
			});
		}
    }

    /* *
     * * on item sort stop, send list order to the server
     * */

	var onItemSortStop = function(ev, ui) {
		clearTimeout(draggingTimer);
		draggingTimer = null;

		var goodDrop = true;

		var $hoveredSectionItem = $('#sections_menu .menu_item:hover');
		if ($hoveredSectionItem.length == 1) {

			// Item is being held over a section in the section menu

			if ($hoveredSectionItem.attr('data-can-drop') == 1) {				
				var sectionID = $hoveredSectionItem.attr('data-section-id');
				var $itemMenu = $('#items_panel_' + sectionID + ' .items_menu');
				if (!$itemMenu.is(':visible')) $hoveredSectionItem.trigger('click');
				$itemMenu.append($draggingItem).sortable('refresh');
			} else goodDrop = false;

		} else {

			// Check that item is being held over an item menu

			var $hoveredItemMenu = $('.items_panel .items_menu:hover');
			if ($hoveredItemMenu.length == 0) goodDrop = false;
			
		}

		if (goodDrop) {

			var $section_container = $draggingItem.closest('.items_panel');
			var sectionID = $section_container.attr('data-section-id');
	
			var $menuItem = $draggingItem.find('.menu_item');
			var isFormatting = $menuItem.parent().hasClass('formatting_item');
		
			// Gather items for whole assessment
	
			var originalData = JSON.stringify(assessmentData);
	
			var numItems = 0;
			var itemEntries = {};
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					var thisEntry = assessmentData.Sections[i].Content[j];
					itemEntries[thisEntry.id] = thisEntry;
					numItems++;
				}
				assessmentData.Sections[i].Content = [];
			}
	
			// Use left-side menus to place items back into the assessment
	
			var success = true;
			$('.items_panel').each(function() {
				var sectionID = $(this).attr('data-section-id');
				var $menuItems = $(this).find('.items_menu li .content_item');
	
				var thisContent = null;
				for (var i = 0; i < assessmentData.Sections.length; ++i) {
					if (sectionID == assessmentData.Sections[i].id) {
						thisContent = assessmentData.Sections[i].Content;
					}
				}
	
				if (thisContent !== null) {
					$menuItems.each(function() {
						var contentID = $(this).attr('data-content-id');
						if (contentID in itemEntries) {
							thisContent.push(itemEntries[contentID]);
							--numItems;
						} else success = false;
					});	
				} else success = false;
			});
	
			if (numItems > 0) success = false;
	
			if (success) {
				var urlVars = getUrlVars();
				urlVars.s = sectionID;
				delete urlVars.c;
				setUrlVars(urlVars);
	
				var newData = JSON.stringify(assessmentData);
				if (newData != originalData) {
			
					// Save assessment
	
					if (pendingAJAXPost) return false;
					pendingAJAXPost = true;
					
					saveAssessmentData()
					.then(function() {
	
						// Finish up
	
						$.gritter.add({
							title: "Success",
							text: (isFormatting ? "Shuffle break" : "Item") + " moved.",
							image: "/img/success.svg"
						});
					})
					.catch(function() {
						showSubmitError();			
					});
				}
			} else {
				showSubmitError();			
			}

		} else {

			$.gritter.add({
				title: "Error",
				text: "Can't move this item here.",
				image: "/img/error.svg"
			});

			$(this).sortable('cancel');

		}

		$draggingItem.css('pointer-events', 'auto');
		$draggingItem = null;
	}
	
	var onChildSortStop = function(ev, ui) {
		var $menuWrapper = ui.item.closest('.child_menu');

		var newQuestionHandleIDs = [];
		$menuWrapper.find('.child_item').each(function() {
			newQuestionHandleIDs.push(parseInt($(this).attr('data-question-handle-id')));
		});

		var newJSON = JSON.stringify(newQuestionHandleIDs);
		var oldJSON = newJSON;

		var contentID = $menuWrapper.siblings('.content_item').attr('data-content-id');
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var thisEntry = assessmentData.Sections[i].Content[j];
				if (thisEntry.id == contentID) {
					oldJSON = JSON.stringify(thisEntry.question_handle_ids);
					var renderedQuestionHandleID = thisEntry.question_handle_ids[thisEntry.rendered_index];
					thisEntry.rendered_index = newQuestionHandleIDs.indexOf(renderedQuestionHandleID);
					thisEntry.question_handle_ids = newQuestionHandleIDs;
				}
			}
		}

		if (newJSON != oldJSON) {
			
			// Save assessment

			if (pendingAJAXPost) return false;
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {

				// Finish up

				$.gritter.add({
					title: "Success",
					text: "Item moved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				showSubmitError();
			});
		}
	}

	$(document).on('mouseenter', '#sections_menu li:not(.formatting_item) .menu_item', function() {
		if ($draggingItem != null) {
			var targetSectionType = false;
			var sectionID = $(this).attr('data-section-id');
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].id == sectionID) {
					targetSectionType = assessmentData.Sections[i].type;
				}
			}

			var isMatch = (targetSectionType !== false);
			
			if (isMatch && !$draggingItem.hasClass('formatting_item')) {
				if (targetSectionType != 'mixed') {
					var contentID = $draggingItem.find('.content_item').attr('data-content-id');

					var questionHandleIDs = [];
					for (var i = 0; i < assessmentData.Sections.length; ++i) {
						if (assessmentData.Sections[i].type == 'page_break') continue;
						for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
							var thisEntry = assessmentData.Sections[i].Content[j];
							if (thisEntry.id == contentID) {
								if (thisEntry.type == 'item') {
									questionHandleIDs = [thisEntry.question_handle_id];
								} else if (thisEntry.type == 'item_set') {
									questionHandleIDs = thisEntry.question_handle_ids;
								}
							}
						}
					}

					for (var i = 0; i < questionHandleIDs.length; ++i) {
						var thisQuestionData = questionData[questionHandleIDs[i]];
						for (var j = 0; j < thisQuestionData.json_data.Parts.length; ++j) {
							var partType = thisQuestionData.json_data.Parts[j].type;
							if ((partType != 'context') && (partType.substr(0, 2) != targetSectionType)) {
								isMatch = false;
							}
						}
					}	
				}
			}

			if (isMatch) {
				$(this).attr('data-can-drop', 1);
				draggingTimer = setTimeout(function($sectionItem) {
					return function() {
						$sectionItem.trigger('click');
						var $itemMenu = $('#items_panel_' + sectionID + ' .items_menu');
						$itemMenu.append($draggingItem).sortable('refresh');
					}
				}($(this)), 300);	
			} else {
				$(this).attr('data-can-drop', 0);
			}
		}
	});

	$(document).on('mouseleave', '#sections_menu .menu_item', function() {
		$(this).removeAttr('data-can-drop');
		clearTimeout(draggingTimer);
		draggingTimer = null;
	});

    /* *
     * * delete preamble
     * */
     
    var deletePreamble = function() {
		var $dialog = $(this);

		delete assessmentData.Preamble;

		// Save assessment

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;
		
		saveAssessmentData()
		.then(function() {

			// Finish up

			$dialog.dialog("close");

			$.gritter.add({
				title: "Success",
				text: "Preamble deleted.",
				image: "/img/success.svg"
			});
		})
		.catch(function() {
			showSubmitError($dialog);			
		});
    }

    /* *
     * * save preamble
     * */
     
    var preambleSave = function() {
		var $dialog = $('#editPreamble_form');
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')){
			var newPreambleHTML = $('#dpre_preamble_edit').htmleditor('getValue');
			if (newPreambleHTML.length > 0) assessmentData.Preamble = {html: newPreambleHTML};
			else delete assessmentData.Preamble;

			// Save assessment

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Preamble saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				showSubmitError($dialog);			
			});
		}
    }

    var sectionSave = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')){
			var sectionID = $('#dsec_id').val();

			var urlVars = getUrlVars();
			urlVars.s = sectionID;
			setUrlVars(urlVars);

			var index = 0;
			for (index = 0; index < assessmentData.Sections.length; ++index) {
				if (sectionID == assessmentData.Sections[index].id) break;
			}

			if (index == assessmentData.Sections.length) {
				assessmentData.Sections.push({
					id: $('#dsec_id').val(),
					Content: []
				});
			}

			var thisSection = assessmentData.Sections[index];
			thisSection.type = $('#dsec_type').val();
			thisSection.header = $('#dsec_header').val();
			thisSection.showHeader = $('#dsec_show_header').prop('checked');
			thisSection.shuffleQuestions = $('#dsec_shuffle_questions').prop('checked');

			// Save assessment

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Section saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				showSubmitError($dialog);			
			});
		}
    }

	var deleteSection = function() {
		var $dialog = $(this);

		var sectionID = $('#deleteSection_dialog').attr('data-section-id');

		var index;
		var sectionType = false;
		for (index = 0; index < assessmentData.Sections.length; ++index) {
			if (assessmentData.Sections[index].id == sectionID) {
				sectionType = assessmentData.Sections[index].type;
				break;
			}
		}

		if (index < assessmentData.Sections.length) {
			var urlVars = getUrlVars();
			if (('s' in urlVars) && (urlVars.s == sectionID)) {
				delete urlVars.c;
				delete urlVars.s;
				setUrlVars(urlVars);
			}

			assessmentData.Sections.splice(index, 1);

			// Remove outcomes and weights attached to this section

			cleanOutcomes();
			cleanWeights();

			// Save assessment

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: ((sectionType == 'page_break') ? 'Page break' : 'Section') + " deleted.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				showSubmitError($dialog);
			});
		}
	}

	$(document).on('click', '.addPageBreak', function(e) {
		assessmentData.Sections.push({
			id: getNewID(4),
			type: 'page_break'
		});

		// Save document

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;
		
		saveAssessmentData()
		.then(function() {

			// Finish up

			$.gritter.add({
				title: "Success",
				text: "Page break added.",
				image: "/img/success.svg"
			});
		})
		.catch(function() {
			showSubmitError();			
		});
	});     

    $(document).on('click', '.menu_item.question_set .edit_icon', function(e) {
		e.stopPropagation();

		var contentID = $(this).closest('.menu_item').attr('data-content-id');
		startEditQuestionSet(contentID);
	});
	
    $(document).on('click', '.menu_item.question .edit_icon', function(e) {
		e.stopPropagation();

		var questionHandleID = $(this).closest('.menu_item').attr('data-question-handle-id');
		window.open('/Questions/build/' + questionData[questionHandleID].id + '?a=' + assessmentID);
	});
	
	$(document).on('click', function(e) {
		if ($(e.target).closest('.item_menu_wrapper').length == 0) {
			$('.question_wrapper.selected').removeClass('selected');
			$('.question_wrapper .item_menu_wrapper').remove();
		}
	});

	$('#targetFolder_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

    function getTarget() {
		return new Promise((resolve, reject) => {
			$('#targetFolder_dialog').dialog(
				'option', 
				'buttons', {
					'Cancel': function() {
						$(this).dialog("close");

						$.gritter.add({
							title: "Success",
							text: "Save cancelled.",
							image: "/img/error.svg"
						});
		
						reject();
					},
					'Save': function() {
						var json = Folders.getSelectedNode("targetFolderParent");
						resolve({
							save_name: $('#target_name').val(), 
							folder_id: json.data.folder_id
						});
					}
				}
			);

			$('#targetFolder_dialog').dialog('open');
		});
	}

	$(document).on('click', '.item_menu_copy', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var $wrapper = $(this).closest('.question_wrapper');

		var questionHandleID = $wrapper.attr('data-question-handle-id');
		$('#parent_question_handle_id').val(questionHandleID);

		var contentID = $wrapper.attr('data-content-id');
		$('#parent_content_id').val(contentID);

		$('#target_name').val(questionData[questionHandleID].name);
		$('#target_name_wrapper').show();

        getTarget().then(copyCallback);
	});

	var copyCallback = function(data) {
		var $dialog = $('#targetFolder_dialog');

		var parentContentID = $('#parent_content_id').val();
		var parentQuestionHandleID = parseInt($('#parent_question_handle_id').val());
		var parentQuestionID = questionData[parentQuestionHandleID].id;

		var thisQuestionData = questionData[parentQuestionHandleID].json_data;
		var cleanedQuestionData = JSON.parse(JSON.stringify(thisQuestionData));

		var importIDs = [];
		for (var importID in cleanedQuestionData.Engine.Imports) {
			if (cleanedQuestionData.Engine.Imports.hasOwnProperty(importID)) {
				importIDs.push(parseInt(importID));
			}
		}
		cleanedQuestionData.Engine.Imports = importIDs;

		for (var i = 0; i < cleanedQuestionData.Parts.length; ++i) {
			delete cleanedQuestionData.Parts[i].index;
		}

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var w = window.open('/Questions/build_waiting');
		
		ajaxFetch('/Questions/create', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				parent_id: parentQuestionID,
				folder_id: data.folder_id,
				name: data.save_name,
				json_data: JSON.stringify(cleanedQuestionData)
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				var oldHandleID = parseInt($('#parent_question_handle_id').val());

				var questionID = responseData.id;
				var questionHandleID = responseData.question_handle_id;
				var userQuestionID = responseData.user_question_id;

				w.location.replace('/Questions/build/' + questionID + '?u=' + userQuestionID);

				var newQuestionData = JSON.parse(JSON.stringify(questionData[oldHandleID]));
				newQuestionData.id = questionID;
				newQuestionData.question_handle_id = questionHandleID;
                newQuestionData.name = data.save_name;

				PreviewCommon.bankQuestions[questionID] = newQuestionData;
				questionData[questionHandleID] = newQuestionData;

				// Update assessment data

				for (var i = 0; i < assessmentData.Sections.length; ++i) {
					if (assessmentData.Sections[i].type == 'page_break') continue;
					for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
						var thisEntry = assessmentData.Sections[i].Content[j];
						if (thisEntry.id == parentContentID) {
							if (thisEntry.type == 'item') {

								delete thisEntry.question_handle_id;

							} else if (thisEntry.type == 'item_set') {

								var index = thisEntry.question_handle_ids.indexOf(parentQuestionHandleID);
								if (index >= 0) thisEntry.question_handle_ids.splice(index, 1);

							}							
						}
					}
				}
	
				assessmentNewQuestions.push({
					content_id: parentContentID,
					question_handle_id: questionHandleID
				});

				saveAssessmentData()
				.then(function() {
					$dialog.dialog('close');
				})
				.catch(function() {
					showSubmitError($dialog);			
				});

			} else {

                w.close();
				$.gritter.add({
					title: "Error",
					text: "Unable to copy question.",
					image: "/img/error.svg"
				});
				$dialog.dialog('close');

			}
		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$(document).on('click', '.item_menu_set', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var $wrapper = $(this).closest('.question_wrapper');
		var contentID = $wrapper.attr('data-content-id');
		startEditQuestionSet(contentID);
	});

	var startEditQuestionSet = function(contentID) {
		$('#set_content_id').val(contentID);
				
		var thisEntry = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				if (assessmentData.Sections[i].Content[j].id == contentID) {
					thisEntry = assessmentData.Sections[i].Content[j];
				}
			}
		}

		if (thisEntry !== false) {
			var questionHandleIDs;
            var renderedIndex = 0;

			if (thisEntry.type == 'item_set') {

				$('#editSet_dialog').dialog('option', 'title', 'Edit question set');
				$('#set_name').val(thisEntry.name);
				questionHandleIDs = thisEntry.question_handle_ids;
                if ('rendered_index' in thisEntry) renderedIndex = thisEntry.rendered_index;

			} else {

				$('#editSet_dialog').dialog('option', 'title', 'Create question set');
				$('#set_name').val('');
				questionHandleIDs = [thisEntry.question_handle_id];

			}

			var scoringHash = questionData[questionHandleIDs[0]].scoring_hash;
			$('#set_scoring_hash').val(scoringHash);
	
			var questionsTableHTML = "";
			for (var i = 0; i < questionHandleIDs.length; ++i) {
				var thisData = questionData[questionHandleIDs[i]];
				questionsTableHTML +=
					'<tr class="existing_item' + ((i == renderedIndex) ? ' rendered_item' : '') + 
                        '" data-question-handle-id="' + questionHandleIDs[i] + '">' +
						'<td class="set_question_name">' + thisData.name + '</td>' +
						'<td class="set_question_actions">' +
                            '<button title="Duplicate" class="actions_button copy_icon"></button>' +
                            '<button title="Delete" class="actions_button delete_icon"></button>' +
                        '</td>' +
					'</tr>';
			}
			
			$('#set_questions_table').html(questionsTableHTML);

			if ($('#set_questions_table tr').length > 1) {
				$('.set_question_actions .delete_icon').removeAttr('disabled');
			} else $('.set_question_actions .delete_icon').attr('disabled', 'disabled');
	
			$('#editSet_dialog').dialog('open');
			$('#set_name').focus();
		}
	}

	var questionSetSave = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')) {

			var contentID = $('#set_content_id').val();
			
			var questionHandleIDs = [];
			var addQuestionData = [];

			$('#set_questions_table tr').each(function() {
				if ($(this).is('.existing_item')) {
					var thisHandleID = parseInt($(this).attr('data-question-handle-id'));
					questionHandleIDs.push(thisHandleID);
				} else if ($(this).is('.new_item')) {
					addQuestionData.push(JSON.parse($(this).attr('data-new-entry')));
				}
			});

			// Get entry in assessment data

			var thisEntry;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					if (assessmentData.Sections[i].Content[j].id == contentID) {
						thisEntry = assessmentData.Sections[i].Content[j];
					}
				}
			}

			// Attach outcomes to part indices

			if (thisEntry.type == 'item') {
				var oldQuestionData = questionData[thisEntry.question_handle_id].json_data;
				
				var indexMap = {};
				for (var i = 0; i < oldQuestionData.Parts.length; ++i) {
					indexMap[oldQuestionData.Parts[i].id] = oldQuestionData.Parts[i].index;
				}

				for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
                    var j = 0;
                    while (j < assessmentData.Outcomes[i].parts.length) {
						if (assessmentData.Outcomes[i].parts[j].content_id == contentID) {
							var partID = assessmentData.Outcomes[i].parts[j].part_id;
							delete assessmentData.Outcomes[i].parts[j].part_id;

							if (partID in indexMap) {
								assessmentData.Outcomes[i].parts[j].part_index = indexMap[partID];
                                ++j;
							} else assessmentData.Outcomes[i].parts.splice(j, 1);
						} else ++j;
                    }
				}
			}

			if (questionHandleIDs.length + addQuestionData.length == 1) {

				// Update assessment data

				if (thisEntry.type == 'item_set') {
					thisEntry.type = 'item';
					delete thisEntry.name;
					delete thisEntry.rendered_index;	
					delete thisEntry.question_handle_ids;	
				} else {
					delete thisEntry.question_handle_id;
				}

				if (questionHandleIDs.length == 1) {
					thisEntry.question_handle_id = questionHandleIDs[0];
				}

				// Attach outcomes to new part IDs

				var newQuestionData;
				if (questionHandleIDs.length == 1) newQuestionData = questionData[questionHandleIDs[0]].json_data;
				else newQuestionData = PreviewCommon.bankQuestions[addQuestionData[0].id].json_data;

				var indexMap = {};
				for (var i = 0; i < newQuestionData.Parts.length; ++i) {
					indexMap[newQuestionData.Parts[i].index] = newQuestionData.Parts[i].id;
				}

				for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
                    var j = 0;
                    while (j < assessmentData.Outcomes[i].parts.length) {
						if (assessmentData.Outcomes[i].parts[j].content_id == contentID) {
							var partIndex = assessmentData.Outcomes[i].parts[j].part_index;
							delete assessmentData.Outcomes[i].parts[j].part_index;
							
							if (partIndex in indexMap) {
								assessmentData.Outcomes[i].parts[j].part_id = indexMap[partIndex];
                                ++j;
							} else assessmentData.Outcomes[i].parts.splice(j, 1);
						} else ++j;
                    }
				}

			} else {

				// Update assessment data

				if (thisEntry.type == 'item') {
					thisEntry.type = 'item_set';
					thisEntry.rendered_index = 0;
					delete thisEntry.question_handle_id;
				} else {
                    var $renderedRow = $('#set_questions_table tr.rendered_item');
                    if ($renderedRow.length == 1) thisEntry.rendered_index = $renderedRow.index();
                    else thisEntry.rendered_index = 0;
                }

				thisEntry.name = $('#set_name').val();
				thisEntry.question_handle_ids = questionHandleIDs;

			}

			// Set up new questions for saving

			for (var i = 0; i < addQuestionData.length; ++i) {
				var newContent = JSON.parse(JSON.stringify(addQuestionData[i]));
				newContent.content_id = contentID;
				assessmentNewQuestions.push(newContent);
			}

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			saveAssessmentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Question set saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				showSubmitError($dialog);			
			});
		}
	}

	$('#editSet_dialog').dialog({
        autoOpen: false,
		width: 500,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#editSet_dialog').find('button:contains("Cancel")').click(function(){
        $('#editSet_dialog').dialog("close");
	});

	$('#editSet_dialog').find('button:contains("Save")').click(function(){
		var contextedSave = $.proxy(questionSetSave, $('#editSet_dialog'));
		contextedSave();
	});

	var getItems = function() {
		return new Promise((resolve, reject) => {

			$('#addItem_dialog').find('button:contains("Cancel")').off('click');
			$('#addItem_dialog').find('button:contains("Cancel")').on('click', function(){
				resolve([]);
			});
		 
			$('#addItem_save').off('click');
			$('#addItem_save').on('click', function() {
				resolve(JSON.parse($('#addItem_dialog').attr('data-selected')));
			});

			$.each($('#addItem_dialog').find('.item_select'), function() {
				if ($(this).prop('checked')) $(this).trigger('click');
			});
			$('#question_preview').html("");
			
			$('#addItem_dialog').attr('data-selected', JSON.stringify([]));
			$('#addItem_count').text('No questions selected.');

			$('#addItem_dialog').dialog('open');

		});		
	}

	$('#set_add_questions').on('click', function() {
		var scoringHash = $('#set_scoring_hash').val();
		
		$('#addItem_dialog').attr('data-scoring-hash', scoringHash);
		$('#addItem_dialog').attr('data-selection-type', 'many');
		$('#addItem_count').show();

		$('#addItem_dialog').dialog('option', 'title', 'Add items to set');
		$('#addItem_save').text('Add');
		
		$('#addItem_outcomes').prop('checked', false);
		$('#addItem_outcomes_wrapper').hide();

		$('#folder_questions').attr('data-section-type', 'mixed');
		initTypeCheckboxes();
		$('#type_checkbox_wrapper').find('input').attr("disabled", "disabled");
		$('#type_checkbox_wrapper').find('label').addClass("disabled");

		$('#editSet_dialog').dialog('close');

		getItems().then(setAddCallback);
	});

	var setAddCallback = function(addQuestionData) {
		$('#addItem_dialog').removeAttr('data-scoring-hash');

		$('#addItem_dialog').dialog("close");
		$('#editSet_dialog').dialog('open');

		if (addQuestionData.length > 0) {
			var newRowsHTML = "";
			for (var i = 0; i < addQuestionData.length; ++i) {
				newRowsHTML +=
					"<tr class='new_item' data-new-entry='" + htmlSingleQuotes(JSON.stringify(addQuestionData[i])) + "'>" +
						"<td class='set_question_name'>" + addQuestionData[i].name + "</td>" +
						'<td class="set_question_actions">' +
                            '<button title="Duplicate" class="actions_button copy_icon"></button>' +
                            '<button title="Delete" class="actions_button delete_icon"></button>' +
                        '</td>' +
					"</tr>";
			}
			$('#set_questions_table').append(newRowsHTML);

			$('.set_question_actions .delete_icon').removeAttr('disabled');
		}
	}

	$('#set_questions_table').on('click', '.set_question_actions .delete_icon', function() {
		$(this).closest('tr').remove();

		if ($('#set_questions_table tr').length == 1) {
			$('.set_question_actions .delete_icon').attr('disabled', 'disabled');
		}
	});

    $('#set_questions_table').on('click', '.set_question_actions .copy_icon', function() {
        var questionHandleID = $(this).closest('tr').attr('data-question-handle-id');
        $('#parent_question_handle_id').val(questionHandleID);
		$('#target_name').val(questionData[questionHandleID].name);
		$('#target_name_wrapper').show();

        getTarget().then(setCopyCallback);
    });

    var setCopyCallback = function(data) {
		var $dialog = $('#targetFolder_dialog');

		var parentQuestionHandleID = parseInt($('#parent_question_handle_id').val());
		var parentQuestionID = questionData[parentQuestionHandleID].id;

		var thisQuestionData = questionData[parentQuestionHandleID].json_data;
		var cleanedQuestionData = JSON.parse(JSON.stringify(thisQuestionData));

		var importIDs = [];
		for (var importID in cleanedQuestionData.Engine.Imports) {
			if (cleanedQuestionData.Engine.Imports.hasOwnProperty(importID)) {
				importIDs.push(parseInt(importID));
			}
		}
		cleanedQuestionData.Engine.Imports = importIDs;

		for (var i = 0; i < cleanedQuestionData.Parts.length; ++i) {
			delete cleanedQuestionData.Parts[i].index;
		}

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var w = window.open('/Questions/build_waiting');

		ajaxFetch('/Questions/create', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				parent_id: parentQuestionID,
				folder_id: data.folder_id,
				name: data.save_name,
				json_data: JSON.stringify(cleanedQuestionData)
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				var oldHandleID = parseInt($('#parent_question_handle_id').val());

                var questionID = responseData.id;
				var questionHandleID = responseData.question_handle_id;
				var userQuestionID = responseData.user_question_id;

				w.location.replace('/Questions/build/' + questionID + '?u=' + userQuestionID);

				var newQuestionData = JSON.parse(JSON.stringify(questionData[oldHandleID]));
				newQuestionData.id = questionID;
				newQuestionData.question_handle_id = questionHandleID;
                newQuestionData.name = data.save_name;

				PreviewCommon.bankQuestions[questionID] = newQuestionData;
				questionData[questionHandleID] = newQuestionData;

                var newEntry = {
                    question_id: questionID,
                    name: newQuestionData.name,
                    from_open: false
                };
    
                var newRowHTML =
                    "<tr class='new_item' data-question-handle-id='" + questionHandleID + 
                        "' data-new-entry='" + htmlSingleQuotes(JSON.stringify(newEntry)) + "'>" +
                        "<td class='set_question_name'>" + newQuestionData.name + "</td>" +
						'<td class="set_question_actions">' +
                            '<button title="Duplicate" class="actions_button copy_icon"></button>' +
                            '<button title="Delete" class="actions_button delete_icon"></button>' +
                        '</td>' +
                    "</tr>";

                $('#set_questions_table').append(newRowHTML);
    
                $('.set_question_actions .delete_icon').removeAttr('disabled');
    
				$dialog.dialog('close');
                
			} else {

                w.close();
				$.gritter.add({
					title: "Error",
					text: "Unable to duplicate question.",
					image: "/img/error.svg"
				});
                
				$dialog.dialog('close');

			}
		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$(document).on('click', '.item_menu_bank', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var $wrapper = $(this).closest('.question_wrapper');
		var contentID = $wrapper.attr('data-content-id');
		var questionHandleID = $wrapper.attr('data-question-handle-id');

		var scoringHash = questionData[questionHandleID].scoring_hash;

		var sectionType = false;
		var itemType = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var thisEntry = assessmentData.Sections[i].Content[j];
				if (thisEntry.id == contentID) {
					sectionType = assessmentData.Sections[i].type;
					itemType = thisEntry.type;
				}
			}
		}

		$('#addItem_dialog').attr('data-content-id', contentID);
		if (itemType == 'item') {

			$('#folder_questions').attr('data-section-type', sectionType);
			initTypeCheckboxes();	

		} else if (itemType == 'item_set') {

			$('#addItem_dialog').attr('data-scoring-hash', scoringHash);
			$('#addItem_dialog').attr('data-question-handle-id', questionHandleID);	

			$('#folder_questions').attr('data-section-type', 'mixed');
			initTypeCheckboxes();
			$('#type_checkbox_wrapper').find('input').attr("disabled", "disabled");
			$('#type_checkbox_wrapper').find('label').addClass("disabled");	
			
		}
		$('#addItem_dialog').attr('data-selection-type', 'one');
		$('#addItem_count').hide();

		$('#addItem_dialog').dialog('option', 'title', 'Replace Item');
		$('#addItem_save').text('Replace');
		
		$('#addItem_outcomes').prop('checked', true);
		$('#addItem_outcomes_wrapper').show();

		getItems().then(bankCallback);
	});
	
	var bankCallback = function(addQuestionData) {
		var $dialog = $('#addItem_dialog');
		$dialog.removeAttr('data-scoring-hash');

		if (addQuestionData.length > 0) {
			var contentID = $dialog.attr('data-content-id');
			$dialog.removeAttr('data-content-id');

			var questionHandleID;
			if ($dialog[0].hasAttribute('data-question-handle-id')) {
				questionHandleID = parseInt($dialog.attr('data-question-handle-id'));
				$dialog.removeAttr('data-question-handle-id');
			} else questionHandleID = false;

			// Update assessment data

			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					var thisEntry = assessmentData.Sections[i].Content[j];
					if (thisEntry.id == contentID) {
						if (thisEntry.type == 'item') {

							delete thisEntry.question_handle_id;

						} else if (thisEntry.type == 'item_set') {

							var index = thisEntry.question_handle_ids.indexOf(questionHandleID);
							if (index >= 0) thisEntry.question_handle_ids.splice(index, 1);

						}		
					}
				}
			}

			var newContent = JSON.parse(JSON.stringify(addQuestionData[0]));
			newContent.content_id = contentID;
			assessmentNewQuestions.push(newContent);

			// Remove outcomes attached to old question

			for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
				var index = 0;
				while (index < assessmentData.Outcomes[i].parts.length) {
					if (assessmentData.Outcomes[i].parts[index].content_id == contentID) {
						assessmentData.Outcomes[i].parts.splice(index, 1);
					} else ++index;
				}
			}

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			saveAssessmentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Item replaced.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				showSubmitError($dialog);			
			});

		} else $dialog.dialog('close');
	};

	$(document).on('click', '.item_menu_edit', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var $wrapper = $(this).closest('.question_wrapper');
		var questionHandleID = $wrapper.attr('data-question-handle-id');
		window.open('/Questions/build/' + questionData[questionHandleID].id + '?a=' + assessmentID);
	});

	$(document).on('click', '.item_menu_remove', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var $wrapper = $(this).closest('.question_wrapper');
		var questionHandleID = $wrapper.attr('data-question-handle-id');
		var $menuItem = $('.items_panel .menu_item.question[data-question-handle-id="' + questionHandleID + '"]');
		$menuItem.find('.delete_icon').trigger('click');
	});

	$('#assessment_preview_wrapper').on('mouseenter', '.question_wrapper:not(.shuffle_break)', function(e) {
		$(this).addClass('selected');
		if ($('.item_menu.open').length == 0)  {
			$(this).prepend($('#item_menu_template').html());

			var contentID = $(this).attr('data-content-id');

			var isQuestionSet = false;
			var allowPrevious = false;
			var allowNext = false;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					var thisEntry = assessmentData.Sections[i].Content[j];
					if (thisEntry.id == contentID) {
                        if ('notes' in thisEntry) {
                            $(this).find('.notes_add').remove();
                        }
                        if (thisEntry.type == 'item_set') {
                            isQuestionSet = true;

                            if (thisEntry.rendered_index > 0) allowPrevious = true;
                            if (thisEntry.rendered_index < thisEntry.question_handle_ids.length - 1) allowNext = true;

                            var questionNumber = thisEntry.rendered_index + 1;
                            var questionCount = thisEntry.question_handle_ids.length;
                            $(this).find('.set_label').html('Version ' + questionNumber + '/' + questionCount);
                        }
                    }
				}
			}

			var $item_menu_set = $(this).find('.item_menu_set');
			if (isQuestionSet) {
				$(this).find('.set_navigation').show();

				if (allowPrevious) $(this).find('.set_prev').removeAttr('disabled');
				else $(this).find('.set_prev').attr('disabled', 'disabled');
				if (allowNext) $(this).find('.set_next').removeAttr('disabled');
				else $(this).find('.set_next').attr('disabled', 'disabled');
	
				$item_menu_set.html("Edit question set");
			} else {
				$(this).find('.set_navigation').hide();
				$item_menu_set.html("Create question set");
			}

            $(this).find('.notes_add').attr('data-content-id', contentID);

			$(this).find('.item_menu_button').dropdown();
		}
	});

	var renderSetQuestion = function(contentID) {
		var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"]');

		var $itemMenuWrapper = $itemWrapper.find('.item_menu_wrapper');
		if ($itemMenuWrapper.length > 0) {
			$itemWrapper.find(':not(.item_menu_wrapper)').remove();
		} else $itemWrapper.empty();

		var questionHandleID = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var thisEntry = assessmentData.Sections[i].Content[j];
				if (thisEntry.id == contentID) {
					questionHandleID = thisEntry.question_handle_ids[thisEntry.rendered_index];

					if (thisEntry.rendered_index > 0) {
						$itemMenuWrapper.find('.set_prev').removeAttr('disabled');
					} else $itemMenuWrapper.find('.set_prev').attr('disabled', 'disabled');
					if (thisEntry.rendered_index < thisEntry.question_handle_ids.length - 1) {
						$itemMenuWrapper.find('.set_next').removeAttr('disabled');
					} else $itemMenuWrapper.find('.set_next').attr('disabled', 'disabled');

					var questionNumber = thisEntry.rendered_index + 1;
					var questionCount = thisEntry.question_handle_ids.length;
					$itemMenuWrapper.find('.set_label').html('Version ' + questionNumber + '/' + questionCount);
				}
			}
		}

		if (questionHandleID !== false) {
			$itemWrapper.attr('data-question-handle-id', questionHandleID);

			AssessmentsRender.renderItemToDiv(questionData[questionHandleID].json_data, $itemWrapper)
			.then(async function() {
				await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($('#assessment_preview_wrapper'));
				$itemWrapper.find('.part_wrapper[data-type="mc"]').each(function() { 
					RenderTools.setMCWidths($(this)); 
				});
			});	
		}
	}

	$(document).on('click', '.set_prev', function() {
		var contentID = $(this).closest('.question_wrapper').attr('data-content-id');

		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var thisEntry = assessmentData.Sections[i].Content[j];
				if ((thisEntry.id == contentID) && (thisEntry.rendered_index > 0)) {
					--thisEntry.rendered_index;
				}
			}
		}

		renderSetQuestion(contentID);

		var urlVars = getUrlVars();
		if (('c' in urlVars) && (urlVars.c == contentID)) {
			selectItem(contentID);
		}
	});

	$(document).on('click', '.set_next', function() {
		var contentID = $(this).closest('.question_wrapper').attr('data-content-id');

		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var thisEntry = assessmentData.Sections[i].Content[j];
				if ((thisEntry.id == contentID) && (thisEntry.rendered_index < thisEntry.question_handle_ids.length - 1)) {
					++thisEntry.rendered_index;
				}
			}
		}

		renderSetQuestion(contentID);

		var urlVars = getUrlVars();
		if (('c' in urlVars) && (urlVars.c == contentID)) {
			selectItem(contentID);
		}
	});

	$('#assessment_preview_wrapper').on('mouseleave', '.question_wrapper:not(.shuffle_break)', function(e) {
		if ($('.item_menu.open').length == 0)  {
			$('.question_wrapper.selected').removeClass('selected');
			$(this).find('.item_menu_wrapper').remove();
		}
	});
	
	$('#assessment_preview_wrapper').on('dblclick', '.question_wrapper:not(.shuffle_break)', function(e) {
		e.preventDefault();
		
		var $wrapper = $(this).closest('.question_wrapper');
		$('.question_wrapper.selected').removeClass('selected');
		$(this).find('.item_menu_wrapper').remove();

		var questionHandleID = $wrapper.attr('data-question-handle-id');
		window.open('/Questions/build/' + questionData[questionHandleID].id + '?a=' + assessmentID);
	});

	window.editQuestionCallback = async function(data) {

        // Need to pass both handle and question ID since the question ID may have changed after an edit, and the
        // question handle ID must be used to update the question data array. Call handle ID is here in case a new
        // version was created, which modifies both the question ID and handle ID.

        if ('assessment_version_id' in data) {
            assessmentVersionID = data.assessment_version_id;
        }

        var oldQuestionHandleID = data.old_question_handle_id;
        var oldQuestionID = data.old_question_id;

        var newQuestionHandleID = data.new_question_handle_id;
        var newQuestionID = data.new_question_id;

        if (newQuestionHandleID != oldQuestionHandleID) {
            for (var i = 0; i < assessmentData.Sections.length; ++i) {
                if (!('Content' in assessmentData.Sections[i])) continue;
                
                for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
                    var entry = assessmentData.Sections[i].Content[j];
                    if (entry.type == 'item') {
                        if (entry.question_handle_id == oldQuestionHandleID) {
                            entry.question_handle_id = newQuestionHandleID;
                        }
                    } else if (entry.type == 'item_set') {
                        for (var k = 0; k < entry.question_handle_ids.length; ++k) {
                            if (entry.question_handle_ids[k] == oldQuestionHandleID) {
                                entry.question_handle_ids[k] = newQuestionHandleID;
                            }    
                        }
                    }
                }
            }

            if (oldQuestionHandleID in questionData) {
                questionData[newQuestionHandleID] = questionData[oldQuestionHandleID];
                delete questionData[oldQuestionHandleID];    
            }
        }

        if (oldQuestionID != newQuestionID) {
            if ($('#set_questions_table').is(':visible')) {
                $('#set_questions_table tr.new_item').each(function() {
                    var entry = JSON.parse($(this).attr('data-new-entry'));
                    if (entry.question_id == oldQuestionID) {
                        entry.question_id = newQuestionID;
                        $(this).attr('data-new-entry', htmlSingleQuotes(JSON.stringify(entry)));
                    }
                });
            }    
        }

		const response = await ajaxFetch('/Questions/get/' + newQuestionID + 
			`?question_handle_id=${newQuestionHandleID}`);
		const responseData = await response.json();

		var thisData = PreviewCommon.parseQuestionData(responseData.Question);
		if (thisData !== false) {

			var questionID = thisData.id;
			var questionHandleID = thisData.question_handle_id;

			PreviewCommon.bankQuestions[questionID] = thisData;
			questionData[questionHandleID] = thisData;
			StatsCommon.fullStatistics[questionID] = responseData.Statistics;

			var assessmentChanged = false;
			assessmentChanged |= cleanOutcomes();
			assessmentChanged |= cleanWeights();
			if (assessmentChanged) saveAssessmentData();

			if ($('#set_questions_table').is(':visible')) {
				$('#set_questions_table tr.new_item').each(function() {
					var entry = JSON.parse($(this).attr('data-new-entry'));
					if (entry.question_id == questionID) {
						$(this).find('.set_question_name').html(thisData.name);
					}
				});
			}

			$('#assessment_preview_wrapper').attr('data-needs-render', 1);
			$('#sections_panel').attr('data-needs-render', 1);
			drawView();
		}
    }

    window.deleteQuestionCallback = function(data) {

        if ('assessment_version_id' in data) {
            assessmentVersionID = data.assessment_version_id;
        }

        var questionID = data.question_id;
        var questionHandleID = data.question_handle_id;

        for (var i = 0; i < assessmentData.Sections.length; ++i) {
            if (!('Content' in assessmentData.Sections[i])) continue;
            
            var index = 0;
            while (index < assessmentData.Sections[i].Content.length) {
                var entry = assessmentData.Sections[i].Content[index];
                if (entry.type == 'item') {
                    if (entry.question_handle_id == questionHandleID) {
                        assessmentData.Sections[i].Content.splice(index, 1);
                    } else index++;
                } else index++;
            }
        }

        if ($('#set_questions_table').is(':visible')) {
            $('#set_questions_table tr.new_item').each(function() {
                var entry = JSON.parse($(this).attr('data-new-entry'));
                if (entry.question_id == questionID) $(this).remove();
            });
        }

        $('#assessment_preview_wrapper').attr('data-needs-render', 1);
        $('#sections_panel').attr('data-needs-render', 1);
        drawView();

        $.gritter.add({
            title: "Success",
            text: "Queston cancelled.",
            image: "/img/success.svg"
        });
    }
	
	$(document).on('click', '.items_panel .add_bank_items', function(e) {
		e.preventDefault();
		if (!canEdit) return;
		
		var sectionID = $(this).closest('.items_panel').attr('data-section-id');

		$('#addItem_dialog').attr('data-section-id', sectionID);
		$('#addItem_dialog').attr('data-selection-type', 'many');
		$('#addItem_count').show();

		$('#addItem_dialog').dialog('option', 'title', 'Add Questions');
		$('#addItem_save').text('Add');

		var sectionType = 'mixed';
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (sectionID == assessmentData.Sections[i].id) {
				sectionType = assessmentData.Sections[i].type;
			}
		}

		$('#folder_questions').attr('data-section-type', sectionType);
		initTypeCheckboxes();

		$('#addItem_outcomes').prop('checked', false);
		$('#addItem_outcomes_wrapper').hide();

		getItems().then(addItemsCallback);
	});

	var addItemsCallback = function(addQuestionData) {
		var $dialog = $('#addItem_dialog');
		
		if (addQuestionData.length > 0) {
			var sectionID = $dialog.attr('data-section-id');
			$dialog.removeAttr('data-section-id');

			var thisSection = null;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (sectionID == assessmentData.Sections[i].id) {
					thisSection = assessmentData.Sections[i];
				}
			}
	
			if (thisSection != null) {

				for (var i = 0; i < addQuestionData.length; ++i) {
					var contentID = getNewID(4);
					thisSection.Content.push({
						type: 'item',
						id: contentID
					});
	
					var newContent = JSON.parse(JSON.stringify(addQuestionData[i]));
					newContent.content_id = contentID;
					assessmentNewQuestions.push(newContent);
				}
	
				// Save assessment
	
				if (pendingAJAXPost) return false;
				showSubmitProgress($dialog);
				pendingAJAXPost = true;
				
				saveAssessmentData()
				.then(function() {
	
					// Finish up
	
					$dialog.dialog("close");
	
					$.gritter.add({
						title: "Success",
						text: ((addQuestionData.length == 1) ? "Question" : "Questions") + " added.",
						image: "/img/success.svg"
					});
				})
				.catch(function() {
					showSubmitError($dialog);			
				});

			} else {

				$dialog.dialog("close");
	
				$.gritter.add({
					title: "Error",
					text: "Unable to add questions.",
					image: "/img/error.svg"
				});

			}
		} else $dialog.dialog("close");
	}

	$(document).on('click', '.items_panel .create_new_item', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

		if (canEdit) {
			var sectionID = $(this).closest('.items_panel').attr('data-section-id');
			$('#target_section_id').val(sectionID);

			$('#target_name').val('');
			$('#target_name_wrapper').hide();

            getTarget().then(createCallback);
		}
	});

    var createCallback = function(data) {		
		var $dialog = $('#targetFolder_dialog');
		
		var sectionID = $('#target_section_id').val();

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var w = window.open('/Questions/build_waiting');

		ajaxFetch('/Questions/create', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				folder_id: data.folder_id
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				
				var questionID = responseData.id;
				var userQuestionID = responseData.user_question_id;

				var thisData = PreviewCommon.parseQuestionData(responseData.Question);
				if (thisData !== false) PreviewCommon.bankQuestions[questionID] = thisData;

				var thisSection = null;
				for (var i = 0; i < assessmentData.Sections.length; ++i) {
					if (sectionID == assessmentData.Sections[i].id) {
						thisSection = assessmentData.Sections[i];
					}
				}

				if (thisSection !== null) {
					var contentID = getNewID(4);

					thisSection.Content.push({
						type: 'item',
						id: contentID
					});
					
					assessmentNewQuestions.push({
						content_id: contentID,
						question_id: questionID,
						name: responseData.Question.name,
						from_open: false
					});
				}

				saveAssessmentData()
				.then(function() {
				w.location.replace('/Questions/build/' + questionID + '?u=' + userQuestionID + '&a=' + assessmentID);
					$dialog.dialog('close');
				})
				.catch(function() {
					showSubmitError($dialog);			
				});

			} else {

                w.close();
				$.gritter.add({
					title: "Error",
					text: "Unable to create new question.",
					image: "/img/error.svg"
				});
				$dialog.dialog('close');

			}
		}).catch(function() {
			showSubmitError($dialog);
		});
	};

	$(document).on('click', '.items_panel .add_shuffle_break', function(e) {
		e.preventDefault();
		if (!canEdit) return;
			
		var sectionID = $(this).closest('.items_panel').attr('data-section-id');

		var thisSection = null;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (sectionID == assessmentData.Sections[i].id) {
				thisSection = assessmentData.Sections[i];
			}
		}

		thisSection.Content.push({
			id: getNewID(4),
			type: 'shuffle_break'
		});

		// Save assessment

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;
		
		saveAssessmentData()
		.then(function() {

			// Finish up

			$.gritter.add({
				title: "Success",
				text: "Shuffle break added.",
				image: "/img/success.svg"
			});
		})
		.catch(function() {
			showSubmitError();			
		});

	});

    $(document).on('click', '.items_menu .menu_item .delete_icon', function(e) {
		e.stopPropagation();
		if (!canEdit) return;
		
		var $listItem = $(this).closest('.items_menu>li');
		var contentID = $listItem.find('.content_item').attr('data-content-id');
		$('#deleteItem_dialog').attr('data-content-id', contentID);

		if ($listItem.hasClass('formatting_item')) {
			
			// This is a shuffle break. Delete it right away.
			
			$('#deleteItem_dialog').removeAttr('data-question-handle-id');
			deleteItem();

		} else if ($(this).closest('.menu_item').hasClass('question_set')) {

			// This is a question set. Open the delete item dialog.

			$('#deleteItem_dialog').removeAttr('data-question-handle-id');
			
			var itemName = $(this).closest('.menu_item').find('.menu_name').html();
			$('#deleteItem_text').html("<p>Remove the question set '" + itemName + "' from this section?</p>");
			$('#deleteItem_dialog').dialog('open');

		} else if ($(this).closest('.menu_item').hasClass('question')) {

			// This is a regular item. Open the delete item dialog.

			var parentType = $(this).closest('.menu_item').hasClass('child_item') ? 'set' : 'section';

			var questionHandleID = $(this).closest('.menu_item').attr('data-question-handle-id');
			$('#deleteItem_dialog').attr('data-question-handle-id', questionHandleID);
			
			var itemName = $(this).closest('.menu_item').find('.menu_text').html();
			$('#deleteItem_text').html("<p>Remove the question '" + itemName + "' from this " + parentType + "?</p>");
			$('#deleteItem_dialog').dialog('open');
			
		}
	});
	
	var deleteItem = function() {
		var $dialog = $('#deleteItem_dialog');

		var contentID = $dialog.attr('data-content-id');

		var sectionID = false;
		var entryType = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			var index = 0;
			while (index < assessmentData.Sections[i].Content.length) {
				if (assessmentData.Sections[i].Content[index].id == contentID) {
					sectionID = assessmentData.Sections[i].id;
					entryType = assessmentData.Sections[i].Content[index].type;
					if (entryType == 'item_set') {
						if ($dialog[0].hasAttribute('data-question-handle-id')) {

							var question_handle_ids = assessmentData.Sections[i].Content[index].question_handle_ids;

							var delete_handle_id = parseInt($dialog.attr('data-question-handle-id'));
							var delete_handle_index = question_handle_ids.indexOf(delete_handle_id);
							if (index >= 0) question_handle_ids.splice(delete_handle_index, 1);

							if (question_handle_ids.length == 0) {
								assessmentData.Sections[i].Content.splice(index, 1);
							} else if (question_handle_ids.length == 1) {
								assessmentData.Sections[i].Content[index] = {
									type: 'item',
									id: contentID,
									question_handle_id: question_handle_ids[0]
								};
							}

							entryType = 'item';
							++index;

						} else {

							assessmentData.Sections[i].Content.splice(index, 1);

						}
					} else {

						assessmentData.Sections[i].Content.splice(index, 1);

					}
				} else ++index;
			}
		}

		if (entryType !== false) {
			var urlVars = getUrlVars();
			if (('c' in urlVars) && (urlVars.c == contentID)) {
				delete urlVars.c;
				urlVars.s = sectionID;
				setUrlVars(urlVars);
			}

			// Clean up outcomes and weights as required

			cleanOutcomes();
			cleanWeights();

			// Save assessment

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			saveAssessmentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				var typeNames = {
					item: "Item",
					item_set: "Item set",
					shuffle_break: "Shuffle break"
				};

				$.gritter.add({
					title: "Success",
					text: typeNames[entryType] + " removed.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				showSubmitError($dialog);			
			});

		} else {
			showSubmitError($dialog);
		}
	}

    // basic form initialisation
    
    $('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : "bottomLeft",
		focusFirstField: false
    }).submit(function(e){ e.preventDefault() });

	$('.addSection').click(async function(e) {
		e.preventDefault();

		// Set Defaults...

		$('#dsec_id').val(getNewID(4));
		
		var type = $(this).attr('data-type');
        var header = await localizationInstance.getString('defaults_' + type + '_header');
		$('#dsec_header').val(header);
		$('#dsec_default_title').val(1);
		
		$('#dsec_show_header').prop('checked', true);
		$('#dsec_type').val($(this).attr('data-type'));
		$('#dsec_shuffle_questions').prop('checked', true);

		if (assessmentData.Scoring.type == 'Sections') {
			$('#dsec_weight').val('0');
			$('#dsec_weight_wrapper').show();
		} else $('#dsec_weight_wrapper').hide();

		$('#editSection_dialog').dialog('open');
		$('#dsec_header').select().focus();
    });
    
	$('#addPreamble').click(function(e) {
		e.preventDefault();

		$('#dpre_preamble_edit').htmleditor('setValue', '');
		$('#dpre_preamble_edit').htmleditor('initSelection');
		$('#editPreamble_form').dialog('open');
    });
    
    /* *
     * * initialize dialog box for preamble
     * */
     
    $('#editPreamble_form').dialog({
        autoOpen: false,
		width:700,
		height:535,
		modal: true,
		resizable: true,
		minWidth:700,
		minHeight:300,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': preambleSave
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));

			$('#dpre_preamble_edit').htmleditor('initSelection');
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });

    $('#dpre_preamble_edit').on('savedom', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });

	$('#deletePreamble_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deletePreamble
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

    /* *
     * * initialize dialog box for section
     * */
     
    $('#editSection_dialog').dialog({
        autoOpen: false,
        width: 420,
        modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': sectionSave
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

	$('#dsec_header').on('keyup', function() {
		$('#dsec_default_title').val(0);
	});

	$('#dsec_type').on('change', async function() {
		if ($('#dsec_default_title').val() == 1) {
			var type = $('#dsec_type').val();
            var header = await localizationInstance.getString('defaults_' + type + '_header');
			$('#dsec_header').val(header);				
		}
	});

	$('#deleteSection_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteSection
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#addItem_dialog').dialog({
        autoOpen: false,
		width: 730,
		height:550,
		modal: true,
		resizable: true,
		minWidth:730,
		minHeight:500,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {	
			var $buttonPane = $(this).find('.custom-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$('#addItem_save').addClass('btn btn-save btn_font').attr('disabled', 'disabled');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$(document).on('click', '#addItem_dialog .item_select', function() {
		var $clickedItem = $(this);
		var thisQuestionID = parseInt($(this).closest('tr').attr('data-question-id'));
		var thisQuestionName = $(this).closest('tr').find('.question_name').text();
		var addQuestionData = JSON.parse($('#addItem_dialog').attr('data-selected'));
		if ($(this).prop('checked')) {
			if ($('#addItem_dialog').attr('data-selection-type') == 'one') {
				$('#addItem_dialog .item_select').each(function() {
					if (!$(this).is($clickedItem)) {
						$(this).prop('checked', false);
						$(this).closest('tr').removeClass('doc-clicked');
					}
				});
				addQuestionData = [];
			}
			addQuestionData.push({
				question_id: thisQuestionID,
				name: thisQuestionName,
				from_open: showingOpen
			});
		} else {
			for (var i = 0; i < addQuestionData.length; ++i) {
				if (addQuestionData[i].question_id == thisQuestionID) {
					addQuestionData.splice(i, 1);
					i--;
				}
			}
		}
    	
    	if (addQuestionData.length == 0) {
			$('#addItem_save').attr('disabled', 'disabled');	
	    	$('#addItem_count').text('No questions selected.');
    	} else {
			$('#addItem_save').removeAttr('disabled');	
	    	if (addQuestionData.length == 1) {
		    	$('#addItem_count').text('1 question selected.');	    	
	    	} else {
		    	$('#addItem_count').text(addQuestionData.length + ' questions selected.');	    	
	    	}
	    }

		$('#addItem_dialog').attr('data-selected', JSON.stringify(addQuestionData));
	});

	$('.search_dropdown').on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var $menu = $(this).parent().find('.search_menu');
		if (!$menu.is(":visible")) {
			$menu.show();
			$menu.find('input').focus();
			$menu.find('input').select();
		} else $menu.hide();
	});
	
	$('#search_type').on('change', function() {
		$('#search_input').focus();
	});

	function updateSearch() {
		var $tableRows = '<tbody><tr id="folder_questions_waiting" class="disabled"><td style="border:none;"><i>Loading&hellip;</i></td></tr></tbody>';
		$('#folder_questions').html($tableRows);

        var json = Folders.getSelectedNode("folderPanel");
		showItems({
			node_id: json.data.raw_id,
			where: $('#search_type').val(),
			search: $('#search_input').val()
		});

		if ($('#search_type').val() == 'open') showingOpen = true;
		else showingOpen = false;

		$('#sort_default').hide();
		$('#sort_search').show();
		$('#item_search_menu').hide();
	}
	
	$('#search_input').on('keyup', function(e) {
		if ((e.key === 'Enter') && ($('#search_input').val().length > 0)) {
			updateSearch();
		}
	});
	
	$('#item_search_menu .search_icon').on('click', function(e) {
		if ($('#search_input').val().length > 0) updateSearch();
	});
	
	$('#deleteItem_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");

				$('.question_wrapper.selected').find('.item_menu_wrapper').remove();
				$('.question_wrapper.selected').removeClass('selected');
			},
          	'Remove': deleteItem
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Remove")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

    /* *
     * * initialize dialog box for document form
     * */
     
	$('#editSettings_form').dialog({
		autoOpen: false,
		width:685,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, open: function(event) {
			initSubmitProgress($(this));
		}, close: function(){
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
    });
    
	$('#editSettings_form').find('button:contains("Cancel")').click(function(){
		if (canCancel == 1) {
			if (pendingAJAXPost) return false;
			showSubmitProgress($('#editSettings_form'));
			pendingAJAXPost = true;

			ajaxFetch("/Assessments/delete/" + assessmentID, {
				method: 'POST'
			}).then(async response => {
				pendingAJAXPost = false;

				addClientFlash({
					title: "Success",
					text: "New assessment cancelled.",
					image: "/img/success.svg"
				});
				window.location = '/Assessments/index';
			}).catch(function() {
				showSubmitError();
			});
		} else $('#editSettings_form').dialog("close");
	});
		 	
	$('#editSettings_form').find('button:contains("Save")').click(function(){
		var contextedSave = $.proxy(settingsSave, $('#editSettings_form'));
		contextedSave();
	});
	
	var settingsSave = function() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')) {
			showSettings = 0;
			canCancel = 0;

			// Check if we'll need to shuffle after

			needsShuffle = false;
			if (assessmentData.Settings.mcFormat != $('#dset_mc_format').val()) {
				if (assessmentData.Settings.mcFormat == 'Pyramid') needsShuffle = true;
				else if ($('#dset_mc_format').val() == 'Pyramid') needsShuffle = true;
			}

			// Process settings keys

			assessmentData.Settings.pageSize = $('#dset_page_size').val();

			assessmentData.Settings.marginSizes.top = parseFloat($('#dset_topMarginSize').val());
			assessmentData.Settings.marginSizes.bottom = parseFloat($('#dset_bottomMarginSize').val());
			assessmentData.Settings.marginSizes.left = parseFloat($('#dset_leftMarginSize').val());
			assessmentData.Settings.marginSizes.right = parseFloat($('#dset_rightMarginSize').val());

			assessmentData.Settings.numberingType = $('#dset_numbering_type').val();
			assessmentData.Settings.wrResponseLocation = $('#dset_wr_response_location').val();
			assessmentData.Settings.mcFormat = $('#dset_mc_format').val();
			assessmentData.Settings.language = $('#dset_language').val();
	
			assessmentData.Title.text = $('#dset_title_text').val().split("\n");

			if ($('#dset_show_logo').prop('checked')) {
				assessmentData.Title.logo = {
					imageHash: $('#dset_logo_hash').val(),
					height: parseFloat($('#dset_logo_height').val())
				};
			} else delete assessmentData.Title.logo;

			rebuildNameLinesField();
			var newNameLines = JSON.parse($('#dset_title_nameLines').val());
			if (newNameLines.length > 0) {
				assessmentData.Title.nameLines = JSON.parse($('#dset_title_nameLines').val());
			} else delete assessmentData.Title.nameLines;

			assessmentData.Scoring.type = $('#dset_scoring_type').val();

			rebuildWeights();
			if ($('#dset_weights').val().length > 0) {
				assessmentData.Scoring.weights = JSON.parse($('#dset_weights').val());
			} else delete assessmentData.Scoring.weights;

			// Save assessment

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {

				// Update save name

				$('#document_title').text($('#dset_save_name').val());

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Settings saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				showSubmitError($dialog);			
			});
		}
	}
		 	
    $('#document_options').click(function(e) {

		$('#dset_save_name').val($('#document_title').text());

		$('#dset_page_size').val(assessmentData.Settings.pageSize);

		$('#dset_topMarginSize').val(assessmentData.Settings.marginSizes.top);
		$('#dset_bottomMarginSize').val(assessmentData.Settings.marginSizes.bottom);
		$('#dset_leftMarginSize').val(assessmentData.Settings.marginSizes.left);
		$('#dset_rightMarginSize').val(assessmentData.Settings.marginSizes.right);

		$('#dset_numbering_type').val(assessmentData.Settings.numberingType);
		$('#dset_wr_response_location').val(assessmentData.Settings.wrResponseLocation);
		$('#dset_mc_format').val(assessmentData.Settings.mcFormat);
		$('#dset_language').val(assessmentData.Settings.language);

		$('#dset_title_text').val(assessmentData.Title.text.join("\n"));

		if ('nameLines' in assessmentData.Title) {
			$('#dset_title_nameLines').val(JSON.stringify(assessmentData.Title.nameLines));
		} else $('#dset_title_nameLines').val('[]');
	    rebuildNameLines();

		$('#dset_scoring_type').val(assessmentData.Scoring.type);

		if ('weights' in assessmentData.Scoring) {
			$('#dset_weights').val(JSON.stringify(assessmentData.Scoring.weights));
		} else $('#dset_weights').val('');
		drawWeightBars();

		if ('logo' in assessmentData.Title) {
			$('#dset_logo_hash').val(assessmentData.Title.logo.imageHash);
			$('#dset_logo_height').val(assessmentData.Title.logo.height);
			$('#dset_show_logo').prop('checked', true);
			$('#dset_show_logo_label').removeClass("disabled");
		} else if (userDefaults.Common.Title.logo.imageHash === false) {
			$('#dset_show_logo').prop('checked', false).attr('disabled', 'disabled');
			$('#dset_show_logo_label').addClass("disabled");
		} else {
			$('#dset_show_logo').prop('checked', false).removeAttr('disabled');
			$('#dset_show_logo_label').removeClass("disabled");
		}
		
		$('#editSettings_form').dialog('open'); 

		resizeSettingsWrappers();

		$('.assessment_settings_tab:first-child').trigger('mousedown');
		if (showSettings) $('#dset_save_name').select();
    });

	$('#section_weights').on('keydown', '.section_weight', function(e) {
		if ((e.key === 'Enter') || (e.key === 'Tab')) {
			e.preventDefault();
			var $nextRow = $(this).closest('.section_weights_row').next('.section_weights_row');
			if ($nextRow.length != 0) {
				$nextRow.find('.section_weight').focus().select();
			}
		}
	});

    $('#section_weights').on('change', '.section_weight', function() {
	    updateWeightBars();
    });
    
    function drawWeightBars() {
		if ($('#dset_scoring_type').val() == 'Total') {
			$('#section_weights').hide();
		} else {
			var weights = ($('#dset_weights').val().length > 0) ? JSON.parse($('#dset_weights').val()) : [];
			var weightRows = [];

			var weightsHTML = "";
			if ($('#dset_scoring_type').val() == 'Sections') {
				for (var i = 0; i < assessmentData.Sections.length; ++i) {
					if (assessmentData.Sections[i].type == 'page_break') continue;
					
					var sectionID = assessmentData.Sections[i].id;
					var section_name = assessmentData.Sections[i].header;

					var section_weight = 0.0;
					for (var j = 0; j < weights.length; ++j) {
						if (weights[j].section_id == sectionID) section_weight = weights[j].weight;
					}

					var questionCount = 0;
					for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
                        var questionHandleID = false;
						if (assessmentData.Sections[i].Content[j].type == 'item') {
							questionHandleID = assessmentData.Sections[i].Content[j].question_handle_id;
						} else if (assessmentData.Sections[i].Content[j].type == 'item_set') {
							questionHandleID = assessmentData.Sections[i].Content[j].question_handle_ids[0];
						}

                        if (questionHandleID !== false) {
                            var thisData = questionData[questionHandleID].json_data;
                            for (var k = 0; k < thisData.Parts.length; ++k) {
                                if (thisData.Parts[k].type != 'context') questionCount++;
                            }    
                        }
                    }

					weightRows.push({
						id: sectionID,
						name: section_name,
						count: questionCount,
						weight: section_weight
					});
				}
			} else if ($('#dset_scoring_type').val() == 'Outcomes') {

				for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
					var outcomeID = assessmentData.Outcomes[i].id;

					var thisWeight = 0;
					for (var j = 0; j < weights.length; ++j) {
						if (weights[j].outcome_id == outcomeID) thisWeight = weights[j].weight;
					}

					weightRows.push({
						id: outcomeID,
						name: assessmentData.Outcomes[i].name,
						count: assessmentData.Outcomes[i].parts.length,
						weight: thisWeight
					});
				}
		
				weightRows.sort(function(a, b) {
					return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
				});
			}
				
			for (var j = 0; j < weightRows.length; ++j) {
				weightsHTML += 
					'<div class="section_weights_row" data-weight-id="' + weightRows[j].id + '">' + 
						'<div class="section_weight_name_wrapper">' + 
							'<div class="section_weight_name">' + weightRows[j].name + '</div>' + 
							'<div class="section_weight_count"> (' + weightRows[j].count + ' questions)</div>' + 
						'</div>' + 
						'<div>' + 
							'<div class="section_weight_label">Weight:</div>' + 
							'<input class="section_weight validate[custom[isPosDecimal]]" value="' + weightRows[j].weight + '"/>' + 
							'<div class="section_weight_visual">' + 
								'<div class="section_weight_bar"></div>' + 
								'<div class="section_weight_percent"></div>' + 
							'</div>' + 
						'</div>' + 
					'</div>';
			}

			$('#section_weights').html(weightsHTML);

			updateWeightBars();
			$('#section_weights').show();
		}

		resizeSettingsWrappers();
    }
    
    $('.assessment_settings_tab').on('click', function() {
	    if ($(this).attr('data-wrapper-id') == 'scoring_settings_wrapper') {
			$('.section_weight_name_wrapper').each(function() {
				var targetWidth = $(this).innerWidth() - 1;
				var $count = $(this).find('.section_weight_count');
				if ($count.length > 0) targetWidth -= $count.outerWidth();

				$(this).find('.section_weight_name').css('width', 'auto');
				if ($(this).find('.section_weight_name').width() > targetWidth) {
					$(this).find('.section_weight_name').width(targetWidth);
				}				
			});			
	    }
    });
    
    function updateWeightBars() {
	    var weights_total = 0.0;
	    $('.section_weight').each(function() {
			var weight = parseFloat($(this).val());
			if (!isNaN(weight)) weights_total += parseFloat($(this).val()); 
	    });
	    
	    $('.section_weights_row').each(function() {
			var weight = parseFloat($(this).find('.section_weight').val());
			if (isNaN(weight)) weight = 0.0;
						
			var weight_percent;
			if (weight == 0.0) weight_percent = 0.0;
			else weight_percent = weight * 100 / weights_total;

			$(this).find('.section_weight_bar').width(164 * weight_percent / 100);
			$(this).find('.section_weight_percent').html(weight_percent.toFixed(0) + "%");
	    });
    }
    
    function rebuildWeights() {
		if ($('#dset_scoring_type').val() == 'Sections') {
			var weights = [];
		    $('.section_weights_row').each(function() {
			    var sectionID = $(this).attr('data-weight-id');
				var weight = parseFloat($(this).find('.section_weight').val());
				weights.push({
					section_id: sectionID,
					weight:	weight
				});
			});
			$('#dset_weights').val(JSON.stringify(weights));
		} else if ($('#dset_scoring_type').val() == 'Outcomes') {
			var weights = [];
		    $('.section_weights_row').each(function() {
			    var outcome_id = $(this).attr('data-weight-id');
				var weight = parseFloat($(this).find('.section_weight').val());
				weights.push({
					outcome_id: outcome_id,
					weight: weight
				});
			});
			$('#dset_weights').val(JSON.stringify(weights));
		} else $('#dset_weights').val('');
    }

	$('#dset_scoring_type').on('change', function() {
		rebuildWeights();
		drawWeightBars();
	});

	function cleanOutcomes() {
        var outcomesChanged = false;

		var allowedContentIDs = [];
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				allowedContentIDs.push(assessmentData.Sections[i].Content[j].id);
			}
		}

		for (var outcomeIndex = 0; outcomeIndex < assessmentData.Outcomes.length; ++outcomeIndex) {
			var partIndex = 0;
			while (partIndex < assessmentData.Outcomes[outcomeIndex].parts.length) {
                var outcomesEntry = assessmentData.Outcomes[outcomeIndex].parts[partIndex];
                var deleteEntry = false;
				if (allowedContentIDs.indexOf(outcomesEntry.content_id) >= 0) {

                    var questionHandleIDs = [];
                    for (var i = 0; i < assessmentData.Sections.length; ++i) {
                        if (assessmentData.Sections[i].type == 'page_break') continue;
                        for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
                            var assessmentEntry = assessmentData.Sections[i].Content[j];
                            if (assessmentEntry.id == outcomesEntry.content_id) {
                                if (assessmentEntry.type == 'item') {
                                    questionHandleIDs = [assessmentEntry.question_handle_id];
                                } else if (assessmentEntry.type == 'item_set') {
                                    questionHandleIDs = assessmentEntry.question_handle_ids;
                                }
                            }
                        }
                    }

                    for (var i = 0; i < questionHandleIDs.length; ++i) {
                        var thisData = questionData[questionHandleIDs[i]].json_data;

                        if ('part_id' in outcomesEntry) {

                            var hasPart = false;
                            for (var i = 0; i < thisData.Parts.length; ++i) {
                                if (thisData.Parts[i].id == outcomesEntry.part_id) hasPart = true;
                            }
                            if (!hasPart) deleteEntry = true;

                        } else if ('part_index' in outcomesEntry) {

                            if (outcomesEntry.part_index >= thisData.Parts.length) deleteEntry = true;
                            else if (thisData.Parts[outcomesEntry.part_index].type == 'context') deleteEntry = true;
                            
                        }    
                    }

                } else deleteEntry = true;

                if (deleteEntry) {
					assessmentData.Outcomes[outcomeIndex].parts.splice(partIndex, 1);
                    outcomesChanged = true;
                } else ++partIndex;
			}
		}	

        return outcomesChanged;
	}

	function cleanWeights() {
        var weightsChanged = false;

		if (assessmentData.Scoring.type != 'Total') {
			var idName;
			var allowedValues = [];

			if (assessmentData.Scoring.type == 'Sections') {
				idName = 'sectionID';
				for (var i = 0; i < assessmentData.Sections.length; ++i) {
					if (assessmentData.Sections[i].type != 'page_break') {
						allowedValues.push(assessmentData.Sections[i].id);
					}
				}
				
			} else if (assessmentData.Scoring.type == 'Outcomes') {
				idName = 'outcome_id';
				for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
					allowedValues.push(assessmentData.Outcomes[i].id);
				}
			}
	
			var weightIndex = 0;
			while (weightIndex < assessmentData.Scoring.weights.length) {
				var thisID = assessmentData.Scoring.weights[weightIndex][idName];
				if (allowedValues.indexOf(thisID) == -1) {
					assessmentData.Scoring.weights.splice(weightIndex, 1);
                    weightsChanged = true;
				} else ++weightIndex;
			}

		}

        return weightsChanged;
	}
    
	function resizeSettingsWrappers() {
		$('#editSettings_buttons').css('margin-top', '5px');
		$('#editSettings_wrapper_left').css('height', 'auto');
		var leftWrapperHeight = $('#editSettings_wrapper_left').outerHeight();
		$('#editSettings_wrapper_right').css('height', 'auto');
		var rightWrapperHeight = $('#editSettings_wrapper_right').outerHeight();

		var newHeight = Math.max(leftWrapperHeight, rightWrapperHeight);
		$('#editSettings_wrapper_left').outerHeight(newHeight);
		$('#editSettings_wrapper_right').outerHeight(newHeight);
		if (rightWrapperHeight > leftWrapperHeight) {
			var newTopMargin = (rightWrapperHeight - leftWrapperHeight + 5) + 'px';
			$('#editSettings_buttons').css('margin-top', newTopMargin);
		}
	}
	
	$('.assessment_settings_tab').on('mousedown', function() {
		if (!$(this).hasClass('selected')) {
			var $form = $(this).closest('form');
			if ($form.validationEngine('validate')) {
				$('.assessment_settings_tab').removeClass('selected');
				$(this).addClass('selected');
				
				$('.settings_wrapper').hide();
				$('#' + $(this).attr('data-wrapper-id')).show();
				
				resizeSettingsWrappers();
			}
		}
		return false;
	});

    $('#dset_show_logo').click(function() {
		if ($('#dset_show_logo').prop('checked')) {
			$('#dset_logo_hash').val(userDefaults.Common.Title.logo.imageHash);
			$('#dset_logo_height').val(userDefaults.Common.Title.logo.height);
		} else {
			$('#dset_logo_hash').val('');
			if (userDefaults.Common.Title.logo.imageHash === false) {
				$('#dset_show_logo').attr('disabled', 'disabled');
				$('#dset_show_logo_label').addClass("disabled");
			} else {
				$('#dset_show_logo').removeAttr('disabled');
				$('#dset_show_logo_label').removeClass("disabled");
			}
		} 
    });

	$('#dset_nameLines_wrapper').on('keydown', 'input', function(e) {
		if ((e.key === 'Tab') || (e.key === 'Enter')) {
			e.preventDefault();
			var $nextRow = $(this).closest('.nameLines_row').next('.nameLines_row');
			if ($nextRow.length != 0) {
				$nextRow.find('input').focus().select();
			} else {
				$(this).closest('.nameLines_row').find('.namelineAdd').trigger('click');
			}
		}
	});

	$('#dset_nameLines_wrapper').on('click', '.namelineAdd', function() {
		var index = parseInt($(this).attr('data-index')) + 1;
		if ($(this).attr('data-index') == -1) {
			var nameLines = userDefaults.Common.Title.nameLines;
			$('#dset_title_nameLines').val(JSON.stringify(nameLines));
		} else {
			rebuildNameLinesField();
			var nameLines = JSON.parse($('#dset_title_nameLines').val());
			nameLines.splice(index, 0, "");
			$('#dset_title_nameLines').val(JSON.stringify(nameLines));
		}
		rebuildNameLines();
		$('#dset_nameLines_wrapper .nameLines_row').eq(index).find('input').focus();
    });
    
	$(document).on('click', '.namelineDelete', function() {
		rebuildNameLinesField();
		var index = parseInt($(this).attr('data-index'));
		var nameLines = JSON.parse($('#dset_title_nameLines').val());
		nameLines.splice(index, 1);
		$('#dset_title_nameLines').val(JSON.stringify(nameLines));
   	    rebuildNameLines();
    });

	function rebuildNameLinesField() {
		var nameLines = [];
		var i = 0;
		while ($('#dset_nameline_' + i).length > 0) {
			nameLines.push($('#dset_nameline_' + i).val());
			++i;
		}
		$('#dset_title_nameLines').val(JSON.stringify(nameLines));
	}

	function rebuildNameLines() {
		var nameLines = JSON.parse($('#dset_title_nameLines').val());
		var add_to = "";
		if (nameLines.length == 0) {
			add_to += '<div class="nameLines_row" style="padding-bottom:5px;">';
			add_to += '<input class="defaultTextBox2" style="width:200px !important; margin-bottom:0px;" type="text" value="No name lines shown" disabled />';
			add_to += '<button title="Add" class="actions_button add_icon namelineAdd" style="margin-left:5px;" data-index=-1>&nbsp;</button>';
			add_to += '<button title="Delete" class="actions_button delete_icon namelineDelete" data-index=-1 disabled>&nbsp;</button>';
			add_to += '</div>';
		} else {
			for (var i = 0; i < nameLines.length; ++i) {
				add_to += '<div class="nameLines_row" style="padding-bottom:5px;">';
				add_to += '<input id="dset_nameline_' + i + '" class="validate[required] defaultTextBox2" style="width:200px !important; margin-bottom:0px;" type="text" value="' + nameLines[i] + '" />';
				add_to += '<button title="Add" class="actions_button add_icon namelineAdd" style="margin-left:5px;" data-index=' + i + '>&nbsp;</button>';
				add_to += '<button title="Delete" class="actions_button delete_icon namelineDelete" data-index=' + i + '>&nbsp;</button>';
				add_to += '</div>';
			}
		}
		$('#dset_nameLines_wrapper').html(add_to);
		
		resizeSettingsWrappers();
	}
	
	$('#addItem_dialog .type_checkbox').on('change', function() {
		updateAllowedQuestions();
	});

    /* *
     * * section click
     * */
     
    $(document).on('click', '#sections_menu .menu_item', function(e) {
        e.preventDefault();

		var sectionID = $(this).attr('data-section-id');
		selectSection(sectionID);

		var urlVars = getUrlVars();
		if (urlVars.view == 'preview') {
			$('.question_wrapper').removeClass('question_selected');
		} else if (urlVars.view == 'outcomes') {
			drawOutcomes();
		} else if (urlVars.view == 'statistics') {
			drawStatistics();
		}

		if ($draggingItem == null) {
            ScrollingCommon.resetLeftPanel();
		}
    });

	var selectSection = function(sectionID) {
		$('#sections_menu .menu_item').removeClass('active');
		$('#sections_menu .menu_item[data-section-id="' + sectionID + '"]').addClass('active');

		$('.items_panel').hide();
		$('#items_panel_' + sectionID).show();

		$('.items_menu .menu_item').removeClass('active');

		var urlVars = getUrlVars();
		urlVars.s = sectionID;
		delete urlVars.c;
		setUrlVars(urlVars);

		var thisSection = null;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (sectionID == assessmentData.Sections[i].id) thisSection = assessmentData.Sections[i];
		}
		if (thisSection != null) {
			$('#folder_questions').attr('data-section-type', thisSection.type);
			initTypeCheckboxes();	
		}
	}
	
	/** edit section **/

	$(document).on('click', '#preamble_menu .edit_icon', function(e) {
		if (!canEdit) return;
		e.stopPropagation();

		var preamble_html = ('Preamble' in assessmentData) ? assessmentData.Preamble.html : '';
		$('#dpre_preamble_edit').htmleditor('setValue', preamble_html);
		$('#dpre_preamble_edit').htmleditor('initSelection');
		$('#editPreamble_form').dialog('open');
	});

	$(document).on('click', '#sections_menu .edit_icon', function(e) {
		if (!canEdit) return;
		e.stopPropagation();

		var sectionID = $(this).closest('.menu_item').attr('data-section-id');
		
		var thisSection = null;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (sectionID == assessmentData.Sections[i].id) thisSection = assessmentData.Sections[i];
		}

		$('#dsec_id').val(sectionID);

		$('#dsec_header').val(thisSection.header);
		$('#dsec_default_title').val(0);

		$('#dsec_show_header').prop('checked', thisSection.showHeader);
		$('#dsec_type').val(thisSection.type);
		$('#dsec_shuffle_questions').prop('checked', thisSection.shuffleQuestions);
		
		if (assessmentData.Scoring.type == 'Sections') {
			var weights = assessmentData.Scoring.weights;

			var thisWeight = 0.0;
			var totalWeight = 0.0;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				var weight_section_id = assessmentData.Sections[i].id;
				var section_weight = 0.0;
				for (var j = 0; j < weights.length; ++j) {
					if (weights[j].section_id == weight_section_id) section_weight = weights[j].weight;
				}

				if (weight_section_id == sectionID) thisWeight = section_weight;
				totalWeight += section_weight;
			}
			if (totalWeight == 0.0) $('#dsec_weight').html('0');
			else {
				var weight_percent = thisWeight * 100 / totalWeight;
				$('#dsec_weight').html(weight_percent.toFixed(0));
			}
			$('#dsec_weight_wrapper').show();
		} else $('#dsec_weight_wrapper').hide();

		$('#editSection_dialog').dialog('open');
		$('#dsec_header').focus();
    });

	/** remove section */
	
    $(document).on('click', '#preamble_menu .delete_icon', function(e) {
		if (!canEdit) return;
		e.stopPropagation();
		
		$('#deletePreamble_dialog').dialog('open');
	});

    $(document).on('click', '#sections_menu .delete_icon', function(e) {
		if (!canEdit) return;
		e.stopPropagation();
		
		var sectionID = $(this).closest('.menu_item').attr('data-section-id');
		$('#deleteSection_dialog').attr('data-section-id', sectionID);
		
		var thisSection = null;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (sectionID == assessmentData.Sections[i].id) thisSection = assessmentData.Sections[i];
		}

		if (thisSection.type == 'page_break') {
			var contextedDelete = $.proxy(deleteSection, $('#deleteSection_dialog'));
			contextedDelete();
		} else {
			$('#deleteSection_text').html('<p>Permanently delete the section "' + thisSection.header + '" from this assessment?</p>');
			$('#deleteSection_dialog').dialog('open');	
		}
	});

    /* *
     * * item hover and click
     * */
     
    $(document).on('click', '.items_menu .menu_item', function(e) {
        e.stopPropagation();

		// Select the item in the assessment preview
		
		var $parent = $(this).closest('.items_menu>li');
		if ($parent.is('.formatting_item')) return false;

		var contentID = $parent.find('.content_item').attr('data-content-id');
		var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"]');

		if ($(this).is('.child_item')) {
			var rendered_index = $(this).closest('li').index();

			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					if (assessmentData.Sections[i].Content[j].id == contentID) {
						assessmentData.Sections[i].Content[j].rendered_index = rendered_index;
					}
				}
			}

			renderSetQuestion(contentID);
		}

		selectItem(contentID);

		var urlVars = getUrlVars();
		if (urlVars.view == 'preview') {

			// Highlight question
			
			$('#assessment_preview_wrapper .question_wrapper').removeClass('question_selected');
			$itemWrapper.addClass('question_selected');

			var itemTop = $itemWrapper.offset().top;
			$("html, body").animate({
				scrollTop: itemTop - 10
			}, 1000);

		} else if (urlVars.view == 'outcomes') {

            $('#assessment_preview_wrapper').attr('data-needs-render', 1);
			drawOutcomes();

		} else if (urlVars.view == 'statistics') {

            $('#assessment_preview_wrapper').attr('data-needs-render', 1);
			drawStatistics();
            
		}
    });    
    
    $('#assessment_preview_wrapper').on('click', '.question_wrapper', function(e) {
        
        // Select the item in the assessment preview
        
		if ($(e.target).closest('.item_menu_wrapper').length == 0) {
			var contentID = $(this).attr('data-content-id');
			selectItem(contentID);

			// Highlight question

			$('#assessment_preview_wrapper .question_wrapper').removeClass('question_selected');
			var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"]');
			$itemWrapper.addClass('question_selected');
		}
    });    
    
    var selectItem = function(contentID) {

		var urlVars = getUrlVars();
		urlVars.c = contentID;
		delete urlVars.s;
		setUrlVars(urlVars);

		var $menuItem = $('.menu_item[data-content-id="' + contentID + '"]');
		var $items_panel = $menuItem.closest('.items_panel');
		var sectionID = $items_panel.attr('data-section-id');

	   	// Unhighlight sections except for the active one

		$('#sections_menu .menu_item').removeClass('active');
		$('#sections_menu .menu_item[data-section-id="' + sectionID + '"]').addClass('active');

		// Open item panel

		$('.items_panel').hide();
		$items_panel.show();

	   	// Highlight the selected item link and unhighlight others

		   $('.items_menu').find('.menu_item').removeClass('active');
		   $menuItem.addClass('active');
   
		// Open item sets as needed

		if ($menuItem.is('.question_set')) {
			$menuItem.find('.question_set_reveal').addClass('selected');
			var $children = $menuItem.siblings('.child_menu').find('.child_item');
			$children.show();

			var rendered_index = 0;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					if (assessmentData.Sections[i].Content[j].id == contentID) {
						rendered_index = assessmentData.Sections[i].Content[j].rendered_index;
					}
				}
			}

			$children.eq(rendered_index).addClass('active');
		}

		ScrollingCommon.fixLeftPanelPosition();
    }

	$(document).on('click', '.items_panel .question_set_reveal', function(e) {
        e.stopPropagation();

		var $thisItem = $(this).closest('.menu_item');

		if ($(this).is('.selected')) {
			$(this).removeClass('selected');
			$thisItem.siblings('.child_menu').find('.child_item').hide();
		} else {
			$(this).addClass('selected');
			$thisItem.siblings('.child_menu').find('.child_item').show();
		}
	});

	$('#reloadButton').on('click', function(e) {
		initialize().then(function() {
			$.gritter.add({
				title: "Success",
				text: "Assessment reloaded.",
				image: "/img/success.svg"
			});	
		});
	});

	$('#shuffleItems').on('click', function(e) {
		$('.buildButton').attr('disabled', 'disabled');
		$('#view_menu').menu('disable');

		// Shuffle questions and parts

		AssessmentsRender.shuffleAssessmentQuestions(assessmentData);
		AssessmentsRender.shuffleAssessmentParts(assessmentData, questionData);

		drawLeftBar();
		drawAssessmentPreview().then(async function() {
			AssessmentsRender.shuffleAssessmentAnswers(assessmentData, questionData, $('#assessment_preview_wrapper'));
			await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($('#assessment_preview_wrapper'));
        
			if (assessmentData.Settings.mcFormat == 'Auto') {
				$('#assessment_preview_wrapper .part_wrapper[data-type="mc"]').each(function() { 
					RenderTools.setMCWidths($(this)); 
				});
			} else $('#assessment_preview_wrapper .question_answer').css('width', '100%');

			var urlVars = getUrlVars();
			if ('c' in urlVars) {
		
				// Highlight question
				
				$('#assessment_preview_wrapper .question_wrapper').removeClass('question_selected');
				var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + urlVars.c + '"]');
				$itemWrapper.addClass('question_selected');
			}

			$('.buildButton').removeAttr('disabled');
			$('#view_menu').menu('enable');
		});
	});

	var saveOutcomes = function(e) {
		var $dialog = $(this);                                                 // Dialog that called the save
		var $form = $dialog.find('form');                                      // Form in that dialog

		var valid = true;

		var $pendingOutcomeInput = $('#outcomes_add_row .edit_outcomes_area');

		if ($pendingOutcomeInput.val().length > 0) {
			$pendingOutcomeInput.validationEngine('showPrompt', "* Click '+' to add this outcome before saving", 'error', 'bottomLeft', true);
			valid = false;
		}

		if (valid && $form.validationEngine('validate')) {

			// Update outcomes

			var newOutcomes = [];

			var editOutcomes = rebuildEditOutcomesJSON();
			for (var i = 0; i < editOutcomes.length; ++i) {
				if ('id' in editOutcomes[i]) {
					var outcomeID = editOutcomes[i].id;
					for (var j = 0; j < assessmentData.Outcomes.length; ++j) {
						if (assessmentData.Outcomes[j].id == outcomeID) {
							var newEntry = assessmentData.Outcomes[j];
							newEntry.name = editOutcomes[i].name;
							newOutcomes.push(newEntry);
						}
					}
				} else {
					newOutcomes.push({
						id: getNewID(4),
						name: editOutcomes[i].name,
						parts: []
					});
				}
			}

			assessmentData.Outcomes = newOutcomes;
			cleanWeights();

			// Save outcomes

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Outcomes saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function(error) {
				showSubmitError();			
			});
		} else {
			setTimeout(function() {
				document.activeElement.blur();
			}, 0);
		}
	}

	var updateOutcome = function(e) {
		var $dialog = $(this);                                                 // Dialog that called the save
		var $form = $dialog.find('form');                                      // Form in that dialog
		
		var urlVars = getUrlVars();
		if (('c' in urlVars) && $form.validationEngine('validate')) {

			var newParts = [];
			$('.outcome_checkbox').each(function() {
				if ($(this).prop('checked')) {
					var thisEntry = {content_id: urlVars.c};
					if (this.hasAttribute('data-part-id')) thisEntry.part_id = $(this).attr('data-part-id');
					if (this.hasAttribute('data-part-index')) thisEntry.part_index = parseInt($(this).attr('data-part-index'));
					newParts.push(thisEntry);
				}
			});
	
			// Update outcome

			var outcomeID = $('#cat_id').val();

			for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
				if (assessmentData.Outcomes[i].id == outcomeID) {
					var index = 0;
					while (index < assessmentData.Outcomes[i].parts.length) {
						if (assessmentData.Outcomes[i].parts[index].content_id == urlVars.c) {
							assessmentData.Outcomes[i].parts.splice(index, 1);
						} else ++index;
					}

					assessmentData.Outcomes[i].parts = assessmentData.Outcomes[i].parts.concat(newParts);	
				}
			}

			// Save outcomes

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {

				// Finish up

				$dialog.dialog("close");

				$.gritter.add({
					title: "Success",
					text: "Outcome saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function(error) {
				showSubmitError();			
			});
		}
    }
    
    var removeOutcome = function() {
		var $dialog = $(this);

		var urlVars = getUrlVars();
		if ('c' in urlVars) {
			var outcomeID = $('#removeOutcome_dialog').attr('data-outcome-id');
	
			// Update outcome
	
			for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
				if (assessmentData.Outcomes[i].id == outcomeID) {
					var index = 0;
					while (index < assessmentData.Outcomes[i].parts.length) {
						if (assessmentData.Outcomes[i].parts[index].content_id == urlVars.c) {
							assessmentData.Outcomes[i].parts.splice(index, 1);
						} else ++index;
					}
				}
			}
	
			// Save outcomes
	
			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
			
			saveAssessmentData()
			.then(function() {
	
				// Finish up
	
				$dialog.dialog("close");
	
				$.gritter.add({
					title: "Success",
					text: "Outcome removed.",
					image: "/img/success.svg"
				});
			})
			.catch(function(error) {
				showSubmitError();			
			});
		}
    }
    
    $('#editOutcomes_dialog').dialog({
        autoOpen: false,
		width: 600,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': saveOutcomes
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

	$('#manageOutcome_dialog').dialog({
        autoOpen: false,
		width: 595,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
            'Cancel': function() {
				$(this).dialog("close");
		    },
            'Save': updateOutcome
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
			initSubmitProgress($(this));
			document.activeElement.blur();
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

	$('#outcomes_checkAll').click(function() {
		$('#outcome_selection_list .outcome_checkbox').prop('checked', true);
    });

	$('#outcomes_clearAll').click(function() {
		$('#outcome_selection_list .outcome_checkbox').prop('checked', false);
    });

	$('#removeOutcome_dialog').dialog({
        autoOpen: false,
		width: 450,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Remove': removeOutcome
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Remove")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	$('#editOutcomes').on('click', function(e) {
		e.preventDefault();
		if (!canEdit) return;

		var editOutcomes = [];
		for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
			editOutcomes.push({
				id: assessmentData.Outcomes[i].id,
				name: assessmentData.Outcomes[i].name
			});
		}
		$('#outcomes_json').val(JSON.stringify(editOutcomes));

		$('#outcomes_add_row .edit_outcomes_area').val('');

		$('#editOutcomes_dialog').dialog('open');
		rebuildEditOutcomesView();
	});

	var addOutcomes = function(newOutcomes) {
		var editOutcomes = rebuildEditOutcomesJSON();

		var numAdded = 0;
		var numSkipped = 0;
		for (var i = 0; i < newOutcomes.length; ++i) {
			var outcomeName = newOutcomes[i].trim();

			if (outcomeName.length > 0) {
				var isNew = true;
				for (var j = 0; j < editOutcomes.length; ++j) {
					if (outcomeName == editOutcomes[j].name) isNew = false;
				}

				if (isNew) {
					editOutcomes.push({ name: outcomeName });
					numAdded++;
				} else numSkipped++;
			}
		}

		if ((numAdded > 0) || (numSkipped > 0)) {
			$('#outcomes_json').val(JSON.stringify(editOutcomes));	
			rebuildEditOutcomesView();

			var notificationText;
			if ((numAdded == 1) && (numSkipped == 0)) {
				notificationText = "New outcome added.";
			} else if (numAdded > 0) {
				notificationText = "Added " + numAdded + " outcome";
				if (numAdded != 1) notificationText += "s";
				if (numSkipped > 0) {
					var plural = (numSkipped != 1);
					notificationText += "; skipped " + numSkipped + " that " + (plural ? "were" : "was") + 
						" already included";
				}
				notificationText += ".";
			} else if (numSkipped > 0) {
				var plural = (numSkipped != 1);
				notificationText += "Skipped " + numSkipped + " outcome" + (plural ? "s" : "");
				notificationText += " that " + (plural ? "were" : "was") + " already included.";
		}

			$.gritter.add({
				title: "Success",
				text: notificationText,
				image: "/img/success.svg"
			});
		}		
	}

	$(document).on('click', '.outcomeAdd', function() {
		var $outcomesRow = $(this).closest('.outcomes_edit_row');
		var $input = $outcomesRow.find('.edit_outcomes_input');
		if ($input.length == 0) $input = $outcomesRow.find('.edit_outcomes_area');

		var lines = $input.val().split("\n");
		addOutcomes(lines);
		$input.val('').trigger('input');
		$input.focus();
	});
    
	$(document).on('click', '.outcomeDelete', function() {
		var editOutcomes = rebuildEditOutcomesJSON();

		var $outcomesRow = $(this).closest('.outcomes_edit_row');
		var index = $outcomesRow.index();

		editOutcomes.splice(index, 1);
		$('#outcomes_json').val(JSON.stringify(editOutcomes));

		$.gritter.add({
			title: "Success",
			text: "Outcome deleted.",
			image: "/img/success.svg"
		});

		rebuildEditOutcomesView();
    });

	$('#outcomes_copy').on('click', function() {
		var text = "";
		var editOutcomes = rebuildEditOutcomesJSON();
		for (var i = 0; i < editOutcomes.length; ++i) {
			if (i > 0) text += "\n";
			text += editOutcomes[i].name;
		}
		copyText(text, "outcomes");
	});

	$('#outcomes_add_row').on('keypress', '.edit_outcomes_area', function(e) {
		if (e.key === 'Enter') {
			e.preventDefault();
			var $input = $('#outcomes_add_row .edit_outcomes_area');
	
			var lines = $input.val().split("\n");
			addOutcomes(lines);
			$input.val('').trigger('input');
			$input.focus();
		}
	});

	var rebuildEditOutcomesJSON = function() {
		var editOutcomes = [];
		$('#edit_outcomes_list .outcomes_edit_row').each(function() {
			var $input = $(this).find('.edit_outcomes_input');
			if ($input.length == 0) $input = $(this).find('.edit_outcomes_area');

			var entry = { name: $input.val() };
			if (this.hasAttribute('data-outcome-id')) {
				entry.id = $(this).attr('data-outcome-id');
			}
			editOutcomes.push(entry);
		});

		$('#outcomes_json').val(JSON.stringify(editOutcomes));
		return editOutcomes;
	}

	var rebuildEditOutcomesView = function() {
		var editOutcomes = JSON.parse($('#outcomes_json').val());

		editOutcomes.sort(function (a, b) {
			return a.name < b.name ? -1 : (a.name > b.name ? 1 : 0);
		});

	    var viewHTML = "";
		if (editOutcomes.length > 0) {
			for (var j = 0; j < editOutcomes.length; ++j) {
				viewHTML += '<div class="outcomes_edit_row save_row"';
				if ('id' in editOutcomes[j]) viewHTML += ' data-outcome-id="' + editOutcomes[j].id;
				viewHTML += '">';
				viewHTML += '<input type="text" class="validate[required] edit_outcomes_input" value="' + editOutcomes[j].name + '" />';
				viewHTML += '<button title="Delete" class="actions_button delete_icon outcomeDelete">&nbsp;</button>';
				viewHTML += '</div>';
			}
		} else {
			viewHTML += '<div class="outcomes_edit_empty">No outcomes to show here. Type an outcome name into the \
				field below, or paste a list containing one outcome per line, then click \
				<span class="add_icon" style="display:inline-block; width:15px; height:10px;"></span> to add them.</div>';
		}
		$('#edit_outcomes_list').html(viewHTML);
	}

	$(document).on('focus', '.save_row .edit_outcomes_input', function(e) {
		$(this).closest('form').validationEngine('hideAll');

		var $row = $(this).closest('.outcomes_edit_row');
		var row_index = $row.index();
		var blurIndex = blurWaiting.indexOf(row_index);
		if (blurIndex >= 0) blurWaiting.splice(blurIndex, 1);
		focusWaiting.push(row_index);			
	});

	$(document).on('blur', '.save_row .edit_outcomes_area', function(e) {
		var $row = $(this).closest('.outcomes_edit_row');
		var row_index = $row.index();
		var focusIndex = focusWaiting.indexOf(row_index);
		if (focusIndex >= 0) focusWaiting.splice(focusIndex, 1);
		blurWaiting.push(row_index);
	});

	$(document).on('click keyup', function(e) {
		
		// Handle blur and focus for input fields on click so that we don't (for example)
		// move dialog buttons out of the way when a field shrinks.
		
		setTimeout(function() {
			while (blurWaiting.length > 0) {
				var index = blurWaiting.pop();
				var $input = $('#edit_outcomes_list .outcomes_edit_row').eq(index).find('.edit_outcomes_area');

                if ($input.length > 0) {
                    var $newElement = $('<input type="text" class="validate[required] edit_outcomes_input" value="' + $input.val() + '"/>');
                    $input.replaceWith($newElement);    
                }
			}
			while (focusWaiting.length > 0) {
				var index = focusWaiting.pop();
				var $input = $('#edit_outcomes_list .outcomes_edit_row').eq(index).find('.edit_outcomes_input');

                if ($input.length > 0) {
                    var selectionStart = $input[0].selectionStart;
                    var $newElement = $('<textarea class="edit_outcomes_area">' + $input.val() + '</textarea>');
                    $input.replaceWith($newElement);
    
                    resizeTextarea($newElement[0]);
                    $newElement.focus();
                    $newElement[0].setSelectionRange(selectionStart, selectionStart);	    
                }
			}
		}, 0);
	});

	$(document).on('input', '.edit_outcomes_area', function(e) {
		resizeTextarea(this);
	});

	function resizeTextarea(element) {
		element.style.height = '0px';
		element.style.height = (element.scrollHeight + 2) + 'px';
	}

    $('#itemOutcomes').on('click', '.actions_button.delete_icon', function(e) {
		e.preventDefault();
		if (!canEdit) return;
		
		var $outcomeRow = $(this).closest('.outcome_row');
		var outcomeName = $outcomeRow.children('.outcome_name').text();
		var outcomeID = $outcomeRow.attr('data-outcome-id');

		var shortLength = 50;
		var shortName = outcomeName;
		if (shortName.length > shortLength) {
			shortName = shortName.substr(0, shortLength - 1) + "…";
		}
		$('.cat_delete_name').text(shortName);

		$('#removeOutcome_dialog').attr('data-outcome-id', outcomeID);
		$('#removeOutcome_dialog').dialog('open');
    });
    
    $('#itemOutcomes').on('click', '.actions_button.view_icon', function(e) {
		e.preventDefault();

	 	var $outcomeRow = $(this).closest('.outcome_row');
		var outcomeID = $outcomeRow.attr('data-outcome-id');

		var outcomeData = false;
		for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
			if (assessmentData.Outcomes[i].id == outcomeID) outcomeData = assessmentData.Outcomes[i];
		}

		$('.view_button[data-view-type="preview"]').trigger('click');

		// Do the following after the view menu click has processed

		setTimeout(function() {

            $('#assessment_preview_wrapper .part_wrapper').removeClass('part_highlighted');
            
            for (var i = 0; i < outcomeData.parts.length; ++i) {
                var contentID = outcomeData.parts[i].content_id;
                var $questionWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"]');

                var partID = false;
                if ('part_id' in outcomeData.parts[i]) {

                    partID = outcomeData.parts[i].part_id;

                } else if ('part_index' in outcomeData.parts[i]) {

                    var questionHandleID = $questionWrapper.attr('data-question-handle-id');
                    var thisQuestionData = questionData[questionHandleID];

                    for (var j = 0; j < thisQuestionData.Parts.length; ++j) {
                        if (thisQuestionData.Parts[j].index == outcomeData.parts[i].part_index) {
                            partID = thisQuestionData.Parts[j].id;
                        }
                    }
                    
                }

                if (partID !== false) {
                    var $partWrapper = $questionWrapper.find('.part_wrapper[data-part-id="' + partID + '"]');
                    $partWrapper.addClass('part_highlighted');    	
                }
            }

            var $wrapper = $('#assessment_preview_wrapper .part_wrapper.part_highlighted').first();
            var itemTop = $wrapper.offset().top;
            $("html, body").animate({
                scrollTop: itemTop - 10
            }, 1000);

			$('#assessment_preview_wrapper .question_wrapper').removeClass('question_selected');
			$(document).on('click', clearHighlight);
		}, 0);
	});

	function clearHighlight(e) {
		$('.part_highlighted').removeClass('part_highlighted');
		$(document).off('click', clearHighlight);
	}

    $('#itemOutcomes').on('click', '.actions_button.add_icon', function(e) {
		e.preventDefault();
		if (!canEdit) return;
	 	
		var contentID = $('#outcomes_item .question_wrapper').attr('data-content-id');

		var itemType = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var itemData = assessmentData.Sections[i].Content[j];
				if (itemData.id == contentID) itemType = itemData.type;
			}
		}

		var questionHandleID = $('#outcomes_item .question_wrapper').attr('data-question-handle-id');
		var thisData = questionData[questionHandleID].json_data;

	 	var cat_checkboxes = "";

	 	var i = 0;
	 	$('#outcomes_item .part_wrapper').each(function() {
			var partID = $(this).attr('data-part-id');

			cat_checkboxes += "<div class='outcome_selection_entry'><label for='cat_include_part_" + i + "'>" + 
				$(this).find('.question_number').html() + "</label><input type='checkbox' class='outcome_checkbox' ";
			if (itemType == 'item') {

				cat_checkboxes += "data-part-id='" + partID + "'";

			} else if (itemType == 'item_set') {

				var partIndex = false;
				for (var j = 0; j < thisData.Parts.length; ++j) {
					if (thisData.Parts[j].id == partID) partIndex = thisData.Parts[j].index;
				}
				cat_checkboxes += "data-part-index='" + partIndex + "'";

			}
			cat_checkboxes += " id='cat_include_part_" + i + "'></div>";
			
			++i;
		});

		$('#outcome_selection_list').html(cat_checkboxes);

		var $outcomeRow = $(this).closest('.outcome_row');
		var outcomeID = $outcomeRow.attr('data-outcome-id');

		var outcomeName = false;
		for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
			if (assessmentData.Outcomes[i].id == outcomeID) outcomeName = assessmentData.Outcomes[i].name;
		}

		$('#cat_type').text(outcomeName);
		$('#cat_id').val(outcomeID);
		 
	 	if ($('#outcomes_item .part_wrapper').length > 1) {
		 	$('#parts_select').show();
			$('#manageOutcome_dialog').dialog('open');		
	 	} else {
			$outcomeRow.find('.wait_icon_actions').css('opacity', 1);
			$('#cat_include_part_0').prop('checked', true);
			var contextedSave = $.proxy(updateOutcome, $('#manageOutcome_dialog'));
			contextedSave();
	 	}
    });

    $('#itemOutcomes').on('click', '.actions_button.edit_icon', function(e) {
		e.preventDefault();
		if (!canEdit) return;
	 	
		var $itemWrapper = $('#outcomes_item .question_wrapper');

		// Get item data

		var contentID = $('#outcomes_item .question_wrapper').attr('data-content-id');

		var itemType = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var itemData = assessmentData.Sections[i].Content[j];
				if (itemData.id == contentID) itemType = itemData.type;
			}
		}

		var questionHandleID = $('#outcomes_item .question_wrapper').attr('data-question-handle-id');
		var thisData = questionData[questionHandleID].json_data;

		// Get outcome parts

		var $outcomeRow = $(this).closest('.outcome_row');
		var outcomeID = $outcomeRow.attr('data-outcome-id');

		var outcomeData = false;
		for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
			if (assessmentData.Outcomes[i].id == outcomeID) outcomeData = assessmentData.Outcomes[i];
		}

		var outcomeParts = [];
		for (var i = 0; i < outcomeData.parts.length; ++i) {
			if (outcomeData.parts[i].content_id == contentID) {
				if ('part_id' in outcomeData.parts[i]) outcomeParts.push(outcomeData.parts[i].part_id);
				else if ('part_index' in outcomeData.parts[i]) outcomeParts.push(outcomeData.parts[i].part_index);
			}
		}

		// Buid checkboxes

	 	var cat_checkboxes = "";
	 	var i = 0;
	 	$('#outcomes_item .part_wrapper').each(function() {
			var partID = $(this).attr('data-part-id');

			var thisNumber = $(this).find('.question_number').html();
			if (assessmentData.Settings.numberingType == 'Alpha') {
				var $contextWrapper = $(this).prevAll('.context_wrapper:first');
				if ($contextWrapper.length > 0) {
					thisNumber = $contextWrapper.find('.question_number').html().slice(0, -1) + thisNumber;
				}
			} else if (assessmentData.Settings.numberingType == 'Type') {
				var thisStyle = $(this).attr('data-type').substr(0, 2);
				thisNumber = thisStyle.toUpperCase() + thisNumber;
			}

			var value;
			cat_checkboxes += "<div class='outcome_selection_entry'><label for='cat_include_part_" + i + "'>" + 
				thisNumber + "</label><input type='checkbox' class='outcome_checkbox' ";
			if (itemType == 'item') {

				cat_checkboxes += "data-part-id='" + partID + "'";
				value = (outcomeParts.indexOf(partID) < 0) ? '' : 'checked';

			} else if (itemType == 'item_set') {

				var partIndex = false;
				for (var j = 0; j < thisData.Parts.length; ++j) {
					if (thisData.Parts[j].id == partID) partIndex = thisData.Parts[j].index;
				}
				cat_checkboxes += "data-part-index='" + partIndex + "'";

				value = (outcomeParts.indexOf(partIndex) < 0) ? '' : 'checked';

			}
			cat_checkboxes += " id='cat_include_part_" + i + "' " + value + "></div>";

			++i;
		});

		$('#outcome_selection_list').html(cat_checkboxes);

		$('#cat_type').text(outcomeData.name);
		$('#cat_id').val(outcomeID);
		 
		if ($itemWrapper.length == 0) $('#parts_select').hide();
		else $('#parts_select').show();

		$('#manageOutcome_dialog').dialog('open');
    });

	function rebuildOutcomesList() {
		var outcomes_sort = [];
		for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
			outcomes_sort.push({
				id: assessmentData.Outcomes[i].id,
				name: assessmentData.Outcomes[i].name.toLowerCase()
			});
		}
		outcomes_sort.sort(function (a, b) {
			return a.name < b.name ? -1 : (a.name > b.name ? 1 : 0);
		});

		var outcome_ids = [];
		for (var i = 0; i < outcomes_sort.length; ++i) {
			outcome_ids.push(outcomes_sort[i].id);
		}

		var newBody = '';
		if (outcome_ids.length == 0) {
			newBody += 
		 		'<tr class="outcome_blank">\
		 			<td colspan="3" style="font-style:italic;">No learning outcomes defined. Click "Edit outcomes" to add outcomes.</td>\
		 		</tr>';
		} else {
			var includedRows = [];
			var newRows = [];

			var numParts = $('#outcomes_item .part_wrapper').length;

			for (var i = 0; i < outcome_ids.length; i++) {
				var outcomeData = false;
				for (var j = 0; j < assessmentData.Outcomes.length; ++j) {
					if (assessmentData.Outcomes[j].id == outcome_ids[i]) outcomeData = assessmentData.Outcomes[j];
				}

				var urlVars = getUrlVars();
				var contentID = ('c' in urlVars) ? urlVars.c : false;
		
	 			var isNew = true;
				for (var j = 0; j < outcomeData.parts.length; ++j) {
					if (outcomeData.parts[j].content_id === contentID) isNew = false;
				}

		 		var thisRow = '<tr class="outcome_row" data-outcome-id="' + outcome_ids[i] + '">\
		 			<td class="outcome_name">' + escapeHTML(outcomeData.name) + '</td>\
		 			<td class="outcome_count" style="text-align:center">' + outcomeData.parts.length + '</td>\
		 			<td>';
		 		if (canEdit && (contentID !== false)) {
					if (isNew) {
						thisRow += '<div class="wait_icon_actions"></div>';
						thisRow += '<button title="Add" class="actions_button add_icon">&nbsp;</button>';
					} else {
						if (numParts > 1) {
							thisRow += '<button title="Edit" class="actions_button edit_icon">&nbsp;</button>';
						}
						thisRow += '<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
					}
				}
				thisRow += '<button title="Show" class="actions_button view_icon"' + 
				 	((outcomeData.parts.length > 0) ? '' : ' disabled="disabled"') + '>&nbsp;</button>';
				
				thisRow += '</td></tr>';

				if (isNew) newRows.push(thisRow);
				else includedRows.push(thisRow);
			}

			if ((includedRows.length > 0) && (newRows.length > 0)) {
				newBody = 
					'<tr><td colspan="3" style="font-weight:bold; font-size:15px;">Included in this question</td></tr>' + includedRows.join('') +
					'<tr><td colspan="3" style="font-weight:bold; font-size:15px;">Other outcomes</td></tr>' + newRows.join('');
			} else if (includedRows.length > 0) {
				newBody = 
					'<tr><td colspan="3" style="font-weight:bold; font-size:15px;">Included in this question</td></tr>' + includedRows.join('');
			} else if (newRows.length > 0) {
				newBody = newRows.join('');
			}
		}
		$('#itemOutcomes').html(newBody);
	}
	
	function updateHighlighting() {
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				var itemData = assessmentData.Sections[i].Content[j];
				if (itemData.type == 'item') {
					var questionHandleID = itemData.question_handle_id;
					var questionID = questionData[questionHandleID].id;
					var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-question-handle-id="' + questionHandleID + '"]');
					if (questionID in StatsCommon.fullStatistics) {
						StatsCommon.highlightQuestion($itemWrapper, StatsCommon.fullStatistics[questionID], questionData[questionHandleID].json_data);
					}
				}
			}
		}

		if ($('#assessment_preview_wrapper .item_highlighted').length > 0) {
			$('#highlighted_notice').show();
		} else $('#highlighted_notice').hide();
	}
				
	$('#stats_print').on('mousedown', function(e) {
		e.preventDefault();
	
		drawPreview()
		.then(function() {
			var questionHandleIDs = StatsCommon.getQuestionHandleIDs(assessmentData);
			var statsHTML = StatsCommon.buildStatsHTML(questionHandleIDs, questionData);

			navigateWithPost('/build_pdf', {
				pdfType: 'pdf_from_url',
				url: '/Assessments/build_preview/' + assessmentID,
				postDataJSON: JSON.stringify({ html: statsHTML }),
				target: 'statistics.pdf'
			});
	
		});
	});
	
	function checkTemplate() {
		var emptySectionName = null;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;

			var hasItems = false;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				itemData = assessmentData.Sections[i].Content[j];
				if (itemData.type == 'item') hasItems = true;
				else if (itemData.type == 'item_set') hasItems = true;
			}
			if (!hasItems) {
				emptySectionName = assessmentData.Sections[i].header;
			}
		}

		if (assessmentData.Sections.length == 0) {

			$('#build_error_text').html('Assessment template does not contain any questions.');
			$('#buildError_dialog').dialog('open');
			return false;

		} else if (emptySectionName != null) {

			$('#build_error_text').html('The "' + emptySectionName + '" section does not contain any questions.');
			$('#buildError_dialog').dialog('open');
			return false;

		} else {

			var questionIndices = {};

			var noCorrect = [];
			var tooManyDigits = [];
			var typeMismatch = [];
			var needsNegative = [];
			var needsFraction = [];
			var duplicateAnswer = [];
			var questionNumbers = [];
			var invalidAnswers = [];
			var scoringMismatch = [];

			var hasAllQuestions = true;
			for (var i = 0; i < assessmentData.Sections.length; ++i) {
				if (assessmentData.Sections[i].type == 'page_break') continue;

				var sectionType = assessmentData.Sections[i].type;
				for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
					var itemData = assessmentData.Sections[i].Content[j];
					if (itemData.type == 'shuffle_break') continue;

					var consistentScoring = true;
					if (itemData.type == 'item_set') {
						var scoringHash = false;
						for (var k = 0; k < itemData.question_handle_ids.length; ++k) {
							var questionHandleID = itemData.question_handle_ids[k];
							if (questionHandleID in questionData) {
								var thisQuestionData = questionData[questionHandleID];
								if ('scoring_hash' in thisQuestionData) {
									if (scoringHash == false) scoringHash = thisQuestionData.scoring_hash;
									else if (thisQuestionData.scoring_hash != scoringHash) consistentScoring = false;
								} else consistentScoring = false;
							}
						}
					}

					var contentID = itemData.id;
					var $questionWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"');
					var questionHandleID = $questionWrapper.attr('data-question-handle-id');

					if (questionHandleID in questionData) {
						var thisQuestionData = questionData[questionHandleID];

						for (var k = 0; k < thisQuestionData.json_data.Parts.length; ++k) {
							var thisPartData = thisQuestionData.json_data.Parts[k];
							var thisType = thisPartData.type;

							if (thisType != 'context') {
								var partID = thisPartData.id;
								var $partWrapper = $questionWrapper.find('.part_wrapper[data-part-id="' + partID + '"]');

								var questionIndex = questionNumbers.length;
								var partKey = contentID.toString() + '.' + partID;
								questionIndices[partKey] = questionIndex;

								if (!consistentScoring) {
									scoringMismatch.push(questionIndex);
								}

								var thisNumber = $partWrapper.find('.question_number').html().slice(0, -1);
								if (assessmentData.Settings.numberingType == 'Alpha') {
									var $contextWrapper = $partWrapper.prevAll('.context_wrapper:first');
									if ($contextWrapper.length > 0) {
										thisNumber = $contextWrapper.find('.question_number').html().slice(0, -1) + thisNumber;
									}
								} else if (assessmentData.Settings.numberingType == 'Type') {
									thisNumber = thisType.substr(0, 2).toUpperCase() + thisNumber;
								}

								questionNumbers.push(thisNumber);

								if ((sectionType != 'mixed') && (thisType.substr(0, 2) != sectionType)) {
									typeMismatch.push(questionIndex);
								}

								var answers = [];
								var hasDuplicate = false;
								$partWrapper.find('.question_answer').each(function() {
									var $filteredAnswer = $('<div></div>');
									$(this).clone().appendTo($filteredAnswer);
									$filteredAnswer.find('.answer_label').remove();
									$filteredAnswer.find('*').each(function() {
										$(this).removeClass();
										var attributeNames = [];
										for (var p = 0; p < $(this)[0].attributes.length; ++p) {
											var thisName = $(this)[0].attributes[p].name;
											if (thisName != 'src') attributeNames.push(thisName);   // Keep images unique
										}
										for (var p = 0; p < attributeNames.length; ++p) {
											$(this)[0].removeAttribute(attributeNames[p]);
										}
									});
									var answerString = $filteredAnswer.html();
									if ($.inArray(answerString, answers) >= 0) hasDuplicate = true;
									answers.push(answerString);
								});
								if (hasDuplicate && (thisType.substr(0, 2) == 'mc')) {
                                    duplicateAnswer.push(questionIndex);
                                }
									
								if (thisType == 'nr_fraction') {
									needsFraction.push(questionIndex);
								}

								if (thisType.substr(0, 2) == 'nr') {
									var numBlanks = thisPartData.TypeDetails.nrDigits;
									var goodDigits = true;
									var negativeAnswer = false;
									var validAnswers = true;
									for (var p = 0; p < thisPartData.ScoredResponses.length; ++p) {
										var answerID = thisPartData.ScoredResponses[p].id;
										var $thisAnswer = $partWrapper.find('.question_answer[data-answer-id="' + answerID + '"]');
										if ($thisAnswer.length == 1) {
											var answerString = $thisAnswer.text();
											if (answerString[0] == '-') negativeAnswer = true;
											if (thisType == 'nr_scientific') {
												answerString = answerString.replace(/\D/g, '');
											}
											if (answerString.length > numBlanks) goodDigits = false;
											
											var testExpression;
											if (thisType == 'nr_decimal') {
												testExpression = /^-?([1-9]\d*|0)(\.\d+)?$/;
											} else if (thisType == 'nr_scientific') {
												testExpression = /^([1-9](\.\d+)?[Ee][+\-]?\d+|-?([1-9]\d*|0)(\.\d+)?)$/;
											} else if (thisType == 'nr_fraction') {
												testExpression = /^-?([1-9]\d*)(\/\d+)?$/;
											} else if (thisType == 'nr_selection') {
												testExpression = /^[\d,\?]*$/;
											}
											if (!testExpression.test(answerString)) validAnswers = false;											
										}
									}
									if (!goodDigits) tooManyDigits.push(questionIndex);
									if (negativeAnswer) needsNegative.push(questionIndex);
									if (!validAnswers) invalidAnswers.push(questionIndex);
								}
								
								if (thisType != 'wr') {
									var hasCorrect = false;
									for (var keyIndex = 0; keyIndex < thisPartData.ScoredResponses.length; ++keyIndex) {
										if (thisPartData.ScoredResponses[keyIndex].value > 0) hasCorrect = true;
									}
									if (!hasCorrect) noCorrect.push(questionIndex);
								}
							}
						}
					} else hasAllQuestions = false;
				}
			}
			
			var warnings = [];
			if (tooManyDigits.length > 0) {
				warnings.push({
					message: 'Too many digits in answer',
					questions: tooManyDigits
				})
			}
			if (duplicateAnswer.length > 0) {
				warnings.push({
					message: 'Duplicate answer',
					questions: duplicateAnswer
				})
			}

			var errors = [];
			if (!hasAllQuestions) {
				errors.push({
					message: 'Unable to find question data'
				})
			}
			if (invalidAnswers.length > 0) {
				errors.push({
					message: 'Invalid answer specified',
					questions: invalidAnswers
				})
			}
			if (noCorrect.length > 0) {
				errors.push({
					message: 'No correct answer specified',
					questions: noCorrect
				})
			}
			if (scoringMismatch.length > 0) {
				errors.push({
					message: 'Inconsistent scoring in question set',
					questions: scoringMismatch
				})
            }
            if (typeMismatch.length > 0) {
                errors.push({
                    message: 'Question type does not match section type',
                    questions: typeMismatch
                })
            }

			// Check for questions with no weight assigned
			
			if (assessmentData.Scoring.type != 'Total') {
				var weights = ('weights' in assessmentData.Scoring) ? assessmentData.Scoring.weights : [];
				var zeroWeight = [];

				if (assessmentData.Scoring.type == 'Sections') {
					for (var i = 0; i < assessmentData.Sections.length; ++i) {
						if (assessmentData.Sections[i].type == 'page_break') continue;

						var sectionID = assessmentData.Sections[i].id;
						var section_weight = 0.0;
						for (var j = 0; j < weights.length; ++j) {
							if (weights[j].section_id == sectionID) section_weight = weights[j].weight;
						}
						if (section_weight == 0.0) {
							for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
								if (assessmentData.Sections[i].Content[j].type == 'shuffle_break') continue;

								var contentID = assessmentData.Sections[i].Content[j].id;
								var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"]');
								$itemWrapper.find('.part_wrapper').each(function() {
									var partKey = contentID + '.' + $(this).attr('data-part-id');
									zeroWeight.push(questionIndices[partKey]);										
								});

							}
						}
					}
				} else if (assessmentData.Scoring.type == 'Outcomes') {
					for (var i = 0; i < questionNumbers.length; ++i) zeroWeight.push(i);
					for (var j = 0; j < assessmentData.Outcomes.length; ++j) {
						var outcomeID = assessmentData.Outcomes[j].id;
						var outcome_weight = 0.0;
						for (var i = 0; i < weights.length; ++i) {
							if (weights[i].outcome_id == outcomeID) outcome_weight = weights[i].weight;
						}
						if (outcome_weight > 0.0) {
							for (var i = 0; i < assessmentData.Outcomes[j].parts.length; ++i) {
								var contentID = assessmentData.Outcomes[j].parts[i].content_id;

								var partID = false;
								if ('part_id' in assessmentData.Outcomes[j].parts[i]) {
									partID = assessmentData.Outcomes[j].parts[i].part_id;
								} else if ('part_index' in assessmentData.Outcomes[j].parts[i]) {
									var $itemWrapper = $('#assessment_preview_wrapper .question_wrapper[data-content-id="' + contentID + '"]');
									var questionHandleID = $itemWrapper.attr('data-question-handle-id');
									if (questionHandleID in questionData) {
										var thisQuestionData = questionData[questionHandleID].json_data;
										var partIndex = assessmentData.Outcomes[j].parts[i].part_index;
										for (var k = 0; k < thisQuestionData.Parts.length; ++k) {
											if (thisQuestionData.Parts[k].index == partIndex) {
												partID = thisQuestionData.Parts[k].id;
											}
										}
									}
								}

								if (partID !== false) {
									var partKey = contentID + '.' + partID;
									var questionIndex = questionIndices[partKey];
									
									var zerosIndex = zeroWeight.indexOf(questionIndex);
									if (zerosIndex >= 0) zeroWeight.splice(zerosIndex, 1);	
								}
							}
						}
					}
				}

				if (zeroWeight.length > 0) {
					errors.push({
						message: 'No weight given',
						questions: zeroWeight
					});
				}
			}

			// Display errors and warnings
	
			if (errors.length > 0) {

				var errorHTML = '<ul>';
				for (var j = 0; j < errors.length; ++j) {
					errorHTML += '<li>' + errors[j].message;
					if ('questions' in errors[j]) {
						errorHTML += ' for ';
						if (errors[j].questions.length == 1) errorHTML += 'question ';
						else errorHTML += 'questions ';
						for (var i = 0; i < errors[j].questions.length; ++i) {
							if ((i > 0) && (i == errors[j].questions.length - 1)) errorHTML += " and ";
							else if (i > 0) errorHTML += ", ";
							errorHTML += questionNumbers[errors[j].questions[i]];
						}
					}
					errorHTML += '.</li>';
				}
				errorHTML += '</ul>';
				$('#build_error_text').html(errorHTML);

				$('#buildError_dialog').dialog('open');
				return false;

			} else {

				if (needsFraction.length > 0) {
					var needsSymbols = needsFraction;
					for (var i = 0; i < needsNegative.length; ++i) {
						if (needsSymbols.indexOf(needsNegative[i]) > -1) needsSymbols.push(needsNegative[i]);
					}
					needsSymbols.sort(function (a, b) { return a - b; });
					
					var infoHTML = 'Note: '
					if (needsSymbols.length == 1) infoHTML += 'Question ';
					else infoHTML += 'Questions ';
					for (var i = 0; i < needsSymbols.length; ++i) {
						if ((i > 0) && (i == needsSymbols.length - 1)) infoHTML += " and ";
						else if (i > 0) infoHTML += ", ";
						infoHTML += questionNumbers[needsSymbols[i]];
					}
					if (needsSymbols.length == 1) infoHTML += ' requires ';
					else infoHTML += ' require ';
					infoHTML += 'fractions or negative answers. Generated NR sections will include the appropriate symbols.';
					$('.build_note_text').html(infoHTML);
					$('.build_note').show();
				} else if (needsNegative.length > 0) {
					var infoHTML = 'Note: '
					if (needsNegative.length == 1) infoHTML += 'Question ';
					else infoHTML += 'Questions ';
					for (var i = 0; i < needsNegative.length; ++i) {
						if ((i > 0) && (i == needsNegative.length - 1)) infoHTML += " and ";
						else if (i > 0) infoHTML += ", ";
						infoHTML += questionNumbers[needsNegative[i]];
					}
					if (needsNegative.length == 1) infoHTML += ' includes ';
					else infoHTML += ' include ';
					infoHTML += 'negative answers. Generated NR sections will include negative symbol.';
					$('.build_note_text').html(infoHTML);
					$('.build_note').show();
				}
				else $('.build_note').hide();

				if (warnings.length > 0) {
					var warningHTML = '<ul>';
					for (var j = 0; j < warnings.length; ++j) {
						warningHTML += '<li>' + warnings[j].message;
						if ('questions' in warnings[j]) {
							warningHTML += ' for ';
							if (warnings[j].questions.length == 1) warningHTML += 'question ';
							else warningHTML += 'questions ';
							for (var i = 0; i < warnings[j].questions.length; ++i) {
								if ((i > 0) && (i == warnings[j].questions.length - 1)) warningHTML += " and ";
								else if (i > 0) warningHTML += ", ";
								warningHTML += questionNumbers[warnings[j].questions[i]];
							}
						}
						warningHTML += '.</li>';
					}
					warningHTML += '</ul>';
					$('.build_warning_text').html(warningHTML);
					$('.build_warning').show();
				}
				else $('.build_warning').hide();

				return true;
			}
		}
	}

	$('#buildError_dialog').dialog({
		autoOpen: false,
		width:425,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
		   	'Close': function() {
               	$(this).dialog("close");
			}
		}, open: function(event) {
		   	$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('btn btn-save btn_font');
		}, close: function(){
            $('form').validationEngine('hideAll');
		}
    });

    $('#build_set').click(function(e){
		e.preventDefault();

		if (checkTemplate()) {
			$('#set_title_text').val(assessmentData.Title.text.join("\n")).focus();

            ClassListsCommon.initializeClassLists({
                needsEmail: false
            });
            $('#buildSet_dialog').dialog('open');        
            document.activeElement.blur();
        }
	});

    var generateVersion = async function() {
        AssessmentsRender.shuffleAssessmentQuestions(assessmentData);
        AssessmentsRender.shuffleAssessmentParts(assessmentData, questionData);
        await drawAssessmentPreview();
        AssessmentsRender.shuffleAssessmentAnswers(assessmentData, questionData, $('#assessment_preview_wrapper'));
        await RenderTools.renderStyles[assessmentData.Settings.numberingType].renumber($('#assessment_preview_wrapper'));
    
        if (assessmentData.Settings.mcFormat == 'Auto') {
            $('#assessment_preview_wrapper .part_wrapper[data-type="mc"]').each(function() { 
                RenderTools.setMCWidths($(this)); 
            });
        } else $('#assessment_preview_wrapper .question_answer').css('width', '100%');        
    }

    var buildSet = async function($dialog) {
        var index = $('#class_list_select').val();
        var classListID = classLists[index].id;

        var oldAssessmentData = JSON.stringify(assessmentData);
        var oldQuestionData = JSON.stringify(questionData);

        var titleHTML = '<p>' + escapeHTML($('#set_title_text').val()).replace(/\n/g, '</p><p>') + '</p>';

        var json = Folders.getSelectedNode("versionFolderParent");
        var targetFolder = json.data.folder_id;

        var $checked = $('#class_list_wrapper .class_list_checkbox:checked');
        for (var i = 0; i < $checked.length; ++i) {
            var $row = $checked.eq(i).closest('.list_row');
            var studentData = JSON.parse($row.attr('data-json-data'));

            await generateVersion();
            var versionData = await getVersionData(titleHTML);

            /** CONTINUE HERE **/
        }

        assessmentData = JSON.parse(oldAssessmentData);
        questionData = JSON.parse(oldQuestionData);
        drawView();

        $('#buildSet_dialog').dialog('close');
    }

    $('#buildSet_dialog').dialog({
        autoOpen: false,
        width: 600,
        modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {

            listStep = 0;
            $('#class_list_continue').html('Next').removeClass('btn-save');

            $('.step_wrapper').css('transition', 'none');
            $('.step_wrapper').css('transform', 'translateX(0px)');
            setTimeout(function() {
                $('.step_wrapper').css('transition', 'transform 0.5s ease');
            }, 0);

            $('#list_text').focus();

            initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
        }
    });

    $('#set_cancel').on('click', function() {
        $('#buildSet_dialog').dialog('close');
    });

    $('#class_list_continue').on('click', function() {
        if ($(this).hasClass('disabled')) return;

        var $dialog = $('#createList_dialog');

        var json = Folders.getSelectedNode("setFolderParent");

        if (listStep == 1) {

            buildSet($dialog);
    
        } else if (!Folders.isItemFolder(formTreeData, json.data.raw_id)) {

            $('#setFolderParent').validationEngine('showPrompt', "* Cannot place items in this folder", 'error', 'bottomLeft', true);

        } else {

            // Move to next panel

            ++listStep;

            if (listStep == 1) {
                $('#class_list_continue').html('Build').addClass('btn-save');
            }
    
            var translation = -listStep * 600;
            $('.step_wrapper').css('transform', 'translateX(' + translation + 'px)');

            if (listStep == 1) {
                setTimeout(function() {
                    $dialog.find('.list_name').focus();
                }, 700);
            }

        }
    });

	function buildSitting() {
		var $dialog = $(this)

		var json = Folders.getSelectedNode("sittingFolderParent");
		if (!Folders.isItemFolder(sittingTreeData, json.data.raw_id)) {

			$('#sittingFolderParent').validationEngine('showPrompt', "* Cannot place items in this folder", 'error', 'bottomLeft', true);

		} else {

			var targetFolder = json.data.folder_id;
	
			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;
	
			ajaxFetch('/Sittings/create/' + assessmentID, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					title: $('#sitting_title_text').val(),
					target_folder: targetFolder
				})
			}).then(async response => {

				pendingAJAXPost = false;
				const responseData = await response.json();
				
				if (responseData.success) {
					window.open('/Sittings/view/' + responseData.sitting_id);
				} else {
					var message = "Unable to create sitting.";
					if ('message' in responseData) message = responseData.message;
	
					$.gritter.add({
						title: "Error",
						text: message,
						image: "/img/error.svg"
					});
				}
				
				$dialog.dialog('close');
			}).catch(function() {
				showSubmitError($dialog);
			});		
			
		}
	}
	
	$('#buildSitting_dialog').dialog({
		autoOpen: false,
		width: 650,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Build': buildSitting
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Build")').addClass('btn btn-save btn_font');
			
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
    });
     
	$('#build_online').on('click', function() {
		if (checkTemplate()) {
			$('#sitting_title_text').val(assessmentData.Title.text.join("\n")).focus();

			$('#buildSitting_dialog').dialog('open');
		}
	});

    async function getVersionData(titleHTML) {

        /**************************************************************************************************/
        /* Build HTML for this version.                                                                   */
        /**************************************************************************************************/

        // Unrender equation elements and remove highlighting
    
        RenderTools.unrenderEquations($('#assessment_preview_wrapper'));
        $('#assessment_preview_wrapper .item_highlighted').each(function() {
            $(this).bubbletip('destroy');
        });
    
        // Build version title
        
        var versionHTML = '<div class="assessment_title">';
        if ('logo' in assessmentData.Title) {
            versionHTML += '<img src="' + userdataPrefix + assessmentData.Title.logo.imageHash + 
                '" style="float:left; height:' + assessmentData.Title.logo.height + 'in; margin-right:0.25in;">'
        }
        versionHTML += '<div style="float:left;">' + titleHTML + '</div>';

        if ('nameLines' in assessmentData.Title) {
            versionHTML += '<div style="float:right;">';
            for (var i = 0; i < assessmentData.Title.nameLines.length; ++i) {
                versionHTML += 
                    '<div style="float:right;">' +
                        '<div style="display:inline-block; margin-right:0.1in;">' + assessmentData.Title.nameLines[i] + ':</div>' +
                        '<div style="display:inline-block; width:3in; border-bottom:1px solid black;"></div>' +
                    '</div>' +
                    '<div style="clear:both;"></div>';
            }
            versionHTML += '</div>';
        }
        versionHTML += '<div style="clear:both;"></div></div>';
        
        // Add preamble if it exists
        
        if ($('#assessment_preview_wrapper .preamble_wrapper').is(':visible')) {
            versionHTML += $('#assessment_preview_wrapper .preamble_wrapper')[0].outerHTML;
        }
        
        // Build the rest of the version from the preview
        
        var needsPageBreak = false;
        $('#assessment_preview_wrapper').find('.preview_section_wrapper').each(function() {
            if ($(this).hasClass('page_break')) {

                needsPageBreak = true;

            } else {

                // Add the header to the HTML if required
    
                var showHeader = false;
                for (var i = 0; i < assessmentData.Sections.length; ++i) {
                    if (assessmentData.Sections[i].id == $(this).attr('data-section-id')) {
                        if (assessmentData.Sections[i].showHeader) showHeader = true;
                    }
                }		
                if (showHeader) {
                    var $header = $(this).find('.preview_header');
                    versionHTML += $header[0].outerHTML;
                }
        
                $(this).find('.question_wrapper').each(function() {

                    // Add this item to the HTML
                    
                    if (!$(this).hasClass('shuffle_break')) {
                        var $questionCopy = $(this).clone();

                        $questionCopy.find('.assessment_note').remove();

                        if (assessmentData.Settings.wrResponseLocation == 'question') {
                            $questionCopy.find('.wr_response_space').css('display', 'block');
                        }

                        $questionCopy.find('.part_wrapper .question_answer_mc').css('width', '');						
                        $questionCopy.find('.part_wrapper .question_answer_mc').css('padding-right', '');
                        $questionCopy.find('.hidden_answer_list').remove();
                        
                        if (needsPageBreak) {
                            $questionCopy.children().eq(0).addClass('pagebreak');
                            needsPageBreak = false;
                        }
                        
                        versionHTML += $questionCopy.html();
                    }
                });
                
            }
        });

        // Get question entries

        var partEntries = VersioningCommon.getPartData($('#assessment_preview_wrapper'), assessmentData, questionData);

        // Re-render equations and bring back highlighting

        RenderTools.renderEquations($('#assessment_preview_wrapper'));
        updateHighlighting();

        /**************************************************************************************************/
        /* Build layout sections for this version.                                                        */
        /**************************************************************************************************/

        // Build version layout

        var scoringSections = [];
        var layoutSections = [];

        var hasMixed = false;
        for (var i = 0; i < assessmentData.Sections.length; ++i) {
            if (assessmentData.Sections[i].type == 'mixed') hasMixed = true;
        }

        if (!hasMixed) {

            // Build a layout that follows the template's sections

            for (var i = 0; i < assessmentData.Sections.length; ++i) {
                if (assessmentData.Sections[i].type == 'page_break') continue;

                var sectionData = assessmentData.Sections[i];

                // Get scoring data for this section

                var scoringEntry = {
                    id: sectionData.id,
                    Content: []
                };

                for (var j = 0; j < sectionData.Content.length; ++j) {
                    if (sectionData.Content[j].type == 'shuffle_break') continue;
                    var itemData = sectionData.Content[j];

                    var questionHandleID;
                    if (itemData.type == 'item') {
                        questionHandleID = itemData.question_handle_id;
                    } else if (itemData.type == 'item_set') {
                        var renderedIndex = ('rendered_index' in itemData) ? itemData.rendered_index : 0;
                        questionHandleID = itemData.question_handle_ids[renderedIndex];
                    }

                    for (var k = 0; k < questionData[questionHandleID].json_data.Parts.length; ++k) {
                        var partID = questionData[questionHandleID].json_data.Parts[k].id;
                        for (var p = 0; p < partEntries.length; ++p) {
                            if (partEntries[p].Source.question_handle_id != questionHandleID) continue;
                            else if (partEntries[p].Source.part_id != partID) continue;

                            partEntries[p].id = getNewID(4);
                            scoringEntry.Content.push(partEntries[p]);
                        }
                    }
                }

                scoringSections.push(scoringEntry);

                // Get layout data for this section

                var layoutEntry = {
                    id: sectionData.id,
                    type: sectionData.type,
                    header: sectionData.header
                };

                layoutSections.push(layoutEntry);
            }

        } else {

            // Build a layout that follows question styles

            var types = ['mc', 'nr', 'wr'];
            var styleSections = [];
            for (const type of types) {
                let header = await localizationInstance.getString(`defaults_${type}_header`);
                styleSections.push({ header: header, style: type });
            }

            for (var i = 0; i < styleSections.length; ++i) {
                var styleParts = [];
                for (var j = 0; j < partEntries.length; ++j) {
                    if (partEntries[j].type.substr(0, 2) == styleSections[i].style) {
                        partEntries[j].id = getNewID(4);
                        styleParts.push(partEntries[j]);
                    }
                }

                if (styleParts.length > 0) {
                    var sectionID = getNewID(4);

                    scoringSections.push({
                        id: sectionID,
                        Content: styleParts
                    });

                    layoutSections.push({
                        id: sectionID,
                        type: styleSections[i].style,
                        header: styleSections[i].header
                    });
                }
            }
        }
        
        // Add layout data for sections as needed

        for (var i = 0; i < layoutSections.length; ++i) {
            if (layoutSections[i].type == 'nr') {
                layoutSections[i].fieldStyle = 'Default';

                var scoringIndex;
                for (scoringIndex = 0; scoringIndex < scoringSections.length; ++scoringIndex) {
                    if (scoringSections[scoringIndex].id == layoutSections[i].id) break;
                }

                if (scoringIndex < scoringSections.length) {
                    for (var j = 0; j < scoringSections[scoringIndex].Content.length; ++j) {
                        var entry = scoringSections[scoringIndex].Content[j];
                        if (entry.type == 'nr_fraction') {
                            layoutSections[i].fieldStyle = 'Fractions';
                        } else if (layoutSections[i].fieldStyle != 'Fractions') {
                            var needsNegative = false;
                            for (var k = 0; k < entry.Answers.length; ++k) {
                                if (entry.Answers[k].response[0] == '-') needsNegative = true;
                            }
                            if (needsNegative) layoutSections[i].fieldStyle = 'Negatives';
                        }
                    }	
                }

            } else if (layoutSections[i].type == 'wr') {
                layoutSections[i].includeSpace = (assessmentData.Settings.wrResponseLocation == 'form');
            }

            if (layoutSections[i].type == 'wr') {
                if (layoutSections[i].includeSpace) layoutSections[i].columnCount = 1;
                else layoutSections[i].columnCount = 2;
            } else if (layoutSections[i].type != 'mixed') {
                layoutSections[i].columnCount = userDefaults.Document.Sections.columnCount;
            }
        }

        return {
            scoringSections: scoringSections,
            layoutSections: layoutSections,
            html: versionHTML
        };
    }

	function buildVersion() {
		var $dialog = $('#buildVersion_dialog');
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')) {
			
			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			/**************************************************************************************************/
			/* Build version.                                                                                 */
			/**************************************************************************************************/

            var titleHTML = '<p>' + escapeHTML($('#version_title_text').val()).replace(/\n/g, '</p><p>') + '</p>';
            getVersionData(titleHTML)
            .then((versionData) => {

                if ($('#version_build_form').val() == 1) {

                    var json = Folders.getSelectedNode("versionFolderParent");
                    if (!Folders.isItemFolder(formTreeData, json.data.raw_id)) {
    
                        $('#versionFolderParent').validationEngine('showPrompt', "* Cannot place items in this folder", 'error', 'bottomLeft', true);
            
                        hideSubmitProgress($dialog);
                        pendingAJAXPost = false;
    
                    } else {
    
                        var targetFolder = json.data.folder_id;
                        
						ajaxFetch('/Assessments/build_version/' + assessmentID, {
							method: 'POST',
							headers: { 'Content-Type': 'application/json' },
							body: JSON.stringify({
								title: $('#version_title_text').val(),
								scoring_sections: JSON.stringify(versionData.scoringSections),
								layout_sections: JSON.stringify(versionData.layoutSections),
								html: versionData.html,
								target_folder: targetFolder
							})
						}).then(async response => {
							pendingAJAXPost = false;
							const responseData = await response.json();
						
                            if (responseData.success) {
                                window.open('/Documents/build/' + responseData.documentID);
                            } else {
                                $.gritter.add({
                                    title: "Error",
                                    text: "Unable to create version",
                                    image: "/img/error.svg"
                                });
                            }
                            
                            $dialog.dialog('close');
                        }).catch(function() {
                            showSubmitError($dialog);
                        });
                        
                    }
    
                } else {

					pendingAJAXPost = false;

					navigateWithPost('/build_pdf', {
						pdfType: 'pdf_from_url',
						url: '/Assessments/build_preview/' + assessmentID,
						postDataJSON: JSON.stringify({ 
							html: versionData.html,
							header: 'ASSESSMENT PREVIEW. CLICK "BUILD" TO BUILD THIS VERSION.'
						}),
						target: 'preview.pdf'
					});

                }
                
                // Add highlighting back
                
                updateHighlighting();

            });
		}
	}

	$('#buildVersion_dialog').dialog({
		autoOpen: false,
		width: 650,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Build': buildVersion
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Build")').addClass('btn btn-save btn_font');
			
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
    });
    
	$('#build_version').on('click', function() {
		if (checkTemplate()) {
			$('#version_title_text').val(assessmentData.Title.text.join("\n")).focus();
			$('#version_build_form').val(1);

			$('#buildVersion_dialog').dialog('open');
		}
	});

	$('#preview_pdf').click(function(e) {
		e.preventDefault();

		$('#version_title_text').val(assessmentData.Title.text.join("\n"));
		$('#version_build_form').val(0);
		
		buildVersion();
	});

    var smartFill = function() {
		var $dialog = $(this);
		
		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

        // Get folder ID

        var json = Folders.getSelectedNode("fill_folder");
        var folderID = json.data.folder_id;

        // Get allowed question types

        var allowedTypes = [];
        $('#fill_include_wrapper .type_checkbox').each(function() {
            if ($(this).prop('checked')) allowedTypes.push($(this).attr('data-type'));
        });

        // Get new question counts

        var outcomeCounts = [];
        $('.fill_outcomes_row').each(function() {
            var $countInput = $(this).find('.fill_outcomes_count');
            var count = parseFloat($countInput.val().trim());
            var minCount = parseFloat($countInput.attr('data-min-value'));

            if (count > minCount) {
                outcomeCounts.push({
                    outcome_id: $(this).attr('data-outcome-id'),
                    add_count: count - minCount
                });
            }            
        });

        // Find and add new questions

        var postData = {
            folder_id: folderID,
            section_id: $('#fill_section_id').val(),
            allowed_types: allowedTypes,
            outcome_counts: outcomeCounts
        };

		ajaxFetch('/Assessments/smart_fill/' + assessmentID, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(postData)
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
            if (responseData.success) {
                
                var isValid = true;
                if ('data_string' in responseData) {
                    if (!('data_hash' in responseData)) isValid = false;
                    else if (responseData.data_hash != SparkMD5.hash(responseData.data_string)) isValid = false;	
                    if (isValid) assessmentData = JSON.parse(responseData.data_string);
                }

                if (isValid) {

                    // Update version

                    assessmentVersionID = responseData.version_id;

                    // Update question data for new items

                    if ('new_items' in responseData) {
                        for (var i = 0; i < responseData.new_items.length; ++i) {
                            var thisData = PreviewCommon.parseQuestionData(responseData.new_items[i]);
                            if (thisData !== false) {
                                var questionHandleID = thisData.question_handle_id;
                                questionData[questionHandleID] = thisData;

                                var questionID = responseData.new_items[i].id;
                                StatsCommon.fullStatistics[questionID] = responseData.new_items[i].Statistics;
                            }
                        }
                    }

                    // Redraw

                    $('#assessment_preview_wrapper').attr('data-needs-render', 1);
                    $('#sections_panel').attr('data-needs-render', 1);
                    drawView();	
    
                    $.gritter.add({
                        title: "Success",
                        text: "Questions added.",
                        image: "/img/success.svg"
                    });
                    $dialog.dialog('close');
    
                } else {

                    $.gritter.add({
                        title: "Error",
                        text: "Unable to add questions.",
                        image: "/img/error.svg"
                    });
                    $dialog.dialog('close');
    
                }

            } else {

                $.gritter.add({
                    title: "Error",
                    text: "Unable to add questions.",
                    image: "/img/error.svg"
                });
                $dialog.dialog('close');

            }
        }).catch(function() {
            showSubmitError($dialog);
        });
    }

    $('#smartFill_dialog').dialog({
        autoOpen: false,
		width: 700,
        height: 400,
		resizable: true,
		modal: true,
        minWidth: 600,
        minHeight: 300,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Add': smartFill
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Add")').addClass('btn btn-save btn_font disabled');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

    $(document).on('click', '.smart_fill', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

        // Set up allowed question types

        var sectionID = $(this).closest('.items_panel').attr('data-section-id');
        $('#fill_section_id').val(sectionID);

        var sectionType = false;
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
            if (assessmentData.Sections[i].id == sectionID) {
                sectionType = assessmentData.Sections[i].type;
            }
		}

        if (sectionType == 'mixed') {
            $('#fill_include_wrapper .type_checkbox').prop('checked', true);
            $('#fill_include_wrapper').find('input').removeAttr("disabled");
            $('#fill_include_wrapper').find('label').removeClass("disabled");
        } else {
            $('#fill_include_wrapper .type_checkbox').prop('checked', false);
            $('#include_' + sectionType).prop('checked', true);
            $('#fill_include_wrapper').find('input').attr("disabled", "disabled");
            $('#fill_include_wrapper').find('label').addClass("disabled");
        }
    
        // Fill in outcomes list

        var outcomes_sort = [];
		for (var i = 0; i < assessmentData.Outcomes.length; ++i) {
			outcomes_sort.push({
				id: assessmentData.Outcomes[i].id,
				name: assessmentData.Outcomes[i].name.toLowerCase()
			});
		}
		outcomes_sort.sort(function (a, b) {
			return a.name < b.name ? -1 : (a.name > b.name ? 1 : 0);
		});

		var outcome_ids = [];
		for (var i = 0; i < outcomes_sort.length; ++i) {
			outcome_ids.push(outcomes_sort[i].id);
		}

		var outcomesHTML = '';

        for (var i = 0; i < outcome_ids.length; i++) {
            var outcomeData = false;
            for (var j = 0; j < assessmentData.Outcomes.length; ++j) {
                if (assessmentData.Outcomes[j].id == outcome_ids[i]) outcomeData = assessmentData.Outcomes[j];
            }

            outcomesHTML += 
                '<div class="fill_outcomes_row" data-outcome-id="' + outcome_ids[i] + '"> \
                    <div class="fill_outcomes_name">' + outcomeData.name + '</div> \
                    <input type="text" class="fill_outcomes_count intSpinner defaultTextBox2" \
                        data-min-value="' + outcomeData.parts.length + '" /> \
                    </div>';
        }

        $('#fill_outcomes_wrapper').html(outcomesHTML);

        $('#fill_outcomes_wrapper .fill_outcomes_count').each(function() {
            $(this).spinner({
                step: 1,
                min: $(this).attr('data-min-value'),
                stop: updateSmartFillAdd
            });
            $(this).val($(this).attr('data-min-value'));
        })

        Folders.openToNode($("#fill_folder"), 'open');

        $('#smartFill_dialog').dialog('open');
    });

    var updateSmartFillAdd = function() {
        var canAdd = false;
        $('#fill_outcomes_wrapper .fill_outcomes_count').each(function() {
            var count = $(this).val().trim();
            var minCount = $(this).attr('data-min-value');
            if ((count.length > 0) && (parseFloat(count) > minCount)) canAdd = true;
        });

        var $buttonPane = $('#smartFill_dialog').siblings('.ui-dialog-buttonpane');
        var $addButton = $buttonPane.find('button:contains("Add")');

        if (canAdd) $addButton.removeClass('disabled');
        else $addButton.addClass('disabled');
    }

    $('#fill_outcomes_wrapper').on('keydown', '.fill_outcomes_count', function(e) {
        var $thisRow = $(this).closest('.fill_outcomes_row');

        if ((e.key === 'Tab') || (e.key === 'Enter')) {
            var $nextRow = $thisRow.next();
            if ($nextRow.length > 0) {
                e.preventDefault();
                $nextRow.find('.fill_outcomes_count').focus().select();
            }

        }
    });

    $('#fill_outcomes_wrapper').on('keyup', '.fill_outcomes_count', updateSmartFillAdd);

	$('#intro_note').dialog({
		autoOpen: false,
		width:600,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
			'Thanks!': function() {
				if ($('#intronote_hide').prop('checked')) {
					if (pendingAJAXPost) return false;
					pendingAJAXPost = true;
			
					ajaxFetch('/Users/hide_tip/assessments_build', {
						method: 'POST'
					}).then(async response => {
						pendingAJAXPost = false;
						const responseData = await response.json();
						if (responseData.success) showIntroNote = 0;
					}).catch(function() {
						showSubmitError();
					});
				}
				$(this).dialog('close');
				if (showSettings) $("#document_options").trigger("click");
			}
		}, open: function(event) {
		 	$('.ui-dialog-buttonpane').find('button:contains("Thanks!")').addClass('btn btn-primary btn_font');
		}, close: function() {
		}
    });
    
    $('#intro_note').parent().find(".ui-dialog-buttonpane").append(
    	'<div style="float:left; margin:10px 10px 0px;"> \
			<div class="labeled-input"> \
				<input type="checkbox" class="normal_chkbx" id="intronote_hide" /><label style="text-align:left;" for="intronote_hide">Don\'t show this message again</label> \
			</div> \
		</div>'
    );
    
    /* Set up notes */

    $('#note_edit_view').htmleditor();
	$('#note_edit_view').htmleditor('hideElement', 'edit_button_variable');
	$('#note_edit_view').htmleditor('hideElement', 'edit_button_blank');

    var noteSave = function() {
		var $dialog = $(this);
        
        var contentID = $('#note_item_id').val();
        var newHTML = $('#note_edit_view').htmleditor('getValue');

        if (contentID.length > 0) {

            for (var i = 0; i < assessmentData.Sections.length; ++i) {
                if (assessmentData.Sections[i].type == 'page_break') continue;
                for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
                    var thisEntry = assessmentData.Sections[i].Content[j];
                    if (thisEntry.id == contentID) {
                        if (newHTML.length == 0) delete thisEntry.notes;
                        else thisEntry.notes = newHTML;
                    }
                }
            }

        } else {

            if (newHTML.length == 0) delete assessmentData.Title.notes;
            else assessmentData.Title.notes = newHTML;

        }

        // Save assessment

        if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
        pendingAJAXPost = true;
        
        saveAssessmentData()
        .then(function() {

            // Finish up

            $dialog.dialog("close");

            $.gritter.add({
                title: "Success",
                text: "Note saved",
                image: "/img/success.svg"
            });
        })
        .catch(function(error) {
            showSubmitError($dialog);			
        });

    }

    $('#editNote_dialog').dialog({
        autoOpen: false,
		width: 550,
		height: 300,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': noteSave
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));

			$('#note_edit_view').htmleditor('initSelection');
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });

    $('#note_edit_view').on('savedom', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });

    $(document).on('click', '.notes_add', function(e) {
		e.preventDefault();
		if (!canEdit) return;

        if (this.hasAttribute('data-content-id')) {

            var contentID = $(this).attr('data-content-id');
            $('#note_item_id').val(contentID);
            $('#note_edit_view').htmleditor('setValue', '');
                
        } else {

            $('#note_item_id').val('');
            $('#note_edit_view').htmleditor('setValue', '');

        }

        $('#editNote_dialog').dialog('open');
    });

    $('#assessment_preview_wrapper').on('click', '.assessment_note .prev_icon', function(e) {
        e.stopPropagation();

        var $allNotes = $("#assessment_preview_wrapper .assessment_note");
        var $thisNote = $(this).closest('.assessment_note');
        var thisIndex = $allNotes.index($thisNote);
        
        thisIndex--;
 
        var itemTop = $allNotes.eq(thisIndex).offset().top;
        $("html, body").animate({
            scrollTop: itemTop - 10
        }, 1000);
    });

    $('#assessment_preview_wrapper').on('click', '.assessment_note .next_icon', function(e) {
        e.stopPropagation();

        var $allNotes = $("#assessment_preview_wrapper .assessment_note");
        var $thisNote = $(this).closest('.assessment_note');
        var thisIndex = $allNotes.index($thisNote);
        
        thisIndex++;
 
        var itemTop = $allNotes.eq(thisIndex).offset().top;
        $("html, body").animate({
            scrollTop: itemTop - 10
        }, 1000);
    });

    $('#assessment_preview_wrapper').on('click', '.assessment_note .edit_icon', function(e) {
        e.stopPropagation();

        if ($(this).closest('.assessment_note')[0].hasAttribute('data-content-id')) {

            var contentID = $(this).closest('.assessment_note').attr('data-content-id');
            $('#note_item_id').val(contentID);

            for (var i = 0; i < assessmentData.Sections.length; ++i) {
                if (assessmentData.Sections[i].type == 'page_break') continue;
                for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
                    var thisEntry = assessmentData.Sections[i].Content[j];
                    if (thisEntry.id == contentID) {
                        if ('notes' in thisEntry) {
                            $('#note_edit_view').htmleditor('setValue', thisEntry.notes);
                        } else $('#note_edit_view').htmleditor('setValue', '');
                    }
                }
            }

        } else {

            $('#note_item_id').val('');

            if ('notes' in assessmentData.Title) {
                $('#note_edit_view').htmleditor('setValue', assessmentData.Title.notes);
            } else $('#note_edit_view').htmleditor('setValue', '');

        }

        $('#editNote_dialog').dialog('open');
    });

    $('#assessment_preview_wrapper').on('click', '.assessment_note .delete_icon', function(e) {
        e.stopPropagation();

        if ($(this).closest('.assessment_note')[0].hasAttribute('data-content-id')) {

            var contentID = $(this).closest('.assessment_note').attr('data-content-id');
            for (var i = 0; i < assessmentData.Sections.length; ++i) {
                if (assessmentData.Sections[i].type == 'page_break') continue;
                for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
                    var thisEntry = assessmentData.Sections[i].Content[j];
                    if (thisEntry.id == contentID) delete thisEntry.notes;
                }
            }

        } else delete assessmentData.Title.notes;

        // Save assessment

        if (pendingAJAXPost) return false;
        pendingAJAXPost = true;
        
        saveAssessmentData()
        .then(function() {

            // Finish up

            $.gritter.add({
                title: "Success",
                text: "Note deleted",
                image: "/img/success.svg"
            });
        })
        .catch(function(error) {
            showSubmitError();			
        });
    });

    /* Set up busy indicators */
    
    $(document).ajaxStart(function() {
		$('body').css( 'cursor', 'wait' );
	});

	$(document).ajaxStop(function() {
		$('body').css( 'cursor', 'default' );
	});
    
    $('#dpre_preamble_edit').htmleditor();
	$('#dpre_preamble_edit').htmleditor('hideElement', 'edit_button_variable');
	$('#dpre_preamble_edit').htmleditor('hideElement', 'edit_button_blank');

	// Initialize statistics filters
	
	$('#stats_update').on('mousedown', function(e) {
		e.preventDefault();
		
		$('#statistics_graphs').html("<div style='font-style:italic'>Loading statistics&hellip;</div>");
		$('#item_statistics_data').html("<div style='font-style:italic'>Loading statistics&hellip;</div>");
		$('#statistics_n').html('0');

		var question_ids = [];
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;
			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				if (assessmentData.Sections[i].Content[j].type == 'item') {
					var questionHandleID = assessmentData.Sections[i].Content[j].question_handle_id;
					question_ids.push(questionData[questionHandleID].id);	
				} else if (assessmentData.Sections[i].Content[j].type == 'item_set') {
					for (var k = 0; k < assessmentData.Sections[i].Content[j].question_handle_ids.length; ++k) {
						var questionHandleID = assessmentData.Sections[i].Content[j].question_handle_ids[k];
						question_ids.push(questionData[questionHandleID].id);		
					}
				}
			}
		}

		showSubmitProgress($('#stats_button_wrapper'));
		$("#stats_print").attr('disabled', 'disabled');
	
		StatsCommon.loadFilteredStatistics(question_ids)
		.then(function() {
			var urlVars = getUrlVars();
			if ('c' in urlVars) {
				var questionHandleID = $('#statistics_item').attr('data-question-handle-id');
                var questionID = questionData[questionHandleID].id;
                StatsCommon.rebuildItemStatistics(questionID, questionData[questionHandleID].json_data, assessmentData.Settings.numberingType);
			}

			var questionHandleIDs = StatsCommon.getQuestionHandleIDs(assessmentData);
			StatsCommon.rebuildAssessmentStatistics(questionHandleIDs, questionData);

			hideSubmitProgress($('#stats_button_wrapper'));
			$("#stats_print").removeAttr('disabled');
		});
	});
	
	// Load assessment

	initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: "Unable to load assessment template.",
			image: "/img/error.svg"
		});
		window.location.href = '/Assessments';
	});
 });


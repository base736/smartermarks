/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { localizationInstance } from '/js/FormEngine/Localization.js?7.0';

import * as RenderTools from '/js/assessments/render_tools.js?7.0';

var assessmentsRenderInitializing = false;
var assessmentsRenderInitialized = false;

var countWords = null;

var renderItemToDiv = async function(data, $target) {
	var contextAnswers = [];
	if (('Common' in data) && ('Answers' in data.Common)) {
		contextAnswers = data.Common.Answers;
	}
	
	var numParts = 0;
	$.each(data.Parts, function($pkey, part) {
		if (part.type != 'context') numParts++;
	});

	var itemHTML = "";

	var hasContext = false;
	var contextTemplate = "";
	var firstPartIndex = 0;
	if ((data.Parts.length > 0) && (data.Parts[0].type == 'context')) {
		hasContext = true;
		contextTemplate = data.Parts[0].template;
		firstPartIndex = 1;
	} else if (contextAnswers.length > 0) {
		hasContext = true;
		contextTemplate = "";
	}

	if (hasContext) {
		itemHTML += '<div class="context_header">';

		if (numParts <= 1) {
			itemHTML += await localizationInstance.getString('questions_following_1');
		} else {
			if (countWords == null) {
				var countsList = await localizationInstance.getString('questions_numbers');
				if (countsList.indexOf('ERROR') == -1) countWords = countsList.split(',');
			}

			var countString;
			if (countWords != null) countString = (numParts < 10) ? countWords[numParts - 1] : numParts.toString();
			else countString = "[ERROR]";

            var questions_following_2 = await localizationInstance.getString('questions_following_2');
			itemHTML += questions_following_2.replace('#', countString);
		}
						
		itemHTML += 
			'</div>' + 
			'<div class="context_wrapper">' +
				'<div class="question_number">&nbsp;</div>' + 
				'<div class="context_text">' + contextTemplate;
			
		if (contextAnswers.length > 0) {
			var answerLabel = "A".charCodeAt(0);
			itemHTML += "<div class='context_answer_list'";
			if (data.Common.answerColumns == 'hide') itemHTML += " style='display:none;'";
			itemHTML += ">";

			$.each(contextAnswers, function($akey, $answer) {
				var $answerData = ' data-answer-id="'+$answer.id+'"';
				
				itemHTML += "<div class='question_answer context_answer'";
				if (data.Common.answerColumns !== 'hide') {
					var answerWidth = 100 / data.Common.answerColumns;
					itemHTML += " style='width:" + answerWidth + "%;'";
				}
				itemHTML += $answerData + ">" + 
						"<div class='answer_label'>" + String.fromCharCode(answerLabel++) + ".</div>" +
						"<div class='answer_text'>" + $answer.template + "</div>" +
					"</div>";
			});
			itemHTML += "</div>";
		}
				
		itemHTML += '</div></div>';
	}

	for (var i = firstPartIndex; i < data.Parts.length; ++i) {
		var part = data.Parts[i];
		var typeDetails = ('TypeDetails' in part) ? part.TypeDetails : null;

		if (part.type == 'context') {

			itemHTML += '<div class="context_header">';
	
			if (numParts <= 1) {
				itemHTML += await localizationInstance.getString('questions_following_3');
			} else {
				if (countWords == null) {
					var countsList = await localizationInstance.getString('questions_numbers');
					if (countsList.indexOf('ERROR') == -1) countWords = countsList.split(',');
				}

				var countString;
				if (countWords != null) countString = (numParts < 10) ? countWords[numParts - 1] : numParts.toString();
				else countString = "[ERROR]";

                var questions_following_4 = await localizationInstance.getString('questions_following_4');
				itemHTML += questions_following_4.replace('#', countString);
			}
							
			itemHTML += 
				'</div>' + 
				'<div class="context_wrapper">' +
					'<div class="question_number">&nbsp;</div>' + 
					'<div class="context_text">' + part.template + '</div>' +
				'</div>';
		} else {
			itemHTML += '<div class="part_wrapper' + (hasContext ? ' context_question' : '') + 
				'" data-part-id="' + part.id + '" data-type="' + part.type + '">';
	
			itemHTML += 
					'<div class="question_number">&nbsp;</div>' +
					'<div class="question_text">' + 
						'<div class="question_prompt">';
			if (('Common' in data) || (part.type == 'mc_truefalse')) {
					itemHTML += '<div class="matching_blank">&nbsp;</div><div style="display:inline-block; vertical-align:text-top;">';
			}
			itemHTML += part.template;
			
			if ((part.type == 'mc_table') && ('prompt' in typeDetails)) {
                var questions_table_prompt = await localizationInstance.getString('questions_table_prompt');
				itemHTML += "<div><br />" + questions_table_prompt + ":</div>";
			}
	
			if (('Common' in data) || (part.type == 'mc_truefalse')) {
				itemHTML += '</div>';
			}
			itemHTML += '</div>';
		
			if ('Common' in data) {

			} else if (part.type == 'mc') {

				var partAnswers = part.Answers;

				var answerLabel = "A".charCodeAt(0);
				itemHTML += "<div class='mc_answer_list'>";
				$.each(partAnswers, function($akey, $answer) {
					var $answerData = ' data-answer-id="'+$answer.id+'"';
					
					itemHTML += 
						"<div class='question_answer question_answer_mc' " + $answerData + ">" + 
							"<div class='answer_label'>" + String.fromCharCode(answerLabel++) + ".</div>" +
							"<div class='answer_text'>" + $answer.template + "</div>" +
						"</div>";
				});
				itemHTML += "</div>";

			} else if (part.type == 'mc_table') {

				var partAnswers = part.Answers;

				if ((typeDetails == null) || !('labels' in typeDetails)) {
					itemHTML += "<div><b>ERROR:</b> No labels defined for table-style multiple choice.</div>";
				} else {
					var needsItalics = (part.template.indexOf('<span class="block_bigBlank">') >= 0);
					
					itemHTML += "<br /><table class='mc_answer_table'>";
                    var questions_table_row = await localizationInstance.getString('questions_table_row');
					itemHTML += "<tr><th>" + questions_table_row + "</th>";
					$.each(typeDetails.labels, function($lkey, $label) {
						if (needsItalics) itemHTML += "<th><b><i>" + $label + "</i></b></th>";
						else itemHTML += "<th><b>" + $label + "</b></th>";
					});
					itemHTML += "</th>";
	
					var answerLabel = "A".charCodeAt(0);
					$.each(partAnswers, function($akey, $answer) {
						var $answerData = ' data-answer-id="'+$answer.id+'"';
	
						itemHTML += "<tr class='question_answer question_answer_table' " + $answerData + "><td class='answer_label'>" + String.fromCharCode(answerLabel++) + ".</td>";
						for (var j = 0; j < typeDetails.labels.length; ++j) {
							if (j < $answer.templates.length) {
                                itemHTML += "<td class='answer_value'>" + $answer.templates[j] + "</td>";
                            } else itemHTML += "<td class='answer_value'></td>";
						}
						itemHTML += "</tr>";
					});
					itemHTML += "</table>";
				}

			} else if (part.type.substr(0, 2) == 'nr') {

				if ('Answers' in part) {
					var partAnswers = part.Answers;

					itemHTML += "<div class='hidden_answer_list'>";
					$.each(partAnswers, function($akey, $answer) {
						if ('template' in $answer) {
							var $answerData = ' data-answer-id="' + $answer.id + '"';
							itemHTML += "<div class='question_answer' " + $answerData + ">" + 
								$answer.template + "</div>";	
						}
					});
					itemHTML += "</div>";	
				}

			} else if (part.type.substr(0, 2) == 'wr') {

                var rubricHTML = '';

                if (('Rubric' in part) && (part.Rubric.type == 'structured')) {

                    for (var j = 0; j < part.TypeDetails.criteria.length; ++j) {
                        rubricHTML += 
                            '<div class="rubric_row_wrapper">';
                        
                        if (part.TypeDetails.criteria.length > 1) {
                            rubricHTML += 
                                '<div class="rubric_row_title"> \
                                    <div>' + part.TypeDetails.criteria[j].label + '</div> \
                                </div>';
                        }
            
                        rubricHTML += '<div class="rubric_row_columns">';
            
                        if (part.TypeDetails.criteria.length > 1) {
                            rubricHTML += 
                                    '<div class="rubric_column_wrapper">';
                            rubricHTML += 
                                        '<div class="rubric_column_title">Focus: When marking <i>' + part.TypeDetails.criteria[j].label + '</i>, the marker should consider:</div> \
                                        <div class="template_view">' + part.Rubric.rows[j].focus + '</div>';
                            rubricHTML += 
                                    '</div>';
                        }
            
                        var sortedScoring = JSON.parse(JSON.stringify(part.Rubric.rows[j].scoring));
                        sortedScoring.sort(function(a, b) {
                            return (a.value < b.value ? 1 : -1);
                        })
                        
                        for (var k = 0; k < sortedScoring.length; ++k) {
            
                            var marks = sortedScoring[k].value;
                            var marksText = marks.toString() + ' marks';
                            if (marks == 0) marksText = 'No marks';
                            else if (marks == 1) marksText = '1 mark';
            
                            rubricHTML += 
                                    '<div class="rubric_column_wrapper">';
                            rubricHTML += 
                                        '<div class="rubric_column_title"><u>' + marksText + '</u></div> \
                                        <div class="template_view">' + sortedScoring[k].description + '</div>';
                            rubricHTML += 
                                    '</div>';
                        }
            
                        rubricHTML += 
                                '</div> \
                            </div>';
                    }
        
                } else if (('Rubric' in part) && ('template' in part.Rubric)) {
        
                    rubricHTML = part.Rubric.template;
        
                }

				if (rubricHTML != '') {
					itemHTML += "<div class='hidden_answer_list'>";
					itemHTML += "<div class='question_answer'>" + rubricHTML + "</div>";
					itemHTML += "</div>";
				}
				
				itemHTML += "<div class='wr_response_space' style='height:" + typeDetails.height + "in;'></div>";

			}
			
			itemHTML += '</div></div>';
			numParts--;
		}
			
		$target.append(itemHTML);
		itemHTML = "";
	}

	if (hasContext || (data.Parts.length > 1)) {
		$target.append("<div class='item_separator'></div>");
	}
	
	$target.find('.question_answer img').each(function() {
		$(this).attr('data-relative-width', $(this)[0].style.width);
	});

	// Run engine

	if ('Engine' in data) {
		if ('data' in data.Engine) {
			RenderTools.updateVariableNames($target, data.Engine.data);
		}

		var fullText = "";
		for (var importID in data.Engine.Imports) {
			if (data.Engine.Imports.hasOwnProperty(importID)) {
				fullText += data.Engine.Imports[importID].js;
			}
		}
		fullText += data.Engine.js;
	
		try {

			function evalInContext(js, context) {
				return function() { return eval(js); }.call(context);
			}

			evalInContext(fullText, $target);

		} catch(err) {

			$.gritter.add({
				title: "Javascript Error",
				text: err.message,
				image: "/img/error.svg"
			});

		}
	}

	// Set image widths

	if ('Common' in data) {
		RenderTools.setMCImageWidths($target.children('.context_wrapper'));
		RenderTools.setMatchingWidths($target);
	} else {
		$.each(data.Parts, function($pkey, part) {				
			if (part.type.substr(0, 2) == 'mc') {
				var $domQuestion = $target.find(".part_wrapper[data-part-id='" + part.id + "']");
				RenderTools.setMCImageWidths($domQuestion);
			}
		});
	}

	// Build response prompts for NR questions
	
	$.each(data.Parts, async function($pkey, part) {

		if (part.type.substr(0, 2) == 'nr') {
			var $domQuestion = $target.find(".part_wrapper[data-part-id='" + part.id + "']");

            if ((part.type == 'nr_decimal') || (part.type == 'nr_scientific')) {

                // Replace scientific notation like 2.5x10<sup>3</sup> with notation like 2.5e3

                $domQuestion.find('.question_answer').each(function() {					
					$(this).find('.block_variable').each(function() {					
						if ($(this).find('sup').length == 1) {
							$(this).text($(this).text().match(/-?\d+\.?\d*/)[0] + 'e' + $(this).find('sup').text());
						}
                    });
                });
            }

			if (part.type == 'nr_decimal') {

                // Make sure answers ARE NOT in scientific notation

                $domQuestion.find('.question_answer').each(function() {					
					$(this).find('.block_variable').each(function() {
                        var exponentIndex = $(this).text().search(/[eE]/);					
						if (exponentIndex != -1) {
                            var base = $(this).text().substr(0, exponentIndex);
                            var exponent = parseInt($(this).text().substr(exponentIndex + 1));

                            var isNegative = false;
                            if (base[0] == '-') {
                                base = base.substr(1);
                                isNegative = true;
                            }

                            var decimalIndex = base.indexOf('.');

                            base = base.replace('.', '');
                            var digitsIndex = base.search('[1-9]');
                            base = base.replace(/^0+/, '');

                            if ((decimalIndex >= 0) && (digitsIndex >= 0)) {
                                exponent += decimalIndex - digitsIndex - 1;
                                var newValue = base;
                                if (exponent < 0) {
                                    while (exponent < -1) {
                                        newValue = '0' + newValue;
                                        ++exponent;
                                    }
                                    newValue = '0.' + newValue;
                                } else if (exponent + 1 < newValue.length) {
                                    newValue = newValue.substr(0, exponent + 1) + '.' + newValue.substr(exponent + 1);
                                } else {
                                    while (exponent + 1 > newValue.length) {
                                        newValue = newValue + '0';
                                        --exponent;
                                    }
                                }

                                if (isNegative) newValue = '-' + newValue;
                                $(this).text(newValue);
                            }
						}
					});
				});

			} else if (part.type == 'nr_scientific') {

                // Make sure answers ARE in scientific notation

                $domQuestion.find('.question_answer').each(function() {					
					$(this).find('.block_variable').each(function() {						
						if ($(this).text().search(/[eE]/) == -1) {
							var start = $(this).text().search(/[^-0.]/);
							if (start >= 0) {
								var dPos = $(this).text().indexOf('.');
								var end = $(this).text().search(/[^-\d.]/);
								if (end == -1) end = $(this).text().length;
								if (end > start) {
									var numDecimals = end - start - 1;
									if (dPos > start) numDecimals--;
									var value = parseFloat($(this).text());
									$(this).text(value.toExponential(numDecimals));
								}
							}
						}
					});
				});

			}

			var key = {};
			var bestValue = 0.0;
			var numDigits = false;
			if ('num_digits' in part) {

				numDigits = parseInt(part['num_digits']);
				
			} else if ('ScoredResponses' in part) {
				
				for (var i = 0; i < part.ScoredResponses.length; ++i) {
					var answerID = part.ScoredResponses[i].id;

                    var value = part.ScoredResponses[i].value;
                    key[answerID] = value;
                    if (value > bestValue) bestValue = value;
                    
					var $domAnswer = $domQuestion.find('.question_answer[data-answer-id="' + answerID + '"]');
					if ($domAnswer.length == 1) {
						var answer = $domAnswer.text();


						var thisDigits = 0;
						for (var j = 0; j < answer.length; ++j) {
							if ((answer[j] >= '0') && (answer[j] <= '9')) thisDigits++;
						}
	
						if (numDigits == false) numDigits = thisDigits;
						else if (numDigits != thisDigits) {
							numDigits = false;
							break;
						}
					}
				}

			} else {

				$domQuestion.find('.question_answer').each(function() {
					var answer = $(this).text();

					var thisDigits = 0;
					for (var i = 0; i < answer.length; ++i) {
						if ((answer[i] >= '0') && (answer[i] <= '9')) thisDigits++;
					}

					if (numDigits == false) numDigits = thisDigits;
					else if (numDigits != thisDigits) {
						numDigits = false;
						return false;
					}
				});

			}
			
			if (part.type == 'nr_scientific') {
				var numExponent = 1;
				var isNegative = false;
				var expNegative = false;
				if (('num_exponent' in part) && ('is_negative' in part)) {

					// Exponent information has been passed along from SittingsController,
					// since answers may be suppressed

					numExponent = part['num_exponent'];
					isNegative = (part['is_negative'] == 1);
					expNegative = (part['exp_negative'] == 1);

				} else {

					// Find the number of exponent digits required and whether exponent is negative

					$domQuestion.find('.question_answer').each(function() {						
                        var answerID = $(this).attr('data-answer-id');
						var value = (answerID in key) ? key[answerID] : 0.0;
						if ((!('ScoredResponses' in part)) || ((value > 0.0) && (value == bestValue))) {
							var exponent = null;
							var parts = $(this)[0].innerText.split(/[eE]/);
							if (parts.length == 2) {
								var coefficient = parts[0];
								if (coefficient[0] == '-') {
									isNegative = true;
								}

								var exponent = parts[1];
								if (exponent[0] == '-') {
									expNegative = true;
									exponent = exponent.substring(1);
								} else if (exponent[0] == '+') {
									exponent = exponent.substring(1);
								}
								if (exponent.length > numExponent) numExponent = exponent.length;
							}
						}
	
					});

				}
				
				if (numDigits !== false) {
					var blankString = isNegative ? "−" : "";
					var digitLabel = "a".charCodeAt(0);
					blankString += "<i><b>";
					for (var i = 0; i < numDigits - numExponent; ++i) {
						blankString += String.fromCharCode(digitLabel++);
                        var form_decimal = await localizationInstance.getString('form_decimal');
						if (i == 0) blankString += "</b></i>" + form_decimal + "<i><b>";
					}
					blankString += "</b></i>&nbsp;×&nbsp;10<sup>";
					if (expNegative) blankString += "−";
					blankString += "<i><b>";
					for (var i = 0; i < numExponent; ++i) {
						blankString += String.fromCharCode(digitLabel++);
					}
					blankString += "</b></i></sup>";
					$domQuestion.find('.block_bigBlank').replaceWith(blankString);
					
					digitLabel = "a".charCodeAt(0);
                    var questions_values = await localizationInstance.getString('questions_values');
					var afterString = " " + questions_values + " ";
					for (var i = 0; i < numDigits; ++i) {
                        var questions_and = await localizationInstance.getString('questions_and');
						if (i == numDigits - 1) afterString += " " + questions_and + " ";
						else if (i > 0) afterString += ", ";
						afterString += "<i><b>" + String.fromCharCode(digitLabel++) + "</b></i>";
					}
                    var questions_are = await localizationInstance.getString('questions_are');
					afterString += " " + questions_are + " ";
					for (var i = 0; i < numDigits; ++i) {
                        var questions_and = await localizationInstance.getString('questions_and');
						if (i == numDigits - 1) afterString += " " + questions_and + " ";
						else if (i > 0) afterString += ", ";
						afterString += "<span class='block_smallBlank'></span>";
					}
					afterString += ".";
					$domQuestion.find('.question_prompt').append(afterString);
				}
			}

			if ('prompt' in part.TypeDetails) {
				if (part.TypeDetails.prompt.type == 'text') {
					$domQuestion.find('.question_prompt').after('<div class="response_prompt">(' + part.TypeDetails.prompt.text + ')</div>');
				} else if ((part.TypeDetails.prompt.type === 'simple') || ((part.TypeDetails.prompt.type === 'digits') && (numDigits === false))) {
                    var questions_nr_prompt_1 = await localizationInstance.getString('questions_nr_prompt_1');
                    var questions_nr_prompt_paper = await localizationInstance.getString('questions_nr_prompt_paper');
					$domQuestion.find('.question_prompt').after('<div class="response_prompt">(' + questions_nr_prompt_1 + ' ' +
						'<span class="response_prompt_location">' + questions_nr_prompt_paper + '</span>.)</div>');
				} else if (part.TypeDetails.prompt.type === 'digits') {
                    var digitString = "[ERROR]";
					if (numDigits <= 10) {
						if (countWords == null) {
							var countsList = await localizationInstance.getString('questions_numbers');
							if (countsList.indexOf('ERROR') == -1) countWords = countsList.split(',');
						}
						if (countWords != null) digitString = countWords[numDigits - 1];
					} else digitString = numDigits.toString();
					
                    var questions_nr_prompt_2 = await localizationInstance.getString('questions_nr_prompt_2');
                    var questions_nr_prompt_paper = await localizationInstance.getString('questions_nr_prompt_paper');
					$domQuestion.find('.question_prompt').after('<div class="response_prompt">(' + 
						questions_nr_prompt_2.replace('#', digitString) + ' ' + '<span class="response_prompt_location">' + 
                        questions_nr_prompt_paper + '</span>.)</div>');
				}
			}
		}
	});

	await RenderTools.renderEquations($target);
}

var fisherYatesShuffle = function($wrapper, answers) {
	for (var i = 0; i < answers.length; ++i) {
		var $answerWrapper = $wrapper.find('.question_answer[data-answer-id="' + answers[i].id + '"]');
		answers[i]._element = $answerWrapper;
	}

	if (answers.length > 0) {
		for (var i = 0; i < answers.length; ++i) {
			var j = i + Math.floor(Math.random() * (answers.length - i));
			if (i != j) {
				var temp = answers[i];
				answers[i] = answers[j];
				answers[j] = temp;	
			}
		}
		
		var $lastElement = answers[answers.length - 1]._element;
		for (var i = 0; i < answers.length; ++i) {
			answers[i]._element.insertBefore($lastElement);
			delete answers[i]._element;
		}
	}
}

var pyramidShuffle = function($wrapper, answers) {
	var isNumeric = true;
	var numericRegex = /^-?([1-9]\d*|0)(\.\d+)?.*$/;
	for (var i = 0; i < answers.length; ++i) {
		var $answerWrapper = $wrapper.find('.question_answer[data-answer-id="' + answers[i].id + '"]');
		var answerText = $answerWrapper.find('.answer_text').text();
		isNumeric &= numericRegex.test(answerText);
	}

	for (var i = 0; i < answers.length; ++i) {
		var $answerWrapper = $wrapper.find('.question_answer[data-answer-id="' + answers[i].id + '"]');
		answers[i]._element = $answerWrapper;

		if (isNumeric) answers[i]._size = parseFloat($answerWrapper.find('.answer_text').text());
		else answers[i]._size = RenderTools.measureWidth($answerWrapper.find('.answer_text'));
	}

	if (answers.length > 0) {
		var sortType = Math.floor(Math.random() * 2) * 2 - 1;
		answers.sort(function(a, b) {
			if (a._size < b._size) return -sortType;
			else if (a._size > b._size) return sortType;
			else return 0;
		});
		
		var $lastElement = answers[answers.length - 1]._element;
		for (var i = 0; i < answers.length; ++i) {
			answers[i]._element.insertBefore($lastElement);
			delete answers[i]._element;
			delete answers[i]._size;
		}
	}
}

var getHalton = function(index, base) {
	var divisor = base;
	var workingIndex = index;

	var value = 0;
	while (workingIndex > 0) {
		value += (workingIndex % base) / divisor;
		workingIndex = Math.floor(workingIndex / base);
		divisor *= base;
	}

	return value;
}

var shuffleAssessmentQuestions = function(assessmentData) {
	for (var i = 0; i < assessmentData.Sections.length; ++i) {
		if (assessmentData.Sections[i].type == 'page_break') continue;
		if (!assessmentData.Sections[i].shuffleQuestions) continue;

		for (var j = 0; j < assessmentData.Sections[i].Content.length - 1; ++j) {
			if (assessmentData.Sections[i].Content[j].type == 'shuffle_break') continue;

			var kLimit;
			for (kLimit = j + 1; kLimit < assessmentData.Sections[i].Content.length; ++kLimit) {
				if (assessmentData.Sections[i].Content[kLimit].type == 'shuffle_break') break;
			}
			var k = j + Math.floor(Math.random() * (kLimit - j));

			var temp = assessmentData.Sections[i].Content[j];
			assessmentData.Sections[i].Content[j] = assessmentData.Sections[i].Content[k];
			assessmentData.Sections[i].Content[k] = temp;
		}	
	}
}

var shuffleQuestionParts = function(questionData) {
	if (questionData.Settings.shuffleParts) {
		for (var p = 0; p < questionData.Parts.length; ++p) {
			if (questionData.Parts[p].type != 'context') {
				var qLimit;
				for (qLimit = p + 1; qLimit < questionData.Parts.length; ++qLimit) {
					if (questionData.Parts[qLimit].type == 'context') break;
				}
				var q = p + Math.floor(Math.random() * (qLimit - p));
				if (q != p) {
					var temp = questionData.Parts[p];
					questionData.Parts[p] = questionData.Parts[q];
					questionData.Parts[q] = temp;	
				}
			}
		}	
	}
}

var shuffleAssessmentParts = function(assessmentData, questionData) {
	if (typeof shuffleAssessmentParts.questionBases == 'undefined') {
		shuffleAssessmentParts.questionBases = {};
		shuffleAssessmentParts.index = 1000 + Math.floor(Math.random() * 10000);
		shuffleAssessmentParts.leap = 373;
	}

	shuffleAssessmentParts.index += shuffleAssessmentParts.leap;
	for (var i = 0; i < assessmentData.Sections.length; ++i) {
		if (assessmentData.Sections[i].type == 'page_break') continue;
		for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
			var questionHandleIDs = [];
			if (assessmentData.Sections[i].Content[j].type == 'item') {
				questionHandleIDs.push(assessmentData.Sections[i].Content[j].question_handle_id);
			} else if (assessmentData.Sections[i].Content[j].type == 'item_set') {
				questionHandleIDs = assessmentData.Sections[i].Content[j].question_handle_ids;

				// Get a prime base for the Halton shuffle for this question

				var numItems = assessmentData.Sections[i].Content[j].question_handle_ids.length;
				var contentID = assessmentData.Sections[i].Content[j].id;
				if (!(contentID in shuffleAssessmentParts.questionBases)) {
					var oldPrimes = [];
					var nextPrime = 2;
					for (var entryID in shuffleAssessmentParts.questionBases) {
						var thisPrime = shuffleAssessmentParts.questionBases[entryID];
						oldPrimes.push(thisPrime);
						if (thisPrime > nextPrime) nextPrime = thisPrime;
					}
				
					var isPrime;
					do {
						++nextPrime;
						isPrime = true;
						for (var k = 0; k < oldPrimes.length; ++k) {
							if (nextPrime % oldPrimes[k] == 0) isPrime = false;
						}
					} while (!isPrime);

					shuffleAssessmentParts.questionBases[contentID] = nextPrime;
				}

				// Get a rendered index for this set using a Halton sequence

				var thisBase = shuffleAssessmentParts.questionBases[contentID];
				var haltonValue = getHalton(shuffleAssessmentParts.index, thisBase);
				assessmentData.Sections[i].Content[j].rendered_index = Math.floor(haltonValue * numItems);
			}

			for (var k = 0; k < questionHandleIDs.length; ++k) {

				// Shuffle parts if required

				var questionHandleID = questionHandleIDs[k];
				var thisData = questionData[questionHandleID].json_data;
				shuffleQuestionParts(thisData);
			}
		}
	}
}

var shuffleQuestionAnswers = function(questionData, $questionWrapper, shuffleType) {
	if ('Common' in questionData) {
		if (questionData.Common.shuffleAnswers) {
			if ((shuffleType == 'Pyramid') && ($questionWrapper.length > 0)) {
				pyramidShuffle($questionWrapper, questionData.Common.Answers);
			} else fisherYatesShuffle($questionWrapper, questionData.Common.Answers);
		}
	} else {
		for (var k = 0; k < questionData.Parts.length; ++k) {
			var $partWrapper = $questionWrapper.find('.part_wrapper[data-part-id="' + questionData.Parts[k].id + '"]');
			if ((questionData.Parts[k].type == 'mc') || (questionData.Parts[k].type == 'mc_table')) {
				if (questionData.Parts[k].TypeDetails.shuffleAnswers) {
					if ((questionData.Parts[k].type == 'mc') && (shuffleType == 'Pyramid') && ($partWrapper.length > 0)) {
						pyramidShuffle($partWrapper, questionData.Parts[k].Answers);
					} else fisherYatesShuffle($partWrapper, questionData.Parts[k].Answers);
				}
			}
		}
	}
}

var shuffleAssessmentAnswers = function(assessmentData, questionData, $wrapper) {
	var shuffleType = (assessmentData.Settings.mcFormat == 'Pyramid') ? 'Pyramid' : 'Full';
	
	for (var i = 0; i < assessmentData.Sections.length; ++i) {
		if (assessmentData.Sections[i].type == 'page_break') continue;
		for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
			if (assessmentData.Sections[i].Content[j].type != 'shuffle_break') {
				var contentID = assessmentData.Sections[i].Content[j].id;

				// Shuffle answers if required

				var $questionWrapper = $wrapper.find('.question_wrapper[data-content-id="' + contentID + '"]');
				var questionHandleID = parseInt($questionWrapper.attr('data-question-handle-id'));

				var thisData = questionData[questionHandleID].json_data;
				shuffleQuestionAnswers(thisData, $questionWrapper, shuffleType);
			}
		}
	}
}

var drawAssessment = function(assessmentData, questionData, $target) {
	var sectionPromises = [];
	for (var i = 0; i < assessmentData.Sections.length; ++i) {
		sectionPromises.push(drawSection(assessmentData.Sections[i], questionData, $target));
	}

	return Promise.all(sectionPromises);
}

var drawSection = function(sectionData, questionData, $target) {
	$('#assessment_empty_wrapper').hide();
	$target.show();

	var sectionAttributes = ' data-section-id="' + sectionData.id + '"';

	var promises = [];
	if (sectionData.type == 'page_break') {

		$target.append('<div class="preview_section_wrapper page_break"' + sectionAttributes + '></div>');

	} else {

		if (('allow_multiple' in sectionData) && sectionData.allow_multiple) sectionAttributes += ' data-allow-multiple';
		if (('allow_negative' in sectionData) && sectionData.allow_negative) sectionAttributes += ' data-allow-negative';
	
		var $sectionWrapper = 
			$('<div class="preview_section_wrapper"' + sectionAttributes + '> \
				<h3 class="preview_header' + (sectionData.pageBreakBefore ? ' pagebreak' : '') + '"' + 
				' style="display:' + (sectionData.showHeader ? 'block' : 'none') + ';">' + 
				escapeHTML(sectionData.header) + '</h3> \
			</div>');
		$target.append($sectionWrapper);
	
		for (var i = 0; i < sectionData.Content.length; ++i) {
			promises.push(drawItem(sectionData.Content[i], questionData, $sectionWrapper));
		}
	
	}

	return Promise.all(promises);
}

var drawItem = function(itemData, questionData, $sectionWrapper) {
	if (itemData.type == 'item') {

		var questionHandleID = itemData.question_handle_id;

		var $itemWrapper = $('<div class="question_wrapper" data-content-id="' + itemData.id + '" \
			data-question-handle-id="' + questionHandleID + '"></div>');
		$sectionWrapper.append($itemWrapper);
		
		var json_data = questionData[questionHandleID].json_data;
		return renderItemToDiv(json_data, $itemWrapper);
		
	} else if (itemData.type == 'item_set') {

        var renderedIndex = 0;
		if ('rendered_index' in itemData) renderedIndex = itemData.rendered_index;
        if (renderedIndex >= itemData.question_handle_ids.length) renderedIndex = 0;
        itemData.rendered_index = renderedIndex;

		var questionHandleID = itemData.question_handle_ids[renderedIndex];

		var $itemWrapper = $('<div class="question_wrapper" data-content-id="' + itemData.id + '" \
			data-question-handle-id="' + questionHandleID + '"></div>');
		$sectionWrapper.append($itemWrapper);
		
		var json_data = questionData[questionHandleID].json_data;
		return renderItemToDiv(json_data, $itemWrapper);
		
	} else if (itemData.type == 'shuffle_break') {
		
		$sectionWrapper.append('<div class="question_wrapper shuffle_break"></div>');

		return Promise.resolve();
	}
}

async function initialize() {
    if (assessmentsRenderInitializing) return;
    assessmentsRenderInitializing = true;

    await RenderTools.initialize();

    assessmentsRenderInitialized = true;
}

export { initialize, renderItemToDiv, 
    shuffleAssessmentQuestions, shuffleQuestionParts, shuffleAssessmentParts, shuffleAssessmentAnswers, 
    drawAssessment };
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { localizationInstance } from '/js/FormEngine/Localization.js?7.0';

import Equation from "/js/EquationEditor/equation.js?7.0";

var renderToolsInitializing = false;
var renderToolsInitialized = false;

var stylesCache = {};

function updateEquationJSON(json_data, variableName, displayName) {
	if (json_data.type == 'Variable') {
		if (json_data.value == variableName) {
			if (displayName.length > 0) json_data.displayName = displayName;
			else delete(json_data.displayName);
		}
	} else if (('operands' in json_data) && (json_data.operands != null)) {
		for (var key in json_data.operands) {
			for (var i = 0; i < json_data.operands[key].length; ++i) {
				json_data.operands[key][i] = updateEquationJSON(json_data.operands[key][i], variableName, displayName);
			}
		}
	}
	return json_data;
}

function updateVariableNames($scope, variables) {
	for (var key in variables) {
		for (var i = 0; i < variables[key].length; ++i) {
			var variableName = variables[key][i].variableName;

			var displayName;
			if (key == 'deleted') displayName = '<i>deleted</i>';
			else displayName = variables[key][i].displayName;

			$scope.find('.block_variable[data-variable-name="' + variableName + '"]').each(function() {
				$(this).html(displayName);
			});
			$scope.find('.equation').each(function() {
				var json_data = JSON.parse($(this).attr('data-json'));
				json_data = updateEquationJSON(json_data, variableName, displayName);
				$(this).attr('data-json', htmlSingleQuotes(JSON.stringify(json_data)));
			});
		}
	}

	$scope.find('.block_variable[data-variable-name="_deleted"]').each(function() {
		$(this).html('<i>deleted</i>');
	});
}

function measureWidth($item) {
	var newCSS = {};
	newCSS['width'] = 'auto';
	newCSS['position'] = 'absolute';
	newCSS['overflow'] = 'hidden';
	newCSS['white-space'] = 'nowrap';
	
	var oldCSS = {};
	for (var key in newCSS) {
		if ($item.prop("style")[key]) oldCSS[key] = $item.css(key);
		$item.css(key, newCSS[key]);
	}
	
	var width = $item.width();

	for (var key in newCSS) {
		if (key in oldCSS) $item.css(key, oldCSS[key]);
		else $item.css(key, "");
	}

	return(width);
}

function setMCImageWidths($item) {
	var $itemAnswers = $item.find('.question_answer');
	var itemWidth = $item.width();
	$.each($itemAnswers, function() {
		$(this).find('img').each(function() {
			var widthPercent = parseFloat($(this).attr('data-relative-width'));
			$(this).css('width', (widthPercent * itemWidth / 100) + 'px');
		});
	});
}

function setMCWidths($item) {
	var $itemAnswers = $item.find('.question_answer');
    if ($itemAnswers.length > 0) {
        var parentWidth = $item.find('.question_text').width();

        var maxWidth = 0.0;
        $.each($itemAnswers, function() {
            
            // Each answer requires the width of answer text, plus padding before the next answer, plus a margin
            // of size equal to that padding on the left for answer_label
    
            var thisWidth = measureWidth($(this).find('.answer_text')) + 2 * parseFloat($(this).css('padding-right'));
            if (thisWidth > maxWidth) maxWidth = thisWidth;
        });
        
        var numAnswers = $item.find('.question_answer').length;
        
        var columns;
        if (numAnswers < 4) columns = 1;
        else columns = Math.floor(parentWidth / maxWidth);
        
        if (columns <= 1) columns = 1;
        else if (columns >= numAnswers) columns = numAnswers;
        else {
            var targetRows = Math.ceil(numAnswers / columns);
            while (Math.ceil(numAnswers / columns) == targetRows) columns--;
            columns++
        }
    
        if (columns == 1) $itemAnswers.css('padding-right', '0px');
    
        var answerWidth = parentWidth / columns;
        $item.find('.question_answer').outerWidth(answerWidth);
    }
}

var setMatchingWidths = function($target) {
	
	// Size matching questions
	
	$target.find('.matching_blank .online_blank').each(function() {
		var parentWidth = $(this).parent().width() - parseFloat($(this).parent().css('padding-right'));
		var selfWidth = $(this).width() + parseFloat($(this).css('margin-right'));
		$(this).next().width(parentWidth - selfWidth);
	});
}

var renumberMC = function($target) {
	$target.find('.part_wrapper, .context_wrapper').each(function() {
		var $answerList = $(this).find('.mc_answer_list, .mc_answer_table, .context_answer_list');
		if ($answerList.length > 0) {
			var answerLabel = "A".charCodeAt(0);
			$answerList.find('.answer_label').each(function() {
				$(this).html(String.fromCharCode(answerLabel++) + ".");
			});
		}
	});
}

var renderStyles = {};

renderStyles['Alpha'] = {
	style:		"/css/render/alpha.css",
	renumber:	async function($target) {
					$target.find('.style_tag').remove();

					var questionNumber = 1;
					$target.find('.question_wrapper').each(function() {
						if ($(this).find('.context_wrapper').length > 0) {
							var partLabel = null;
							$(this).find('.context_wrapper,.part_wrapper').each(function() {
								if ($(this).is('.context_wrapper')) {
									$(this).find('.question_number').text((questionNumber++).toString() + '.');
									partLabel = "a".charCodeAt(0);
								} else {
									$(this).find('.question_number').text(String.fromCharCode(partLabel++) + ')');
								}
							});							
						} else {
							$(this).find('.part_wrapper').each(function() {
								$(this).find('.question_number').text((questionNumber++).toString() + '.');
							});
						}
					});
	
					renumberMC($target);
				}
};

renderStyles['Numbers'] = {
	style:		"/css/render/numbers.css",
	renumber:	async function($target) {
					$target.find('.style_tag').remove();

					var questionNumber = 1;
					$target.find('.question_wrapper').each(function() {
						if ($(this).find('.context_wrapper').length > 0) {
							$(this).find('.context_wrapper .question_number').text('&nbsp;');
						}
						$(this).children('.part_wrapper').each(function() {
							$(this).find('.question_number').text((questionNumber++).toString() + '.');
						});
					});
	
					renumberMC($target);
				}
};

renderStyles['Type'] = {
	style:		"/css/render/type.css",
	renumber:	async function($target) {
					$target.find('.style_tag').remove();

					var questionNumbers = {};
					$target.find('.question_wrapper').each(function() {
						if ($(this).find('.context_wrapper').length > 0) {
							$(this).find('.context_wrapper .question_number').text('&nbsp;');
						}
						$(this).children('.part_wrapper').each(async function() {
							var style = $(this).attr('data-type').substr(0, 2);
							if (!(style in questionNumbers)) questionNumbers[style] = 1;
							$(this).find('.question_number').text((questionNumbers[style]++).toString() + '.');
							
							if (style == 'nr') {
                                var questions_nr_tag = await localizationInstance.getString('questions_nr_tag');
								$(this).before('<div class="style_tag">' + questions_nr_tag + '</div>');
							} else if (style == 'wr') {
                                var questions_wr_tag = await localizationInstance.getString('questions_wr_tag');
								$(this).before('<div class="style_tag">' + questions_wr_tag + '</div>');
							}
						});
					});
	
					renumberMC($target);
				}
};

var setRenderStyle = async function(name) {
	if (!(name in stylesCache)) {
		const response = await fetch(renderStyles[name].style);
		stylesCache[name] = await response.text();
	}
	createStyleElement('renderStyle', stylesCache[name]);
}

var createStyleElement = function(id, content) {
	if ($('#' + id).length > 0) $('#' + id).remove();
	var newSheet = document.createElement('style');
	newSheet.setAttribute("id", id);
	newSheet.innerHTML = content;
	document.body.appendChild(newSheet);
}

function sampleVariables(questionData) {
    
    // Build a workspace for variable values if required

    if (('Engine' in questionData) && ('data' in questionData.Engine)) {
        var variablesHTML = "<div id='variable_rows' style='display:none;'>";

        var tableData = questionData.Engine.data;

        var allVariables = [];
        for (var type in tableData) {
            if (tableData.hasOwnProperty(type) && (type != 'deleted')) {
                allVariables = allVariables.concat(tableData[type]);
            }
        }

        for (var i = 0; i < allVariables.length; ++i) {
            variablesHTML += "<span class='block_variable' data-variable-name='" + allVariables[i].variableName + "'>" + 
                allVariables[i].displayName + "</span>";
        }

        variablesHTML += "</div>";
        var $variablesDOM = $(variablesHTML);
        $('body').append($variablesDOM);

        var fullText = "";
        for (var importID in questionData.Engine.Imports) {
            if (questionData.Engine.Imports.hasOwnProperty(importID)) {
                fullText += questionData.Engine.Imports[importID].js;
            }
        }
        fullText += questionData.Engine.js;

        function evalInContext(js, context) {
            return function() { return eval(js); }.call(context);
        }

        var variableValues = {};
        for (var i = 0; i < allVariables.length; ++i) {
            variableValues[allVariables[i].variableName] = [];
        }
        
        var errors = [];
        for (var j = 0; j < 1000; ++j) {

            try {
    
                evalInContext(fullText, $variablesDOM);
        
            } catch(err) {
    
                if (errors.indexOf(err.message) == -1) errors.push(err.message);
    
            }    

            for (var i = 0; i < allVariables.length; ++i) {
                var $variable = $variablesDOM.find('.block_variable[data-variable-name="'
                    + allVariables[i].variableName + '"]');
                if ($variable.is('.rendered')) {
                    var htmlValue = $variable.html();
                    variableValues[allVariables[i].variableName].push(htmlValue);
                }
            }
        }

        if (errors.length > 0) {
            $('#variable_error_list').empty();
            for (var i = 0; i < errors.length; ++i) {
                $('#variable_error_list').append('<li>' + errors[i] + '</li>');
            }

            $("#variable_error_wrapper").show();
            $(".variable_values_wrapper").hide();
            
        } else {
            
            $("#variable_error_wrapper").hide();
            $(".variable_values_wrapper").show();

            for (var i = 0; i < allVariables.length; ++i) {
                var variableName = allVariables[i].variableName;
                if (variableValues[variableName].length > 0) {
    
                    for (var j = 0; j < variableValues[variableName].length; ++j) {
                        var renderedValue = variableValues[variableName][j];
                        var floatValue;
                        if (renderedValue.indexOf('×10<sup>') >= 0) {
                            var scientificText = renderedValue.replace('×10<sup>', 'e');
                            scientificText = scientificText.replace('</sup>', '');
                            floatValue = parseFloat(scientificText);
                        } else if (renderedValue.indexOf('/') >= 0) {
                            var parts = renderedValue.split('/');
                            floatValue = parseFloat(parts[0]) / parseFloat(parts[1]);
                        } else floatValue = parseFloat(renderedValue);
        
                        variableValues[variableName][j] = {
                            rendered: renderedValue,
                            floatValue: floatValue
                        };
                    }
        
                    variableValues[variableName].sort((a, b) => (a.floatValue - b.floatValue));
        
                    var minEntry = variableValues[variableName][0];
                    var maxEntry = variableValues[variableName][variableValues[variableName].length - 1];
                    var rangeHTML = '<span>' + minEntry.rendered + '</span><span style="margin:0px 5px;">→</span>' + 
                        '<span>' + maxEntry.rendered + '</span>'
                    $('.variable_range[data-variable-name="' + variableName + '"]').html(rangeHTML);
    
                }
            }    
            
        }
    }
}

async function renderEquations($target) {
    if ($target.find('.equation').length == 0) return;

    $target.find('.equation').each(function() {
        if (!('equation' in this)) {
            var jsonObj;
            if (this.hasAttribute('data-json')) {
                jsonObj = JSON.parse($(this).attr('data-json'));
            } else {
                jsonObj = {
                    type: "Equation",
                    value: null,
                    operands: {
                        topLevelContainer: [{type: "Symbol", value:"△", operands:null}]
                    }
                };
                $(this).attr('data-json', JSON.stringify(jsonObj));
            } 

            var equation = Equation.constructFromJsonObj(jsonObj);
            $(this).empty().append(equation.domObj.value);
            equation.updateAll();
            
            var baselineOffset = 6.0;
            var topLevelContainer = equation.topLevelContainer;
            var topPadding = (topLevelContainer.padTop + equation.adjustTop) * equation.getFontHeight();
            var maxTopAlign = topLevelContainer.wrappers[topLevelContainer.maxTopAlignIndex].topAlign + topPadding;
            var offset = (maxTopAlign + baselineOffset) - equation.topLevelContainer.height;
            $(this).css('vertical-align', offset.toString() + 'px');	
        }
    });
}

function unrenderEquations($target) {
	$target.find('.equation').each(function() {

        // Remove rendered equation from HTML

		$(this).empty().html("Loading&hellip;");

    });
}

var _loadFonts = function(callback) {
    return new Promise((resolve, reject) => {
		WebFont.load({
			custom: {
				families: ['MathJax_Main:n4,i4', 'MathJax_Math:i4', 'MathJax_AMS:n4'],
				urls: ['/js/EquationEditor/Fonts/TeX/font.css']
			},

			active: function() {
				resolve();
			},

			inactive: function() {
				reject(new Error("Failed to load custom fonts via WebFont."));
			}
		});  
	});
}

var _loadImages = function(arrayOfImages, callback) {
    return new Promise((resolve, reject) => {
		$(arrayOfImages).each(function () {
			$('<img />').attr('src',this).appendTo('body').css('display','none');
		});

		resolve();
	});
}

async function initialize() {
    if (renderToolsInitializing) return;
    renderToolsInitializing = true;

    await Promise.all([_loadFonts(), _loadImages()]);

	renderToolsInitialized = true;
}

export { initialize,
	renderEquations, unrenderEquations,
	updateVariableNames, updateEquationJSON,
    setRenderStyle, renderStyles,
    measureWidth, setMCWidths, setMCImageWidths, setMatchingWidths,
    sampleVariables };
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as IndexCommon from '/js/common/index.js?7.0';
import * as Folders from '/js/common/folders.js?7.0';

function drawIndexRow(data) {

	// Add a row in the index table for this entry

	var canCopy = 1;
	var canDelete = 0;
	if (('Permissions' in data) && (data['Permissions'].indexOf('can_delete') >= 0)) canDelete = 1;

	var html = "<tr data-assessment-id='" + data['Assessment']['id'] + "' " +
		"data-can-copy='" + canCopy + "' " +
		"data-can-delete='" + canDelete + "'>";

	html += '<td style="width:15px; text-align:center;">' + 
		'<input type="checkbox" class="item_select" style="margin:1px 0px;" autocomplete="off" />' + 
		'</td>';

	if ('User' in data) {
		html += '<td style="width:250px;">' + data['User']['email'] + '</td>';
	}

	var date = dayjs.utc(data['Assessment']['moved']).local();
	html += '<td style="width:120px;">' + date.format('YYYY-MM-DD HH:mm') + '</td>';

	html += '<td><div style="display:flex; flex-direction:row; align-items:flex-start; gap:5px;">';
	html += '<div class="index_folder_link" data-folder-id="' + data['Assessment']['folder_id'] + '"></div>';
	if (data['Assessment']['is_new'] >= 2) html += "<div class='index_new_label label label-info'>New</div> ";
	html += '<div class="index_item_name">' + escapeHTML(data['Assessment']['save_name']) + '</div>';
	html += '</div></td>';

	html += "</tr>";
	
	$('.index_table').append(html);
}

function finishIndex() {
	if (IndexCommon.activeNode === false) {

		$('#share_dropdown').removeAttr('disabled');
		$('#sendCopy_button').removeClass('always-disabled-link');
		$('#newItem_button').hide();

	} else {
		
		// Update button availability

		if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_export') >= 0) {
			$('#share_dropdown').removeAttr('disabled');
			$('#sendCopy_button').removeClass('always-disabled-link');
		} else {
			$('#share_dropdown').attr('disabled', 'disabled');
			$('#sendCopy_button').addClass('always-disabled-link');
		}

		// Set up right-side tab buttons

		$('.tab_right').hide();
		if (IndexCommon.activeNode.id == IndexCommon.roots['Trash'].id) $('#emptyTrash_button').show();
		else {
			var canAdd = Folders.isItemFolder(treeData, IndexCommon.activeNode.id);
			if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_add') == -1) canAdd = false;
			if (canAdd) {
				$('#newItem_button .tab_icon').css('background-image', 'url("/img/tab_icons/add_item_icon.svg")');
				$('#newItem_button .tab_text').html('New<br />Assessment');
				$('#newItem_button').show();
			} else $('#newItem_button').hide();
		}

		// Check for an empty index

		if ($('.index_table tr').length == 0) {
			var html = "";
			html += '<tbody><tr class="disabled"><td colspan="5"><div style="padding-left:10px;">';
			html += '<i>No assessments to show here.';
			if (IndexCommon.activeNode.id != IndexCommon.roots['Trash'].id) html += ' Click "New Assessment" in the menu bar to create one.';
			html += '</i></div></td></tr></tbody>';
			$('.index_table').html(html);
		}
	}
}

$(document).ready(function() {

    IndexCommon.initialize({ drawIndexRow, finishIndex });

	var questionRoots = false;

	if (typeof(questionTreeData) != 'undefined') {
		$(".folderTree2").each(function() {
			Folders.initFolderTree($(this), questionTreeData);
		});

		questionRoots = {};
		for (var i = 0; i < questionTreeData.length; ++i) {
			questionRoots[questionTreeData[i].text] = {
				id: questionTreeData[i].id,
				folder_id: questionTreeData[i].data.folder_id
			};
		}

		Folders.disableSubtree($('#exportFolderParent'), questionRoots['Trash'].id);
	}
	
	$('#editDocument_button').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;
		
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				window.location.href = '/Assessments/build/' + $(this).closest('tr').attr('data-assessment-id');
			}
		});
	});
	
	$(document).on('dblclick', '.selection_table tbody tr', function(e) {
		e.preventDefault();
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked == 0) {
			window.location.href = '/Assessments/build/' + $(this).attr('data-assessment-id');
		} else if (numChecked == 1) {
			var selectedID = $selectedCheckbox.closest('tr').attr('data-assessment-id');
			if (selectedID == $(this).attr('data-assessment-id')) {
				window.location.href = '/Assessments/build/' + selectedID;
			}
		}
	});

	$(document).on('change', '.export_checkbox>input', function(e) {
		var hasChecked = false;
		$('.export_checkbox>input').each(function() {
			if ($(this).prop('checked')) hasChecked = true;
		});
		if (hasChecked) $('#export_dialog').parent().find('button:contains("Save")').removeAttr('disabled');	
		else $('#export_dialog').parent().find('button:contains("Save")').attr('disabled', 'disabled');	
	});

	$(document).on('click', '.export_checkbox>input', function(e) {
		e.stopPropagation();
	});
	
	$(document).on('click', '.export_row', function(e) {
		var isChecked = $(this).find('.export_checkbox>input').prop('checked');
		$(this).find('.export_checkbox>input').prop('checked', !isChecked).trigger('change');
	});
	
	$('#export_select_all').on('click', function() {
		$('.export_checkbox>input').prop('checked', true).trigger('change');
	});

	$('#export_button').on('click', function() {
		var selectedIds = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var thisID = parseInt($(this).closest('tr').attr('data-assessment-id'));
				selectedIds.push(thisID);
			}
		});

		var defaultFolderID = null;
		if (IndexCommon.activeCommunity == null) {

			// Make sure questions for an assessment in user folders are saved to user folders

			Folders.enableAll($('#exportFolderParent'));
			Folders.disableSubtree($('#exportFolderParent'), questionRoots['Communities'].id);
			defaultFolderID = questionRoots['Home'].id;

		} else {

			// Make sure questions for an assessment in community folders are saved to community folders

			Folders.disableAll($('#exportFolderParent'));
			defaultFolderID = IndexCommon.findCommunityNodeID(questionTreeData, IndexCommon.activeCommunity.data.community_id);
			Folders.enableSubtree($('#exportFolderParent'), defaultFolderID);
		}

        var selectID = $('#exportFolderParent').attr('data-prefix') + defaultFolderID;

        $('#exportFolderParent').jstree('close_all');
        $('#exportFolderParent').jstree('open_node', selectID);

        $('#exportFolderParent').jstree('deselect_all');
        $('#exportFolderParent').jstree('select_node', selectID);			

		// Get unlinked items and open dialog

    	if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		const queryString = new URLSearchParams({
			assessment_ids: selectedIds
		}).toString();

		ajaxFetch(`/Assessments/get_unlinked?${queryString}`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ 
				assessment_ids: selectedIds
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
				
			if (responseData.success){
				if (responseData.unlinked.length == 0) {
					$.gritter.add({
						title: "Success",
						text: "All questions in assessment are already linked in this question bank.",
						image: "/img/success.svg"
					});
				} else {
					if (responseData.numLinked > 0) {
						$('#export_omitted').html(' (omitting ' + responseData.numLinked + ' in bank)');
					} else $('#export_omitted').html('');
					
					var listHTML = "";
					for (var i = 0; i < responseData.unlinked.length; ++i) {
						listHTML += 
							"<div class='export_row' data-question-handle-id='" + responseData.unlinked[i].id + "'> \
								<div class='export_checkbox'><input type='checkbox' style='margin:0px;'/></div> \
								<div class='export_name'>" + responseData.unlinked[i].name_alias + "</div> \
							</div>"
					}
					$("#export_list").html(listHTML);
					
					$('#export_dialog').parent().find('button:contains("Save")').attr('disabled', 'disabled');	
					$('#export_dialog').dialog('open');
				}
			}
		});			
	});
	
	var linkItems = function() {
		var $dialog = $(this);
		
		var handle_ids = [];
		$('.export_row').each(function() {
			if ($(this).find('.export_checkbox>input').prop('checked')) {
				handle_ids.push($(this).attr('data-question-handle-id'));
			}
		});
		
		var json = Folders.getSelectedNode("exportFolderParent");
		var target_id = json.data.folder_id;

    	if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/UserQuestions/link_handles', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ 
				handle_ids: handle_ids,
				folder_id: target_id
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

			if (responseData.success) {
				$.gritter.add({
					title: "Success",
					text: "Questions saved to question bank.",
					image: "/img/success.svg"
				});

				$dialog.dialog('close');
			} else showSubmitError($dialog);

		}).catch(function() {
			showSubmitError($dialog);
		});
	}
    
	$('#export_dialog').dialog({
		autoOpen: false,
		width:610,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': linkItems
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
		   	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
    });
    
});


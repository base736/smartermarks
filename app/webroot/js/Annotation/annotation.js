/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as BezierFit from './bezier_fit.js?7.0';

const viewBoxSize = 100.0;

class ColourPicker {
    static $dialog = null;

    // State variables
    static _hue = 0;       // 0 to 1
    static _sat = 0;       // 0 to 1
    static _val = 0;       // 0 to 1
    static _penWidth = 3;

    static swatchColours = ['#000000', '#1255f3', '#ed201c', '#52b036', '#ffff54', '#ea33cf', '#bcfd53'];

    static initialize() {
        if (this.$dialog !== null) return;

        this.$dialog = $('#colourPicker_dialog');
        if (this.$dialog.length != 1) {
            throw new Error("Colour picker dialog not present");
        }
            
        this.$dialog.dialog({
            autoOpen: false,
            modal: false,
            dialogClass: false,
            width:180,
            height:250,
            zIndex: 50000,
            resizable: true,
            minWidth:180,
            minHeight:250,
            position: {
                my: "center center",
                at: "center center",
                of: window
            },
            open: function(event) {
                // Keep colour picker on top, and focus it
                $(this).closest(".ui-dialog").css('z-index', 50000);
                $(this).dialog("moveToTop");
                ColourPicker._updateInterface();
            }
        });
    
        // Bind event handlers
        this._bindEvents();
    }

    static open() {
        if (this.$dialog === null) this.initialize();
        this.$dialog.dialog('open');
    }

    static close() {
        if (this.$dialog !== null) {
            this.$dialog.dialog('close');
        }
    }

    static getColour() {
        return this._hsvToHex(this._hue, this._sat, this._val);
    }

    static setColour(value) {
        const hsv = this._hexToHsv(value);
        this._hue = hsv.h;
        this._sat = hsv.s;
        this._val = hsv.v;
        this._updateInterface();
    }

    static getPenWidth() {
        return Math.round(this._penWidth);
    }

    static setPenWidth(value) {
        if (value < 1.0) value = 1.0;
        if (value > 20.0) value = 20.0;
        this._penWidth = value;
        this._updateInterface();
    }

    // Attach pointer events using jQuery
    static _bindEvents() {

        // Drag on the colour area updates saturation and value
        const $colourArea = this.$dialog.find('.picker-colour-area');
        this._attachIndicatorHandlers($colourArea, (pos) => {
            this._sat = pos.x;
            this._val = 1.0 - pos.y;
            this._updateInterface();
        });
    
        // Drag on the hue slider updates hue
        const $hueSlider = this.$dialog.find('.picker-hue-slider');
        this._attachIndicatorHandlers($hueSlider, (pos) => {
            this._hue = pos.x;
            this._updateInterface();
        });
    
        // Drag on the width slider updates penWidth
        const $widthSlider = this.$dialog.find('.picker-width-slider');
        this._attachIndicatorHandlers($widthSlider, (pos) => {
            this._penWidth = pos.x * 19 + 1;
            this._updateInterface();
        });
    
        // Clicking a swatch updates the current colour (convert hex to HSV)
        const $swatchWrapper = this.$dialog.find('.picker-swatch-wrapper');
        $swatchWrapper.on('pointerdown.colourPicker', '.picker-swatch', (e) => {
            const $swatch = $(e.currentTarget);
            const colour = $swatch.attr('data-colour');
            this.setColour(colour);
        });
    }

    static _getIndicatorPosition(event, $wrapper) {
        const offset = $wrapper.offset();
        const width = $wrapper.width();
        const height = $wrapper.height();

        const orig = event.originalEvent;
        const pageX = orig.pageX || (orig.clientX + window.scrollX);
        const x = Math.min(Math.max((pageX - offset.left) / width, 0.0), 1.0);
        const pageY = orig.pageY || (orig.clientY + window.scrollY);
        const y = Math.min(Math.max((pageY - offset.top) / height, 0.0), 1.0);

        return { x, y };
    }
    
    // Click and drag behaviours for slider and area indicators
    static _attachIndicatorHandlers($elem, moveCallback) {
        $elem.on("pointerdown.colourPicker", (e) => {
            moveCallback(this._getIndicatorPosition(e, $elem));
            $(document).on("pointermove.colourPickerIndicator", (e2) => {
                moveCallback(this._getIndicatorPosition(e2, $elem));
            });
            $(document).on("pointerup.colourPickerIndicator pointercancel.colourPickerIndicator", (e2) => {
                $(document).off(".colourPickerIndicator");
            });
        });
    }
    
    static _updateInterface() {
        // Redraw colour area background. Three layers are used:
        // 1. A black-to-transparent gradient from bottom (value)
        // 2. A white-to-transparent gradient from left (saturation)
        // 3. The base hue colour.
        const bg = `
            linear-gradient(to top, rgba(0,0,0,1), rgba(0,0,0,0)),
            linear-gradient(to right, rgba(255,255,255,1), rgba(255,255,255,0)),
            hsl(${this._hue * 360}, 100%, 50%)
        `;
        const $colourArea = this.$dialog.find('.picker-colour-area');
        $colourArea.css("background", bg);

        // Update the colour area indicator position
        const areaWidth = $colourArea.width();
        const areaHeight = $colourArea.height();
        const x = this._sat * areaWidth;
        const y = (1 - this._val) * areaHeight;
        const $colourAreaIndicator = this.$dialog.find('.picker-colour-area-indicator');
        $colourAreaIndicator.css({ left: x, top: y });

        // Convert HSV to RGB and update the preview circle background
        const rgb = this._hsvToRgb(this._hue, this._sat, this._val);
        const r = Math.round(rgb[0]),
              g = Math.round(rgb[1]),
              b = Math.round(rgb[2]);
        const $previewCircle = this.$dialog.find('.picker-preview-circle');
        $previewCircle.css("background-color", `rgb(${r}, ${g}, ${b})`);
        
        // Update hue slider indicator: position based on current hue
        const $hueSlider = this.$dialog.find('.picker-hue-slider');
        const $hueIndicator = $hueSlider.find('.picker-slider-indicator');
        const huePos = (this._hue) * $hueSlider.width();
        $hueIndicator.css({
            left: huePos,
            backgroundColor: `hsl(${this._hue * 360}, 100%, 50%)`
        });
    
        // Update width slider indicator: position based on penWidth
        const $widthSlider = this.$dialog.find('.picker-width-slider');
        const $widthIndicator = $widthSlider.find('.picker-slider-indicator');
        const widthPos = ((this._penWidth - 1) / 19) * $widthSlider.width();
        $widthIndicator.css({
            left: widthPos,
            backgroundColor: '#000'
        });


        // Update swatches. Need to delay this so click handlers get a chance to
        // see the original swatch before it goes away.

        setTimeout(() => {
            let swatchesHTML = '';
            for (let swatchColour of this.swatchColours) {
                swatchesHTML += `<div class="picker-swatch" data-colour="${swatchColour}" 
                    style="background-color:${swatchColour};"></div>`;
            }
            const $swatches = this.$dialog.find('.picker-swatch-wrapper');
            $swatches.html(swatchesHTML);
        }, 0);
    }
  
    // Colour conversion helpers
    static _hsvToRgb(h, s, v) {
        const i = Math.floor(h * 6);
        const f = h * 6 - i;
        const p = v * (1 - s);
        const q = v * (1 - f * s);
        const t = v * (1 - (1 - f) * s);
        let r, g, b;
        switch (i % 6) {
            case 0: r = v; g = t; b = p; break;
            case 1: r = q; g = v; b = p; break;
            case 2: r = p; g = v; b = t; break;
            case 3: r = p; g = q; b = v; break;
            case 4: r = t; g = p; b = v; break;
            case 5: r = v; g = p; b = q; break;
        }
        return [r * 255, g * 255, b * 255];
    }  

    static _rgbToHsv(r, g, b) {
        r /= 255; g /= 255; b /= 255;
        const max = Math.max(r, g, b), min = Math.min(r, g, b);
        const d = max - min;
        let h, s = (max === 0 ? 0 : d / max);
        const v = max;
        if (max === min) {
            h = 0;
        } else {
            switch (max) {
                case r: h = (g - b) / d + (g < b ? 6 : 0); break;
                case g: h = (b - r) / d + 2; break;
                case b: h = (r - g) / d + 4; break;
            }
            h /= 6;
        }
        return { h, s, v };
    }

    static _hexToHsv(hex) {
        hex = hex.replace("#", "");
        if (hex.length === 3) {
            // Expand shorthand form (e.g. "03F") to full form ("0033FF")
            hex = hex.split("").map(c => c + c).join("");
        }
        const bigint = parseInt(hex, 16);
        const r = (bigint >> 16) & 255;
        const g = (bigint >> 8) & 255;
        const b = bigint & 255;
        return this._rgbToHsv(r, g, b);
    }

    static _hsvToHex(h, s, v) {
        const rgb = this._hsvToRgb(h, s, v);
        return '#' + rgb.map(value => {
            // Round the value, convert to hex, and pad with a leading zero if necessary
            return Math.round(value).toString(16).padStart(2, '0');
        }).join('');
    }
}

class DrawingTool {
    static fitPauseTime            = 500;  // Pause time to trigger fitted modes (ms)
    static vertexPauseTime         = 100;  // Pause time to flag a vertex (ms)
    static pauseMovementThreshold  = 3.0;  // Movement threshold (pixels) within fitPauseTime to trigger pause logic
    static polylineTolerance       = 10.0; // Max allowed distance from a candidate segment
    static collapseThresholdFactor = 0.1;  // Multiple of the maximum gap allowed in candidate polylines
    static snapAngleRadians        = 0.05; // Snap if the line is within this angle (radians) of horizontal or vertical
    static bezierError             = 3.0;  // Maximum distance from drawn points when fitting bezier curve

    constructor({name, iconURL, description, defaults}) {
        this.name         = name;
        this.iconURL      = iconURL;
        this.description  = description;
        this.defaults     = defaults;

        this.points = [];
        this.currentPath = null;
        this.drawingMode = null; // "freehand" (default), "line", or "polygon"
        this.checkPause = false;
        this.pauseTimeout = null;
        this.parentElement = null;
    }

    _pixelsToSVG(x, y) {
        if (this.parentElement.classList.contains('html_drawing')) {
            const rect = this.parentElement.getBoundingClientRect();
            x *= viewBoxSize / rect.width;
            y *= viewBoxSize / rect.height;
        }
        return `${x.toFixed(1)} ${y.toFixed(1)}`;
    }

    /** Called at the very start of drawing. */
    startDrawing(initPos, parentElement) {
        // Clear old references and initialize
        this.points = [{ x: initPos.x, y: initPos.y, t: Date.now() }];
        this.currentPath = null;
        this.drawingMode = 'freehand';
        this.checkPause = false;
        if (this.pauseTimeout) {
            clearTimeout(this.pauseTimeout);
            this.pauseTimeout = null;
        }
        this.parentElement = parentElement;

        // Ensure we have an <svg> to draw in (specific to this tool)
        let targetSVG = parentElement.querySelector(`:scope > .annotation_wrapper[data-tool-name="${this.name}"]`);
        if (!targetSVG) {
            targetSVG = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            if (parentElement.classList.contains('html_drawing')) {
                targetSVG.setAttribute('viewBox', `0 0 ${viewBoxSize} ${viewBoxSize}`);
                targetSVG.setAttribute('preserveAspectRatio', 'none');
            }
            targetSVG.setAttribute('data-tool-name', this.name);
            targetSVG.classList.add('annotation_wrapper');
            parentElement.appendChild(targetSVG);
        }

        // Create a new path element
        const pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        pathElement.setAttribute('stroke', ColourPicker.getColour());
        pathElement.setAttribute('stroke-width', ColourPicker.getPenWidth());
        pathElement.setAttribute('d', `M ${this._pixelsToSVG(initPos.x, initPos.y)}`);
        targetSVG.appendChild(pathElement);
        this.currentPath = pathElement;

        this.defaults = {
            stroke: ColourPicker.getColour(),
            strokeWidth: ColourPicker.getPenWidth()
        }
    }

    // ---------------- Helper Functions ----------------

    _distanceBetweenPoints(a, b) {
        const dx = a.x - b.x;
        const dy = a.y - b.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    _pointToSegmentDistance(pt, A, B) {
        let dx = B.x - A.x;
        let dy = B.y - A.y;
        if (dx === 0 && dy === 0) {
            return this._distanceBetweenPoints(pt, A);
        }
        let t = ((pt.x - A.x) * dx + (pt.y - A.y) * dy) / (dx * dx + dy * dy);
        t = Math.max(0, Math.min(1, t));
        const projX = A.x + t * dx;
        const projY = A.y + t * dy;
        return this._distanceBetweenPoints(pt, { x: projX, y: projY });
    }

    _tryLineFit() {
        if (this.points.length < 2) return false;
        const start = this.points[0];
        const end = this.points[this.points.length - 1];
        for (let i = 1; i < this.points.length - 1; i++) {
            const pt = this.points[i];
            if (this._pointToSegmentDistance(pt, start, end) > DrawingTool.polylineTolerance) {
                return false;
            }
        }
        this.currentPath.setAttribute('d', 
            `M ${this._pixelsToSVG(start.x, start.y)} L ${this._pixelsToSVG(end.x, end.y)}`);
        this.points = [start, end];
        this.drawingMode = 'line';
        return true;
    }

    _collapseCandidateVertices(candidates) {
        if (candidates.length < 2) return candidates;
        let maxDist = 0;
        for (let i = 0; i < candidates.length - 1; i++) {
            const d = this._distanceBetweenPoints(candidates[i], candidates[i+1]);
            if (d > maxDist) maxDist = d;
        }
        const threshold = DrawingTool.collapseThresholdFactor * maxDist;
        let collapsed = [];
        let group = [candidates[0]];
        for (let i = 1; i < candidates.length; i++) {
            let sumX = 0, sumY = 0;
            for (const p of group) {
                sumX += p.x;
                sumY += p.y;
            }
            const avgX = sumX / group.length;
            const avgY = sumY / group.length;
            const avgPoint = { x: avgX, y: avgY };
            if (this._distanceBetweenPoints(candidates[i], avgPoint) < threshold) {
                group.push(candidates[i]);
            } else {
                collapsed.push({ x: avgX, y: avgY, t: group[0].t });
                group = [candidates[i]];
            }
        }
        if (group.length > 0) {
            let sumX = 0, sumY = 0;
            for (const p of group) {
                sumX += p.x;
                sumY += p.y;
            }
            collapsed.push({ x: sumX / group.length, y: sumY / group.length, t: group[0].t });
        }
        return collapsed;
    }

    _tryPolylineFit() {
        let candidateVertices = [];
        for (let i = 0; i < this.points.length; ++i) {
            if (this._isPaused(i, DrawingTool.vertexPauseTime)) {
                candidateVertices.push(this.points[i]);
            }
        }
        candidateVertices.push(this.points[this.points.length - 1]);

        candidateVertices = this._collapseCandidateVertices(candidateVertices);
        if (candidateVertices.length < 3) return false;
        for (let j = 0; j < candidateVertices.length - 1; j++) {
            const A = candidateVertices[j];
            const B = candidateVertices[j + 1];
            const segmentPoints = this.points.filter(pt => pt.t >= A.t && pt.t <= B.t);
            for (const pt of segmentPoints) {
                const d = this._pointToSegmentDistance(pt, A, B);
                if (d > DrawingTool.polylineTolerance) {
                    return false;
                }
            }
        }
        let closePolygon = false;
        const firstVertex = candidateVertices[0];
        const lastVertex = candidateVertices[candidateVertices.length - 1];
        if (this._distanceBetweenPoints(firstVertex, lastVertex) <= DrawingTool.polylineTolerance) {
            candidateVertices[0] = { 
                x: (firstVertex.x + lastVertex.x) / 2.0, 
                y: (firstVertex.y + lastVertex.y) / 2.0, 
                t: firstVertex.t 
            };
            candidateVertices.pop();
            closePolygon = true;
        }
        this.points = candidateVertices;
        let pathString = `M ${this._pixelsToSVG(candidateVertices[0].x, candidateVertices[0].y)}`;
        for (let i = 1; i < candidateVertices.length; i++) {
            pathString += ` L ${this._pixelsToSVG(candidateVertices[i].x, candidateVertices[i].y)}`;
        }
        if (closePolygon) {
            pathString += " Z";
            this.drawingMode = 'polygon';
        } else {
            this.drawingMode = 'line';
        }
        this.currentPath.setAttribute('d', pathString);
        return true;
    }

    _isPaused(index, pauseTime) {
        let isPaused = true;

        const pEnd = this.points[index];
        for (let i = index - 1; i >= 0; --i) {
            const pCheck = this.points[i];
            if (pEnd.t - pCheck.t > pauseTime) break;
            const distance = Math.hypot(pCheck.x - pEnd.x, pCheck.y - pEnd.y);
            if (distance > DrawingTool.pauseMovementThreshold) isPaused = false;
        }
        return isPaused;
    }

    _tryFits() {
        const fitFunctions = [
            () => this._tryLineFit(),
            () => this._tryPolylineFit()
        ];
        for (let fitFunction of fitFunctions) {
            if (fitFunction()) break;
        }
    }

    /** Called as the pointer moves, to append new points and update the rough path. */
    appendDrawingPoint(newPos) {
        if (!this.currentPath) return;

        if (this.drawingMode === 'freehand') {
            // Cancel any existing pause timer
            if (this.pauseTimeout) clearTimeout(this.pauseTimeout);

            const now = Date.now();
            // Append the new point
            this.points.push({ x: newPos.x, y: newPos.y, t: now });

            // Update the SVG path
            if (this.points.length === 1) {
                this.currentPath.setAttribute('d', `M ${this._pixelsToSVG(newPos.x, newPos.y)}`);
            } else {
                const d = this.currentPath.getAttribute('d');
                const newD = `${d} L ${this._pixelsToSVG(newPos.x, newPos.y)}`;
                this.currentPath.setAttribute('d', newD);
            }

            // Check if pointer has paused
            let isPaused = this._isPaused(this.points.length - 1, DrawingTool.fitPauseTime);

            if (!isPaused) this.checkPause = true;            
            if (this.checkPause) {
                if (isPaused) {
                    // If pointer has paused, try fitted modes
                    this._tryFits();
                } else {
                    // Otherwise, set a timer in case we stop moving entirely
                    this.pauseTimeout = setTimeout(() => {
                        this._tryFits();
                    }, DrawingTool.fitPauseTime);        
                }
            }
        } else if (this.drawingMode === 'line') {
            const start = this.points[this.points.length - 2];
            const dx = newPos.x - start.x;
            const dy = newPos.y - start.y;
            const angle = Math.atan2(Math.abs(dy), Math.abs(dx));
            const snapTol = DrawingTool.snapAngleRadians;
            if (angle < snapTol) {
                newPos.y = start.y;
            } else if ((Math.PI / 2 - angle) < snapTol) {
                newPos.x = start.x;
            }
            this.points[this.points.length - 1] = { x: newPos.x, y: newPos.y, t: Date.now() };

            let pathString = `M ${this._pixelsToSVG(this.points[0].x, this.points[0].y)}`;
            for (let i = 1; i < this.points.length; i++) {
                pathString += ` L ${this._pixelsToSVG(this.points[i].x, this.points[i].y)}`;
            }
            this.currentPath.setAttribute('d', pathString);
        } else if (this.drawingMode === 'polygon') {
            // No updates after a polygon stroke is finished.
        }
    }

    /**
     * Called when drawing is finished (pointer up).
     * For freehand mode, we fit a Bézier; for line or polygon modes we leave the path as is.
     */
    finishDrawing() {
        if (!this.currentPath) return;
        if (this.pauseTimeout) {
            clearTimeout(this.pauseTimeout);
            this.pauseTimeout = null;
        }
        if (this.drawingMode === 'freehand') {
            const convertedPoints = this.points.map(point => [point.x, point.y]);
            const beziers = BezierFit.fitCurve(convertedPoints, DrawingTool.bezierError);
            for (let i = 0; i < beziers.length; i++) {
                for (let j = 0; j < 4; ++j) {
                    beziers[i][j] = { x: beziers[i][j][0], y: beziers[i][j][1] };
                }
            }
            let pathString = '';
            if (beziers.length > 0) {
                pathString = `M ${this._pixelsToSVG(beziers[0][0].x, beziers[0][0].y)}`;
                for (let i = 0; i < beziers.length; i++) {
                    const b = beziers[i];
                    pathString += ` C ${this._pixelsToSVG(b[1].x, b[1].y)}, ${this._pixelsToSVG(b[2].x, b[2].y)}, ${this._pixelsToSVG(b[3].x, b[3].y)}`;
                }
            }
            this.currentPath.setAttribute('d', pathString);
        }
        this.points = [];
        this.currentPath = null;
        this.drawingMode = null;
    }

    discardDrawing() {
        if (this.currentPath) {
            const parentSVG = this.currentPath.parentNode;
            parentSVG.removeChild(this.currentPath);
        }
        this.points = [];
        this.currentPath = null;
        this.drawingMode = null;
        if (this.pauseTimeout) {
            clearTimeout(this.pauseTimeout);
            this.pauseTimeout = null;
        }
    }
}

class EraserTool {
    static eraseDistance = 5.0;
    
    constructor() {
        this.name           = 'eraser';
        this.iconURL        = '/img/icons/annotation_eraser.svg';
        this.description    = 'Eraser';
        this.erasedElements = new Set();
    }

    startDrawing(initPos, parentElement) {
        this.erasedElements = new Set();
        this.parentElement = parentElement;
    }

    appendDrawingPoint(newPos) {
        let scaleX = 1, scaleY = 1;
        if (this.parentElement.classList.contains('html_drawing')) {
            const rect = this.parentElement.getBoundingClientRect();
            scaleX = rect.width / viewBoxSize;
            scaleY = rect.height / viewBoxSize;
        }

        const paths = this.parentElement.querySelectorAll(':scope > .annotation_wrapper path');
        for (const pathElement of paths) {
            if (this.erasedElements.has(pathElement)) continue;

            const bbox = pathElement.getBBox(); // in SVG coordinates

            // Convert bbox to pixel coordinates
            const pixelBbox = {
                x: bbox.x * scaleX,
                y: bbox.y * scaleY,
                width: bbox.width * scaleX,
                height: bbox.height * scaleY
            };
            const half = EraserTool.eraseDistance / 2;
            const expandedBox = {
                x:      pixelBbox.x - half,
                y:      pixelBbox.y - half,
                width:  pixelBbox.width + EraserTool.eraseDistance,
                height: pixelBbox.height + EraserTool.eraseDistance
            };
            if (!this._pointInBox(newPos, expandedBox)) continue;

            if (this._pointHitsPath(pathElement, newPos, scaleX, scaleY)) {
                this.erasedElements.add(pathElement);
                pathElement.setAttribute('stroke', 'grey');
            }
        }
    }

    _pointInBox(point, box) {
        return (
            point.x >= box.x &&
            point.x <= (box.x + box.width) &&
            point.y >= box.y &&
            point.y <= (box.y + box.height)
        );
    }
    
    _pointHitsPath(pathElement, newPos, scaleX, scaleY) {
        const length = pathElement.getTotalLength();
        const step   = EraserTool.eraseDistance / 2.0;
        for (let dist = 0; dist <= length; dist += step) {
            const ptSVG = pathElement.getPointAtLength(dist); // in SVG coordinates
            const pt = { x: ptSVG.x * scaleX, y: ptSVG.y * scaleY }; // convert to pixels
            const dx = pt.x - newPos.x;
            const dy = pt.y - newPos.y;
            if ((dx * dx + dy * dy) <= (EraserTool.eraseDistance * EraserTool.eraseDistance)) {
                return true;
            }
        }
        return false;
    }

    finishDrawing() {
        this.erasedElements.forEach(el => {
            el.parentNode?.removeChild(el);
        });
        this.erasedElements.clear();
    }

    discardDrawing() { }
}

class Annotation {
    static tools = {
        pen: new DrawingTool({
            name: 'pen', 
            iconURL: '/img/icons/annotation_pen.svg', 
            description: 'Pen', 
            defaults: {
                stroke: '#000000',
                strokeWidth: 3
            }
        }),
        highlighter: new DrawingTool({
            name: 'highlighter', 
            iconURL: '/img/icons/annotation_highlighter.svg', 
            description: 'Highlighter',
            defaults: {
                stroke: '#ffff00',
                strokeWidth: 15
            }
        }),
        eraser: new EraserTool()
    };

    constructor(parent, saveCallback) {
        this.parent         = parent;
        this.saveCallback   = saveCallback;
        this.drawingInitPos = false;
        this.drawingPointerID = null;
        this.isDrawing      = false;
        this.activeTool = null;

        this._listeners = [
            { type: 'pointerdown',   handler: (e) => this._initDrawing(e), options: { passive: false } },
            { type: 'pointermove',   handler: (e) => this._draw(e), options: { passive: false } },
            { type: 'pointerup',     handler: (e) => this._stopDrawing(e) },
            { type: 'pointercancel', handler: (e) => this._stopDrawing(e) },
            { type: 'pointerleave',  handler: (e) => this._stopDrawing(e) }
        ];
        this._initListeners();
    }

    _initListeners() {
        this._listeners.forEach(({ type, handler, options }) => {
            this.parent.addEventListener(type, handler, options || false);
        });
    }

    destroy() {
        this._listeners.forEach(({ type, handler, options }) => {
            this.parent.removeEventListener(type, handler, options || false);
        });
    }

    setTool(toolName) {
        if (toolName in Annotation.tools) {
            this.activeTool = Annotation.tools[toolName];
        } else {
            this.activeTool = null;
        }
    }

    _getPointerPosition(event) {
        const rect = this.parent.getBoundingClientRect();
        let x = event.clientX - rect.left;
        let y = event.clientY - rect.top;
        return { x, y };
    }

    _initDrawing(event) {
        if (!this.activeTool) return;
        event.preventDefault();

        this.drawingInitPos = this._getPointerPosition(event);
        this.isDrawing = false;
        this.drawingPointerID = event.pointerId;
    }

    _draw(event) {
        if (this.drawingPointerID === null) return;
        if (event.pointerId !== this.drawingPointerID) return;
        event.preventDefault();

        const pos = this._getPointerPosition(event);
        if (!this.isDrawing) {
            this.isDrawing = true;
            if (this.activeTool.startDrawing) {
                this.activeTool.startDrawing(this.drawingInitPos, this.parent);
            }
        }
        if (this.activeTool.appendDrawingPoint) {
            this.activeTool.appendDrawingPoint(pos);
        }
    }

    _stopDrawing(event) {
        if (this.drawingPointerID !== event.pointerId) return;
        event.preventDefault();

        this.drawingInitPos    = false;
        this.drawingPointerID  = null;
        if (!this.isDrawing) return;
        if (this.activeTool.finishDrawing) {
            this.activeTool.finishDrawing();
        }
        this.isDrawing = false;
        this.saveCallback(this.parent);
    }

    _discardCurrentStroke() {
        if (this.activeTool && this.activeTool.discardDrawing) {
            this.activeTool.discardDrawing();
        }
        this.drawingInitPos   = false;
        this.drawingPointerID = null;
        this.isDrawing        = false;
    }
}

export { ColourPicker, Annotation }
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

const FontMetrics = {
	symbolCharacters: [
	
		['00a0', 'MathJax_Main', 'symbol', 'nbsp'], 
		['2013', 'MathJax_Main', 'symbol', 'endash'], 
		['2014', 'MathJax_Main', 'symbol', 'emdash'], 

		['00ab', 'MathJax_Main', 'symbol', 'gleft'], 
		['00bb', 'MathJax_Main', 'symbol', 'gright'], 

		['007c', 'MathJax_Main', 'operator', 'suchthat'], 

		// Common relations and operators
		['00b0', 'MathJax_Main', 'symbol', 'degree'], 
		['00b1', 'MathJax_Main', 'operator', 'pm'], 
		['2213', 'MathJax_Main', 'operator', 'mp'], 
		['00d7', 'MathJax_Main', 'operator', 'times'], 
		['00b7', 'MathJax_Main', 'operator', 'cdot'], 
		['00f7', 'MathJax_Main', 'operator', 'div'], 
		['2218', 'MathJax_Main', 'operator', 'circ'], 
		['2264', 'MathJax_Main', 'operator', 'leq'], 
		['2265', 'MathJax_Main', 'operator', 'geq'], 
		['2260', 'MathJax_Main', 'operator', 'ne'], 
		['2261', 'MathJax_Main', 'operator', 'equiv'], 
		['221d', 'MathJax_Main', 'operator', 'propto'], 
		['223c', 'MathJax_Main', 'operator', 'sim'], 
		['2248', 'MathJax_Main', 'operator', 'approx'], 
		['2245', 'MathJax_Main', 'operator', 'cong'],
						
		// Lowercase Greek
		['03b1', 'MathJax_MathItalic', 'symbol', 'alpha'], 
		['03b2', 'MathJax_MathItalic', 'symbol', 'beta'], 
		['03b3', 'MathJax_MathItalic', 'symbol', 'gamma'], 
		['03b4', 'MathJax_MathItalic', 'symbol', 'delta'], 
		['03b5', 'MathJax_MathItalic', 'symbol', 'epsilon'], 
		['03b6', 'MathJax_MathItalic', 'symbol', 'zeta'], 
		['03b7', 'MathJax_MathItalic', 'symbol', 'eta'], 
		['03b8', 'MathJax_MathItalic', 'symbol', 'theta'], 
		['03b9', 'MathJax_MathItalic', 'symbol', 'iota'], 
		['03ba', 'MathJax_MathItalic', 'symbol', 'kappa'], 
		['03bb', 'MathJax_MathItalic', 'symbol', 'lambda'], 
		['03bc', 'MathJax_MathItalic', 'symbol', 'mu'], 
		['03bd', 'MathJax_MathItalic', 'symbol', 'nu'], 
		['03be', 'MathJax_MathItalic', 'symbol', 'xi'], 
		['03c0', 'MathJax_MathItalic', 'symbol', 'pi'], 
		['03c1', 'MathJax_MathItalic', 'symbol', 'rho'], 
		['03c3', 'MathJax_MathItalic', 'symbol', 'sigma'], 
		['03c4', 'MathJax_MathItalic', 'symbol', 'tau'], 
		['03c5', 'MathJax_MathItalic', 'symbol', 'upsilon'], 
		['03c6', 'MathJax_MathItalic', 'symbol', 'phi'], 
		['03c7', 'MathJax_MathItalic', 'symbol', 'chi'], 
		['03c8', 'MathJax_MathItalic', 'symbol', 'psi'], 
		['03c9', 'MathJax_MathItalic', 'symbol', 'omega'], 
						
		// Uppercase Greek
		['0393', 'MathJax_Main', 'symbol', 'Gamma'], 
		['0394', 'MathJax_Main', 'symbol', 'Delta'], 
		['0398', 'MathJax_Main', 'symbol', 'Theta'], 
		['039b', 'MathJax_Main', 'symbol', 'Lambda'], 
		['039e', 'MathJax_Main', 'symbol', 'Xi'], 
		['03a0', 'MathJax_Main', 'symbol', 'Pi'], 
		['03a3', 'MathJax_Main', 'symbol', 'Sigma'], 
		['03a5', 'MathJax_Main', 'symbol', 'Upsilon'], 
		['03a6', 'MathJax_Main', 'symbol', 'Phi'], 
		['03a8', 'MathJax_Main', 'symbol', 'Psi'], 
		['03a9', 'MathJax_Main', 'symbol', 'Omega'],
						
		// Arrows
		['2190', 'MathJax_Main', 'operator', 'leftarrow'], 
		['21d0', 'MathJax_Main', 'operator', 'Leftarrow'], 
		['2192', 'MathJax_Main', 'operator', 'rightarrow'], 
		['21d2', 'MathJax_Main', 'operator', 'Rightarrow'], 
		['2194', 'MathJax_Main', 'operator', 'leftrightarrow'], 
		['21d4', 'MathJax_Main', 'operator', 'Leftrightarrow'], 
		['21bc', 'MathJax_Main', 'operator', 'leftharpoonup'], 
		['21bd', 'MathJax_Main', 'operator', 'leftharpoondown'], 
		['21c0', 'MathJax_Main', 'operator', 'rightharpoonup'], 
		['21c1', 'MathJax_Main', 'operator', 'rightharpoondown'], 
		['21cc', 'MathJax_Main', 'operator', 'rightleftharpoons'], 
		['21cb', 'MathJax_Main', 'operator', 'leftrightharpoons'], 
		['2794', 'MathJax_Main', 'operator', 'heavyrightarrow'],
						
		// Other symbols
		['2220', 'MathJax_Main', 'symbol', 'angle'],

		['2205', 'MathJax_Main', 'symbol', 'emptyset'],
		['2208', 'MathJax_Main', 'operator', 'elem'],
		['2209', 'MathJax_Main', 'operator', 'notelem'],
		['2229', 'MathJax_Main', 'operator', 'cap'],
		['222a', 'MathJax_Main', 'operator', 'cup'],
		['2283', 'MathJax_Main', 'operator', 'supset'],
		['2282', 'MathJax_Main', 'operator', 'subset'],

		['2b1a', 'MathJax_Main', 'symbol', 'dashbox'],
		['25a1', 'MathJax_Main', 'symbol', 'whitesquare'],
		['25a0', 'MathJax_Main', 'symbol', 'blacksquare'],

		['221e', 'MathJax_Main', 'symbol', 'infty']
	],

	fontSizes: ["fontSizeSmallest", "fontSizeSmaller", "fontSizeNormal"],
	fontStyles: ["MathJax_MathItalic", "MathJax_Main", "MathJax_MainItalic"],

	// Lists all characters which need to be rendered in a normal font.
	MathJax_Main: [
		'0', '1', '2', '3', '4', '5', '6', '7', '8', 
		'9', '−', '*',
		'-', '=', '+', '/', '<', '>', '∞', '%', '!', '$', '.', '(', ')', '[', 
		']', '{', '}', '∂', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 
		'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 
		's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A',
		'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
		'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
		'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '→', '^',
		'˙', '∈', '⃗', '¯', '◦',
		'∧', '∨', '∖',
		'≺', '⪯', '⊂', '⊆', '≻',
		'⪰', '⊥', '∣', '∥', ':',
		'′', ' '
	],
	MathJax_MainItalic: ['ı', 'ȷ'],
	// Lists all characters which need to be rendered in an italic font. 
	MathJax_MathItalic: [
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 
		'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 
		's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A',
		'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
		'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
		'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Γ', 
		'ς', '\''
	],
	MathJax_Size1: [],
	MathJax_Size2: [],
	MathJax_Size3: ['(', ')', '{', '}', '[', ']'],
	MathJax_Size4: [
		'(', ')', '{', '}', '[', ']', '⎛',
		'⎜', '⎝', '⎞', '⎟',
		'⎠', '⎧', '⎪', '⎨', 
		'⎩', '⎫', '⎪', '⎬', 
		'⎭', '⎡', '⎢', '⎣', 
		'⎤', '⎥', '⎦'
	],
						
	// This will be the union of the characters in each font style.
	// Can be used to see if a character is allowed to be part of an equation.
	shortCharacters: [
		'a', 'c', 'e', 'g', 'ı', 'ȷ', 'm',
		'n', 'o', 'p', 'q', 'r', 's', 'u', 'v', 'w', 'x', 'y', 'z',
		'α', 'γ', 'ε', 'ϵ', 'η', 'ι',
		'κ', 'μ', 'ν', 'π', 'ϖ', 'ρ',
		'ϱ', 'σ', 'ς','τ', 'υ', 'φ', 
		'χ', 'ω'
	],
	mediumCharacters: ['i', 'j','t'],
	tallCharacters: [
		'b', 'd', 'f', 'h', 'k', 'l', 'A', 'B', 'C', 'D', 'E', 'F',
		'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
		'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'β', 'δ',
		'ζ', 'θ', 'ϑ', 'λ', 'ξ', 'ϕ',
		'ψ', 'Γ', 'Δ', 'Θ', 'Λ', 'Ξ',
		'Π', 'Σ', 'Υ', 'Φ', 'Ψ', 'Ω'
	],
	charWidthExceedsBoundingBox: {
		'b': 0.01,
		'c': 0.01,
		'd': 0.01,
		'f': 0.15,
		'g': 0.01,
		'q': 0.075,
		'y': 0.01,
		'z': 0.01,
		'B': 0.01,
		'C': 0.0875,
		'E': 0.0875,
		'F': 0.2,
		'H': 0.1,
		'I': 0.175,
		'J': 0.15,
		'K': 0.075,
		'M': 0.075,
		'N': 0.125,
		'P': 0.175,
		'R': 0.02,
		'S': 0.04,
		'T': 0.25,
		'U': 0.15,
		'V': 0.33,
		'W': 0.13,
		'X': 0.075,
		'Y': 0.33,
		'Z': 0.08,
		'β': 0.05,
		'γ': 0.05,
		'δ': 0.05,
		'ζ': 0.075,
		'η': 0.05,
		'θ': 0.025,
		'ν': 0.1,
		'ξ': 0.025,
		'π': 0.025,
		'ϖ': 0.025,
		'ρ': 0.025,
		'ϱ': 0.025,
		'σ': 0.015,
		'ς': 0.15,
		'τ': 0.185,
		'υ': 0.015,
		'ψ': 0.015,
		'ı': 0.15,
		'ȷ': 0.15,
		'|': 0.5,
		',': 1.0
	},

	height: {},
	getHeight(size) {
		if (typeof FontMetrics.height[size] === 'undefined') {
			// Create an invisible element with the desired class to inspect its font-size.
			const $inspector = $("<div>").css("display", "none").addClass(size);
			$("body").append($inspector);
			try {
				FontMetrics.height[size] = parseInt($inspector.css("font-size"), 10);
			} finally {
				$inspector.remove();
			}
		}
		return FontMetrics.height[size];
	},

	fontTestIndex: 0,
	width: {},
	getWidth(character, style, size) {
		let hasEntry = true;
		if (typeof FontMetrics.width[character] === 'undefined') {
			FontMetrics.width[character] = {};
			hasEntry = false;
		}
		if (typeof FontMetrics.width[character][style] === 'undefined') {
			FontMetrics.width[character][style] = {};
			hasEntry = false;
		}
		if (typeof FontMetrics.width[character][style][size] === 'undefined') {
			FontMetrics.width[character][style][size] = {};
			hasEntry = false;
		}
		
		if (!hasEntry) {
			var divName = "fontTest_" + (FontMetrics.fontTestIndex++);
			
			var extraWidth;
			if ((style === 'MathJax_Main') && (character != ',')) extraWidth = 0;
			else if (typeof FontMetrics.charWidthExceedsBoundingBox[character] !== "undefined") 
				extraWidth = FontMetrics.charWidthExceedsBoundingBox[character];
			else extraWidth = 0;

			var insertedCharacter;
			if (character == ' ') insertedCharacter = '-';
			else insertedCharacter = character;
			
			$('body').append('<div id="' + divName + '" class="' + size + ' ' + style + ' eqEdEquation">' + insertedCharacter + '</div>');
			FontMetrics.width[character][style][size] = $(`#${divName}`).outerWidth() * (1 + extraWidth);
			$(`#${divName}`).remove();
		}
		
		return FontMetrics.width[character][style][size];
	}
};
						
// Append characters from symbolCharacters into the appropriate font style array.
for (let i = 0; i < FontMetrics.symbolCharacters.length; ++i) {
	const character = String.fromCharCode(
	  	parseInt("0x" + FontMetrics.symbolCharacters[i][0])
	);
	FontMetrics[FontMetrics.symbolCharacters[i][1]].push(character);
}
  
export default FontMetrics;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationComponent from './components/misc/equationComponent.js?7.0';
import EquationDom from './components/dom/equationDom.js?7.0';
import TopLevelContainer from './components/containers/topLevelContainer.js?7.0';
import Wrapper from "./components/wrappers/wrapper.js?7.0";
import Property from './property.js?7.0';

// Trigger registration of active wrappers
import './components/wrappers/index.js?7.0';

class Equation extends EquationComponent {
    constructor() {
        super();
        this.className = 'Equation';

        // To allow uniform determination of equation for components
        this.equation = this;

        // Create the top-level container for the equation.
        this.topLevelContainer = new TopLevelContainer(this);

        // Build the DOM object and append the topLevelContainer.
        this.domObj = this.buildDomObj();
        this.domObj.append(this.topLevelContainer.domObj);

        this.fontSize = "fontSizeNormal";
        this.children = [this.topLevelContainer];

        // Define computed properties for width and height.
        let width = 0;
        this.properties.push(new Property(this, "width", width, {
            get() {
                return width;
            },
            set(value) {
                width = value;
            },
            compute() {
                return this.topLevelContainer.width;
            },
            updateDom() {
                this.domObj.updateWidth(this.width);
            }
        }));

        let height = 0;
        this.properties.push(new Property(this, "height", height, {
            get() {
                return height;
            },
            set(value) {
                height = value;
            },
            compute() {
                return this.topLevelContainer.height;
            },
            updateDom() {
                this.domObj.updateHeight(this.height);
            }
        }));
    }

    buildDomObj() {
        return new EquationDom(
            this,
            '<div class="eqEdEquation" contenteditable="false" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></div>'
        );
    }

    clone() {
        return new this.constructor();
    }

    buildJsonObj() {
        return {
            type: "Equation",
            operands: {
                topLevelContainer: this.topLevelContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj) {
        const equation = new Equation();
        // Assume jsonObj.operands.topLevelContainer is an array of wrapper JSON objects.
        for (let i = 0; i < jsonObj.operands.topLevelContainer.length; i++) {
            const innerWrapper = Wrapper.wrapperFromJsonObj(
                jsonObj.operands.topLevelContainer[i],
                equation
            )
            equation.topLevelContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        }
        return equation;
    }
}

export default Equation;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class EquationDom {
    constructor(binding, html) {
        this.className = "EquationDom";
        this.binding = binding;
        this.html = html;
        this.value = $(html);
        this.value.data("eqObject", this.binding);
        this.width = 0;
        this.height = 0;
        this.left = 0;
        this.top = 0;

        this.value.focus();
        this.value.blur();
    }

    updateWidth(width) {
        this.width = width;
        this.value.css("width", width + "px");
    }

    updateHeight(height) {
        this.height = height;
        this.value.css("height", height + "px");
    }

    updateLeft(left) {
        this.left = left;
        this.value.css("left", left + "px");
    }

    updateTop(top) {
        this.top = top;
        this.value.css("top", top + "px");
    }

    clone() {
        const copy = new EquationDom(this.binding, this.html);
        copy.updateWidth(this.width);
        copy.updateHeight(this.height);
        copy.updateLeft(this.left);
        copy.updateTop(this.top);
        return copy;
    }

    append(domObject) {
        this.value.append(domObject.value);
    }

    empty() {
        this.value.empty();
    }

    addClass(className) {
        this.value.addClass(className);
    }

    updateFontSize(fontClass) {
        // Remove all known font size classes before adding the new one.
        this.value.removeClass("fontSizeNormal fontSizeSmaller fontSizeSmallest");
        this.value.addClass(fontClass);
    }

    updateBorderWidth(borderWidth) {
        this.value.css("border-width", borderWidth + "px");
    }
}

export default EquationDom;
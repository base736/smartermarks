/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "./equationDom.js?7.0";

class ContainerDom extends EquationDom {
    constructor(binding, html) {
        super(binding, html);
        this.className = "ContainerDom";
    }

    addWrapper(index, wrapper) {
        const element = wrapper.domObj.value;
        const children = this.value.children();

        let lastIndex = children.length;
        if (index < 0) {
            index = Math.max(0, lastIndex + 1 + index);
        }

        this.value.append(element);

        if (index < lastIndex) {
            // Re-fetch children as the DOM has been updated.
            this.value.children().eq(index).before(this.value.children().last());
        }
        return this.value;
    }

    removeWrapper(index) {
        this.value.children().eq(index).remove();
    }

    updateClipping(clipping) {
        // this.value.css('clip', 'rect(' + clipping + ')');
    }
}

export default ContainerDom;
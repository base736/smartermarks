/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Container from "./container.js?7.0";
import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SquareEmptyContainerWrapper from "../wrappers/squareEmptyContainerWrapper.js?7.0";

class StackedFractionDenominatorContainer extends Container {
    constructor(parent) {
        super(parent);
        this.className = "StackedFractionDenominatorContainer";
        this.domObj = this.buildDomObj();
        const squareEmptyContainerWrapper = new SquareEmptyContainerWrapper(this.equation);
        this.addWrappers([{
            index: 0,
            wrapper: squareEmptyContainerWrapper
        }]);

        this.padBottom = 0.025;
        this.padTop = 0.025;

        // padTop property
        let padTop = 0;
        this.properties.push(
            new Property(this, "padTop", padTop, {
                get() {
                    return padTop;
                },
                set(value) {
                    padTop = value;
                },
                compute() {
                    let padTopVal = 0.025;
                    let hasRoot = false;
                    for (let i = 0; i < this.wrappers.length; i++) {
                        if (
                            this.wrappers[i].className == 'SquareRootWrapper' ||
                            this.wrappers[i].className == 'NthRootWrapper'
                        ) {
                            hasRoot = true;
                            break;
                        }
                    }
                    if (hasRoot) {
                        padTopVal = 0.1;
                    }
                    return padTopVal;
                },
                updateDom() {}
            })
        );

        // left property
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const maxNumDenomWidth =
                        this.width > this.parent.stackedFractionNumeratorContainer.width
                            ? this.width
                            : this.parent.stackedFractionNumeratorContainer.width;
                    return (
                        0.5 * (maxNumDenomWidth - this.width) +
                        0.5 * this.parent.stackedFractionHorizontalBar.exceedsMaxNumDenomWidth * fontHeight
                    );
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // top property
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    return (
                        this.parent.stackedFractionNumeratorContainer.height +
                        this.parent.stackedFractionHorizontalBar.height
                    );
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // fontSize property
        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() {
                    return fontSize;
                },
                set(value) {
                    fontSize = value;
                },
                compute() {
                    let fontSizeVal = "";
                    let actualParentContainer = this.parent.parent;
                    while (actualParentContainer?.className == 'BracketContainer') {
                        actualParentContainer = actualParentContainer.parent.parent;
                    }
                    if (
                        actualParentContainer.fontSize === "fontSizeSmaller" ||
                        actualParentContainer.fontSize === "fontSizeSmallest"
                    ) {
                        fontSizeVal = "fontSizeSmallest";
                    } else {
                        if (actualParentContainer.parent.className == 'StackedFractionWrapper') {
                            fontSizeVal = "fontSizeSmaller";
                        } else {
                            fontSizeVal = "fontSizeNormal";
                        }
                    }
                    return fontSizeVal;
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );
    }

    buildDomObj() {
        return new ContainerDom(this, '<div class="equationContainer stackedFractionDenominatorContainer"></div>');
    }
}

export default StackedFractionDenominatorContainer;
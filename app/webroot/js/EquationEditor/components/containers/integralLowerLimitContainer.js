/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Container from "./container.js?7.0";
import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SquareEmptyContainerWrapper from "../wrappers/squareEmptyContainerWrapper.js?7.0";

class IntegralLowerLimitContainer extends Container {
    constructor(parent) {
        super(parent);
        this.className = "IntegralLowerLimitContainer";
        this.domObj = this.buildDomObj();
        const squareEmptyContainerWrapper = new SquareEmptyContainerWrapper(this.equation);
        this.addWrappers([{
            index: 0, 
            wrapper: squareEmptyContainerWrapper
        }]);

        this.inlineLeftOverlap = 0.55;

        // left property
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let leftVal = 0;
                    if (this.parent.isInline) {
                        leftVal = this.parent.symbol.width + (this.parent.inlineLimitGap - this.inlineLeftOverlap) * fontHeight;
                    } else {
                        const maxWidthList = [];
                        if (this.parent.hasUpperLimit) {
                            maxWidthList.push(this.parent.upperLimitContainer.width);
                        }
                        if (this.parent.hasLowerLimit) {
                            maxWidthList.push(this.parent.lowerLimitContainer.width);
                        }
                        maxWidthList.push(this.parent.symbol.width);
                        const maxWidth = Math.max(...maxWidthList);
                        leftVal = 0.5 * (maxWidth - this.width);
                    }
                    return leftVal;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // top property
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let topVal = 0;
                    if (this.parent.isInline) {
                        let additionalTopAlign = 0;
                        if (this.height > this.parent.symbol.height * this.parent.inlineLowerLimitOverlap) {
                            additionalTopAlign = (0.5 - this.parent.inlineLowerLimitOverlap) * this.parent.symbol.height;
                        } else {
                            additionalTopAlign = 0.5 * this.parent.symbol.height - this.height;
                        }
                        topVal = (this.parent.topAlign - this.parent.padTop * fontHeight) + additionalTopAlign;
                    } else {
                        topVal = (this.parent.topAlign - this.parent.padTop * fontHeight) +
                                         this.parent.symbol.height * 0.5 +
                                         this.parent.lowerLimitGap * fontHeight;
                    }
                    return topVal;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // fontSize property
        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() {
                    return fontSize;
                },
                set(value) {
                    fontSize = value;
                },
                compute() {
                    let actualParentContainer = this.parent.parent;
                    while (actualParentContainer?.className == 'BracketContainer') {
                        actualParentContainer = actualParentContainer.parent.parent;
                    }
                    return (actualParentContainer.fontSize === "fontSizeSmaller" ||
                                    actualParentContainer.fontSize === "fontSizeSmallest")
                        ? "fontSizeSmallest"
                        : "fontSizeSmaller";
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );
    }

    buildDomObj() {
        return new ContainerDom(this, '<div class="equationContainer integralLowerLimitContainer"></div>');
    }
}

export default IntegralLowerLimitContainer;
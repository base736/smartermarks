/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Container from "./container.js?7.0";
import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SquareEmptyContainerWrapper from "../wrappers/squareEmptyContainerWrapper.js?7.0";

class NthRootDegreeContainer extends Container {
    constructor(parent) {
        super(parent);
        this.className = "NthRootDegreeContainer";
        this.offsetRadicalRight = 0.2;
        this.diagonalHeightAdjustment = 0.0;
        const degreeRightPadding = 0.25;
        const degreeBottomConstant = 0.40;
        const degreeBottomScaling = 0.25;
        this.domObj = this.buildDomObj();
        const squareEmptyContainerWrapper = new SquareEmptyContainerWrapper(this.equation);
        this.addWrappers([{
            index: 0, 
            wrapper: squareEmptyContainerWrapper
        }]);

        this.getBottom = () => {
            const radicalHeight = this.parent.radical.height;
            const diagonalHeight = this.parent.nthRootDiagonal.height;
            return radicalHeight * degreeBottomConstant + diagonalHeight * degreeBottomScaling;
        };

        // isLeftFlushToWrapper property
        let isLeftFlushToWrapper = false;
        this.properties.push(
            new Property(this, "isLeftFlushToWrapper", isLeftFlushToWrapper, {
                get() { return isLeftFlushToWrapper; },
                set(value) { isLeftFlushToWrapper = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const radicalWidth = this.parent.radical.width;
                    const diagonalWidth = this.parent.nthRootDiagonal.width;
                    const diagonalHeight = this.parent.nthRootDiagonal.height;
                    let extraHeight = diagonalHeight - this.parent.radical.height;
                    if (extraHeight < 0) extraHeight = 0;
                    const paddedOffset = this.getBottom() * diagonalWidth / diagonalHeight - degreeRightPadding * fontHeight - this.width;
                    return (radicalWidth + paddedOffset * extraHeight / (diagonalWidth + extraHeight)) < 0;
                },
                updateDom() {}
            })
        );

        // left property
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() { return left; },
                set(value) { left = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const radicalWidth = this.parent.radical.width;
                    const diagonalWidth = this.parent.nthRootDiagonal.width;
                    const diagonalHeight = this.parent.nthRootDiagonal.height;
                    let extraHeight = diagonalHeight - this.parent.radical.height;
                    if (extraHeight < 0) extraHeight = 0;
                    let leftVal = 0;
                    if (!this.isLeftFlushToWrapper) {
                        const paddedOffset = this.getBottom() * diagonalWidth / diagonalHeight - degreeRightPadding * fontHeight - this.width;
                        leftVal = radicalWidth + paddedOffset * extraHeight / (diagonalWidth + extraHeight);
                    }
                    return leftVal;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // top property
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() { return top; },
                set(value) { top = value; },
                compute() {
                    const diagonalHeight = this.parent.nthRootDiagonal.height;
                    let topVal = 0;
                    if (!this.isTopFlushToWrapper) {
                        topVal = diagonalHeight - this.getBottom() - this.height;
                    }
                    return topVal;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // fontSize property
        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() { return fontSize; },
                set(value) { fontSize = value; },
                compute() {
                    return "fontSizeSmallest";
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );
    }

    buildDomObj() {
        return new ContainerDom(this, '<div class="equationContainer nthRootDegreeContainer"></div>');
    }
}

export default NthRootDegreeContainer;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Container from "./container.js?7.0";
import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SquareEmptyContainerWrapper from "../wrappers/squareEmptyContainerWrapper.js?7.0";

class NthRootRadicandContainer extends Container {
    constructor(parent) {
        super(parent);
        this.className = "NthRootRadicandContainer";
        this.domObj = this.buildDomObj();
        const squareEmptyContainerWrapper = new SquareEmptyContainerWrapper(this.equation);
        this.addWrappers([{
            index: 0, 
            wrapper: squareEmptyContainerWrapper
        }]);
        this.padTopMaxChildAlignTopIsRoot = 0.45;
        this.padTopMaxChildAlignTopIsNotRoot = 0.15;
        this.padBottomMaxChildAlignTopIsRoot = 0.2;
        this.padBottomMaxChildAlignTopIsNotRoot = 0;

        // isMaxTopAlignRootWrapper property
        let isMaxTopAlignRootWrapper = false;
        this.properties.push(
            new Property(this, "isMaxTopAlignRootWrapper", isMaxTopAlignRootWrapper, {
                get() {
                    return isMaxTopAlignRootWrapper;
                },
                set(value) {
                    isMaxTopAlignRootWrapper = value;
                },
                compute() {
                    const maxTopAlignIndexWrapper = this.wrappers[this.maxTopAlignIndex];
                    return (
                        maxTopAlignIndexWrapper?.className == 'SquareRootWrapper' ||
                        maxTopAlignIndexWrapper?.className == 'NthRootWrapper'
                    );
                },
                updateDom() {}
            })
        );

        // left property
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let leftVal = this.parent.radical.width + this.parent.nthRootDiagonal.width;
                    if (this.parent.nthRootDegreeContainer.isLeftFlushToWrapper) {
                        leftVal += this.parent.nthRootDegreeContainer.width -
                            this.parent.nthRootDegreeContainer.offsetRadicalRight * fontHeight +
                            this.parent.nthRootDegreeContainer.diagonalHeightAdjustment * this.parent.nthRootDiagonal.height -
                            this.parent.radical.width;
                    }
                    return leftVal;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // top property
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let topVal = 0;
                    if (this.isMaxTopAlignRootWrapper) {
                        topVal += this.padTopMaxChildAlignTopIsRoot * fontHeight;
                    } else {
                        topVal += this.padTopMaxChildAlignTopIsNotRoot * fontHeight;
                    }
                    if (this.parent.nthRootDegreeContainer.isTopFlushToWrapper) {
                        topVal += this.parent.nthRootDegreeContainer.getBottom() +
                            this.parent.nthRootDegreeContainer.height -
                            this.parent.nthRootDiagonal.height;
                    }
                    return topVal;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // fontSize property
        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() {
                    return fontSize;
                },
                set(value) {
                    fontSize = value;
                },
                compute() {
                    let actualParentContainer = this.parent.parent;
                    while (actualParentContainer?.className == 'BracketContainer') {
                        actualParentContainer = actualParentContainer.parent.parent;
                    }
                    return actualParentContainer.fontSize;
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );
    }

    buildDomObj() {
        return new ContainerDom(this, '<div class="equationContainer nthRootRadicandContainer"></div>');
    }
}

export default NthRootRadicandContainer;
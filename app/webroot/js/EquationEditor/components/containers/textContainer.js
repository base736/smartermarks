/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Container from "./container.js?7.0";
import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SquareEmptyContainerWrapper from "../wrappers/squareEmptyContainerWrapper.js?7.0";

class TextContainer extends Container {
    constructor(parent) {
        super(parent);
        this.className = "TextContainer";
        this.domObj = this.buildDomObj();

        const squareEmptyContainerWrapper = new SquareEmptyContainerWrapper(this.equation);
        this.addWrappers([{
            index: 0,
            wrapper: squareEmptyContainerWrapper
        }]);

        // Left property: even though a leftOffset is calculated, we return 0.
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    // Although leftOffset is computed, we simply return 0.
                    return 0;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // Top property: computed based on parent's textGap.
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let topVal = 0;
                    if (this.parent.textGap >= 0) {
                        topVal = this.parent.textGap * fontHeight;
                    }
                    return topVal;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // FontSize property: determined by the actual parent's fontSize (skipping BracketContainers)
        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() {
                    return fontSize;
                },
                set(value) {
                    fontSize = value;
                },
                compute() {
                    let actualParentContainer = this.parent.parent;
                    while (actualParentContainer?.className == 'BracketContainer') {
                        actualParentContainer = actualParentContainer.parent.parent;
                    }
                    return actualParentContainer.fontSize;
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );
    }

    buildDomObj() {
        return new ContainerDom(this, '<div class="equationContainer textContainer"></div>');
    }
}

export default TextContainer;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Container from "./container.js?7.0";
import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SquareEmptyContainerWrapper from "../wrappers/squareEmptyContainerWrapper.js?7.0";

import SymbolWrapper from "../wrappers/symbolWrapper.js?7.0";

class SubscriptContainer extends Container {
    constructor(parent) {
        super(parent);
        this.className = "SubscriptContainer";
        this.domObj = this.buildDomObj();
        const squareEmptyContainerWrapper = new SquareEmptyContainerWrapper(this.equation);
        this.addWrappers([{
            index: 0, 
            wrapper: squareEmptyContainerWrapper
        }]);
        this.offsetTop = 0.75;

        // left property
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    return 0;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // top property
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    let baseWrapper = null;
                    if (this.parent.index !== 0) {
                        baseWrapper = this.parent.parent.wrappers[this.parent.index - 1];
                    } else {
                        // Create a dummy SymbolWrapper if none exists.
                        baseWrapper = new SymbolWrapper(this.equation, 'a', 'MathJax_MathItalic');
                        baseWrapper.parent = this.parent.parent;
                        baseWrapper.index = 0;
                        baseWrapper.update();
                    }
                    const fontHeight = FontMetrics.getHeight(this.fontSize);
                    return this.parent.topAlign + baseWrapper.bottomAlign - this.offsetTop * fontHeight;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // fontSize property
        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() {
                    return fontSize;
                },
                set(value) {
                    fontSize = value;
                },
                compute() {
                    let fontSizeVal = "";
                    let baseWrapper = null;
                    if (this.parent.index !== 0) {
                        baseWrapper = this.parent.parent.wrappers[this.parent.index - 1];
                    } else {
                        baseWrapper = null;
                    }
                    let actualParentContainer = this.parent.parent;
                    while (actualParentContainer?.className == 'BracketContainer') {
                        actualParentContainer = actualParentContainer.parent.parent;
                    }
                    if (
                        actualParentContainer.fontSize === "fontSizeSmaller" ||
                        actualParentContainer.fontSize === "fontSizeSmallest"
                    ) {
                        fontSizeVal = "fontSizeSmallest";
                    } else {
                        if (
                            baseWrapper?.className == 'SubscriptWrapper' ||
                            baseWrapper?.className == 'SuperscriptAndSubscriptWrapper'
                        ) {
                            fontSizeVal = "fontSizeSmallest";
                        } else {
                            fontSizeVal = "fontSizeSmaller";
                        }
                    }
                    return fontSizeVal;
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );
    }

    buildDomObj() {
        return new ContainerDom(this, '<div class="equationContainer subscriptContainer"></div>');
    }
}

export default SubscriptContainer;
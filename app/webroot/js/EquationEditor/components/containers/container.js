/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";

import BoundEquationComponent from "../misc/boundEquationComponent.js?7.0";

class Container extends BoundEquationComponent {
    constructor(parent) {
        super(parent);
        this.className = "Container";

        // The wrappers property holds the wrapper objects, in order.
        this.wrappers = [];

        // fontSize can be "fontSizeNormal", "fontSizeSmaller", or "fontSizeSmallest".
        this.fontSize = "";

        // Assume this.properties is defined in the superclass.
        
        // maxTopAlignIndex property.
        let maxTopAlignIndex = null;
        this.properties.push(
            new Property(this, "maxTopAlignIndex", maxTopAlignIndex, {
                get() {
                    return maxTopAlignIndex;
                },
                set(value) {
                    maxTopAlignIndex = value;
                },
                compute() {
                    let maxIndex = 0;
                    for (let i = 1; i < this.wrappers.length; i++) {
                        if (this.wrappers[i].topAlign > this.wrappers[maxIndex].topAlign) {
                            maxIndex = i;
                        }
                    }
                    return this.wrappers.length === 0 ? null : maxIndex;
                },
                updateDom() {}
            })
        );

        // maxBottomAlignIndex property.
        let maxBottomAlignIndex = null;
        this.properties.push(
            new Property(this, "maxBottomAlignIndex", maxBottomAlignIndex, {
                get() {
                    return maxBottomAlignIndex;
                },
                set(value) {
                    maxBottomAlignIndex = value;
                },
                compute() {
                    let maxIndex = 0;
                    for (let i = 1; i < this.wrappers.length; i++) {
                        if (this.wrappers[i].bottomAlign > this.wrappers[maxIndex].bottomAlign) {
                            maxIndex = i;
                        }
                    }
                    return this.wrappers.length === 0 ? null : maxIndex;
                },
                updateDom() {}
            })
        );

        // width property: sum of all wrappers' widths.
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    let sum = 0;
                    for (let i = 0; i < this.wrappers.length; i++) {
                        sum += this.wrappers[i].width;
                    }
                    return sum;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        // height property: based on top and bottom alignments.
        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    if (this.wrappers.length > 0) {
                        return (
                            this.wrappers[this.maxTopAlignIndex].topAlign +
                            this.wrappers[this.maxBottomAlignIndex].bottomAlign
                        );
                    } else {
                        return 0;
                    }
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );

        // clipping property.
        let clipping = "";
        this.properties.push(
            new Property(this, "clipping", clipping, {
                get() {
                    return clipping;
                },
                set(value) {
                    clipping = value;
                },
                compute() {
                    // Adding 2px because j-hat bottom gets cut off.
                    return '0px ' + (this.width + 5) + 'px ' + (height + 2) + 'px ' + (-5) + 'px';
                },
                updateDom() {
                    this.domObj.updateClipping(this.clipping);
                }
            })
        );
    }

    updateWrapperProperties() {
        // Update index, parent, and equation for each wrapper.
        for (let i = 0; i < this.wrappers.length; i++) {
            this.wrappers[i].index = i;
            this.wrappers[i].parent = this;
            this.wrappers[i].equation = this.equation;
        }
    }

    addWrappers(indexAndWrapperList) {
        indexAndWrapperList.sort((a, b) => a.index - b.index);

        // If the first wrapper is an EmptyContainerWrapper, remove it.
        if (this.wrappers[0]?.isEmptyContainerWrapper) {
            this.removeWrappers([0]);
        }
        
        // Insert each wrapper into the array and update the DOM.
        for (let i = 0; i < indexAndWrapperList.length; i++) {
            const index = indexAndWrapperList[i].index;
            const wrapper = indexAndWrapperList[i].wrapper;
            this.wrappers.splice(index, 0, wrapper);
            this.domObj.addWrapper(index, wrapper);
        }
        this.updateWrapperProperties();
    }

    removeWrappers(indexList) {
        // indexList is an array of indices to remove.
        indexList.sort((a, b) => a - b);
        let correction = 0;
        for (let i = 0; i < indexList.length; i++) {
            this.wrappers.splice(indexList[i] - correction, 1);
            this.domObj.removeWrapper(indexList[i] - correction);
            correction++;
        }
        this.updateWrapperProperties();
    }

    update() {
        // Recompute all properties.
        for (let i = 0; i < this.properties.length; i++) {
            this.properties[i].compute();
        }
        // Recursively update each wrapper.
        for (let i = 0; i < this.wrappers.length; i++) {
            this.wrappers[i].update();
        }
    }

    clone() {
        // Create a new container instance and clone its wrappers.
        const copy = new this.constructor(this.parent);
        const indexAndWrapperList = [];
        for (let i = 0; i < this.wrappers.length; i++) {
            indexAndWrapperList.push([{
                index: i,
                wrapper: this.wrappers[i].clone()
            }]);
        }
        copy.addWrappers(indexAndWrapperList);
        return copy;
    }

    buildJsonObj() {
        const jsonWrappers = [];
        // Only include wrappers if the first isn't an EmptyContainerWrapper.
        if (!(this.wrappers[0]?.isEmptyContainerWrapper)) {
            for (let i = 0; i < this.wrappers.length; i++) {
                jsonWrappers.push(this.wrappers[i].buildJsonObj());
            }
        }
        return jsonWrappers;
    }

    buildDomObj() {
        return new ContainerDom(this, '<div class="equationContainer"></div>');
    }
}

export default Container;
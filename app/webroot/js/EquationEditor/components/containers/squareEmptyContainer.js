/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Container from "./container.js?7.0";
import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SquareEmptyContainerFillerWrapper from "../wrappers/squareEmptyContainerFillerWrapper.js?7.0";

class SquareEmptyContainer extends Container {
    constructor(parent) {
        super(parent);
        this.className = "SquareEmptyContainer";
        this.borderWidth = 4;
        this.fontSize = "fontSizeNormal";
        this.domObj = this.buildDomObj();

        this.squareEmptyContainerFillerWrapper = new SquareEmptyContainerFillerWrapper(this.equation);
        this.addWrappers([{
            index: 0, 
            wrapper: this.squareEmptyContainerFillerWrapper
        }]);

        // left property
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    // compute hooks get called; returns 0 per original code.
                    return 0;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // top property
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.fontSize);
                    return 0.5 * fontHeight - 0.5 * this.squareEmptyContainerFillerWrapper.height;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // fontSize property
        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() {
                    return fontSize;
                },
                set(value) {
                    fontSize = value;
                },
                compute() {
                    let actualParentContainer = this.parent.parent;
                    while (actualParentContainer?.className == 'BracketContainer') {
                        actualParentContainer = actualParentContainer.parent.parent;
                    }
                    return actualParentContainer.fontSize;
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );

        // borderWidth property
        let borderWidth = 0;
        this.properties.push(
            new Property(this, "borderWidth", borderWidth, {
                get() {
                    return borderWidth;
                },
                set(value) {
                    borderWidth = value;
                },
                compute() {
                    return 0.088 * FontMetrics.getHeight("fontSizeNormal");
                },
                updateDom() {
                    this.domObj.updateBorderWidth(borderWidth);
                }
            })
        );
    }

    buildDomObj() {
        return new ContainerDom(this, `<div class="equationContainer squareEmptyContainer ${this.fontSize}"></div>`);
    }
}

export default SquareEmptyContainer;
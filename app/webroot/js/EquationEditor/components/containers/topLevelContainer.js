/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Container from "./container.js?7.0";
import ContainerDom from "../dom/containerDom.js?7.0";
import Property from "../../property.js?7.0";

import TopLevelEmptyContainerWrapper from "../wrappers/topLevelEmptyContainerWrapper.js?7.0";

class TopLevelContainer extends Container {
    constructor(parent) {
        super(parent);
        this.className = "TopLevelContainer";
        
        this.equation = parent;
        this.padTop = 0.0;        // Was 0.2
        this.padBottom = 0.0;     // Was 0.2
        this.domObj = this.buildDomObj();
        
        const topLevelEmptyContainerWrapper = new TopLevelEmptyContainerWrapper(this.equation);
        this.addWrappers([{
            index: 0, 
            wrapper: topLevelEmptyContainerWrapper
        }]);
        
        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() {
                    return fontSize;
                },
                set(value) {
                    fontSize = value;
                },
                compute() {
                    return "fontSizeNormal";
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );
    }

    buildDomObj() {
        return new ContainerDom(this, '<div class="equationContainer topLevelContainer"></div>');
    }
}

export default TopLevelContainer;
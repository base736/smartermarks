/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import Symbol from "../misc/symbol.js?7.0";

class OperatorWrapper extends Wrapper {
    constructor(equation, operatorSymbol, fontStyle) {
        super(equation);
        this.className = "OperatorWrapper";
        this.operatorSymbol = operatorSymbol;
        this.operator = new Symbol(this, operatorSymbol, fontStyle);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.operator.domObj);
        this.childNoncontainers = [this.operator];

        let isUnary = false;
        this.properties.push(
            new Property(this, "isUnary", isUnary, {
                get: () => isUnary,
                set: (value) => { isUnary = value; },
                compute() {
                    let isUnaryVal = false;
                    const i = this.index;
                    // Assuming LeftBracket is globally available (or imported) for bracket checks.
                    if (
                        (i === 0 || this.parent.wrappers[i - 1]?.className == 'OperatorWrapper') &&
                        (this.operator.character === "+" || this.operator.character === "−")
                    ) {
                        isUnaryVal = true;
                    } else if (this.operator.character === "!") {
                        isUnaryVal = true;
                    }
                    return isUnaryVal;
                },
                updateDom() {}
            })
        );

        let isComparison = false;
        this.properties.push(
            new Property(this, "isComparison", isComparison, {
                get: () => isComparison,
                set: (value) => { isComparison = value; },
                compute() {
                    let isComparisonVal = false;
                    if (
                        [
                            "=",
                            "<",
                            ">",
                            "≤",
                            "≥",
                            "≈",
                            "≡",
                            "≅",
                            "≠",
                            "∼",
                            "∝",
                            "≺",
                            "⪯",
                            "⊂",
                            "⊆",
                            "≻",
                            "⪰",
                            "|"
                        ].includes(this.operator.character)
                    ) {
                        isComparisonVal = true;
                    }
                    return isComparisonVal;
                },
                updateDom() {}
            })
        );

        let padLeft = 0.15;
        this.properties.push(
            new Property(this, "padLeft", padLeft, {
                get: () => padLeft,
                set: (value) => { padLeft = value; },
                compute() {
                    let padLeftVal = 0.15;
                    if (this.isComparison) {
                        padLeftVal = 0.2;
                    } else if (this.isUnary) {
                        padLeftVal = 0;
                    }
                    return padLeftVal;
                },
                updateDom() {}
            })
        );

        let padRight = 0;
        this.properties.push(
            new Property(this, "padRight", padRight, {
                get: () => padRight,
                set: (value) => { padRight = value; },
                compute() {
                    let padRightVal = 0;
                    if (this.isUnary) {
                        padRightVal = 0;
                    } else {
                        padRightVal = this.isComparison ? 0.2 : 0.15;
                    }
                    return padRightVal;
                },
                updateDom() {}
            })
        );

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    return this.operator.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    return 0.62 * this.operator.height;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    return 0.38 * this.operator.height;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper operatorWrapper"></div>');
    }

    clone() {
        return new OperatorWrapper(
            this.equation,
            this.operatorSymbol,
            this.operator.fontStyle
        );
    }

    buildJsonObj() {
        return {
            type: "Operator",
            value: this.operator.character
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        return new OperatorWrapper(equation, jsonObj.value, "MathJax_Main");
    }
}

Wrapper.registerType('Operator', OperatorWrapper);

export default OperatorWrapper;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SquareRootRadicandContainer from "../containers/squareRootRadicandContainer.js?7.0";
import SquareRootOverBar from "../misc/squareRootOverBar.js?7.0";
import SquareRootRadical from "../misc/squareRootRadical.js?7.0";
import SquareRootDiagonal from "../misc/squareRootDiagonal.js?7.0";

class SquareRootWrapper extends Wrapper {
    constructor(equation) {
        super(equation);
        this.className = "SquareRootWrapper";
        this.domObj = this.buildDomObj();

        this.radicandContainer = new SquareRootRadicandContainer(this);
        this.squareRootOverBar = new SquareRootOverBar(this);
        this.radical = new SquareRootRadical(this);
        this.squareRootDiagonal = new SquareRootDiagonal(this);

        this.domObj.append(this.radicandContainer.domObj);
        this.domObj.append(this.squareRootOverBar.domObj);
        this.domObj.append(this.radical.domObj);
        this.domObj.append(this.squareRootDiagonal.domObj);
        this.childContainers = [this.radicandContainer];
        this.childNoncontainers = [this.squareRootDiagonal, this.radical, this.squareRootOverBar];

        this.padLeft = 0.1;
        this.padRight = 0.1;

        let padBottom = 0;
        this.properties.push(
            new Property(this, "padBottom", padBottom, {
                get: () => padBottom,
                set: (value) => { padBottom = value; },
                compute() {
                    // If the parent is a fraction numerator container, add extra bottom padding.
                    if (this.parent && this.parent.className == 'FractionNumeratorContainer') {
                        return 0.2;
                    }
                    return 0;
                },
                updateDom() {}
            })
        );

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    return this.radical.width + this.squareRootDiagonal.width + this.radicandContainer.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let topAlignVal = 0;
                    if (this.radicandContainer.wrappers.length > 0) {
                        topAlignVal += this.radicandContainer.wrappers[
                            this.radicandContainer.maxTopAlignIndex
                        ].topAlign;
                    }
                    if (this.radicandContainer.isMaxTopAlignRootWrapper) {
                        topAlignVal += this.radicandContainer.padTopMaxChildAlignTopIsRoot * fontHeight;
                    } else {
                        topAlignVal += this.radicandContainer.padTopMaxChildAlignTopIsNotRoot * fontHeight;
                    }
                    return topAlignVal;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let bottomAlignVal = 0;
                    if (this.radicandContainer.wrappers.length > 0) {
                        bottomAlignVal += this.radicandContainer.wrappers[
                            this.radicandContainer.maxBottomAlignIndex
                        ].bottomAlign;
                    }
                    if (this.radicandContainer.isMaxTopAlignRootWrapper) {
                        bottomAlignVal += this.radicandContainer.padBottomMaxChildAlignTopIsRoot * fontHeight;
                    } else {
                        bottomAlignVal += this.radicandContainer.padBottomMaxChildAlignTopIsNotRoot * fontHeight;
                    }
                    return bottomAlignVal;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper squareRootWrapper"></div>');
    }

    clone() {
        const copy = new SquareRootWrapper(this.equation);
        copy.domObj = copy.buildDomObj();
        copy.radicandContainer = this.radicandContainer.clone();
        copy.radicandContainer.parent = copy;
        copy.squareRootOverBar = this.squareRootOverBar.clone();
        copy.squareRootOverBar.parent = copy;
        copy.radical = this.radical.clone();
        copy.radical.parent = copy;
        copy.squareRootDiagonal = this.squareRootDiagonal.clone();
        copy.squareRootDiagonal.parent = copy;
        copy.domObj.append(copy.radicandContainer.domObj);
        copy.domObj.append(copy.squareRootOverBar.domObj);
        copy.domObj.append(copy.radical.domObj);
        copy.domObj.append(copy.squareRootDiagonal.domObj);
        copy.childContainers = [copy.radicandContainer];
        copy.childNoncontainers = [copy.squareRootDiagonal, copy.radical, copy.squareRootOverBar];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "SquareRoot",
            operands: {
                radicand: this.radicandContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const squareRootWrapper = new SquareRootWrapper(equation);
        jsonObj.operands.radicand.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            squareRootWrapper.radicandContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        return squareRootWrapper;
    }
}

Wrapper.registerType('SquareRoot', SquareRootWrapper);

export default SquareRootWrapper;
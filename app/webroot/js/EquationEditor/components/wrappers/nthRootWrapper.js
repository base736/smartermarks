/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import NthRootRadicandContainer from "../containers/nthRootRadicandContainer.js?7.0";
import NthRootOverBar from "../misc/nthRootOverBar.js?7.0";
import NthRootRadical from "../misc/nthRootRadical.js?7.0";
import NthRootDiagonal from "../misc/nthRootDiagonal.js?7.0";
import NthRootDegreeContainer from "../containers/nthRootDegreeContainer.js?7.0";

class NthRootWrapper extends Wrapper {
    constructor(equation) {
        super(equation);
        this.className = "NthRootWrapper";
        this.domObj = this.buildDomObj();

        this.radicandContainer = new NthRootRadicandContainer(this);
        this.nthRootOverBar = new NthRootOverBar(this);
        this.radical = new NthRootRadical(this);
        this.nthRootDiagonal = new NthRootDiagonal(this);
        this.nthRootDegreeContainer = new NthRootDegreeContainer(this);

        this.domObj.append(this.nthRootDegreeContainer.domObj);
        this.domObj.append(this.radicandContainer.domObj);
        this.domObj.append(this.nthRootOverBar.domObj);
        this.domObj.append(this.radical.domObj);
        this.domObj.append(this.nthRootDiagonal.domObj);
        this.childContainers = [this.nthRootDegreeContainer, this.radicandContainer];
        this.childNoncontainers = [this.nthRootDiagonal, this.radical, this.nthRootOverBar];

        this.padBottomWhenParentIsFraction = 0.2;
        this.padLeft = 0.1;
        this.padRight = 0.1;

        let padBottom = 0;
        this.properties.push(
            new Property(this, "padBottom", padBottom, {
                get: () => padBottom,
                set: (value) => { padBottom = value; },
                compute() {
                    let padBottomVal = 0;
                    // Assuming FractionNumeratorContainer is imported globally.
                    if (this.parent.className == 'FractionNumeratorContainer') {
                        padBottomVal = 0.2;
                    }
                    return padBottomVal;
                },
                updateDom() {}
            })
        );

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let widthVal = this.radical.width + this.nthRootDiagonal.width + this.radicandContainer.width;
                    if (this.nthRootDegreeContainer.isLeftFlushToWrapper) {
                        widthVal +=
                            this.nthRootDegreeContainer.width -
                            this.nthRootDegreeContainer.offsetRadicalRight * fontHeight +
                            this.nthRootDegreeContainer.diagonalHeightAdjustment * this.nthRootDiagonal.height -
                            this.radical.width;
                    }
                    return widthVal;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let topAlignVal = 0;
                    if (this.radicandContainer.wrappers.length > 0) {
                        topAlignVal += this.radicandContainer.wrappers[
                            this.radicandContainer.maxTopAlignIndex
                        ].topAlign;
                    }
                    if (this.radicandContainer.isMaxTopAlignRootWrapper) {
                        topAlignVal += this.radicandContainer.padTopMaxChildAlignTopIsRoot * fontHeight;
                    } else {
                        topAlignVal += this.radicandContainer.padTopMaxChildAlignTopIsNotRoot * fontHeight;
                    }
                    if (this.nthRootDegreeContainer.isTopFlushToWrapper) {
                        topAlignVal +=
                            this.nthRootDegreeContainer.getBottom() +
                            this.nthRootDegreeContainer.height -
                            this.nthRootDiagonal.height;
                    }
                    return topAlignVal;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let bottomAlignVal = 0;
                    if (this.radicandContainer.wrappers.length > 0) {
                        bottomAlignVal += this.radicandContainer.wrappers[
                            this.radicandContainer.maxBottomAlignIndex
                        ].bottomAlign;
                    }
                    if (this.radicandContainer.isMaxTopAlignRootWrapper) {
                        bottomAlignVal += this.radicandContainer.padBottomMaxChildAlignTopIsRoot * fontHeight;
                    } else {
                        bottomAlignVal += this.radicandContainer.padBottomMaxChildAlignTopIsNotRoot * fontHeight;
                    }
                    return bottomAlignVal;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper nthRootWrapper"></div>');
    }

    clone() {
        const copy = new NthRootWrapper(this.equation);
        copy.domObj = copy.buildDomObj();
        copy.radicandContainer = this.radicandContainer.clone();
        copy.radicandContainer.parent = copy;
        copy.nthRootOverBar = this.nthRootOverBar.clone();
        copy.nthRootOverBar.parent = copy;
        copy.radical = this.radical.clone();
        copy.radical.parent = copy;
        copy.nthRootDiagonal = this.nthRootDiagonal.clone();
        copy.nthRootDiagonal.parent = copy;
        copy.nthRootDegreeContainer = this.nthRootDegreeContainer.clone();
        copy.nthRootDegreeContainer.parent = copy;
        copy.domObj.append(copy.radicandContainer.domObj);
        copy.domObj.append(copy.nthRootOverBar.domObj);
        copy.domObj.append(copy.radical.domObj);
        copy.domObj.append(copy.nthRootDiagonal.domObj);
        copy.domObj.append(copy.nthRootDegreeContainer.domObj);
        copy.childContainers = [copy.nthRootDegreeContainer, copy.radicandContainer];
        copy.childNoncontainers = [copy.nthRootDiagonal, copy.radical, copy.nthRootOverBar];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "NthRoot",
            operands: {
                radicand: this.radicandContainer.buildJsonObj(),
                degree: this.nthRootDegreeContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const nthRootWrapper = new NthRootWrapper(equation);
        jsonObj.operands.radicand.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            nthRootWrapper.radicandContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        });
        jsonObj.operands.degree.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            nthRootWrapper.nthRootDegreeContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        return nthRootWrapper;
    }
}

Wrapper.registerType('NthRoot', NthRootWrapper);

export default NthRootWrapper;
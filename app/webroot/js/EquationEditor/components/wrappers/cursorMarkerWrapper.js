/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from './wrapper.js?7.0';
import WrapperDom from '../dom/wrapperDom.js?7.0';

class CursorMarkerWrapper extends Wrapper {
    constructor(equation, highlightLength) {
        super(equation);
        this.className = "CursorMarkerWrapper";
        this.highlightLength = highlightLength;

        this.domObj = this.buildDomObj();
    }

    buildDomObj() {
        return new WrapperDom(this, `<div class="equationWrapper CursorMarkerWrapper" data-length="${this.highlightLength}"></div>`);
    }

    clone() {
        const copy = new CursorMarkerWrapper(this.equation, this.highlightLength);
        copy.domObj = copy.buildDomObj();
        return copy;
    }

    buildJsonObj() {
        return {
            type: "CursorMarker",
            value: this.highlightLength
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const instance = new CursorMarkerWrapper(equation, jsonObj.value);
        return instance;
    }
}

Wrapper.registerType('CursorMarker', CursorMarkerWrapper);

export default CursorMarkerWrapper;
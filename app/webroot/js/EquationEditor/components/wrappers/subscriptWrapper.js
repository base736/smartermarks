/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SubscriptContainer from "../containers/subscriptContainer.js?7.0";
import SymbolWrapper from "./symbolWrapper.js?7.0";

class SubscriptWrapper extends Wrapper {
    constructor(equation) {
        super(equation);
        this.className = "SubscriptWrapper";

        this.subscriptContainer = new SubscriptContainer(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.subscriptContainer.domObj);
        this.childContainers = [this.subscriptContainer];

        // padRight calculation
        let padRight = 0;
        this.properties.push(
            new Property(this, "padRight", padRight, {
                get: () => padRight,
                set: (v) => { padRight = v; },
                compute() {
                    let padRightVal = 0.05;
                    if (
                        this.index !== 0 &&
                        this.parent.wrappers[this.index - 1].isFunctionWrapper
                    ) {
                        if (
                            this.parent.wrappers[this.index + 1]?.className == 'BracketPairWrapper'
                        ) {
                            padRightVal = 0.05;
                        } else {
                            padRightVal = 0.175;
                        }
                    }
                    return padRightVal;
                },
                updateDom() {}
            })
        );

        // Width calculation: delegate to the subscript container.
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (v) => { width = v; },
                compute() {
                    return this.subscriptContainer.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        // topAlign calculation: use the topAlign of the preceding wrapper.
        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (v) => { topAlign = v; },
                compute() {
                    let baseWrapper = null;
                    if (this.index !== 0) {
                        baseWrapper = this.parent.wrappers[this.index - 1];
                    } else {
                        // Create a temporary SymbolWrapper if none exists.
                        baseWrapper = new SymbolWrapper(this.equation, 'a', 'MathJax_MathItalic');
                        baseWrapper.index = 0;
                        baseWrapper.parent = this.parent;
                        baseWrapper.properties.forEach(prop => {
                            if (prop.propName !== "top" && prop.propName !== "left") {
                                prop.compute();
                            }
                        });
                    }
                    return baseWrapper.topAlign;
                },
                updateDom() {}
            })
        );

        // bottomAlign calculation: use the bottomAlign of the preceding wrapper.
        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (v) => { bottomAlign = v; },
                compute() {
                    let baseWrapper = null, base = null;
                    if (this.index !== 0) {
                        baseWrapper = this.parent.wrappers[this.index - 1];
                        if (baseWrapper?.className == 'SubscriptWrapper') {
                            base = baseWrapper.subscriptContainer;
                        } else {
                            base = baseWrapper;
                        }
                    } else {
                        baseWrapper = new SymbolWrapper(this.equation, 'a', 'MathJax_MathItalic');
                        baseWrapper.index = 0;
                        baseWrapper.parent = this.parent;
                        baseWrapper.properties.forEach(prop => {
                            if (prop.propName !== "top" && prop.propName !== "left") {
                                prop.compute();
                            }
                        });
                    }
                    const fontHeightNested = FontMetrics.getHeight(this.subscriptContainer.fontSize);
                    return this.subscriptContainer.height + baseWrapper.bottomAlign - this.subscriptContainer.offsetTop * fontHeightNested;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper subscriptWrapper"></div>');
    }

    clone() {
        const copy = new SubscriptWrapper(this.equation);
        copy.subscriptContainer = this.subscriptContainer.clone();
        copy.subscriptContainer.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.subscriptContainer.domObj);
        copy.childContainers = [copy.subscriptContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "Subscript",
            operands: {
                subscript: this.subscriptContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const subscriptWrapper = new SubscriptWrapper(equation);
        jsonObj.operands.subscript.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            subscriptWrapper.subscriptContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        });
        return subscriptWrapper;
    }
}

Wrapper.registerType('Subscript', SubscriptWrapper);

export default SubscriptWrapper;
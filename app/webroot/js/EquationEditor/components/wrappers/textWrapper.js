/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import TextContainer from "../containers/textContainer.js?7.0";

class TextWrapper extends Wrapper {
    constructor(equation, fontStyle) {
        super(equation);
        this.className = "TextWrapper";
        this.fontStyle = fontStyle;
        this.textContainer = new TextContainer(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.textContainer.domObj);

        this.childNoncontainers = [];
        this.childContainers = [this.textContainer];

        let textContainerCharacter = "";
        this.properties.push(new Property(this, "textContainerCharacter", textContainerCharacter, {
            get: () => textContainerCharacter,
            set: (v) => { textContainerCharacter = v; },
            compute() {
                let val = "";
                if (this.textContainer.wrappers.length > 0) {
                    if (this.textContainer.wrappers.length === 1) {
                        const w = this.textContainer.wrappers[0];
                        if (w?.className == 'SymbolWrapper') {
                            val = w.symbol.character;
                        } else if (w?.className == 'SquareEmptyContainerWrapper') {
                            val = "squareEmptyContainerWrapper";
                        }
                    } else {
                        val = "multipleWrappers";
                    }
                }
                return val;
            },
            updateDom() {}
        }));

        let width = 0;
        this.properties.push(new Property(this, "width", width, {
            get: () => width,
            set: (v) => { width = v; },
            compute() {
                return this.textContainer.width;
            },
            updateDom() {
                this.domObj.updateWidth(this.width);
            }
        }));

        let topAlign = 0;
        this.properties.push(new Property(this, "topAlign", topAlign, {
            get: () => topAlign,
            set: (v) => { topAlign = v; },
            compute() {
                const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                let val = 0;
                if (this.textContainer.wrappers.length > 0) {
                    val = this.textContainer.wrappers[this.textContainer.maxTopAlignIndex].topAlign;
                }
                return val;
            },
            updateDom() {}
        }));

        let bottomAlign = 0;
        this.properties.push(new Property(this, "bottomAlign", bottomAlign, {
            get: () => bottomAlign,
            set: (v) => { bottomAlign = v; },
            compute() {
                let val = 0;
                if (this.textContainer.wrappers.length > 0) {
                    val = this.textContainer.wrappers[this.textContainer.maxBottomAlignIndex].bottomAlign;
                }
                return val;
            },
            updateDom() {}
        }));
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper textWrapper"></div>');
    }

    clone() {
        const copy = new TextWrapper(this.equation, this.fontStyle);
        copy.textContainer = this.textContainer.clone();
        copy.textContainer.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.textContainer.domObj);
        copy.childNoncontainers = [];
        copy.childContainers = [copy.textContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "Text",
            operands: { textExpression: this.textContainer.buildJsonObj() }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const textWrapper = new TextWrapper(equation, "MathJax_Main");
        jsonObj.operands.textExpression.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            textWrapper.textContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        return textWrapper;
    }
}

Wrapper.registerType('Text', TextWrapper);

export default TextWrapper;
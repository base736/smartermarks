/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";

import StackedFractionNumeratorContainer from "../containers/stackedFractionNumeratorContainer.js?7.0";
import StackedFractionDenominatorContainer from "../containers/stackedFractionDenominatorContainer.js?7.0";
import StackedFractionHorizontalBar from "../misc/stackedFractionHorizontalBar.js?7.0";

class StackedFractionWrapper extends Wrapper {
    constructor(equation) {
        super(equation);
        this.className = "StackedFractionWrapper";

        this.stackedFractionNumeratorContainer = new StackedFractionNumeratorContainer(this);
        this.stackedFractionDenominatorContainer = new StackedFractionDenominatorContainer(this);
        this.stackedFractionHorizontalBar = new StackedFractionHorizontalBar(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.stackedFractionNumeratorContainer.domObj);
        this.domObj.append(this.stackedFractionDenominatorContainer.domObj);
        this.domObj.append(this.stackedFractionHorizontalBar.domObj);

        this.childNoncontainers = [this.stackedFractionHorizontalBar];
        this.childContainers = [this.stackedFractionNumeratorContainer, this.stackedFractionDenominatorContainer];

        this.padLeft = 0.05;
        this.padRight = 0.05;

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    return this.stackedFractionHorizontalBar.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    return 0.5 * this.stackedFractionHorizontalBar.height + this.stackedFractionNumeratorContainer.height;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    return 0.5 * this.stackedFractionHorizontalBar.height + this.stackedFractionDenominatorContainer.height;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper stackedFractionWrapper"></div>');
    }

    clone() {
        const copy = new StackedFractionWrapper(this.equation);
        copy.stackedFractionNumeratorContainer = this.stackedFractionNumeratorContainer.clone();
        copy.stackedFractionNumeratorContainer.parent = copy;
        copy.stackedFractionDenominatorContainer = this.stackedFractionDenominatorContainer.clone();
        copy.stackedFractionDenominatorContainer.parent = copy;
        copy.stackedFractionHorizontalBar = this.stackedFractionHorizontalBar.clone();
        copy.stackedFractionHorizontalBar.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.stackedFractionNumeratorContainer.domObj);
        copy.domObj.append(copy.stackedFractionDenominatorContainer.domObj);
        copy.domObj.append(copy.stackedFractionHorizontalBar.domObj);
        copy.childNoncontainers = [copy.stackedFractionHorizontalBar];
        copy.childContainers = [copy.stackedFractionNumeratorContainer, copy.stackedFractionDenominatorContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "StackedFraction",
            operands: {
                numerator: this.stackedFractionNumeratorContainer.buildJsonObj(),
                denominator: this.stackedFractionDenominatorContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const stackedFractionWrapper = new StackedFractionWrapper(equation);
        jsonObj.operands.numerator.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            stackedFractionWrapper.stackedFractionNumeratorContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        jsonObj.operands.denominator.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            stackedFractionWrapper.stackedFractionDenominatorContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        });
        return stackedFractionWrapper;
    }
}

Wrapper.registerType('StackedFraction', StackedFractionWrapper);

export default StackedFractionWrapper;
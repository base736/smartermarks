/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import Symbol from "../misc/symbol.js?7.0";

class SymbolWrapper extends Wrapper {
    constructor(equation, character, fontStyle) {
        super(equation);
        this.className = "SymbolWrapper";
        this.symbol = new Symbol(this, character, fontStyle);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.symbol.domObj);
        this.childNoncontainers = [this.symbol];

        // Set up padLeft
        let padLeft = 0;
        this.properties.push(new Property(this, "padLeft", padLeft, {
            get: () => padLeft,
            set: (v) => { padLeft = v; },
            compute() {
                let val = 0;
                if (this.isDifferential) {
                    val = 0.15;
                }
                return val;
            },
            updateDom() {}
        }));

        // Set up padRight
        let padRight = 0;
        this.properties.push(new Property(this, "padRight", padRight, {
            get: () => padRight,
            set: (v) => { padRight = v; },
            compute() {
                let val = 0;
                if (this.index !== 0 && this.parent.wrappers[this.index - 1].isDifferential) {
                    val = 0;
                }
                return val;
            },
            updateDom() {}
        }));

        // Width property
        let width = 0;
        this.properties.push(new Property(this, "width", width, {
            get: () => width,
            set: (v) => { width = v; },
            compute() {
                return this.symbol.width;
            },
            updateDom() {
                this.domObj.updateWidth(this.width);
            }
        }));

        // topAlign property
        let topAlign = 0;
        this.properties.push(new Property(this, "topAlign", topAlign, {
            get: () => topAlign,
            set: (v) => { topAlign = v; },
            compute() {
                return 0.5 * this.symbol.height;
            },
            updateDom() {}
        }));

        // bottomAlign property
        let bottomAlign = 0;
        this.properties.push(new Property(this, "bottomAlign", bottomAlign, {
            get: () => bottomAlign,
            set: (v) => { bottomAlign = v; },
            compute() {
                return 0.5 * this.symbol.height;
            },
            updateDom() {}
        }));
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper symbolWrapper"></div>');
    }

    clone() {
        return new SymbolWrapper(this.equation, this.symbol.character, this.symbol.fontStyle);
    }

    buildJsonObj() {
        return { type: "Symbol", value: this.symbol.character };
    }

    static constructFromJsonObj(jsonObj, equation) {
        let fontStyle = "";
        if (FontMetrics.MathJax_MathItalic.includes(jsonObj.value)) {
            fontStyle = "MathJax_MathItalic";
        } else if (FontMetrics.MathJax_Main.includes(jsonObj.value)) {
            fontStyle = "MathJax_Main";
        } else if (FontMetrics.MathJax_MainItalic.includes(jsonObj.value)) {
            fontStyle = "MathJax_MainItalic";
        }
        return new SymbolWrapper(equation, jsonObj.value, fontStyle);
    }
}

Wrapper.registerType('Symbol', SymbolWrapper);

export default SymbolWrapper;
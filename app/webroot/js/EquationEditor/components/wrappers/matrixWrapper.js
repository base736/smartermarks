/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import MatrixContainer from "../containers/matrixContainer.js?7.0";

class MatrixWrapper extends Wrapper {
    constructor(equation, numRows, numCols, horAlign) {
        super(equation);
        this.className = "MatrixWrapper";
        this.numRows = numRows;
        this.numCols = numCols;
        this.horAlign = horAlign;
        this.horGap = 1;
        this.vertGap = 0.25;

        if (this.numRows === 2 && this.numCols === 1) {
            this.padLeft = 0;
            this.padRight = 0;
        } else {
            this.padLeft = 0.25;
            this.padRight = 0.25;
        }

        this.domObj = this.buildDomObj();

        this.childContainers = [];
        this.matrixContainers = [];
        for (let i = 0; i < this.numRows; i++) {
            let row = [];
            for (let j = 0; j < this.numCols; j++) {
                let matrixContainer = new MatrixContainer(this, i, j);
                this.domObj.append(matrixContainer.domObj);
                row.push(matrixContainer);
                this.childContainers.push(matrixContainer);
            }
            this.matrixContainers.push(row);
        }

        let rowTopAligns = [];
        this.properties.push(
            new Property(this, "rowTopAligns", rowTopAligns, {
                get: () => rowTopAligns,
                set: (value) => { rowTopAligns = value; },
                compute() {
                    let rowTopAlignsVal = [];
                    for (let i = 0; i < this.numRows; i++) {
                        let rowTopAlignsList = [];
                        for (let j = 0; j < this.numCols; j++) {
                            let topAlign = 0;
                            if (this.matrixContainers[i][j].wrappers.length > 0) {
                                topAlign =
                                    this.matrixContainers[i][j].wrappers[
                                        this.matrixContainers[i][j].maxTopAlignIndex
                                    ].topAlign;
                            }
                            rowTopAlignsList.push(topAlign);
                        }
                        rowTopAlignsVal.push(Math.max(...rowTopAlignsList));
                    }
                    return rowTopAlignsVal;
                },
                updateDom() {}
            })
        );

        let rowBottomAligns = [];
        this.properties.push(
            new Property(this, "rowBottomAligns", rowBottomAligns, {
                get: () => rowBottomAligns,
                set: (value) => { rowBottomAligns = value; },
                compute() {
                    let rowBottomAlignsVal = [];
                    for (let i = 0; i < this.numRows; i++) {
                        let rowBottomAlignsList = [];
                        for (let j = 0; j < this.numCols; j++) {
                            let bottomAlign = 0;
                            if (this.matrixContainers[i][j].wrappers.length > 0) {
                                bottomAlign =
                                    this.matrixContainers[i][j].wrappers[
                                        this.matrixContainers[i][j].maxBottomAlignIndex
                                    ].bottomAlign;
                            }
                            rowBottomAlignsList.push(bottomAlign);
                        }
                        rowBottomAlignsVal.push(Math.max(...rowBottomAlignsList));
                    }
                    return rowBottomAlignsVal;
                },
                updateDom() {}
            })
        );

        let colWidths = [];
        this.properties.push(
            new Property(this, "colWidths", colWidths, {
                get: () => colWidths,
                set: (value) => { colWidths = value; },
                compute() {
                    let colWidthsVal = [];
                    for (let i = 0; i < this.numCols; i++) {
                        let colWidthsList = [];
                        for (let j = 0; j < this.numRows; j++) {
                            colWidthsList.push(this.matrixContainers[j][i].width);
                        }
                        colWidthsVal.push(Math.max(...colWidthsList));
                    }
                    return colWidthsVal;
                },
                updateDom() {}
            })
        );

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let widthVal = 0;
                    for (let i = 0; i < this.numCols; i++) {
                        widthVal += this.colWidths[i];
                    }
                    widthVal += (this.numCols - 1) * this.horGap * fontHeight;
                    return widthVal;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let matrixHeight = 0;
        this.properties.push(
            new Property(this, "matrixHeight", matrixHeight, {
                get: () => matrixHeight,
                set: (value) => { matrixHeight = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let matrixHeightVal = 0;
                    for (let i = 0; i < this.numRows; i++) {
                        matrixHeightVal += this.rowTopAligns[i] + this.rowBottomAligns[i];
                    }
                    matrixHeightVal += (this.numRows - 1) * this.vertGap * fontHeight;
                    return matrixHeightVal;
                },
                updateDom() {}
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    return 0.5 * this.matrixHeight;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    return 0.5 * this.matrixHeight;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper matrixWrapper"></div>');
    }

    clone() {
        const copy = new MatrixWrapper(this.equation, this.numRows, this.numCols, this.horAlign);
        copy.domObj = copy.buildDomObj();
        copy.childContainers = [];
        copy.matrixContainers = [];
        for (let i = 0; i < copy.numRows; i++) {
            let row = [];
            for (let j = 0; j < copy.numCols; j++) {
                let matrixContainer = this.matrixContainers[i][j].clone();
                matrixContainer.parent = copy;
                copy.domObj.append(matrixContainer.domObj);
                row.push(matrixContainer);
                copy.childContainers.push(matrixContainer);
            }
            copy.matrixContainers.push(row);
        }
        return copy;
    }

    buildJsonObj() {
        let jsonMatrixContainers = [];
        for (let i = 0; i < this.matrixContainers.length; i++) {
            let jsonRow = [];
            for (let j = 0; j < this.matrixContainers[i].length; j++) {
                jsonRow.push(this.matrixContainers[i][j].buildJsonObj());
            }
            jsonMatrixContainers.push(jsonRow);
        }
        return {
            type: "Matrix",
            horAlign: this.horAlign,
            operands: {
                elements: jsonMatrixContainers
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const horAlign = jsonObj.horAlign || "center";
        const numRows = jsonObj.operands.elements.length;
        const numCols = jsonObj.operands.elements[0].length;
        const matrixWrapper = new MatrixWrapper(equation, numRows, numCols, horAlign);
        for (let i = 0; i < jsonObj.operands.elements.length; i++) {
            for (let j = 0; j < jsonObj.operands.elements[i].length; j++) {
                const matrixEntry = jsonObj.operands.elements[i][j];
                for (let k = 0; k < matrixEntry.length; k++) {
                    const innerWrapper = Wrapper.wrapperFromJsonObj(matrixEntry[k], equation);
                    matrixWrapper.matrixContainers[i][j].addWrappers([{
                        index: k,
                        wrapper: innerWrapper
                    }]);
                }
            }
        }
        return matrixWrapper;
    }
}

Wrapper.registerType('Matrix', MatrixWrapper);

export default MatrixWrapper;
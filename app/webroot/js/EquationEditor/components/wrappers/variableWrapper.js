/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";

import Variable from "../misc/variable.js?7.0";

class VariableWrapper extends Wrapper {
    constructor(equation, data) {
        super(equation);
        this.className = "VariableWrapper";
        this.variable = new Variable(this, data);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.variable.domObj);
        this.childNoncontainers = [this.variable];

        if (this.variable.isRendered) {
            this.padLeft = 0.0;
            this.padRight = 0.0;
            this.marginBottom = 0.0;
        } else {
            this.padLeft = 0.1;
            this.padRight = 0.1;
            this.marginBottom = 1.0;
        }

        let width = 0;
        this.properties.push(new Property(this, "width", width, {
            get: () => width,
            set: (v) => { width = v; },
            compute() {
                return this.variable.width;
            },
            updateDom() {
                this.domObj.updateWidth(this.width);
            }
        }));

        let topAlign = 0;
        this.properties.push(new Property(this, "topAlign", topAlign, {
            get: () => topAlign,
            set: (v) => { topAlign = v; },
            compute() {
                return 0.5 * this.variable.height;
            },
            updateDom() {}
        }));

        let bottomAlign = 0;
        this.properties.push(new Property(this, "bottomAlign", bottomAlign, {
            get: () => bottomAlign,
            set: (v) => { bottomAlign = v; },
            compute() {
                return 0.5 * this.variable.height + this.marginBottom;
            },
            updateDom() {}
        }));
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper variableWrapper"></div>');
    }

    clone() {
        const data = { value: this.variable.value };
        if (this.variable.isRendered) {
            data.displayValue = this.variable.displayText;
        } else if (this.variable.displayText !== this.variable.value) {
            data.displayName = this.variable.displayText;
        }
        return new VariableWrapper(this.equation, data);
    }

    buildJsonObj() {
        const jsonObj = { type: "Variable", value: this.variable.value };
        if (this.variable.isRendered) {
            jsonObj.displayValue = this.variable.displayText;
        }
        return jsonObj;
    }

    static constructFromJsonObj(jsonObj, equation) {
        return new VariableWrapper(equation, jsonObj);
    }
}

Wrapper.registerType('Variable', VariableWrapper);

export default VariableWrapper;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import LabeledArrowLabelContainer from "../containers/LabeledArrowLabelContainer.js?7.0";
import LabeledArrowArrow from "../misc/labeledArrowArrow.js?7.0";

class LabeledArrowWrapper extends Wrapper {
    constructor(equation) {
        super(equation);
        this.className = "LabeledArrowWrapper";

        this.labeledArrowLabelContainer = new LabeledArrowLabelContainer(this);
        this.labeledArrowArrow = new LabeledArrowArrow(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.labeledArrowLabelContainer.domObj);
        this.domObj.append(this.labeledArrowArrow.domObj);

        this.labelOffsetRatio = 0.15;

        this.childNoncontainers = [this.labeledArrowArrow];
        this.childContainers = [this.labeledArrowLabelContainer];

        this.padLeft = 0.3;
        this.padRight = 0.3;

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    return this.labeledArrowArrow.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight("fontSizeSmaller");
                    return this.labeledArrowLabelContainer.height + this.labelOffsetRatio * fontHeight;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    return 0.5 * this.labeledArrowArrow.height;
                },
                updateDom() {}
            })
        );

    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper labeledArrowWrapper"></div>');
    }

    clone() {
        const copy = new LabeledArrowWrapper(this.equation);
        copy.labeledArrowLabelContainer = this.labeledArrowLabelContainer.clone();
        copy.labeledArrowLabelContainer.parent = copy;
        copy.labeledArrowArrow = this.labeledArrowArrow.clone();
        copy.labeledArrowArrow.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.labeledArrowLabelContainer.domObj);
        copy.domObj.append(copy.labeledArrowArrow.domObj);
        copy.childNoncontainers = [copy.labeledArrowArrow];
        copy.childContainers = [copy.labeledArrowLabelContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "LabeledArrow",
            operands: {
                label: this.labeledArrowLabelContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const labeledArrowWrapper = new LabeledArrowWrapper(equation);
        jsonObj.operands.label.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            labeledArrowWrapper.labeledArrowLabelContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        return labeledArrowWrapper;
    }
}

Wrapper.registerType('LabeledArrow', LabeledArrowWrapper);

export default LabeledArrowWrapper;
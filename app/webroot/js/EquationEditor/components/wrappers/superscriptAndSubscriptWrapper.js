/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import SuperscriptContainer from "../containers/superscriptContainer.js?7.0";
import SubscriptContainer from "../containers/subscriptContainer.js?7.0";
import SymbolWrapper from "./symbolWrapper.js?7.0";

class SuperscriptAndSubscriptWrapper extends Wrapper {
    constructor(equation) {
        super(equation);
        this.className = "SuperscriptAndSubscriptWrapper";

        this.superscriptContainer = new SuperscriptContainer(this);
        this.subscriptContainer = new SubscriptContainer(this);
        this.subscriptContainer.offsetTop = 0.45;

        this.domObj = this.buildDomObj();
        this.domObj.append(this.superscriptContainer.domObj);
        this.domObj.append(this.subscriptContainer.domObj);
        this.childContainers = [this.superscriptContainer, this.subscriptContainer];

        // Set up padRight based on adjacent wrappers.
        let padRight = 0;
        this.properties.push(
            new Property(this, "padRight", padRight, {
                get: () => padRight,
                set: (v) => { padRight = v; },
                compute() {
                    let val = 0.05;
                    if (this.index !== 0 && this.parent.wrappers[this.index - 1].isFunctionWrapper) {
                        if (
                            this.parent.wrappers[this.index + 1]?.className == 'BracketPairWrapper'
                        ) {
                            val = 0.05;
                        } else {
                            val = 0.175;
                        }
                    }
                    return val;
                },
                updateDom() {}
            })
        );

        // Width: use the maximum of the two container widths.
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (v) => { width = v; },
                compute() {
                    return Math.max(this.superscriptContainer.width, this.subscriptContainer.width);
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        // topAlign: based on the preceding wrapper.
        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (v) => { topAlign = v; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let baseWrapper = null;
                    if (this.index !== 0) {
                        baseWrapper = this.parent.wrappers[this.index - 1];
                    } else {
                        // Create a temporary base using a SymbolWrapper.
                        baseWrapper = new SymbolWrapper(this.equation, 'a', 'MathJax_MathItalic');
                        baseWrapper.parent = this.parent;
                        baseWrapper.index = 0;
                        baseWrapper.properties.forEach(prop => {
                            if (prop.propName !== "top" && prop.propName !== "left") {
                                prop.compute();
                            }
                        });
                    }
                    let supTop = 0, supBottom = 0;
                    if (this.superscriptContainer.wrappers.length !== 0) {
                        supTop = this.superscriptContainer.wrappers[this.superscriptContainer.maxTopAlignIndex].topAlign;
                        supBottom = this.superscriptContainer.wrappers[this.superscriptContainer.maxBottomAlignIndex].bottomAlign;
                    }
                    let offset = 0.15, maxOverlap = 0.75;
                    let computedTop = 0;
                    // Different rules based on the type of the base wrapper.
                    if (baseWrapper.className == 'NthRootWrapper') {
                        if (baseWrapper.nthRootDegreeContainer.isLeftFlushToWrapper) {
                            computedTop = baseWrapper.nthRootDiagonal.height + supTop - baseWrapper.bottomAlign;
                        } else {
                            computedTop = baseWrapper.topAlign + supTop;
                        }
                    } else if (baseWrapper.className == 'SquareRootWrapper') {
                        computedTop = baseWrapper.topAlign + supTop;
                    } else if (baseWrapper.className == 'SuperscriptWrapper' || baseWrapper.className == 'SuperscriptAndSubscriptWrapper') {
                        const base = baseWrapper.superscriptContainer;
                        let baseTop = 0;
                        if (base.wrappers.length !== 0) {
                            baseTop = base.wrappers[base.maxTopAlignIndex].topAlign;
                        }
                        if (supBottom + offset * fontHeight > maxOverlap * base.height) {
                            computedTop = baseWrapper.topAlign + (this.superscriptContainer.height - maxOverlap * base.height);
                        } else {
                            computedTop = baseWrapper.topAlign + supTop - offset * fontHeight;
                        }
                    } else {
                        offset = 0.3;
                        maxOverlap = 0.625;
                        if (supBottom + offset * fontHeight > maxOverlap * baseWrapper.height) {
                            computedTop = baseWrapper.topAlign + (this.superscriptContainer.height - maxOverlap * baseWrapper.height);
                        } else {
                            computedTop = baseWrapper.topAlign + supTop - offset * fontHeight;
                        }
                    }
                    return computedTop;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (v) => { bottomAlign = v; },
                compute() {
                    let baseWrapper = null;
                    if (this.index !== 0) {
                        baseWrapper = this.parent.wrappers[this.index - 1];
                    } else {
                        baseWrapper = new SymbolWrapper(this.equation, 'a', 'MathJax_MathItalic');
                        baseWrapper.index = 0;
                        baseWrapper.parent = this.parent;
                        baseWrapper.properties.forEach(prop => {
                            if (prop.propName !== "top" && prop.propName !== "left") {
                                prop.compute();
                            }
                        });
                    }
                    let fontHeightNested = FontMetrics.getHeight(this.subscriptContainer.fontSize);
                    return this.subscriptContainer.height + baseWrapper.bottomAlign - this.subscriptContainer.offsetTop * fontHeightNested;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper superscriptAndSubscriptWrapper"></div>');
    }

    clone() {
        const copy = new SuperscriptAndSubscriptWrapper(this.equation);
        copy.superscriptContainer = this.superscriptContainer.clone();
        copy.superscriptContainer.parent = copy;
        copy.subscriptContainer = this.subscriptContainer.clone();
        copy.subscriptContainer.parent = copy;
        copy.subscriptContainer.offsetTop = 0.45;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.superscriptContainer.domObj);
        copy.domObj.append(copy.subscriptContainer.domObj);
        copy.childContainers = [copy.superscriptContainer, copy.subscriptContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "SuperscriptAndSubscript",
            operands: {
                superscript: this.superscriptContainer.buildJsonObj(),
                subscript: this.subscriptContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const instance = new SuperscriptAndSubscriptWrapper(equation);
        jsonObj.operands.superscript.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            instance.superscriptContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        });
        jsonObj.operands.subscript.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            instance.subscriptContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        });
        return instance;
    }
}

Wrapper.registerType('SuperscriptAndSubscript', SuperscriptAndSubscriptWrapper);

export default SuperscriptAndSubscriptWrapper;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

class SquareEmptyContainerFillerWrapper extends Wrapper {
    constructor(equation) {
        super(equation);
        this.className = "SquareEmptyContainerFillerWrapper";
        this.domObj = this.buildDomObj();

        this.sideLength = 0.85;

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    return this.sideLength * fontHeight;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    return 0.5 * this.sideLength * fontHeight;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    return 0.5 * this.sideLength * fontHeight;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper squareEmptyContainerFillerWrapper"></div>');
    }

    clone() {
        return new SquareEmptyContainerFillerWrapper(this.equation);
    }
}

Wrapper.registerType('SquareEmptyContainerFiller', SquareEmptyContainerFillerWrapper);

export default SquareEmptyContainerFillerWrapper;
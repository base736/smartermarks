/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import FunctionWrapper from "./functionWrapper.js?7.0";
import LimitWord from "../misc/limitWord.js?7.0";
import LimitLeftContainer from "../containers/limitLeftContainer.js?7.0";
import LimitRightContainer from "../containers/limitRightContainer.js?7.0";
import LimitSymbol from "../misc/limitSymbol.js?7.0";

class LimitWrapper extends FunctionWrapper {
    constructor(equation) {
        super(equation, "lim", "MathJax_Main");
        this.className = "LimitWrapper";

        // Remove inherited topAlign, bottomAlign, and width properties.
        this.properties = this.properties.filter(
            (prop) => !["topAlign", "bottomAlign", "width"].includes(prop.propName)
        );

        this.leftLimitContainerGap = 0;
        this.rightLimitContainerGap = 0;
        this.belowLimitGap = -0.18;

        this.limitWord = new LimitWord(this);
        this.limitLeftContainer = new LimitLeftContainer(this);
        this.limitRightContainer = new LimitRightContainer(this);
        this.symbol = new LimitSymbol(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.limitWord.domObj);
        this.domObj.append(this.limitLeftContainer.domObj);
        this.domObj.append(this.limitRightContainer.domObj);
        this.domObj.append(this.symbol.domObj);

        this.childNoncontainers = [this.symbol, this.limitWord];
        this.childContainers = [this.limitLeftContainer, this.limitRightContainer];

        let bottomHalfWidth = 0;
        this.properties.push(
            new Property(this, "bottomHalfWidth", bottomHalfWidth, {
                get: () => bottomHalfWidth,
                set: (value) => {
                    bottomHalfWidth = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    return (
                        this.limitLeftContainer.width +
                        this.leftLimitContainerGap * fontHeight +
                        this.symbol.width +
                        this.rightLimitContainerGap * fontHeight +
                        this.limitRightContainer.width
                    );
                },
                updateDom() {}
            })
        );

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    const topWidth = this.limitWord.width;
                    return topWidth > this.bottomHalfWidth ? topWidth : this.bottomHalfWidth;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => {
                    topAlign = value;
                },
                compute() {
                    return 0.5 * this.limitWord.height;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => {
                    bottomAlign = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    const maxBottomHalfHeight = Math.max(
                        this.symbol.height,
                        this.limitLeftContainer.height,
                        this.limitRightContainer.height
                    );
                    return 0.5 * this.limitWord.height + this.belowLimitGap * fontHeight + maxBottomHalfHeight;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper limitWrapper"></div>');
    }

    clone() {
        const copy = new LimitWrapper(this.equation);
        copy.limitWord = new LimitWord(copy);
        copy.limitLeftContainer = this.limitLeftContainer.clone();
        copy.limitLeftContainer.parent = copy;
        copy.limitRightContainer = this.limitRightContainer.clone();
        copy.limitRightContainer.parent = copy;
        copy.symbol = new LimitSymbol(copy);
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.limitWord.domObj);
        copy.domObj.append(copy.limitLeftContainer.domObj);
        copy.domObj.append(copy.limitRightContainer.domObj);
        copy.domObj.append(copy.symbol.domObj);
        copy.childNoncontainers = [copy.symbol, copy.limitWord];
        copy.childContainers = [copy.limitLeftContainer, copy.limitRightContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "Limit",
            operands: {
                left: this.limitLeftContainer.buildJsonObj(),
                right: this.limitRightContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const limitWrapper = new LimitWrapper(equation);
        jsonObj.operands.left.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            limitWrapper.limitLeftContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        });
        jsonObj.operands.right.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            limitWrapper.limitRightContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        return limitWrapper;
    }
}

Wrapper.registerType('Limit', LimitWrapper);

export default LimitWrapper;
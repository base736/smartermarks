/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import FunctionWrapper from "./functionWrapper.js?7.0";

import FunctionLowerWord from "../misc/functionLowerWord.js?7.0";
import FunctionLowerContainer from "../containers/functionLowerContainer.js?7.0";

class FunctionLowerWrapper extends FunctionWrapper {
    constructor(equation, characters, fontStyle) {
        super(equation, characters, fontStyle);
        this.className = "FunctionLowerWrapper";

        // Remove the inherited topAlign, bottomAlign, and width properties.
        this.properties = this.properties.filter(
            (prop) => !["topAlign", "bottomAlign", "width"].includes(prop.propName)
        );

        this.belowFunctionGap = -0.075;

        this.functionWord = new FunctionLowerWord(this, characters, fontStyle);
        this.functionLowerContainer = new FunctionLowerContainer(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.functionWord.domObj);
        this.domObj.append(this.functionLowerContainer.domObj);

        this.childNoncontainers = [this.functionWord];
        this.childContainers = [this.functionLowerContainer];

        this.padLeft = 0;
        this.padRight = 0.05;

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    const topWidth = this.functionWord.width;
                    return topWidth > this.functionLowerContainer.width
                        ? topWidth
                        : this.functionLowerContainer.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => {
                    topAlign = value;
                },
                compute() {
                    return 0.5 * this.functionWord.height;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => {
                    bottomAlign = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    return (
                        0.5 * this.functionWord.height +
                        this.belowFunctionGap * fontHeight +
                        this.functionLowerContainer.height
                    );
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(
            this,
            '<div class="equationWrapper functionLowerWrapper"></div>'
        );
    }

    clone() {
        const copy = new FunctionLowerWrapper(
            this.equation,
            this.functionWord.characters.join(""),
            this.functionWord.fontStyle
        );
        copy.functionLowerContainer = this.functionLowerContainer.clone();
        copy.functionLowerContainer.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.functionWord.domObj);
        copy.domObj.append(copy.functionLowerContainer.domObj);
        copy.childNoncontainers = [copy.functionWord];
        copy.childContainers = [copy.functionLowerContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "FunctionLower",
            value: this.functionWord.characters.join(""),
            operands: {
                lower: this.functionLowerContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const functionLowerWrapper = new FunctionLowerWrapper(
            equation,
            jsonObj.value,
            "MathJax_Main"
        );
        jsonObj.operands.lower.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            functionLowerWrapper.functionLowerContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        });
        return functionLowerWrapper;
    }
}

Wrapper.registerType('FunctionLower', FunctionLowerWrapper);

export default FunctionLowerWrapper;
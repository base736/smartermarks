/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import FunctionWrapper from "./functionWrapper.js?7.0";

import LogLowerWord from "../misc/logLowerWord.js?7.0";
import LogLowerContainer from "../containers/logLowerContainer.js?7.0";

class LogLowerWrapper extends FunctionWrapper {
    constructor(equation) {
        super(equation, "log", "MathJax_Main");
        this.className = "LogLowerWrapper";

        // Remove inherited topAlign, bottomAlign, and width properties.
        this.properties = this.properties.filter(
            (prop) => !["topAlign", "bottomAlign", "width"].includes(prop.propName)
        );

        this.logLowerOverlap = 0.75;

        this.functionWord = new LogLowerWord(this);
        this.functionLowerContainer = new LogLowerContainer(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.functionWord.domObj);
        this.domObj.append(this.functionLowerContainer.domObj);

        this.childNoncontainers = [this.functionWord];
        this.childContainers = [this.functionLowerContainer];

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    return this.functionWord.width + this.functionLowerContainer.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    return 0.5 * this.functionWord.height;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.functionLowerContainer.fontSize);
                    return (
                        this.functionWord.height -
                        this.logLowerOverlap * fontHeight +
                        this.functionLowerContainer.height -
                        this.topAlign
                    );
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper logLowerWrapper"></div>');
    }

    clone() {
        const copy = new LogLowerWrapper(this.equation);
        copy.functionLowerContainer = this.functionLowerContainer.clone();
        copy.functionLowerContainer.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.functionWord.domObj);
        copy.domObj.append(copy.functionLowerContainer.domObj);
        copy.childNoncontainers = [copy.functionWord];
        copy.childContainers = [copy.functionLowerContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "LogLower",
            operands: {
                lower: this.functionLowerContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const logLowerWrapper = new LogLowerWrapper(equation);
        jsonObj.operands.lower.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            logLowerWrapper.functionLowerContainer.addWrappers([{
                index: i, 
                wrapper: innerWrapper
            }]);
        });
        return logLowerWrapper;
    }
}

Wrapper.registerType('LogLower', LogLowerWrapper);

export default LogLowerWrapper;
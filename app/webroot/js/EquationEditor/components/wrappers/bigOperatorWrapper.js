/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from './wrapper.js?7.0';
import WrapperDom from '../dom/wrapperDom.js?7.0';
import Property from '../../property.js?7.0';
import FontMetrics from '../../fontMetrics.js?7.0';

import BigOperatorLowerLimitContainer from '../containers/bigOperatorLowerLimitContainer.js?7.0';
import BigOperatorUpperLimitContainer from '../containers/bigOperatorUpperLimitContainer.js?7.0';

import SumBigOperatorSymbol from '../misc/sumBigOperatorSymbol.js?7.0';
import BigCapBigOperatorSymbol from '../misc/bigCapBigOperatorSymbol.js?7.0';
import BigCupBigOperatorSymbol from '../misc/bigCupBigOperatorSymbol.js?7.0';
import BigSqCapBigOperatorSymbol from '../misc/bigSqCapBigOperatorSymbol.js?7.0';
import BigSqCupBigOperatorSymbol from '../misc/bigSqCupBigOperatorSymbol.js?7.0';
import ProdBigOperatorSymbol from '../misc/prodBigOperatorSymbol.js?7.0';
import CoProdBigOperatorSymbol from '../misc/coProdBigOperatorSymbol.js?7.0';
import BigVeeBigOperatorSymbol from '../misc/bigVeeBigOperatorSymbol.js?7.0';
import BigWedgeBigOperatorSymbol from '../misc/bigWedgeBigOperatorSymbol.js?7.0';

class BigOperatorWrapper extends Wrapper {
    constructor(equation, hasUpperLimit, hasLowerLimit, bigOperatorType) {
        super(equation);
        this.className = "BigOperatorWrapper";
        this.isInline = false;
        this.hasUpperLimit = hasUpperLimit;
        this.hasLowerLimit = hasLowerLimit;
        this.bigOperatorType = bigOperatorType;

        this.upperLimitGap = 0.3;
        this.lowerLimitGap = 0.1;
        this.inlineUpperLimitOverlap = 0.4;
        this.inlineLowerLimitOverlap = 0.4;
        this.inlineLimitGap = 0.1;

        this.bigOperatorSymbolClasses = {
            'sum': SumBigOperatorSymbol,
            'bigCap': BigCapBigOperatorSymbol,
            'bigCup': BigCupBigOperatorSymbol,
            'bigSqCap': BigSqCapBigOperatorSymbol,
            'bigSqCup': BigSqCupBigOperatorSymbol,
            'prod': ProdBigOperatorSymbol,
            'coProd': CoProdBigOperatorSymbol,
            'bigVee': BigVeeBigOperatorSymbol,
            'bigWedge': BigWedgeBigOperatorSymbol
        };

        this.domObj = this.buildDomObj();
        this.childContainers = [];

        if (this.hasLowerLimit) {
            this.lowerLimitContainer = new BigOperatorLowerLimitContainer(this);
            this.domObj.append(this.lowerLimitContainer.domObj);
            this.childContainers.push(this.lowerLimitContainer);
        }
        if (this.hasUpperLimit) {
            this.upperLimitContainer = new BigOperatorUpperLimitContainer(this);
            this.domObj.append(this.upperLimitContainer.domObj);
            this.childContainers.push(this.upperLimitContainer);
        }

        const SymbolClass = this.bigOperatorSymbolClasses[this.bigOperatorType];
        this.symbol = new SymbolClass(this);
        
        this.domObj.append(this.symbol.domObj);
        this.childNoncontainers = [this.symbol];

        this.padLeft = 0.05;
        this.padRight = 0.15;

        let width = 0;
        this.properties.push(new Property(this, "width", width, {
            get: () => width,
            set: (value) => { width = value; },
            compute() {
                const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                let widthVal = 0;
                if (this.isInline) {
                    const maxWidthList = [];
                    if (this.hasUpperLimit) maxWidthList.push(this.upperLimitContainer.width);
                    if (this.hasLowerLimit) maxWidthList.push(this.lowerLimitContainer.width);
                    const limitWidth = maxWidthList.length > 0 ? Math.max(...maxWidthList) : 0;
                    widthVal = this.symbol.width + this.inlineLimitGap * fontHeight + limitWidth;
                } else {
                    const maxWidthList = [];
                    if (this.hasUpperLimit) maxWidthList.push(this.upperLimitContainer.width);
                    if (this.hasLowerLimit) maxWidthList.push(this.lowerLimitContainer.width);
                    maxWidthList.push(this.symbol.width);
                    widthVal = Math.max(...maxWidthList);
                }
                return widthVal;
            },
            updateDom() {
                this.domObj.updateWidth(this.width);
            }
        }));

        let topAlign = 0;
        this.properties.push(new Property(this, "topAlign", topAlign, {
            get: () => topAlign,
            set: (value) => { topAlign = value; },
            compute() {
                let topAlignVal = 0;
                if (this.isInline) {
                    if (this.hasUpperLimit) {
                        if (this.upperLimitContainer.height > this.symbol.height * this.inlineUpperLimitOverlap) {
                            topAlignVal = 0.1 * this.symbol.height + this.upperLimitContainer.height;
                        } else {
                            topAlignVal = 0.5 * this.symbol.height;
                        }
                    } else {
                        topAlignVal = 0.5 * this.symbol.height;
                    }
                } else {
                    if (this.hasUpperLimit) {
                        const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                        topAlignVal = 0.5 * this.symbol.height + this.upperLimitContainer.height + this.upperLimitGap * fontHeight;
                    } else {
                        topAlignVal = 0.5 * this.symbol.height;
                    }
                }
                return topAlignVal;
            },
            updateDom() {}
        }));

        let bottomAlign = 0;
        this.properties.push(new Property(this, "bottomAlign", bottomAlign, {
            get: () => bottomAlign,
            set: (value) => { bottomAlign = value; },
            compute() {
                let bottomAlignVal = 0;
                if (this.isInline) {
                    if (this.hasLowerLimit) {
                        if (this.lowerLimitContainer.height > this.symbol.height * this.inlineLowerLimitOverlap) {
                            bottomAlignVal = 0.1 * this.symbol.height + this.lowerLimitContainer.height;
                        } else {
                            bottomAlignVal = 0.5 * this.symbol.height;
                        }
                    } else {
                        bottomAlignVal = 0.5 * this.symbol.height;
                    }
                } else {
                    if (this.hasLowerLimit) {
                        const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                        bottomAlignVal = 0.5 * this.symbol.height + this.lowerLimitContainer.height + this.lowerLimitGap * fontHeight;
                    } else {
                        bottomAlignVal = 0.5 * this.symbol.height;
                    }
                }
                return bottomAlignVal;
            },
            updateDom() {}
        }));
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper bigOperatorWrapper"></div>');
    }

    clone() {
        const copy = new BigOperatorWrapper(this.equation, this.hasUpperLimit, this.hasLowerLimit, this.bigOperatorType);
        copy.childContainers = [];
        copy.domObj = copy.buildDomObj();
        if (copy.hasUpperLimit) {
            copy.upperLimitContainer = this.upperLimitContainer.clone();
            copy.upperLimitContainer.parent = copy;
            copy.domObj.append(copy.upperLimitContainer.domObj);
            copy.childContainers.push(copy.upperLimitContainer);
        }
        if (copy.hasLowerLimit) {
            copy.lowerLimitContainer = this.lowerLimitContainer.clone();
            copy.lowerLimitContainer.parent = copy;
            copy.domObj.append(copy.lowerLimitContainer.domObj);
            copy.childContainers.push(copy.lowerLimitContainer);
        }

        const SymbolClass = this.bigOperatorSymbolClasses[this.bigOperatorType];
        copy.symbol = new SymbolClass(copy);

        copy.domObj.append(copy.symbol.domObj);
        copy.childNoncontainers = [copy.symbol];
        return copy;
    }

    buildJsonObj() {
        const jsonObj = {
            type: "BigOperator",
            value: this.bigOperatorType
        };
        if (!this.hasLowerLimit && !this.hasUpperLimit) {
            jsonObj.operands = null;
        } else if (this.hasLowerLimit && !this.hasUpperLimit) {
            jsonObj.operands = { lowerLimit: this.lowerLimitContainer.buildJsonObj() };
        } else if (!this.hasLowerLimit && this.hasUpperLimit) {
            jsonObj.operands = { upperLimit: this.upperLimitContainer.buildJsonObj() };
        } else {
            jsonObj.operands = {
                lowerLimit: this.lowerLimitContainer.buildJsonObj(),
                upperLimit: this.upperLimitContainer.buildJsonObj()
            };
        }
        return jsonObj;
    }

    static constructFromJsonObj(jsonObj, equation) {
        const hasUpperLimit = jsonObj.operands && typeof jsonObj.operands.upperLimit !== "undefined";
        const hasLowerLimit = jsonObj.operands && typeof jsonObj.operands.lowerLimit !== "undefined";
        const bigOperatorWrapper = new BigOperatorWrapper(equation, hasUpperLimit, hasLowerLimit, jsonObj.value);
        if (hasUpperLimit) {
            jsonObj.operands.upperLimit.forEach((wrapperJson, i) => {
                const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
                bigOperatorWrapper.upperLimitContainer.addWrappers([{
                    index: i,
                    wrapper: innerWrapper
                }]);
            });
        }
        if (hasLowerLimit) {
            jsonObj.operands.lowerLimit.forEach((wrapperJson, i) => {
                const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
                bigOperatorWrapper.lowerLimitContainer.addWrappers([{
                    index: i,
                    wrapper: innerWrapper
                }]);
            });
        }
        return bigOperatorWrapper;
    }
}

Wrapper.registerType('BigOperator', BigOperatorWrapper);

export default BigOperatorWrapper;
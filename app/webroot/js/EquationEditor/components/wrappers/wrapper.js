/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";

import EquationComponent from "../misc/equationComponent.js?7.0";

class Wrapper extends EquationComponent {
    constructor(equation) {
        super(); // call super constructor from EquationComponent
        this.className = "Wrapper";
        this.isWrapper = true;

        this.equation = equation;

        this.topAlign = 0;
        this.bottomAlign = 0;
        this.index = null;
        this.childContainers = [];
        this.childNoncontainers = [];

        // Set up the left property.
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    let leftVal = 0;
                    if (this.index === 0) {
                        leftVal = this.parent.padLeft;
                    } else {
                        let prevWrapper = this.parent.wrappers[this.index - 1];
                        leftVal = prevWrapper.left + prevWrapper.width;
                    }
                    return leftVal;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // Set up the top property.
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    return this.parent.wrappers[this.parent.maxTopAlignIndex].topAlign - this.topAlign;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // Set up the height property.
        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    return this.topAlign + this.bottomAlign;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );
    }

    // Recursively compute all properties and update child components.
    update() {
        for (let i = 0; i < this.properties.length; i++) {
            this.properties[i].compute();
        }
        for (let i = 0; i < this.childContainers.length; i++) {
            this.childContainers[i].update();
        }
        for (let i = 0; i < this.childNoncontainers.length; i++) {
            this.childNoncontainers[i].update();
        }
    }

    // Subclasses should override this to generate a JSON representation.
    buildJsonObj() {
        // Empty implementation; override in subclass.
    }

    // Each wrapper class should provide its own clone() method.
    clone() {
        // Empty implementation; override in subclass.
    }

    static registerType(type, constructor) {
        Wrapper.typeToConstructorMapping[type] = constructor;
    }
      
    static wrapperFromJsonObj(jsonObj, equation) {
        const wrapperClass = Wrapper.typeToConstructorMapping[jsonObj.type];
        return wrapperClass.constructFromJsonObj(jsonObj, equation);
    }

}

Wrapper.typeToConstructorMapping = {};

export default Wrapper;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import TenToTheWord from "../misc/tenToTheWord.js?7.0";
import TenToTheContainer from "../containers/tenToTheContainer.js?7.0";

class TenToTheWrapper extends Wrapper {
    constructor(equation) {
        super(equation);
        this.className = "TenToTheWrapper";

        this.timesTenWord = new TenToTheWord(this);
        this.exponentContainer = new TenToTheContainer(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.timesTenWord.domObj);
        this.domObj.append(this.exponentContainer.domObj);

        this.childNoncontainers = [this.timesTenWord];
        this.childContainers = [this.exponentContainer];

        let width = 0;
        this.properties.push(new Property(this, "width", width, {
            get: () => width,
            set: (v) => { width = v; },
            compute() {
                return this.timesTenWord.width + this.exponentContainer.width;
            },
            updateDom() {
                this.domObj.updateWidth(this.width);
            }
        }));

        let topAlign = 0;
        this.properties.push(new Property(this, "topAlign", topAlign, {
            get: () => topAlign,
            set: (v) => { topAlign = v; },
            compute() {
                const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                const offset = 0.3, maxOverlap = 0.75;
                let expTop = 0, expBottom = 0;
                if (this.exponentContainer.wrappers.length > 0) {
                    expTop = this.exponentContainer.wrappers[this.exponentContainer.maxTopAlignIndex].topAlign;
                    expBottom = this.exponentContainer.wrappers[this.exponentContainer.maxBottomAlignIndex].bottomAlign;
                }
                if (expBottom + offset * fontHeight > maxOverlap * this.timesTenWord.height) {
                    return 0.5 * this.timesTenWord.height + (this.exponentContainer.height - maxOverlap * this.timesTenWord.height);
                } else {
                    return 0.5 * this.timesTenWord.height + expTop - offset * fontHeight;
                }
            },
            updateDom() {}
        }));

        let bottomAlign = 0;
        this.properties.push(new Property(this, "bottomAlign", bottomAlign, {
            get: () => bottomAlign,
            set: (v) => { bottomAlign = v; },
            compute() {
                return 0.5 * this.timesTenWord.height;
            },
            updateDom() {}
        }));
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper TenToTheWrapper"></div>');
    }

    clone() {
        const copy = new TenToTheWrapper(this.equation);
        copy.exponentContainer = this.exponentContainer.clone();
        copy.exponentContainer.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.timesTenWord.domObj);
        copy.domObj.append(copy.exponentContainer.domObj);
        copy.childNoncontainers = [copy.timesTenWord];
        copy.childContainers = [copy.exponentContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "TenToThe",
            operands: {
                exponent: this.exponentContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const tenToTheWrapper = new TenToTheWrapper(equation);
        jsonObj.operands.exponent.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            tenToTheWrapper.exponentContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        return tenToTheWrapper;
    }
}

Wrapper.registerType('TenToThe', TenToTheWrapper);

export default TenToTheWrapper;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from './wrapper.js?7.0';
import WrapperDom from '../dom/wrapperDom.js?7.0';
import Property from '../../property.js?7.0';

import BracketContainer from '../containers/bracketContainer.js?7.0';

import LeftParenthesisBracket from '../misc/leftParenthesisBracket.js?7.0';
import LeftSquareBracket from '../misc/leftSquareBracket.js?7.0';
import LeftCurlyBracket from '../misc/leftCurlyBracket.js?7.0';
import LeftAngleBracket from '../misc/leftAngleBracket.js?7.0';
import LeftFloorBracket from '../misc/leftFloorBracket.js?7.0';
import LeftCeilBracket from '../misc/leftCeilBracket.js?7.0';
import LeftAbsValBracket from '../misc/leftAbsValBracket.js?7.0';
import LeftNormBracket from '../misc/leftNormBracket.js?7.0';

const bracketClasses = {
    "parenthesisBracket": LeftParenthesisBracket,
    "squareBracket": LeftSquareBracket,
    "curlyBracket": LeftCurlyBracket,
    "angleBracket": LeftAngleBracket,
    "floorBracket": LeftFloorBracket,
    "ceilBracket": LeftCeilBracket,
    "absValBracket": LeftAbsValBracket,
    "normBracket": LeftNormBracket
};

class BracketLeftWrapper extends Wrapper {
    constructor(equation, bracketType) {
        super(equation);
        this.className = "BracketLeftWrapper";
        this.bracketType = bracketType;
        this.bracketContainer = new BracketContainer(this);

        const BracketClass = bracketClasses[bracketType];
        this.leftBracket = new BracketClass(this);

        this.domObj = this.buildDomObj();
        this.domObj.append(this.leftBracket.domObj);
        this.domObj.append(this.bracketContainer.domObj);
        this.childContainers = [this.bracketContainer];
        this.childNoncontainers = [this.leftBracket];

        let width = 0;
        this.properties.push(new Property(this, "width", width, {
            get: () => width,
            set: (value) => { width = value; },
            compute() {
                return this.leftBracket.width + this.bracketContainer.width;
            },
            updateDom() {
                this.domObj.updateWidth(this.width);
            }
        }));

        let topAlign = 0;
        this.properties.push(new Property(this, "topAlign", topAlign, {
            get: () => topAlign,
            set: (value) => { topAlign = value; },
            compute() {
                let topAlignVal = 0;
                if (this.bracketContainer.wrappers.length > 0) {
                    const containerTopAlign = this.bracketContainer.wrappers[this.bracketContainer.maxTopAlignIndex].topAlign;
                    const bracketTopAlign = 0.5 * this.leftBracket.height;
                    topAlignVal = bracketTopAlign < containerTopAlign ? containerTopAlign : bracketTopAlign;
                }
                return topAlignVal;
            },
            updateDom() {}
        }));

        let bottomAlign = 0;
        this.properties.push(new Property(this, "bottomAlign", bottomAlign, {
            get: () => bottomAlign,
            set: (value) => { bottomAlign = value; },
            compute() {
                let bottomAlignVal = 0;
                if (this.bracketContainer.wrappers.length > 0) {
                    const containerBottomAlign = this.bracketContainer.wrappers[this.bracketContainer.maxBottomAlignIndex].bottomAlign;
                    const bracketBottomAlign = 0.5 * this.leftBracket.height;
                    bottomAlignVal = bracketBottomAlign < containerBottomAlign ? containerBottomAlign : bracketBottomAlign;
                }
                return bottomAlignVal;
            },
            updateDom() {}
        }));
    }

    buildDomObj() {
        return new WrapperDom(this, `<div class="equationWrapper BracketLeftWrapper ${this.bracketType}"></div>`);
    }

    clone() {
        const copy = new BracketLeftWrapper(this.equation, this.bracketType);
        copy.leftBracket = this.leftBracket.clone();
        copy.bracketContainer = this.bracketContainer.clone();
        copy.leftBracket.parent = copy;
        copy.bracketContainer.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.leftBracket.domObj);
        copy.domObj.append(copy.bracketContainer.domObj);
        copy.childNoncontainers = [copy.leftBracket];
        copy.childContainers = [copy.bracketContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "BracketLeft",
            value: this.bracketType,
            operands: {
                bracketedExpression: this.bracketContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const instance = new BracketLeftWrapper(equation, jsonObj.value);
        jsonObj.operands.bracketedExpression.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            instance.bracketContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        return instance;
    }
}

Wrapper.registerType('BracketLeft', BracketLeftWrapper);

export default BracketLeftWrapper;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";

import Letter from "../misc/letter.js?7.0";

class LetterWrapper extends Wrapper {
    constructor(equation, character) {
        super(equation);
        this.className = "LetterWrapper";
        this.letter = new Letter(this, character);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.letter.domObj);
        this.childNoncontainers = [this.letter];

        let padLeft = 0;
        this.properties.push(
            new Property(this, "padLeft", padLeft, {
                get: () => padLeft,
                set: (value) => {
                    padLeft = value;
                },
                compute() {
                    let padLeftVal = 0;
                    if (this.isDifferential) {
                        padLeftVal = 0.15;
                    }
                    return padLeftVal;
                },
                updateDom() {}
            })
        );

        let padRight = 0;
        this.properties.push(
            new Property(this, "padRight", padRight, {
                get: () => padRight,
                set: (value) => {
                    padRight = value;
                },
                compute() {
                    let padRightVal = 0;
                    if (
                        this.index !== 0 &&
                        this.parent.wrappers[this.index - 1].isDifferential
                    ) {
                        padRightVal = 0;
                    }
                    return padRightVal;
                },
                updateDom() {}
            })
        );

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => {
                    width = value;
                },
                compute() {
                    return this.letter.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => {
                    topAlign = value;
                },
                compute() {
                    return 0.5 * this.letter.height;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => {
                    bottomAlign = value;
                },
                compute() {
                    return 0.5 * this.letter.height;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper letterWrapper"></div>');
    }

    clone() {
        return new LetterWrapper(this.equation, this.letter.character);
    }

    buildJsonObj() {
        return {
            type: "Letter",
            value: this.letter.character
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        return new LetterWrapper(equation, jsonObj.value);
    }
}

Wrapper.registerType('Letter', LetterWrapper);

export default LetterWrapper;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import FunctionWord from "../misc/functionWord.js?7.0";

class FunctionWrapper extends Wrapper {
    constructor(equation, functionCharacters, fontStyle) {
        super(equation);
        this.className = "FunctionWrapper";
        this.isFunctionWrapper = true;

        this.word = new FunctionWord(this, functionCharacters, fontStyle);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.word.domObj);
        this.childNoncontainers = [this.word];

        this.padLeft = 0.1;

        let padRight = 0;
        this.properties.push(
            new Property(this, "padRight", padRight, {
                get: () => padRight,
                set: (value) => { padRight = value; },
                compute() {
                    let padRightVal = 0.175;
                    if (this.index !== this.parent.wrappers.length - 1) {
                        const next = this.parent.wrappers[this.index + 1];
                        if (
                            next?.className == 'SuperscriptWrapper' ||
                            next?.className == 'SubscriptWrapper'
                        ) {
                            padRightVal = 0;
                        } else if (
                            next?.className == 'BracketPairWrapper'
                        ) {
                            padRightVal = 0.05;
                        }
                    }
                    return padRightVal;
                },
                updateDom() {}
            })
        );

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute() {
                    return this.word.width;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => { topAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    return 0.5 * this.word.height;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => { bottomAlign = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    return 0.5 * this.word.height;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(
            this,
            '<div class="equationWrapper symbolWrapper"></div>'
        );
    }

    clone() {
        return new FunctionWrapper(
            this.equation,
            this.word.characters.join(""),
            this.word.fontStyle
        );
    }

    buildJsonObj() {
        return {
            type: "Function",
            value: this.word.characters.join("")
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        return new FunctionWrapper(equation, jsonObj.value, "MathJax_Main");
    }
}

Wrapper.registerType('Function', FunctionWrapper);

export default FunctionWrapper;
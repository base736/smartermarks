/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from "./wrapper.js?7.0";
import WrapperDom from "../dom/wrapperDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import IntegralLowerLimitContainer from "../containers/integralLowerLimitContainer.js?7.0";
import IntegralUpperLimitContainer from "../containers/integralUpperLimitContainer.js?7.0";

import IntegralSymbol from "../misc/integralSymbol.js?7.0";
import DoubleIntegralSymbol from "../misc/doubleIntegralSymbol.js?7.0";
import TripleIntegralSymbol from "../misc/tripleIntegralSymbol.js?7.0";
import ContourIntegralSymbol from "../misc/contourIntegralSymbol.js?7.0";
import ContourDoubleIntegralSymbol from "../misc/contourDoubleIntegralSymbol.js?7.0";
import ContourTripleIntegralSymbol from "../misc/contourTripleIntegralSymbol.js?7.0";

const integralSymbolClasses = {
    "single": IntegralSymbol,
    "double": DoubleIntegralSymbol,
    "triple": TripleIntegralSymbol,
    "singleContour": ContourIntegralSymbol,
    "doubleContour": ContourDoubleIntegralSymbol,
    "tripleContour": ContourTripleIntegralSymbol
};

class IntegralWrapper extends Wrapper {
    constructor(equation, hasUpperLimit, hasLowerLimit, integralType) {
        super(equation);
        this.className = "IntegralWrapper";
        this.isInline = true;
        this.hasUpperLimit = hasUpperLimit;
        this.hasLowerLimit = hasLowerLimit;
        this.integralType = integralType;

        this.upperLimitGap = 0.1;
        this.lowerLimitGap = 0.2;
        this.inlineUpperLimitOverlap = 0.25;
        this.inlineLowerLimitOverlap = 0.25;
        this.inlineLimitGap = 0.15;

        this.numIntegrals = 0;
        if (
            this.integralType === "single" ||
            this.integralType === "singleContour"
        ) {
            this.numIntegrals = 1;
        } else if (
            this.integralType === "double" ||
            this.integralType === "doubleContour"
        ) {
            this.numIntegrals = 2;
        } else if (
            this.integralType === "triple" ||
            this.integralType === "tripleContour"
        ) {
            this.numIntegrals = 3;
        }

        this.domObj = this.buildDomObj();
        this.childContainers = [];

        if (this.hasLowerLimit) {
            this.lowerLimitContainer = new IntegralLowerLimitContainer(this);
            this.domObj.append(this.lowerLimitContainer.domObj);
            this.childContainers.push(this.lowerLimitContainer);
        }
        if (this.hasUpperLimit) {
            this.upperLimitContainer = new IntegralUpperLimitContainer(this);
            this.domObj.append(this.upperLimitContainer.domObj);
            this.childContainers.push(this.upperLimitContainer);
        }

        const IntegralSymbol = integralSymbolClasses[this.integralType];
        this.symbol = new IntegralSymbol(this);
        this.domObj.append(this.symbol.domObj);
        this.childNoncontainers = [this.symbol];

        this.padLeft = 0.15;
        this.padRight = 0.15;

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                    let widthVal = 0;
                    if (this.isInline) {
                        const maxWidthList = [];
                        if (this.hasUpperLimit) {
                            maxWidthList.push(this.upperLimitContainer.width);
                        }
                        if (this.hasLowerLimit) {
                            maxWidthList.push(
                                this.lowerLimitContainer.width -
                                    this.lowerLimitContainer.inlineLeftOverlap * fontHeight
                            );
                        }
                        const limitWidth =
                            maxWidthList.length > 0 ? Math.max(...maxWidthList) : 0;
                        widthVal = this.symbol.width + this.inlineLimitGap * fontHeight + limitWidth;
                    } else {
                        const maxWidthList = [];
                        if (this.hasUpperLimit) {
                            maxWidthList.push(this.upperLimitContainer.width);
                        }
                        if (this.hasLowerLimit) {
                            maxWidthList.push(this.lowerLimitContainer.width);
                        }
                        maxWidthList.push(this.symbol.width);
                        widthVal = Math.max(...maxWidthList);
                    }
                    return widthVal;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let topAlign = 0;
        this.properties.push(
            new Property(this, "topAlign", topAlign, {
                get: () => topAlign,
                set: (value) => {
                    topAlign = value;
                },
                compute() {
                    let topAlignVal = 0;
                    if (this.isInline) {
                        if (this.hasUpperLimit) {
                            if (this.upperLimitContainer.height > this.symbol.height * this.inlineUpperLimitOverlap) {
                                topAlignVal =
                                    (0.5 - this.inlineUpperLimitOverlap) * this.symbol.height +
                                    this.upperLimitContainer.height;
                            } else {
                                topAlignVal = 0.5 * this.symbol.height;
                            }
                        } else {
                            topAlignVal = 0.5 * this.symbol.height;
                        }
                    } else {
                        if (this.hasUpperLimit) {
                            const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                            topAlignVal =
                                0.5 * this.symbol.height +
                                this.upperLimitContainer.height +
                                this.upperLimitGap * fontHeight;
                        } else {
                            topAlignVal = 0.5 * this.symbol.height;
                        }
                    }
                    return topAlignVal;
                },
                updateDom() {}
            })
        );

        let bottomAlign = 0;
        this.properties.push(
            new Property(this, "bottomAlign", bottomAlign, {
                get: () => bottomAlign,
                set: (value) => {
                    bottomAlign = value;
                },
                compute() {
                    let bottomAlignVal = 0;
                    if (this.isInline) {
                        if (this.hasLowerLimit) {
                            if (this.lowerLimitContainer.height > this.symbol.height * this.inlineLowerLimitOverlap) {
                                bottomAlignVal =
                                    (0.5 - this.inlineLowerLimitOverlap) * this.symbol.height +
                                    this.lowerLimitContainer.height;
                            } else {
                                bottomAlignVal = 0.5 * this.symbol.height;
                            }
                        } else {
                            bottomAlignVal = 0.5 * this.symbol.height;
                        }
                    } else {
                        if (this.hasLowerLimit) {
                            const fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                            bottomAlignVal =
                                0.5 * this.symbol.height +
                                this.lowerLimitContainer.height +
                                this.lowerLimitGap * fontHeight;
                        } else {
                            bottomAlignVal = 0.5 * this.symbol.height;
                        }
                    }
                    return bottomAlignVal;
                },
                updateDom() {}
            })
        );
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper integralWrapper"></div>');
    }

    clone() {
        const copy = new IntegralWrapper(
            this.equation,
            this.hasUpperLimit,
            this.hasLowerLimit,
            this.integralType
        );
        copy.childContainers = [];
        copy.domObj = copy.buildDomObj();
        if (copy.hasUpperLimit) {
            copy.upperLimitContainer = this.upperLimitContainer.clone();
            copy.upperLimitContainer.parent = copy;
            copy.domObj.append(copy.upperLimitContainer.domObj);
            copy.childContainers.push(copy.upperLimitContainer);
        }
        if (copy.hasLowerLimit) {
            copy.lowerLimitContainer = this.lowerLimitContainer.clone();
            copy.lowerLimitContainer.parent = copy;
            copy.domObj.append(copy.lowerLimitContainer.domObj);
            copy.childContainers.push(copy.lowerLimitContainer);
        }

        const IntegralSymbol = integralSymbolClasses[this.integralType];
        copy.symbol = new IntegralSymbol(copy);
        copy.domObj.append(copy.symbol.domObj);
        copy.childNoncontainers = [copy.symbol];

        return copy;
    }

    buildJsonObj() {
        const jsonObj = {
            type: "Integral",
            value: this.integralType
        };
        if (!this.hasLowerLimit && !this.hasUpperLimit) {
            jsonObj.operands = null;
        } else if (this.hasLowerLimit && !this.hasUpperLimit) {
            jsonObj.operands = { lowerLimit: this.lowerLimitContainer.buildJsonObj() };
        } else if (!this.hasLowerLimit && this.hasUpperLimit) {
            jsonObj.operands = { upperLimit: this.upperLimitContainer.buildJsonObj() };
        } else {
            jsonObj.operands = {
                lowerLimit: this.lowerLimitContainer.buildJsonObj(),
                upperLimit: this.upperLimitContainer.buildJsonObj()
            };
        }
        return jsonObj;
    }

    static constructFromJsonObj(jsonObj, equation) {
        const hasUpperLimit =
            jsonObj.operands !== null &&
            typeof jsonObj.operands.upperLimit !== "undefined";
        const hasLowerLimit =
            jsonObj.operands !== null &&
            typeof jsonObj.operands.lowerLimit !== "undefined";
        const integralWrapper = new IntegralWrapper(
            equation,
            hasUpperLimit,
            hasLowerLimit,
            jsonObj.value
        );
        if (hasUpperLimit) {
            jsonObj.operands.upperLimit.forEach((wrapperJson, i) => {
                const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
                integralWrapper.upperLimitContainer.addWrappers([{
                    index: i,
                    wrapper: innerWrapper
                }]);
            });
        }
        if (hasLowerLimit) {
            jsonObj.operands.lowerLimit.forEach((wrapperJson, i) => {
                const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson,equation);
                integralWrapper.lowerLimitContainer.addWrappers([{
                    index: i,
                    wrapper: innerWrapper
                }]);
            });
        }
        return integralWrapper;
    }
}

Wrapper.registerType('Integral', IntegralWrapper);

export default IntegralWrapper;
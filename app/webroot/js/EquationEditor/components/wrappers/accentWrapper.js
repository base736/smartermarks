/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Wrapper from './wrapper.js?7.0';
import WrapperDom from '../dom/wrapperDom.js?7.0';
import Property from '../../property.js?7.0';
import FontMetrics from '../../fontMetrics.js?7.0';

import AccentSymbol from '../misc/accentSymbol.js?7.0';
import AccentContainer from '../containers/accentContainer.js?7.0';

class AccentWrapper extends Wrapper {
    constructor(equation, character, fontStyle) {
        super(equation);
        this.className = "AccentWrapper";
        this.character = character;
        this.fontStyle = fontStyle;

        this.accentSymbol = new AccentSymbol(this, character, fontStyle);
        this.accentContainer = new AccentContainer(this);
        this.domObj = this.buildDomObj();
        this.domObj.append(this.accentSymbol.domObj);
        this.domObj.append(this.accentContainer.domObj);
        this.childNoncontainers = [this.accentSymbol];
        this.childContainers = [this.accentContainer];

        let accentGap = null;
        this.properties.push(new Property(this, "accentGap", accentGap, {
            get: () => accentGap,
            set: (value) => { accentGap = value; },
            compute() {
                let accentGapVal = null;
                for (let i = 0; i < this.accentContainer.wrappers.length; i++) {
                    const wrapper = this.accentContainer.wrappers[i];
                    if (wrapper?.className == 'SymbolWrapper') {
                        const thisCharacter = wrapper.symbol.character;
                        let thisGapVal;
                        if (FontMetrics.shortCharacters.includes(thisCharacter)) thisGapVal = -0.08;
                        else if (FontMetrics.mediumCharacters.includes(thisCharacter)) thisGapVal = 0.00;
                        else if (FontMetrics.tallCharacters.includes(thisCharacter)) thisGapVal = 0.15;
                        else thisGapVal = 0.20;
                        if (accentGapVal === null || thisGapVal < accentGapVal) {
                            accentGapVal = thisGapVal;
                        }
                    }
                }
                return accentGapVal;
            },
            updateDom() {}
        }));

        let width = 0;
        this.properties.push(new Property(this, "width", width, {
            get: () => width,
            set: (value) => { width = value; },
            compute() {
                return this.accentContainer.width;
            },
            updateDom() {
                this.domObj.updateWidth(this.width);
            }
        }));

        let topAlign = 0;
        this.properties.push(new Property(this, "topAlign", topAlign, {
            get: () => topAlign,
            set: (value) => { topAlign = value; },
            compute() {
                let fontHeight = FontMetrics.getHeight(this.parent.fontSize);
                let topAlignVal = 0;
                if (this.accentContainer.wrappers.length > 0) {
                    topAlignVal = this.accentContainer.wrappers[this.accentContainer.maxTopAlignIndex].topAlign;
                }
                return topAlignVal;
            },
            updateDom() {}
        }));

        let bottomAlign = 0;
        this.properties.push(new Property(this, "bottomAlign", bottomAlign, {
            get: () => bottomAlign,
            set: (value) => { bottomAlign = value; },
            compute() {
                let bottomAlignVal = 0;
                if (this.accentContainer.wrappers.length > 0) {
                    bottomAlignVal = this.accentContainer.wrappers[this.accentContainer.maxBottomAlignIndex].bottomAlign;
                }
                return bottomAlignVal;
            },
            updateDom() {}
        }));
    }

    buildDomObj() {
        return new WrapperDom(this, '<div class="equationWrapper accentWrapper"></div>');
    }

    clone() {
        const copy = new AccentWrapper(this.equation, this.character, this.fontStyle);
        copy.accentSymbol = new AccentSymbol(copy, this.character, this.fontStyle);
        copy.accentContainer = this.accentContainer.clone();
        copy.accentContainer.parent = copy;
        copy.domObj = copy.buildDomObj();
        copy.domObj.append(copy.accentSymbol.domObj);
        copy.domObj.append(copy.accentContainer.domObj);
        copy.childNoncontainers = [copy.accentSymbol];
        copy.childContainers = [copy.accentContainer];
        return copy;
    }

    buildJsonObj() {
        return {
            type: "Accent",
            value: this.character,
            operands: {
                accentedExpression: this.accentContainer.buildJsonObj()
            }
        };
    }

    static constructFromJsonObj(jsonObj, equation) {
        const accentWrapper = new AccentWrapper(equation, jsonObj.value, 'MathJax_Main');
        jsonObj.operands.accentedExpression.forEach((wrapperJson, i) => {
            const innerWrapper = Wrapper.wrapperFromJsonObj(wrapperJson, equation);
            accentWrapper.accentContainer.addWrappers([{
                index: i,
                wrapper: innerWrapper
            }]);
        });
        return accentWrapper;
    }
}

Wrapper.registerType('Accent', AccentWrapper);

export default AccentWrapper;
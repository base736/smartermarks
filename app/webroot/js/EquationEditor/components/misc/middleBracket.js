/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class MiddleBracket extends BoundEquationComponent {
    constructor(parent, index) {
        super(parent);
        this.className = "MiddleBracket";
        this.index = index;
        // Assume this.fontStyle and this.character are set externally

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() { return width; },
                set(value) { width = value; },
                compute() { return 0; },
                updateDom() { this.domObj.updateWidth(this.width); }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() { return height; },
                set(value) { height = value; },
                compute() { return 0; },
                updateDom() { this.domObj.updateHeight(this.height); }
            })
        );

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() { return left; },
                set(value) { left = value; },
                compute() { return 0; },
                updateDom() { this.domObj.updateLeft(this.left); }
            })
        );
    }

    buildDomObj() {
        return new EquationDom(this, `<div class="middleBracket ${this.fontStyle}">${this.character}</div>`);
    }

    clone() {
        return new MiddleBracket(this.parent, this.index);
    }
}

export default MiddleBracket;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";

import Bracket from "./bracket.js?7.0";

class LeftBracket extends Bracket {
    constructor(parent) {
        super(parent);
        this.className = "LeftBracket";
        this.isLeftBracket = true;

        // Set up the desiredHeight calculation
        let desiredHeight = 0;
        this.properties.push(
            new Property(this, "desiredHeight", desiredHeight, {
                get() {
                    return desiredHeight;
                },
                set(value) {
                    desiredHeight = value;
                },
                compute() {
                    let desiredHeightVal = 0;
                    if (this.parent.className == 'BracketPairWrapper' ||
                            this.parent.className == 'BracketLeftWrapper') {
                        if (this.parent.bracketContainer.wrappers.length > 0) {
                            let maxTopAlign = this.parent.bracketContainer.wrappers[this.parent.bracketContainer.maxTopAlignIndex].topAlign;
                            let maxBottomAlign = this.parent.bracketContainer.wrappers[this.parent.bracketContainer.maxBottomAlignIndex].bottomAlign;
                            desiredHeightVal = maxTopAlign > maxBottomAlign ? 2 * maxTopAlign : 2 * maxBottomAlign;
                        }
                    }
                    return desiredHeightVal;
                },
                updateDom() {}
            })
        );
    }
}

export default LeftBracket;
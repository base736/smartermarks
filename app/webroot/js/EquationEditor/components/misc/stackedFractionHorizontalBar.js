/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class StackedFractionHorizontalBar extends BoundEquationComponent {
    constructor(parent) {
        super(parent);
        this.className = "StackedFractionHorizontalBar";
        this.domObj = this.buildDomObj();
        this.exceedsMaxNumDenomWidth = 0.25;
        this.barHeightRatio = 0.05;

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const maxNumDenomWidth = Math.max(
                        this.parent.stackedFractionDenominatorContainer.width,
                        this.parent.stackedFractionNumeratorContainer.width
                    );
                    return maxNumDenomWidth + this.exceedsMaxNumDenomWidth * fontHeight;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight("fontSizeNormal");
                    return this.barHeightRatio * fontHeight;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    return 0;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    return this.parent.stackedFractionNumeratorContainer.height;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }

    buildDomObj() {
        return new EquationDom(this, '<div class="stackedFractionHorizontalBar"></div>');
    }
}

export default StackedFractionHorizontalBar;
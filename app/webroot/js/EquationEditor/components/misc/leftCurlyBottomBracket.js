/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BottomBracket from "./bottomBracket.js?7.0";

class LeftCurlyBottomBracket extends BottomBracket {
    constructor(parent) {
        super(parent);
        this.className = "LeftCurlyBottomBracket";
        this.character = "⎩";
        this.fontStyle = "MathJax_Size4";
        this.domObj = this.buildDomObj();

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.parent.fontSize);
                    const length = this.parent.middleBrackets.length;
                    const centerIndex = Math.floor(length / 2);
                    return this.parent.middleBrackets[centerIndex].top + ((length - 1 - centerIndex) * 0.231 + 0.5) * fontHeight;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }
}

export default LeftCurlyBottomBracket;
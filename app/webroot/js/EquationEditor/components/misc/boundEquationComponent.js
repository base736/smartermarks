/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationComponent from "./equationComponent.js?7.0";

class BoundEquationComponent extends EquationComponent {
    constructor(parent) {
        super(); // call the EquationComponent constructor
        this.className = "BoundEquationComponent";
        this.parent = parent;
        this.equation = parent === null ? null : parent.equation;
    }

    clone() {
        return new this.constructor(this.parent);
    }
}

export default BoundEquationComponent;
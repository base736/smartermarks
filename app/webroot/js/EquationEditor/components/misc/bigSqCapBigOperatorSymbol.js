/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BigOperatorSymbol from "./bigOperatorSymbol.js?7.0";

class BigSqCapBigOperatorSymbol extends BigOperatorSymbol {
    constructor(parent) {
        super(parent);
        this.className = "BigSqCapBigOperatorSymbol";
        this.domObj = this.buildDomObj();

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute: () => {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    return 0.71426980882 * this.height;
                },
                updateDom: () => { this.domObj.updateWidth(this.width); }
            })
        );
    }

    buildDomObj() {
        const htmlRep = 
            `<div class="bigOperatorSymbol bigSqCapBigOperatorSymbol" style="width: 39.999485px; height: 56.000526px;">
                <svg style="position: absolute; width: 100%; height: 100%;" viewBox="0 0 39.999485 56.000526" preserveAspectRatio="none">
                    <g transform="translate(-336.48568,-397.21981)">
                        <g transform="scale(1,-1)">
                            <path d="M...Z"/>
                        </g>
                    </g>
                </svg>
            </div>`;
        return new EquationDom(this, htmlRep);
    }
}

export default BigSqCapBigOperatorSymbol;
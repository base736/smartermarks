/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class LabeledArrowArrow extends BoundEquationComponent {
    constructor(parent) {
        super(parent);
        this.className = "LabeledArrowArrow";
        this.domObj = this.buildDomObj();
        this.exceedsLabelWidth = 1.5;
        this.arrowHeightRatio = 0.50;

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const labelWidth = this.parent.labeledArrowLabelContainer.width;
                    return labelWidth + this.exceedsLabelWidth * fontHeight;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight("fontSizeNormal");
                    return this.arrowHeightRatio * fontHeight;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    return 0;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight("fontSizeSmaller");
                    return this.parent.labeledArrowLabelContainer.height - 0.5 * this.height + 
                        this.parent.labelOffsetRatio * fontHeight;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }

    buildDomObj() {
        const htmlRep =
            '<div class="labeledArrowArrow" style="display:flex;">' +
                '<svg class="arrow-1" style="position:static; flex:none; height:100%;" viewBox="0 0 0.626918 1.105" preserveAspectRatio="none">' +
                    '<g transform="translate(-97.803792,-135.98051)">' +
                        '<path d="m 98.430696,136.49063 h -0.597274 c -0.01693,0.008 -0.02963,0.019 -0.02963,0.0423 ' +
                        'l 1.4e-5,1.7e-4 v 0 c 0,0.0233 0.0127,0.0339 0.02963,0.0423 h 0.597274 z"/>' +
                    '</g>' +
                '</svg>' +
                '<svg class="arrow-2" style="position:static; flex:1 1 100%; height:100%;" viewBox="0 0 0.626222 1.105" preserveAspectRatio="none">' +
                    '<g transform="translate(-98.430696,-135.98051)">' +
                        '<path d="m 99.056903,136.49063 h -0.626207 l 1.4e-5,0.0848 h 0.626208 z"/>' +
                    '</g>' +
                '</svg>' +
                '<svg class="arrow-3" style="position:static; flex:none; height:100%;" viewBox="0 0 0.62649898 1.105" preserveAspectRatio="none">' +
                    '<g transform="translate(-99.056903,-135.98051)">' +
                        '<path d="m 99.056918,136.5754 h 0.395771 c -0.146051,0.10795 -0.247651,0.24765 -0.294218,0.43391 ' + 
                        '-0.0063,0.0233 -0.0127,0.0402 -0.0127,0.0529 0,0.0169 0.0127,0.0233 0.04445,0.0233 0.03387,0 ' + 
                        '0.0381,-0.004 0.04445,-0.0318 0.0042,-0.0233 0.01482,-0.0614 0.02328,-0.0847 0.06138,-0.18415 ' + 
                        '0.211667,-0.34714 0.406401,-0.41275 0.0127,-0.004 0.01905,-0.0106 0.01905,-0.0233 0,-0.0127 ' + 
                        '-0.0063,-0.019 -0.01905,-0.0233 -0.196851,-0.0656 -0.345018,-0.2286 -0.406401,-0.41275 ' + 
                        '-0.0085,-0.0233 -0.01905,-0.0614 -0.02328,-0.0868 -0.0063,-0.0254 -0.01058,-0.0296 -0.04445,-0.0296 ' + 
                        '-0.02752,0 -0.04233,0.002 -0.04233,0.0233 0,0.004 0.0021,0.008 0.0021,0.0148 0.01058,0.0593 ' + 
                        '0.03598,0.13547 0.06985,0.20109 0.05503,0.10583 0.124883,0.1905 0.232834,0.27093 h -0.395772 z"/>' +
                    '</g>' +
                '</svg>' +
            '</div>';
        return new EquationDom(this, htmlRep);
    }

}

export default LabeledArrowArrow;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";

import BigOperatorSymbol from "./bigOperatorSymbol.js?7.0";

class ProdBigOperatorSymbol extends BigOperatorSymbol {
    constructor(parent) {
        super(parent);
        this.className = "ProdBigOperatorSymbol";
        this.domObj = this.buildDomObj();

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    return 0.83214285669 * this.height;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );
    }

    buildDomObj() {
        const htmlRep =
            '<div class="bigOperatorSymbol prodBigOperatorSymbol" style="width: 46.5994; height: 55.999279;">' +
                '<svg style="position: absolute; width: 100%; height: 100%;" viewBox="0 0 46.5994 55.999279" preserveAspectRatio="none">' +
                    '<g transform="translate(-256.52568,-351.50552)"><g>' +
                        '<path d="m 296.60516,401.94488 0,-44.91943 c 0.33245,-1.22495 1.02744,-2.09494 2.08497,-2.60996 ' +
                        '1.05744,-0.51496 2.40243,-0.76496 4.03495,-0.74999 l 0.4,0 0,-2.15998 -46.5994,0 0,2.15998 ' +
                        '0.39999,0 c -1.6375,0.0158 -2.99248,-0.23584 -4.06495,-0.75499 -1.0725,-0.51917 ' +
                        '-1.75749,-1.40083 -2.05497,-2.64496 l -0.04,-24.11969 0,-24.15969 20.03974,0 ' +
                        '0,48.27938 -0.12,0.35999 c -0.3892,1.10664 -1.10086,1.89329 -2.13497,2.35997 ' +
                        '-1.03419,0.46665 -2.33584,0.69331 -3.90495,0.67999 l -0.4,0 0,2.15997 19.83975,0 ' +
                        '0,-2.15997 -0.4,0 c -1.63752,0.0158 -2.99251,-0.23584 -4.06495,-0.75499 ' +
                        '-1.07253,-0.51917 -1.75752,-1.40083 -2.05497,-2.64496 z" />' +
                    '</g></g></svg></div>';
        return new EquationDom(this, htmlRep);
    }
}

export default ProdBigOperatorSymbol;
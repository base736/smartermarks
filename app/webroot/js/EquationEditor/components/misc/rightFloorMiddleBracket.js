/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import FontMetrics from "../../fontMetrics.js?7.0";
import Property from "../../property.js?7.0";

import MiddleBracket from "./middleBracket.js?7.0";

class RightFloorMiddleBracket extends MiddleBracket {
    constructor(parent, index) {
        super(parent, index);
        this.className = "RightFloorMiddleBracket";
        this.character = "⎥";
        this.fontStyle = "MathJax_Size4";
        this.domObj = this.buildDomObj();

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() { return top; },
                set(value) { top = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.parent.fontSize);
                    return (0.45 * this.index - 0.15) * fontHeight;
                },
                updateDom() { this.domObj.updateTop(this.top); }
            })
        );
    }
}

export default RightFloorMiddleBracket;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class TopLevelEmptyContainerMessage extends BoundEquationComponent {
    constructor(parent) {
        super(parent);
        this.className = "TopLevelEmptyContainerMessage";
        this.message = "Enter&nbsp;Your&nbsp;Equation&nbsp;Here";
        this.fontSize = "fontSizeMessage";
        this.domObj = this.buildDomObj();

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    return this.domObj.value.width();
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    return this.domObj.value.height();
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    return 0;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = this.getFontHeight();
                    return 0.5 * (this.parent.height - this.height) - this.parent.padTop * fontHeight;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }
    
    clone() {
        return new this.constructor();
    }
    
    buildDomObj() {
        return new EquationDom(this, `<span class="topLevelEmptyContainerMessage ${this.fontSize}">${this.message}</span>`);
    }
}

export default TopLevelEmptyContainerMessage;
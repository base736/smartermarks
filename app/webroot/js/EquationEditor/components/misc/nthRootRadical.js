/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class NthRootRadical extends BoundEquationComponent {
    constructor(parent) {
        super(parent);
        this.className = "NthRootRadical";
        this.domObj = this.buildDomObj();

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let widthVal = 0;
                    if (this.parent.nthRootDiagonal.height < 2 * fontHeight) {
                        widthVal = 0.4 * fontHeight;
                    } else {
                        widthVal = 0.5 * fontHeight;
                    }
                    return widthVal;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let heightVal = 0;
                    if (this.parent.nthRootDiagonal.height < 2 * fontHeight) {
                        heightVal = 0.7 * fontHeight;
                    } else {
                        heightVal = 0.75 * fontHeight;
                    }
                    return heightVal;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let leftVal = 0;
                    if (this.parent.nthRootDegreeContainer.isLeftFlushToWrapper) {
                        leftVal += this.parent.nthRootDegreeContainer.width -
                                             this.parent.nthRootDegreeContainer.offsetRadicalRight * fontHeight +
                                             this.parent.nthRootDegreeContainer.diagonalHeightAdjustment * this.parent.nthRootDiagonal.height -
                                             this.parent.radical.width;
                    }
                    return leftVal;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let topVal = this.parent.nthRootDiagonal.height - this.height;
                    if (this.parent.nthRootDegreeContainer.isTopFlushToWrapper) {
                        topVal += this.parent.nthRootDegreeContainer.getBottom() +
                                            this.parent.nthRootDegreeContainer.height - this.parent.nthRootDiagonal.height;
                    }
                    return topVal;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }

    buildDomObj() {
        const htmlRep =
            '<div class="nthRootRadical" style="width: 74.842293px; height: 127.48769px;">' +
                '<svg style="position: absolute; width: 100%; height: 100%;" viewBox="0 0 74.842293 127.48769" preserveAspectRatio="none">' +
                    '<defs id="defs4">' +
                        '<clipPath clipPathUnits="userSpaceOnUse" id="clipPath3765">' +
                            '<rect style="fill:#b1ded2;fill-opacity:1;stroke:none" id="rect3767" width="74.842293" height="127.62585" x="198.84776" y="668.99451" />' +
                        '</clipPath>' +
                    '</defs>' +
                    '<g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(-198.84776,-668.99451)">' +
                        '<g clip-path="url(#clipPath3765)">' +
                            '<path d="m 265.30006,796.48219 -47.75994,-111.23309 -14.88621,11.47479 -3.82493,-3.82493 30.28931,-23.46646 44.65864,103.89336 109.8892,-228.9789 c 0.68896,-1.30943 1.89502,-1.96414 3.61817,-1.96415 1.17139 0 2.17069,0.41351 2.99792,1.24052 0.8268,0.82701 1.2403,1.82632 1.24052,2.99791 -0.00002,0.68918 -0.0691,1.17161 -0.20676,1.44728 L 273.15667,794.51804 c -0.55144,1.30919 -1.61966,1.96391 -3.20467,1.96415 l -4.65194,0" id="path2987"/>' +
                        '</g>' +
                    '</g>' +
                '</svg>' +
            '</div>';
        return new EquationDom(this, htmlRep);
    }
}

export default NthRootRadical;
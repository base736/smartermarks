/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class Word extends BoundEquationComponent {
    constructor(parent, characters, fontStyle) {
        super(parent);
        this.className = "Word";
        this.characters = characters.split("");
        this.fontStyle = fontStyle;
        this.domObj = this.buildDomObj();
        if (this.fontStyle === "MathJax_MathItalic") {
            this.adjustTop = 0.025;
        }
        
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    let widthVal = 0;
                    for (let i = 0; i < this.characters.length; i++) {
                        widthVal += FontMetrics.getWidth(this.characters[i], this.fontStyle, this.parent.parent.fontSize);
                    }
                    return widthVal + 2;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );
        
        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    const fontHeight = this.getFontHeight();
                    return 1 * fontHeight;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );
    }

    buildDomObj() {
        return new EquationDom(this, `<div class="symbol ${this.fontStyle}">${this.characters.join("")}</div>`);
    }
}

export default Word;
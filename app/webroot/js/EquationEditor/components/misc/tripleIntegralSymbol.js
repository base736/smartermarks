/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";
import Property from "../../property.js?7.0";

import BigOperatorSymbol from "./bigOperatorSymbol.js?7.0";

class TripleIntegralSymbol extends BigOperatorSymbol {
    constructor(parent) {
        super(parent);
        this.className = "TripleIntegralSymbol";
        this.domObj = this.buildDomObj();

        // Remove inherited height property so we can override it.
        for (let i = 0; i < this.properties.length; i++) {
            if (this.properties[i].propName === "height") {
                this.properties.splice(i, 1);
                break;
            }
        }
        
        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    return 2.25 * fontHeight;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );
        
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    return 0.86633665265 * this.height;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );
    }
    
    buildDomObj() {
        const htmlRep =
            '<div class="bigOperatorSymbol tripleIntegralSymbol" style="width: 76.999008; height: 88.878853;">' +
                '<svg style="position: absolute; width: 100%; height: 100%;" viewBox="0 0 76.999008 88.878853" preserveAspectRatio="none">' +
                    '<g transform="translate(-242.19997,-417.96288)"><g>' +
                        '<path d="m 246.67991,502.0018 c -0.0167,-0.67085 -0.23333,-1.20918 -0.64999,-1.61498 -0.41666,-0.40586 -0.93332,-0.61419 -1.54998,-0.62499 -0.74249,0.0116 -1.30748,0.2383 -1.69498,0.67999 -0.38749,0.44163 -0.58249,1.02829 -0.58499,1.75998 0.0833,1.29578 0.52666,2.3841 1.32997,3.26496 0.80332,0.88078 1.82664,1.33911 3.06997,1.37498 0.94664,-0.025 1.7933,-0.28503 2.53996,-0.77999 0.74665,-0.49503 1.35331,-1.07502 1.81998,-1.73998 0.57998,-0.75835 1.13997,-1.70167 1.67998,-2.82996 0.53998,-1.12835 1.05997,-2.45166 1.55998,-3.96995 0.31248,-0.95335 0.63747,-2.03667 0.97498,-3.24996 0.33748,-1.21334 0.73248,-2.71665 1.18499,-4.50994 1.21577,-4.8797 2.32193,-9.52902 3.31847,-13.94797 0.99651,-4.41895 1.9486,-8.92902 2.85626,-13.5302 0.90762,-4.60116 1.83601,-9.61491 2.78515,-15.04128 0.46414,-2.82161 0.8558,-5.08824 1.17499,-6.79992 0.31913,-1.71161 0.6608,-3.45825 1.02499,-5.23993 0.58663,-2.96575 1.10329,-5.34406 1.54998,-7.13491 0.44663,-1.79076 0.90329,-3.31907 1.36998,-4.58494 0.42329,-1.1666 0.85662,-2.09325 1.29998,-2.77996 0.4433,-0.68661 0.87662,-1.09327 1.29998,-1.21999 0.0733,-0.0333 0.16664,-0.0466 0.28,-0.04 0.20746,0.002 0.43246,0.0384 0.67499,0.11 0.24247,0.0717 0.45746,0.16839 0.64499,0.29 0.13913,0.0984 0.3008,0.23172 0.485,0.39999 0.18413,0.16839 0.27579,0.26172 0.27499,0.28 -0.61336,0.14505 -1.11668,0.38505 -1.50998,0.71999 -0.39336,0.33505 -0.59669,0.85504 -0.60999,1.55998 0.0166,0.67087 0.2333,1.2092 0.64999,1.61498 0.41663,0.40587 0.93329,0.61421 1.54998,0.625 0.75996,-0.0108 1.32995,-0.23912 1.70998,-0.68499 0.37996,-0.44578 0.56996,-1.0441 0.56999,-1.79498 -0.0884,-1.2941 -0.53169,-2.37575 -1.32998,-3.24495 -0.79836,-0.86911 -1.90168,-1.32077 -3.30996,-1.35499 -0.48169,0.008 -0.93835,0.10256 -1.36998,0.285 -0.43169,0.18255 -0.82835,0.40755 -1.18998,0.67499 -1.70834,1.37503 -3.09166,3.575 -4.14995,6.59992 -1.05835,3.025 -1.98167,6.18496 -2.76996,9.47987 -1.22915,4.92837 -2.34419,9.6004 -3.34515,14.01612 -1.00099,4.41576 -1.95308,8.92484 -2.85626,13.52723 -0.90321,4.60242 -1.8227,9.64778 -2.75848,15.1361 -0.56834,3.30495 -1.07167,6.13491 -1.50998,8.48989 -0.43834,2.35496 -0.88167,4.56493 -1.32998,6.62992 -0.73334,3.4591 -1.43666,6.16073 -2.10998,8.10489 -0.67333,1.94412 -1.35665,3.25576 -2.04997,3.93495 -0.21,0.19747 -0.39,0.33247 -0.53999,0.405 -0.15001,0.0725 -0.33,0.0975 -0.53999,0.075 -0.34001,0.002 -0.66,-0.0617 -0.95999,-0.19 -0.3,-0.12836 -0.58,-0.33169 -0.83999,-0.60999 -0.08,-0.0625 -0.14,-0.11753 -0.18,-0.165 -0.04,-0.0475 -0.06,-0.0725 -0.06,-0.075 0.61332,-0.14503 1.11665,-0.38505 1.50998,-0.71999 0.39332,-0.33505 0.59665,-0.85504 0.60999,-1.55998 z"/>' +
            '</g></svg></div>';
        return new EquationDom(this, htmlRep);
    }
}

export default TripleIntegralSymbol;
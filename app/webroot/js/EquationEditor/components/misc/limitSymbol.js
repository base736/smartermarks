/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import Symbol from "./symbol.js?7.0";

class LimitSymbol extends Symbol {
    constructor(parent) {
        super(parent, "→", "MathJax_Main");
        this.className = "LimitSymbol";
        this.adjustTop = -0.07;

        // Remove inherited left and top properties
        for (let i = 0; i < this.properties.length; i++) {
            if (this.properties[i].propName === "left" || this.properties[i].propName === "top") {
                this.properties.splice(i, 1);
                i--;
            }
        }

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const leftOffset = 0.5 * ((this.parent.width - (this.parent.padLeft + this.parent.padRight) * fontHeight) - this.parent.bottomHalfWidth);
                    return leftOffset + this.parent.limitLeftContainer.width + this.parent.leftLimitContainerGap * fontHeight;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let bottomHalfMaxTopAlign = 0;
                    let topOffset = 0;
                    if (
                        this.parent.limitLeftContainer.wrappers.length > 0 &&
                        this.parent.limitRightContainer.wrappers.length > 0
                    ) {
                        bottomHalfMaxTopAlign = Math.max(
                            this.parent.limitLeftContainer.wrappers[this.parent.limitLeftContainer.maxTopAlignIndex].topAlign,
                            0.5 * this.height,
                            this.parent.limitRightContainer.wrappers[this.parent.limitRightContainer.maxTopAlignIndex].topAlign
                        );
                        topOffset = bottomHalfMaxTopAlign - 0.5 * this.height;
                    }
                    return this.parent.limitWord.height + this.parent.belowLimitGap * fontHeight + topOffset;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }
}

export default LimitSymbol;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import LeftBracket from "./leftBracket.js?7.0";
import LeftFloorWholeBracket from "./leftFloorWholeBracket.js?7.0";
import LeftFloorBottomBracket from "./leftFloorBottomBracket.js?7.0";
import LeftFloorMiddleBracket from "./leftFloorMiddleBracket.js?7.0";

class LeftFloorBracket extends LeftBracket {
    constructor(parent) {
        super(parent);
        this.className = "LeftFloorBracket";
        this.matchingClassName = 'RightFloorBracket';

        this.wholeBracket = new LeftFloorWholeBracket(parent, "MathJax_Main");
        this.topBracket = null;
        this.middleBrackets = [];
        this.bottomBracket = null;

        this.wholeBracket.parent = this;

        this.domObj = this.buildDomObj();
        this.domObj.append(this.wholeBracket.domObj);
        this.children = [this.wholeBracket];

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let widthVal = 0;
                    if (this.heightRatio <= 1.5) {
                        widthVal = 0.444444 * fontHeight;
                    } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
                        widthVal = 0.5777777 * fontHeight;
                    } else if (this.heightRatio > 2.4 && this.heightRatio <= 3) {
                        widthVal = 0.644444 * fontHeight;
                    } else {
                        widthVal = 0.666666 * fontHeight;
                    }
                    return widthVal;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let heightVal = 0;
                    if (this.heightRatio <= 1.5) {
                        heightVal = fontHeight;
                    } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
                        heightVal = 2.4 * fontHeight;
                    } else if (this.heightRatio > 2.4 && this.heightRatio <= 3) {
                        heightVal = 3 * fontHeight;
                    } else {
                        heightVal = (0.6 + (0.45 * (this.middleBrackets.length - 1))) * fontHeight;
                    }
                    return heightVal;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );
    }

    buildDomObj() {
        return new EquationDom(this, '<div class="bracket leftBracket leftFloorBracket"></div>');
    }

    updateBracketStructure() {
        this.domObj.empty();
        this.wholeBracket = null;
        this.topBracket = null;
        this.middleBrackets = [];
        this.bottomBracket = null;
        this.children = [];
        if (this.heightRatio <= 1.5) {
            this.wholeBracket = new LeftFloorWholeBracket(this.parent, "MathJax_Main");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
            this.wholeBracket = new LeftFloorWholeBracket(this.parent, "MathJax_Size3");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        } else if (this.heightRatio > 2.4 && this.heightRatio <= 3) {
            this.wholeBracket = new LeftFloorWholeBracket(this.parent, "MathJax_Size4");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        } else {
            const numberOfMiddleBrackets = Math.ceil((this.heightRatio - 0.6) / 0.45) + 1;
            this.bottomBracket = new LeftFloorBottomBracket(this.parent);
            this.bottomBracket.parent = this;
            this.domObj.append(this.bottomBracket.domObj);
            for (let i = 0; i < numberOfMiddleBrackets; i++) {
                const middleBracket = new LeftFloorMiddleBracket(this.parent, i);
                middleBracket.parent = this;
                this.domObj.append(middleBracket.domObj);
                this.middleBrackets.push(middleBracket);
            }
            this.children = [...this.middleBrackets, this.bottomBracket];
        }
    }
}

export default LeftFloorBracket;
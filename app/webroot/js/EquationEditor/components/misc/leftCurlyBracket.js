/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import LeftBracket from "./leftBracket.js?7.0";
import LeftCurlyWholeBracket from "./leftCurlyWholeBracket.js?7.0";
import LeftCurlyTopBracket from "./leftCurlyTopBracket.js?7.0";
import LeftCurlyMiddleBracket from "./leftCurlyMiddleBracket.js?7.0";
import LeftCurlyBottomBracket from "./leftCurlyBottomBracket.js?7.0";

class LeftCurlyBracket extends LeftBracket {
    constructor(parent) {
        super(parent);
        this.className = "LeftCurlyBracket";
        this.matchingClassName = 'RightCurlyBracket';
        
        this.wholeBracket = new LeftCurlyWholeBracket(this.parent, "MathJax_Main");
        this.topBracket = null;
        this.middleBrackets = [];
        this.bottomBracket = null;
        
        this.wholeBracket.parent = this;
        
        this.domObj = this.buildDomObj();
        this.domObj.append(this.wholeBracket.domObj);
        this.children = [this.wholeBracket];
        
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() { return width; },
                set(value) { width = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let widthVal = 0;
                    if (this.heightRatio <= 1.5) {
                        widthVal = 0.511111 * fontHeight;
                    } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
                        widthVal = 0.755555 * fontHeight;
                    } else if (this.heightRatio > 2.4 && this.heightRatio <= 3) {
                        widthVal = 0.8 * fontHeight;
                    } else if (this.heightRatio > 3 && this.heightRatio <= 3.33) {
                        widthVal = 0.666666 * fontHeight;
                    } else {
                        widthVal = 0.888888 * fontHeight;
                    }
                    return widthVal;
                },
                updateDom() { this.domObj.updateWidth(this.width); }
            })
        );
        
        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() { return height; },
                set(value) { height = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let heightVal = 0;
                    if (this.heightRatio <= 1.5) {
                        heightVal = fontHeight;
                    } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
                        heightVal = 2.4 * fontHeight;
                    } else if (this.heightRatio > 2.4 && this.heightRatio <= 3) {
                        heightVal = 3 * fontHeight;
                    } else if (this.heightRatio > 3 && this.heightRatio <= 3.33) {
                        heightVal = 3.33 * fontHeight;
                    } else {
                        const bottomBracketTop = this.bottomBracket.top / fontHeight;
                        heightVal = (bottomBracketTop + 1.652778 - this.padTop) * fontHeight;
                    }
                    return heightVal;
                },
                updateDom() { this.domObj.updateHeight(this.height); }
            })
        );
    }
    
    buildDomObj() {
        return new EquationDom(this, '<div class="bracket leftBracket leftCurlyBracket"></div>');
    }
    
    updateBracketStructure() {
        this.domObj.empty();
        this.wholeBracket = null;
        this.topBracket = null;
        this.middleBrackets = [];
        this.bottomBracket = null;
        this.children = [];
        if (this.heightRatio <= 1.5) {
            this.wholeBracket = new LeftCurlyWholeBracket(this.parent, "MathJax_Main");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
            this.wholeBracket = new LeftCurlyWholeBracket(this.parent, "MathJax_Size3");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        } else if (this.heightRatio > 2.4 && this.heightRatio <= 3) {
            this.wholeBracket = new LeftCurlyWholeBracket(this.parent, "MathJax_Size4");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        } else {
            let numberOfMiddleBrackets = Math.round((this.heightRatio - 3.4) / 0.231);
            // Ensure an even number of middle brackets.
            if (numberOfMiddleBrackets % 2 !== 0) {
                numberOfMiddleBrackets++;
            }
            this.topBracket = new LeftCurlyTopBracket(this.parent);
            this.bottomBracket = new LeftCurlyBottomBracket(this.parent);
            this.topBracket.parent = this;
            this.bottomBracket.parent = this;
            this.domObj.append(this.topBracket.domObj);
            this.domObj.append(this.bottomBracket.domObj);
            for (let i = 0; i < Math.round(0.5 * numberOfMiddleBrackets); i++) {
                let middleBracket = new LeftCurlyMiddleBracket(this.parent, i, "middleVert");
                middleBracket.parent = this;
                this.domObj.append(middleBracket.domObj);
                this.middleBrackets.push(middleBracket);
            }
            const middleCurly = new LeftCurlyMiddleBracket(this.parent, Math.round(0.5 * numberOfMiddleBrackets), "middleCurly");
            middleCurly.parent = this;
            this.domObj.append(middleCurly.domObj);
            this.middleBrackets.push(middleCurly);
            for (let i = Math.round(0.5 * numberOfMiddleBrackets) + 1; i < numberOfMiddleBrackets + 1; i++) {
                let middleBracket = new LeftCurlyMiddleBracket(this.parent, i, "middleVert");
                middleBracket.parent = this;
                this.domObj.append(middleBracket.domObj);
                this.middleBrackets.push(middleBracket);
            }
            this.children = [this.topBracket].concat(this.middleBrackets);
        }
    }
}

export default LeftCurlyBracket;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class Bracket extends BoundEquationComponent {
    constructor(parent) {
        super(parent);
        this.className = "Bracket";

        this.adjustTop = 0.025;

        // Set up the heightRatio calculation.
        let heightRatio = 0;
        this.properties.push(
            new Property(this, "heightRatio", heightRatio, {
                get() {
                    return heightRatio;
                },
                set(value) {
                    heightRatio = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    return this.desiredHeight / fontHeight;
                },
                updateDom() {
                    // Not only a DOM update, but a convenient callback.
                    this.updateBracketStructure();
                }
            })
        );

        // Set up the top calculation.
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    let topVal = 0;
                    if (this.parent.className == 'BracketPairWrapper') {
                        if (this.parent.bracketContainer.wrappers.length > 0) {
                            const containerTopAlign = this.parent.bracketContainer.wrappers[this.parent.bracketContainer.maxTopAlignIndex].topAlign;
                            const bracketTopAlign = 0.5 * this.height;
                            if (bracketTopAlign < containerTopAlign) {
                                topVal = containerTopAlign - bracketTopAlign;
                            }
                        }
                    }
                    return topVal;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        // Set up the left calculation.
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    let leftVal = 0;
                    if (this.parent.className == 'BracketPairWrapper' && this.isRightBracket) {
                        leftVal = this.parent.leftBracket.width + this.parent.bracketContainer.width;
                    }
                    return leftVal;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );
    }

    clone() {
        const copy = new this.constructor(this.parent);
        copy.domObj = copy.buildDomObj();
        if (this.wholeBracket !== null) {
            copy.wholeBracket = this.wholeBracket.clone();
            copy.wholeBracket.parent = copy;
            copy.domObj.append(copy.wholeBracket.domObj);
            copy.children.push(copy.wholeBracket);
        } else {
            copy.wholeBracket = null;
        }
        if (this.topBracket !== null) {
            copy.topBracket = this.topBracket.clone();
            copy.topBracket.parent = copy;
            copy.domObj.append(copy.topBracket.domObj);
            copy.children.push(copy.topBracket);
        } else {
            copy.topBracket = null;
        }
        copy.middleBrackets = [];
        for (let i = 0; i < this.middleBrackets.length; i++) {
            const middleBracket = this.middleBrackets[i].clone();
            middleBracket.parent = copy;
            copy.domObj.append(middleBracket.domObj);
            copy.middleBrackets.push(middleBracket);
            copy.children.push(middleBracket);
        }
        if (this.bottomBracket !== null) {
            copy.bottomBracket = this.bottomBracket.clone();
            copy.bottomBracket.parent = copy;
            copy.domObj.append(copy.bottomBracket.domObj);
            copy.children.push(copy.bottomBracket);
        } else {
            copy.bottomBracket = null;
        }
        this.childNoncontainers = [this.wholeBracket];
        return copy;
    }
}

export default Bracket;
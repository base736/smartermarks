/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import MiddleBracket from "./middleBracket.js?7.0";

class LeftCurlyMiddleBracket extends MiddleBracket {
    constructor(parent, index, characterType) {
        super(parent, index);
        this.className = "LeftCurlyMiddleBracket";
        this.characterType = characterType;
        if (this.characterType === "middleVert") {
            this.character = "⎪";
        } else if (this.characterType === "middleCurly") {
            this.character = "⎨";
        }
        this.fontStyle = "MathJax_Size4";
        this.domObj = this.buildDomObj();

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.parent.fontSize);
                    const numSegs = this.parent.middleBrackets.length - 1;
                    const adjustTopFactor = 0.231;
                    if (this.index < Math.round(numSegs / 2)) {
                        return ((this.index + 1) * adjustTopFactor + 0.15) * fontHeight;
                    } else if (this.index === Math.round(numSegs / 2)) {
                        return (this.index * adjustTopFactor + 1.1 + 0.15) * fontHeight;
                    } else {
                        const centerBracket = Math.round(numSegs / 2) * adjustTopFactor + 1.1 + 0.15;
                        return (centerBracket + 0.878 + (this.index - Math.round(numSegs / 2) - 1) * adjustTopFactor) * fontHeight;
                    }
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }
}

export default LeftCurlyMiddleBracket;
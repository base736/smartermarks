/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class BigOperatorSymbol extends BoundEquationComponent {
    constructor(parent) {
        super(parent);
        this.className = "BigOperatorSymbol";
        this.domObj = this.buildDomObj();

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get: () => left,
                set: (value) => { left = value; },
                compute: () => {
                    let leftVal = 0;
                    if (this.parent.isInline) {
                        leftVal = 0;
                    } else {
                        // Assume this.parent.upperLimitContainer and lowerLimitContainer exist and have width.
                        const widths = [];
                        if (this.parent.hasUpperLimit) widths.push(this.parent.upperLimitContainer.width);
                        if (this.parent.hasLowerLimit) widths.push(this.parent.lowerLimitContainer.width);
                        widths.push(this.parent.symbol.width);
                        const maxWidth = Math.max(...widths);
                        leftVal = 0.5 * (maxWidth - this.width);
                    }
                    return leftVal;
                },
                updateDom: () => { this.domObj.updateLeft(this.left); }
            })
        );

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get: () => top,
                set: (value) => { top = value; },
                compute: () => {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    return this.parent.topAlign - 0.5 * this.height;
                },
                updateDom: () => { this.domObj.updateTop(this.top); }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get: () => height,
                set: (value) => { height = value; },
                compute: () => {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    return 1.5 * fontHeight;
                },
                updateDom: () => { this.domObj.updateHeight(this.height); }
            })
        );
    }

    buildDomObj() {
        return new EquationDom(this, '<div class="bigOperatorSymbol"></div>');
    }
}

export default BigOperatorSymbol;
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class Variable extends BoundEquationComponent {
    constructor(parent, data) {
        super(parent);
        this.className = "Variable";
        this.value = data.value;
        this.displayText = this.value;
        this.isRendered = false;
        this.fontStyle = "fontStyleVariable";
        this.adjustTop = 0.0;

        if ("displayValue" in data) {
            // Rendered value in new documents
            this.displayText = data.displayValue;
            this.isRendered = true;
            this.fontStyle = "MathJax_Main";
            this.adjustTop = 0.025;
        } else if ("displayName" in data) {
            // Unrendered variable
            this.displayText = data.displayName;
        }

        this.paddingLR = 0;
        this.paddingTB = 0;

        this.domObj = this.buildDomObj();

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    return 0;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    return 0;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    const widthVal = FontMetrics.getWidth(this.displayText, this.fontStyle, this.fontSize);
                    return widthVal + 2 * this.paddingLR + 1;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    let lineHeight = this.getFontHeight();
                    if (!this.isRendered) lineHeight *= 1.2;
                    return Math.floor(lineHeight) + 2 * this.paddingTB;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );

        let fontSize = "";
        this.properties.push(
            new Property(this, "fontSize", fontSize, {
                get() {
                    return fontSize;
                },
                set(value) {
                    fontSize = value;
                },
                compute() {
                    let fontSizeVal = "";
                    let actualParentContainer = this.parent.parent;
                    while (actualParentContainer?.className == 'BracketContainer') {
                        actualParentContainer = actualParentContainer.parent.parent;
                    }
                    if (this.isRendered) {
                        fontSizeVal = actualParentContainer.fontSize;
                        this.paddingLR = 0;
                        this.paddingTB = 0;
                    } else {
                        if (
                            actualParentContainer.fontSize === "fontSizeSmaller" ||
                            actualParentContainer.fontSize === "fontSizeSmallest"
                        ) {
                            fontSizeVal = "fontSizeVariableSmall";
                            this.paddingLR = 3;
                            this.paddingTB = 2;
                        } else {
                            fontSizeVal = "fontSizeVariable";
                            this.paddingLR = 4;
                            this.paddingTB = 3;
                        }
                    }
                    return fontSizeVal;
                },
                updateDom() {
                    this.domObj.updateFontSize(this.fontSize);
                }
            })
        );
    }

    buildDomObj() {
        return new EquationDom(this, `<div class="block_variable${this.isRendered ? " rendered" : ""}" data-variable-name="${this.value}">${this.displayText}</div>`);
    }
}

export default Variable;
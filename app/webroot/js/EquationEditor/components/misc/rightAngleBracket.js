/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import RightBracket from "./rightBracket.js?7.0";
import RightAngleWholeBracket from "./rightAngleWholeBracket.js?7.0";

class RightAngleBracket extends RightBracket {
    constructor(parent) {
        super(parent);
        this.className = "RightAngleBracket";
        this.matchingClassName = 'LeftAngleBracket';
        
        this.wholeBracket = new RightAngleWholeBracket(parent, "MathJax_Main");
        this.topBracket = null;
        this.middleBrackets = [];
        this.bottomBracket = null;
        this.wholeBracket.parent = this;

        this.domObj = this.buildDomObj();
        this.domObj.append(this.wholeBracket.domObj);
        this.children = [this.wholeBracket];

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() { return width; },
                set(value) { width = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let widthVal = 0;
                    if (this.heightRatio <= 1.5) {
                        widthVal = 0.377777 * fontHeight;
                    } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
                        widthVal = 0.733333 * fontHeight;
                    } else {
                        widthVal = 0.777777 * fontHeight;
                    }
                    return widthVal;
                },
                updateDom() { this.domObj.updateWidth(this.width); }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() { return height; },
                set(value) { height = value; },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    let heightVal = 0;
                    if (this.heightRatio <= 1.5) {
                        heightVal = fontHeight;
                    } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
                        heightVal = 2.4 * fontHeight;
                    } else {
                        heightVal = 3 * fontHeight;
                    }
                    return heightVal;
                },
                updateDom() { this.domObj.updateHeight(this.height); }
            })
        );
    }

    buildDomObj() {
        return new EquationDom(this, '<div class="bracket leftBracket RightAngleBracket"></div>');
    }

    updateBracketStructure() {
        this.domObj.empty();
        this.wholeBracket = null;
        this.topBracket = null;
        this.middleBrackets = [];
        this.bottomBracket = null;
        this.children = [];
        if (this.heightRatio <= 1.5) {
            this.wholeBracket = new RightAngleWholeBracket(this.parent, "MathJax_Main");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        } else if (this.heightRatio > 1.5 && this.heightRatio <= 2.4) {
            this.wholeBracket = new RightAngleWholeBracket(this.parent, "MathJax_Size3");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        } else {
            this.wholeBracket = new RightAngleWholeBracket(this.parent, "MathJax_Size4");
            this.wholeBracket.parent = this;
            this.domObj.append(this.wholeBracket.domObj);
            this.children = [this.wholeBracket];
        }
    }
}

export default RightAngleBracket;
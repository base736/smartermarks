/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

class EquationComponent {
    constructor() {
        this.className = "EquationComponent";
        // Reference to the root equation object.
        this.equation = null;
        // Parent component (null for the root container).
        this.parent = null;
        // Array for Property instances used in dependency resolution.
        this.properties = [];
        // Basic geometric properties.
        this.left = 0;
        this.top = 0;
        this.width = 0;
        this.height = 0;
        // Padding values (included in width calculations).
        this.padLeft = 0;
        this.padRight = 0;
        this.padTop = 0;
        this.padBottom = 0;
        // Adjustments that affect absolute placement but not width.
        this.adjustLeft = 0;
        this.adjustTop = 0;
        // The DOM representation (to be populated via buildDomObj).
        this.domObj = null;
        // Default children array for non-container/wrapper components.
        this.children = [];
    }

    // Must be implemented by subclasses to support deep cloning.
    clone() {
        return new this.constructor();
    }

    // Should be overridden to create the corresponding DOM object.
    buildDomObj() {}

    // Recursively compute all properties and update children.
    update() {
        for (let i = 0; i < this.properties.length; i++) {
            this.properties[i].compute();
        }
        for (let i = 0; i < this.children.length; i++) {
            this.children[i].update();
        }
    }

    // Recursively update the entire equation.
    updateAll() {
        // Begin dependency scanning for Property.
        Property.beginComputing();
        // Update the root element.
        let rootElement = this.equation;
        if (rootElement !== null) rootElement.update();
        // End dependency scanning.
        Property.endComputing();
    }

    // Returns the font height based on the first defined fontSize in the parent chain.
    getFontHeight() {
        let context = this;
        while (typeof context.fontSize === "undefined") {
            context = context.parent;
        }
        return FontMetrics.getHeight(context.fontSize);
    }
}

export default EquationComponent;
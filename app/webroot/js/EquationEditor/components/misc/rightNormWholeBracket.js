/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";

import WholeBracket from "./wholeBracket.js?7.0";

class RightNormWholeBracket extends WholeBracket {
    constructor(parent, fontStyle) {
        super(parent);
        this.className = "RightNormWholeBracket";
        this.character = "∥";
        this.fontStyle = fontStyle;
        this.domObj = this.buildDomObj();
        this.adjustTop = 0;
        if (this.fontStyle === "MathJax_Main") {
            this.adjustTop = -0.0625;
        } else if (this.fontStyle === "MathJax_Size3") {
            this.adjustTop = 0.7;
        } else if (this.fontStyle === "MathJax_Size4") {
            this.adjustTop = 0.995;
        }
        
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() { return width; },
                set(value) { width = value; },
                compute() { return 0; },
                updateDom() { this.domObj.updateWidth(this.width); }
            })
        );
        
        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() { return height; },
                set(value) { height = value; },
                compute() { return 0; },
                updateDom() { this.domObj.updateHeight(this.height); }
            })
        );
        
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() { return left; },
                set(value) { left = value; },
                compute() { return 0; },
                updateDom() { this.domObj.updateLeft(this.left); }
            })
        );
        
        let topProp = 0;
        this.properties.push(
            new Property(this, "top", topProp, {
                get() { return topProp; },
                set(value) { topProp = value; },
                compute() { return 0; },
                updateDom() { this.domObj.updateTop(this.top); }
            })
        );
    }
}

export default RightNormWholeBracket;
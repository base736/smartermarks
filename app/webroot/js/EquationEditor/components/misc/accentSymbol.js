/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class AccentSymbol extends BoundEquationComponent {
    constructor(parent, character, fontStyle) {
        super(parent);
        this.className = "AccentSymbol";
        this.character = character;
        this.fontStyle = fontStyle;
        this.domObj = this.buildDomObj();
        this.heightRatio = 0.055;

        // Add appropriate accent classes based on character.
        if (this.character === '˙') {
            this.domObj.addClass('dotAccent');
        } else if (this.character === '^') {
            this.domObj.addClass('hatAccent');
        } else if (this.character === '➡') {
            this.domObj.addClass('vectorAccent');
        } else if (this.character === '↔') {
            this.domObj.addClass('lineAccent');
        } else if (this.character === '¯') {
            this.domObj.addClass('barAccent');
        }

        // left calculation
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get: () => left,
                set: (value) => { left = value; },
                compute: () => {
                    let leftOffset;
                    if (this.parent.accentGap !== null) {
                        const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                        const accentHeight = (1.0 + this.parent.accentGap) * fontHeight;
                        leftOffset = this.heightRatio * accentHeight;
                    } else {
                        leftOffset = 0.0;
                    }
                    return leftOffset;
                },
                updateDom: () => { this.domObj.updateLeft(this.left); }
            })
        );

        // top calculation
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get: () => top,
                set: (value) => { top = value; },
                compute: () => {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const topVal = -((this.parent.accentGap === null) ? 0.20 : this.parent.accentGap) * fontHeight;
                    return topVal;
                },
                updateDom: () => { this.domObj.updateTop(this.top); }
            })
        );

        // width calculation
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get: () => width,
                set: (value) => { width = value; },
                compute: () => {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const newWidth = this.parent.accentContainer.width - this.heightRatio * fontHeight;
                    return newWidth;
                },
                updateDom: () => {
                    this.domObj.updateWidth(this.width);
                    this.redrawDomObj();
                }
            })
        );

        // height calculation
        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get: () => height,
                set: (value) => { height = value; },
                compute: () => 3.0,
                updateDom: () => {
                    this.domObj.updateHeight(this.height);
                    this.redrawDomObj();
                }
            })
        );
    }

    redrawDomObj() {
        const boxHeight = 1.0;
        const boxWidth = this.width / this.height;
        let html = `<svg style="width:100%; height:100%; overflow:visible;" viewBox="0 0 ${boxWidth} ${boxHeight}">`;

        if (this.character === '˙') {
            html += `<circle fill="currentColor" stroke="none" cx="${boxWidth / 2}" cy="${boxHeight / 2}" r="${boxHeight * 0.4}" />`;
        } else {
            let points = [];
            if (this.character === '^') {
                points = ['M', [0, 1], 'L', [0.5, 0.5], 'L', [1, 1]];
            } else if (this.character === '➡') {
                points = ['M', [0, 0.5], 'L', [1, 0.5],
                                    'm', [-0.61455, -0.5],
                                    'c', [0.26432, 0.30062], [0.49092, 0.38707], [0.61455, 0.5],
                                    [-0.19302, 0.14040], [-0.39660, 0.23843], [-0.56115, 0.5]];
            } else if (this.character === '↔') {
                points = ['M', [0, 0.5], 'L', [1, 0.5],
                                    'm', [-0.61455, -0.5],
                                    'c', [0.26432, 0.30062], [0.49092, 0.38707], [0.61455, 0.5],
                                    [-0.19302, 0.14040], [-0.39660, 0.23843], [-0.56115, 0.5],
                                    'M', [0, 0.5],
                                    'm', [0.61455, -0.5],
                                    'c', [-0.26432, 0.30062], [-0.49092, 0.38707], [-0.61455, 0.5],
                                    [0.19302, 0.14040], [0.39660, 0.23843], [0.56115, 0.5]];
            } else if (this.character === '¯') {
                points = ['M', [0, 0.7], 'L', [1, 0.7]];
            }
        
            const strokeWidth = 1.0 / this.height;
            let isAbsolute = true;
            let pathString = '';
            for (let i = 0; i < points.length; i++) {
                if (typeof points[i] === 'string') {
                    isAbsolute = (points[i] === points[i].toUpperCase());
                    if (pathString.length > 0) pathString += ' ';
                    pathString += points[i];
                } else {
                    if (isAbsolute) {
                        points[i][0] = points[i][0] * boxWidth;
                        points[i][1] = points[i][1] * boxHeight;
                    }
                    pathString += ` ${points[i][0]},${points[i][1]}`;
                }
            }
            html += `<path stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round" fill="none" stroke="currentColor" d="${pathString}" />`;
        }

        html += '</svg>';
        this.domObj.value.html(html);
    }

    buildDomObj() {
        return new EquationDom(this, '<div class="accentSymbol"></div>');
    }
}

export default AccentSymbol;
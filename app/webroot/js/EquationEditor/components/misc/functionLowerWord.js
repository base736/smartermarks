/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import Word from "./word.js?7.0";

class FunctionLowerWord extends Word {
    constructor(parent, characters, fontStyle) {
        super(parent, characters, fontStyle);
        this.className = "FunctionLowerWord";

        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const leftOffset = 0.5 * ((this.parent.width - (this.parent.padLeft + this.parent.padRight) * fontHeight) - this.width);
                    return leftOffset;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    return 0;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }
}

export default FunctionLowerWord;
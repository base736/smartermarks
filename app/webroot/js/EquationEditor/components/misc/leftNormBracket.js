/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import Property from "../../property.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";

import LeftBracket from "./leftBracket.js?7.0";
import LeftNormWholeBracket from "./leftNormWholeBracket.js?7.0";
import LeftNormMiddleBracket from "./leftNormMiddleBracket.js?7.0";

class LeftNormBracket extends LeftBracket {
    constructor(parent) {
        super(parent);
        this.className = "LeftNormBracket";
        this.matchingClassName = 'RightNormBracket';
        
        this.wholeBracket = new LeftNormWholeBracket(parent, "MathJax_Main");
        this.topBracket = null;
        this.middleBrackets = [];
        this.bottomBracket = null;

        this.wholeBracket.parent = this;

        this.domObj = this.buildDomObj();
        this.domObj.append(this.wholeBracket.domObj);
        this.children = [this.wholeBracket];

        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    return 0.4888888 * fontHeight;
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    const fontHeight = FontMetrics.getHeight(this.parent.parent.fontSize);
                    const numBrackets = Math.ceil((this.heightRatio - 1.07) / 0.5) + 1;
                    return (1.07 + (0.5 * (numBrackets - 1))) * fontHeight;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );
    }

    buildDomObj() {
        return new EquationDom(this, '<div class="bracket leftBracket leftNormBracket"></div>');
    }

    updateBracketStructure() {
        this.domObj.empty();
        this.wholeBracket = null;
        this.topBracket = null;
        this.middleBrackets = [];
        this.bottomBracket = null;
        this.children = [];
        const numberOfMiddleBrackets =
            Math.ceil((this.heightRatio - 1.07) / 0.5) + 1;
        for (let i = 0; i < numberOfMiddleBrackets; i++) {
            const middleBracket = new LeftNormMiddleBracket(this.parent, i);
            middleBracket.parent = this;
            this.domObj.append(middleBracket.domObj);
            this.middleBrackets.push(middleBracket);
        }
        this.children = this.middleBrackets;
    }
}

export default LeftNormBracket;
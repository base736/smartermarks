/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";
import FontMetrics from "../../fontMetrics.js?7.0";
import Property from "../../property.js?7.0";

import BoundEquationComponent from "./boundEquationComponent.js?7.0";

class Symbol extends BoundEquationComponent {
    constructor(parent, character, fontStyle) {
        super(parent);
        this.className = "Symbol";
        this.character = character;
        this.fontStyle = fontStyle;
        this.domObj = this.buildDomObj();

        if (this.fontStyle === "MathJax_MathItalic") {
            this.adjustTop = 0.085;
        } else {
            this.adjustTop = 0.025;
        }

        // Set up the width calculation
        let width = 0;
        this.properties.push(
            new Property(this, "width", width, {
                get() {
                    return width;
                },
                set(value) {
                    width = value;
                },
                compute() {
                    let fontSize;
                    if (this.character === '′') {
                        fontSize = "fontSizeSmaller";
                    } else {
                        fontSize = this.parent.parent.fontSize;
                    }
                    return FontMetrics.getWidth(this.character, this.fontStyle, fontSize);
                },
                updateDom() {
                    this.domObj.updateWidth(this.width);
                }
            })
        );

        // Set up the height calculation
        let height = 0;
        this.properties.push(
            new Property(this, "height", height, {
                get() {
                    return height;
                },
                set(value) {
                    height = value;
                },
                compute() {
                    const fontHeight = this.getFontHeight();
                    return fontHeight;
                },
                updateDom() {
                    this.domObj.updateHeight(this.height);
                }
            })
        );

        // Set up the left calculation
        let left = 0;
        this.properties.push(
            new Property(this, "left", left, {
                get() {
                    return left;
                },
                set(value) {
                    left = value;
                },
                compute() {
                    return 0;
                },
                updateDom() {
                    this.domObj.updateLeft(this.left);
                }
            })
        );

        // Set up the top calculation
        let top = 0;
        this.properties.push(
            new Property(this, "top", top, {
                get() {
                    return top;
                },
                set(value) {
                    top = value;
                },
                compute() {
                    return (this.character === '′') ? -2 : 0;
                },
                updateDom() {
                    this.domObj.updateTop(this.top);
                }
            })
        );
    }

    buildDomObj() {
        if (this.character === '′') {
            return new EquationDom(this, `<div class="symbol ${this.fontStyle} fontSizeSmaller">${this.character}</div>`);
        } else {
            return new EquationDom(this, `<div class="symbol ${this.fontStyle}">${this.character}</div>`);
        }
    }
}

export default Symbol;
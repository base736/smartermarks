/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import EquationDom from "../dom/equationDom.js?7.0";

import BigOperatorSymbol from "./bigOperatorSymbol.js?7.0";

class BigCupBigOperatorSymbol extends BigOperatorSymbol {
    constructor(parent) {
        super(parent);
        this.className = "BigCupBigOperatorSymbol";
        this.domObj = this.buildDomObj();
        // You can add similar property calculations if needed. (Original code details any specific computations.)
    }

    buildDomObj() {
        const htmlRep = 
            `<div class="bigOperatorSymbol bigCupBigOperatorSymbol" style="width: 200px; height: 279.78879px;">
                <svg style="position: absolute; width: 100%; height: 100%;" viewBox="0 0 200 279.78879" preserveAspectRatio="none">
                    <g transform="translate(-259.34158,-210.57319)">
                        <g>
                            <path d="M...Z"/>
                        </g>
                    </g>
                </svg>
            </div>`;
        return new EquationDom(this, htmlRep);
    }
}

export default BigCupBigOperatorSymbol;
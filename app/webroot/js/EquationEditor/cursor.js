/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class Cursor {

    constructor($parent) {
        this.$parent = $parent;
        this.details = null;
        this.cursorBlinkTimers = [];
        this.moveCallback = null;
    
        $parent.on('touchstart mousedown', '.equationContainer', (e) => {
            e.stopPropagation();
    
            const container = $(e.currentTarget).data('eqObject');
            let xOffset = (typeof e.originalEvent.pageX !== 'undefined')
                ? e.originalEvent.pageX 
                : e.originalEvent.touches[0].pageX;
            let characterClickPos = xOffset - container.domObj.value.offset().left;
            
            this.addCursorAtClick(container, characterClickPos);
    
            // Attach drag handler
            $(document).on('mousemove.cursorDrag touchmove.cursorDrag', (ev) => {
                let pageX = ev.pageX || ev?.originalEvent?.touches[0].pageX;
                if (pageX) {
                    let xOff = pageX - container.domObj.value.offset().left;
                    let index = this._calculateIndex(container, xOff);
                    this.setHighlightEndIndex(index);
                }
            });
    
            // Remove the drag handler when mouse is released
            $(document).on('mouseup.cursorDrag touchend.cursorDrag', () => {
                $(document).off('.cursorDrag');
            });
        });

        $parent.on('mousemove', '.equationContainer', function(e) {
            var $container = $(this);
            var container = $container.data("eqObject");

            $container.removeClass('hoverContainer');
        
            // Check that the container is not active or top-level,
            // and that it isn’t a square empty container (similar to the old check)
            // Also, don't add hover if a highlight is already in place.
            
            if (
                !$container.hasClass('activeContainer') &&
                !$container.hasClass('topLevelContainer') &&
                container && container.wrappers && container.wrappers.length > 0 &&
                container.wrappers[0].className !== 'SquareEmptyContainerWrapper' &&
                $('.highlighted').length === 0
            ) {
                $container.addClass('hoverContainer');
            }
        });
        
        $parent.on('mouseleave', '.equationContainer', function(e) {
            $(this).removeClass('hoverContainer');
        });
    }

    initialize() {
        let container = this.$parent.find('.eqEdEquation').data("eqObject").children[0];
        this.addCursorAtIndex(container, 0);
    }

    delete() {
        this.unrender();
        this.details = null;
    }

    _calculateIndex(container, offsetLeft) {
        let cumulative = 0;
        for (let i = 0; i < container.wrappers.length; i++) {
            cumulative += 0.5 * container.wrappers[i].width;
            if (offsetLeft < cumulative) return i;
            cumulative += 0.5 * container.wrappers[i].width;
        }
        return container.wrappers.length;
    }

    addCursorAtClick(container, characterClickPos) {
        let cursorIndex = this._calculateIndex(container, characterClickPos);
        this.addCursorAtIndex(container, cursorIndex);
    }

    addCursorAtIndex(container, index) {

        // Create a new cursor state.

        this.details = {
            container: container,
            highlightStartIndex: index,
            highlightEndIndex: index
        };
        
        this.render();
        if (this.moveCallback) this.moveCallback();
    }
    
    _toggleCursorVisibility() {
        $('.cursor').toggleClass('cursorOff');
    }

    addBlink() {
        let intervalId = window.setInterval(this._toggleCursorVisibility, 750);
        this.cursorBlinkTimers.push(intervalId);
    }

    removeBlink() {
        for (let i = 0; i < this.cursorBlinkTimers.length; i++) {
            window.clearInterval(this.cursorBlinkTimers[i]);
        }
        this.cursorBlinkTimers = [];
        $('.cursorOff').removeClass('cursorOff');
    }

    _calculateCursorLeft(container, index) {
        let cumulative = 0;
        let cursorLeft = -1;
        let cursorLeftSet = false;
        if (!(container.wrappers[0]?.className === 'TopLevelEmptyContainerWrapper')) {
            for (let i = 0; i < container.wrappers.length; i++) {
                let wrapper = container.wrappers[i];
                if (i === index && !cursorLeftSet) {
                    cursorLeft += cumulative;
                    cursorLeftSet = true;
                }
                cumulative += 0.5 * wrapper.width;
                cumulative += 0.5 * wrapper.width;
            }
        }
        if (!cursorLeftSet) {
            cursorLeft += cumulative;
        }
        return cursorLeft;
    }

    unrender() {
        $('.highlighted').removeClass('highlighted');
        this.$parent.find('.activeContainer').removeClass('activeContainer');
        this.$parent.find('.highlight').remove();
        this.$parent.find('.cursor').remove();
        this.removeBlink();
    }

    render() {        
        this.unrender();
        if (!this.details) return;

        const container = this.details.container;
        if (container.className !== 'TopLevelContainer') {
            container.domObj.value.addClass('activeContainer');
        }

        if (this.details.highlightStartIndex === this.details.highlightEndIndex) {

            // Selection is collapsed -- draw cursor

            const cursorLeft = this._calculateCursorLeft(container, this.details.highlightStartIndex);

            let $cursor;
            if (container.className === 'SquareEmptyContainer') {
                $cursor = $('<div class="cursor squareCursor"></div>');
            } else {
                $cursor = $('<div class="cursor normalCursor"></div>');
                $cursor.css('left', cursorLeft);
            }
            container.domObj.value.append($cursor);
            this.addBlink();
            
            if ($('.cursor').closest('.textWrapper').length > 0) {
                this.$parent.find('.edit_button').addClass('disabled');
            } else {
                this.$parent.find('.edit_button').removeClass('disabled');			
            }

        } else {

            // Create highlight if it doesn't exist already

            let $highlight = container.domObj.value.children('.highlight');
            if ($highlight.length === 0) {
                let highlight = $('<div class="highlight"></div>');
                container.domObj.value.css('z-index', 4);
                highlight.css('z-index', 5);
                container.domObj.value.children().css('z-index', 6);
                container.domObj.value.append(highlight);
        
                $highlight = container.domObj.value.children('.highlight');
            }

            const firstIndex = Math.min(this.details.highlightStartIndex, this.details.highlightEndIndex);
            const lastIndex = Math.max(this.details.highlightStartIndex, this.details.highlightEndIndex);

            const left = container.wrappers[firstIndex].left;
            const top = 0;
            const height = container.height;

            let width = 0;
            for (let i = firstIndex; i < lastIndex; i++) {
                let wrapper = container.wrappers[i];
                wrapper.domObj.value.addClass('highlighted');
                width += wrapper.width;
            }

            $highlight.css({ left: left, top: top, height: height, width: width });

            if ($('.textWrapper .highlighted').length > 0) {
                this.$parent.find('.edit_button').addClass('disabled');
            } else {
                this.$parent.find('.edit_button').removeClass('disabled');			
                this.$parent.find('.edit_button_text').addClass('disabled');			
            }

        }
    }

    getDetails() {
        // Return a shallow copy of the cursor details
        return this.details ? { ...this.details } : null;
    }

    setDetails(newDetails) {
        this.details = newDetails ? { ...newDetails } : null;
        this.render();
        if (this.moveCallback) this.moveCallback();
    }

    setHighlightEndIndex(newIndex) {
        if (this.details) this.details.highlightEndIndex = newIndex;
        this.render();
        if (this.moveCallback) this.moveCallback();
    }
}

export default Cursor;
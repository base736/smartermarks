/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenderTools from '/js/assessments/render_tools.js?7.0';

import Equation from "./equation.js?7.0";
import Wrapper from "./components/wrappers/wrapper.js?7.0";
import Cursor from './cursor.js?7.0';

import { createTools } from './tools/index.js?7.0';
import { createMenus } from './menus/index.js?7.0';

import {
    TopLevelEmptyContainerWrapper, SquareEmptyContainerWrapper,
    CursorMarkerWrapper
} from './components/wrappers/index.js?7.0';

var eqnEditInitializing = false;
var eqnEditInitialized = false;

var internalClipboard = null;

const letterKeys = [
    'q','w','e','r','t','y','u','i','o','p',
    'a','s','d','f','g','h','j','k','l',
    'z','x','c','v','b','n','m',
    'Q','W','E','R','T','Y','U','I','O','P',
    'A','S','D','F','G','H','J','K','L',
    'Z','X','C','V','B','N','M'
];

const numberKeys = ['1','2','3','4','5','6','7','8','9','0'];

const symbolKeys = ['$','%', '.', ','];

const operatorKeys = ['*','-','+','/','<','>','!'];
const operatorKeysMap = {
    '-': '−',
    '/': '/',
    '*': '×',
    '+': '+',
    '<': '<',
    '>': '>',
    '!': '!'
};

const openBracketKeys = ['(', '[', '{', '|'];
const closeBracketKeys = [')',']','}','|'];
const bracketCharactersMap = {
    '(': "parenthesisBracket",
    '[': "squareBracket",
    '{': "curlyBracket",
    '|': "absValBracket",
    ')': "parenthesisBracket",
    ']': "squareBracket",
    '}': "curlyBracket"
};

var commonContext = { variables: {} };
function setCommonContext(newContext) {
    Object.keys(commonContext).forEach((key) => {
        delete commonContext[key];
    });
    Object.keys(newContext).forEach((key) => {
        commonContext[key] = newContext[key];
    });
}

async function initialize() {
    if (eqnEditInitializing) return;
    eqnEditInitializing = true;

    await RenderTools.initialize();

    $.widget("sm.equationeditor", {

        _savedStates: [],
        _savedStateIndex: -1,

        _commandMode: false,
        _commandText: "",
        _eventManager: null,
        _buttonShortcuts: null,
        _cursor: null,

        // Default options

        options: {
            restrictEquation: false,
            blockedKeys: []
        },
        
        _create: function() {

            this._cursor = new Cursor(this.element);
            this._cursor.moveCallback = this._cursorMoved.bind(this);

            const context = {
                common: commonContext,
                editor: {
                    $element: this.element,
                    cursor: this._cursor    
                }
            };

            this._tools = createTools(context);
            this._menus = createMenus(context);

            this.element.empty().append(
                '<div class="eqnLoadMessage fontSizeMessage" style="padding:0px 3px 6px;">Loading&hellip;</div>'
            );

            var $parent = $("<div tabindex='0' class='edit_parent' style='width:auto; height:auto; display:inline-block; outline:none;'></div>");
            $parent.insertBefore(this.element);
            $parent.tabindex = -1; // Enable focus on parent
                    
            // Set up toolbar

            $parent.css('margin', this.element.css('margin'));
            this.element.css('margin', '0px');

            var $bar = $("<div class='edit_bar' style='flex:0 1 auto;'></div>");
            $parent.append($bar);
            $parent.append(this.element);
            
            var $status = $("<div class='command_status' style='height:20px; background-color:white; font-size:10px; padding:0px 10px;'></div>");
            $parent.append($status);

            this.element.addClass('eqn_editor');

            $bar.append("<div style='float:left;'></div><div style='float:right;'></div>");
            var $barLeft = $bar.children().eq(0);
            var $barRight = $bar.children().eq(1);
                    
            var $sectionLeft = $("<div class='edit_bar_section edit_bar_section_font'></div>");
            $barLeft.append($sectionLeft);
            
            this._addTool($sectionLeft, 'insertText');
            this._addMenu($sectionLeft, 'insertVariable');
            this._addMenu($sectionLeft, 'insertConstant');
            this._addMenu($sectionLeft, 'insertFunction');
            this._addMenu($sectionLeft, 'insertSymbol');
            this._addMenu($sectionLeft, 'insertAccent');
            this._addTool($sectionLeft, 'insertTenToThe');
            this._addTool($sectionLeft, 'insertStackedFraction');
            this._addTool($sectionLeft, 'insertSuperscript');
            this._addMenu($sectionLeft, 'insertSupSub');
            this._addTool($sectionLeft, 'insertSquareRoot');
            this._addTool($sectionLeft, 'insertNthRoot');
            this._addMenu($sectionLeft, 'insertBrackets');
            this._addMenu($sectionLeft, 'insertMatrix');
            this._addMenu($sectionLeft, 'insertOther');
            
            // Hide menus
            
            $parent.find('.edit_button_menu').hide();

            var widget = this;

            // Set up shortcuts

            var $menuTargets = $bar.find(`.edit_button`);
            $menuTargets.find('.edit_button_menu').each(function() {
                const $menuButton = $(this).closest('.edit_button');
                const menuID = $menuButton.attr('data-menu-id');
                if ((menuID !== 'insertVariable') && (menuID !== 'insertConstant')) {
                    widget._menus[menuID].setMenu($(this));
                }
            });

            widget._buttonShortcuts = {};
            $bar.find('.edit_button, .edit_menu_button').each(function() {
                if (this.hasAttribute('data-shortcut')) {
                    const shortcutText = $(this).attr('data-shortcut');
                    widget._buttonShortcuts[shortcutText] = $(this);    
                }
            });

            // Set up keyboard events
            
            $parent.on('keydown', function(e) {
                return widget._handleKeydown(widget, e);
            });

            $parent.on('keypress', function(e) {
                const blocked = widget.options.blockedKeys;

                const cursorDetails = widget._cursor.getDetails();
                if (cursorDetails !== null) {
                    const container = cursorDetails.container;

                    let inTextWrapper = false;
                    const parentClassName = container.parent?.className;
                    const grandparentClassName = container.parent?.parent?.parent?.className;
                    if ((parentClassName == 'TextWrapper') || 
                        ((parentClassName == 'SquareEmptyContainerWrapper') && (grandparentClassName == 'TextWrapper'))) {
                        inTextWrapper = true;
                    }

                    const character = e.key;
                    if (inTextWrapper) {
                        widget._runTool('insertLetter', {character: character});
                    } else if (((character === 'e') || (character === 'E')) && blocked.includes(character)) {
                        widget._runTool('insertTenToThe', );
                    } else if (blocked.includes(character)) {
                        return;
                    } else if (letterKeys.includes(character)) {
                        widget._runTool('insertSymbol', {symbol: character, font: 'MathJax_MathItalic'});
                    } else if (numberKeys.includes(character)) {
                        widget._runTool('insertSymbol', {symbol: character, font: 'MathJax_Main'});
                    } else if (symbolKeys.includes(character)) {
                        widget._runTool('insertSymbol', {symbol: character, font: 'MathJax_Main'});
                    } else if (character === "'") {
                        widget._runTool('insertSymbol', {symbol: '′', font: 'MathJax_Main'});
                    } else if (operatorKeys.includes(character)) {
                        widget._runTool('insertOperator', {symbol: operatorKeysMap[character], font: 'MathJax_Main'});
                    } else if (character === '=') {
                        widget._runTool('insertOperator', {symbol: '=', font: 'MathJax_Main'});
                    } else if (closeBracketKeys.includes(character)) {
                        const bracketType = bracketCharactersMap[character];
                        if ((container.parent.className == 'BracketPairWrapper') && (container.parent.bracketType === bracketType)) {
                            if (cursorDetails.highlightStartIndex === container.wrappers.length) {
                                widget._cursor.addCursorAtIndex(container.parent.parent, container.parent.index + 1);
                            }
                        } else if (character === '|') {
                            widget._runTool('insertBracketPair', {type: 'absValBracket'});
                        }
                    } else if (openBracketKeys.includes(character)) {
                        widget._runTool('insertBracketPair', {type: bracketCharactersMap[character]});
                    } else if (character === '^') {
                        widget._runTool('insertSuperscript', );
                    } else if (character === '_') {
                        widget._runTool('insertSubscript', );
                    } else {
                        return;
                    }
                    widget._centerCursor();
                    widget._pushSavedState();
                    return false;
                }
            });
            
            // Actions to show tooltip when mouse is over a toolbar button

            $parent.on('mouseenter', '.edit_button', function(e) {
                $parent.find('.edit_title').remove();
                
                if (!$(this).hasClass('disabled') && ($(this).attr('data-show-title') == 'true')) {
                    var $title = $(
                        "<div class='edit_title'> \
                            <div class='edit_titleArrow'></div> \
                            <div class='edit_titleText'>" + $(this).attr("data-text") + "</div> \
                        </div>"
                    );
                    
                    var offset = 0;
                    if ($(this)[0].hasAttribute('data-title-offset')) {
                        offset = parseFloat($(this).data("title-offset"));
                        var $arrow = $title.children('.edit_titleArrow');
                        $arrow.css('position', 'relative').css('left', -offset);
                    }
                    
                    $parent.append($title);
                            
                    $title.css('left', $(this).position().left + $(this).outerWidth() / 2 - $title.outerWidth() / 2 + offset);
                    $title.css('top', $(this).position().top + $(this).outerHeight() + 2);				
                }
            });

            $parent.on('mouseleave', '.edit_button', function(e) {
                $parent.find('.edit_title').remove();
            });
            
            // Hide all menus when the main view is clicked
            
            this.element.click(function() {
                widget._hideMenu($parent.find('.edit_button'));
            });

            // Focus and blur events
            
            $parent.on('blur', function(e) {
                widget._cursor.unrender();
                widget._hideMenu($('.edit_button'));
            });
            
            $parent.on('focus', function(e) {
                if (widget._cursor.getDetails() === null) {
                    widget._cursor.initialize();
                } else widget._cursor.render();

                widget._hideMenu($('.edit_button'));
            });
            
            // Actions for toolbar button click
                
            $parent.on('click', '.edit_button, .edit_menu_button', function(e) {
                if ($(this).hasClass('disabled')) return;

                e.preventDefault();
                e.stopPropagation();

                var $button = $(this);
                    
                // Process the command that was clicked

                if (this.hasAttribute('data-tool-id')) {

                    $parent.find('.edit_button').each(function() {
                        widget._hideMenu($(this));
                    });
    
                    const toolID = this.getAttribute('data-tool-id');
                    const toolOptions = JSON.parse(this.getAttribute('data-options') || "{}");
                    widget._runTool(toolID, toolOptions);

                }  else if (this.hasAttribute('data-menu-id')) {

                    const menuID = this.getAttribute('data-menu-id');

                    if ($button.find('.edit_button_menu').is(":visible")) {

                        widget._hideMenu($button);

                    } else {

                        $parent.find('.edit_button').each(function() {
                            widget._hideMenu($(this));
                        });
        
                        const $menuTarget = $button.find('.edit_button_menu');
                        if (menuID == 'insertVariable') {
                            // Skip constants if there is a visible menu for them
                            const includeConstants = ($parent.find('.edit_button_constant:visible').length == 0);
                            widget._menus[menuID].setMenu($menuTarget, includeConstants);
                        } else if (menuID == 'insertConstant') {
                            widget._menus[menuID].setMenu($menuTarget);
                        }

                        widget._showMenu($button);
                    }

                }

                e.stopPropagation();
            });
        },

        _cursorMoved: function() {
            var $parent = this.element.closest('.edit_parent');
            const cursorDetails = this._cursor.getDetails();

            if (cursorDetails === null) {
                $parent.find('.edit_button, .edit_menu_button').addClass('disabled');
            } else {
                const includeConstants = ($parent.find('.edit_button_constant:visible').length == 0);
                this._menus['insertVariable'].setMenu($parent.find('.edit_button_variable2 .edit_button_menu'), includeConstants);
                this._menus['insertConstant'].setMenu($parent.find('.edit_button_constant .edit_button_menu'));

                $parent.find('.edit_button, .edit_menu_button').each((index, element) => {
                    if (element.hasAttribute('data-tool-id')) {
                        const toolID = $(element).attr('data-tool-id');
                        if (!this._isAllowed(toolID)) $(element).addClass('disabled');
                        else $(element).removeClass('disabled');    
                    }
                });
                $parent.find('.edit_button').each(function() {
                    const $buttonMenu = $(this).children('.edit_button_menu');
                    if ($buttonMenu.length == 1) {
                        var hasAllowed = false;
                        $buttonMenu.find('.edit_menu_button').each(function() {
                            if (!$(this).hasClass('disabled')) hasAllowed = true;
                        });
                        if (hasAllowed) $(this).removeClass('disabled');
                        else $(this).addClass('disabled');
                    }
                });
            }
        },
        
        _hideMenu: function($button) {
            $button.find('.edit_button_menu').hide();
            $('.edit_button').attr('data-show-title', 'true');
            $button.removeClass('selected');
        },
        
        _showMenu: function($button) {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.edit_title').remove();

            $('.edit_button').attr('data-show-title', 'false');
            $button.addClass('selected');
            $button.find('.edit_button_menu').show();
        },

        _addMenu: function($target, menuID) {
            const menu = this._menus[menuID];
            var menuButtonHTML = "<div style='position:relative' class='edit_button";
            if ('iconClass' in menu) menuButtonHTML += " " + menu.iconClass;
            menuButtonHTML += "'";
            if ('label' in menu) menuButtonHTML += " data-text='" + menu.label + "'";
            if ('titleOffset' in menu) menuButtonHTML += " data-title-offset='" + menu.titleOffset + "'";
            menuButtonHTML += " data-menu-id='" + menuID + "'";
            menuButtonHTML += " data-show-title='true'>";
            menuButtonHTML += "<div class='edit_button_icon'></div>";
            menuButtonHTML += "<div class='edit_button_menu' style='display:none;'></div>";
            menuButtonHTML += "</div>";

            const $newElement = $(menuButtonHTML);
            $target.append($newElement);

            return $newElement;
        },

        _addTool: function($target, toolID) {
            const tool = this._tools[toolID];
            let buttonHTML = "<div style='position:relative' class='edit_button";
            if (tool.iconClass) buttonHTML += " " + tool.iconClass;
            buttonHTML += "'";
            if (tool.shortcut) buttonHTML += " data-shortcut='" + tool.shortcut + "'";
            if (tool.label) buttonHTML += " data-text='" + tool.label + "'";
            if (tool.titleOffset) buttonHTML += " data-title-offset='" + tool.titleOffset + "'";
            buttonHTML += " data-tool-id='" + toolID + "'";
            buttonHTML += " data-show-title='true'>";
            buttonHTML += "<div class='edit_button_icon'></div>";
            buttonHTML += "</div>";
        
            const $newElement = $(buttonHTML);
            $target.append($newElement);

            return $newElement;
        },

        _isAllowed: function(toolID) {
            const cursorDetails = this._cursor.getDetails();
            if (cursorDetails) {
                let container = cursorDetails.container;
                if (container.className === 'SquareEmptyContainer') {
                    container = container.parent.parent;
                }

                const firstIndex = Math.min(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
                const lastIndex = Math.max(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);

                const previousClassName = (firstIndex == 0) ? null : container.wrappers[firstIndex - 1].className;

                if (container.className == 'TextContainer') {
                    if (toolID !== 'insertLetter') return false;
                }

                if (toolID == 'insertText') {
                    if (firstIndex !== lastIndex) return false;
                }

                if (toolID == 'insertTenToThe') {
                    if ((previousClassName != 'SymbolWrapper') && (previousClassName != 'VariableWrapper')) return false;
                }
                
                if (toolID == 'insertSuperscript') {
                    if (this.options.restrictEquation) {
                        if (previousClassName === null) return false;
                        else if (previousClassName == 'TopLevelEmptyContainerWrapper') return false;
                        else if (previousClassName == 'SquareEmptyContainerFillerWrapper') return false;
                        else if (previousClassName == 'OperatorWrapper') return false;
                    }
                }
                
            } else return false;

            return true;
        },

        _runTool: function(toolID, toolOptions) {
            if (this._isAllowed(toolID)) {
                this._tools[toolID].apply(toolOptions);
                this._pushSavedState();
            }
        },

        _replaceCursorMarker: function(container) {
            for (let i = 0; i < container.wrappers.length; i++) {
                const wrapper = container.wrappers[i];
                if (wrapper.className === "CursorMarkerWrapper") {
                    container.removeWrappers([i]);
                    this._cursor.addCursorAtIndex(container, i);
                    if (wrapper.highlightLength > 0) {
                        this._cursor.setHighlightEndIndex(i + wrapper.highlightLength);
                    }
                    this._fixEmptyContainer();
                    return true;
                }
                if (wrapper.childContainers && wrapper.childContainers.length > 0) {
                    for (let j = 0; j < wrapper.childContainers.length; j++) {
                        const result = this._replaceCursorMarker(wrapper.childContainers[j]);
                        if (result) return result;
                    }
                }
            }
            return false;
        },
    
        // Save DOM state including selection and HTML
        _pushSavedState: function() {

            // Insert a cursor marker for the selection

            const cursorDetails = this._cursor.getDetails();
            if (cursorDetails !== null) {
                let container = cursorDetails.container;
                const firstIndex = Math.min(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
                const lastIndex = Math.max(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
                const highlightLength = lastIndex - firstIndex;

                const wrapper = new CursorMarkerWrapper(container.equation, highlightLength);

                if (container.className == 'SquareEmptyContainer') {
                    container = container.parent.parent;
                    container.removeWrappers([0]);
                }
                container.addWrappers([{
                    index: firstIndex, 
                    wrapper: wrapper
                }]);
            }        

            // Get entry

            var jsonData = this.element.find('.eqEdEquation').data('eqObject').buildJsonObj();

            const entry = {
                json: JSON.stringify(jsonData)
            };

            // Restore cursor

            if (cursorDetails !== null) {
                var equation = this.element.find('.eqEdEquation').data('eqObject');
                this._replaceCursorMarker(equation.topLevelContainer);
            }
            
            // Save state
        
            if ((this._savedStates.length == 0) || (this._savedStates[this._savedStateIndex].json !== entry.json)) {
                if (this._savedStateIndex + 1 < this._savedStates.length) {
                    this._savedStates = this._savedStates.slice(0, this._savedStateIndex + 1);
                }
                this._savedStates.push(entry);
                this._savedStateIndex++;
            }
        },
        
        // Restore DOM state to the last saved
        _restoreSavedState: function() {
            this.element.focus();
        
            var entry = this._savedStates[this._savedStateIndex];

            // Restore equation JSON
            var newData = JSON.parse(entry.json);
            var equation = Equation.constructFromJsonObj(newData);
            this.element.empty().append(equation.domObj.value);
            equation.updateAll();

            // Find cursor marker and set selection
            this._replaceCursorMarker(equation.topLevelContainer);
        },

        _selectAll: function() {
            var equation = this.element.find('.eqEdEquation').data('eqObject');
            var container = equation.topLevelContainer;
            this._cursor.addCursorAtIndex(container, 0);
            this._cursor.setHighlightEndIndex(container.wrappers.length);
        },
        
        _fixEmptyContainer: function() {
            const cursorDetails = this._cursor.getDetails();
            if (!cursorDetails) return;

            const container = cursorDetails.container;
            if (container.wrappers.length === 0) {
                if (container.parent.className === 'Equation') {
                    container.addWrappers([{
                        index: 0, 
                        wrapper: new TopLevelEmptyContainerWrapper(container.equation)
                    }]);
                    container.updateAll();
                    this._cursor.addCursorAtIndex(container, 0);
                } else {
                    container.addWrappers([{
                        index: 0, 
                        wrapper: new SquareEmptyContainerWrapper(container.equation)
                    }]);
                    container.updateAll();
                    this._cursor.addCursorAtIndex(container.wrappers[0].childContainers[0], 0);
                }
            }
        },

        _deleteSelection: function() {
            const cursorDetails = this._cursor.getDetails();
            if (!cursorDetails) return;

            const container = cursorDetails.container;
            const firstIndex = Math.min(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
            const lastIndex = Math.max(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
            if (firstIndex == lastIndex) return;

            const deleteWrappers = this._range(firstIndex, lastIndex);
            container.removeWrappers(deleteWrappers);

            container.updateAll();
            this._cursor.addCursorAtIndex(container, firstIndex);
            this._fixEmptyContainer();

            this._pushSavedState();
        },
        
        _copySelection: function() {
            const cursorDetails = this._cursor.getDetails();
            if (!cursorDetails) return;

            const container = cursorDetails.container;
            const firstIndex = Math.min(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
            const lastIndex = Math.max(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);

            if (firstIndex === lastIndex) return;
            
            const copiedWrappers = container.wrappers.slice(firstIndex, lastIndex);
            const jsonArray = copiedWrappers.map(wrapper => wrapper.buildJsonObj());
            internalClipboard = {
                json: JSON.stringify(jsonArray)
            };
        },

        _pasteSelection: function() {
            if (!internalClipboard) return;

            const cursorDetails = this._cursor.getDetails();
            if (!cursorDetails) return;

            const container = cursorDetails.container;
            const insertIndex = cursorDetails.highlightEndIndex;

            let jsonArray = JSON.parse(internalClipboard.json);
            for (let i = 0; i < jsonArray.length; i++) {
                const newWrapper = Wrapper.wrapperFromJsonObj(jsonArray[i], container.equation);
                container.addWrappers([{ index: insertIndex + i, wrapper: newWrapper }]);
            }

            container.updateAll();
            this._cursor.addCursorAtIndex(container, insertIndex + jsonArray.length);
            this._pushSavedState();
            this._centerCursor();
        },

        _centerCursor: function() {
            // Scroll container to bring cursor closer to the center
            if ($('.cursor').length > 0) {
                const cursorPos = $('.cursor').offset().left;
                const $equation = $('.cursor').closest('.eqEdEquation');
                const equationMargin = parseFloat($equation.css('margin'));
                const $container = $equation.parent();
                const containerLeft = $container.offset().left + equationMargin;
                const containerRight = $container.offset().left + $container.outerWidth() - equationMargin;
                if (cursorPos < containerLeft) {
                    $container.scrollLeft($container.scrollLeft() + cursorPos - containerLeft);
                } else if (cursorPos > containerRight) {
                    $container.scrollLeft($container.scrollLeft() + cursorPos - containerRight);
                }
            }
        },
        
        _range: function(startIndex, endIndex) {
            return Array.from({ length: endIndex - startIndex }, (_, i) => startIndex + i);
        },

        getJSON: function() {
            var data = this.element.find('.eqEdEquation').data('eqObject').buildJsonObj();
            return JSON.stringify(data);
        },

        setJSON: function(newJSON) {
            var newData = JSON.parse(newJSON);
            var equation = Equation.constructFromJsonObj(newData);
            this.element.empty().append(equation.domObj.value);
            equation.updateAll();
        
            this._commandMode = false;
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.command_status').html("");

            this._savedStates = [];
            this._savedStateIndex = -1;
            this._pushSavedState();
        },

        attachEditor: function() {
            if (this._commandMode) {
                this._commandMode = false;
                var $parent = this.element.closest('.edit_parent');
                $parent.find('.command_status').html("");
            }
            
            var equation = new Equation();
            this.element.empty().append(equation.domObj.value);
            equation.updateAll();

            // Delete the cursor, since it may reference an old equation. Cursor is re-initialized
            // on focus.
            
            this._cursor.delete();
            this._pushSavedState();
        },

        makeInactive: function() {
            var $parent = this.element.closest('.edit_parent');
            $parent.addClass('inactive');
        },

        makeActive: function() {
            var $parent = this.element.closest('.edit_parent');
            $parent.removeClass('inactive');
        },

        // Undo and redo functions
        
        _stepUndo: function() {
            if (this._savedStateIndex > 0) {
                this._savedStateIndex--;
                this._restoreSavedState();
            }
        },

        _stepRedo: function() {
            if (this._savedStateIndex + 1 < this._savedStates.length) {
                this._savedStateIndex++;
                this._restoreSavedState();
            }
        },

        // Hide and show buttons and sections
        
        hideAllButtons: function() {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.edit_button').hide();
            $parent.find('.edit_bar_section').hide();
        },
        
        showAllButtons: function() {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.edit_button').show();
            $parent.find('.edit_bar_section').show();
        },
        
        hideElement: function(elementClass) {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.' + elementClass).each(function() {
                $(this).hide();
                if ($(this).hasClass('edit_bar_section')) $(this).find('.edit_button').hide();
            });
        },

        showElement: function(elementClass) {
            var $parent = this.element.closest('.edit_parent');
            $parent.find('.' + elementClass).each(function() {
                $(this).show();
                if ($(this).hasClass('edit_bar_section')) $(this).find('.edit_button').show();
                else if ($(this).hasClass('edit_button')) $(this).closest('.edit_bar_section').show();
            });
        },
        
        _handleKeydown: function(widget, e) {
            var $parent = widget.element.closest('.edit_parent');
            var $status = $parent.find('.command_status');
            if (widget._commandMode) {
                if ((widget._commandText.length == 0) && (e.key === '\\')) {

                    widget._commandMode = false;
                    $status.html("");

                } else if (!e.altKey && !e.ctrlKey && !e.metaKey) {

                    if (e.key === 'Escape') {
                        widget._commandMode = false;
                        $status.html("");
                    } else if ((e.key === 'Backspace' || e.key === 'Delete')) {
                        if (widget._commandText.length > 0) {
                            widget._commandText = widget._commandText.slice(0, -1);
                            $status.html("<span style='float:left;'>Command: " + widget._commandText + "</span><span style='float:right;'>Type esc to cancel</span>");
                        }
                    } else if (e.key === 'Enter') {
                        widget._commandMode = false;
                        $status.html("");
                    } else if (/^[a-zA-Z]$/.test(e.key)) {
                        widget._commandText += e.key;
                        
                        // Check shortcuts
                        
                        if (widget._commandText in widget._buttonShortcuts) {
                            widget._commandMode = false;
                            const $button = widget._buttonShortcuts[widget._commandText];
                            if ($button.closest('.edit_button').is(':visible')) {
                                const toolID = $button.attr('data-tool-id');
                                const toolOptions = JSON.parse($button.attr('data-options') || "{}");
                                widget._runTool(toolID, toolOptions);
                            }
                        }
                        
                        // Show command
                        
                        $status.html("<span style='float:left;'>Command: " + widget._commandText + "</span><span style='float:right;'>Type esc to cancel</span>");
                        if (!widget._commandMode) {
                            $status.animate({
                                color: '#fff'
                            }, 500, 'swing', function() {
                                $status.html("");
                                $status.css('color', '#222');
                            });
                        }
                    }
        
                    e.stopImmediatePropagation();
                    return false;
                }

            } else if (e.key === '\\') {

                widget._commandMode = true;
                widget._commandText = "";
                $status.html("<span style='float:left;'>Command:</span><span style='float:right;'>Type esc to cancel</span>");

                e.stopImmediatePropagation();
                return false;

            } else if (e.ctrlKey || e.metaKey) {

                if (e.key === 'z' && e.shiftKey) {
                    widget._stepRedo();
                    return false;
                } else if (e.key === 'z' && !e.shiftKey) {
                    widget._stepUndo();
                    return false;
                } else if (e.key === 'y' && !e.shiftKey) {
                    widget._stepRedo();
                    return false;
                } else if (e.key === 'c' && !e.shiftKey) {
                    widget._copySelection();
                    return false;
                } else if (e.key === 'x' && !e.shiftKey) {
                    widget._copySelection();
                    widget._deleteSelection();
                    return false;
                } else if (e.key === 'v' && !e.shiftKey) {
                    widget._pasteSelection();
                    return false;
                } else if (e.key === 'a' && !e.shiftKey) {
                    widget._selectAll();
                    return false;                    
                }

            } else {

                const cursorDetails = widget._cursor.getDetails();
                if (cursorDetails !== null) {
                    let container = cursorDetails.container;
                
                    if (e.key === 'Backspace') {
                        // Backspace handling.
                        if (!container.parent.isEmptyContainerWrapper) {  
                            if (cursorDetails.highlightStartIndex === cursorDetails.highlightEndIndex) {
                
                                const cursorIndex = cursorDetails.highlightStartIndex;
                                if (cursorIndex !== 0) {
                                    if (container.wrappers[cursorIndex - 1].childContainers.length > 0) {
                                        // If the wrapper to be deleted has children, highlight it first
                                        widget._pushSavedState();
                                        widget._cursor.setHighlightEndIndex(cursorIndex - 1);
                                    } else {
                                        container.removeWrappers([cursorIndex - 1]);
                                        container.updateAll();
                                        widget._cursor.addCursorAtIndex(container, cursorIndex - 1);
                                        widget._fixEmptyContainer();
                                        widget._pushSavedState();
                                    }
                                }
                    
                            } else {
                
                                widget._deleteSelection();
                
                            }                
                        }
                        
                    } else if (e.key === 'Delete') {
                        // Delete key handling.
                        if (!container.parent.isEmptyContainerWrapper) {
                            if (cursorDetails.highlightStartIndex === cursorDetails.highlightEndIndex) {

                                const cursorIndex = cursorDetails.highlightStartIndex;
                                if (cursorIndex !== container.wrappers.length) {
                                    if (container.wrappers[cursorIndex].childContainers.length > 0) {
                                        // If the wrapper to be deleted has children, highlight it first
                                        widget._pushSavedState();
                                        widget._cursor.setHighlightEndIndex(cursorIndex + 1);
                                    } else {
                                        container.removeWrappers([cursorIndex]);
                                        container.updateAll();
                                        widget._cursor.addCursorAtIndex(container, cursorIndex);
                                        widget._fixEmptyContainer();
                                        widget._pushSavedState();
                                    }
                                }
        
                            } else {

                                widget._deleteSelection();

                            }
                        }

                    } else if (e.key === 'ArrowLeft') {
                        if (e.shiftKey) {

                            // Drag a selection to the left
                            let newIndex = cursorDetails.highlightEndIndex - 1;
                            if (newIndex < 0) { newIndex = 0; }
                            widget._cursor.setHighlightEndIndex(newIndex);

                        } else {

                            // Move the cursor left
                            if (container.wrappers[0].className === 'TopLevelEmptyContainerWrapper') {
                                return false;
                            }

                            const firstIndex = Math.min(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
                            if (firstIndex !== 0 && container.className !== 'SquareEmptyContainer') {
                                if (container.wrappers[firstIndex - 1].childContainers.length > 0) {
                                    const childContainers = container.wrappers[firstIndex - 1].childContainers;
                                    const lastChild = childContainers[childContainers.length - 1];
                                    if (lastChild.wrappers[0].isEmptyContainerWrapper) {
                                        widget._cursor.addCursorAtIndex(lastChild.wrappers[0].childContainers[0], 0);
                                    } else {
                                        widget._cursor.addCursorAtIndex(lastChild, lastChild.wrappers.length);
                                    }
                                } else {
                                    widget._cursor.addCursorAtIndex(container, firstIndex - 1);
                                }
                            } else {
                                if (container.className === 'SquareEmptyContainer') {
                                    container = container.parent.parent;
                                }
                                if (container.domObj.value.prev('.equationContainer').length > 0) {
                                    container = container.domObj.value.prev('.equationContainer').first().data('eqObject');
                                    if (container.wrappers[0].className === 'SquareEmptyContainerWrapper') {
                                        container = container.wrappers[0].childContainers[0];
                                    }
                                    widget._cursor.addCursorAtIndex(container, container.wrappers.length);
                                } else {
                                    if (container.parent.className !== 'Equation') {
                                        widget._cursor.addCursorAtIndex(container.parent.parent, container.parent.index);
                                    }
                                }
                            }
                        }
                    } else if (e.key === 'ArrowRight') {
                        if (e.shiftKey) {

                            // Drag a selection to the right
                            let newIndex = cursorDetails.highlightEndIndex + 1;
                            if (newIndex > container.wrappers.length) { newIndex = container.wrappers.length; }
                            widget._cursor.setHighlightEndIndex(newIndex);

                        } else {
                                
                            // Move the cursor right
                            if (container.wrappers[0].className === 'TopLevelEmptyContainerWrapper') {
                                return false;
                            }
                            const lastIndex = Math.max(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
                            if (lastIndex !== container.wrappers.length && container.className !== 'SquareEmptyContainer') {
                                if (container.wrappers[lastIndex].childContainers.length > 0) {
                                    const childContainer = container.wrappers[lastIndex].childContainers[0];
                                    if (childContainer.wrappers[0].isEmptyContainerWrapper) {
                                        widget._cursor.addCursorAtIndex(childContainer.wrappers[0].childContainers[0], 0);
                                    } else {
                                        widget._cursor.addCursorAtIndex(childContainer, 0);
                                    }
                                } else {
                                    widget._cursor.addCursorAtIndex(container, lastIndex + 1);
                                }
                            } else {
                                if (container.className === 'SquareEmptyContainer') {
                                    container = container.parent.parent;
                                }
                                if (container.domObj.value.next('.equationContainer').length > 0) {
                                    container = container.domObj.value.next('.equationContainer').first().data('eqObject');
                                    if (container.wrappers[0].className === 'SquareEmptyContainerWrapper') {
                                        container = container.wrappers[0].childContainers[0];
                                    }
                                    widget._cursor.addCursorAtIndex(container, 0);
                                } else {
                                    if (container.parent.className !== 'Equation') {
                                        widget._cursor.addCursorAtIndex(container.parent.parent, container.parent.index + 1);
                                    }
                                }
                            }
                        }
                    } else {
                        return;
                    }
                    return false;
                }

            }
        }
    });
   
    eqnEditInitialized = true;
}

export { initialize, setCommonContext };
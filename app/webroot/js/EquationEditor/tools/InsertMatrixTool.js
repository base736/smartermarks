/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { Tool } from './Tool.js?7.0';

import { MatrixWrapper } from '../components/wrappers/index.js?7.0';

export class InsertMatrixTool extends Tool {
    constructor(context) {
        super(context);
        this.id = 'insertMatrix';
    }

    apply(options) {
        const equation = this.context.editor.$element.find('.eqEdEquation').data('eqObject');

        var alignment = 'center';
        const cursorDetails = this.context.editor.cursor.getDetails();
        if (cursorDetails != null) {
            const parentClassName = cursorDetails.container.parent?.className;
            const grandparentClassName = cursorDetails.container.parent?.parent?.parent?.className;
            if ((parentClassName == 'BracketLeftWrapper') || 
                ((parentClassName == 'SquareEmptyContainerWrapper') && (grandparentClassName == 'BracketLeftWrapper'))) {
                alignment = 'left';
            } 
        }
    
        const wrapper = new MatrixWrapper(equation, options.rows, options.cols, alignment);
        this.insertWrapper(wrapper);
        return Promise.resolve('done');
    }
}
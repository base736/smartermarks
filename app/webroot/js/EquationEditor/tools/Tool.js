/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class Tool {
    constructor(context) {
        this.context = context;
        this.id = null;
    }

    static _range(startIndex, endIndex) {
        return Array.from({ length: endIndex - startIndex }, (_, i) => startIndex + i);
    }

    insertWrapper(wrapper) {
        const cursor = this.context.editor.cursor;
        const cursorDetails = cursor.getDetails();
        if (cursorDetails === null) return;

        let container = cursorDetails.container;
        if (cursorDetails.highlightStartIndex == cursorDetails.highlightEndIndex) {
    
            // No selection: insert at the cursor's index.
            if (container.className == 'SquareEmptyContainer') {
                container = container.parent.parent;
                container.removeWrappers([0]);
            }
            container.addWrappers([{
                index: cursorDetails.highlightStartIndex, 
                wrapper: wrapper
            }]);
    
            wrapper.updateAll();
    
            if (wrapper.childContainers.length > 0) {
                // If the new wrapper has a child container, position the cursor there.
                if (wrapper.childContainers[0].wrappers[0].isEmptyContainerWrapper) {
                    cursor.addCursorAtIndex(wrapper.childContainers[0].wrappers[0].childContainers[0], 0);
                } else {
                    cursor.addCursorAtIndex(wrapper.childContainers[0], wrapper.childContainers[0].wrappers.length);
                }
            } else {
                // Otherwise, move the cursor one spot ahead.
                cursor.addCursorAtIndex(container, cursorDetails.highlightStartIndex + 1);
            }
    
        } else {
            
            var deleteIndices;
            if (cursorDetails.highlightStartIndex < cursorDetails.highlightEndIndex) {
                deleteIndices = Tool._range(cursorDetails.highlightStartIndex, cursorDetails.highlightEndIndex);
            } else {
                deleteIndices = Tool._range(cursorDetails.highlightEndIndex, cursorDetails.highlightStartIndex);
            }
    
            var copiedWrappers = [];
            for (let i = 0; i < deleteIndices.length; i++) {
                var deleteWrapper = container.wrappers[deleteIndices[i]];
                var copiedWrapper = deleteWrapper.clone();
                copiedWrappers.push({
                    index: i, 
                    wrapper: copiedWrapper}
                );
            }
    
            const startIndex = deleteIndices[0];
    
            container.removeWrappers(deleteIndices);
            container.addWrappers([{
                index: startIndex, 
                wrapper: wrapper
            }]);
    
            if ((copiedWrappers.length > 0) && (wrapper.childContainers.length > 0)) {
                wrapper.childContainers[0].addWrappers(copiedWrappers);
                container.updateAll();
                cursor.addCursorAtIndex(wrapper.childContainers[0], copiedWrappers.length);
            } else {
                container.updateAll();
                cursor.addCursorAtIndex(container, startIndex + 1);
            }
        }
    }
}
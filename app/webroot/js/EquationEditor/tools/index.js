/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { InsertTextTool } from './InsertTextTool.js?7.0';
import { InsertVariableTool } from './InsertVariableTool.js?7.0';
import { InsertFunctionTool } from './InsertFunctionTool.js?7.0';

import { InsertSymbolTool } from './InsertSymbolTool.js?7.0';
import { InsertOperatorTool } from './InsertOperatorTool.js?7.0';
import { InsertLetterTool } from './InsertLetterTool.js?7.0';

import { InsertTenToTheTool } from './InsertTenToTheTool.js?7.0';

import { InsertAccentTool } from './InsertAccentTool.js?7.0';
import { InsertStackedFractionTool } from './InsertStackedFractionTool.js?7.0';

import { InsertSuperscriptTool } from './InsertSuperscriptTool.js?7.0';
import { InsertSubscriptTool } from './InsertSubscriptTool.js?7.0';
import { InsertSuperscriptAndSubscriptTool } from './InsertSuperscriptAndSubscriptTool.js?7.0';

import { InsertSquareRootTool } from './InsertSquareRootTool.js?7.0';
import { InsertNthRootTool } from './InsertNthRootTool.js?7.0';

import { InsertBracketPairTool } from './InsertBracketPairTool.js?7.0';
import { InsertBracketLeftTool } from './InsertBracketLeftTool.js?7.0';

import { InsertMatrixTool } from './InsertMatrixTool.js?7.0';

import { InsertBigOperatorTool } from './InsertBigOperatorTool.js?7.0';
import { InsertLimitTool } from './InsertLimitTool.js?7.0';
import { InsertIntegralTool } from './InsertIntegralTool.js?7.0';
import { InsertLabeledArrowTool } from './InsertLabeledArrowTool.js?7.0';

export function createTools(context) {
    return {
        insertText: new InsertTextTool(context),
        insertVariable: new InsertVariableTool(context),
        insertFunction: new InsertFunctionTool(context),

        insertSymbol: new InsertSymbolTool(context),
        insertOperator: new InsertOperatorTool(context),
        insertLetter: new InsertLetterTool(context),

        insertTenToThe: new InsertTenToTheTool(context),

        insertAccent: new InsertAccentTool(context),
        insertStackedFraction: new InsertStackedFractionTool(context),
        
        insertSuperscript: new InsertSuperscriptTool(context),
        insertSubscript: new InsertSubscriptTool(context),
        insertSuperscriptAndSubscript: new InsertSuperscriptAndSubscriptTool(context),

        insertSquareRoot: new InsertSquareRootTool(context),
        insertNthRoot: new InsertNthRootTool(context),

        insertBracketPair: new InsertBracketPairTool(context),
        insertBracketLeft: new InsertBracketLeftTool(context),

        insertMatrix: new InsertMatrixTool(context),

        insertBigOperator: new InsertBigOperatorTool(context),
        insertLimit: new InsertLimitTool(context),
        insertIntegral: new InsertIntegralTool(context),
        insertLabeledArrow: new InsertLabeledArrowTool(context)
    };
}
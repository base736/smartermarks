/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class InsertBracketsMenu {
    constructor(context) {
        this.id = 'insertBrackets';
        this.label = 'Brackets';
        this.iconClass = 'edit_button_brackets';
    }

    setMenu($target) {
        const bracketPairButtons = [
            { type: 'openLeftBracket', iconClass: 'openLeftBracketPairButton' },
            { type: 'openRightBracket', iconClass: 'openRightBracketPairButton' },
            { type: 'parenthesisBracket', iconClass: 'parenthesisBracketPairButton' },
            { type: 'squareBracket', iconClass: 'squareBracketPairButton' },
            { type: 'curlyBracket', iconClass: 'curlyBracketPairButton' },
            { type: 'absValBracket', iconClass: 'absValBracketPairButton' }
        ];

        let menuHTML = `<div class="edit_symbol_list" style="width:96px; height:auto; margin:0px 5px; background:white;">`;

        bracketPairButtons.forEach(details => {
            const options = { type: details.type };
            const optionsString = htmlSingleQuotes(JSON.stringify(options));

            menuHTML += `
                <div class='symbol_cell edit_menu_button ${details.iconClass}'
                    data-tool-id='insertBracketPair'
                    data-options='${optionsString}'>
                </div>
            `;
        });

        menuHTML += `
            <div class='symbol_cell edit_menu_button curlyBracketLeftButton'
                data-tool-id='insertBracketLeft' data-shortcut='lbrace'
                data-options='{"type":"curlyBracket"}'>
            </div>
        `;

        menuHTML += '</div>';

        $target.html(menuHTML);
    }
}
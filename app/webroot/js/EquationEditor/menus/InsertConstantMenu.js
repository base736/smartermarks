/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class InsertConstantMenu {
    constructor(context) {
        this.context = context;
        this.id = 'insertConstant';
        this.label = 'Constant';
        this.iconClass = 'edit_button_constant';
    }

    setMenu($target) {
        const variables = this.context.common.variables;

        var variableEntries = [];
        if ('constants' in variables) {
            const constants = variables['constants'];
            for (var i = 0; i < constants.length; ++i) {
                if ('displayName' in constants[i]) {
                    variableEntries.push({
                        variableName: constants[i].variableName,
                        displayName: constants[i].displayName
                    });	
                }
            }
        }
        variableEntries.sort(function(a, b) {
            return (a.displayName > b.displayName ? 1 : -1);
        });

        variableEntries = [
            { variableName: 'π', displayName: '&#x03c0;' },
            { variableName: 'e', displayName: 'e' },
            ...variableEntries
        ];

        var menuHTML = `<ul style="width:auto;">`;

        for (var i = 0; i < variableEntries.length; ++i) {
            const thisOptions = {
                variableName: variableEntries[i].variableName,
                displayName: variableEntries[i].displayName
            };
            const thisOptionsString = htmlSingleQuotes(JSON.stringify(thisOptions));

            menuHTML += `<li class="edit_menu_button variable_select" data-tool-id="insertVariable" 
                data-options='${thisOptionsString}'>${thisOptions.displayName}</li>`;
        }
        menuHTML += '</ul>';

        $target.html(menuHTML);
    }
};
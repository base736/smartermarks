/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class InsertOtherMenu {
    constructor(context) {
        this.id = 'insertOther';
        this.label = 'Others';
        this.iconClass = 'edit_button_other';
    }

    setMenu($target) {
        let menuHTML = `
            <div class="edit_symbol_list" style="width:72px; height:auto; margin:0px 5px; overflow-y:hidden; background:white;">
                <div class='symbol_cell edit_menu_button sumOperatorButton' 
                    data-tool-id='insertBigOperator'
                    data-shortcut='sum'
                    data-options='{"hasUpperLimit":true,"hasLowerLimit":true,"bigOperatorType":"sum"}'>
                </div>
                <div class='symbol_cell edit_menu_button limitButton' 
                    data-tool-id='insertLimit'
                    data-shortcut='lim'>
                </div>
                <div class='symbol_cell edit_menu_button integralButton' 
                    data-tool-id='insertIntegral'
                    data-shortcut='int'
                    data-options='{"hasUpperLimit":true,"hasLowerLimit":true,"integralType":"single"}'>
                </div>
                <div class='symbol_cell edit_menu_button labeledArrowButton' 
                    data-tool-id='insertLabeledArrow'>
                </div>
            </div>
        `;

        $target.html(menuHTML);
    }
}
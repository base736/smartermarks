/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import Equation from '../equation.js?7.0';

export class InsertAccentMenu {
    constructor(context) {
        this.id = 'insertAccent';
        this.label = 'Accent';
        this.iconClass = 'edit_button_accent';
    }

    setMenu($target) {
        const accentButtons = [
            { accent: '^', iconClass: 'hatAccentButton', shortcut: 'hat' },
            { accent: '¯', iconClass: 'barAccentButton', shortcut: 'bar' },
            { accent: '➡', iconClass: 'vectorAccentButton', shortcut: 'vec' },
            { accent: '↔', iconClass: 'lineAccentButton', shortcut: 'line' },
            { accent: '˙', iconClass: 'dotAccentButton', shortcut: 'dot' }
        ];
        
        let menuHTML = `<div class="edit_symbol_list" style="width:120px; height:24px; margin:0px 5px; overflow-y:hidden; background:white;"></div>`;
        $target.html(menuHTML);

        const $newMenu = $target.find('.edit_symbol_list');
        accentButtons.forEach(details => {
            const options = { accent: details.accent, label: details.label };
            const optionsString = htmlSingleQuotes(JSON.stringify(options));

            var $newCell = $(`
                <div class="symbol_cell ${details.iconClass} edit_menu_button" 
                     title="${details.shortcut}"
                     data-shortcut="${details.shortcut}"
                     data-tool-id="insertAccent" data-options='${optionsString}'>
                </div>
            `).appendTo($newMenu);

            const symbol = Equation.constructFromJsonObj(JSON.parse('{"type":"Equation","value":null,"operands":{"topLevelContainer":[{"type":"Accent","value":"' + details.accent + '","operands":{"accentedExpression":[{"type":"Symbol","value":"a","operands":null}]}}]}}'));
            $newCell.append(symbol.domObj.value);
            symbol.updateAll();        
        });

        $target.find('.edit_symbol_list').tooltip();
    }
}
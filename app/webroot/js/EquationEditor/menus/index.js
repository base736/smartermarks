/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { InsertVariableMenu } from './InsertVariableMenu.js?7.0';
import { InsertConstantMenu } from './InsertConstantMenu.js?7.0';
import { InsertFunctionMenu } from './InsertFunctionMenu.js?7.0';
import { InsertSymbolMenu } from './InsertSymbolMenu.js?7.0';
import { InsertAccentMenu } from './InsertAccentMenu.js?7.0';
import { InsertSupSubMenu } from './InsertSupSubMenu.js?7.0';
import { InsertBracketsMenu } from './InsertBracketsMenu.js?7.0';
import { InsertMatrixMenu } from './InsertMatrixMenu.js?7.0';
import { InsertOtherMenu } from './InsertOtherMenu.js?7.0';

export function createMenus(context) {
    return {
        insertVariable: new InsertVariableMenu(context),
        insertConstant: new InsertConstantMenu(context),
        insertFunction: new InsertFunctionMenu(context),
        insertSymbol: new InsertSymbolMenu(context),
        insertAccent: new InsertAccentMenu(context),
        insertSupSub: new InsertSupSubMenu(context),
        insertBrackets: new InsertBracketsMenu(context),
        insertMatrix: new InsertMatrixMenu(context),
        insertOther: new InsertOtherMenu(context)
    };
}
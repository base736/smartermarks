/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class InsertSupSubMenu {
    constructor(context) {
        this.id = 'insertSupSub';
        this.label = 'Super and subscript';
        this.iconClass = 'edit_button_sup_sub';
    }

    setMenu($target) {
        let menuHTML = `
            <div class="edit_symbol_list" style="width:72px; height:24px; margin:0px 5px; overflow-y:hidden; background:white;">
                <div class='symbol_cell edit_menu_button superscriptButton' data-tool-id='insertSuperscript'></div>
                <div class='symbol_cell edit_menu_button subscriptButton' data-tool-id='insertSubscript'></div>
                <div class='symbol_cell edit_menu_button superscriptAndSubscriptButton' data-tool-id='insertSuperscriptAndSubscript'></div>
            </div>
        `;

        $target.html(menuHTML);
    }
}
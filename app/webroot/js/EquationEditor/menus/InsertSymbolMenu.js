/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import FontMetrics from "/js/EquationEditor/fontMetrics.js?7.0";

export class InsertSymbolMenu {
    constructor(context) {
        this.id = 'insertSymbol';
        this.label = 'Symbol';
        this.iconClass = 'edit_button_symbol';
    }

    setMenu($target) {
        var menuHTML = `<div class="edit_symbol_list">`;
        
        var symbolCharacters = FontMetrics.symbolCharacters;
        for (var i = 0; i < symbolCharacters.length; ++i) {
            const character = '&#x' + symbolCharacters[i][0];

            let toolName;
            const type = symbolCharacters[i][2];
            if (type == 'symbol') toolName = 'insertSymbol';
            else if (type == 'operator') toolName = 'insertOperator';
            else continue;

            const thisOptions = { 
                symbol: character,
                font: symbolCharacters[i][1]
            };
            const thisOptionsString = htmlSingleQuotes(JSON.stringify(thisOptions));

            menuHTML += `
                <div class='symbol_cell edit_menu_button' 
                    title='\\${symbolCharacters[i][3]}'
                    data-shortcut='${symbolCharacters[i][3]}'
                    data-tool-id='${toolName}' data-options='${thisOptionsString}'
                    data-pasted-text='${character};'>${character};</div>
            `;
        }
        
        menuHTML += `</div>`;

        $target.html(menuHTML);
        $target.find('.edit_symbol_list').tooltip();
    }
};
/**************************************************************************************************/
/* SmarterMarks equation editor: WYSIWIG equation editor for SmarterMarks, adapted from Camden    */
/* Reslink's equation editor                                                                      */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class Property {
    constructor(ctx, propName, initialValue, methods) {
        // methods should include: get, set, compute, updateDom.
        Property.uniqueId++;
        this.uniqueId = Property.uniqueId;
        this.isAlreadyComputed = false;
        this.ctx = ctx;
        this.propName = propName;
        this.value = initialValue;
        const self = this;

        Object.defineProperty(ctx, propName, {
            get() {
                if (!self.isAlreadyComputed && Property.isComputing) {
                    self.compute();
                }
                return methods.get.call(ctx);
            },
            set(value) {
                methods.set.call(ctx, value);
            }
        });

        this.compute = function () {
            if (!self.isAlreadyComputed) {
                const oldValue = self.value;
                // Compute the new value using the provided compute method.
                self.value = methods.compute.call(ctx);
                // Run any post-compute hooks.
                if (Property.postComputeHooks[self.propName] !== undefined) {
                    self.value = Property.postComputeHooks[self.propName].call(ctx, self.value);
                }
                if (Property.postComputeHooks["all"] !== undefined) {
                    self.value = Property.postComputeHooks["all"].call(ctx, self.value, self.propName);
                }
                // Update the property on the context.
                ctx[propName] = self.value;
                self.isAlreadyComputed = true;
                Property.alreadyComputed.push(self);
                self.updateDom(oldValue);
            }
        };

        this.updateDom = function (oldValue) {
            const isNumeric = !isNaN(self.value);
            const isString = Object.prototype.toString.call(self.value) === "[object String]";
            if (isNumeric) {
                if (Math.abs(oldValue - self.value) >= 0.001) {
                    methods.updateDom.call(ctx);
                }
            } else if (isString) {
                if (oldValue !== self.value) {
                    methods.updateDom.call(ctx);
                }
            }
        };
    }

    static beginComputing() {
        Property.isComputing = true;
    }

    static endComputing() {
        for (let i = 0; i < Property.alreadyComputed.length; i++) {
            Property.alreadyComputed[i].isAlreadyComputed = false;
        }
        Property.alreadyComputed = [];
        Property.isComputing = false;
    }
}

Property.alreadyComputed = [];
Property.isComputing = false;
Property.uniqueId = 0;

Property.postComputeHooks = {
    width: function(value) {
        if (typeof value === "undefined" || value === null) {
            value = 0;
        }
        var fontHeight = this.getFontHeight();
        return value + (this.padLeft + this.padRight) * fontHeight;
    },

    height: function(value) {
        if (typeof value === "undefined" || value === null) {
            value = 0;
        }
        var fontHeight = this.getFontHeight();
        return value + (this.padTop + this.padBottom) * fontHeight;
    },

    left: function(value) {
        if (typeof value === "undefined" || value === null) {
            value = 0;
        }
        var fontHeight = this.getFontHeight();
        var parentFontHeight = this.parent.getFontHeight();
        var additionalLeft = 0;
        if (this.isWrapper) {
            additionalLeft = this.adjustLeft * fontHeight;
        } else {
            additionalLeft = this.parent.padLeft * parentFontHeight + this.adjustLeft * fontHeight;
        }
        return value + additionalLeft;
    },

    top: function(value) {
        if (typeof value === "undefined" || value === null) {
            value = 0;
        }
        var fontHeight = this.getFontHeight();
        return value + (this.parent.padTop + this.adjustTop) * fontHeight;
    },

    topAlign: function(value) {
        if (typeof value === "undefined" || value === null) {
            value = 0;
        }
        var fontHeight = this.getFontHeight();
        return value + this.padTop * fontHeight;
    },

    bottomAlign: function(value) {
        if (typeof value === "undefined" || value === null) {
            value = 0;
        }
        var fontHeight = this.getFontHeight();
        return value + this.padBottom * fontHeight;
    },

    all: function(value, propName) {
        var isNumeric = (value !== null) && !isNaN(value) &&
                        !(value === true || value === false) &&
                        Object.prototype.toString.call(value) !== '[object Array]';
        if (isNumeric &&
            propName !== "padLeft" && propName !== "padRight" &&
            propName !== "padTop" && propName !== "padBottom" &&
            propName !== "adjustTop" && propName !== "adjustLeft" &&
            propName !== "heightRatio" && propName !== "accentGap" &&
            propName !== "borderWidth") {
            value = Math.ceil(value);
        }
        return value;
    }
}

export default Property;
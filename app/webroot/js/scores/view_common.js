/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as ScoringCommon from '/js/common/scoring.js?7.0';

// Helper function to convert a string to title case

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function getQRParts(qrText) {
    let resp = {};

    // Remove any text in parentheses

    while (/\([^)]*\)/.test(qrText)) {
        qrText = qrText.replace(/\([^)]*\)/g, '');
    }

    // Get the name part of the remaining text

    let name = '';
    qrText.replace(/([a-zA-Z-' ,&;]+)/g, function(match, p1) {

        // Trim leading and trailing '-\' , characters

        let block = p1.trim().replace(/^[-' ,]+|[-' ,]+$/g, '');
        if (block.length > name.length) name = block;
        return match; // Return the original match since we're not replacing it
    });

    if (name.length > 0) {
        resp.name = toTitleCase(name);
    }

    // Get the ID part of the remaining text

    let id = '';
    qrText.replace(/(\d+)/g, function(match, p1) {
        let block = p1;
        if (block.length > id.length) id = block;
        return match;
    });

    if (id.length > 0) {
        resp.id = id;
    }

    return resp;
}

async function getReportData(cacheHash, getDataToken) {

    if (cacheHash !== '') {
        const cachedJSON = localStorage.getItem('reportData');
        if (cachedJSON) {
            const cachedData = JSON.parse(cachedJSON);
            if (cachedData && (cachedData.cacheHash == cacheHash)) {
                return cachedData;
            }
        }
    }

    const response = await fetch('/Scores/get_data/' + getDataToken);
	const responseData = await response.json();

    if (!responseData.success) {
        throw new Error('Scores/get_data: Failed');
    }

    var userDefaults = responseData.userDefaults;
    var scoreData = responseData.scoreData;

    // Get warnings and errors

    const details = JSON.parse(scoreData.Score.details_json);

    var warnings = ('warnings' in details) ? details.warnings : [];
    var errors = ('errors' in details) ? details.errors : [];

    // Parse scoring data

    const scoringData = JSON.parse(scoreData.Document.VersionData.scoring_json);

    // Get item data

    let numbers = [];
    scoringData.Sections.forEach(section => {
        section.Content.forEach(item => {
            numbers.push(item.number);
        });
    });

    // Sort numbers as strings

    numbers.sort();

    // Determine if sections are needed

    const partsRegex = /^([0-9]+)([a-z])?$/;
    let needsSections = false;
    let lastMatches = false;
    for (let i = 0; i < numbers.length; i++) {
        let thisMatches = numbers[i].match(partsRegex);
        if (thisMatches) {
            if (lastMatches) {
                let digitsMatch = (thisMatches[1] === lastMatches[1]);
                if (!lastMatches[2] || !thisMatches[2]) {
                    if (digitsMatch) needsSections = true;
                } else {
                    let alphaMatch = (thisMatches[2] === lastMatches[2]);
                    if (digitsMatch && alphaMatch) needsSections = true;
                }
            }
            lastMatches = thisMatches;
        }
    }

    // Process item data

    let itemData = [];
    let maxDecimals = 1;
    let sectionIndex = 1;
    scoringData.Sections.forEach(section => {
        section.Content.forEach(item => {
            item.section_id = section.id;
            if (needsSections) item.number = sectionIndex + '.' + item.number;
            delete item.Source;

            if (!('scoring' in item)) item.scoring = 'regular';

            if (item.type !== 'wr') {
                item.ScoredResponses.forEach(entry => {
                    let valueString = entry.value.toFixed(3);
                    while (valueString[valueString.length - 1] === '0') {
                        valueString = valueString.slice(0, -1);
                    }
                    if (valueString.indexOf('.') !== -1) {
                        let responseDecimals = valueString.length - valueString.indexOf('.') - 1;
                        if (responseDecimals > maxDecimals) maxDecimals = responseDecimals;
                    }
                });
            }

            item.maxScore = ScoringCommon.getMaxScore(item);
            itemData.push(item);
        });
        sectionIndex++;
    });

    // Sort item data

    itemData.sort((a, b) => {
        const partsRegex = /^([0-9]+\.)?([0-9]+)([a-z])?$/;

        let aMatches = a.number.match(partsRegex);
        let aSection = (!aMatches || !aMatches[1]) ? -1 : parseInt(aMatches[1]);
        let aNumber = (!aMatches || !aMatches[2]) ? -1 : parseInt(aMatches[2]);
        let aAlpha = (!aMatches || !aMatches[3]) ? '' : aMatches[3];

        let bMatches = b.number.match(partsRegex);
        let bSection = (!bMatches || !bMatches[1]) ? -1 : parseInt(bMatches[1]);
        let bNumber = (!bMatches || !bMatches[2]) ? -1 : parseInt(bMatches[2]);
        let bAlpha = (!bMatches || !bMatches[3]) ? '' : bMatches[3];

        if (aSection < bSection) return -1;
        else if (aSection > bSection) return 1;
        else if (aNumber < bNumber) return -1;
        else if (aNumber > bNumber) return 1;
        else if (aAlpha < bAlpha) return -1;
        else if (aAlpha > bAlpha) return 1;
        else return 0;
    });

    // Get student details

    const layoutData = JSON.parse(scoreData.Document.layout_json);

    let hasNames = 'nameField' in layoutData.Title;
    let hasIDs = 'idField' in layoutData.Title;

    let responseDetails = details.responses || {};

    // Process response details

    Object.keys(responseDetails).forEach(key => {
        let pEntry = responseDetails[key];
        let parts = {};

        if ('qr' in pEntry && !/^\d{15}$/.test(pEntry.qr)) {
            parts = getQRParts(pEntry.qr);
        } else if ('label' in pEntry) {
            parts = getQRParts(pEntry.label);
        }

        if ('name' in pEntry) {
            pEntry.name = pEntry.name.replace(/_/g, ' ');
            pEntry.name = pEntry.name.trim();
            pEntry.name = toTitleCase(pEntry.name);
            hasNames = true;
        } else if ('name' in parts) {
            pEntry.name = toTitleCase(parts.name);
            hasNames = true;
        }

        if ('id' in pEntry) {
            pEntry.id = pEntry.id.trim();
            hasIDs = true;
        } else if ('id' in parts) {
            pEntry.id = parts.id;
            hasIDs = true;
        }

        responseDetails[key] = pEntry;
    });

    // Get responses

    let outOf = false;
    let responses = [];

    let resultDataArray = scoreData.ResponsePaper.ResultSet.ResultData;
    for (let index = 0; index < resultDataArray.length; index++) {
        let resultData = resultDataArray[index];
        let entry = {
            index: index + 1
        };

        if (resultData.id in responseDetails) {
            if ('pages' in responseDetails[resultData.id]) {
                entry.pages = responseDetails[resultData.id].pages;
                delete responseDetails[resultData.id].pages;
            }

            entry.student = responseDetails[resultData.id];

            let sortString = false;
            if ('qr' in entry.student && !/^\d{15}$/.test(entry.student.qr)) {
                sortString = entry.student.qr;
            } else if ('label' in entry.student) {
                sortString = entry.student.label;
            } else if ('name' in entry.student) {
                sortString = entry.student.name;
            } else if ('id' in entry.student) {
                sortString = entry.student.id;
            }

            if (sortString !== false) entry.sort = sortString;
        }

        let responseData = JSON.parse(resultData.results);
        let scoring = ScoringCommon.scoreResults(scoringData, responseData);

        if (scoring === false) return false;
        else if (outOf === false) outOf = scoring.outOf;
        else if (scoring.outOf !== outOf) return false;

        let studentResponses = [];
        for (let i = 0; i < itemData.length; i++) {
            let thisResponse = false;
            for (let j = 0; j < scoring.questions.length; j++) {
                if (scoring.questions[j].item_id === itemData[i].id) {
                    thisResponse = {
                        response: scoring.questions[j].response,
                        score: scoring.questions[j].score,
                        weight: scoring.questions[j].weight
                    };
                    break;
                }
            }
            if (thisResponse !== false) {
                studentResponses.push(thisResponse);
            } else return false;
        }
        entry.items = studentResponses;
        responses.push(entry);
    }

    // Sort responses

    if (userDefaults.Document.Reports.Teacher.reportorder === 'alpha') {
        responses.sort((a, b) => {
            if ('sort' in a && 'sort' in b) {
                return a.sort.localeCompare(b.sort);
            } else if ('sort' in a) {
                return 1;
            } else if ('sort' in b) {
                return -1;
            } else return a.index - b.index;
        });
    } else {
        responses.sort((a, b) => {
            if ('pages' in a && 'pages' in b) {
                return a.pages[0] - b.pages[0];
            } else return a.index - b.index;
        });
    }

    // Remove sort property

    responses.forEach(pEntry => {
        delete pEntry.sort;
    });

    // Get scoring and outcomes

    let isWeighted = (scoringData.Scoring.type !== 'Total');

    let outcomes = scoringData.Outcomes || [];
    outcomes.sort((a, b) => {
        return a.name.localeCompare(b.name);
    });

    // Assemble report data

    let reportData = {
        cacheHash: responseData.cacheHash,
        defaults: userDefaults.Document.Reports.Teacher,
        layout: layoutData,
        items: itemData,
        scoring: scoringData.Scoring,
        outcomes: outcomes,
        responses: responses,
        warnings: warnings,
        errors: errors,
        maxDecimals: maxDecimals,
        hasNames: hasNames,
        hasIDs: hasIDs,
        isWeighted: isWeighted
    };

    if (!isWeighted && responses.length > 0) {
        reportData.outOf = outOf;
    }

    if (layoutData.StudentReports && layoutData.StudentReports.outcomesLevels) {
        reportData.outcomesLevels = layoutData.StudentReports.outcomesLevels;
    }

    // Store report data to cache and return

    localStorage.setItem('reportData', JSON.stringify(reportData));
    return reportData;
}

export { getReportData };
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

$(document).ready(function() {

	var initialize = async function() {
        drawScores();
    }

    var getPageText = function(pages) {
        var pageList = numbersToList(pages);
        var pageText = (pages.length == 1) ? 'Page' : 'Pages';
        return pageText + ' ' + pageList;
    }

    var drawScores = function() {
        $('#results_expected .set_results_table').empty();
        $('#results_expected').hide();

        $('#results_other .set_results_table').empty();
        $('.output_other').hide();

        var expectedMenu = '';
        expectedMenu += '<div class="btn-group" style="float:right; margin-left:10px;">';
        expectedMenu += '<button class="btn" data-toggle="dropdown">';
        expectedMenu += 'Move to <span class="caret"></span>';
        expectedMenu += '</button>';
        expectedMenu += '<ul class="dropdown-menu pull-right">';

        var hasUnsaved = false;

        for (var i = 0; i < cacheDetails.response_data.length; ++i) {
            var thisData = cacheDetails.response_data[i];

            let pages = [];
            if ((thisData.type == 'expected') || (thisData.type == 'loaded')) {
                pages = thisData.responses
                    .flatMap(response => response.details.pages)
                    .sort((a, b) => a - b);
            }

            if (thisData.type == 'expected') {

                var newHTML = '<div>';
                newHTML += '<div style="font-weight:bold;">' + thisData.documentName + '</div>';
                newHTML += '<div>' + getPageText(pages) + '</div>';
                newHTML += '</div>';
        
                newHTML += '<div>';
                newHTML += '<div class="wait_icon" style="flex:none; margin-right:10px; opacity:0;"></div>';
                newHTML += '<button class="btn score_open" data-score-id=' + thisData.scoreID + '>Open</button>';
                newHTML += '</div>';

                expectedMenu += '<li><a href="#" class="score_move" data-score-id="' + thisData.scoreID + '">' + 
                    thisData.documentName + '</a></li>';
        
                $('#results_expected').show();
                $('#results_expected .set_results_table').append(newHTML);
        
            } else if (thisData.type == 'loaded') {

                var newHTML = '<div>';
                newHTML += '<div style="font-weight:bold;">' + thisData.documentName + '</div>';
                newHTML += '<div>' + getPageText(pages) + '</div>';
                newHTML += '</div>';
        
                newHTML += '<div class="button_wrapper" data-response-id=' + thisData.id + '>';
                newHTML += '<div class="wait_icon" style="flex:none; margin-right:10px; opacity:0;"></div>';
                newHTML += '<button class="btn score_save">Save</button>';
                newHTML += '</div>';
        
                $('.output_other').show();
                $('#results_other .set_results_table').append(newHTML);
                hasUnsaved = true;
        
            } else if (thisData.type == 'missing') {

                var newHTML = '<div>';
                newHTML += '<div style="font-weight:bold;">' + thisData.documentName + '</div>';
                newHTML += '<div><i>No pages read</i></div>';
                newHTML += '</div>';
        
                newHTML += '<div class="button_wrapper" data-response-id=' + thisData.id + '>';
                newHTML += '<div class="wait_icon" style="flex:none; margin-right:10px; opacity:0;"></div>';
                newHTML += '<button class="btn score_save" disabled>Open</button>';
                newHTML += '</div>';
        
                expectedMenu += '<li><a href="#" class="score_move" data-document-id="' + thisData.documentID + '">' + 
                    thisData.documentName + '</a></li>';

                $('#results_expected').show();
                $('#results_expected .set_results_table').append(newHTML);

            }
        }
        
        expectedMenu += '</ul>';
        expectedMenu += '</div>';

        for (var i = 0; i < cacheDetails.response_data.length; ++i) {
            var thisData = cacheDetails.response_data[i];

            if (thisData.type == 'unknown') {

                var pages = thisData.details.pages;

                var detailsText = [];
                if ('name' in thisData.details) {
                    var titleName = thisData.details.name.replace(/[a-zA-Z][^ _]*/g, 
                        txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
                    detailsText.push(titleName.trim());
                }
                if ('id' in thisData.details) {
                    detailsText.push(thisData.details.id.trim());
                }
                detailsText = detailsText.concat(thisData.results);

                var newHTML = '<div style="overflow:hidden;">';
                newHTML += '<div style="font-weight:bold; font-style:italic;">Unknown version or student</div>';
                newHTML += '<div style="overflow:hidden; white-space:nowrap; text-overflow:ellipsis;">' + 
                    getPageText(pages) + " (" + detailsText.join(', ') + ')</div>';
                newHTML += '</div>';
        
                newHTML += '<div class="button_wrapper" data-response-id=' + thisData.id + '>';
                newHTML += '<div class="wait_icon" style="flex:none; margin-right:10px; opacity:0;"></div>';
                newHTML += expectedMenu;
                newHTML += '</div>';
        
                $('.output_other').show();
                $('#results_other .set_results_table').append(newHTML);
                hasUnsaved = true;
        
            }
        }

        if (hasUnsaved) {
            window.onbeforeunload = function() {
                return true;
            };
        } else window.onbeforeunload = null;
    }

    $(document).on('click', '.score_open', function() {
        $(this).closest('.button_wrapper').find('.wait_icon').css('opacity', 1);
        $(this).attr('disabled', 'disabled');

        var scoreID = $(this).attr('data-score-id');
        window.open('/Scores/view_results/' + scoreID, '_blank');

        $(this).closest('.button_wrapper').find('.wait_icon').css('opacity', 0);
        $(this).removeAttr('disabled');
    });

    $(document).on('click', '.score_save', function() {
        $(this).closest('.button_wrapper').find('.wait_icon').css('opacity', 1);
        $(this).attr('disabled', 'disabled');

        if (pendingAJAXPost) return;
		pendingAJAXPost = true;

        var responseID = $(this).closest('.button_wrapper').attr('data-response-id');

        ajaxFetch('/Scores/set_save/' + tempID, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                response_id: responseID
            })
        }).then(async response => {
            pendingAJAXPost = false;
            const responseData = await response.json();
        
            if (responseData.success) {
                cacheDetails = responseData.cache_details;
                drawScores();

                $.gritter.add({
                    title: "Success",
                    text: "Results saved",
                    image: "/img/success.svg"
                });

            } else showSubmitError();

            $(this).closest('.button_wrapper').find('.wait_icon').css('opacity', 0);
            $(this).removeAttr('disabled'); 

        }).catch(function() {
			showSubmitError();
		});

    });

    $(document).on('click', '.score_move', function(e) {
        e.preventDefault();

        if (pendingAJAXPost) return;
		pendingAJAXPost = true;

        $(this).closest('.button_wrapper').find('.wait_icon').css('opacity', 1);
        $(this).closest('.button_wrapper').find('button').attr('disabled', 'disabled');

        var postData = { response_id: $(this).closest('.button_wrapper').attr('data-response-id') };
        if (this.hasAttribute('data-score-id')) postData.score_id = $(this).attr('data-score-id');
        else if (this.hasAttribute('data-document-id')) postData.document_id = $(this).attr('data-document-id');

        ajaxFetch('/Scores/set_move/' + tempID, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(postData)
        }).then(async response => {
            pendingAJAXPost = false;
            const responseData = await response.json();

            if (responseData.success) {
                cacheDetails = responseData.cache_details;
                drawScores();

                $.gritter.add({
                    title: "Success",
                    text: "Results moved",
                    image: "/img/success.svg"
                });

            } else showSubmitError();

            $(this).closest('.button_wrapper').find('.wait_icon').css('opacity', 0);
            $(this).removeAttr('disabled');  
              
        }).catch(function() {
			showSubmitError();
		});
    });

    initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});
		window.location.href = '/Documents';
	});
});
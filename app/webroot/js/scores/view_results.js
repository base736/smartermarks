/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as ScoresViewCommon from '/js/scores/view_common.js?7.0';
import * as XLSX from '/js/lib/sheetjs/xlsx.mjs';

var reportData = false;

var getMessageList = function(messages) {
    var list = '';

    for (let entry of messages) {
        list += '<li>' + entry.message;        
        if (entry.pages.length == 1) list += ' on page ';
        else if (entry.pages.length > 1) list += ' on pages ';

        var numbers = numbersToList(entry.pages);
        numbers = numbers.replace(/,([^,]+)$/, ' and$1');

        list += numbers + '.</li>';
    }
        
    return list;
}

$(document).ready(function() {

    var initialize = async function() {
        reportData = await ScoresViewCommon.getReportData(cacheHash, getDataToken);

        $('.message_loading').hide();

        var hasMessages = false;

        if (repeatEvents > 0) {
            $('.message_repeat_events').show();
            hasMessages = true;
        }

        if (reportData.warnings.length > 0) {
            $('#warnings_list').html(getMessageList(reportData.warnings));
            $('.message_warnings').show();
            hasMessages = true;
        }

        if (reportData.errors.length > 0) {
            $('#errors_list').html(getMessageList(reportData.errors));
            $('.message_errors').show();
            hasMessages = true;
        }

        if (!hasMessages) $('#message_wrapper').hide();

        $('#reports_wrapper').show();
    }

	$("#show_pdf").on('click', function(e) {
        e.preventDefault();

        navigateWithPost('/build_pdf', {
            pdfType: 'response_form',
            formatXML: formatXML
        });
    });

    $('#get_xlsx').on('click', function(e) {
        e.preventDefault();

        $('#view_button_wrapper .wait_icon').css('opacity', '1');

        let maxDecimals_style = '0.';
        for (let i = 0; i < reportData.maxDecimals; ++i) maxDecimals_style += '0';
    
        /**************************************************************************************************/
        /* Build title                                                                                    */
        /**************************************************************************************************/
    
        let summaryData = [];
        let merges = [];

        summaryData.push([reportData.layout.Title.text.join(' / ')]);
        summaryData.push(['Processed ' + dayjs().format('MMMM D, YYYY')]);
        summaryData.push([]);
    
        /**************************************************************************************************/
        /* Build table headings                                                                           */
        /**************************************************************************************************/
    
        let showLevels = reportData.layout.StudentReports.results !== 'numerical';
    
        let thisRow = [];
    
        let numBlank = 4;
        if (reportData.hasNames && reportData.hasIDs) numBlank++;
        if (!reportData.isWeighted) numBlank++;
        if (showLevels) numBlank++;
        for (let i = 0; i < numBlank; ++i) thisRow.push('');
    
        if (reportData.outcomes && reportData.outcomes.length > 0) {
            thisRow.push('OUTCOME SCORES');
        
            let numBlank = reportData.outcomes.length;
            if (showLevels) numBlank *= 2;
            for (let i = 0; i < numBlank; ++i) thisRow.push('');
        }
    
        thisRow.push('STUDENT RESPONSES');
    
        summaryData.push(thisRow);
    
        thisRow = [];
        
        thisRow.push('');
        if (reportData.hasNames && reportData.hasIDs) thisRow.push('');
    
        thisRow.push('');
        thisRow.push('Total');
        if (!reportData.isWeighted) thisRow.push('Percent');
        if (showLevels) thisRow.push('Level');
    
        if (reportData.outcomes && reportData.outcomes.length > 0) {
            thisRow.push('');
            for (let entry of reportData.outcomes) {
                thisRow.push(entry.name);
                if (showLevels) {
                    merges.push({ 
                        s: { 
                            c: thisRow.length - 1, 
                            r: summaryData.length 
                        },
                        e: { 
                            c: thisRow.length, 
                            r: summaryData.length 
                        } 
                    });
                    thisRow.push('');
                }
            }
        }
    
        thisRow.push('');
        for (let item of reportData.items) {
            thisRow.push({ v: item.number, t: 's' });
        }
    
        summaryData.push(thisRow);
        summaryData.push([]);
    
        /**************************************************************************************************/
        /* Build outcome total scores and item keys                                                       */
        /**************************************************************************************************/
    
        thisRow = [];
    
        thisRow.push('KEY:');
        if (reportData.hasNames && reportData.hasIDs) thisRow.push('');
    
        thisRow.push('');
        if (!reportData.isWeighted) {
            thisRow.push({ v: reportData.outOf, t: 'n', z: maxDecimals_style });            
        }

        thisRow.push({ v: 100.0, t: 'n', z: '0.0' });

        if (showLevels) thisRow.push('');
    
        thisRow.push('');
    
        // Outcomes total scores
    
        if (reportData.outcomes && reportData.outcomes.length > 0) {
            for (let outcomes_entry of reportData.outcomes) {
                if (!reportData.isWeighted) {
                    let outcome_score = 0.0;
                    for (let item of reportData.items) {
                        if (outcomes_entry.item_ids.includes(item.id) &&
                            item.scoring !== 'omit'
                        ) {
                            outcome_score += item.maxScore;
                        }
                    }
            
                    thisRow.push({ v: outcome_score, t: 'n', z: maxDecimals_style });            
                } else {
                    thisRow.push({ v: 100.0, t: 'n', z: '0.0' });
                }
        
                if (showLevels) thisRow.push('');
            }
        
            thisRow.push('');
        }
    
        // Item keys
    
        for (let item of reportData.items) {
            if (item.type === 'wr') {
                thisRow.push('WR');
            } else {
                let keyed_answers = [];
                for (let entry of item.ScoredResponses) {
                    let answer_ids = [];
                    if ('id' in entry) answer_ids = [entry.id];
                    else if ('ids' in entry) answer_ids = entry.ids;
            
                    if (entry.value === item.maxScore) {
                        let this_answer = '';
                        for (let answer of item.Answers) {
                            if (answer_ids.includes(answer.id)) {
                                if (item.type === 'nr_scientific') {
                                    this_answer += answer.response.replace(/\D/g, '');
                                } else {
                                    this_answer += answer.response;
                                }
                            }
                        }
                        keyed_answers.push(this_answer);
                    }
                }
                
                thisRow.push({ v: keyed_answers.join(', '), t: 's' });
            }
        }
    
        summaryData.push(thisRow);
        summaryData.push([]);
    
        /**************************************************************************************************/
        /* Add student results to output                                                                  */
        /**************************************************************************************************/

        let outcome_scores = {};
        let item_scores = {};
        let total_scores = [];
    
        for (let response_entry of reportData.responses) {
            thisRow = [];
        
            // Show student information
        
            if (response_entry.student.id) {
                thisRow.push(response_entry.student.id);
            } else if (reportData.hasIDs) thisRow.push('');
        
            if (response_entry.student.name) {
                thisRow.push(response_entry.student.name);
            } else if (reportData.hasNames) thisRow.push('');
        
            if (!reportData.hasIDs && !reportData.hasNames) {
                thisRow.push(response_entry.index);
            }
        
            thisRow.push('');
        
            // Show total score
        
            let total_score = 0.0;
            for (let i = 0; i < reportData.items.length; ++i) {
                let item_entry = reportData.items[i];
                if (item_entry.scoring !== 'omit') {
                    let results_entry = response_entry.items[i];
                    if (results_entry.score !== false) {
                        total_score += results_entry.score * results_entry.weight;
                    }
                }
            }
        
            let percent;
            if (reportData.isWeighted) {
                percent = total_score * 100;
            } else {
                percent = (total_score * 100) / reportData.outOf;
                thisRow.push({ v: total_score, t: 'n', z: maxDecimals_style });            
            }
        
            total_scores.push(percent);
            thisRow.push({ v: percent, t: 'n', z: '0.0' });
        
            if (showLevels) {
                let this_level = '';
                for (let entry of reportData.outcomesLevels) {
                    if (percent >= entry.from) this_level = entry.name;
                }
                thisRow.push(this_level);
            }
        
            thisRow.push('');
        
            // Show outcomes scores
        
            if (reportData.outcomes && reportData.outcomes.length > 0) {
                for (let outcomes_entry of reportData.outcomes) {
                    let outcome_score = 0.0;
                    let outcome_max_score = 0.0;
                    for (let i = 0; i < reportData.items.length; ++i) {
                        let item_entry = reportData.items[i];
                        if (item_entry.scoring !== 'omit') {
                            let results_entry = response_entry.items[i];
                            if (results_entry.score !== false) {
                                if (outcomes_entry.item_ids.includes(item_entry.id)) {
                                    outcome_score += results_entry.score * results_entry.weight;
                                    outcome_max_score += item_entry.maxScore * results_entry.weight;
                                }    
                            }
                        }
                    }
            
                    if (!outcome_scores[outcomes_entry.id]) {
                        outcome_scores[outcomes_entry.id] = [];
                    }
            
                    if (outcome_max_score > 0.0) {
                        percent = (100.0 * outcome_score) / outcome_max_score;
                    } else percent = 0.0;
                    outcome_scores[outcomes_entry.id].push(percent);
            
                    if (!reportData.isWeighted) {
                        thisRow.push({ v: outcome_score, t: 'n', z: maxDecimals_style });
                    } else {
                        thisRow.push({ v: percent, t: 'n', z: '0.0' });
                    }
            
                    if (showLevels) {
                        let this_level = '';
                        for (let entry of reportData.outcomesLevels) {
                        if (percent >= entry.from) this_level = entry.name;
                        }
                        thisRow.push(this_level);
                    }
                }
        
                thisRow.push('');
            }
        
            // Show responses
        
            for (let i = 0; i < reportData.items.length; ++i) {
                let item_entry = reportData.items[i];
                let results_entry = response_entry.items[i];

                if (item_entry.scoring !== 'omit') {
                    if (results_entry.score !== false) {
                        if (!item_scores[item_entry.id]) {
                            item_scores[item_entry.id] = [];
                        }
                        if (item_entry.maxScore > 0) {
                            item_scores[item_entry.id].push(
                                (100.0 * results_entry.score) / item_entry.maxScore
                            );
                        }    
                    }
                }
        
                if (item_entry.type === 'wr') {
                    let score = 0;
                    for (let j = 0; j < results_entry.response.length; j += 2) {
                        if (results_entry.response[j] !== ' ')
                        score += parseFloat(results_entry.response[j]);
                        if (results_entry.response[j + 1] === '+') score += 0.5;
                    }
                    thisRow.push({ v: score, t: 'n', z: '0.0' });
                } else {
                    thisRow.push({ v: results_entry.response, t: 's' });
                }
            }
        
            summaryData.push(thisRow);
        }

        /**************************************************************************************************/
        /* Add average scores for each outcome and question                                               */
        /**************************************************************************************************/
    
        summaryData.push([]);
    
        thisRow = [];
    
        thisRow.push('AVERAGE');
        thisRow.push('');
    
        if (total_scores.length > 0) {
            let this_average = total_scores.reduce((a, b) => a + b, 0) / total_scores.length;
        
            if (!reportData.isWeighted) {
                let this_average_score = (this_average * reportData.outOf) / 100;
                thisRow.push({ v: this_average_score, t: 'n', z: maxDecimals_style });
            }

            thisRow.push({ v: this_average, t: 'n', z: '0.0' });
        } else thisRow.push('');
    
        thisRow.push('');
    
        if (showLevels) thisRow.push('');
    
        if (reportData.outcomes && reportData.outcomes.length > 0) {
            for (let key in outcome_scores) {
                let entry = outcome_scores[key];
        
                if (entry.length > 0) {
                    let this_average = entry.reduce((a, b) => a + b, 0) / entry.length;
                    thisRow.push({ v: this_average, t: 'n', z: '0.0' });
                } else thisRow.push('');
        
                if (showLevels) thisRow.push('');
            }
            thisRow.push('');
        }
    
        for (let i = 0; i < reportData.items.length; ++i) {
            let item_id = reportData.items[i].id;
            if (item_scores[item_id]) {
                let entry = item_scores[item_id];
        
                if (entry.length > 0) {
                    let this_average = entry.reduce((a, b) => a + b, 0) / entry.length;
                    thisRow.push({ v: this_average, t: 'n', z: '0.0' });
                } else thisRow.push('');
        
            } else thisRow.push('Omitted');
        }
    
        summaryData.push(thisRow);
    
        /**************************************************************************************************/
        /* Generate XLSX                                                                                  */
        /**************************************************************************************************/
    
        let worksheet = XLSX.utils.aoa_to_sheet(summaryData);
    
        // Fix column width
    
        let num_columns = 0;
        for (let summary_row of summaryData) {
            if (summary_row.length > num_columns) num_columns = summary_row.length;
        }
    
        worksheet['!cols'] = [];
        for (let i = 0; i < num_columns; ++i) {
            worksheet['!cols'].push({ wch: 10 }); // Set column width to 10
        }
    
        // Merge cells as appropriate
    
        if (merges.length > 0) worksheet['!merges'] = merges;

        // Create workbook and append worksheet
    
        let workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
    
        // Write workbook to binary string
    
        let workbookBinary = XLSX.write(workbook, {
            bookType: 'xlsx',
            type: 'binary',
        });
  
        // Download

        const arrayBuffer = new ArrayBuffer(workbookBinary.length);
        const view = new Uint8Array(arrayBuffer);
        for (let i = 0; i < workbookBinary.length; ++i) {
            view[i] = workbookBinary.charCodeAt(i) & 0xff;
        }

        const blob = new Blob([arrayBuffer], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        });
        const link = document.createElement("a");
        if (link.download !== undefined) {
            const url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", 'summary.xlsx');
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        $('#view_button_wrapper .wait_icon').css('opacity', '0');
    });

    $('#get_csv').on('click', function(e) {
        e.preventDefault();

        $('#view_button_wrapper .wait_icon').css('opacity', '1');

        // Build the CSV rows

        const results = [];
    
        // Construct the header line

        const headerLine = [];
        if (reportData.hasIDs) headerLine.push("Student ID");
        if (reportData.hasNames) headerLine.push("Name");
        if (!reportData.hasIDs && !reportData.hasNames) headerLine.push("Page");
    
        let scoreHeader = "Score";
        if (!reportData.isWeighted) {
            scoreHeader += " /" + Number(reportData.outOf).toFixed(reportData.maxDecimals);
        } else {
            scoreHeader += " /100.0";
        }
        headerLine.push(scoreHeader);
        results.push(headerLine);
    
        // Process each response

        for (let response_entry of reportData.responses) {
            const resultLine = [];
    
            if (response_entry.student && response_entry.student.id) {
                resultLine.push(response_entry.student.id);
            } else if (reportData.hasIDs) {
                resultLine.push("");
            }
    
            if (response_entry.student && response_entry.student.name) {
                resultLine.push(response_entry.student.name);
            } else if (reportData.hasNames) {
                resultLine.push("");
            }
    
            if (!reportData.hasIDs && !reportData.hasNames) {
                resultLine.push(response_entry.index);
            }
    
            // Calculate the total score

            let totalScore = 0.0;
            for (let i = 0; i < reportData.items.length; ++i) {
                let item_entry = reportData.items[i];
                if (item_entry.scoring !== 'omit') {
                    let results_entry = response_entry.items[i];
                    if (results_entry.score !== false) {
                        totalScore += results_entry.score * results_entry.weight;
                    }
                }
            }
    
            // Format the total score

            if (!reportData.isWeighted) {
                resultLine.push(totalScore.toFixed(reportData.maxDecimals));
            } else {
                resultLine.push((totalScore * 100).toFixed(1));
            }
    
            results.push(resultLine);
        }
    
        // Convert the results array to CSV format

        const csvText = results.map((line) => line.map((field) => {
            if (field == null) field = '';
            field = field.toString();
            if (field.includes('"')) {
                field = field.replace(/"/g, '""');
                return `"${field}"`;
            } else if (field.includes(',') || field.includes('\n')) {
                return `"${field}"`;
            } else {
                return field;
            }
        }).join(',')).join('\n');

        // Download

        const blob = new Blob([csvText], { type: 'text/csv;charset=utf-8;' });
        const link = document.createElement("a");
        if (link.download !== undefined) {
            const url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", 'results.csv');
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        $('#view_button_wrapper .wait_icon').css('opacity', '0');
    });
	
	// Load report data

	initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});
		window.location.href = '/Documents';
	});

});
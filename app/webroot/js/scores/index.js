/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as TableSelection from '/js/common/table_selection.js?7.0';

$.extend($.ui.dialog.prototype.options, {
	resizable: false,
    create: function() {
        var $this = $(this);

        $this.keypress(function(e) {
            if (e.key === 'Enter' && $this.attr('id') != 'editScore_dialog'
            					&& $this.attr('id') != 'newAnalysis_dialog') {
                $this.parent().find('.ui-dialog-buttonpane .btn-save').click();
                return false;
            }
        });
    }
});

$(document).ready(function() {

    TableSelection.initialize();

	var scoreData = false;
	var searchMatches = [];
	var matchIndex = 0;

	var hasName = false;
	var hasID = false;
	var hasQR = false;

    var updateIndex = function() {
        var urlVars = getUrlVars();

        // Show loading in index list
    
        $('.index_table').html('<tbody><tr class="disabled"><td colspan="5"><div style="padding-left:10px;">' +
            '<div class="wait_icon"></div><i>Loading&hellip;</i></div></td></tr></tbody>');
        $('#footer_wrapper').hide();

        // Get parameters to pass to server

        var getParams = {};
        if (documentID === false) {
            getParams.limit = ('limit' in urlVars) ? urlVars.limit : 10;
            getParams.page = ('page' in urlVars) ? urlVars.page : 1;
        } else {
            getParams.documentID = documentID;
            getParams.limit = 'all';
        }

		ajaxFetch('/Scores/get_index', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(getParams)
		}).then(async response => {
			const responseData = await response.json();

            if (responseData.success) {

                // Fill in index and update action buttons

                $('.index_table').empty();

                var html = '';
                if (responseData.results.length == 0) {

                    html += '<tr class="disabled"><td colspan="6"><div style="padding-left:10px;">';
                    html += '<i>No scores to show here.</i>';
                    html += '</div></td></tr>';
    
                } else if (documentID !== false) {

                    var showEmails = false;
                    var userID = responseData.results[0].Score.user_id;
                    for (var i = 1; i < responseData.results.length; ++i) {
                        if (responseData.results[i].Score.user_id != userID) showEmails = true;
                    }

                    if (showEmails) $('.emails_element').show();
                    else $('.emails_element').hide();

                    for (var i = 0; i < responseData.results.length; ++i) {
                        var data = responseData.results[i];
                    
                        html += '<tr data-score-id="' + data.Score.id + '">';
                        html += '<td style="text-align:center; width:15px;">';
						html += '<input type="checkbox" class="item_select" autocomplete="off" />';
					    html += '</td>';
				
                        html += '<td width="15px"></td>';
                    
                        if (showEmails) {
                            html += '<td class="score_owner" width="250px">' + data.Score.user_email + '</td>'
                        }
                    
                        var date = dayjs.utc(data.Score.created).local();
                        html += '<td class="score_created" width="120px">' + date.format('YYYY-MM-DD HH:mm') + '</td>';

                        html += '<td class="score_details">';
                        html += (data.Score.title == '') ? "<i>[No details]</i>" : data.Score.title;
                        html += '</td>';

                        html += '</tr>';
                    }

                } else {

                    for (var i = 0; i < responseData.results.length; ++i) {
                        var data = responseData.results[i];
                    
                        html += '<tr data-score-id="' + data.Score.id + '">';
                    
                        html += '<td style="text-align:center; width:15px;">';
                        html += '<input type="checkbox" class="item_select" autocomplete="off" />';
                        html += '</td>';
                    
                        html += '<td>' + data.Score.user_email + '</td>';

                        var date = dayjs.utc(data.Score.created).local();
                        html += '<td width="110px">' + date.format('YYYY-MM-DD HH:mm') + '</td>';

                        html += '<td style="width:140px; font-family:\'Lucida Console\', Monaco, monospace;">';
                        html += (data.Score.pdf_hash == null) ? '--' : data.Score.pdf_hash.substr(0, 16);
                        html += '</td>';

                        html += '<td style="width:40px; text-align:right;">';
                        html += (data.Score.pageCount == null) ? '--' : data.Score.pageCount;
                        html += '</td>';

                        html += '<td style="width:40px; text-align:right;">';
                        html += (data.Score.errorCount == null) ? '--' : data.Score.errorCount;
                        html += '</td>';

                        html += '</tr>';
                    }

                }

                $('.index_table').html(html);

                TableSelection.updateItemActions();

                // Show footer

                if ('count_total' in responseData) {
                    html = responseData.count_total.toLocaleString() + ' item';
                    if (responseData.count_total != 1) html += 's';
                    $('#count_wrapper').html(html);    
                }

                var pageNum = ('page' in urlVars) ? parseInt(urlVars.page) : 1;

                var numPages;
                if ('count_matching' in responseData) {
                    numPages = ('limit' in responseData) ? Math.ceil(responseData.count_matching / responseData.limit) : 1;
                } else numPages = false;

                if (responseData.results.length > 0) {
                    html = '<span class="previous pag_button">&lt;</span>';
                    if ((numPages > 1) || (numPages === false)) {
                        var minPage, maxPage;
                        if (numPages === false) {

                            minPage = pageNum - 2;
                            maxPage = pageNum + 2;
                            if (minPage < 1) {
                                minPage = 1;
                                maxPage = 5;
                            }

                        } else if (numPages > 5) {

                            minPage = pageNum - 2;
                            maxPage = pageNum + 2;
                            if (minPage < 1) {
                                minPage = 1;
                                maxPage = 5;
                            } else if (maxPage > numPages) {
                                minPage = numPages - 4;
                                maxPage = numPages;
                            }

                        } else {

                            minPage = 1;
                            maxPage = numPages;	
                            
                        }
                        for (var pageLabel = minPage; pageLabel <= maxPage; ++pageLabel) {
                            html += '<span class="pag_button';
                            if (pageLabel == pageNum) html += ' current';
                            html += '">' + pageLabel + '</span>';
                        }
                    }
                    html += '<span class="next pag_button">&gt;</span>';

                    $('#pagination_wrapper').html(html);
                    $('#pagination_wrapper').show();
                    
                } else $('#pagination_wrapper').hide();

                if (pageNum == 1) {
                    $('.index_table tr:first').addClass('first_row');
                    $('.pag_button.previous').addClass('disabled');
                }
                if (pageNum == numPages) {
                    $('.index_table tr:last').addClass('last_row');
                    $('.pag_button.next').addClass('disabled');
                }

                $('#footer_wrapper').show();
            }
        });        
    }

    $(document).on('click', '.pag_button:not(.disabled, .current)', function() {
		var urlVars = getUrlVars();
		var pageNum = ('page' in urlVars) ? parseInt(urlVars.page) : 1;

		if ($(this).is('.previous')) pageNum = pageNum - 1;
		else if ($(this).is('.next')) pageNum = pageNum + 1;
		else pageNum = parseInt($(this).text());

		urlVars.page = pageNum;
        setUrlVars(urlVars);

        updateIndex();
	});

	$(document).bind('keydown', function (e) {
		if ($('.ui-dialog:visible').length == 0) {
	    	if (((e.key === 'Backspace') || (e.key === 'Delete')) && !$(e.target).is("input:not([readonly]), textarea")) {
				$('#deleteScore_button').trigger("click");
			}
		}
	});

	var saveScore = function() {
		var $dialog = $(this);

		if (storeResponses()) {
		    if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			ajaxFetch('/Scores/save/' + $('#score_id').val(), {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					title: $('#score_title').val(),
					responses: JSON.stringify(scoreData)
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();

				if (responseData.success) {
                    var $scores_row = $('#scores tr[data-score-id="' + $('#score_id').val() + '"]');
                    if ($scores_row.length == 1) {
                        var newDetails = $('#score_title').val().trim();
                        if (newDetails.length > 0) {
                            $scores_row.find('.score_details').html(newDetails);
                        } else $scores_row.find('.score_details').html( "<i>[No details]</i>");
                    }

					$.gritter.add({
						title: "Success",
						text: "Score saved",
						image: "/img/success.svg"
					});	
				} else {
					$.gritter.add({
						title: "Error",
						text: "Error saving score",
						image: "/img/error.svg"
					});	
				}
				
				$dialog.dialog('close');
				$('.item_select').prop('checked', false);
				$('.item_select').closest('tr').removeClass('doc-clicked');
				TableSelection.updateItemActions();
			});
		}
	}

	var deleteScore = function() {
		var $dialog = $(this);

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		var selectedItems = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var scoreID = $(this).closest('tr').data('score-id');
				selectedItems.push(scoreID);
			}
		});

		ajaxFetch('/Scores/delete', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				score_ids: selectedItems
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				var message = responseData.count + " score" + ((responseData.count == 1) ? "" : "s") + " deleted.";

				addClientFlash({
					title: "Success",
					text: message,
					image: "/img/success.svg"
				});	

				window.location.reload();
			} else showSubmitError($dialog);
		});
	}
	
	$('#viewScore_button').click(function(e) {
		e.preventDefault();
		if ($(this).prop('disabled')) return;

		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var $tr = $(this).closest('tr');
				var scoreID = $tr.data('score-id');
				window.location = "/scores/view_results/" + scoreID;
			}
		});
	});

	$(document).on('dblclick', '.index_table tbody tr', function(e) {
		e.preventDefault();
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		var scoreID = $(this).data('score-id');
		if (numChecked == 0) {
			window.location = "/scores/view_results/" + scoreID;
		} else if (numChecked == 1) {
			var $selectedID = $selectedCheckbox.closest('tr').data('doc-id');
			if ($selectedID == scoreID) {
				window.location = "/scores/view_results/" + scoreID;
			}
		}
	});
	
	$('#newScore_button').click(function(e) {
		e.preventDefault();
		if ($(this).attr('disabled')) return;

		$("#na_title").val("");

		$('#na_filter_string').val("[]");
		$('#filters_criteria').html('');

		$('#addFilters_button').show();
		$('#filters_wrapper').hide();
				
		$('#newAnalysis_dialog').dialog('open');		
		$("#na_title").focus();
	});

	$('#addFilters_button').click(function(e) {
		$('#filters_wrapper').show();
		$('#addFilters_button').hide();		

		var filters = [{index:-1, response:''}];
		$('#na_filter_string').val(JSON.stringify(filters));
		rebuildFilters();
	});
	
	$(document).on('click', '.filterAdd', function() {
		if (validateFilters(-1)) {
			rebuildFilterString();
			var index = $(this).closest('.options_row').index() + 1;
			var filters = JSON.parse($('#na_filter_string').val());
			filters.splice(index, 0, {index:-1, response:''});
			$('#na_filter_string').val(JSON.stringify(filters));
			rebuildFilters();
		}
    });
    
	$(document).on('click', '.filterDelete', function() {
		var index = $(this).closest('.options_row').index();
		if (validateFilters(index)) {
			rebuildFilterString();
			var filters = JSON.parse($('#na_filter_string').val());
			filters.splice(index, 1);
						
			if (filters.length == 0) {
				$('#addFilters_button').show();
				$('#filters_wrapper').hide();
				$('#na_filter_string').val('[]');
			} else {
				$('#na_filter_string').val(JSON.stringify(filters));
			}

			rebuildFilters();
		}
    });

	var rebuildFilterString = function() {
		var filters = [];

		$('#filters_criteria').children().each(function() {
			var filterSection = parseInt($(this).find('.filterSection').val());
			var filterNumber = $(this).find('.filterNumber').val();
			var filterResponse = $(this).find('.filterResponse').val();
		
			var thisFilter = {
				index: -1,
				response: filterResponse
			};
			
			if (filterSection >= 0) {
				for (var i = 0; i < documentScoring.Sections[filterSection].Content.length; ++i) {
					if (documentScoring.Sections[filterSection].Content[i].number == filterNumber) {
						thisFilter.index = i;
					}
				}
				if (thisFilter.index >= 0) {
					for (var i = 0; i < filterSection; ++i) {
						if ('Content' in documentScoring.Sections[i]) {
							thisFilter.index += documentScoring.Sections[i].Content.length;
						}
					}
				}
			}

			filters.push(thisFilter);
		});
		
		$('#na_filter_string').val(JSON.stringify(filters));
	}
	
	var rebuildFilters = function() {
		var filters = JSON.parse($('#na_filter_string').val());

		var filterHTML = 
			'<div class="options_row" style="display:flex; align-items:center;"> \
				<label style="flex:none; width:auto;">Section:&nbsp</label> \
				<select style="flex:1 1 auto;" class="filterSection defaultSelectBox"> \
					<option value="-1" selected>Choose a section...</option>';

		for (var i = 0; i < documentLayout.Body.length; ++i) {
			if ((documentLayout.Body[i].type != 'page_break') && (documentLayout.Body[i].type != 'wr')) {
				filterHTML += '<option value="' + i + '">' + documentLayout.Body[i].header + '</option>';
			}
		}

		filterHTML +=  
				'</select> \
				<label style="flex:none; width:auto; margin-left:10px;">Question:&nbsp</label> \
				<input class="filterNumber defaultTextBox2" style="flex:none; width:30px;" type="text" /> \
				<label style="flex:none; width:auto; margin-left:10px;">Answer is:&nbsp</label> \
				<input class="filterResponse defaultTextBox2" style="flex:none; width:30px;" type="text" /> \
				<button title="Add" class="actions_button add_icon filterAdd" style="flex:none; margin-left:15px;">&nbsp;</button> \
				<button title="Delete" class="actions_button delete_icon filterDelete" style="flex:none;">&nbsp;</button> \
			</div>';

		$('#filters_criteria').html('');
		for (var j = 0; j < filters.length; ++j) {
			var $newFilter = $(filterHTML).appendTo($('#filters_criteria'));
			$newFilter.find('.filterResponse').val(filters[j].response);

			if (filters[j].index >= 0) {
				var questionIndex = filters[j].index;
				for (var i = 0; i < documentScoring.Sections.length; ++i) {
					if (questionIndex < documentScoring.Sections[i].Content.length) {
						var number = documentScoring.Sections[i].Content[questionIndex].number;
						$newFilter.find('.filterSection').val(i);
						$newFilter.find('.filterNumber').val(number);
						break;
					} else questionIndex -= documentScoring.Sections[i].Content.length;
				}
			}
		}
	}
	
	var validateFilters = function(skipIndex) {
		var isValid = true;
		$('#filters_criteria').children().each(function() {
			if ($(this).index() == skipIndex) return;

			var filterSection = parseInt($(this).find('.filterSection').val());
			if (filterSection == -1) {

				isValid = false;
				$(this).find('.filterSection').validationEngine('showPrompt', 
					'* Must choose a section', 'error', 'bottomLeft', true);

			} else {

				var filterNumber = $(this).find('.filterNumber').val();

				var filterQuestion = -1;
				for (var i = 0; i < documentScoring.Sections[filterSection].Content.length; ++i) {
					if (documentScoring.Sections[filterSection].Content[i].number == filterNumber) {
						filterQuestion = i;
					}
				}

				if (filterQuestion == -1) {
					isValid = false;
					$(this).find('.filterNumber').validationEngine('showPrompt', 
						'* Invalid question number', 'error', 'bottomLeft', true);
				}

			}			
		});

		return isValid;
	}
	
	var buildAnalysis = function() {
		var $dialog = $(this);

		var scoreIDs = [];
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var $tr = $(this).closest('tr');
				var scoreID = parseInt($tr.data('score-id'));
				scoreIDs.push(scoreID);
			}
		});

		var isValid = true;
		if ($('#na_title').val().length == 0) {
			$('#na_title').validationEngine('showPrompt', 
				'* Analysis must have a title.', 'error', 'bottomLeft', true);
			isValid = false;
		} else isValid &= validateFilters(-1);

		if (isValid) {
			rebuildFilterString();
			var filters = JSON.parse($('#na_filter_string').val());
            var filterMode = $('#filters_operation').val();
		
			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

            var formatXML = false;
            var scorePromises = [];
            var scoreData = [];
            for (var i = 0; i < scoreIDs.length; ++i) {
				var thisPromise = ajaxFetch('/Scores/get/' + scoreIDs[i])
				.then(async response => {
					const responseData = await response.json();
		
					if (responseData.success) {
						if ((formatXML === false) && ('format' in responseData)) {
							formatXML = responseData.format;
						}
						for (var j = 0; j < responseData.responses.length; ++j) {
							var includeThis = true;
							if (filters.length > 0) {
								if (filterMode == 'any') includeThis = false;
								for (var k = 0; k < filters.length; ++k) {
									var trimmedValue = responseData.responses[j].results[filters[k].index].trim();
									if ((filterMode == 'all') && (trimmedValue != filters[k].response)) includeThis = false;
									if ((filterMode == 'any') && (trimmedValue == filters[k].response)) includeThis = true;
								}
							}
							if (includeThis) {
								delete responseData.responses[j].details.pages;
								scoreData.push(responseData.responses[j]);
							}                  
						}
						return;

					} else {

						throw new Error('Invalid score data in buildAnalysis');

					}
				});

                scorePromises.push(thisPromise);
            }

            Promise.all(scorePromises)
            .then(async function() {
				
				const response = await ajaxFetch('/Scores/create', {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						documentID: documentID,
						title: $('#na_title').val(),
						formatXML: formatXML,
						responses: JSON.stringify(scoreData)
					})
				});
		
				const responseData = await response.json();
		
				if (responseData.success) {
					window.location = '/Scores/view_results/' + responseData.id;
				} else showSubmitError($dialog);

            });
		}
	}

	$('#editScore_button').click(function(e){
		e.preventDefault();
		if ($(this).attr('disabled')) return;

		var $tr = null;
		var scoreID = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$tr = $(this).closest('tr');
				scoreID = $tr.data('score-id');
			}
		});
		
		$('#score_id').val(scoreID);

		$('#data_wrapper').hide();
		$('#loading_wrapper').show();

		if ($('#data_wrapper_inner').html().length == 0) {

			// Draw interface

			var inputHTML = "<input type='hidden' id='displayed_index' />";
			for (var i = 0; i < documentScoring.Sections.length; ++i) {
				var layoutSection = false;
				for (var j = 0; j < documentLayout.Body.length; ++j) {
					if (documentLayout.Body[j].id == documentScoring.Sections[i].id) {
						layoutSection = documentLayout.Body[j];
					}
				}
				
				inputHTML += "<label>" + layoutSection.header + "</label>";
	
				for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
					var questionData = documentScoring.Sections[i].Content[j];
	
					var questionStyle = questionData.type.substr(0, 2);				
					if (questionStyle == 'mc') {
	
						inputHTML += 
							"<div class='response_entry' data-section-index='" + i + "' data-content-index='" + j + "' style='width:65px;'> \
								<span style='display: inline-block; width: 25px; position:relative; top:1px;'>" + questionData.number + ".</span> \
								<input type='text' class='response_input' style='width:20px;'> \
							</div>";
	
					} else if (questionStyle == 'nr') {
	
						var size = questionData.TypeDetails.nrDigits;
						inputHTML += 
							"<div class='response_entry' data-section-index='" + i + "' data-content-index='" + j + "' style='width:" + (45 + size * 9) + "px;'> \
								<span style='display: inline-block; width: 25px; position:relative; top:1px;'>" + questionData.number + ".</span> \
								<input type='text' class='response_input' data-nr-style='" + layoutSection.fieldStyle + "' style='width:" + (size * 9) + "px;'> \
							</div>";
	
					} else if (questionStyle == 'wr') {
						
						var criteria = questionData.TypeDetails.criteria;
						inputHTML += 
							"<div class='response_entry' data-section-index='" + i + "' data-content-index='" + j + "' style='width:" + (34 + 36 * criteria.length + 10 * (criteria.length - 1)) + "px;'> \
								<span style='display: inline-block; width: 25px; position:relative; top:1px;'>" + questionData.number + ".</span>";
	
						for (var k = 0; k < questionData.TypeDetails.criteria.length; ++k) {
							if (k > 0) inputHTML += "<span width='10px'>+</span>";
							inputHTML += "<input type='text' class='response_input' data-criteria-index='" + k + "' style='width:25px; margin-right:2px; margin-left:3px;'>";
						}
						inputHTML += "</div>";
	
					}
				}
			}
			$('#data_wrapper_inner').html("<div style='width: 650px;'>" + inputHTML + "</div>");
		}

		$('.score-detail-wrapper').hide();
		$('#score_search_wrapper').hide();
		$('#scores_search').val('');

		$('#editScore_dialog').dialog('open');

		var $buttonPane = $('#editScore_dialog').siblings('.ui-dialog-buttonpane');
		$buttonPane.find('button:contains("Save")').prop('disabled', true);

		// Get score data

		scoreData = false;
		ajaxFetch('/Scores/get/' + scoreID)
		.then(async response => {
			const responseData = await response.json();

			if (responseData.success) {
                $('#score_title').val(responseData.title);        
				scoreData = responseData.responses;

                hasName = false;
				hasID = false;
				hasQR = false;

                for (var i = 0; i < scoreData.length; ++i) {
                    if ('name' in scoreData[i].details) {
						scoreData[i].details.name = scoreData[i].details.name.replace('_', ' ').trim();
                        hasName = true;
                    }

					if ('id' in scoreData[i].details) {
						scoreData[i].details.id = scoreData[i].details.id.replace('_', ' ').trim();
                        hasID = true;
                    }

					if ('qr' in scoreData[i].details) hasQR = true;
                }

				// Update view
				
				if (hasName) $('#score_name_wrapper, #score_search_wrapper').show();
				if (hasID) $('#score_id_wrapper, #score_search_wrapper').show();
				if (hasQR) $('#score_qr_wrapper, #score_search_wrapper').show();
			
				$('#assessment_number').val(1);
				drawResponses();

				$('#num_responses').text(' of ' + scoreData.length);
				$('#assessment_number').spinner({
					step: 1,
					min: 1,
					max: scoreData.length,
					stop: drawResponses
				});

				$('#assessment_number').on('spinstart keydown', function(e) {
					if (!storeResponses()) {
						e.preventDefault();
						return false;
					}
				});

				$('#assessment_number').on('keypress', function(e) {
					drawResponses();
				});
	
				$('#editScore_dialog .search_nav').addClass('disabled');
			
				$buttonPane.find('button:contains("Save")').prop('disabled', false);

				$('#loading_wrapper').hide();
				$('#data_wrapper').show();
	
			} else {

				throw new Error('Invalid score data');

			}

		}).catch(function() {
			showSubmitError();
		});
	});
	
	var storeResponses = function() {
		var scoreIndex = parseInt($('#displayed_index').val());
		if (scoreIndex == -1) return true;
		
		var validates = true;
        $('.response_entry.modified').each(function() {
            var i = $(this).attr('data-section-index');
            var j = $(this).attr('data-content-index');

            var questionData = documentScoring.Sections[i].Content[j];
            if ($(this).length == 1) {
                var questionStyle = questionData.type.substr(0, 2);

                if (questionStyle == 'mc') {

                    var $responseInput = $(this).find('.response_input');
                    if ($responseInput.length == 1) {
                        var responseValue = $responseInput.eq(0).val();

                        var mcRegex, errorText;
                        if (questionData.type == 'mc_truefalse') {
                            mcRegex = /^[tf]$/;
                            errorText = 't or f';
                        } else {
                            if (questionData.Answers.length == 1) {
                                mcRegex = /^[a ]$/;
                                errorText = 'a or blank';
                            } else {
                                var maxLetter = String.fromCharCode(96 + questionData.Answers.length);
                                mcRegex = new RegExp('^[a-' + maxLetter + ' ]*$');
                                errorText = 'letters a-' + maxLetter;
                            }
                        }
    
                        if (mcRegex.test(responseValue)) {
                            var hasDuplicates = false;
                            for (var k = 0; k < responseValue.length; ++k) {
                                if (responseValue.indexOf(responseValue[k]) != k) hasDuplicates = true;
                            }

                            if (hasDuplicates) {
                                $responseInput.validationEngine('showPrompt', '* Must not repeat letters', 'error', 'bottomLeft', true);
                                validates = false;
                            }	
                        } else {
                            $responseInput.validationEngine('showPrompt', '* Must contain ' + errorText + ' only', 'error', 'bottomLeft', true);
                            validates = false;
                        }
                    } else validates = false;

                } else if (questionStyle == 'nr') {

                    var $responseInput = $(this).find('.response_input');
                    if ($responseInput.length == 1) {
                        var responseValue = $responseInput.eq(0).val();

                        var nrRegex;
                        if ($responseInput.attr('data-nr-style') == 'Fractions') nrRegex = /^[-]?[\.\d\/]*$/;
                        else if ($responseInput.attr('data-nr-style') == 'Negatives') nrRegex = /^[-]?[\.\d]*$/;
                        else nrRegex = /^[\.\d]*$/;
        
                        if (!nrRegex.test(responseValue)) {
                            $responseInput.validationEngine('showPrompt', '* Not a valid NR response', 'error', 'bottomLeft', true);
                            validates = false;
                        }
                    } else validates = false;

                } else if (questionStyle == 'wr') {

                    var wrRegex = /^[\d]?(\.[05])?$/;

                    for (var k = 0; k < questionData.TypeDetails.criteria.length; ++k) {
                        var $responseInput = $(this).find('.response_input[data-criteria-index="' + k + '"]');
                        if ($responseInput.length == 1) {
                            var responseValue = $responseInput.eq(0).val();
                            if (!wrRegex.test(responseValue)) {
                                $responseInput.validationEngine('showPrompt', '* Not a valid written response score', 'error', 'bottomLeft', true);
                                validates = false;
                            }
                        } else validates = false;
                    }
                }
            } else validates = false;            
        });

        if (!validates) return false;

        if ($('#score_field_name').val().length > 0) {
			scoreData[scoreIndex].details.name = $('#score_field_name').val().toLowerCase();
		} else delete scoreData[scoreIndex].details.name;

		if ($('#score_field_id').val().length > 0) {
			scoreData[scoreIndex].details.id = $('#score_field_id').val();
		} else delete scoreData[scoreIndex].details.id;

		if ($('#score_field_qr').val().length > 0) {
			scoreData[scoreIndex].details.qr = $('#score_field_qr').val();
		} else delete scoreData[scoreIndex].details.qr;

		var questionIndex = 0;
		for (var i = 0; i < documentScoring.Sections.length; ++i) {
			if (!('Content' in documentScoring.Sections[i])) continue;
			for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
				var questionData = documentScoring.Sections[i].Content[j];
				var $responseEntry = $('.response_entry[data-section-index="' + i + '"][data-content-index="' + j + '"]');

                var questionStyle = questionData.type.substr(0, 2);
                var responseString = '';

                if ((questionStyle == 'mc') || (questionStyle == 'nr')) {

                    responseString = $responseEntry.find('.response_input').eq(0).val();

                } else if (questionStyle == 'wr') {

                    for (var k = 0; k < questionData.TypeDetails.criteria.length; ++k) {
                        var $responseInput = $responseEntry.find('.response_input[data-criteria-index="' + k + '"]');
                        if ($responseInput.length == 1) {
                            var responseValue = $responseInput.eq(0).val();
                            var valueParts = responseValue.split('.');
                            var criterionValue = valueParts[0];
                            if (valueParts.length < 2) criterionValue += ' ';
                            else if (valueParts[1] == '5') criterionValue += '+';
                            else criterionValue += ' ';
                            responseString += criterionValue;
                        } else return false;
                    }
                }	

                scoreData[scoreIndex].results[questionIndex] = responseString;
				questionIndex++;
			}
		}
		
		return true;
	}
	
	var drawResponses = function() {
		var scoreIndex = parseInt($('#assessment_number').val()) - 1;
		var validIndex = ((scoreIndex >= 0) && (scoreIndex < scoreData.length));

        $('.response_entry').removeClass('modified');

		if (validIndex) {

			$('#displayed_index').val(scoreIndex);
		
			if ('name' in scoreData[scoreIndex].details) {
				var nameParts = scoreData[scoreIndex].details.name.split(' ');
				for (var i = 0; i < nameParts.length; i++) {
					nameParts[i] = nameParts[i].charAt(0).toUpperCase() + nameParts[i].substring(1);
				}
				$('#score_field_name').val(nameParts.join(' '));
			} else $('#score_field_name').val('');

			if ('id' in scoreData[scoreIndex].details) {
				$('#score_field_id').val(scoreData[scoreIndex].details.id);
			} else $('#score_field_id').val('');

			if ('qr' in scoreData[scoreIndex].details) {
				$('#score_field_qr').val(scoreData[scoreIndex].details.qr);
			} else $('#score_field_qr').val('');
	
			var questionIndex = 0;
			for (var i = 0; i < documentScoring.Sections.length; ++i) {
				if (!('Content' in documentScoring.Sections[i])) continue;
				for (var j = 0; j < documentScoring.Sections[i].Content.length; ++j) {
					var questionData = documentScoring.Sections[i].Content[j];
					var $responseEntry = $('.response_entry[data-section-index="' + i + '"][data-content-index="' + j + '"]');
					if ($responseEntry.length == 1) {
						if (questionData.type == 'wr') {
	
							for (var k = 0; k < questionData.TypeDetails.criteria.length; ++k) {
								var $responseInput = $responseEntry.find('.response_input[data-criteria-index="' + k + '"]');
	
								var criterionValue = scoreData[scoreIndex].results[questionIndex].substring(2 * k, 2 * k + 2);
								var displayValue = '';
								if (criterionValue[0] == ' ') displayValue += '0';
								else displayValue += criterionValue[0];
								if (criterionValue[1] == ' ') displayValue += '.0';
								else displayValue += '.5';
								$responseInput.val(displayValue);
							}
	
						} else {
	
							var $responseInput = $responseEntry.find('.response_input');
							$responseInput.val(scoreData[scoreIndex].results[questionIndex].trim());
								
						}
					} else return false;
	
					questionIndex++;
				}
			}

		} else {

			$('#displayed_index').val(-1);
			$('.response_input').val('');

		}
	}

    $('#data_wrapper_inner').on('change keyup', '.response_input', function() {
        $(this).validationEngine('hide');
        $(this).closest('.response_entry').addClass('modified');
    });

	$('#deleteScore_button').click(function(e){
		e.preventDefault();
		if ($(this).attr('disabled')) return;
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked > 0) {
			if (numChecked == 1) {
				$('#deleteScore_text').html('Delete the selected score?').show();
			} else {
				$('#deleteScore_text').html('Delete the ' + numChecked + ' selected scores?').show();
			}

			$('#deleteScore_dialog').dialog('open');
		}
	});

	$('#editScore_dialog').dialog({
        autoOpen: false,
		width: 675,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Save': saveScore
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
		  	initSubmitProgress($(this));
	      	$('#email').focus();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});		

	$('#deleteScore_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() {
               	$(this).dialog("close");
			},
          	'Delete': deleteScore
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
		   	initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});		
	
	$('#newAnalysis_dialog').dialog({
        autoOpen: false,
		width: 600,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			initSubmitProgress($(this));
	      	$(document.activeElement).blur();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});		
	
	$('#newAnalysis_dialog').find('button:contains("Cancel")').click(function(){
		$('#newAnalysis_dialog').dialog("close");
	});
		 	
	$('#newAnalysis_dialog').find('button:contains("Build")').click(function(){
		var contextedSave = $.proxy(buildAnalysis, $('#newAnalysis_dialog'));
		contextedSave();
	});
		
	$('#scores_search').keyup(function() {
		if (storeResponses()) {
			var searchTerm = $('#scores_search').val().toLowerCase();
			if (searchTerm.length == 0) {
				$('#assessment_number').spinner('value', 1);
				$('.search_nav').addClass('disabled');
			} else {
				searchMatches = [];
				matchIndex = 0;
				for (var i = 0; i < scoreData.length; ++i) {
					if ('name' in scoreData[i].details) {
						if (scoreData[i].details.name.toLowerCase().search(searchTerm) != -1) searchMatches.push(i);
					}
					if ('id' in scoreData[i].details) {
						if (scoreData[i].details.id.toLowerCase().search(searchTerm) != -1) searchMatches.push(i);
					}
					if ('qr' in scoreData[i].details) {
						if (scoreData[i].details.qr.toLowerCase().search(searchTerm) != -1) searchMatches.push(i);
					}
				}
				if (searchMatches.length == 0) {
					$('#assessment_number').spinner('value', 1);
					$('.search_nav').addClass('disabled');
				} else {
					$('#assessment_number').spinner('value', searchMatches[matchIndex] + 1);
					$('.search_nav').addClass('disabled');
					if (searchMatches.length > 1) $('#search_next').removeClass('disabled');
				}
			}
			
			drawResponses();
		}
	});
	
	$('#search_prev').click(function() {
		if (matchIndex > 0) {
			if (storeResponses()) {
				matchIndex--;
				$('#assessment_number').spinner('value', searchMatches[matchIndex] + 1);	
				drawResponses();
			}
		}
		$('.search_nav').addClass('disabled');
		if (matchIndex > 0) $('#search_prev').removeClass('disabled');
		if (matchIndex < searchMatches.length - 1) $('#search_next').removeClass('disabled');
	});
		 	
	$('#search_next').click(function() {
		if (matchIndex < searchMatches.length - 1) {
			if (storeResponses()) {
				matchIndex++;
				$('#assessment_number').spinner('value', searchMatches[matchIndex] + 1);	
				drawResponses();
			}
		}
		$('.search_nav').addClass('disabled');
		if (matchIndex > 0) $('#search_prev').removeClass('disabled');
		if (matchIndex < searchMatches.length - 1) $('#search_next').removeClass('disabled');
	});
		 	
    $('.intSpinner').spinner({
	    step: 1,
	    min: 1
    });

    $('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : "bottomLeft"
    }).submit(function(e){ e.preventDefault() });

	$('#hide_scoring_howto').click(function(e) {
		ajaxFetch('/Users/hide_tip/scoring_howto', {
			method: 'POST'
		}).then(async response => {
			$('#scoring_howto_wrapper').hide();
		});
	});

	function sortRows(sortBy, order) {
		var rows = [];
		$('#scores tr').each(function() {
			rows.push({
				sortValue: $(this).find('td.score_' + sortBy).text(),
				element: $(this)
			});
		});

		var sortResult = (order == 'asc') ? 1 : -1;
		rows.sort(function(a, b) {
			return a.sortValue < b.sortValue ? -sortResult : a.sortValue > b.sortValue ? sortResult : 0;
		});

		for (var i = 0; i < rows.length; ++i) {
			$('#scores').append(rows[i].element);
		}
	}

	$('#sort_type').on('change', function() {
		var $selectedOption = $('#sort_type option:selected');
		sortRows($selectedOption.attr('value'), $selectedOption.attr('data-default-direction'));
	});

	$('.header_sort').on('click', function() {
		sortRows($('#sort_type').val(), $(this).attr('data-direction'));
	});
	
    updateIndex();
});

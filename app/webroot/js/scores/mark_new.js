/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { readForm, cancelReadForm } from '/js/FormEngine/ReadEngine.js?7.0';

$(document).ready(function() {

    var dragTarget = false;

    $("#progressbar").progressbar();

	$(document).bind('dragenter', function(e) {
    	e.preventDefault();

        dragTarget = e.target;
        $('#fileDropTarget').show();
        $("#fileDropTarget").fadeTo('fast', 0.7);

        return false;
    });

    $(document).bind('dragover', function(e) {
    	e.preventDefault();
    });

    $(document).bind('dragleave', function(e) {
        if (e.target == dragTarget) {
            dragTarget = false;
            $("#fileDropTarget").fadeTo('fast', 0.0, function() {
                $(this).hide();
            });
        }
	});

    $(document).bind('drop', function(e) {
    	e.preventDefault();
        $("#fileDropTarget").fadeTo('fast', 0.0, function() {
            $(this).hide();
        });

        var files = e.originalEvent.dataTransfer.files;
        if (files.length == 1) {
            var file = files[0];
            if (file.type === 'application/pdf') {

                $('#pdf_select')[0].files = files;
                $('#pdf_select').trigger('change');
                
            } else {

                $.gritter.add({
                    title: "Error",
                    text: "Selected file must be a PDF.",
                    image: "/img/error.svg"
                });
    
            }
        } else {

            $.gritter.add({
                title: "Error",
                text: "Scanned PDFs must be read one at a time.",
                image: "/img/error.svg"
            });

        }

        return false;
    });
	
	$('#select_button').click(function(e) {
		$('#filename').validationEngine('hideAll');
	});

    $('#pdf_select').on('change', function() {
        $("#read_button").removeAttr("disabled");
        $('#filename').html($('#pdf_select')[0].files[0].name);
    });
  
    $('#cancel_button').on('click', function() {
        $('#cancel_wait').show();
        $('#cancel_button').attr('disabled', 'disabled');
        cancelReadForm();
    });

    $('#read_button').click(function() {

        window.onbeforeunload = function() {
            return true;
        };

        $('#select_panel').hide();
        $('#waiting_panel').show();

        $("#progressbar").progressbar("value", 0);
        $('#progresstext').html(`Loading PDF (0% done)`);

        const reader = new FileReader();

        reader.addEventListener("progress", (e) => {
            if (e.lengthComputable) {
                const loadPercent = Math.round(e.loaded * 100.0 / e.total);

                const progressValue = 0;
                const progressText = `Loading PDF (${loadPercent}% done)`;

                $("#progressbar").progressbar("value", progressValue);
                $('#progresstext').html(progressText);
            }
        });

        reader.addEventListener("load", async (e) => {
            
            const arrayBuffer = e.target.result;

            const spark = new SparkMD5.ArrayBuffer();
            spark.append(arrayBuffer);
            const md5Hash = spark.end();
                
            // Read and process PDF
        
            $("#progressbar").progressbar("value", 0);
            $('#progresstext').html(`Reading PDF (0% done)`);
    
            var jobStart = dayjs().utc().format('YYYY-MM-DD HH:mm:ss');
            const data = await readForm(arrayBuffer, formats, function(progressData) {

                var progressValue, progressText;

                if (progressData.stage == 'reading') {

                    const readPercent = Math.round(progressData.loaded * 100.0 / progressData.total);

                    progressValue = 0;
                    progressText = `Reading PDF (${readPercent}% done)`;

                } else if (progressData.stage == 'processing') {

                    var counter = progressData.pagesDone;
                    var scanPageCount = progressData.scanPageCount;
                    var warningsCount = progressData.warnings;
                    var errorsCount = progressData.errors;
                    var problemCount = warningsCount + errorsCount;
                
                    if (scanPageCount === false) {
        
                        progressValue = false;
                        progressText = `${counter} pages processed.`;
        
                    } else if (counter == scanPageCount) {
        
                        progressValue = false;
                        progressText = 'Processing results&hellip;';
        
                    } else {
        
                        progressValue = Math.round(counter * 100.0 / scanPageCount);
                        progressText = `${counter}/${scanPageCount} pages processed.`;
        
                    }
        
                    if (problemCount == 0) {
                    } else if (problemCount == 1) {
                        progressText += ' <b>' + problemCount + ' error or warning.</b>';
                    } else if ((problemCount < 5) || (problemCount < counter)) {
                        progressText += ' <b>' + problemCount + ' errors or warnings.</b>';
                    } else if (errorsCount == counter) {
                        progressText += ' <b>Errors on all pages. Wrong PDF selected?</b>';
                    } else if (warningsCount == counter) {
                        progressText += ' <b>Warnings on all pages. Wrong PDF selected?</b>';
                    } else {
                        progressText += ' <b>Problems on all pages. Wrong PDF selected?</b>';
                    }

                }

                $("#progressbar").progressbar("value", progressValue);
                $('#progresstext').html(progressText);

            });
            var jobEnd = dayjs().utc().format('YYYY-MM-DD HH:mm:ss');

            $('#cancel_button').off('click');

            if (data.status === 'canceled') {

                addClientFlash({
                    title: "Success",
                    text: "Score cancelled",
                    image: "/img/success.svg"
                });

                window.onbeforeunload = null;
                window.location.reload();
            
            } else {

                var scoreCommon = {
                    user_id: userID,
                    title: $('#details').val(),
                    job_created: jobStart,
                    job_start: jobStart,
                    job_end: jobEnd,
                    pdf_hash: md5Hash,
                    pageCount: data.pageCount
                };
    
                ajaxFetch('/Scores/save_results', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        results: JSON.stringify(data),
                        score_common: JSON.stringify(scoreCommon)
                    })
                }).then(async response => {
                    pendingAJAXPost = false;
                    const responseData = await response.json();
        
                    if (responseData.success && ('forward_url' in responseData)) {
    
                        window.onbeforeunload = null;
                        window.location.replace(responseData.forward_url);
    
                    } else {
    
                        $.gritter.add({
                            title: "Error",
                            text: "Unable to save results. Please try again.",
                            image: "/img/error.svg"
                        });
    
                    }
    
                }).catch(function() {
    
                    $.gritter.add({
                        title: "Error",
                        text: "Unable to save results. Please try again.",
                        image: "/img/error.svg"
                    });
    
                });

            }
        });

        var selectedFile = $('#pdf_select')[0].files[0];
        reader.readAsArrayBuffer(selectedFile);

    });
});
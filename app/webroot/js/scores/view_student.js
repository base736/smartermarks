/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as ScoresViewCommon from '/js/scores/view_common.js?7.0';
import { localizationInstance } from '/js/FormEngine/Localization.js?7.0';

var reportData = false;

var layout = false;
var items = false;
var scoring = false;
var outcomes = false;
var responses = false;
var maxDecimals = false;
var isWeighted = false;
var outOf = false;

function escapeHTML(text) {
    return $('<div>').text(text).html();
}

function getQuestionList(itemIDs, itemData) {
    var questionList = '';

    var startIndex = -1;
    for (var index = 0; index <= itemData.length; ++index) {
        var inItemIDs = false;
        if (index < itemData.length) {
            if (itemIDs.indexOf(itemData[index].id) >= 0) inItemIDs = true;
        }

        if (inItemIDs) {

            if (startIndex == -1) startIndex = index;

        } else if (startIndex >= 0) {

            if (questionList.length > 0) questionList += ', ';
            if (index == startIndex + 1) {
                questionList += itemData[startIndex].number;
            } else if (index == startIndex + 2) {
                questionList += itemData[startIndex].number + ', ';
                questionList += itemData[startIndex + 1].number;
            } else {
                questionList += itemData[startIndex].number + '&ndash;';
                questionList += itemData[index - 1].number;
            }

            startIndex = -1;
            
        }					
    }    

    return questionList;
}

$(document).ready(function() {

    var initialize = async function() {

        reportData = await ScoresViewCommon.getReportData(cacheHash, getDataToken);

        layout = reportData.layout;
        items = reportData.items;
        scoring = reportData.scoring;
        outcomes = reportData.outcomes;
        responses = reportData.responses;
        maxDecimals = reportData.maxDecimals;
        isWeighted = reportData.isWeighted;
    
        if (!isWeighted && (responses.length > 0)) {
            outOf = reportData.outOf;
        }
    
        localizationInstance.setLanguage(layout.Settings.language);
    
        /**************************************************************************************************/
        /* Draw reports title                                                                             */
        /**************************************************************************************************/
    
        var titleHTML = '<h2 style="margin-top:0px;">Student reports</h2>';
        for (const textLine of layout.Title.text) titleHTML += '<p>' + textLine + '</p>';
        titleHTML += '<p>Processed ' + dayjs().format('MMMM D, YYYY') + '</p>';
    
        $('#reports_title').html(titleHTML);
    
        /**************************************************************************************************/
        /* Find out what we should be showing                                                             */
        /**************************************************************************************************/
    
        var includeWeights = (layout.StudentReports.weights == 'show');
        var includeOutcomes = (layout.StudentReports.outcomes != 'none');
        var includeOutcomeNumbers = (layout.StudentReports.outcomes == 'numbers');
    
        var includeResponses = (layout.StudentReports.responses != 'none');
        var includeAnswerKey = (layout.StudentReports.responses == 'key');
        var incorrectOnly;
        if ('incorrectOnly' in layout.StudentReports) {
            incorrectOnly = layout.StudentReports.incorrectOnly;
        } else incorrectOnly = false;
    
        var includeScores = (layout.StudentReports.results != 'proficiency');
        var includeProficiencies = (layout.StudentReports.results != 'numerical');
    
        var numPerPage = -1;
        if (layout.StudentReports.reportsPerPage == 'singles') numPerPage = 1;
        else if (layout.StudentReports.reportsPerPage == '2perpage') numPerPage = 2;
        else if (layout.StudentReports.reportsPerPage == '3perpage') numPerPage = 3;
        else if (layout.StudentReports.reportsPerPage == '4perpage') numPerPage = 4;
        else if (layout.StudentReports.reportsPerPage == '5perpage') numPerPage = 5;
    
        var outcomeSort = reportData.defaults.outcomeSort;
        var reportLayout = reportData.defaults.reportLayout;
    
        var oneLineTitle = "";
        for (var i = 0; i < layout.Title.text.length; ++i) {
            if (i > 0) oneLineTitle += " / ";
            oneLineTitle += escapeHTML(layout.Title.text[i]);
        }
    
        // Build student reports
    
        var allReports = [];
        for (var studentIndex = 0; studentIndex < responses.length; ++studentIndex) {
    
            var $studentReport = $('<div class="student_report"></div>');
            allReports.push($studentReport);
    
            var thisResults = responses[studentIndex];
            
            /**************************************************************************************************/
            /* Draw header                                                                                    */
            /**************************************************************************************************/
    
            var scoreHTML = '';
            
            var totalScore = 0.0;
            for (var j = 0; j < thisResults.items.length; ++j) {
                if ((items[j].scoring != 'omit') && (thisResults.items[j].score !== false)) {
                    totalScore += thisResults.items[j].score * thisResults.items[j].weight;
                }
            }
    
            if (includeScores) {
                if (scoreHTML == '') {
                    var report_total = await localizationInstance.getString('report_total');
                    scoreHTML += "<b>" + report_total + "</b>: ";
                }
    
                if (isWeighted) {
    
                    scoreHTML += (totalScore * 100).toFixed(1) + "%";
        
                } else {
        
                    scoreHTML += totalScore.toFixed(maxDecimals) + "/" + outOf.toFixed(maxDecimals) +
                        " (" + (100.0  * totalScore / outOf).toFixed(1) + "%)";
                    
                }    
            }
    
            if (includeProficiencies) {
                if (scoreHTML == '') {
                    var report_total = await localizationInstance.getString('report_total');
                    scoreHTML += "<b>" + report_total + "</b>: ";
                }
    
                var objectiveLevels = layout.StudentReports.outcomesLevels;
    
                var percentScore;
                if (isWeighted) percentScore = totalScore * 100;
                else percentScore = 100.0  * totalScore / outOf;
    
                var categoryIndex;
                for (categoryIndex = 1; categoryIndex < objectiveLevels.length; ++categoryIndex)
                    if (percentScore < objectiveLevels[categoryIndex].from) break;
                var levelString = objectiveLevels[--categoryIndex].name;
    
                if (includeScores) scoreHTML += ', ';
                scoreHTML += levelString;
    
            }
            
            var studentQR = (('student' in thisResults) && ('qr' in thisResults.student)) ? thisResults.student.qr : '';
            var studentID = (('student' in thisResults) && ('id' in thisResults.student)) ? thisResults.student.id : '';
            var studentName = (('student' in thisResults) && ('name' in thisResults.student)) ? thisResults.student.name : '';
    
            var root;
            if ('pages' in thisResults) {
                const numPages = thisResults.pages.length;
                if (numPages == 1) root = "p.&hairsp;" + thisResults.pages[0];
                else root = "pp.&hairsp;" + thisResults.pages[0] + "&ndash;" + thisResults.pages[numPages - 1];
            } else root = "#" + thisResults.index;
    
            var headerLine;
            if ((studentQR.length > 0) && !(/^\d{15}$/.test(studentQR))) {
                headerLine = oneLineTitle + " (" + studentQR + ", " + root + ")";
            } else if ((studentID.length > 0) && (studentName.length > 0)) {
                headerLine = oneLineTitle + " (" + studentName + ", " + studentID + ", " + root + ")";
            } else if (studentID.length > 0) {
                headerLine = oneLineTitle + " (" + studentID + ", " + root + ")";
            } else if (studentName.length > 0) {
                headerLine = oneLineTitle + " (" + studentName + ", " + root + ")";
            } else {
                headerLine = oneLineTitle + " (" + root + ")";
            }
    
            $studentReport.append(
                '<div class="student_report_title">' +
                    '<div style="flex:0 0 auto;">' + scoreHTML + '</div>' + 
                    '<div style="flex:0 1 auto;">' + headerLine + '</div>' +
                '</div>'
            );
    
            /**************************************************************************************************/
            /* Draw weights                                                                                   */
            /**************************************************************************************************/
    
            if (((scoring.type == 'Sections') || (scoring.type == 'Custom')) && includeWeights) {
    
                var weightsData = [];
                var totalWeight = 0;
    
                for (var i = 0; i < scoring.weights.length; ++i) {
                    var thisGroup = scoring.weights[i];
    
                    var name = false;
                    var itemIDs = [];
                    var weight = thisGroup.weight;
    
                    if (scoring.type == 'Sections') {
    
                        var sectionID = thisGroup.section_id;
    
                        for (var sectionIndex = 0; sectionIndex < layout.Body.length; ++sectionIndex) {
                            if (layout.Body[sectionIndex].id == sectionID) name = layout.Body[sectionIndex].header;
                        }
    
                        for (var itemIndex = 0; itemIndex < items.length; ++itemIndex) {
                            if (items[itemIndex].section_id == sectionID) itemIDs.push(items[itemIndex].id);
                        }
        
                    } else {
    
                        name = thisGroup.name;
                        itemIDs = thisGroup.item_ids;
    
                    }
    
                    weightsData.push({
                        name: name,
                        questionList: getQuestionList(itemIDs, items),
                        weight: weight
                    });
    
                    totalWeight += weight;
                }
    
                var weightsTableRows = '';
    
                for (var i = 0; i < weightsData.length; ++i) {
                    var gridColumn = 1;
    
                    var label = weightsData[i].name;
                    if (includeOutcomeNumbers && (weightsData[i].questionList.length > 0)) {
                        label += ': #' + weightsData[i].questionList;
                    }
    
                    weightsTableRows += '<div style="padding-left:1.5em; text-indent:-1.5em; grid-column:' + gridColumn + '/' + (gridColumn + 1) + '">' + 
                        label + '</div>';
                    ++gridColumn;
    
                    var weightPercent = weightsData[i].weight * 100.0 / totalWeight;
                    var percentString = weightPercent.toFixed(0) + '%';
    
                    weightsTableRows += '<div style="grid-column:' + gridColumn + '/' + (gridColumn + 1) + '; justify-self:end;">' + 
                        percentString + '</div>';
                    ++gridColumn;    
                }
    
                var report_weighting = await localizationInstance.getString('report_weighting');
                
                var newHTML = 
                '<div class="student_report_wrapper">' + 
                    '<div class="student_report_header">' + report_weighting + '</div>' + 
                    '<div class="student_report_body student_outcomes_table">' + weightsTableRows + '</div>' + 
                '</div>';
    
                $studentReport.append(newHTML);
            }
    
            /**************************************************************************************************/
            /* Draw outcomes                                                                                  */
            /**************************************************************************************************/
    
            if (includeOutcomes && (outcomes.length > 0)) {
                    
                // Get outcome scores
    
                var categoryScores = [];
            
                for (var i = 0; i < outcomes.length; ++i) {
            
                    // Get numbers for this outcome
    
                    var questionList = getQuestionList(outcomes[i].item_ids, items);
    
                    // Get scores
    
                    var score = 0.0;
                    var maxScore = 0.0;
            
                    for (var j = 0; j < items.length; ++j) {
                        if (outcomes[i].item_ids.includes(items[j].id)) {
                            if ((items[j].scoring !== 'omit') && (thisResults.items[j].score !== false)) {
                                score += thisResults.items[j].score;
                            }        
                            if (items[j].scoring == 'regular') {
                                maxScore += items[j].maxScore;
                            }
                        }
                    }
    
                    // Add outcome entry
            
                    categoryScores.push({
                        id: outcomes[i].id,
                        name: outcomes[i].name,
                        numbers: questionList,
                        score: score,
                        maxScore: maxScore,
                        percent: 100.0 * score / maxScore
                    });
                }
    
                if (outcomeSort == 'score') {
                    categoryScores.sort((a,b) => (a.percent - b.percent));
                }
    
                var outcomesTableRows = '';
    
                for (var i = 0; i < categoryScores.length; ++i) {
                    var gridColumn = 1;
    
                    var outcomeLabel = escapeHTML(categoryScores[i].name);
                    if (includeOutcomeNumbers) outcomeLabel += ': #' + categoryScores[i].numbers;
                    outcomesTableRows += '<div style="padding-left:1.5em; text-indent:-1.5em; grid-column:' + gridColumn + '/' + (gridColumn + 1) + '">' + 
                        outcomeLabel + '</div>';
                    ++gridColumn;
    
                    if (includeScores) {
    
                        var percentString;
                        if (categoryScores[i].maxScore > 0) percentString = categoryScores[i].percent.toFixed(0) + '%';
                        else percentString = '&mdash;';
                        
                        outcomesTableRows += '<div style="grid-column:' + gridColumn + '/' + (gridColumn + 1) + '; justify-self:end;">' + 
                            percentString + '</div>';
                        ++gridColumn;
    
                        if (scoring.type == 'Total') {
                            var scoreString = categoryScores[i].score.toFixed(maxDecimals) + ' / ' + categoryScores[i].maxScore.toFixed(maxDecimals);
                            var form_decimal = await localizationInstance.getString('form_decimal');
                            scoreString = scoreString.replace('.', form_decimal);
                            outcomesTableRows += '<div style="grid-column:' + gridColumn + '/' + (gridColumn + 1) + '; justify-self:end;">' + 
                                scoreString + '</div>';
                            ++gridColumn;
                        }
    
                    }
                    
                    if (includeProficiencies) {
    
                        var objectiveLevels = layout.StudentReports.outcomesLevels;
    
                        var levelString;
                        if (categoryScores[i].maxScore > 0) {
                            var categoryIndex;
                            for (categoryIndex = 1; categoryIndex < objectiveLevels.length; ++categoryIndex)
                                if (categoryScores[i].percent < objectiveLevels[categoryIndex].from) break;
                            levelString = objectiveLevels[--categoryIndex].name;    
                        } else levelString = '&mdash;';
    
                        outcomesTableRows += '<div style="grid-column:' + gridColumn + '/' + (gridColumn + 1) + '; justify-self:end;">' + 
                            levelString + '</div>';
                        ++gridColumn;
    
                    }
    
                }
    
                var report_outcomes = await localizationInstance.getString('report_outcomes');
    
                var newHTML = 
                    '<div class="student_report_wrapper">' + 
                        '<div class="student_report_header">' + report_outcomes + '</div>' + 
                        '<div class="student_report_body student_outcomes_table">' + outcomesTableRows + '</div>' + 
                    '</div>';
    
                $studentReport.append(newHTML);
                
            }
    
            /**************************************************************************************************/
            /* Draw responses                                                                                 */
            /**************************************************************************************************/
    
            if (includeResponses) {
    
                var titleString;
                if (incorrectOnly) {
                    titleString = await localizationInstance.getString('report_responses_2');
                } else {
                    titleString = await localizationInstance.getString('report_responses_1');
                }
    
                // Gather data for the results table
        
                var tableData = [];
                var tableRows = 0;
                var needsSections = false;
    
                for (var itemIndex = 0; itemIndex < items.length; ++itemIndex) {
                    var thisRows = 1;
                    var rowText = '';
                    var gridColumnStart = 1;
    
                    var questionNumber = items[itemIndex].number;
    
                    var decimalPos = questionNumber.indexOf('.');
                    if (decimalPos >= 0) {
                        questionNumber = questionNumber.substr(decimalPos + 1);
                        needsSections = true;
                    }
    
                    var score = thisResults.items[itemIndex].score;
                    if (score === false) {

                        var classes = 'student_responses_number';
                        rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/span 2;">' + questionNumber + '</div>';
                        gridColumnStart += 2;

                        classes = 'student_responses_score';
                        rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/-1;"><i>Error reading response</i></div>';  

                        tableData.push({
                            sectionID: items[itemIndex].section_id,
                            isCorrect: true,
                            rowCount: thisRows,
                            text: rowText
                        });
    
                        tableRows += thisRows;

                    } else {

                        var maxScore = items[itemIndex].maxScore;
                        var needsShading = (score < maxScore) && !incorrectOnly && (items[itemIndex].scoring != 'omit');

                        var classes = 'student_responses_number' + (needsShading ? ' incorrect' : '');
                        rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/span 2;">' + questionNumber + '</div>';
                        gridColumnStart += 2;
        
                        if (items[itemIndex].scoring == 'omit') {
        
                            score = 0.0;
                            
                            classes = 'student_responses_omitted' + (needsShading ? ' incorrect' : '');
                            rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/-1;"><i>Omitted</i></div>';  
        
                        } else {
        
                            classes = 'student_responses_score' + (needsShading ? ' incorrect' : '');
                            if (includeScores) {
                                rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/span 2;">' + score.toFixed(maxDecimals) + '</div>';
                                gridColumnStart += 2;
                            }
            
                            var response = thisResults.items[itemIndex].response;
                            if (items[itemIndex].type == 'wr') {
            
                                if (response.length == 2 * items[itemIndex].TypeDetails.criteria.length) {
            
                                    classes = 'student_responses_wr_response' + (needsShading ? ' incorrect' : '');
        
                                    var wrText = '<div class="' + classes + '" style="display:grid; grid-template-columns:auto 1fr; grid-row-gap:3px; ' + 
                                        'grid-column-gap:1em; grid-column:' + gridColumnStart + '/-1;">';
            
                                    for (var i = 0; i < items[itemIndex].TypeDetails.criteria.length; ++i) {
                                        var wrScore = (response[2 * i] == ' ') ? 0 : parseInt(response[2 * i]);
                                        if (response[2 * i + 1] == '+') wrScore += 0.5;
            
                                        wrText += '<div>' + wrScore.toFixed(1) + ' / ' + items[itemIndex].TypeDetails.criteria[i].value.toFixed(0) + '</div>';
                                        wrText += '<div>' + items[itemIndex].TypeDetails.criteria[i].label + '</div>';
                                    }
            
                                    wrText += '</div>';
            
                                    thisRows = items[itemIndex].TypeDetails.criteria.length;
                                    rowText += wrText;
            
                                } else {
            
                                    classes = 'student_responses_wr_error' + (needsShading ? ' incorrect' : '');
                                    rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/-1;">&mdash;</div>';
            
                                }
            
                            } else if (includeAnswerKey) {
        
                                var correctAnswers = [];
                                for (var i = 0; i < items[itemIndex].ScoredResponses.length; ++i) {
                                    var answerIDs = [];
                                    if ('id' in items[itemIndex].ScoredResponses[i]) {
                                        answerIDs = [items[itemIndex].ScoredResponses[i].id];
                                    } else if ('ids' in items[itemIndex].ScoredResponses[i]) {
                                        answerIDs = items[itemIndex].ScoredResponses[i].ids;
                                    }

                                    var thisAnswer = '';
                                    for (var k = 0; k < items[itemIndex].Answers.length; ++k) {
                                        if (answerIDs.indexOf(items[itemIndex].Answers[k].id) >= 0) {
                                            var rawResponse = items[itemIndex].Answers[k].response;
                                            if (items[itemIndex].type == 'nr_scientific') {
                                                thisAnswer += rawResponse.replace(/\D/g, '');
                                            } else thisAnswer += rawResponse;
                                        }
                                    }
        
                                    var thisValue = items[itemIndex].ScoredResponses[i].value;
                                    if ((items[itemIndex].type == 'nr_selection') && items[itemIndex].TypeDetails.markByDigits) {
                                        thisValue *= thisAnswer.length;
                                    }
        
                                    if (thisValue == maxScore) correctAnswers.push(thisAnswer);
                                }
            
                                classes = 'student_responses_response' + (needsShading ? ' incorrect' : '');
                                rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/span 2;">' + response + '</div>';  
                                gridColumnStart += 2;
        
                                classes = 'student_responses_answer' + (needsShading ? ' incorrect' : '');
                                rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/-1;">' + correctAnswers.join(', ') + '</div>';
            
                            } else {
        
                                classes = 'student_responses_response' + (needsShading ? ' incorrect' : '');
                                rowText += '<div class="' + classes + '" style="grid-column:' + gridColumnStart + '/-1;">' + response + '</div>';  
        
                            }
        
                        }
                        
                        if ((score < maxScore) || !incorrectOnly) {
                            tableData.push({
                                sectionID: items[itemIndex].section_id,
                                isCorrect: (score == maxScore),
                                rowCount: thisRows,
                                text: rowText
                            });
        
                            tableRows += thisRows;
                        }

                    }
                }
        
                // Add section headings if required
        
                if (needsSections) {
        
                    var sectionID = false;
                    for (var rowIndex = 0; rowIndex < tableData.length; ++rowIndex) {
                        if (sectionID != tableData[rowIndex].sectionID) {
                            sectionID = tableData[rowIndex].sectionID
                            for (var sectionIndex = 0; sectionIndex < layout.Body.length; ++sectionIndex) {
                                if (layout.Body[sectionIndex].id == sectionID) {
                                    var rowText = '<div style="grid-column:1 / -1;"><i>' + 
                                        layout.Body[sectionIndex].header + '</i></div>';
        
                                    tableData.splice(rowIndex, 0, {
                                        sectionID: sectionID,
                                        rowCount: 1,
                                        text: rowText
                                    });
                                    ++tableRows;
        
                                    ++rowIndex;
                                }
                            }
                        }
                    }
            
                }
        
                // Display the results table
        
                var responseColumnTemplate = 'auto'
                if (includeScores) responseColumnTemplate += ' 1em auto';
                if (includeAnswerKey) responseColumnTemplate += ' 0.5em auto';
                responseColumnTemplate += ' 0.5em 1fr';
        
                var numColumns = 2;
                var rowsPerColumn = tableRows / numColumns;
        
                var lastColumn = -1;
                var thisRow = 0;
                var responseTableRows = '';
                for (var i = 0; i < tableData.length; ++i) {
                    var thisColumn = Math.floor(thisRow / rowsPerColumn);
                    if (thisColumn != lastColumn) {
                        lastColumn = thisColumn;
                        
                        if (i > 0) responseTableRows += '</div>';
                        responseTableRows += '<div class="student_report_body student_results_table" style="grid-template-columns:' + responseColumnTemplate + ';">';
        
                        responseTableRows += '<div class="header">#</div>';
                        if (includeScores) {
                            responseTableRows += '<div class="header"></div>';
                            responseTableRows += '<div class="header">Score</div>';    
                        }
                        responseTableRows += '<div class="header"></div>';
                        responseTableRows += '<div class="header">Response</div>';
                        if (includeAnswerKey) {
                            responseTableRows += '<div class="header"></div>';
                            responseTableRows += '<div class="header">Correct Answer</div>';
                        }
                    }
        
                    responseTableRows += tableData[i].text;
                    thisRow += tableData[i].rowCount;
                }
        
                responseTableRows += '</div>';
        
                var newHTML = 
                    '<div class="student_report_wrapper">' + 
                        '<div class="student_report_header">' + titleString + '</div>' + 
                        '<div style="display:flex; align-items:flex-start; gap:20px; width:100%;">' + responseTableRows + '</div>' +
                    '</div>';
        
                $studentReport.append(newHTML);
    
            }
    
        }
    
        for (var i = 0; i < allReports.length; ++i) {
            $('#student_reports').append(allReports[i]);
        }
    
        /**************************************************************************************************/
        /* PDF render stuff                                                                               */
        /**************************************************************************************************/
    
        if ($('#student_reports').is('.printed_report')) {
            var pageHeight_inches;
            if (pageSize == 'letter') pageHeight_inches = 11;
            else if (pageSize == 'legal') pageHeight_inches = 14;
            else if (pageSize == 'a4') pageHeight_inches = 11.7;
    
            if (numPerPage == -1) {
                $('#student_reports .student_report').each(function() {
                    var height_inches = this.offsetHeight / 96 + marginSizes.top + marginSizes.bottom;
                    var thisCount = Math.floor(pageHeight_inches / height_inches);
                    if (thisCount < 1) thisCount = 1;
    
                    if ((numPerPage == -1) || (thisCount < numPerPage)) numPerPage = thisCount;
                });
            }
    
            if (numPerPage > 1) {
                var height_inches = pageHeight_inches / numPerPage - marginSizes.top - marginSizes.bottom;
                height_inches = Math.floor(height_inches * 1000) / 1000;
        
                if (reportLayout == 'guillotine') {
                    var numReports = allReports.length;
        
                    var pagePosition = 0;
                    var pageNum = 0;
                    var selectionStep = Math.ceil(numReports / numPerPage);
        
                    var $lastReport = false;
                    for (var i = 0; i < numReports; ++i) {
                        var arrayIndex = pageNum + pagePosition * selectionStep;
    
                        var $nextReport;
                        if (arrayIndex < numReports) $nextReport = allReports[arrayIndex];
                        else $nextReport = $('<div class="student_report"></div>');
        
                        if ($lastReport == false) $('#student_reports').prepend($nextReport);
                        else $nextReport.insertAfter($lastReport);
                        
                        $lastReport = $nextReport;
        
                        if (++pagePosition == numPerPage) {
                            pagePosition = 0;
                            ++pageNum;
                        }
                    }
                }
        
                $('#student_reports .student_report').css('height', height_inches.toFixed(3) + 'in');
                $('#student_reports .student_report').css('padding-top', marginSizes.top + 'in');
                $('#student_reports .student_report').css('padding-bottom', marginSizes.bottom + 'in');
                $('#student_reports .student_report').css('padding-left', marginSizes.left + 'in');
                $('#student_reports .student_report').css('padding-right', marginSizes.right + 'in');
    
            } else {
    
                $('head').append(
                    '<style>@page{' + 
                        'margin-top:' + marginSizes.top + 'in;' +
                        'margin-bottom:' + marginSizes.bottom + 'in;' +
                        'margin-left:' + marginSizes.left + 'in;' +
                        'margin-right:' + marginSizes.right + 'in;' +
                    '}</style>');
    
                $('#student_reports .student_report').css('page-break-after', 'always');
    
            }
        }
    }

    if (!isPrint) {

/**************************************************************************************************/
/* PDF render stuff                                                                               */
/**************************************************************************************************/

        $('#print_button').on('click', function() {
            $('#print_button').attr('disabled', 'disabled');
            var urlParts = window.location.href.split('/');
            var scoreID = urlParts[urlParts.length - 1];

            navigateWithPost('/build_pdf', {
                pdfType: 'pdf_from_url',
                url: '/Scores/print_student/' + scoreID,
                target: 'student.pdf'
            });

            $('#print_button').removeAttr('disabled');
        });

    }

	// Load report data

	initialize()
    .then(() => {

        $('body').append('<span class="page_loaded"></span>');

    }).catch((error) => {

        if (isPrint) {

            $('body').html(
                '<div>Error: ' + error.message + '</div> \
                <span class="page_loaded"></span>');
    
        } else {

            addClientFlash({
                title: "Error",
                text: error.message,
                image: "/img/error.svg"
            });
            window.location.href = '/Documents';
    
        }

	});

});

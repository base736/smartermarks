/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as StatsCommon from '/js/common/stats.js?7.0';
import * as ScoringCommon from '/js/common/scoring.js?7.0';
import * as ScoresViewCommon from '/js/scores/view_common.js?7.0';

var reportData = false;

var layout = false
var items = false
var outcomes = false;
var responses = false;
var maxDecimals = false;

var getStatistics = function(isIncluded) {
    var stats = {};

    if (isIncluded.length != items.length) return false;

    var numIncluded = 0;
    for (var i = 0; i < items.length; ++i) {
        if (isIncluded[i] && (items[i].scoring != 'omit')) {
            numIncluded++;
        }
    }

    var studentScores = [];

    for (var i = 0; i < responses.length; ++i) {
        var score = 0.0;
        var maxScore = 0.0;
        for (var j = 0; j < responses[i].items.length; ++j) {
            if (!isIncluded[j]) continue;

            if (items[j].scoring != 'omit') {
                score += responses[i].items[j].score * responses[i].items[j].weight;
            }
            if (items[j].scoring == 'regular') {
                maxScore += items[j].maxScore * responses[i].items[j].weight;
            }
        }
        studentScores.push(score / maxScore);
    }

    studentScores.sort();

    var sum = 0.0;
    var sumSquared = 0.0;
    for (var i = 0; i < studentScores.length; ++i)
    {
        sum += studentScores[i];
        sumSquared += studentScores[i] * studentScores[i];
    }

    var mean = sum / studentScores.length;
    stats['mean'] = mean;

    var median = studentScores[Math.floor(studentScores.length / 2)];
    stats['median'] = median;

    if (studentScores.length > 1) {
        var variance = (sumSquared - 2.0 * sum * mean + studentScores.length * mean * mean) / (studentScores.length - 1);
        stats['variance'] = variance;    
    }

    if ((responses.length > 1) && (numIncluded > 1)) {

        var meanVariance = 0.0;
        var meanCovariance = 0.0;
        for (var i = 0; i < items.length; ++i) {
            if (!isIncluded[i] || (items[i].scoring == 'omit')) continue;
    
            for (var j = 0; j <= i; ++j) {
                if (!isIncluded[j] || (items[j].scoring == 'omit')) continue;
    
                var numCommon = 0;
                var iAverage = 0.0;
                var jAverage = 0.0;
                for (var k = 0; k < responses.length; ++k) {
                    iAverage += responses[k].items[i].score;
                    jAverage += responses[k].items[j].score;
                    numCommon++;
                }
                iAverage /= numCommon;
                jAverage /= numCommon;
                
                var covariance = 0.0;
                for (var k = 0; k < responses.length; ++k) {
                    var iScore = responses[k].items[i].score;
                    var jScore = responses[k].items[j].score;
                    covariance += (iScore - iAverage) * (jScore - jAverage);
                }
                covariance /= numCommon;
                
                if (i == j) meanVariance += covariance;
                else meanCovariance += covariance;
            }
        }
    
        meanVariance /= numIncluded;
        meanCovariance /= numIncluded * (numIncluded - 1) / 2;
    
        stats['alpha'] = numIncluded * meanCovariance / (meanVariance + (numIncluded - 1) * meanCovariance);        
    }

    return stats;
}

$(document).ready(function() {

    var initialize = async function() {

        reportData = await ScoresViewCommon.getReportData(cacheHash, getDataToken);

        layout = reportData.layout;
        items = reportData.items
        outcomes = reportData.outcomes;
        responses = reportData.responses;
        maxDecimals = reportData.maxDecimals;

    /**************************************************************************************************/
    /* Draw report title                                                                              */
    /**************************************************************************************************/

        var titleHTML = '<h2 style="margin-top:0px;">Assessment analysis</h2>';
        for (const textLine of layout.Title.text) titleHTML += '<p>' + textLine + '</p>';
        titleHTML += '<p>Processed ' + dayjs().format('MMMM D, YYYY') + '</p>';

        $('#report_title').html(titleHTML);

    /**************************************************************************************************/
    /* Find out what we should be showing                                                             */
    /**************************************************************************************************/

        var showGraph = reportData.defaults.graph;
        var showMinorGraphs = reportData.defaults.minorgraphs;
        var showMeanSD = reportData.defaults.meansd;
        var showMedian = reportData.defaults.median;
        var showCronbach = reportData.defaults.cronbach;
        var showCollusion = reportData.defaults.collusion;    
        var showCategories = reportData.defaults.categories;

        var showAssessmentProfile = showGraph || showMeanSD || showMinorGraphs || 
            showMedian || showCronbach || showCollusion || showCategories;
        if (!showAssessmentProfile) $('#assessment_profile_wrapper').hide();

        var showStatistics = showMeanSD || showMedian || showCronbach;
        if (!showStatistics) $('#statistics_wrapper').hide();
        
        var showAverage = reportData.defaults.average;
        var showPearson = reportData.defaults.pearson;
        var showResponses = reportData.defaults.responses;

        var showResponseAnalysis = showAverage || showPearson || showResponses;
        if (!showResponseAnalysis) $('#item_analysis_wrapper').hide();

    /**************************************************************************************************/
    /* Get student scores                                                                             */
    /**************************************************************************************************/

        var epsilon = 1.0E-5;

        var studentScores = [];
        for (var i = 0; i < responses.length; ++i) {
            var score = 0.0;
            var maxScore = 0.0;
            for (var j = 0; j < responses[i].items.length; ++j) {
                if ((items[j].scoring != 'omit') && (responses[i].items[j].score !== false)) {
                    score += responses[i].items[j].score * responses[i].items[j].weight;
                }
                if (items[j].scoring == 'regular') {
                    maxScore += items[j].maxScore * responses[i].items[j].weight;
                }
            }
            studentScores.push(score / maxScore);
        }

    /**************************************************************************************************/
    /* Get item statistics                                                                            */
    /**************************************************************************************************/

        var difficulties = [];
        var discriminations = [];

        for (var i = 0; i < items.length; ++i) {
            var sumItem = 0.0;
            var sumTotal = 0.0;
            var itemCount = 0;
            for (var j = 0; j < responses.length; ++j) {
                var itemScore = responses[j].items[i].score;
                if (itemScore !== false) {
                    var itemMax = items[i].maxScore;
                    sumItem += itemScore / itemMax;
                    itemCount++;
                }
                sumTotal += studentScores[j];    
            }

            var averageThis;
            if (itemCount > 0) averageThis = sumItem / itemCount;
            else averageThis = false;

            difficulties.push(averageThis);

            var averageTotal = sumTotal / responses.length;

            var numerator = 0.0;
            var sigmaThis = 0.0;
            var sigmaTotal = 0.0;
            for (var j = 0; j < responses.length; ++j) {
                var itemScore = responses[j].items[i].score;
                if (itemScore !== false) {
                    var itemMax = items[i].maxScore;

                    var thisValue = itemScore / itemMax - averageThis;
                    var totalValue = studentScores[j] - averageTotal;
                    numerator += thisValue * totalValue;
                    sigmaThis += Math.pow(thisValue, 2.0);
                    sigmaTotal += Math.pow(totalValue, 2.0);
                }
            }

            var discrimination;
            if (itemCount > 0) {
                numerator /= itemCount;
                sigmaThis = Math.sqrt(sigmaThis / itemCount);
                sigmaTotal = Math.sqrt(sigmaTotal / itemCount);
            
                if ((sigmaThis < epsilon) || (sigmaTotal < epsilon)) discrimination = 0.0;
                else discrimination = numerator / (sigmaThis * sigmaTotal);
            } else discrimination = false;
            
            discriminations.push(discrimination);    
        }

    /**************************************************************************************************/
    /* Set up score plot                                                                              */
    /**************************************************************************************************/

        if (showGraph) {

            var numScoreBins = 10;

            var scoreCounts = [];
            for (var i = 0; i < numScoreBins; ++i) scoreCounts.push(0);
        
            for (var i = 0; i < studentScores.length; ++i) {
                var binNum = Math.floor(studentScores[i] * numScoreBins);
                if (binNum >= numScoreBins) binNum = numScoreBins - 1;
                scoreCounts[binNum]++;
            }
        
            var scoreGraphPoints = [];
            for (var i = 0; i < numScoreBins; ++i) {
                scoreGraphPoints.push([100 * i / numScoreBins, scoreCounts[i]]);
                scoreGraphPoints.push([100 * (i + 1) / numScoreBins, scoreCounts[i]]);
            }
        
            $.plot($('#div_scoreDistribution'), [{
                data: scoreGraphPoints,
                lines: {
                    show: true,
                    fill: true
                },
            }], {
                xaxis: {
                    tickFormatter: function(val, axis) {
                        return val.toString() + '%';
                    }
                },
                yaxis: {
                    tickDecimals: 0
                },
                colors:["#5D95C4"]
            });

        } else {

            $('#graph_wrapper').hide();

        }

    /**************************************************************************************************/
    /* Set up difficulty and discrimination plots                                                     */
    /**************************************************************************************************/

        if (showMinorGraphs) {

            // Get binned values for statistics
            
            var difficultyCounts = [];
            for (var i = 0; i < StatsCommon.difficultyBins; ++i) difficultyCounts.push(0);
            
            var discriminationCounts = [];
            for (var i = 0; i < StatsCommon.discriminationBins; ++i) discriminationCounts.push(0);
            
            for (var i = 0; i < items.length; ++i) {

                if (items[i].scoring != 'omit') {

                    if (difficulties[i] !== false) {

                        // Update difficulty bin

                        var lowerLimit, upperLimit;
                        if (items[i].type.substr(0, 2) == 'mc') {
                            lowerLimit = 1.0 / items[i].Answers.length;
                            upperLimit = 1.0 - 0.03 * (items[i].Answers.length - 1);
                        } else {
                            lowerLimit = StatsCommon.difficultyLowerLimit;
                            upperLimit = StatsCommon.difficultyUpperLimit;
                        }
                        var idealAverage = (lowerLimit + 1.0) / 2.0 + 0.1;
            
                        var lowerWarning = idealAverage - (idealAverage - lowerLimit) * StatsCommon.difficultyWarningFraction;
                        var upperWarning = idealAverage + (upperLimit - idealAverage) * StatsCommon.difficultyWarningFraction;
                        
                        var lowerDanger = idealAverage - (idealAverage - lowerLimit) * StatsCommon.difficultyDangerFraction;
                        var upperDanger = idealAverage + (upperLimit - idealAverage) * StatsCommon.difficultyDangerFraction;
                            
                        var binNum;
                        if (difficulties[i] < lowerDanger) {
                            var binBase = 0;
                            var binSpan = StatsCommon.difficultyDangerBins;
                            var fraction = (difficulties[i] - 0) / (lowerWarning - 0);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else if (difficulties[i] < lowerWarning) {
                            var binBase = StatsCommon.difficultyDangerBins;
                            var binSpan = StatsCommon.difficultyWarningBins;
                            var fraction = (difficulties[i] - lowerDanger) / (lowerWarning - lowerDanger);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else if (difficulties[i] < upperWarning) {
                            var binBase = StatsCommon.difficultyDangerBins + StatsCommon.difficultyWarningBins;
                            var binSpan = StatsCommon.difficultyBins - 2 * StatsCommon.difficultyDangerBins - 2 * StatsCommon.difficultyWarningBins;
                            var fraction = (difficulties[i] - lowerWarning) / (upperWarning - lowerWarning);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else if (difficulties[i] < upperDanger) {
                            var binBase = StatsCommon.difficultyBins - StatsCommon.difficultyDangerBins - StatsCommon.difficultyWarningBins;
                            var binSpan = StatsCommon.difficultyWarningBins;
                            var fraction = (difficulties[i] - upperWarning) / (upperDanger - upperWarning);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else {
                            var binBase = StatsCommon.difficultyBins - StatsCommon.difficultyDangerBins;
                            var binSpan = StatsCommon.difficultyDangerBins;
                            var fraction = (difficulties[i] - upperDanger) / (1.0 - upperDanger);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        }

                        difficultyCounts[binNum]++;
                    } 

                    if (discriminations[i] !== false) {

                        // Update discrimination bin

                        if (discriminations[i] < StatsCommon.discriminationDanger) {
                            var binBase = 0;
                            var binSpan = StatsCommon.discriminationDangerBins;
                            var clampedDiscrimination = (discriminations[i] < 0.0) ? 0.0 : discriminations[i];
                            var fraction = (clampedDiscrimination - 0.0) / (StatsCommon.discriminationWarning - 0);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else if (discriminations[i] < StatsCommon.discriminationWarning) {
                            var binBase = StatsCommon.discriminationDangerBins;
                            var binSpan = StatsCommon.discriminationWarningBins;
                            var fraction = (discriminations[i] - StatsCommon.discriminationDanger) / (StatsCommon.discriminationWarning - StatsCommon.discriminationDanger);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        } else {
                            var binBase = StatsCommon.discriminationDangerBins + StatsCommon.discriminationWarningBins;
                            var binSpan = StatsCommon.discriminationBins - StatsCommon.discriminationDangerBins - StatsCommon.discriminationWarningBins;
                            var fraction = (discriminations[i] - StatsCommon.discriminationWarning) / (1.0 - StatsCommon.discriminationWarning);
                            binNum = binBase + Math.floor(fraction * binSpan);
                            if (fraction == 1.0) binNum--;
                        }

                        discriminationCounts[binNum]++;   
                    }
 
                }
        
            }

            // Set up difficulty plot

            var maxDifficultyCount = Math.max.apply(null, difficultyCounts);
            var difficultyGraphPoints = [];
            for (var i = 0; i < StatsCommon.difficultyBins; ++i) {
                difficultyGraphPoints.push([100 * i / StatsCommon.difficultyBins, difficultyCounts[i]]);
                difficultyGraphPoints.push([100 * (i + 1) / StatsCommon.difficultyBins, difficultyCounts[i]]);
            }

            $.plot($('#div_difficultyDistribution'), [{
                data: difficultyGraphPoints.slice(0, 2),
                lines: {
                    show: true,
                    lineWidth: 0,
                    fill: 1
                },
                color: jQuery.Color(StatsCommon.dangerColour).toHexString()
            }, {
                data: difficultyGraphPoints.slice(2, 4),
                lines: {
                    show: true,
                    lineWidth: 0,
                    fill: 1
                },
                color: jQuery.Color(StatsCommon.warningColour).toHexString()
            }, {
                data: difficultyGraphPoints.slice(4, difficultyGraphPoints.length - 4),
                lines: {
                    show: true,
                    lineWidth: 0,
                    fill: 1
                },
                color: jQuery.Color(StatsCommon.goodColour).toHexString()
            }, {
                data: difficultyGraphPoints.slice(difficultyGraphPoints.length - 4, difficultyGraphPoints.length - 2),
                lines: {
                    show: true,
                    lineWidth: 0,
                    fill: 1
                },
                color: jQuery.Color(StatsCommon.warningColour).toHexString()
            }, {
                data: difficultyGraphPoints.slice(difficultyGraphPoints.length - 2),
                lines: {
                    show: true,
                    lineWidth: 0,
                    fill: 1
                },
                color: jQuery.Color(StatsCommon.dangerColour).toHexString()
            }, {
                data: difficultyGraphPoints,
                lines: {
                    show: true
                },
                color: '#333333'
            }], {
                xaxis: {
                    ticks: [
                        [50 / StatsCommon.difficultyBins, 'hard'],
                        [50, 'ideal'],
                        [100 - 50 / StatsCommon.difficultyBins, 'easy']
                    ],
                    tickLength: 0
                }, 
                yaxis: {
                    min: 0, 
                    max: maxDifficultyCount * 1.35,
                    ticks: false
                },
                colors:["#5D95C4"]
            });
            $('#div_difficultyDistribution').append(
                "<div style='position:absolute; left:20px; top:15px; display:flex; align-items:center; gap:5px;'>" + 
                    "<div>Average score</div>" + 
                    "<button class='btn btn-light btn-mini info_button average_info'>Info</button>" +
                "</div>");

            // Set up discrimination plot
            
            if ($('#div_discriminationDistribution').length) {
                var maxDiscriminationCount = Math.max.apply(Math, discriminationCounts);
                var discriminationGraphPoints = [];
                for (var i = 0; i < StatsCommon.difficultyBins; ++i) {
                    discriminationGraphPoints.push([100 * i / StatsCommon.discriminationBins, discriminationCounts[i]]);
                    discriminationGraphPoints.push([100 * (i + 1) / StatsCommon.discriminationBins, discriminationCounts[i]]);
                }

                $.plot($('#div_discriminationDistribution'), [{
                    data: discriminationGraphPoints.slice(0, 2),
                    lines: {
                        show: true,
                        lineWidth: 0,
                        fill: 1
                    },
                    color: jQuery.Color(StatsCommon.dangerColour).toHexString()
                }, {
                    data: discriminationGraphPoints.slice(2, 6),
                    lines: {
                        show: true,
                        lineWidth: 0,
                        fill: 1
                    },
                    color: jQuery.Color(StatsCommon.warningColour).toHexString()
                }, {
                    data: discriminationGraphPoints.slice(6),
                    lines: {
                        show: true,
                        lineWidth: 0,
                        fill: 1
                    },
                    color: jQuery.Color(StatsCommon.goodColour).toHexString()
                }, {
                    data: discriminationGraphPoints,
                    lines: {
                        show: true
                    },
                    color: '#333333'
                }], {
                    xaxis: {
                        ticks: [
                            [50 / StatsCommon.discriminationBins, 'poor'],
                            [50, 'good'],
                            [100 - 50 / StatsCommon.discriminationBins, 'great']
                        ],
                        tickLength: 0
                    }, 
                    yaxis: {
                        min: 0, 
                        max: maxDiscriminationCount * 1.35,
                        ticks: false
                    },
                    colors:["#5D95C4"]
                });
                $('#div_discriminationDistribution').append(
                    "<div style='position:absolute; left:20px; top:15px; display:flex; align-items:center; gap:5px;'>" + 
                        "<div>Discrimination</div>" + 
                        "<button class='btn btn-light btn-mini info_button pearson_info'>Info</button>" +
                    "</div>");
            }

        } else {

            $('#minor_graphs_wrapper').hide();

        }

    /**************************************************************************************************/
    /* Set up assessment statistics                                                                   */
    /**************************************************************************************************/

        var isIncluded = [];
        for (var i = 0; i < items.length; ++i) isIncluded.push(true);
        var stats = getStatistics(isIncluded);

        $('#stats_table').append('<tr><td>Number of responses</td><td style="text-align:right">' + studentScores.length + '</td></tr>');

        if (showMeanSD) {
            var mean = (stats['mean'] * 100).toFixed(1) + "%";
            $('#stats_table').append('<tr><td>Mean score</td><td style="text-align:right">' + mean + '</td></tr>');
        }
        if (showMedian) {
            var median = (stats['median'] * 100).toFixed(1) + "%";
            $('#stats_table').append('<tr><td>Median score</td><td style="text-align:right">' + median + '</td></tr>');
        }

        if (showMeanSD) {
            var stdev = ('variance' in stats) ? ((Math.sqrt(stats['variance']) * 100).toFixed(0) + "%") : '&mdash;';
            $('#stats_table').append('<tr><td>Standard deviation (&sigma;)</td><td style="text-align:right">' + stdev + '</td></tr>');

            var stderr;
            if (('variance' in stats) && ('alpha' in stats)) {
                stderr = (Math.sqrt(stats['variance'] * (1.0 - stats['alpha'])) * 100).toFixed(0) + "%";
            } else stderr = '&mdash;';

            $('#stats_table').append('<tr><td>Standard error of measurement</td><td style="text-align:right">' + stderr + '</td></tr>');
        }

        if (showCronbach) {
            var cronbach = ('alpha' in stats) ? stats['alpha'].toFixed(2) : '&mdash;';
            $('#stats_table').append('<tr><td>Cronbach\'s alpha (&alpha;)</td><td style="text-align:right">' + cronbach + '</td></tr>');
        }

    /**************************************************************************************************/
    /* Get collusion analysis                                                                         */
    /**************************************************************************************************/

        var largeGroupThreshold = 100
        var numTestValues = 100000;
        var collusionThreshold = 4.0;
        
        if (showCollusion) {

            isIncluded = [];
            var numIncluded = 0;

            for (var i = 0; i < items.length; ++i) {
                var itemIsIncluded = (items[i].type.substr(0, 2) == 'mc');
                if (itemIsIncluded) numIncluded++;
                isIncluded.push(itemIsIncluded);
            }
        
            if (responses.length > largeGroupThreshold) {
        
                $('#collusion_results').html("<p>Too many assessments to run collusion analysis.</p>");
        
            } else if (numIncluded == 0) {
        
                $('#collusion_results').html("<p>No warnings.<p>");
        
            } else  {
                            
                // Find the probability of each response in the observed distribution
        
                var responseProbabilities = [];
                for (var i = 0; i < items.length; ++i) {            
                    var thisProbabilities = {};
                    
                    var questionResponses = 0;
                    for (var j = 0; j < responses.length; ++j) {
                        var responseValue = responses[j].items[i].response.trim();
                        if (!(responseValue in thisProbabilities)) thisProbabilities[responseValue] = 0.0;
                        thisProbabilities[responseValue] += 1.0;
                        questionResponses++;
                    }
        
                    for (var responseValue in thisProbabilities) {
                        thisProbabilities[responseValue] /= questionResponses;
                    }
        
                    responseProbabilities.push(thisProbabilities);
                }
                
                // Sample answer pairs from the observed distribution and calculate their probabilities as a reference
                
                var sum = 0.0;
                var sumSquared = 0.0;
                for (var n = 0; n < numTestValues; ++n) {
                    var product = 1.0;
                    for (var i = 0; i < items.length; ++i) {
                        if (!isIncluded[i]) continue;
        
                        var aValue, bValue;
                        
                        var aRand = Math.random();
                        for (aValue in responseProbabilities[i]) {
                            if (aRand < responseProbabilities[i][aValue]) break;
                            else aRand -= responseProbabilities[i][aValue];
                        }    
                        if (aValue.length == 0) continue;
                        if (aValue.indexOf('!') >= 0) continue;
                        
                        var bRand = Math.random();
                        for (bValue in responseProbabilities[i]) {
                            if (bRand < responseProbabilities[i][bValue]) break;
                            else bRand -= responseProbabilities[i][bValue];
                        }    
                        if (bValue.length == 0) continue;
                        if (bValue.indexOf('!') >= 0) continue;
                        
                        if (aValue == bValue) product *= responseProbabilities[i][aValue];
                    }
                    
                    product = Math.pow(product, 1.0 / items.length);
                    sum += product;
                    sumSquared += product * product;
                }
                
                // Fit a beta distribution to the products, normalized by the number of MC questions
                
                var mean = sum / numTestValues;
                var variance = (sumSquared - 2.0 * sum * mean + numTestValues * mean * mean) / (numTestValues - 1);
                var factor = mean * (1.0 - mean) / variance - 1.0;
                var alpha = mean * factor;
                var beta = (1.0 - mean) * factor;
                
                // Build a response correlation grid to show the probability that two students would share
                // the responses they do
                
                var tableContents = '';
                for (var a = 0; a < responses.length; ++a) {
                    for (var b = 0; b < a; ++b) {
                        var probability = 1.0;
                        for (var k = 0; k < items.length; ++k) {
                            if (!isIncluded[k]) continue;
        
                            var aValue = responses[a].items[k].response.trim();
                            if (aValue.length == 0) continue;
                            if (aValue.indexOf('!') >= 0) continue;
                                
                            var bValue = responses[b].items[k].response.trim();
                            if (bValue.length == 0) continue;
                            if (bValue.indexOf('!') >= 0) continue;
                            
                            if (aValue == bValue) probability *= responseProbabilities[k][aValue];
                        }
                        
                        if (probability == 1.0) continue;
                        
                        var normalizedProbability = Math.pow(probability, 1.0 / items.length);
                        var collusionScale = -Math.log10(jStat.beta.cdf(normalizedProbability, alpha, beta));
                        if (collusionScale > collusionThreshold) {
                            tableContents += "<tr><td style='text-transform:capitalize;'>";
                            if (('student' in responses[a]) && ('name' in responses[a].student)) {
                                tableContents += responses[a].student.name;
                            } else tableContents += responses[a].index;
        
                            tableContents += "</td><td style='text-transform:capitalize;'>";
        
                            if (('student' in responses[b]) && ('name' in responses[b].student)) {
                                tableContents += responses[b].student.name;
                            } else tableContents += responses[b].index;
        
                            tableContents += "</td>";
                            
                            var prefix = Math.floor(Math.pow(10.0, collusionScale - Math.floor(collusionScale)));
                            var power = Math.floor(collusionScale);
                            if (power >= 9) {
                                tableContents += "<td style='text-align:right;'>Less than one in a billion</td>";
                            } else {
                                tableContents += "<td style='text-align:right;'>One in " + prefix;
                                for (var n = 0; n < power % 3; ++n) tableContents += "0";
                                for (var n = 0; n < Math.floor(power / 3); ++n) tableContents += ",000";
                                tableContents += "</td>";
                            }
                            tableContents += "</tr>";
                        }
                    }
                }
        
                if (tableContents.length > 0) 
                {
                    var tableHTML = "";
        
                    tableHTML += "<p>Possible collusions were detected:</p>";
                    tableHTML += "<table class='report_table'>";
                    tableHTML += "<tr class='tableHeader'><td>Student 1</td><td>Student 2</td><td style='text-align:right;'>Probablity by chance</td></tr>";
                    tableHTML += tableContents;
                    tableHTML += "</table>";
        
                    $('#collusion_results').html(tableHTML);
        
                } else {
        
                    $('#collusion_results').html("<p>No collusions were detected.</p>");
        
                } 
            }

        } else {

            $('#collusion_wrapper').hide();

        }

    /**************************************************************************************************/
    /* Get outcomes analysis                                                                          */
    /**************************************************************************************************/

        if (showCategories && (outcomes.length > 0)) {

            var sortedStats = [];
            for (var i = 0; i < outcomes.length; ++i) {

                isIncluded = [];
                var numIncluded = 0;

                for (var j = 0; j < items.length; ++j) {
                    var itemIsIncluded = outcomes[i].item_ids.includes(items[j].id);
                    if (itemIsIncluded) numIncluded++;
                    isIncluded.push(itemIsIncluded);
                }

                var stats = getStatistics(isIncluded);
                stats['name'] = outcomes[i].name;
                stats['numIncluded'] = numIncluded;
                sortedStats.push(stats);

            }

            if (reportData.defaults.outcomesort == 'score') {
                sortedStats.sort((a,b) => (a.mean - b.mean));
            }
                
            for (var i = 0; i < sortedStats.length; ++i)
            {
                var name = sortedStats[i]["name"];
                var numIncluded = sortedStats[i]["numIncluded"];

                var rowHTML = '<tr>';

                rowHTML += "<td style='width:350px;'>" + name + "</td>";

                rowHTML += "<td style='text-align:right;'>" + numIncluded + "</td>";

                var meanText = (numIncluded > 0) ? ((100.0 * sortedStats[i]["mean"]).toFixed(1) + '%') : '&mdash;';
                rowHTML += "<td style='text-align:right;'>" + meanText + "</td>";

                var medianText = (numIncluded > 0) ? ((100.0 * sortedStats[i]["median"]).toFixed(1) + '%') : '&mdash;';
                rowHTML += "<td style='text-align:right;'>" + medianText + "</td>";

                if ('alpha' in sortedStats[i]) {
                    rowHTML += "<td style='text-align:right;'>" + sortedStats[i]["alpha"].toFixed(2) + "</td>";
                } else rowHTML += "<td style='text-align:right;'>&mdash;</td>";

                rowHTML += "</tr>\n";

                $('#outcomes_table').append(rowHTML);
            }

        } else {

            $('#outcomes_wrapper').hide();

        }

    /**************************************************************************************************/
    /* Get item analyses                                                                              */
    /**************************************************************************************************/

        if (showResponseAnalysis) {
            var graphElements = [];

            for (var questionIndex = 0; questionIndex < items.length; ++questionIndex) {
                var newHTML = '<div class="item_statistics_outer"><div class="item_statistics_entry">';
                newHTML += '<h4>Question ' + items[questionIndex].number;
                if (items[questionIndex].scoring == 'omit') newHTML += ' (omitted)';
                newHTML += '</h4>';

                var difficulty = difficulties[questionIndex];
                if (difficulty !== false) difficulty *= 100;

                if (showAverage && (difficulty !== false)) {

                    var lowerLimit, upperLimit;
                    if (items[questionIndex].type.substr(0, 2) == 'mc') {
                        lowerLimit = 1.0 / items[questionIndex].Answers.length;
                        upperLimit = 1.0 - 0.03 * (items[questionIndex].Answers.length - 1);
                    } else {
                        lowerLimit = StatsCommon.difficultyLowerLimit;
                        upperLimit = StatsCommon.difficultyUpperLimit;
                    }
                    var idealAverage = (lowerLimit + 1.0) / 2.0 + 0.1;
    
                    var lowerWarning = idealAverage - (idealAverage - lowerLimit) * StatsCommon.difficultyWarningFraction;
                    var upperWarning = idealAverage + (upperLimit - idealAverage) * StatsCommon.difficultyWarningFraction;
                    
                    var lowerDanger = idealAverage - (idealAverage - lowerLimit) * StatsCommon.difficultyDangerFraction;
                    var upperDanger = idealAverage + (upperLimit - idealAverage) * StatsCommon.difficultyDangerFraction;
    
                    var widthA = lowerDanger * 100;
                    var widthB = (lowerWarning - lowerDanger) * 100;
                    var widthC = (upperWarning - lowerWarning) * 100;
                    var widthD = (upperDanger - upperWarning) * 100;
                        
                    var difficultyBar = 
                        "<div style='display:inline-block; vertical-align:middle; width:100px; height:13px; border:1px solid black; background-color:" + StatsCommon.dangerColour + "; position:relative;'>" +
                            "<div class='stats_indicator' style='left:" + difficulty + "%;'>&#9660</div>" +
                            "<div style='display:inline-block; height:100%; width:" + widthA + "%; background-color:" + StatsCommon.dangerColour + "'></div>" +
                            "<div style='display:inline-block; height:100%; width:" + widthB + "%; background-color:" + StatsCommon.warningColour + "'></div>" +
                            "<div style='display:inline-block; height:100%; width:" + widthC + "%; background-color:" + StatsCommon.goodColour + "'></div>" +
                            "<div style='display:inline-block; height:100%; width:" + widthD + "%; background-color:" + StatsCommon.warningColour + "'></div>" +
                        "</div>";

                    newHTML +=
                        "<div class='stats_difficulty_wrapper' style='margin-top:10px; margin-bottom:10px;'>" + 
                            "<div class='part_statistics_label' style='vertical-align:middle;'>Average score:</div>" + difficultyBar + 
                            "<div class='part_statistics_value' style='vertical-align:middle; margin-left:5px;'>" + difficulty.toFixed(0) + "%</div>" + 
                        "</div>";
                }

                var discrimination = discriminations[questionIndex];

                if (showPearson && (discrimination !== false)) {
                    var discriminationString = discrimination.toFixed(2);

                    var barDiscrimination = Math.max(discrimination, 0) * 100;

                    var disciminationBar = 
                        "<div style='display:inline-block; vertical-align:middle; width:100px; height:13px; border:1px solid black; background-color:" + StatsCommon.goodColour + "; position:relative'>" +
                            "<div class='stats_indicator' style='left:" + barDiscrimination + "%;'>&#9660</div>" +
                            "<div style='display:inline-block; height:100%; width:" + (StatsCommon.discriminationDanger * 100).toFixed(2) + "%; background-color:" + StatsCommon.dangerColour + "'></div>" +
                            "<div style='display:inline-block; height:100%; width:" + ((StatsCommon.discriminationWarning - StatsCommon.discriminationDanger) * 100).toFixed(2) + "%; background-color:" + StatsCommon.warningColour + "'></div>" +
                        "</div>";

                
                    newHTML += 
                        "<div class='stats_discrimination_wrapper' style='margin-top:10px; margin-bottom:10px;'>" + 
                            "<div class='part_statistics_label' style='vertical-align:middle;'>Discrimination:</div>" + disciminationBar + 
                            "<div class='part_statistics_value' style='vertical-align:middle; margin-left:5px;'>" + discriminationString + "</div>" + 
                        "</div>";
                }

                if (showResponses && (items[questionIndex].type != 'wr')) {

                    var partStyle = items[questionIndex].type.substr(0, 2);

                    // Get answers and scores

                    var rowData = [];

                    for (var i = 0; i < items[questionIndex].Answers.length; ++i) {

                        var response = items[questionIndex].Answers[i].response;
                        if (items[questionIndex].type == 'nr_scientific') {
                            response = response.replace(/\D/g,'');
                        } else if (items[questionIndex].type == 'nr_selection') {
                            if (items[questionIndex].TypeDetails.ignoreOrder) {
                                var responseParts = response.split('');
                                responseParts.sort();
                                response = responseParts.join('').trim();                            
                            }
                        }

                        rowData.push({
                            answer_id: items[questionIndex].Answers[i].id,
                            response: response,
                            value: 0,
                            n: 0
                        });
                    }

                    for (var i = 0; i < items[questionIndex].ScoredResponses.length; ++i) {
                        if ('id' in items[questionIndex].ScoredResponses[i]) {

                            var answerID = items[questionIndex].ScoredResponses[i].id;
                            for (var j = 0; j < items[questionIndex].Answers.length; ++j) {
                                if (rowData[j].answer_id == answerID) {
                                    var value = items[questionIndex].ScoredResponses[i].value;
                                    if (items[questionIndex].type == 'nr_selection') {
                                        if (items[questionIndex].TypeDetails.markByDigits) {
                                            value *= rowData[j].response.length;
                                        }
                                    }
                                    rowData[j].value = value;
                                }
                            }
                            
                        } else if ('ids' in items[questionIndex].ScoredResponses[i]) {

                            var answerIDs = items[questionIndex].ScoredResponses[i].ids;

                            var responseText = '';
                            for (var j = 0; j < answerIDs.length; ++j) {
                                for (var k = 0; k < items[questionIndex].Answers.length; ++k) {
                                    if (items[questionIndex].Answers[k].id == answerIDs[j]) {
                                        responseText += items[questionIndex].Answers[k].response;
                                    }
                                }
                            }
                            rowData[i].response = responseText;
        
                            rowData.push({
                                response: responseText,
                                value: items[questionIndex].ScoredResponses[i].value,
                                n: 0
                            });	

                        }
                    }

                    // Count responses and add any that aren't there yet

                    for (var i = 0; i < responses.length; ++i) {
                        var responseText = responses[i].items[questionIndex].response;

                        if (items[questionIndex].type == 'nr_selection') {
                            
                            if (items[questionIndex].TypeDetails.ignoreOrder) {

                                var responseParts = responseText.split('');
                                responseParts.sort();
                                responseText = responseParts.join('').trim();

                            } else {

                                var fullResult = ScoringCommon.scoreNumericResponse(responseText, items[questionIndex]);

                                var trimmedResponse = responseText.trim();
                                var trimmedResult = ScoringCommon.scoreNumericResponse(trimmedResponse, items[questionIndex]);

                                if ((fullResult !== false) && (trimmedResult !== false)) {
                                    if (fullResult.score == trimmedResult.score) responseText = trimmedResponse;
                                    else responseText = responseText.replace(' ', '_');    
                                }

                            }
                        } else responseText = responseText.trim();
                        
                        if (responseText.length > 0) {
                            
                            var matchingRow;
                            for (matchingRow = 0; matchingRow < rowData.length; ++matchingRow) {
                                if (responseText == rowData[matchingRow].response) break;
                            }
    
                            if (matchingRow == rowData.length) {    
                                if (partStyle == 'nr') {
                                    var rawResponse = responses[i].items[questionIndex].response;
                                    var result = ScoringCommon.scoreNumericResponse(rawResponse, items[questionIndex]);
                                    if (result !== false) {
                                        rowData.push({
                                            response: responseText,
                                            value: result.score,
                                            n: 1
                                        });        
                                    }
                                }
                            } else rowData[matchingRow].n++;

                        }                        
                    }

                    if (rowData.length == 0) {

                        newHTML += "<div><i>No responses to show</i></div>";

                    } else {

                        // Sort responses

                        if ((partStyle == 'mc') && (items[questionIndex].type != 'mc_truefalse')) {

                            rowData.sort(function(a, b) {
                                if (a.response.length > b.response.length) return 1;
                                else if (a.response.length < b.response.length) return -1;
                                if (a.response > b.response) return 1;
                                else if (a.response < b.response) return -1;
                                else return 0;
                            });

                        } else if (partStyle == 'nr') {

                            rowData.sort(function(a, b) {
                                if (a.n > b.n) return -1;
                                else if (a.n < b.n) return 1;
                                else if (a.value > b.value) return -1;
                                else if (a.value < b.value) return 1;
                                else return 0;
                            });

                        }

                        // Build table

                        var numRows = rowData.length;
                        
                        var hasTable = false;
                        for (var i = 0; i < numRows; ++i) {

                            var responseText = rowData[i].response;

                            var scoreType;
                            if (rowData[i].value == items[questionIndex].maxScore) scoreType = 'correct';
                            else if (rowData[i].value > 0) scoreType = 'partial';
                            else scoreType = 'incorrect';

                            if (!hasTable) {
                                newHTML += "<table class='stats_table' style='margin-top:10px;'  data-part-style='" + partStyle + "'>";

                                newHTML += "<tr>";
                                newHTML += "<td colspan='2'>Response distribution</td>";
                                newHTML += "<td style='padding-left:10px;'>Marks</td>";
                                newHTML += "</tr>";

                                hasTable = true;
                            }

                            if (partStyle == 'mc') responseText = responseText.toUpperCase() + '.';

                            var barColour = StatsCommon.neutralColour;
                            if (scoreType == 'incorrect') barColour = StatsCommon.incorrectColour;
                            else if (scoreType == 'partial') barColour = StatsCommon.partialColour;
                            else if (scoreType == 'correct') barColour = StatsCommon.correctColour;

                            var barPercent = rowData[i].n * 100 / responses.length;
                            var percentText = barPercent.toFixed(0);
                            
                            newHTML += "<tr class='stats_table_element'>";

                            newHTML += "<td style='padding-right:10px;'>" + responseText + "</td>";
                            newHTML += 
                                "<td style='position:relative; border:1px solid black; width:200px; height:15px; display:flex; align-items:center;'>" +
                                    "<div class='stats_response_bar' style='background-color:" + barColour + "; width:" + percentText + "%; height:100%; display:flex; justify-content:flex-end; align-items:center;'>" +
                                        "<div class='stats_score_label' style='font-size:12px; color:white; padding:0px 5px;'>" + percentText + "%</div>" +
                                    "</div>" + 
                                "</td>";
                            newHTML += "<td style='width:40px; padding-left:10px;'>" + rowData[i].value.toFixed(maxDecimals) + "</td>";

                            newHTML += "</tr>";
                        }

                        if (partStyle == 'nr') {
                            newHTML += 
                                "<tr class='stats_show_more' data-num-shown='0'>" + 
                                    "<td colspan=4>Show more (<span class='stats_show_count'>" + numRows + "</span> hidden)</td>" + 
                                "</tr>";
                        }

                        newHTML += "</table>";
                        
                    }

                }

                newHTML += "</div></div><div></div>";

                graphElements.push({
                    number: items[questionIndex].number,
                    difficulty: difficulty,
                    discrimination: discrimination,
                    html: newHTML
                });
            }

            var itemSort = reportData.defaults.itemsort;
            if (itemSort == 'difficulty') {
                graphElements.sort((a, b) => {
                    if (a.difficulty === false && b.difficulty === false) return 0;
                    else if (a.difficulty === false) return -1;
                    else if (b.difficulty === false) return 1;
                    else return a.difficulty - b.difficulty;
                });
            } else if (itemSort == 'discrimination') {
                graphElements.sort((a, b) => {
                    if (a.discrimination === false && b.discrimination === false) return 0;
                    else if (a.discrimination === false) return -1;
                    else if (b.discrimination === false) return 1;
                    else return a.discrimination - b.discrimination;
                });
            }

            for (var i = 0; i < graphElements.length; ++i) {
                var needsHighlight = false;
                if (graphElements[i].discrimination === false) needsHighlight = false;
                else if ((responses.length > 100) && (graphElements[i].discrimination < 0.15)) needsHighlight = true;
                else if ((responses.length > 10) && (graphElements[i].discrimination <= 0.0)) needsHighlight = true;

                var $newElement = $('<div>' + graphElements[i].html + '</div>');
                if (needsHighlight) $newElement.find('.item_statistics_entry').addClass('item_highlighted');
                $('#item_statistics_wrapper').append($newElement.children());
            }

            $(document).on('click', '.stats_show_more', function(e) {
                var numShown = parseInt($(this).attr('data-num-shown')) + 5;
                
                var numRows = $(this).siblings('.stats_table_element').length;
                $(this).siblings('.stats_table_element').each(function() {
                    var rowIndex = $(this).index();
                    if (rowIndex > numShown) $(this).hide();
                    else $(this).show();
                });
        
                if (numRows > numShown) {
                    $(this).attr('data-num-shown', numShown);
                    $(this).find('.stats_show_count').html(numRows - numShown);
                } else $(this).remove();
            });        

            $('.stats_table[data-part-style="nr"] .stats_show_more').trigger('click');
            $('.stats_table[data-part-style="mc"] .stats_table_element').show();

            StatsCommon.fixScoreLabels($('#item_statistics_wrapper'));

            if ($('#item_statistics_wrapper .item_highlighted').length > 0) {
                $('#highlighted_notice').show();
            } else $('#highlighted_notice').hide();
        }
    }

    if (!isPrint) {

/**************************************************************************************************/
/* Click events and dialogs                                                                       */
/**************************************************************************************************/

        $('.info_dialog').dialog({
            autoOpen: false,
            width: 500,
            modal: true,
            position: {
                my: "center",
                at: "center",
                of: window
            }, buttons: {
                'Thanks!': function() {
                    $(this).dialog("close");
                }
            }, open: function(event) {
                $('.ui-dialog-buttonpane').find('button:contains("Thanks!")').addClass('btn btn-save btn_font');
            }, close: function() {
                $('form').validationEngine('hideAll');
            }
        });		

        $(document).on('click', '.statistics_info', function() {
            $('#statisticsInfo_dialog').dialog('open');
        });

        $(document).on('click', '.collusion_info', function() {
            $('#collusionInfo_dialog').dialog('open');
        });

        $(document).on('click', '.outcomes_info', function() {
            $('#outcomesInfo_dialog').dialog('open');
        });

        $(document).on('click', '.average_info', function() {
            $('#averageInfo_dialog').dialog('open');
        });

        $(document).on('click', '.question_info', function() {
            $('#questionInfo_dialog').dialog('open');
        });

        $(document).on('click', '.pearson_info', function() {
            $('#pearsonInfo_dialog').dialog('open');
        });

        $(document).on('click', '.graphical_info', function() {
            $('#graphicalInfo_dialog').dialog('open');
        });

/**************************************************************************************************/
/* PDF render stuff                                                                               */
/**************************************************************************************************/

        $('#print_button').on('click', function() {
            $('#print_button').attr('disabled', 'disabled');
            var urlParts = window.location.href.split('/');
            var scoreID = urlParts[urlParts.length - 1];

            navigateWithPost('/build_pdf', {
                pdfType: 'pdf_from_url',
                url: '/Scores/print_teacher/' + scoreID,
                target: 'teacher.pdf'
            });

            $('#print_button').removeAttr('disabled');
        });
        
    }

	// Load report data

	initialize()
    .then(() => {

        $('body').append('<span class="page_loaded"></span>');

    }).catch((error) => {

        if (isPrint) {

            $('body').html(
                '<div>Error: ' + error.message + '</div> \
                <span class="page_loaded"></span>');
    
        } else {

            addClientFlash({
                title: "Error",
                text: error.message,
                image: "/img/error.svg"
            });
            window.location.href = '/Documents';
    
        }
        
	});

});

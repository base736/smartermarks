/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

export class ImageUpload {

    constructor(file, onProgress = null) {
        this.file = file;
        this.onProgress = onProgress;
        this._xhr = null;
    }

    start() {
        return new Promise((resolve, reject) => {
            if (this._xhr !== null) {
                reject(new Error('Upload already in progress'));
                return;
            }

            // Create the request

            this._xhr = new XMLHttpRequest();
            this._xhr.open('POST', '/upload_image');
            this._xhr.responseType = 'json';

            // Track progress if needed

            if (this.onProgress) {
                this._xhr.upload.addEventListener('progress', (evt) => {
                    if (evt.lengthComputable) {
                        const percent = Math.round((evt.loaded / evt.total) * 100);
                        this.onProgress(percent);
                    }
                });
            }

            // XHR event handlers

            this._xhr.onload = () => {
                if (this._xhr.status === 200) {
                    const responseData = this._xhr.response;
                    if (responseData && responseData.success) {
                        resolve(responseData);
                    } else {
                        reject(new Error('Unknown server error'));
                    }
                } else {
                    reject(new Error('HTTP error: ' + this._xhr.status));
                }
            };

            this._xhr.onerror = () => {
                reject(new Error('Network error'));
            };

            this._xhr.onabort = () => {
                reject(new Error('Upload cancelled'));
            };

            // Build FormData

            const formData = new FormData();
            formData.append('imageFile', this.file);

            // Send

            this._xhr.send(formData);

        }).finally(() => {
            this._xhr = null;
        });
    }

    abort() {
        if (this._xhr) {
            this._xhr.abort();
        }
    }
}
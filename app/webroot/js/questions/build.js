/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import { localizationInstance } from '/js/FormEngine/Localization.js?7.0';
import { ImageUpload } from '/js/ImageUpload/ImageUpload.js?7.0';

import * as Folders from '/js/common/folders.js?7.0';
import * as AssessmentsRender from '/js/assessments/render.js?7.0';
import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as ScrollingCommon from '/js/common/scrolling.js?7.0';
import * as EqnEdit from '/js/EquationEditor/eqn_edit.js?7.0';

import * as HtmlEdit from '/js/HTMLEditor/html_edit.js?7.0';
import * as HTMLEditorUtils from '/js/HTMLEditor/utils.js?7.0';

import * as StatsCommon from '/js/common/stats.js?7.0';

$.extend($.ui.dialog.prototype.options, {
	resizable: false
});

var countWords = null;

document.addEventListener("DOMContentLoaded", async function() {

    ScrollingCommon.initialize();
    await AssessmentsRender.initialize();
    await RenderTools.initialize();
    await EqnEdit.initialize();
    await HtmlEdit.initialize();

	StatsCommon.initFilterInterface();

    var blurWaiting = [];
    var focusWaiting = [];

	var questionID;
	var questionData;
	var questionSettings;
	var questionVersionID;

    var versionData;
    var versionIndex;
	
	var newQuestionData = false;
	var newQuestionSettings = false;
    var assessmentVersionID = false;

    var oldQuestionHandleID = false;
    var oldQuestionID = false;

	var activePart = false;
	var editPart = false;

    $('.intSpinner').spinner({
	    step: 1,
	    min: 1
    });

    $('.marginSpinner').spinner({
	    step: 0.25,
        min: 0
    });

    $('#djsq_wr_height').spinner('option', 'min', 0.5);
	$('#djsq_wr_height').spinner('option', 'max', 10.0);

    $('#preview').click(function() {
		window.open('/Questions/preview/' + questionID, '_blank');
    });

	$('#newItem_button').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link') || $(this).hasClass('always-disabled-link')) return;

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		var folderID = $(this).attr('data-folder-id');

		ajaxFetch('/Questions/create', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				folder_id: folderID
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				var target = '/Questions/build/' + responseData.id;
				target += "?u=" + responseData.user_question_id;
				window.location = target;
			} else {
				var message = "Unable to create question.";
				if ('message' in responseData) message = responseData.message;

				$.gritter.add({
					title: "Error",
					text: message,
					image: "/img/error.svg"
				});			
			}
		});
	});

    function get_part_text(rawText) {
	    var $div = $('<div>' + rawText + '</div>');
	    $div.find('.equation').replaceWith('<div>[Equation]</div>');
		return $div.text().replace(/\s+/g, " ");
    }

	var getWizardData = function() {
		if ('data' in questionData.Engine) return questionData.Engine.data;
		else return {};
	}

	var setWizardData = function(targetQuestionData, newData) {
		var hasData = false;
		for (var type in newData) {
			if (newData.hasOwnProperty(type)) {
				if (newData[type].length > 0) hasData = true;
				else delete newData[type];
			}
		}		
		if (hasData) targetQuestionData.Engine.data = newData;
		else delete targetQuestionData.Engine.data;
	}

	var initialize = async function() { 

		var pathParts = window.location.pathname.split('/');
		questionID = parseInt(pathParts.pop());

		var requestURL = '/Questions/get/' + questionID;
		if (questionHandleID !== false) {
			requestURL += `?question_handle_id=${questionHandleID}`;
		}

		const response = await ajaxFetch(requestURL);
		const responseData = await response.json();

		if (responseData.success) {
			var isValid = true;
			if (!('Question' in responseData)) isValid = false;
			if (!('data_string' in responseData.Question)) isValid = false;
			else if (!('data_hash' in responseData.Question)) isValid = false;
			else if (responseData.Question.data_hash != SparkMD5.hash(responseData.Question.data_string)) isValid = false;

			if (isValid) {

				// Initialize folder trees

				initFolderTrees();

				// Copy question data to global variables

				$('#loading_panel').hide();

				questionData = JSON.parse(responseData.Question.data_string);

				versionData = [responseData.Question.data_string];
				versionIndex = 0;

				questionSettings = {
					name: responseData.Question.name,
					open_status: responseData.Question.open_status
				};

				questionVersionID = responseData.Question.version_id;

				StatsCommon.fullStatistics[questionID] = responseData.Statistics;

				// Show appropriate view buttons

				if (questionData.Engine.type == 'code') {
					if (canWriteCode) $('.view_button[data-view-type="engine"]').show();
					else $('.view_button[data-view-type="engine"]').hide();
					$('.view_button[data-view-type="variables"]').hide();
				} else {
					$('.view_button[data-view-type="engine"]').hide();
					$('.view_button[data-view-type="variables"]').show();
				}
				$('#view_menu').menu('enable');

				// Initialize used IDs

				if ('Common' in questionData) {
					for (var i = 0; i < questionData.Common.Answers.length; ++i) {
						usedIDs.push(questionData.Common.Answers[i].id);
					}	
				}

				for (var i = 0; i < questionData.Parts.length; ++i) {
					usedIDs.push(questionData.Parts[i].id);
					if ('Answers' in questionData.Parts[i]) {
						for (var j = 0; j < questionData.Parts[i].Answers.length; ++j) {
							usedIDs.push(questionData.Parts[i].Answers[j].id);
						}	
					}
				}

				// Draw name alias

				drawNameAlias();

				// Show the selected view

				triggerActiveView();

				// Enable buttons

				$('#startEditDetails').removeAttr('disabled');
				$('#preview').removeAttr('disabled');
				$('#edit_mode_select').removeAttr('disabled');

				// Open dialogs as required

				if (showPartNote) {
					$('#editPart_note').dialog('open');
				} else if (showSettings) {
					$("#startEditDetails").trigger("click");
				}
				
			} else {

				throw new Error('Invalid question data.');

			}
		} else {

			throw new Error('Failed to fetch question data.');

        }
	}

	function initFolderTrees() {
		if ($('#treeData').html().length) {
			var treeData = JSON.parse($('#treeData').html());
	
			var roots = {};
			for (var i = 0; i < treeData.length; ++i) {
				roots[treeData[i].text] = {
					id: treeData[i].id,
					folder_id: treeData[i].data.folder_id
				};
			}
	
			// Set up question folder trees
	
			Folders.initFolderTree($('#targetFolderParent'), treeData);
			$('#targetFolderParent').jstree('close_all');
			$('#targetFolderParent').jstree('deselect_all');

			Folders.disableConditional($('#folderPanel'), treeData, function(data) { 
				if (('community_id' in data) && (data.community_id == communityID)) return false;
				else if (('can_export' in data) && !data.can_export) return true;
				else return false;
			});
	
			var defaultQuestionNodeId;
			if (communityID == null) {
	
				Folders.disableSubtree($('#targetFolderParent'), roots['Communities'].id);
				defaultQuestionNodeId = roots['Home'].id;
	
			} else if (questionFolderId !== false) {
	
				Folders.disableAll($('#targetFolderParent'));
				Folders.enableConditional($('#targetFolderParent'), treeData, function(data) { 
					return (('community_id' in data) && (data.community_id == communityID));
				});
				defaultQuestionNodeId = Folders.getNodeId(treeData, questionFolderId);
	
			}
				
			var selectID = $('#targetFolderParent').attr('data-prefix') + defaultQuestionNodeId;
			$('#targetFolderParent').jstree('deselect_all');
			$('#targetFolderParent').jstree('select_node', selectID);
		}
	}

	var triggerActiveView = function() {
	
		// Show the selected view

		var urlVars = getUrlVars();

		if ('view' in urlVars) {
			var $viewButton = $('.view_button[data-view-type="' + urlVars.view + '"]:visible');
			if ($viewButton.length == 1) $viewButton.trigger('click');
			else $('.view_button:visible').first().trigger('click');
		} else $('.view_button:visible').first().trigger('click');
	}

	$(".view_button").click(function(e) {
		e.preventDefault();
		if (!$("#view_menu").menu("option", "disabled") && !$(this).is('.ui-state-disabled')) {
			var urlVars = getUrlVars();
			urlVars.view = $(this).attr('data-view-type');
			setUrlVars(urlVars);

			$(".view_button").removeClass("ui-state-active");
			$(this).addClass("ui-state-active");

			drawView();
		}
	});

	var drawNameAlias = function() {
		var questionTitle = questionSettings.name;
		if (!canEdit) questionTitle += ' &mdash; <b>READ ONLY</b>';
		$('#question_title').html(questionTitle);
	}

	var drawView = function() {
		$('.question_details_wrapper').hide();

        if ($('#edit_mode_select').val() == 'smart') {
            $('.view_button[data-view-type="statistics"]').addClass('ui-state-disabled');
            $('#versioning_wrapper').show();
            $('.manual_edit_control').hide();
        } else {
            $('.view_button[data-view-type="statistics"]').removeClass('ui-state-disabled');
            $('#versioning_wrapper').hide();
            $('.manual_edit_control').show();
        }

		var urlVars = getUrlVars();
		if (urlVars.view == 'templates') {
            $('#edit_mode_wrapper').show();
			$('.templates_wrapper').show();
			drawTemplates();
		} else if (urlVars.view == 'variables') {
            $('#edit_mode_wrapper').show();
			$('#wizard_wrapper').show();
			drawVariables();
		} else if (urlVars.view == 'engine') {
            $('#edit_mode_wrapper').hide();
			$('#engine_wrapper').show();
			drawEngine();
		} else if (urlVars.view == 'statistics') {
            $('#edit_mode_wrapper').hide();
			$('#statistics_wrapper').show();
			drawStatistics();
		}
	}

    $('#edit_mode_select').on('change', function() {
        if ($(this).val() == 'smart') {
            $('.view_button[data-view-type="statistics"]').addClass('ui-state-disabled');
            $('.manual_edit_control').hide();
            $('.smart_edit_control').show();
        } else {
            $('.view_button[data-view-type="statistics"]').removeClass('ui-state-disabled');
            $('.smart_edit_control').hide();
            $('.manual_edit_control').show();
        }
        $('#edit_mode_select').blur();
    });

    $('#smart_version_input').on('focus', function() {
        if ($(this).val().length == 0) {
            var offset = $('#smart_version_input').outerHeight() + 5;
            $('#suggestion_list').css({
                'top': offset + "px"
            });
        
            $('#suggestion_list').show();
            $('#suggestion_list').animate({
                opacity: 1.0
            }, 300);    
        }
    });

    $('#smart_version_input').on('blur', function() {
        $('#suggestion_list').css('opacity', 0.0);
        $('#suggestion_list').hide();
    });

    $('#smart_version_input').on('keyup paste change', function() {
        if ($(this).val().length == 0) {
            $('#smart_version_send').addClass('disabled-link');
            if ($('.smart_action:not(.disabled-link)').length == 0) {
                $('#smart_actions_dropdown').attr('disabled', 'disabled');
            }

            $('#suggestion_list').animate({
                opacity: 1.0
            }, 200);    

        } else {
            $('#smart_version_send').removeClass('disabled-link');
            $('#smart_actions_dropdown').removeAttr('disabled');

            $('#suggestion_list').css('opacity', 0.0);
        }
    });

    $('#suggestion_list').on('mousedown', '.suggestion_row', function() {
        $('#smart_version_input').val($(this).text()).trigger('change');
        setTimeout(function() {
            $('#smart_version_input').focus();
        }, 0);
    });

	var drawTemplates = function() {   
		$('#parts_wrapper').show();

		var firstPartIndex;
		if ((questionData.Parts.length > 0) && (questionData.Parts[0].type == 'context')) {
			firstPartIndex = 1;
			$('#jsq_context_template').html(questionData.Parts[0].template);
		} else {
			firstPartIndex =  0;
			$('#jsq_context_template').empty();
		}

		if ('Common' in questionData) {
			$('#jsq_answer_columns').val(questionData.Common.answerColumns);
			$('.context_answer_block').show();

			if (questionData.Common.answerColumns == 'hide') $('.contextAnswers').hide()
			else $('.contextAnswers').show();
		} else {
			$('.context_answer_block').hide();
			$('.contextAnswers').hide();
		}

		$('#parts_menu').html('');
		for (var i = firstPartIndex; i < questionData.Parts.length; ++i) {
			var part = questionData.Parts[i];

			var text = '';
			if (part.type == 'context') text = 'Context: ';
			else if (part.type.substring(0, 2) == 'mc') text = 'MC: ';
			else if (part.type.substring(0, 2) == 'nr') text = 'NR: ';
			else if (part.type.substring(0, 2) == 'wr') text = 'WR: ';

			if (part.template.length > 0) text += get_part_text(part.template);
			else text += '<i>No text</i>';

			var $newLink = '<li><div class="menu_item" data-part-id="' + part.id + '">' +
				'<div class="menu_text">' + text + '</div>';
			if (canEdit) $newLink += '<div title="Delete" class="manual_edit_control menu_button delete_icon"></div>';
			$newLink += '</div></li>';
			$('#parts_menu').append($newLink);
		}
    
		if (canEdit) {
			$('#parts_menu').sortable({
				stop: onPartSortStop
			});
		}

        var questionCount = 0;
        var questionPartID = 0;
        for (var i = 0; i < questionData.Parts.length; ++i) {
            if (questionData.Parts[i].type != 'context') {
                questionPartID = questionData.Parts[i].id;
                questionCount++;
            }
        }

        if (questionCount == 1) {
            $('#parts_menu .menu_item[data-part-id="' + questionPartID + '"] .delete_icon').hide();
        }

		if ('Common' in questionData) {
			$('#jsq_shuffle_context_answers').prop('checked', questionData.Common.shuffleAnswers);
			rebuildContextAnswers($('.contextAnswers'), questionData.Common.answerColumns);
			drawAnswers(questionData.Common.Answers);

			$('#addContext').attr('disabled', 'disabled');
		}

		var urlVars = getUrlVars();

		var $partButton = $('#parts_menu .menu_item').first();
		if ('part' in urlVars) {
			$('#parts_menu .menu_item').each(function() {
				if ($(this).attr('data-part-id') == urlVars.part) $partButton = $(this);
			});
		}
		$partButton.trigger('click');
    }

	async function drawActivePart() {
		$('#question_panel').show();

		// Select question on left

		$('#parts_menu .menu_item').each(function() {
			if ($(this).attr('data-part-id') != activePart.id) $(this).removeClass('active');
			else $(this).addClass('active');
		});

		// Draw question

		if (activePart.type == 'context') $('#part_window_title').html('Additional Context');
		else $('#part_window_title').html('Question');

		// Draw answers

		if ((activePart.type == 'mc_truefalse') || (activePart.type == 'context')) {

			$('#answer_panel').hide();
			$('#rubric_panel').hide();

		} else if (activePart.type == 'wr') {

			$('#answer_panel').hide();
			$('#rubric_panel').show();

            if ('Rubric' in activePart) {
                drawRubric(activePart.Rubric);                
            } else {
                drawRubric({
                    type: 'free',
                    template: ''
                });
            }

		} else {

			$('#answer_panel').show();
			$('#rubric_panel').hide();

			if ('Answers' in activePart) {
				drawAnswers(activePart.Answers);
			}
		}

		if ((activePart.type != 'context') && (activePart.type != 'wr')) {
			rebuildScoredResponses();
		}

		$('#jsq_question_template').html(activePart.template);

		await drawPartOptions(activePart, 'jsq');

		// Finish up

		RenderTools.updateVariableNames($(document), getWizardData());
		RenderTools.renderEquations($(document));

		updateHighlighting();
	}

	var drawRubric = function(data) {
        if (data.type == 'structured') {

            var rubricHTML = '';
            for (var i = 0; i < activePart.TypeDetails.criteria.length; ++i) {
                rubricHTML += 
                    '<div class="rubric_row_wrapper">';
                
                if (activePart.TypeDetails.criteria.length > 1) {
                    rubricHTML += 
                        '<div class="rubric_row_title"> \
                            <div class="rubric_row_reveal"></div><div>' + activePart.TypeDetails.criteria[i].label + '</div> \
                        </div>';
                }
    
                rubricHTML += '<div class="rubric_row_columns" style="display:none;">';
    
                if (activePart.TypeDetails.criteria.length > 1) {
                    rubricHTML += 
                            '<div class="rubric_column_wrapper">';
                    rubricHTML += 
                                '<div class="rubric_column_title">Focus: When marking <i>' + activePart.TypeDetails.criteria[i].label + '</i>, the marker should consider:</div> \
                                <div style="box-sizing:border-box; width:100%; padding:10px; height:auto; background-color:#FFF; color:#555; fill:#555; margin-top:10px; border:1px solid #ccc; border-radius:5px; min-height:42px; height:auto;" class="template_view">' +
                                    data.rows[i].focus + 
                                '</div>';
                    rubricHTML += 
                            '</div>';
                }
    
                var sortedScoring = JSON.parse(JSON.stringify(data.rows[i].scoring));
                sortedScoring.sort(function(a, b) {
                    return (a.value < b.value ? 1 : -1);
                })
                
                for (var j = 0; j < sortedScoring.length; ++j) {
    
                    var marks = sortedScoring[j].value;
                    var marksText = marks.toString() + ' marks';
                    if (marks == 0) marksText = 'No marks';
                    else if (marks == 1) marksText = '1 mark';
    
                    rubricHTML += 
                            '<div class="rubric_column_wrapper">';
                    rubricHTML += 
                                '<div class="rubric_column_title">' + marksText + '</div> \
                                <div style="box-sizing:border-box; width:100%; padding:10px; height:auto; background-color:#FFF; color:#555; fill:#555; margin-top:10px; border:1px solid #ccc; border-radius:5px; min-height:42px; height:auto;" class="template_view">' +
                                    sortedScoring[j].description + 
                                '</div>';
                    rubricHTML += 
                            '</div>';
                }
    
                rubricHTML += 
                        '</div> \
                    </div>';
            }

            $('#rub_grid_wrapper').html(rubricHTML);

            if (activePart.TypeDetails.criteria.length == 1) {
                $('#rub_grid_wrapper .rubric_row_columns').show();
            }

            $('#rub_grid_wrapper').show();
            $('#rub_rubric_template').hide();

        } else {

            $('#rub_rubric_template').html(data.template);

            $('#rub_rubric_template').show();
            $('#rub_grid_wrapper').hide();

        }
	}

	var drawAnswers = function(data) {
		if (data.length == 0) {

			var $newRow = 
				'<tr class="answer_blank"> \
		 			<td colspan="4" style="font-style:italic; border-right:none;">No answers.';
		 	if (canEdit) $newRow += ' Click "Edit" to add answers.';
		 	$newRow += 
		 			'</td> \
		 		</tr>';
			$('#answerlist').html($newRow);

		} else {
						
			$('#answerlist').html("");
			for (var i = 0; i < data.length; ++i) {
				var answer_column = "";
				if ((activePart != null) && (activePart.type == 'mc_table')) {
					var labels = activePart.TypeDetails.labels;
					for (var j = 0; j < labels.length; ++j) {
						answer_column += '<div class="mc_table_row">';
						answer_column += '<div class="mc_table_column mc_label_column mc_label_' + j + '">' + labels[j] + ':</div>';
						answer_column += '<div class="mc_table_column mc_answer_column">' + data[i].templates[j] + '</div>';
						answer_column += '</div>';
					}
				} else {
					answer_column = data[i].template;
				}

				var rowHTML =  '<tr class="answer_row" id="answer_' + data[i].id + '" data-answer-id="' + data[i].id + '">';

				rowHTML += '<td class="answer_label_column"></td>';
		
				rowHTML += '<td class="template" style="max-width:360px; overflow-x:scroll;">' + answer_column + '</td>';
				rowHTML += '<td class="answer_value_column" align="right"></td>';
		
				rowHTML += '</tr>';

				$('#answerlist').append(rowHTML);
			}

			if ((activePart != null) && (activePart.type == 'mc_table')) {
				resizeAnswerTable($('#answerlist'));
			}
		}		
	}
	
	var resizeAnswerTable = function($target) {
		var maxWidth = 0.0;
		$target.find('.mc_label_column').each(function() {
			$(this).css('width', 'auto');
			if ($(this).width() > maxWidth) maxWidth = $(this).width();
		});
		maxWidth += 1;
		var parentWidth = $target.find('.mc_table_row').width();
		$target.find('.mc_label_column').width(maxWidth);
		$target.find('.mc_answer_column').width(parentWidth - maxWidth - 5);
	}
	
	$('#djsq_answer_columns').on('change', function() { 
		var answerColumns = $('#djsq_answer_columns').val();
    	rebuildContextAnswers($('#djsq_answer_wrapper'), answerColumns);
    	if (answerColumns == 'hide') {
	    	$('#djsq_shuffle_context_answers').prop('checked', false);
	    	$('#djsq_shuffle_context_answers').attr('disabled', 'disabled');
			$('#djsq_shuffle_context_answers').next('label').addClass('disabled');
    	} else {
	    	$('#djsq_shuffle_context_answers').removeAttr('disabled');
			$('#djsq_shuffle_context_answers').next('label').removeClass('disabled');
    	}
	});
    
	function rebuildContextAnswers($target, numColumns) {
		if (numColumns == 'hide') {
			$target.empty().hide();
		} else {
			if (questionData.Common.Answers.length == 0) {
				$target.html("<span style='font-style:italic;'>[Answers added in \"Answers\" will show here]</span>");
			} else {
                numColumns = parseInt(numColumns);
				var columnWidth = 100 / numColumns;

				var answerHTML = "";
				var labelCode = "A".charCodeAt(0);
				for (var i = 0; i < questionData.Common.Answers.length; ++i) {
					var answerLabel = String.fromCharCode(labelCode++) + '.'
					var answerTemplate = questionData.Common.Answers[i].template;
					answerHTML += "<div class='question_answer context_answer' style='width:" + columnWidth + "%;'>" + 
						"<div class='answer_label'>" + answerLabel + "</div>" + 
						"<div class='answer_text'>" + answerTemplate + "</div></div>";
				}
				$target.html(answerHTML);
			}
			$target.show();
		}
		
		var targetWidth = $target.width();
		$target.find('img').each(function() {
			var widthPercent = parseFloat($(this)[0].style.width);
			$(this).css('width', (widthPercent * targetWidth / 100) + 'px');
		});
	}
    
    function drawVariables() {
		var tableData = getWizardData();
		
        $("#variable_error_wrapper").hide();

		var $constantsRows = "";
		if (('constants' in tableData) && (tableData.constants.length > 0)) {

			for (var i = 0; i < tableData.constants.length; ++i) {
				$constantsRows += 
					'<tr class="wizard_row">\
                        <td class="wizard_subtype" style="display:none;">constants</td>\
                        <td class="wizard_name"><span class="block_variable" data-variable-name="' + 
							tableData.constants[i].variableName + '">' + tableData.constants[i].displayName + '</span></td>\
						<td class="wizard_value">' + tableData.constants[i].value + '</td>\
						<td>';
				if (canEdit) $constantsRows += 
							'<button title="Edit" class="actions_button edit_icon">&nbsp;</button>' +
							'<button title="Duplicate" class="actions_button copy_icon">&nbsp;</button>' +
							'<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
				$constantsRows += 
						'</td>\
					</tr>';
			}
			
		} else {

			$constantsRows += 
				'<tr class="wizard_blank">\
					<td colspan="3" style="font-style:italic; border-right:none;">No constants.';
			if (canEdit) $constantsRows += ' Click "Add Constant" to create one.';
			$constantsRows += 
					'</td>\
				</tr>';

		}
		$('#constantList').html($constantsRows);
		
		var $randomizedRows = "";
		if (('randomized' in tableData) && (tableData.randomized.length > 0)) {

			for (var i = 0; i < tableData.randomized.length; ++i) {
				if (!('sdType' in tableData.randomized[i])) tableData.randomized[i].sdType = 'SigDigs';

				var rounding;
				if (tableData.randomized[i].sdType == 'Fraction') {
					rounding = 'Fraction';
				} else {
					var sdSuffix;
					if (tableData.randomized[i].sdType == 'Decimals')  sdSuffix = ' decimal';
                    else sdSuffix = ' sig dig';
					if (tableData.randomized[i].sd != 1) sdSuffix += 's';
					rounding = tableData.randomized[i].sd + sdSuffix;
				}

				$randomizedRows += 
					'<tr class="wizard_row">\
						<td class="wizard_subtype" style="display:none;">randomized</td>\
						<td class="wizard_name"><span class="block_variable" data-variable-name="' + 
							tableData.randomized[i].variableName + '">' + tableData.randomized[i].displayName + '</span></td>\
						<td class="wizard_min">' + tableData.randomized[i].min + '</td>\
						<td class="wizard_max">' + tableData.randomized[i].max + '</td>\
						<td class="wizard_step">' + tableData.randomized[i].step + '</td>\
						<td class="wizard_rounding" data-sd="' + tableData.randomized[i].sd + 
                            '" data-sd-type="' + tableData.randomized[i].sdType + '">' + rounding + '</td>\
						<td>';
				if (canEdit) $randomizedRows += 
							'<button title="Edit" class="actions_button edit_icon">&nbsp;</button>' +
							'<button title="Duplicate" class="actions_button copy_icon">&nbsp;</button>' +
							'<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
				$randomizedRows += 
						'</td>\
					</tr>';
			}

		}

		if (('from_list' in tableData) && (tableData.from_list.length > 0)) {

			for (var i = 0; i < tableData.from_list.length; ++i) {
				$randomizedRows += 
					'<tr class="wizard_row">\
						<td class="wizard_subtype" style="display:none;">from_list</td>\
						<td class="wizard_name"><span class="block_variable" data-variable-name="' + 
							tableData.from_list[i].variableName + '">' + tableData.from_list[i].displayName + '</span></td>\
						<td colspan=4>From list {<span class="wizard_list">' + tableData.from_list[i].list.join(',') + '</span>}</td>\
						<td>';
				if (canEdit) $randomizedRows += 
							'<button title="Edit" class="actions_button edit_icon">&nbsp;</button>' +
							'<button title="Duplicate" class="actions_button copy_icon">&nbsp;</button>' +
							'<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
				$randomizedRows += 
						'</td>\
					</tr>';
			}

		}
		
		if ($randomizedRows.length == 0) {

			$randomizedRows += 
				'<tr class="wizard_blank">\
					<td colspan="6" style="font-style:italic; border-right:none;">No randomized variables.';
			if (canEdit) $randomizedRows += ' Click "Add Randomized" to create one.';
			$randomizedRows += 
					'</td>\
				</tr>';

		}
		$('#randomizedList').html($randomizedRows);
		
		var $calculatedRows = "";
		if (('calculated' in tableData) && (tableData.calculated.length > 0)) {

			for (var i = 0; i < tableData.calculated.length; ++i) {
				$calculatedRows += 
					'<tr class="wizard_row">\
						<td class="wizard_subtype" style="display:none;">calculated</td>\
						<td class="wizard_name" style="vertical-align:top;"><span class="block_variable" data-variable-name="' + 
							tableData.calculated[i].variableName + '">' + tableData.calculated[i].displayName + '</span></td>';
						
				if (!('sdType' in tableData.calculated[i])) tableData.calculated[i].sdType = 'SigDigs';

				if (!('round' in tableData.calculated[i])) tableData.calculated[i].round = false;
				
				var rounding;
				if (tableData.calculated[i].sdType == 'Fraction') {
					rounding = 'Fraction';
				} else {
					var sdSuffix;
					if (tableData.calculated[i].sdType == 'Decimals') sdSuffix = ' decimal';
                    else sdSuffix = ' sig dig';
					if (tableData.calculated[i].sd != 1) sdSuffix += 's';
					rounding = tableData.calculated[i].sd + sdSuffix;

                    if (tableData.calculated[i].round) rounding += ", rounded immediately";
				}

                $calculatedRows += "<td>";
                if ('json' in tableData.calculated[i]) {
                    $calculatedRows += "<div class='wizard_equation'>";
                    $calculatedRows += "<div style='margin-bottom:5px;'><b>Equation</b>:</div>";
                    $calculatedRows += "<div class='equation_wrapper'>";
					$calculatedRows += "<div class='equation' style='margin-left:10px;' data-json='" + 
						htmlSingleQuotes(JSON.stringify(tableData.calculated[i].json)) + "'></div>";
                    $calculatedRows += "</div>";
                    $calculatedRows += "</div>";
                    }

				$calculatedRows += '<div class="wizard_rounding" data-sd="' + tableData.calculated[i].sd + '" ' +
                    'data-sd-type="' + tableData.calculated[i].sdType + '" ' +
                    'data-round="' + (tableData.calculated[i].round ? 1 : 0) + '" style="margin-top:10px;">';
                $calculatedRows += "<div><b>Rounding</b>: " + rounding + "</div>";
                $calculatedRows += '</div>';

                $calculatedRows += "<div class='variable_values_wrapper' style='margin-top:5px;'>";
                $calculatedRows += "<b>Estimated range</b>: <span class='variable_range' data-variable-name='" + 
                    tableData.calculated[i].variableName + "'><i>Calculating&hellip;</i></span>";
                $calculatedRows += "</div>";

                $calculatedRows += '</td>';

                $calculatedRows += '<td style="vertical-align:top;">';
				if (canEdit) $calculatedRows += 
							'<button title="Edit" class="actions_button edit_icon">&nbsp;</button>' +
							'<button title="Duplicate" class="actions_button copy_icon">&nbsp;</button>' +
							'<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
				$calculatedRows += '</td>';

                $calculatedRows += '</tr>';
			}
		
		}
		
		if (('conditional' in tableData) && (tableData.conditional.length > 0)) {

			for (var i = 0; i < tableData.conditional.length; ++i) {
				$calculatedRows += 
					'<tr class="wizard_row">\
						<td class="wizard_subtype" style="display:none;">conditional</td>\
						<td class="wizard_name" style="vertical-align:top;"><span class="block_variable" data-variable-name="' + 
							tableData.conditional[i].variableName + '">' + tableData.conditional[i].displayName + '</span></td>';
            
                var rounding;
                if (tableData.conditional[i].sdType == 'Fraction') {
                    rounding = 'Fraction';
                } else {
                    var sdSuffix;
                    if (tableData.conditional[i].sdType == 'Decimals') sdSuffix = ' decimal';
                    else sdSuffix = ' sig dig';
                    if (tableData.conditional[i].sd != 1) sdSuffix += 's';
                    rounding = tableData.conditional[i].sd + sdSuffix;

                    if (tableData.conditional[i].round) rounding += ", rounded immediately";
                }

				$calculatedRows += "<td>";

                $calculatedRows += "<div class='wizard_equation'>";
				$calculatedRows += "<div style='margin:0px 0px 5px;'><b>If&hellip;</b></div>";
                $calculatedRows += "<div class='equation_wrapper'>";
				$calculatedRows += "<div class='equation if_equation' style='display:block; margin-left:10px;' data-json='" + 
					htmlSingleQuotes(JSON.stringify(tableData.conditional[i].if_json)) + "'></div>";
                $calculatedRows += "</div>";
                $calculatedRows += "<div style='margin:10px 0px 5px;'><b>Then&hellip;</b></div>";
                $calculatedRows += "<div class='equation_wrapper'>";
                $calculatedRows += "<div class='equation then_equation' style='display:block; margin-left:10px;' data-json='" + 
					htmlSingleQuotes(JSON.stringify(tableData.conditional[i].then_json)) + "'></div>";
                $calculatedRows += "</div>";
                $calculatedRows += "<div style='margin:10px 0px 5px;'><b>Otherwise&hellip;</b></div>";
                $calculatedRows += "<div class='equation_wrapper'>";
				$calculatedRows += "<div class='equation else_equation' style='display:block; margin-left:10px;' data-json='" + 
					htmlSingleQuotes(JSON.stringify(tableData.conditional[i].else_json)) + "'></div>";
                $calculatedRows += "</div>";
                $calculatedRows += "</div>";
				
				$calculatedRows += '<div class="wizard_rounding" data-sd="' + tableData.conditional[i].sd + '" ' +
                    'data-sd-type="' + tableData.conditional[i].sdType + '" ' +
                    'data-round="' + (tableData.conditional[i].round ? 1 : 0) + '" style="margin-top:10px;">';
                $calculatedRows += "<div><b>Rounding</b>: " + rounding + "</div>";
                $calculatedRows += '</div>';

                $calculatedRows += "<div class='variable_values_wrapper' style='margin-top:5px;'>";
                $calculatedRows += "<b>Estimated range</b>: <span class='variable_range' data-variable-name='" + 
                    tableData.conditional[i].variableName + "'><i>Calculating&hellip;</i></span>";
                $calculatedRows += "</div>";

                $calculatedRows += '</td>';

                $calculatedRows += '<td style="vertical-align:top;">';
				if (canEdit) $calculatedRows += 
							'<button title="Edit" class="actions_button edit_icon">&nbsp;</button>' +
							'<button title="Duplicate" class="actions_button copy_icon">&nbsp;</button>' +
							'<button title="Delete" class="actions_button delete_icon">&nbsp;</button>';
				$calculatedRows += '</td>';

                $calculatedRows += '</tr>';
			}
		
		}

		if ($calculatedRows.length == 0) {

			$calculatedRows += 
				'<tr class="wizard_blank">\
					<td colspan="3" style="font-style:italic; border-right:none;">No calculated variables.';
			if (canEdit) $calculatedRows += ' Click "Add Calculated" to create one.';
			$calculatedRows += 
					'</td>\
				</tr>';

		}
		$('#calculatedList').html($calculatedRows);
		
		var $deletedRows = "";
		if ('deleted' in tableData) {
			for (var i = 0; i < tableData.deleted.length; ++i) {
				$deletedRows += 
					'<tr class="wizard_row">\
                        <td class="wizard_subtype" style="display:none;">deleted</td>\
                        <td class="wizard_name"><span class="block_variable" data-variable-name="' + 
							tableData.deleted[i].variableName + '">' + tableData.deleted[i].variableName + '</span></td>\
					</tr>';
			}
		}
		$('#deletedList').html($deletedRows);

		RenderTools.updateVariableNames($('#calculatedList .wizard_equation'), tableData);
		RenderTools.renderEquations($('#calculatedList'));

        setTimeout(function() {
            RenderTools.sampleVariables(questionData);
        }, 0);
    }

	async function drawStatistics() {
        $('#statistics_item').html('<div class="question_wrapper" data-question-id="' + questionID + '"></div>');

		await RenderTools.setRenderStyle('Numbers')
		await AssessmentsRender.renderItemToDiv(questionData, $('#statistics_item .question_wrapper'));

		$('#statistics_item .part_wrapper[data-type="mc"] .question_answer').css('width', '100%');
		await RenderTools.renderStyles['Numbers'].renumber($('#statistics_item'));

		// Update item statistics

		$('#statistics_n').html('0');

		await StatsCommon.loadFilteredStatistics([questionID]);
		StatsCommon.rebuildItemStatistics(questionID, questionData, 'Numbers');

		// Update item highlighting

		StatsCommon.highlightQuestion($('#statistics_item'), StatsCommon.fullStatistics[questionID], questionData);
	}
    
    $('#startAddConstant').click(function(e) {
		if (!canEdit) return;

		var tableData = getWizardData();
	    $('#wiz_constant_variable_name').val(HTMLEditorUtils.newVariableName(tableData));
	    $('#wiz_constant_display_name').val('');
	    $('#wiz_constant_value').val('');
		$('#editConstant_form').dialog('open');
    });
    
    window.checkVariableName = function(field, rules, i, options) {
		var name = field.val();
		if (name == '_temp') return options.allrules.checkVariableName.alertTextInvalid;

		var $variableNameInput = field.closest('form').find('.variable_name');
		var variable_name = ($variableNameInput.length == 1) ? $variableNameInput.val() : '';
	
		var newName = true;
		var tableData = getWizardData();
		
		if (name == 'e') newName = false;
		if (name == 'π') newName = false;
		for (var type in tableData) {
			if (tableData.hasOwnProperty(type)) {
				for (var i = 0; i < tableData[type].length; ++i) {
					var entry = tableData[type][i];
					if ((variable_name != entry.variableName) && (name == entry.displayName)) newName = false; 
				}
			}
		}

		if (!newName) return options.allrules.checkVariableName.alertTextDuplicate;
	}

	function updateFormatInputs(prefix) {
		var $inputField = $(prefix + 'sd');
		var roundingType = $(prefix + 'sdType').val();

		if ((roundingType == 'SigDigs') || (roundingType == 'Scientific')) {
			$inputField.attr('class', 'validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner');
			$inputField.spinner('option', 'min', 1);
			$(prefix + 'sd_wrapper').show();
			$(prefix + 'round_wrapper').show();

			var label = "sig dig";
			if ($(prefix + 'sd').val() != 1) label += 's';
			$(prefix + 'sdLabel').html(label);
		} else if (roundingType == 'Decimals') {
			$inputField.attr('class', 'validate[required,custom[isNonNegativeInteger]] defaultTextBox2 intSpinner');
			$inputField.spinner('option', 'min', 0);
			$(prefix + 'sd_wrapper').show();
			$(prefix + 'round_wrapper').show();

			var label = "decimal";
			if ($(prefix + 'sd').val() != 1) label += 's';
			$(prefix + 'sdLabel').html(label);
		} else {
			$(prefix + 'sd_wrapper').hide();
			$(prefix + 'round_wrapper').hide();
		}
	}
	    
    function constantSave() {
		var $dialog = $('#editConstant_form');
		var $form = $dialog.find('form');
		
		if ($form.validationEngine('validate')) {
			newQuestionData = JSON.parse(JSON.stringify(questionData));
			newQuestionSettings = false;

			var variableName = $('#wiz_constant_variable_name').val();

			var newEntry = {
				'variableName': variableName,
				'displayName': escapeHTML($('#wiz_constant_display_name').val()),
				'value': $('#wiz_constant_value').val()
			};
			
			var tableData = getWizardData();

			var saveType = 'constants';
			var foundIndex = -1;
			for (var type in tableData) {
				if (tableData.hasOwnProperty(type)) {
					var index = 0;
					while (index < tableData[type].length) {
						if (tableData[type][index].variableName == variableName) {
							if (type == saveType) {
								tableData[type][index] = newEntry;
								foundIndex = index;
								++index;
							} else tableData[type].splice(index, 1);
						} else ++index;
					}
				}
			}

			var name_changes = [];
			if (foundIndex >= 0) {
				if (newEntry.displayName != tableData[saveType][foundIndex].displayName) {
					name_changes.push({
						'variable_name': newEntry.variableName,
						'new_display_name': newEntry.displayName
					});
				}
			} else {
				if (!(saveType in tableData)) tableData[saveType] = [];
				tableData[saveType].push(newEntry);
			}
	
			setWizardData(newQuestionData, tableData);

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			saveQuestionData()
			.then(function() {
				$dialog.dialog("close");
	
				$.gritter.add({
					title: "Success",
					text: "Constant saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				hideSubmitProgress($dialog);
			});
		}
    }

	$('#editConstant_form').dialog({
		autoOpen: false,
		width:375,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
		        $(this).dialog("close"); 
	        },
			'Save': function() {
				constantSave();
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
		 	initSubmitProgress($(this));
		}, close: function(){
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });

	$('#editConstant_form input').on('change', function() {
		window.onbeforeunload = function() {
		    return true;
		};
	});

    $('#startAddRandomized').click(function(e) {
		if (!canEdit) return;

		var tableData = getWizardData();

		$('#wiz_randomized_subtype').val('randomized');
		$('.wiz_randomized_details').hide();
		$('.wiz_randomized_randomized').show();

	    $('#wiz_randomized_variable_name').val(HTMLEditorUtils.newVariableName(tableData));
	    $('#wiz_randomized_display_name').val('');
	    $('#wiz_randomized_min').val('');
	    $('#wiz_randomized_max').val('');
	    $('#wiz_randomized_step').val('');
	    $('#wiz_randomized_sdType').val(userDefaults.Question.Engine.sigDigsType);
	    $('#wiz_randomized_sd').val(userDefaults.Question.Engine.sigDigs);
	    updateFormatInputs('#wiz_randomized_');
	    $('#editRandomized_form .row_index').val(-1);
	    $('#editRandomized_form').dialog('open');

		$('#wiz_randomized_display_name').focus();
    });

    function randomizedSave() {
		var $dialog = $('#editRandomized_form');
		var $form = $dialog.find('form');

		var saveType = $('#wiz_randomized_subtype').val();
		var valueList;

		var isValid = true;
		if (saveType == 'randomized') {

			valueList = false;

		} else if (saveType == 'from_list') {

			valueList = $('#wiz_randomized_list').val().split(',');
			var valueRegex = /^-?(([1-9]\d*|0)(\.\d+)?|-?[1-9](\.\d+)?[Ee][+\-]?\d+)$/;
			for (var i = 0; i < valueList.length; ++i) {
				valueList[i] = valueList[i].trim();
				if (!valueRegex.test(valueList[i])) isValid = false;
			}

			if (!isValid) {
				$('#wiz_randomized_list').validationEngine('showPrompt', '* Must be a comma-separated list of numbers', 
					'error', 'bottomLeft', true);				
			}

		} else isValid = false;

		if (isValid && $form.validationEngine('validate')) {
			newQuestionData = JSON.parse(JSON.stringify(questionData));
			newQuestionSettings = false;

			var variableName = $('#wiz_randomized_variable_name').val();

			var newEntry;
			if (saveType == 'randomized') {

				newEntry = {
					'variableName': $('#wiz_randomized_variable_name').val(),
					'displayName': escapeHTML($('#wiz_randomized_display_name').val()),
					'min': $('#wiz_randomized_min').val(),
					'max': $('#wiz_randomized_max').val(),
					'step': $('#wiz_randomized_step').val(),
					'sd': parseInt($('#wiz_randomized_sd').val()),
					'sdType': $('#wiz_randomized_sdType').val()
				};

			} else {

				newEntry = {
					'variableName': $('#wiz_randomized_variable_name').val(),
					'displayName': escapeHTML($('#wiz_randomized_display_name').val()),
					'list': valueList
				};	

			}
			
			var tableData = getWizardData();

			var foundIndex = -1;
			for (var type in tableData) {
				if (tableData.hasOwnProperty(type)) {
					var index = 0;
					while (index < tableData[type].length) {
						if (tableData[type][index].variableName == variableName) {
							if (type == saveType) {
								tableData[type][index] = newEntry;
								foundIndex = index;
								++index;
							} else tableData[type].splice(index, 1);
						} else ++index;
					}
				}
			}

			var name_changes = [];
			if (foundIndex >= 0) {
				if (newEntry.displayName != tableData[saveType][foundIndex].displayName) {
					name_changes.push({
						'variable_name': newEntry.variableName,
						'new_display_name': newEntry.displayName
					});
				}
			} else {
				if (!(saveType in tableData)) tableData[saveType] = [];
				tableData[saveType].push(newEntry);
			}

			setWizardData(newQuestionData, tableData);
			
			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			saveQuestionData()
			.then(function() {
				$dialog.dialog("close");
	
				$.gritter.add({
					title: "Success",
					text: "Variable saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				hideSubmitProgress($dialog);
			});
		}
	}

	$('#editRandomized_form').dialog({
		autoOpen: false,
		width:375,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
		        $(this).dialog("close"); 
	        },
			'Save': function() {
				randomizedSave();
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
		 	initSubmitProgress($(this));
		}, close: function(){
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });

    $('#wiz_randomized_sdType').on('change', function() {
		updateFormatInputs('#wiz_randomized_');
		$('#wiz_randomized_sd').focus();
    });

	$('#editRandomized_form input').on('change', function() {
		window.onbeforeunload = function() {
		    return true;
		};
	});

	$('#wiz_randomized_subtype').on('change', function() {
		var subType = $('#wiz_randomized_subtype').val();
		$('.wiz_randomized_details').hide();
		$('.wiz_randomized_' + subType).show();

		if (subType == 'randomized') {
			$('#wiz_randomized_min').val('');
			$('#wiz_randomized_max').val('');
			$('#wiz_randomized_step').val('');
			$('#wiz_randomized_sdType').val(userDefaults.Question.Engine.sigDigsType);
			$('#wiz_randomized_sd').val(userDefaults.Question.Engine.sigDigs);
			updateFormatInputs('#wiz_randomized_');	
			$('#wiz_randomized_min').focus();
		} else {
			$('#wiz_randomized_list').val('');
			$('#wiz_randomized_list').focus()
		}
	});

	$('#editRandomized_form .intSpinner').spinner({          
	    stop: function(e, ui) {
			updateFormatInputs('#wiz_randomized_');
			window.onbeforeunload = function() {
			    return true;
			};
	    }
	});

	function setCommonContext(tableData) {
		const common = { 
			variables: JSON.parse(JSON.stringify(tableData)),
			userDefaults: userDefaults
		};

		HtmlEdit.setCommonContext(common);
		EqnEdit.setCommonContext(common);
	}

    $('#startAddCalculated').click(function(e) {
		if (!canEdit) return;

		var tableData = getWizardData();
		setCommonContext(tableData);

		$('#wiz_calculated_subtype').val('calculated');
		$('.wiz_calculated_details').hide();
		$('.wiz_calculated_calculated').show();
		
	    $('#wiz_calculated_variable_name').val(HTMLEditorUtils.newVariableName(tableData));
	    $('#wiz_calculated_display_name').val('');
	    $('#wiz_calculated_sdType').val(userDefaults.Question.Engine.sigDigsType);
	    $('#wiz_calculated_sd').val(userDefaults.Question.Engine.sigDigs);
	    updateFormatInputs('#wiz_calculated_');
	    $('#wiz_calculated_round').prop('checked', false);
	    $('#editCalculated_form').dialog('open');

		$('#wiz_calculated_equation').equationeditor('attachEditor');

		$('#wiz_calculated_display_name').focus();
    
		var hasVariables = false;
		for (var type in tableData) {
			if (tableData.hasOwnProperty(type) && (type != 'constants') && (type != 'deleted')) {
				if (tableData[type].length > 0) hasVariables = true;
			}
		}

		if (!hasVariables) $('.edit_button_variable2').addClass('dead');
    	else $('.edit_button_variable2').removeClass('dead');
    });

    function calculatedSave() {
		var $dialog = $('#editCalculated_form');
		var $form = $dialog.find('form');

		var saveType = $('#wiz_calculated_subtype').val();
		
		var isValid = true;

		if (saveType == 'conditional') {
			var ifData = JSON.parse($('#wiz_conditional_if').equationeditor('getJSON'));

			var ifGood = false;
			if (('operands' in ifData) && ('topLevelContainer' in ifData.operands)) {
				var topLevel = ifData.operands.topLevelContainer;
				for (var i = 0; i < topLevel.length; ++i) {
					if (topLevel[i].type == 'Operator') {
						if (topLevel[i].value == '=') ifGood = true;
						else if (topLevel[i].value == '<') ifGood = true;
						else if (topLevel[i].value == '>') ifGood = true;
					}
				}
			}

			if (!ifGood) {
				isValid = false;
				$('#wiz_conditional_if').parent().focus();
				setTimeout(function() {
					$('#wiz_conditional_if').validationEngine('showPrompt', '* Must be of the form a=b, a&lt;b, or a&gt;b', 'error', 'bottomLeft', true);
				}, 0);
			}
		}

		if (isValid && $form.validationEngine('validate')) {
			newQuestionData = JSON.parse(JSON.stringify(questionData));
			newQuestionSettings = false;

			var variableName = $('#wiz_calculated_variable_name').val();

			var newEntry = {
				'variableName': $('#wiz_calculated_variable_name').val(),
				'displayName': escapeHTML($('#wiz_calculated_display_name').val()),
				'round': $('#wiz_calculated_round').prop('checked'),
				'sd': parseInt($('#wiz_calculated_sd').val()),
				'sdType': $('#wiz_calculated_sdType').val()
			};

			if (saveType == 'calculated') {

				var equationJSON = $('#wiz_calculated_equation').equationeditor('getJSON');
				newEntry.json = JSON.parse(equationJSON);

			} else {

				var equationJSON = $('#wiz_conditional_if').equationeditor('getJSON');
				newEntry.if_json = JSON.parse(equationJSON);

				equationJSON = $('#wiz_conditional_then').equationeditor('getJSON');
				newEntry.then_json = JSON.parse(equationJSON);

				equationJSON = $('#wiz_conditional_else').equationeditor('getJSON');
				newEntry.else_json = JSON.parse(equationJSON);

			}

			var tableData = getWizardData();

			var foundIndex = -1;
			for (var type in tableData) {
				if (tableData.hasOwnProperty(type)) {
					var index = 0;
					while (index < tableData[type].length) {
						if (tableData[type][index].variableName == variableName) {
							if (type == saveType) {
								tableData[type][index] = newEntry;
								foundIndex = index;
								++index;
							} else tableData[type].splice(index, 1);
						} else ++index;
					}
				}
			}

			var name_changes = [];
			if (foundIndex >= 0) {
				if (newEntry.displayName != tableData[saveType][foundIndex].displayName) {
					name_changes.push({
						'variable_name': newEntry.variableName,
						'new_display_name': newEntry.displayName
					});
				}
			} else {
				if (!(saveType in tableData)) tableData[saveType] = [];
				tableData[saveType].push(newEntry);
			}

			setWizardData(newQuestionData, tableData);

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			saveQuestionData()
			.then(function() {
				$dialog.dialog("close");
	
				$.gritter.add({
					title: "Success",
					text: "Variable saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				hideSubmitProgress($dialog);
			});
		}
	}

	$('#editCalculated_form').dialog({
		autoOpen: false,
		width:400,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
		        $(this).dialog("close"); 
	        },
			'Save': function() {
				calculatedSave();
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
		 	initSubmitProgress($(this));
			window.onbeforeunload = function() {
			    return true;
			};
		}, close: function(){
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });
    
    $('#wiz_calculated_sdType').on('change', function() {
	    updateFormatInputs('#wiz_calculated_');
		$('#wiz_calculated_sd').focus();
    });

	$('#wiz_calculated_subtype').on('change', function() {
		var subType = $('#wiz_calculated_subtype').val();
		$('.wiz_calculated_details').hide();
		$('.wiz_calculated_' + subType).show();

		$('#wiz_calculated_sdType').val(userDefaults.Question.Engine.sigDigsType);
		$('#wiz_calculated_sd').val(userDefaults.Question.Engine.sigDigs);
		updateFormatInputs('#wiz_calculated_');
		$('#wiz_calculated_round').prop('checked', false);

		if (subType == 'calculated') {
	
			$('#wiz_calculated_equation').equationeditor('attachEditor');
			$('#wiz_calculated_equation').parent().focus();

		} else {

			$('#wiz_conditional_if').equationeditor('attachEditor');
			$('#wiz_conditional_then').equationeditor('attachEditor');
			$('#wiz_conditional_else').equationeditor('attachEditor');

			$('.wiz_calculated_conditional .variable_equation').parent().blur();
			$('#wiz_conditional_if').parent().focus();
			$(document).trigger('click');

		}
	});
	$('#editCalculated_form .intSpinner').spinner({          
	    stop: function(e, ui) {
			updateFormatInputs('#wiz_calculated_');
	    }
	});

	$('.variables_table').on('click', '.delete_icon', function() {
		newQuestionData = JSON.parse(JSON.stringify(questionData));
		newQuestionSettings = false;

		var $row = $(this).closest('tr');
		var variableName = $row.find('.wizard_name .block_variable').attr('data-variable-name');
		var newDeleted = { variableName: variableName };

		var tableData = getWizardData();

		var type = $(this).closest('tr').find('.wizard_subtype').html();
		var index = 0;
		while (index < tableData[type].length) {
			if (tableData[type][index].variableName == variableName) {
				tableData[type].splice(index, 1);
			} else ++index;
		}

		if (!('deleted' in tableData)) tableData.deleted = [];
		tableData.deleted.push(newDeleted);

		setWizardData(newQuestionData, tableData);

		if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		saveQuestionData()
		.then(function() {
			$.gritter.add({
				title: "Success",
				text: "Variable deleted.",
				image: "/img/success.svg"
			});
		})
		.catch(function() {
		});
	});
	
	$('#constants_table').on('click', '.edit_icon, .copy_icon', function() {
		var $row = $(this).closest('tr');
		
		if ($(this).is('.edit_icon')) {
		    $('#wiz_constant_variable_name').val($row.find('.wizard_name .block_variable').attr('data-variable-name'));
		    $('#wiz_constant_display_name').val($row.find('.wizard_name .block_variable').text());
		} else {
			var tableData = getWizardData();
		    $('#wiz_constant_variable_name').val(HTMLEditorUtils.newVariableName(tableData));
		    $('#wiz_constant_display_name').val('');
		}
	    $('#wiz_constant_value').val($row.find('.wizard_value').html());
	    $('#editConstant_form').dialog('open');
	});

	$('#randomized_table').on('click', '.edit_icon, .copy_icon', function() {
		var $row = $(this).closest('tr');

		var subType = $row.find('.wizard_subtype').html();
		$('#wiz_randomized_subtype').val(subType);
		$('.wiz_randomized_details').hide();
		$('.wiz_randomized_' + subType).show();

		if ($(this).is('.edit_icon')) {
		    $('#wiz_randomized_variable_name').val($row.find('.wizard_name .block_variable').attr('data-variable-name'));
		    $('#wiz_randomized_display_name').val($row.find('.wizard_name .block_variable').text());
		} else {
			var tableData = getWizardData();
		    $('#wiz_randomized_variable_name').val(HTMLEditorUtils.newVariableName(tableData));
		    $('#wiz_randomized_display_name').val('');
		}

		if (subType == 'randomized') {
			$('#wiz_randomized_min').val($row.find('.wizard_min').html());
			$('#wiz_randomized_max').val($row.find('.wizard_max').html());
			$('#wiz_randomized_step').val($row.find('.wizard_step').html());
			$('#wiz_randomized_sd').val($row.find('.wizard_rounding').attr('data-sd'));
			$('#wiz_randomized_sdType').val($row.find('.wizard_rounding').attr('data-sd-type'));
			updateFormatInputs('#wiz_randomized_');	
		} else {
			$('#wiz_randomized_list').val($row.find('.wizard_list').html());
		}

	    $('#editRandomized_form').dialog('open');

		$('#wiz_randomized_display_name').focus();
	});

	$('#calculated_table').on('click', '.edit_icon, .copy_icon', function() {
		var $row = $(this).closest('tr');

		var subType = $row.find('.wizard_subtype').html();
		$('#wiz_calculated_subtype').val(subType);
		$('.wiz_calculated_details').hide();
		$('.wiz_calculated_' + subType).show();

		var tableData = getWizardData();
		setCommonContext(tableData);

		if ($(this).is('.edit_icon')) {
		    $('#wiz_calculated_variable_name').val($row.find('.wizard_name .block_variable').attr('data-variable-name'));
		    $('#wiz_calculated_display_name').val($row.find('.wizard_name .block_variable').text());
		} else {
		    $('#wiz_calculated_variable_name').val(HTMLEditorUtils.newVariableName(tableData));
		    $('#wiz_calculated_display_name').val('');
		}

	    $('#wiz_calculated_round').prop('checked', ($row.find('.wizard_rounding').attr('data-round') == 1));
	    $('#wiz_calculated_sd').val($row.find('.wizard_rounding').attr('data-sd'));
	    $('#wiz_calculated_sdType').val($row.find('.wizard_rounding').attr('data-sd-type'));
	    updateFormatInputs('#wiz_calculated_');
	    $('#editCalculated_form').dialog('open');

		if (subType == 'calculated') {

			var json_data = $row.find('.wizard_equation .equation').attr('data-json');
			$('#wiz_calculated_equation').equationeditor('setJSON', json_data);

		} else {

			var json_data = $row.find('.wizard_equation .if_equation').attr('data-json');
			$('#wiz_conditional_if').equationeditor('setJSON', json_data);

			json_data = $row.find('.wizard_equation .then_equation').attr('data-json');
			$('#wiz_conditional_then').equationeditor('setJSON', json_data);

			json_data = $row.find('.wizard_equation .else_equation').attr('data-json');
			$('#wiz_conditional_else').equationeditor('setJSON', json_data);

			$('.wiz_calculated_conditional .variable_equation').parent().blur();
			$(document).trigger('click');
	
		}

		$('#wiz_calculated_display_name').focus();
		
		var hasVariables = false;
		for (var type in tableData) {
			if (tableData.hasOwnProperty(type) && (type != 'constants') && (type != 'deleted')) {
				if (tableData[type].length > 0) hasVariables = true;
			}
		}

		if (!hasVariables) $('.edit_button_variable2').addClass('dead');
    	else $('.edit_button_variable2').removeClass('dead');
	});

	function drawEngine() {
		if ('js' in questionData.Engine) {
			codeView.setValue(questionData.Engine.js, -1);
		} else codeView.setValue('');

		codeView.resize();

		var $importList = "";
		for (var importID in questionData.Engine.Imports) {
			if (questionData.Engine.Imports.hasOwnProperty(importID)) {

				$importList += 
					'<tr class="import_row" data-id="' + importID + '">' +
						'<td>' + questionData.Engine.Imports[importID].name + '</td>' +
						'<td>' + questionData.Engine.Imports[importID].description + '</td>' +
					'</tr>';

			}
		}
		
		if ($importList == "") {
			$importList = 
				'<tr class="import_blank"> \
					<td colspan="3" style="font-style:italic; border-right:none;">No imports to show here.</td> \
				</tr>';
		}
		
		$('#importList').html($importList);
	}
    
	$('#addPart').click(function(e) {
		var newType = 'mc';
		if ($('#jsq_answer_location').val() != 'context') {
			for (var i = 0; i < questionData.Parts.length; ++i) {
				if (questionData.Parts[i].type != 'context') newType = questionData.Parts[i].type;
			}
			if (newType == 'mc_table') newType = 'mc';
		}

		var $newLink = '<li><div class="menu_item" id="partLink_new">' +
			'<div class="menu_text"><i>No text</i></div>';
		if (canEdit) $newLink += '<div title="Delete" class="manual_edit_control menu_button delete_icon"></div>';
		$newLink += '</div></li>';

		$('#parts_menu').append($newLink);
		
		if (questionData.Parts.length == 1) {
			$('#parts_menu .delete_icon').hide();
		} else $('#parts_menu .delete_icon').show();

		$('#parts_menu .menu_item').removeClass('active');
		$('#parts_menu li:last-child .menu_item').addClass('active');

		var partID = getNewID(4);
		
		editPart = {
			id: partID,
			type: newType,
			template: ''
		};

		initPartType();

		var urlVars = getUrlVars();
		urlVars.part = partID;
		setUrlVars(urlVars);

		openEditQuestion();
	});
	
	$('#addContext').click(function(e) {
		if ((questionData.Parts.length > 0) && (questionData.Parts[0].type == 'context')) {

			var newType = 'context';

			var $newLink = '<li><div class="menu_item" id="partLink_new" data-part-id="new">' + 
				'<div class="menu_text"><i>No text</i></div>';
			if (canEdit) $newLink += '<div title="Delete" class="manual_edit_control menu_button delete_icon"></div>';
			$newLink += '</div></li>';
			
			$('#parts_menu').append($newLink);
			
			if (questionData.Parts.length == 1) {
				$('#parts_menu .delete_icon').hide();
			} else $('#parts_menu .delete_icon').show();
	
			$('#parts_menu .menu_item').removeClass('active');
			$('#parts_menu li:last-child .menu_item').addClass('active');
	
			editPart = {
				id: getNewID(4),
				type: newType,
				template: ''
			};
	
			openEditQuestion();
			
		} else {

			$('#startEditContext').trigger('click');

		}
	});
	
	var rebuildScoredResponses = function() {
		
		var answerRows = $('#answerlist tr');

		var answerLabel = "A".charCodeAt(0);
		answerRows.each(function() {
			var thisID = $(this).attr('data-answer-id');

			var value = 0.0;
			for (var i = 0; i < activePart.ScoredResponses.length; ++i) {
				if (('id' in activePart.ScoredResponses[i]) && (activePart.ScoredResponses[i].id == thisID)) {
					value = activePart.ScoredResponses[i].value;
				}
			}

			$(this).find('.answer_value_column').html(value);
			$(this).find('.answer_label_column').html(String.fromCharCode(answerLabel++) + '.');
		});

		var needsKey = false;
		if (activePart.type.substr(0, 2) == 'mc') {
			for (var i = 0; i < activePart.ScoredResponses.length; ++i) {
				if ('ids' in activePart.ScoredResponses[i]) needsKey = true;
			}
			$('.answer_label_column').show();
		} else $('.answer_label_column').hide();
		
		if (needsKey) {
			
			var answerLabel = "a".charCodeAt(0);
		    var availableResponses = [];
		    var answerMap = {};
		    answerRows.each(function() {
			    var thisID = $(this).attr('data-answer-id');
			    var thisChar = String.fromCharCode(answerLabel++);
			    availableResponses.push(thisChar);
			    answerMap[thisID] = thisChar;
		    });
	
			var new_responses = "";
			for (var j = 0; j < activePart.ScoredResponses.length; ++j) {
				var answerLetters = '';
				if ('id' in activePart.ScoredResponses[j]) {
					answerLetters += answerMap[activePart.ScoredResponses[j].id];
				} else if ('ids' in activePart.ScoredResponses[j]) {
					for (var index = 0; index < activePart.ScoredResponses[j].ids.length; ++index) {
						answerLetters += answerMap[activePart.ScoredResponses[j].ids[index]];						
					}
				}
				var answerValue = activePart.ScoredResponses[j].value;
				
				new_responses += "<tr><td>"
				for (var i = 0; i < availableResponses.length; ++i) {
					var thisLetter = availableResponses[i];
			   		var value = (answerLetters.indexOf(thisLetter.toLowerCase()) == -1) ? '' : 'checked';
					new_responses += "<div class='mcq_detail_response'><label style='text-align:left;'>" +  thisLetter + ".</label><input type='checkbox' disabled " + value + "></div>";
				}			
				new_responses += '</td><td></td>';
				new_responses += '<td>' + answerValue + '</td></tr>';
			}
			$('#scored_responses').html(new_responses);
	
			$('.answer_value_column').hide();
			$('#key_wrapper').show();
		} else {
			$('.answer_value_column').show();
			$('#key_wrapper').hide();
		}
	}
	
    $('#parts_menu').on('click', '.delete_icon', function(e) {
		e.stopPropagation();
		if (!canEdit) return;

		var partID = $(this).closest('.menu_item').attr('data-part-id');

		var partType = false;
		var partIndex = 0;
		for (var i = 0; i < questionData.Parts.length; ++i) {
			if (questionData.Parts[i].type != 'context') partIndex++;
			if (questionData.Parts[i].id == partID) {
				partType = questionData.Parts[i].type;
				break;
			}
		}

		if (partType == 'context') {
			$('#deletePart_text').html("Delete selected additional context?");
			$('#deletePart_dialog').dialog('option', 'title', 'Delete Additional Context');
		} else {
			$('#deletePart_text').html("Delete part " + partIndex + " of this question?");
			$('#deletePart_dialog').dialog('option', 'title', 'Delete Question Part');
		}
		
		$('#deletePart_id').val(partID);
		
		$('#deletePart_dialog').dialog('open');
	});

    var deletePart = function() {
		var $dialog = $(this);
	   	var partID = $('#deletePart_id').val();

		newQuestionData = JSON.parse(JSON.stringify(questionData));
		newQuestionSettings = false;

		var index;
		for (index = 0; index < newQuestionData.Parts.length; ++index) {
			if (newQuestionData.Parts[index].id == partID) {
				newQuestionData.Parts.splice(index, 1);
				break;
			}
		}

		if (partID == activePart.id) {
			activePart = JSON.parse(JSON.stringify(newQuestionData.Parts[0]));
		}

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;
		
		saveQuestionData()
		.then(function() {
			$dialog.dialog("close");

			$.gritter.add({
				title: "Success",
				text: "Item part deleted.",
				image: "/img/success.svg"
			});
		})
		.catch(function() {
			hideSubmitProgress($dialog);
		});
    }

	$('#deletePart_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
		   	'Cancel': function() { $(this).dialog("close"); },
          	'Delete': deletePart
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Delete")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

    $('#parts_menu').on('click', '.menu_item', function(e) {
        var partID = $(this).attr('data-part-id');

		for (var i = 0; i < questionData.Parts.length; ++i) {
			if ((i == 0) || (questionData.Parts[i].id == partID)) {
				activePart = JSON.parse(JSON.stringify(questionData.Parts[i]));
			}
		}

		var urlVars = getUrlVars();
		urlVars.part = partID;
		setUrlVars(urlVars);

		drawActivePart();		
    });
	 
    var onPartSortStop = function(ev, ui) {
		var $parent = ui.item.parent();

		var originalData = JSON.stringify(questionData);
		newQuestionData = JSON.parse(originalData);
		newQuestionSettings = false;

		var contextPart = false;
		if (newQuestionData.Parts[0].type == 'context') {
			contextPart = newQuestionData.Parts[0];
			newQuestionData.Parts.splice(0, 1);
		}

		var questionParts = {};
		for (var i = 0; i < newQuestionData.Parts.length; ++i) {
			var thisID = newQuestionData.Parts[i].id;
			questionParts[thisID] = newQuestionData.Parts[i];
		}

		if (contextPart == false) newQuestionData.Parts = [];
		else newQuestionData.Parts = [contextPart];

        var foundAll = true;
        $parent.find('.menu_item').each(function() {
			var thisID = $(this).attr('data-part-id');
			if (thisID in questionParts) {
				newQuestionData.Parts.push(questionParts[thisID]);
			} else foundAll = false;
        });

		if (foundAll) {

			var newData = JSON.stringify(newQuestionData);
			if (newData != originalData) {
	
				if (pendingAJAXPost) return false;
				pendingAJAXPost = true;
				
				saveQuestionData()
				.then(function() {
					$.gritter.add({
						title: "Success",
						text: "Item parts reordered.",
						image: "/img/success.svg"
					});
				})
				.catch(function() {
					$.gritter.add({
						title: "Error",
						text: "Unable to reorder parts.",
						image: "/img/error.svg"
					});			
				});
			}
			
		} else {

			$.gritter.add({
				title: "Error",
				text: "Unable to reorder parts.",
				image: "/img/error.svg"
			});
		}
    }

    /* *
     * * Editing rubric template
     * */

	var isEmptyHTML = function(html) {

		// Check for an empty rubric

		html = '<div>' + html + '</div>';                                  // Get enclosed HTML
		html = html.replace(/<br\s*\/?>|&nbsp;/g, '');                     // Remove whitespace elements
		html = html.replace(/([^<]*)(<[^>]*>)?/g, function(match, a, b) {  // Remove other whitespace outside tags
			if (typeof b == 'undefined') b = '';
			return a.replace(/<br\s*\/?>|\s|&nbsp;/g, '') + b;
		});

		return isEmptyObject($(html));
	}

	var isEmptyObject = function($object) {
		if ($object[0].nodeType == 3) return false;
		else if (!$object.is('div, span')) return false;
		else if ($object.is('.block_variable, .block_bigBlank, .equation, .html_drawing')) return false;
		else {
			var isEmpty = true;
			$object.contents().each(function() {
				isEmpty &= isEmptyObject($(this));
			});
			return isEmpty;
		}
	}

    var rubricSave = function() {
		var $dialog = $('#editRubric_form');
		var $form = $dialog.find('form');

        var index;
        for (index = 0; index < questionData.Parts.length; ++index) {
            if (questionData.Parts[index].id == activePart.id) break;
        }

        if (index < questionData.Parts.length) {
            var thisPart = questionData.Parts[index];

            var isValid = true;

            if ($('#drub_type').val() == 'structured') {
                for (var i = 0; i < thisPart.TypeDetails.criteria.length; ++i) {
                    var $thisRow = $('#rubric_grid_wrapper .rubric_row_wrapper').eq(i);

                    var rowData = {}
                    if (thisPart.TypeDetails.criteria.length > 1) {
                        var $focusInput = $thisRow.find('.rubric_focus');
                        var focusValue = $focusInput.htmleditor('getValue');
                        if (isEmptyHTML(focusValue)) {
                            $thisRow.find('.rubric_row_columns').show();
                            $thisRow.find('.rubric_row_reveal').addClass('selected');

                            $focusInput.validationEngine('showPrompt', "* Focus must not be blank.", 
                                'error', 'bottomLeft', true);
                            isValid = false;
                            break;
                        }
                    }

                    var score = thisPart.TypeDetails.criteria[i].value;
                    var $topInput = $thisRow.find('.rubric_marks[data-value="' + score + '"]');
                    var topValue = $topInput.htmleditor('getValue');
                    if (isEmptyHTML(topValue)) {
                        $thisRow.find('.rubric_row_columns').show();
                        $thisRow.find('.rubric_row_reveal').addClass('selected');

                        $topInput.validationEngine('showPrompt', "* Description for top value must not be blank.", 
                            'error', 'bottomLeft', true);
                        isValid = false; 
                        break;
                    }
                }
            }

            if (isValid && $form.validationEngine('validate')) {

                if ($('#drub_type').val() == 'free') {
                    
                    // Update active part

                    var rubricHTML = $('#drub_rubric_edit').htmleditor('getValue');

                    if (!isEmptyHTML(rubricHTML)) {
                        thisPart.Rubric = { 
                            type: 'free',
                            template: rubricHTML 
                        };    
                    } else delete thisPart.Rubric;

                } else if ($('#drub_type').val() == 'structured') {

                    thisPart.Rubric = {
                        type: 'structured',
                        rows: []
                    };

                    for (var i = 0; i < thisPart.TypeDetails.criteria.length; ++i) {
                        var $thisRow = $('#rubric_grid_wrapper .rubric_row_wrapper').eq(i);

                        var rowData = {}
                        if (thisPart.TypeDetails.criteria.length > 1) {
                            rowData.focus = $thisRow.find('.rubric_focus').htmleditor('getValue');
                        }
                        rowData.scoring = [];
                        
                        for (var j = 0; j <= thisPart.TypeDetails.criteria[i].value; ++j) {
                            var marksHTML = $thisRow.find('.rubric_marks[data-value="' + j + '"]').htmleditor('getValue');
                            if (!isEmptyHTML(marksHTML)) {
                                rowData.scoring.push({
                                    value: j,
                                    description: marksHTML
                                });
                            }
                        }
                        thisPart.Rubric.rows.push(rowData);
                    }
                }
	
                newQuestionData = JSON.parse(JSON.stringify(questionData));
                newQuestionSettings = false;

				// Save variables
	
				setWizardData(newQuestionData, HtmlEdit.getEditorVariables());
					
				// Save question data

				if (pendingAJAXPost) return false;
				showSubmitProgress($dialog);
				pendingAJAXPost = true;
				
				saveQuestionData()
				.then(function() {
					$dialog.dialog("close");
		
					$.gritter.add({
						title: "Success",
						text: "Rubric saved.",
						image: "/img/success.svg"
					});
				})
				.catch(function() {
					hideSubmitProgress($dialog);
				});
			} else showSubmitError($dialog);
		}
    }
         
	$('#openEditRubric').click(function(e) {
		if (!canEdit) return;

		e.preventDefault();

		const tableData = getWizardData();
		setCommonContext(tableData);

        var freeValue = false;
        var rubricRows = false;

        if (('Rubric' in activePart) && (activePart.Rubric.type == 'structured')) {

            $('#drub_type').val('structured');

            freeValue = '';

            rubricRows = activePart.Rubric.rows;

        } else {

            $('#drub_type').val('free');

            if (('Rubric' in activePart) && ('template' in activePart.Rubric)) {
                freeValue = activePart.Rubric.template;
            } else freeValue = '';

            rubricRows = [];
            if (activePart.TypeDetails.criteria.length == 1) {

                rubricRows.push({scoring: []});

            } else {

                for (var i = 0; i < activePart.TypeDetails.criteria.length; ++i) {
                    rubricRows.push({focus: '', scoring: []});
                }

            }

        }

        // Initialize free rubric

        $('#drub_rubric_edit').htmleditor('setValue', freeValue);
        
        // Initialize structured rubric

        var rubricHTML = '';
        for (var i = 0; i < activePart.TypeDetails.criteria.length; ++i) {
            rubricHTML += 
                '<div class="rubric_row_wrapper">';
            
            if (activePart.TypeDetails.criteria.length > 1) {
                rubricHTML += 
                    '<div class="rubric_row_title"> \
                        <div class="rubric_row_reveal"></div><div>' + activePart.TypeDetails.criteria[i].label + '</div> \
                    </div>';
            }

            rubricHTML += '<div class="rubric_row_columns">';

            if (activePart.TypeDetails.criteria.length > 1) {
                rubricHTML += 
                        '<div class="rubric_column_wrapper">';
                rubricHTML += 
                            '<div class="rubric_column_title">Focus: When marking <i>' + activePart.TypeDetails.criteria[i].label + '</i>, the marker should consider:</div> \
                            <div style="height:auto;" class="rubric_html rubric_focus template_view"></div>';
                rubricHTML += 
                        '</div>';
            }

            for (var j = activePart.TypeDetails.criteria[i].value; j >= 0; --j) {

                var marksText = j.toString() + ' marks';
                if (j == 0) marksText = 'No marks';
                else if (j == 1) marksText = '1 mark';

                rubricHTML += 
                        '<div class="rubric_column_wrapper">';
                rubricHTML += 
                            '<div class="rubric_column_title">' + marksText + '</div> \
                            <div style="height:auto;" class="rubric_html rubric_marks template_view" data-value="' + j + '"></div>';
                rubricHTML += 
                        '</div>';
            }

            rubricHTML += 
                    '</div> \
                </div>';
        }
        $('#rubric_grid_wrapper').html(rubricHTML);

        if (activePart.TypeDetails.criteria.length > 1) {
            $('.rubric_row_columns').hide(); 
        }

        $('.rubric_html').each(function() {
            $(this).htmleditor({
                height: 'auto',
                defaultVariableType: 'calculated'
            });
    
            $(this).htmleditor('hideElement', 'edit_bar_section_alignment');
            $(this).htmleditor('hideElement', 'edit_button_blank');
            $(this).htmleditor('hideElement', 'edit_button_table');
            $(this).htmleditor('hideElement', 'edit_button_image');                
            $(this).htmleditor('makeInactive');
        });

        for (var i = 0; i < activePart.TypeDetails.criteria.length; ++i) {
            var $thisRow = $('#rubric_grid_wrapper .rubric_row_wrapper').eq(i);
            if ('focus' in rubricRows[i]) {
                $thisRow.find('.rubric_focus').htmleditor('setValue', rubricRows[i].focus);
            }
            for (var j = 0; j < rubricRows[i].scoring.length; ++j) {
                var columnIndex = rubricRows[i].scoring[j].value;
                var $thisColumn = $thisRow.find('.rubric_marks[data-value="' + columnIndex + '"]');
                $thisColumn.htmleditor('setValue', rubricRows[i].scoring[j].description);
            }
        }

        // Open dialog

		$('#editRubric_form').dialog('open');
        showRubric();
    });

    var showRubric = function() {
        if ($('#drub_type').val() == 'structured') {

            $('.rubric_free').hide();
            $('.rubric_structured').show();
            $('#drub_type').val('structured');

        } else {

            $('.rubric_structured').hide();
            $('.rubric_free').show();
            $('#drub_type').val('free');

        }
    }

    $('#drub_type').on('change', function() {
        showRubric();
    });

    $('#rubric_grid_wrapper').on('keydown', '.rubric_html', function(e) {
		if (e.key === 'Tab') {
            e.preventDefault();

            var $rubricFields = $('#rubric_grid_wrapper').find('.rubric_html');
            var thisIndex = $rubricFields.index($(this));
            if (thisIndex != $rubricFields.length - 1) {
                var $nextField = $rubricFields.eq(thisIndex + 1);

                var $thisRow = $nextField.closest('.rubric_row_wrapper');
                $thisRow.find('.rubric_row_reveal').addClass('selected');
                $thisRow.find('.rubric_row_columns').show();

                $nextField.focus();
            }
        }
    });

    $(document).on('click', '.rubric_row_reveal', function(e) {
        e.stopPropagation();

		var $thisRow = $(this).closest('.rubric_row_wrapper');

		if ($(this).is('.selected')) {
			$(this).removeClass('selected');
			$thisRow.find('.rubric_row_columns').hide();
		} else {
			$(this).addClass('selected');
			$thisRow.find('.rubric_row_columns').show();
		}
	});

	var contextSave = function() {
		var $dialog = $('#editContext_form');
		var $form = $dialog.find('form');

		if ($form.validationEngine('validate')) {
			newQuestionData = JSON.parse(JSON.stringify(questionData));
			newQuestionSettings = false;

			var newContext = $('#djsq_context_edit').htmleditor('getValue');
			if (!isEmptyHTML(newContext)) {
				if ((newQuestionData.Parts.length > 0) && (newQuestionData.Parts[0].type == 'context')) {
					newQuestionData.Parts[0].template = newContext;
				} else {
					newQuestionData.Parts.unshift({
						id: getNewID(4),
						type: 'context',
						template: newContext
					});
				}
			} else {
				if ((newQuestionData.Parts.length > 0) && (newQuestionData.Parts[0].type == 'context')) {
					newQuestionData.Parts.splice(0, 1);
				}
			}

			if ('Common' in newQuestionData) {
                if ($('#djsq_answer_columns').val() == 'hide') newQuestionData.Common.answerColumns = 'hide';
                else newQuestionData.Common.answerColumns = parseInt($('#djsq_answer_columns').val());

                newQuestionData.Common.shuffleAnswers = $('#djsq_shuffle_context_answers').prop('checked');
			}

			// Save variables

			setWizardData(newQuestionData, HtmlEdit.getEditorVariables());

			// Finish saving data

			showSubmitProgress($dialog);
			
			saveQuestionData()
			.then(function() {
				$dialog.dialog("close");
	
				$.gritter.add({
					title: "Success",
					text: "Context saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				hideSubmitProgress($dialog);
			});
		}
	}
	
	var partSave = function() {
		var $dialog = $('#editQuestion_form');
		var $form = $dialog.find('form');

		rebuildTypeDetails();

		var hasError = false;
		if (editPart.type == 'mc_table') {
			var typeDetails = editPart.TypeDetails;

			var labelsGood = true;
			$('#djsq_question_edit .block_bigBlank').each(function() {
				var thisLabel = $(this).html();
				if ((thisLabel.length > 0) && ($.inArray(thisLabel, typeDetails.labels) == -1)) labelsGood = false;
			});
			if (!labelsGood) {
				$('#djsq_question_edit').validationEngine('showPrompt', "* Invalid label used in text", 
					'error', 'topLeft', true);
				hasError = true;
			}
			
			var labelsFilled = true;
			for (var i = 0; i < typeDetails.labels.length; ++i) {
				if (typeDetails.labels[i].length == 0) labelsFilled = false;
			}
			if (!labelsFilled) {
				$('#djsq_label_wrapper').validationEngine('showPrompt', "* Labels cannot be empty", 
					'error', 'bottomLeft', true);
				hasError = true;
			}
		} else if (editPart.type == 'wr') {
			var typeDetails = editPart.TypeDetails;

            if ($('#djsq_wr_height').val() < 0.5) {
                $('#djsq_wr_height').validationEngine('showPrompt', "* Must be at least 0.5", 'error', 'bottomLeft', true);
                hasError = true;
            } 

			if ($('#djsq_promptType').val() == 'Image') {
				var imageHeight = 0.0;
				if (('prompt' in typeDetails) && ('image' in typeDetails.prompt)) {
					imageHeight = typeDetails.prompt.height;
				}
				
				if (imageHeight > $('#djsq_wr_height').val()) {
					$('#djsq_wr_height').validationEngine('showPrompt', "* Must not be less than image height", 'error', 'bottomLeft', true);
					hasError = true;
				} 
			}
		}

		if (!hasError && $form.validationEngine('validate')) {
			newQuestionData = JSON.parse(JSON.stringify(questionData));
			newQuestionSettings = false;

			var index = 0;
			for (index = 0; index < newQuestionData.Parts.length; ++index) {
				if (newQuestionData.Parts[index].id == editPart.id) break;
			}

			var resetAnswers = false;
			if (index < newQuestionData.Parts.length) {
				var oldPart = newQuestionData.Parts[index];
				if (oldPart.type != editPart.type) resetAnswers = true;
				else if (editPart.type == 'mc_table') {
					if (editPart.TypeDetails.labels.length != oldPart.TypeDetails.labels.length) {
						resetAnswers = true;
					}
				}
			} else resetAnswers = true;

			$dialog.attr('data-needs-reset', resetAnswers ? 1 : 0);

			var needsConfirmation = resetAnswers;
			if ('Answers' in editPart) {

				if (editPart.Answers.length == 0) needsConfirmation = false;

			} else if ('Rubric' in editPart) {

                var emptyRubric = true;
                if (editPart.Rubric.type == 'free') {
                    emptyRubric = (editPart.Rubric.template.length == 0);
                } else if (editPart.Rubric.type == 'structured') {
                    for (var i = 0; i < editPart.Rubric.rows.length; ++i) {
                        if (editPart.Rubric.rows[i].scoring.length > 0) emptyRubric = false;
                    }
                }
                if (emptyRubric) needsConfirmation = false;

			} else needsConfirmation = false;

			if (needsConfirmation) {
				$('#confirmation_dialog').dialog("open");
			} else finishPartSave();
		}
	}

	var finishPartSave = function() {
		var $dialog = $('#editQuestion_form');

		// Update question data

		if ($dialog.attr('data-needs-reset') == 1) {
			if (editPart.type == 'wr') {
				delete editPart.Answers;
				delete editPart.ScoredResponses;
			} else {
				delete editPart.Rubric;
                if (editPart.type != 'context') {
                    if (!('Common' in newQuestionData)) editPart.Answers = [];
                    editPart.ScoredResponses = [];
                }
            }
		}

		editPart.template = $('#djsq_question_edit').htmleditor('getValue');
	
		if (editPart.type == 'mc_truefalse') {

			delete editPart.TypeDetails;

			var answerValue = $('#djsq_tfAnswer').val();

			editPart.Answers = [
				{ id: getNewID(4), template: 'True' },
				{ id: getNewID(4), template: 'False' }
			];

			var answerID = false;
			for (var i = 0; i < editPart.Answers.length; ++i) {
				if (editPart.Answers[i].template == answerValue) {
					answerID = editPart.Answers[i].id;
				}
			}

			if (answerID !== false) {
				editPart.ScoredResponses = [
					{ id: answerID, value: 1 }
				];
			}

		} else if ((editPart.type.substr(0, 2) == 'mc') && (!('Common' in newQuestionData))) {

			editPart.TypeDetails.shuffleAnswers = $('#djsq_shuffle_answers').prop('checked');

		}

		// Save variables

		setWizardData(newQuestionData, HtmlEdit.getEditorVariables());

		// Finish saving
		
		activePart = editPart;

		var index;
		for (index = 0; index < newQuestionData.Parts.length; ++index) {
			if (newQuestionData.Parts[index].id == activePart.id) {
				newQuestionData.Parts[index] = activePart;
				break;
			}
		}
		if (index == newQuestionData.Parts.length) {
			newQuestionData.Parts.push(activePart);
		}

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		saveQuestionData()
		.then(function() {
			$dialog.dialog("close");

			$.gritter.add({
				title: "Success",
				text: "Item part saved.",
				image: "/img/success.svg"
			});
		})
		.catch(function() {
			hideSubmitProgress($dialog);
		});
	}

	function finishQuestionSave() {
		var pathParts = window.location.pathname.split('/');
		var newParts = pathParts.slice(0, -1);
		newParts.push(questionID);
		var newPath = newParts.join('/');

		var urlVars = getUrlVars();
		if (activePart !== false) urlVars.part = activePart.id;
		window.history.replaceState({}, '', newPath);
		setUrlVars(urlVars);

		if (questionData.Engine.type == 'code') {
			if (canWriteCode) $('.view_button[data-view-type="engine"]').show();
			else $('.view_button[data-view-type="engine"]').hide();
			$('.view_button[data-view-type="variables"]').hide();
		} else {
			$('.view_button[data-view-type="engine"]').hide();
			$('.view_button[data-view-type="variables"]').show();
		}

		drawNameAlias();
		drawView();
		
        $('#smart_version_send').removeAttr('disabled');
        $('#versioning_wrapper .wait_icon').css('opacity', 0);
        
		if ((window.opener != null) && (typeof window.opener.editQuestionCallback === "function")) {
			try {

                var callbackData = {
                    old_question_handle_id: oldQuestionHandleID,
                    old_question_id: oldQuestionID,
                    new_question_handle_id: questionHandleID,
                    new_question_id: questionID
                }
                if (assessmentVersionID !== false) {
                    callbackData.assessment_version_id = assessmentVersionID;
                }

                window.opener.editQuestionCallback(callbackData);
                    
			} catch(e) {
				console.log("Failed question callback.");
			}
		}
	}

	$('#saveType_dialog').dialog({
        autoOpen: false,
		width: 450,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
	        $(this).find('button').blur();
		}, close: function() {
            $('form').validationEngine('hideAll');
		}
	});		

	$('#targetFolder_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Save")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});		

	function saveQuestionData() {
		if ((newQuestionData !== false) || (newQuestionSettings !== false)) {

			showSettings = 0;
			canCancel = 0;
	
			// Save question
	
            oldQuestionHandleID = questionHandleID;
            oldQuestionID = questionID;

            var urlVars = getUrlVars();
			if (('a' in urlVars) && (assessmentItemCount > 1) && (newQuestionData !== false)) {
	
                assessmentVersionID = false;

				return getSaveType()
				.then(function(saveType) {
					if (saveType == 'modify') {
						return sendQuestionData();
					} else if (saveType == 'version') {
						return getTarget().then(saveToVersion);
					} else return Promise.reject();
				});
	
			} else return sendQuestionData();
			
		} else {

			$.gritter.add({
				title: "Error",
				text: "New question data and settings not set.",
				image: "/img/error.svg"
			});

			return Promise.reject();
		}
	}

	function getSaveType() {
		return new Promise((resolve, reject) => {
			$('#saveType_modify').off('click');
			$('#saveType_modify').on('click', function() {
				$('#saveType_dialog').dialog('close');
				resolve('modify');
			});
		
			$('#saveType_version').off('click');
			$('#saveType_version').on('click', function() {
				$('#saveType_dialog').dialog('close');
				resolve('version');
			});
		
			$('#saveType_cancel').off('click');
			$('#saveType_cancel').on('click', function() {
				$('#saveType_dialog').dialog('close');

				$.gritter.add({
					title: "Error",
					text: "Save cancelled.",
					image: "/img/error.svg"
				});

				reject();
			});
				
			$('#assessment_item_count').text(assessmentItemCount);
			$('#saveType_dialog').dialog('open');

		});
	}

	$('#version_conflict_dialog').dialog({
		autoOpen: false,
		width:400,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Go back")').addClass('btn btn_font');
			$buttonPane.find('button:contains("Reload")').addClass('btn btn-primary btn_font');
		}
    });

	function sendQuestionData() {
		return new Promise((resolve, reject) => {
			var postData = {
				question_handle_id: questionHandleID,
				version_id: questionVersionID
			};

			if (newQuestionData !== false) {
				var data_string = JSON.stringify(newQuestionData);
				postData.data_string = data_string;
				postData.data_hash = SparkMD5.hash(data_string);
			}

			if (newQuestionSettings !== false) {
				postData.settings = newQuestionSettings;
			}

			var urlVars = getUrlVars();
			if ('a' in urlVars) {
				delete urlVars.a;
				urlVars.h = questionHandleID;
				setUrlVars(urlVars);	
			}

			ajaxFetch('/Questions/save/' + questionID, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(postData)
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();

				if (responseData.success) {

					if (newQuestionData !== false) {
						questionData = newQuestionData;
						newQuestionData = false;

                        if ('engine_js' in responseData) {
                            questionData['Engine']['js'] = responseData.engine_js;
                        }

                        versionData = [JSON.stringify(questionData)];
                        versionIndex = 0;
                        $('#smart_version_undo').attr('disabled', 'disabled');
                        $('#smart_version_redo').attr('disabled', 'disabled');
                    }
		
					if (newQuestionSettings !== false) {
						questionSettings = newQuestionSettings;
						newQuestionSettings = false;
					}
		
					questionID = responseData.id;
					questionVersionID = responseData.version_id;

					if ('statistics' in responseData) {
						StatsCommon.fullStatistics[questionID] = responseData.Statistics;
					}

					finishQuestionSave();
					resolve();

				} else if (('error_type' in responseData) && (responseData.error_type == 'version_conflict')) {

					$('#version_conflict_dialog').dialog(
						'option', 
						'buttons', {
							'Go back': function() {
								$(this).dialog("close");
								reject();
							},
							'Reload': function() {
								window.onbeforeunload = null;
								window.location.reload();
							}
						}
					);
		
					$('#version_conflict_dialog').dialog('open');

				} else {

					$.gritter.add({
						title: "Error",
						text: "There was a problem processing your request.",
						image: "/img/error.svg"
					});
					reject();
				}
			}).catch(function() {
				$.gritter.add({
					title: "Error",
					text: "There was a problem processing your request.",
					image: "/img/error.svg"
				});
				
				reject();
			});
		});
	}

	function getTarget() {
		return new Promise((resolve, reject) => {
			$('#targetFolder_dialog').dialog(
				'option', 
				'buttons', {
					'Cancel': function() {
						$(this).dialog("close");

						$.gritter.add({
							title: "Error",
							text: "Save cancelled.",
							image: "/img/error.svg"
						});
		
						reject();
					},
					'Save': function() {
						var json = Folders.getSelectedNode("targetFolderParent");
						resolve({
							save_name: $('#target_name').val(), 
							folder_id: json.data.folder_id
						});
					}
				}
			);

			$('#target_name').val(questionSettings.name);
			$('#targetFolder_dialog').dialog('open');
		});
	}

	function saveToVersion(data) {
		$('#targetFolder_dialog').dialog('close');

		var cleanedQuestionData = JSON.parse(JSON.stringify(newQuestionData));

		var importIDs = [];
		for (var importID in cleanedQuestionData.Engine.Imports) {
			if (cleanedQuestionData.Engine.Imports.hasOwnProperty(importID)) {
				importIDs.push(parseInt(importID));
			}
		}
		cleanedQuestionData.Engine.Imports = importIDs;

		return ajaxFetch('/Questions/create', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				parent_id: questionID,
				folder_id: data.folder_id,
				name: data.save_name,
				json_data: JSON.stringify(cleanedQuestionData)
			})
		}).then(async response => {
			const responseData = await response.json();
			
			if (responseData.success) {
				questionSettings.name = data.save_name;

				questionID = responseData.id;
				questionHandleID = responseData.question_handle_id;
				questionVersionID = responseData.version_id;

				if ('Statistics' in responseData) {
					StatsCommon.fullStatistics[questionID] = responseData.Statistics;
				}

				var urlVars = getUrlVars();
				var assessmentID = parseInt(urlVars.a);

				delete urlVars.a;
				urlVars.h = questionHandleID;
				setUrlVars(urlVars);

				return ajaxFetch('/Assessments/replace_handle/' + assessmentID, {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						old_question_handle_id: oldQuestionHandleID,
						new_question_handle_id: questionHandleID,
					})
				});

			} else {

				throw new Error('Unable to save question');

			}

		}).then(async response => {
			const responseData = await response.json();

			if (responseData.success) {

				assessmentVersionID = responseData.assessment_version_id;

				if (newQuestionData !== false) {
					questionData = newQuestionData;
					newQuestionData = false;

					versionData = [JSON.stringify(questionData)];
					versionIndex = 0;
					$('#smart_version_undo').attr('disabled', 'disabled');
					$('#smart_version_redo').attr('disabled', 'disabled');
				}
	
				if (newQuestionSettings !== false) {
					questionSettings = newQuestionSettings;
					newQuestionSettings = false;
				}
	
				finishQuestionSave();
				return;

			} else {

				throw new Error('Unable to update assessment template');

			}

		});
	}
	
	async function drawPartOptions(part, prefix) {
		var typeDetails = ('TypeDetails' in part) ? part.TypeDetails : {};

		var type = part.type;
		var style = type.substr(0, 2);

		$('#' + prefix + '_options_wrapper .type_block').hide();
		if (type == 'context') {

			var type_options =
				"<option value='context'>Context</option>";
			$('#' + prefix + '_type').html(type_options);

			style = '';

		} else if (style == 'mc') {

			var type_options = 
				"<option value='mc'>Default (a,b,c,...)</option>" +
				"<option value='mc_table'>Response table</option>" +
				"<option value='mc_truefalse'>True / false</option>";

			$('#' + prefix + '_type').html(type_options);

			// Set up MC fields

			$('#' + prefix + '_shuffle_answers').prop('checked', typeDetails.shuffleAnswers);
			if (type == 'mc_table') {
				if (prefix == 'jsq') {
					var labels = typeDetails.labels;
					$('#' + prefix + '_label_wrapper').html(labels.join(', '));	
				} else rebuildLabelLines();

				if ('prompt' in typeDetails) {
					$('#' + prefix + '_show_prompt_mc').prop('checked', true);
				} else $('#' + prefix + '_show_prompt_mc').prop('checked', false);
			} else if (type == 'mc_truefalse') {
				if (('ScoredResponses' in part) && (part.ScoredResponses.length == 1)) {
					var answerID = part.ScoredResponses[0].id;

					var tfAnswer = false;
					for (var i = 0; i < part.Answers.length; ++i) {
						if (part.Answers[i].id == answerID) {
							tfAnswer = part.Answers[i].template;
						}
					}
					$('#' + prefix + '_tfAnswer').val(tfAnswer);
				} else $('#' + prefix + '_tfAnswer').val('True');
			}

			// Show and hide as required

			$('#' + prefix + '_options_wrapper .mc_block').show();
			if ('Common' in questionData) {
				$('#' + prefix + '_style').attr('disabled', 'disabled');
				$('#' + prefix + '_type').hide();

				$('#' + prefix + '_options_wrapper .shuffle_wrapper').hide();
				$('.context_answer_block').show();

				if (questionData.Common.answerColumns == 'hide') $('.contextAnswers').hide()
				else $('.contextAnswers').show();
			} else {
				if (prefix == 'djsq') {
					$('#' + prefix + '_style').removeAttr('disabled');
				}
				$('#' + prefix + '_type').show();

				$('.context_answer_block').hide();
				$('.contextAnswers').hide();
			}

			if (type == 'mc_table') {
				$('#' + prefix + '_options_wrapper .mc_table_block').show();
			} else if (type == 'mc_truefalse') {
				$('#' + prefix + '_options_wrapper .shuffle_wrapper').hide();
				$('#' + prefix + '_mc_truefalse_block').show();
			}

		} else if (style == 'nr') {

			var type_options = 
				"<option value='nr_decimal'>Decimal</option>" +
				"<option value='nr_scientific'>Scientific</option>" +
				"<option value='nr_fraction'>Fraction</option>" +
				"<option value='nr_selection'>Selection</option>";

			$('#' + prefix + '_type').html(type_options);
			$('#' + prefix + '_type').show();

			if ('nrDigits' in typeDetails) $('#' + prefix + '_nr_digits').val(typeDetails.nrDigits);
			else $('#' + prefix + '_nr_digits').val(4);
			
			if ('prompt' in typeDetails) {
				$('#' + prefix + '_show_prompt_nr').val(typeDetails.prompt.type);
			} else $('#' + prefix + '_show_prompt_nr').val('none');
			
			$('#' + prefix + '_options_wrapper .nr_block').show();
			if ((type == 'nr_decimal') || (type == 'nr_scientific')) {
				$('#' + prefix + '_numberOptions').show();
				$('#' + prefix + '_selectionOptions').hide();

				$('#' + prefix + '_fudgeFactor').val(typeDetails.fudgeFactor);
				$('#' + prefix + '_tensValue').val(typeDetails.tensValue);
				$('#' + prefix + '_sigDigsBehaviour').val(typeDetails.sigDigsBehaviour);

				if (($('#' + prefix + '_sigDigsBehaviour').val() == 'Zeros') || ($('#' + prefix + '_sigDigsBehaviour').val() == 'Round')) {
					$('#' + prefix + '_sigDigsValue').val(typeDetails.sigDigsValue);
				} else $('#' + prefix + '_sigDigsValue').val(0.0);
				
				if (typeDetails.fudgeFactor == 0.0) {
					$('#' + prefix + '_allowSmallErrors').prop('checked', false);
					$('#' + prefix + '_options_wrapper .fudgeFactor_block').hide();
				} else {
					$('#' + prefix + '_allowSmallErrors').prop('checked', true);
					$('#' + prefix + '_options_wrapper .fudgeFactor_block').show();
				}
		
				if (typeDetails.tensValue == 0.0) {
					$('#' + prefix + '_allowFactorOfTen').prop('checked', false);
					$('#' + prefix + '_options_wrapper .tensValue_block').hide();
				} else {
					$('#' + prefix + '_allowFactorOfTen').prop('checked', true);
					$('#' + prefix + '_options_wrapper .tensValue_block').show();
				}
		
				if ((typeDetails.sigDigsBehaviour == 'Zeros') || (typeDetails.sigDigsBehaviour == 'Round')) {
					$('#' + prefix + '_options_wrapper .sigDigsValue_block').show();
				} else {
					$('#' + prefix + '_options_wrapper .sigDigsValue_block').hide();
				}
			} else if (type == 'nr_selection') {
				$('#' + prefix + '_numberOptions').hide();
				$('#' + prefix + '_selectionOptions').show();

				$('#' + prefix + '_markByDigits').prop('checked', typeDetails.markByDigits);
				$('#' + prefix + '_partialValue').val(typeDetails.partialValue);
				$('#' + prefix + '_ignoreOrder').prop('checked', typeDetails.ignoreOrder);

				if (prefix == 'djsq') {
					if (typeDetails.partialValue > 0.0) {
						$('#' + prefix + '_markByDigits').prop('checked', false);
						$('#' + prefix + '_markByDigits').attr('disabled', 'disabled');
						$('#' + prefix + '_markByDigits').next('label').addClass('disabled');
					} else if (typeDetails.markByDigits) {
						$('#' + prefix + '_partialValue').val(0.0);
						$('#' + prefix + '_allowPartialMatch').attr('disabled', 'disabled');
						$('#' + prefix + '_allowPartialMatch').next('label').addClass('disabled');
						$('#' + prefix + '_detailWrapper .nrAnswerAdd').attr('disabled', 'disabled');	
					} else {
						$('#' + prefix + '_markByDigits').removeAttr('disabled');
						$('#' + prefix + '_markByDigits').next('label').removeClass('disabled');
						$('#' + prefix + '_allowPartialMatch').removeAttr('disabled');
						$('#' + prefix + '_allowPartialMatch').next('label').removeClass('disabled');
					}
				}

				if (typeDetails.partialValue == 0.0) {
					$('#' + prefix + '_allowPartialMatch').prop('checked', false);
					$('#' + prefix + '_partialValueWrapper').hide();
				} else {
					$('#' + prefix + '_allowPartialMatch').prop('checked', true);
					$('#' + prefix + '_partialValueWrapper').show();
				}

			} else {

				$('#' + prefix + '_numberOptions').hide();
				$('#' + prefix + '_selectionOptions').hide();

			}

			if ($('#' + prefix + '_show_prompt_nr').val() == 'none') {

				$('#' + prefix + '_prompt_wrapper').hide();
	
			} else if ($('#' + prefix + '_show_prompt_nr').val() == 'simple') {
	
                var nr_prompt_1 = await localizationInstance.getString('questions_nr_prompt_1');
                var nr_prompt_paper = await localizationInstance.getString('questions_nr_prompt_paper');

				$('#' + prefix + '_prompt_wrapper').show();
				$('#' + prefix + '_prompt_text').val(nr_prompt_1 + ' ' + nr_prompt_paper + '.');
				if (prefix == 'djsq') $('#' + prefix + '_prompt_text').attr('disabled', 'disabled');
	
			} else if ($('#' + prefix + '_show_prompt_nr').val() == 'digits') {
	
				var numDigits = parseInt($('#' + prefix + '_nr_digits').val());
				var digitString;
	
				if (numDigits <= 10) {
                    if (countWords == null) {
                        var countsList = await localizationInstance.getString('questions_numbers');
                        if (countsList.indexOf('ERROR') == -1) countWords = countsList.split(',');
                    }
	
					if (countWords != null) digitString = countWords[numDigits - 1];
					else digitString = "[ERROR]";
				} else digitString = numDigits.toString();
				
                var nr_prompt_2 = await localizationInstance.getString('questions_nr_prompt_2');
                var nr_prompt_paper = await localizationInstance.getString('questions_nr_prompt_paper');
				var $promptElement = $('<div>' + nr_prompt_2.replace('#', digitString) + ' ' + nr_prompt_paper + '.</div>');
	
				$('#' + prefix + '_prompt_wrapper').show();
				$('#' + prefix + '_prompt_text').val($promptElement.text());
				if (prefix == 'djsq') $('#' + prefix + '_prompt_text').attr('disabled', 'disabled');
	
			} else if ($('#' + prefix + '_show_prompt_nr').val() == 'text') {
	
				$('#' + prefix + '_prompt_wrapper').show();
				$('#' + prefix + '_prompt_text').val(typeDetails.prompt.text);
				if (prefix == 'djsq') $('#' + prefix + '_prompt_text').removeAttr('disabled');
	
			}

		} else if (style == 'wr') {

			var type_options =
				"<option value='wr' selected>Written response</option>";
			$('#' + prefix + '_type').html(type_options);
			$('#' + prefix + '_type').hide();

			if ('height' in typeDetails) $('#' + prefix + '_wr_height').val(typeDetails.height);
			else $('#' + prefix + '_wr_height').val(1);
			
			var promptType = ('prompt' in typeDetails) ? typeDetails.prompt.type : 'none';
			$('#' + prefix + '_promptType').val(promptType);

			if (prefix == 'jsq') {
				var criteriaString = "";
				if (('criteria' in typeDetails) && (typeDetails.criteria.length > 0)) {
					for (var i = 0; i < typeDetails.criteria.length; ++i) {
						if (i > 0) criteriaString += ", ";
						criteriaString += typeDetails.criteria[i].label + " (/" + typeDetails.criteria[i].value + ")";
					}
				} else criteriaString = "None";
	
				$('#' + prefix + '_criteriaWrapper').html(criteriaString);	
			} else {
				if (promptType == 'none') $('#startEditPrompt').attr('disabled', 'disabled');
				else $('#startEditPrompt').removeAttr('disabled');

				rebuildWRCriteria();
			}
			
			$('#' + prefix + '_options_wrapper .wr_block').show();

		}

		$('#' + prefix + '_style').val(style);
		$('#' + prefix + '_type').val(type);
	}

	$('#editContext_form').dialog({
		autoOpen: false,
		width:700,
		height:535,
		modal: true,
		resizable: true,
		minWidth:700,
		minHeight:400,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
				$(this).dialog("close");
			},
			'Save': function() {
				contextSave();
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
			$('#djsq_context_edit').htmleditor('initSelection');
			initSubmitProgress($(this));
		}, close: function(event) {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });

    $('#djsq_context_edit').on('savedom', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });

    $(document).on('change', '.context_answer_block select, .context_answer_block input', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });
    
	$('#editQuestion_form').dialog({
		autoOpen: false,
		width:700,
		height:535,
		modal: true,
		resizable: true,
		minWidth:700,
		minHeight:450,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
				var newPart = true;
				for (var i = 0; i < questionData.Parts.length; ++i) {
					if (questionData.Parts[i].id == editPart.id) newPart = false;
				}
	            if (newPart) {
					$('#partLink_new').remove();
					$('#parts_menu .menu_item').first().trigger('click');
	            }
				$(this).dialog("close"); 
	        },
			'Save': function() {
				partSave();
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
			$('#djsq_question_edit').htmleditor('initSelection');
			initSubmitProgress($(this));
		}, close: function(event) {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });
    
    $('#djsq_question_edit').on('savedom', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });

    $(document).on('change', '#djsq_options_wrapper select, #djsq_options_wrapper input', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });
    
    $('#djsq_promptType').on('change', function(e) {
		if ('prompt' in editPart.TypeDetails) {
			$('#djsq_last_prompt').val(JSON.stringify(editPart.TypeDetails.prompt));
		} else $('#djsq_last_prompt').val('');

		if ($('#djsq_promptType').val() == 'none') {
			delete editPart.TypeDetails.prompt;
	
			$('#startEditPrompt').attr('disabled', 'disabled');
	    } else {
			editPart.TypeDetails.prompt = {
				type: $('#djsq_promptType').val()
			};

			$(this).blur();
			$('#startEditPrompt').removeAttr('disabled');
		    $('#startEditPrompt').trigger('click');
		}
    });
    
    $('#startEditPrompt').on('click', function(e) {
		$(this).blur();

	    if ($('#djsq_promptType').val() == 'text') {
			if (('prompt' in editPart.TypeDetails) && ('text' in editPart.TypeDetails.prompt)) {
				$('#djsq_wr_prompt_text').val(editPart.TypeDetails.prompt.text);
			} else $('#djsq_wr_prompt_text').val('');

		    $('#editPromptText_dialog').dialog('open');
	    } else if ($('#djsq_promptType').val() == 'image') {
			if (('prompt' in editPart.TypeDetails) && ('imageHash' in editPart.TypeDetails.prompt)) {
				setPromptImagePreview(editPart.TypeDetails.prompt.imageHash);
			} else {
				$('#djsq_prompt_image img').remove();
				$('#djsq_prompt_image_blank').show();
			}

			if (('prompt' in editPart.TypeDetails) && ('height' in editPart.TypeDetails.prompt)) {
				$('#djsq_prompt_image_height').val(editPart.TypeDetails.prompt.height);
			} else $('#djsq_prompt_image_height').val($('#djsq_wr_height').val());

			$('#prompt_image_extension').val('');
			$("#prompt_filename").html('No file selected');

		    $('#editPromptImage_dialog').dialog('open');
	    }
    });
    
    function setPromptImagePreview(imageName) {
		$('#djsq_prompt_image_blank').hide();

		$('#djsq_prompt_image_name').val(imageName);
		var $newImage = $('<img style="display:block; margin:auto;" src="' + 
			userdataPrefix + imageName + '" />');
		$('#djsq_prompt_image img').remove();
		$('#djsq_prompt_image').append($newImage);

		$newImage.on('fitPromptImage', function(){
		    var image_ratio = $newImage[0].naturalWidth / $newImage[0].naturalHeight;
		    var parent_ratio = $('#djsq_prompt_image').innerWidth() / $('#djsq_prompt_image').innerHeight();
		    if (image_ratio < parent_ratio) $newImage.css({width: 'auto', height:'100%'});
		    else $newImage.css({width: '100%', height:'auto'});
		}).on('load', function(){
		    $(this).trigger('fitPromptImage');
		}).trigger('fitPromptImage');
    }
    
    function promptTextSave() {
		editPart.TypeDetails.prompt = {
			type: 'text',
			text: $('#djsq_wr_prompt_text').val()
		};

	    $('#editPromptText_dialog').dialog('close');
    }

    function promptImageSave() {
		var $dialog     = $(this);
		var $form       = $dialog.find('form');
				
		if ($('#djsq_prompt_image_name').val() == '') {
			$('#djsq_prompt_image').validationEngine('showPrompt', "* Click 'Select new' to choose an image, thn upload", 'error', 'bottomLeft', true);
		} else if ($('#djsq_prompt_image_height').val() > $('#djsq_wr_height').val()) {
			$('#djsq_prompt_image_height').validationEngine('showPrompt', "* Must not be greater than question height", 'error', 'bottomLeft', true);
		} else if ($form.validationEngine('validate')) {
			editPart.TypeDetails.prompt = {
				type: 'image',
				imageHash: $('#djsq_prompt_image_name').val(),
				height: parseFloat($('#djsq_prompt_image_height').val())
			};
			
		    $('#editPromptImage_dialog').dialog('close');
		}
	}

	$('#editPromptText_dialog').dialog({
		autoOpen: false,
		width:500,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
	            if ($('#djsq_last_prompt').val() != '') {
					editPart.TypeDetails.prompt = JSON.parse($('#djsq_last_prompt').val());
					$('#djsq_promptType').val(editPart.TypeDetails.prompt.type);
				} else {
					delete editPart.TypeDetails.prompt;
					$('#djsq_promptType').val('none');
				}
				$(this).dialog("close"); 
	        },
			'Done': promptTextSave
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Done")').addClass('btn btn-primary btn_font');
		}, close: function(event) {
			$('form').validationEngine('hideAll');
		}
    });

	$('#editPromptImage_dialog').dialog({
		autoOpen: false,
		width:520,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
	            if ($('#djsq_last_prompt').val() != '') {
					editPart.TypeDetails.prompt = JSON.parse($('#djsq_last_prompt').val());
					$('#djsq_promptType').val(editPart.TypeDetails.prompt.type);
				} else {
					delete editPart.TypeDetails.prompt;
					$('#djsq_promptType').val('none');
				}
				$(this).dialog("close"); 
	        },
			'Done': promptImageSave
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Done")').addClass('btn btn-primary btn_font');
		}, close: function(event) {
			$('form').validationEngine('hideAll');
		}
    });

	$("#prompt_progressbar").progressbar();

	let promptImageUpload = null;
	let promptFile = null;

	$('#prompt_fileupload').on('change', function(e) {
		const files = e.target.files;
		if (!files || !files[0]) {

			// No file chosen
			promptFile = null;
			$('#prompt_filename').text('No file selected');
			$('#prompt_upload_button').attr('disabled', 'disabled');

		} else {

			// Store the file, enable “Upload” button
			promptFile = files[0];
			$('#prompt_filename').text(promptFile.name);
			$('#prompt_upload_button').removeAttr('disabled');

		}
	});

	$('#prompt_upload_button').click(function() {
		if (promptImageUpload) {

			// Cancel the upload
			promptImageUpload.abort();
			promptImageUpload = null;

			$('#prompt_fileupload').removeAttr('disabled');
			$('#prompt_select_button').removeAttr('disabled');
			$('#prompt_title').removeAttr('disabled');
			$('#prompt_progressbar').hide();
			$(this).removeClass('btn-danger');
			$(this).text('Upload');

		} else if (promptFile) {

			// Check file size
			if (promptFile.size > 10000000) {
				$('#prompt_filename').validationEngine(
					'showPrompt',
					'* Images must be smaller than 10 MB',
					'error',
					'bottomLeft',
					true
				);
				return;
			}

			// Start the upload
			$('#prompt_progressbar').progressbar('value', 0).show();
			$('#prompt_fileupload').attr('disabled', 'disabled');
			$('#prompt_select_button').attr('disabled', true);
			$('#prompt_title').attr('disabled', true);
			$(this).addClass('btn-danger');
			$(this).text('Cancel');

			promptImageUpload = new ImageUpload(promptFile, (percent) => {
				$('#prompt_progressbar').progressbar('value', percent);
			});

			promptImageUpload.start()
			.then((responseData) => {

				$('#prompt_upload_button').attr('disabled', 'disabled');
				$('#prompt_fileupload').removeAttr('disabled');
				$('#prompt_select_button').removeAttr('disabled');

				$('#prompt_progressbar').hide();

				$(this).removeClass('btn-danger');
				$(this).text('Upload');

				promptFile = null;
				promptImageUpload = null;

				setPromptImagePreview(responseData.imageHash);

			}).catch((err) => {

				if (err && err.message == 'Upload cancelled') {
					$.gritter.add({
						title: "Success",
						text: "Image upload cancelled.",
						image: "/img/success.svg"
					});	
				} else {
					showSubmitError();
				}

				// Reset UI
				$('#prompt_fileupload').removeAttr('disabled');
				$('#prompt_select_button').removeAttr('disabled');
				$('#prompt_title').removeAttr('disabled');
				$('#prompt_progressbar').hide();
				$(this).removeClass('btn-danger');
				$(this).text('Upload');

				promptImageUpload = null;
			});
		}
	});

	$('#confirmation_dialog').dialog({
		autoOpen: false,
		width:400,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
				$(this).dialog("close"); 
	        },
			'Save': function() {
				$(this).dialog("close");
				finishPartSave();
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
		}
    });

    $('#openEditAnswers').click(function(e) {
		e.preventDefault();
		if (!canEdit) return;

        var answerData;
        if ('Common' in questionData) answerData = questionData.Common.Answers;
        else answerData = activePart.Answers;

        var scoringData = JSON.parse(JSON.stringify(activePart.ScoredResponses));

        var firstAnswer = false;
        if (answerData.length == 0) {
            firstAnswer = true;
            var answerID = getNewID(4);

            var initTemplate;
            if (activePart.type == 'mc_table') {
                initTemplate = '[';
                for (var i = 0; i < activePart.TypeDetails.labels.length; ++i) {
                    if (i > 0) initTemplate += ',';
                    initTemplate += '""';
                }
                initTemplate += ']';
            } else initTemplate = "";

            answerData = [{
                id: answerID,
                template: initTemplate
            }];

            scoringData = [{
                id: answerID,
                value: 1.0
            }];
        }

		const tableData = getWizardData();
		setCommonContext(tableData);

        var needsKey = false;
        if (activePart.type == 'mc') {
            for (var i = 0; i < activePart.ScoredResponses.length; ++i) {
                if ('ids' in activePart.ScoredResponses[i]) needsKey = true;
            }    
        }

        // Open dialog

        $('#editAnswers_dialog').dialog("open");

        // Draw answer rows

        $('#dans_answer_rows').empty();
        for (var i = 0; i < answerData.length; ++i) {
            addAnswerRow(answerData[i]);
        }

        $('#scoring_json').val(JSON.stringify(scoringData));

        if ($('#dans_answer_rows .answer_wrapper').length == 1) {
            $('#dans_answer_rows .delete_icon').attr('disabled', 'disabled');
        }

        formatAnswers();
        drawAnswerScores();

        // Build key and show if necessary

        updateScoreElements(needsKey);        
        if (activePart.type == 'mc') $('#dans_show_key').show();
        else $('#dans_show_key').hide();
        
        if (firstAnswer) {
            $('#dans_answer_rows .answer_template').eq(0).focus();
        } else {
            $('#dans_answer_rows .answer_rationale').each(function() {
                if ($(this).find('input').val().length > 0) $(this).show();
                else $(this).hide();
            });

            document.activeElement.blur();
        }
	});

    var addEmptyAnswer = function() {
        var value;
        if (activePart.type.substr(0, 2) == 'nr') {
            value = parseFloat($('#dans_answer_rows .answer_score>input').last().val());
        } else value = 0;

        var answerID = getNewID(4);

        // Update answer list

        var $newRow = addAnswerRow({
            id: answerID
        });

        // Update scoring data

        if (value != 0.0) {
            var scoringData = JSON.parse($('#scoring_json').val());
            scoringData.push({id: answerID, value: value});
            $('#scoring_json').val(JSON.stringify(scoringData));
        }

        return $newRow;
    }

    var addAnswerRow = function(data) {
        var rowHTML = false;

        rowHTML = 
            '<div class="answer_wrapper" style="display:flex; margin-bottom:5px; gap:10px; align-items:flex-start;" data-answer-id="' + data.id + '">';

        if (activePart.type == 'mc_table') {

            rowHTML += 
                '<div class="answer_label" style="flex:none; width:20px;"></div> \
                <div style="flex:1 1 100%;"><div style="width:100%; box-sizing:border-box; padding:0px 5px 5px; margin-bottom:5px; border:1px solid #cccccc; border-radius:5px;">';

            for (var i = 0; i < activePart.TypeDetails.labels.length; ++i) {
                rowHTML += 
                    '<div style="width:100%; height:auto;"> \
                        <div style="font-size:12px; margin-top:3px;">' + activePart.TypeDetails.labels[i] + '</div> \
                        <div class="answer_template template_view" style="height:auto;"></div> \
                    </div>';
            }

            rowHTML += 
                '</div> \
                <div class="answer_rationale" style="flex:1 1 100%; display:flex; flex-direction:row; align-items:center; justify-content:space-between; gap:10px; margin:5px 0px 5px;"> \
                    <div style="flex:none; width:auto; font-size:14px;">Rationale:</div> \
                    <input style="flex:1 1 100%; height:30px; margin:0px; box-sizing:border-box; color:#222222;" type="text" /> \
                </div></div>';

        } else if (activePart.type == 'mc') {

            rowHTML += 
                '<div class="answer_label" style="flex:none; width:20px;"></div> \
                <div style="flex:1 1 100%;"> \
                    <div class="answer_template template_view" style="flex:1 1 100%; height:auto;"></div> \
                    <div class="answer_rationale" style="flex:1 1 100%; display:flex; flex-direction:row; align-items:center; justify-content:space-between; gap:10px; margin:5px 0px 5px;"> \
                        <div style="flex:none; width:auto; font-size:14px;">Rationale:</div> \
                        <input style="flex:1 1 100%; height:30px; margin:0px; box-sizing:border-box; color:#222222;" type="text" /> \
                    </div> \
                </div>';

        } else if (activePart.type.substr(0, 2) == 'nr') {

            rowHTML += 
                '<div style="flex:1 1 100%;"> \
                    <div class="answer_template template_view" style="flex:1 1 100%; height:auto;"></div> \
                    <div class="answer_rationale" style="flex:1 1 100%; display:flex; flex-direction:row; align-items:center; justify-content:space-between; gap:10px; margin:5px 0px 5px;"> \
                        <div style="flex:none; width:auto; font-size:14px;">Rationale:</div> \
                        <input style="flex:1 1 100%; height:30px; margin:0px; box-sizing:border-box; color:#222222;" type="text" /> \
                    </div> \
                </div>';

        }

        rowHTML +=
                '<div class="answer_score" style="flex:none; width:60px;"> \
                    <input class="validate[required,custom[isPosDecimal]] answer_value" style="width:100%;" type="text" /> \
                </div> \
                <div class="answer_actions" style="flex:none; width:50px; display:flex; flex-direction:row; align-items:center; justify-content:space-between;"> \
                    <button title="Duplicate" class="actions_button copy_icon answerCopy">&nbsp;</button> \
                    <button title="Delete" class="actions_button delete_icon answerDelete">&nbsp;</button> \
                </div> \
            </div>';

        var $thisRow = $(rowHTML);

        $('#dans_answer_rows').append($thisRow);

        $thisRow.find('.answer_template').each(function() {
            $(this).htmleditor({
                height: 'auto',
                defaultVariableType: 'calculated'
            });
    
            $(this).htmleditor('hideAllButtons');

            if (activePart.type.substr(0, 2) == 'mc') {

                $(this).htmleditor('showElement', 'edit_bar_section_font');
                $(this).htmleditor('showElement', 'edit_button_image');
                $(this).htmleditor('showElement', 'edit_button_variable');
                $(this).htmleditor('showElement', 'edit_button_symbol');
                $(this).htmleditor('showElement', 'edit_button_equation');
                $(this).htmleditor('showElement', 'edit_button_settings');
                
            } else if (activePart.type.substr(0, 2) == 'nr') {

                $(this).htmleditor('showElement', 'edit_button_variable');
                $(this).htmleditor('showElement', 'edit_button_settings');

            }

            $(this).htmleditor('makeInactive');
        });

        var $answerFields = $thisRow.find('.answer_template');
        if (activePart.type == 'mc_table') {
            if ('templates' in data) {
                for (var i = 0; i < data.templates.length; ++i) {
                    $answerFields.eq(i).htmleditor('setValue', data.templates[i]);
                }
            }
        } else if ('template' in data) {
            $answerFields.eq(0).htmleditor('setValue', data.template);
        }

        if ('rationale' in data) {
            $thisRow.find('.answer_rationale input').val(data.rationale);
        }

        $('#dans_answer_rows .answerDelete').removeAttr('disabled');

        return $thisRow;
    }

    var formatAnswers = function() {
        if (activePart.type.substr(0, 2) == 'mc') {
            var index = 0;
            $('#dans_answer_rows .answer_wrapper').each(function() {
                var $thisLabel = $(this).find('.answer_label');
                $thisLabel.html(String.fromCharCode('A'.charCodeAt(0) + index) + '.');
                ++index;
            });
        }

        $('#dans_answer_rows .answer_wrapper').each(function() {
            var $firstAnswer = $(this).find('.answer_template').eq(0);
            var $alignedElements = $(this).find('.answer_label, .answer_score, .answer_actions');

            var offset = $firstAnswer.offset().top - $(this).offset().top;

            var maxHeight = 0.0;
            $alignedElements.each(function() {
                var thisHeight = $(this).height();
                if (thisHeight > maxHeight) maxHeight = thisHeight;
            });
            $alignedElements.each(function() {
                $(this).css('top', offset + (maxHeight - $(this).height()) / 2);
            });
        });

        if (activePart.type == 'mc_table') {
            $('#dans_answer_rows .answer_label').css('top', '5px');
        }
    }

    var drawAnswerScores = function() {
        var scoringData = JSON.parse($('#scoring_json').val());

        $('#dans_answer_rows .answer_score>input').val('0');
        for (var i = 0; i < scoringData.length; ++i) {
            if ('id' in scoringData[i]) {
                var answerID = scoringData[i].id;
                var $answerWrapper = $('#dans_answer_rows .answer_wrapper[data-answer-id="' + answerID + '"]');
                $answerWrapper.find('.answer_score>input').val(scoringData[i].value);
            }
        }

        var answers = [];
        $('#dans_answer_rows .answer_wrapper').each(function() {
            if ((answers !== false) && ($(this).find('.answer_label').length > 0)) {
                answers.push({
                    id: $(this).attr('data-answer-id'),
                    label: $(this).find('.answer_label').text().toLowerCase()
                });    
            } else answers = false;
        });

        if (answers !== false) {
            var keyHTML = "";
            for (var j = 0; j < scoringData.length; ++j) {
                var answerIDs = ('ids' in scoringData[j]) ? scoringData[j].ids : [scoringData[j].id];
                
                keyHTML += 
                    '<div class="scoring_row" style="display:flex; flex-direction:row; align-items:center; margin-bottom:5px; gap:10px;"> \
                        <div style="flex:1 1 100%;">';
    
                for (var i = 0; i < answers.length; ++i) {
                    var value = (answerIDs.indexOf(answers[i].id) == -1) ? '' : 'checked';
                    var label = answers[i].label;
    
                    keyHTML += 
                            '<div class="mcq_detail_response"> \
                                <label for="dmcq_' + j + '_response_' + i + '">' +  label + '</label> \
                                <input type="checkbox" class="scoring_response" data-answer-id=' + answers[i].id + ' id="dmcq_' + j + '_response_' + i + '" ' + value + ' /> \
                            </div>';
                }			
    
                keyHTML += 
                        '</div> \
                        <label style="flex:none; width:auto; margin:0px;" for="scoring_value_' + j + '">Marks:</label> \
                        <input class="defaultTextBox2 scoring_value validate[required,custom[isPosDecimal]]" style="flex:none; width:30px; margin:0px;" type="text" id="scoring_value_' + j + '" value="' + scoringData[j].value + '" /> \
                        <div style="flex:none; width:50px; display:flex; flex-direction:row; align-items:center; justify-content:space-between;"> \
                            <button title="Add" class="actions_button add_icon">&nbsp;</button> \
                            <button title="Delete" class="actions_button delete_icon">&nbsp;</button> \
                        </div> \
                    </div>';
            }
            $('#dans_scoring_rows').html(keyHTML);

            if (scoringData.length == 1) $('#dans_scoring_rows .delete_icon').attr('disabled', 'disabled');

            // Update scoring description

            var answerMap = {};
            $('#dans_answer_rows .answer_wrapper').each(function() {
                var thisID = $(this).attr('data-answer-id');
                var thisLabel = $(this).find('.answer_label').text().toLowerCase()[0];
                answerMap[thisID] = thisLabel;
            });
    
            var needsDescription = (scoringData.length > 1);
            var descriptionText = "<b>Note</b>: Students will recieve ";
            for (var j = 0; j < scoringData.length; ++j) {
                var answerIDs = ('ids' in scoringData[j]) ? scoringData[j].ids : [scoringData[j].id];
                if (answerIDs.length > 1) needsDescription = true;
    
                var thisScore = scoringData[j].value;
    
                if (j > 0) descriptionText += ", or ";
                descriptionText += thisScore + " mark" + ((thisScore == 1) ? '' : 's') + " if they fill in ";
                if (answerIDs.length > 1) {
                    needsDescription = true;
                    if (answerIDs.length == 2) descriptionText += "both ";
                }
                for (var i = 0; i < answerIDs.length; ++i) {
                    if (i > 0) descriptionText += " and "
                    descriptionText += "'" + answerMap[answerIDs[i]] + "'";
                }
            }
            descriptionText += ".";
            if (needsDescription && (scoringData.length == 1)) {
                descriptionText += " To score alternative responses instead, click '+' and enter a second response.";
            }
            
            if (needsDescription) {
                $('#dans_scoring_description').html(descriptionText);
            } else $('#dans_scoring_description').empty();
        }

        var scoringVisible = $('#dans_scoring_header').is(':visible');
        updateScoreElements(scoringVisible);
    }

    var updateScoreElements = function(keyVisible) {
        
        // Draw header
        
        var headerHTML = '';
        if (keyVisible) {

            headerHTML = '<div>Answers:</div>';

        } else if (activePart.type.substr(0, 2) == 'mc') {

            headerHTML =
                '<div style="flex:none; width:20px;"></div> \
                <div style="flex:1 1 100%;">Answer</div> \
                <div style="flex:none; width:60px;">Marks</div> \
                <div style="flex:none; width:50px;"></div>';

        } else if (activePart.type.substr(0, 2) == 'nr') {

            headerHTML =
                '<div style="flex:1 1 100%;">Answer:</div> \
                <div style="flex:none; width:60px;">Marks:</div> \
                <div style="flex:none; width:50px;"></div>';

        }

        $('#dans_answer_header').html(headerHTML);

        // Show scoring

        var scoringData = JSON.parse($('#scoring_json').val());

        var needsKey = false;
        for (var i = 0; i < scoringData.length; ++i) {
            if ('ids' in scoringData[i]) needsKey = true;
        }

        if (needsKey) {
            keyVisible = true;
            $('#dans_show_key').attr('disabled', 'disabled');
        } else $('#dans_show_key').removeAttr('disabled');

        if (keyVisible) {
            $('#dans_scoring_header').show();
            $('#dans_scoring_rows').show();
            $('#dans_scoring_description').show();
            $('#dans_show_key').html('Hide key');
            $('.answer_score').hide();
        } else {
            $('#dans_scoring_header').hide();
            $('#dans_scoring_rows').hide();
            $('#dans_scoring_description').hide();
            $('#dans_show_key').html('Show key');
            $('.answer_score').show();
        }
    }
    
    var updateScoringFromKey = function() {
        var scoringData = [];
        $('.scoring_row').each(function() {
            var ids = [];
            $(this).find('.mcq_detail_response').each(function() {
                var $checkbox = $(this).children('.scoring_response');
                if ($checkbox.prop('checked')) ids.push($checkbox.attr('data-answer-id'));
            });

            var value = parseFloat($(this).find('.scoring_value').val());
            if (ids.length == 1) {
                scoringData.push({
                    id: ids[0],
                    value: value
                });
            } else {
                scoringData.push({
                    ids: ids,
                    value: value
                });
            }
        });

        $('#scoring_json').val(JSON.stringify(scoringData));
        drawAnswerScores();

        var scoringVisible = $('#dans_scoring_header').is(':visible');
        updateScoreElements(scoringVisible);
    }

    $('#dans_scoring_rows').on('change', '.scoring_value', function() {
        updateScoringFromKey();
    });

    $('#dans_scoring_rows').on('change', '.scoring_response', function() {
        updateScoringFromKey();
    });

    $('#dans_scoring_rows').on('click', '.add_icon', function() {
        var scoringData = JSON.parse($('#scoring_json').val());
        var scoreIndex = $(this).closest('.scoring_row').index();

        scoringData.splice(scoreIndex + 1, 0, {
            ids: [],
            value: 0
        });

        $('#scoring_json').val(JSON.stringify(scoringData));
        drawAnswerScores();
    });

    $('#dans_scoring_rows').on('click', '.delete_icon', function() {
        $(this).closest('.scoring_row').remove();
        updateScoringFromKey();
    });

    $('#dans_answer_rows').on('change', '.answer_value', function() {
        var scoringData = JSON.parse($('#scoring_json').val());
        var answerID = $(this).closest('.answer_wrapper').attr('data-answer-id');

        var newValue = parseFloat($(this).val());
        var needsValue = (newValue != 0);
        
        var scoreIndex = 0;
        while (scoreIndex < scoringData.length) {
            if (('id' in scoringData[scoreIndex]) && (scoringData[scoreIndex].id == answerID)) {
                if (needsValue) scoringData[scoreIndex].value = newValue;
                else scoringData.splice(scoreIndex, 1);
                break;
            } else ++scoreIndex;
        }

        if ((scoreIndex == scoringData.length) && needsValue) {
            scoringData.push({
                id: answerID,
                value: newValue
            });
        }

        $('#scoring_json').val(JSON.stringify(scoringData));
        drawAnswerScores();
    });

    $('#dans_answer_rows').on('click', '.answerCopy', function() {
        var $thisRow = $(this).closest('.answer_wrapper');
        var oldAnswerID = $thisRow.attr('data-answer-id');

        // Update answer list

        var newAnswerID = getNewID(4);
        var newData = { id: newAnswerID };

        if (activePart.type == 'mc_table') {
            newData.templates = [];
            $thisRow.find('.answer_template').each(function() {
                newData.templates.push($(this).htmleditor('getValue'));
            });
        } else {
            newData.template = $thisRow.find('.answer_template').htmleditor('getValue');
        }

        var $newRow = addAnswerRow(newData);

        // Update scoring data

        var scoringData = JSON.parse($('#scoring_json').val());
        var numScores = scoringData.length;

        for (var i = 0; i < numScores; ++i) {
            if (('id' in scoringData[i]) && (scoringData[i].id == oldAnswerID)) {
                scoringData.push({
                    id: newAnswerID,
                    value: scoringData[i].value
                });
            } else if ('ids' in scoringData[i]) {
                var thisIndex = scoringData[i].ids.indexOf(oldAnswerID);
                if (thisIndex >= 0) {
                    var newEntry = JSON.parse(JSON.stringify(scoringData[i]));
                    newEntry.ids[thisIndex] = newAnswerID;
                    scoringData.push(newEntry);
                }
            }
        }

        $('#scoring_json').val(JSON.stringify(scoringData));

        // Update interface

        var $thisAnswer = $newRow.find('.answer_template');
        $thisAnswer.htmleditor('initSelection');
        $thisAnswer.focus();

        formatAnswers();
        drawAnswerScores();
    });

    $('#dans_answer_rows').on('click', '.answerDelete', function() {

        // Update scoring data

        var scoringData = JSON.parse($('#scoring_json').val());

        var scoringIndex = 0;
        var answerID = $(this).closest('.answer_wrapper').attr('data-answer-id');
        while (scoringIndex < scoringData.length) {
            if (('id' in scoringData[scoringIndex]) && (scoringData[scoringIndex].id == answerID)) {
                scoringData.splice(scoringIndex, 1);
            } else if ('ids' in scoringData[scoringIndex]) {
                var answerIndex = scoringData[scoringIndex].ids.indexOf(answerID);
                if (answerIndex >= 0) scoringData.splice(scoringIndex, 1);
                else ++scoringIndex;
            } else ++scoringIndex;
        }

        $('#scoring_json').val(JSON.stringify(scoringData));

        // Update answer list

        $(this).closest('.answer_wrapper').remove();

        if ($('#dans_answer_rows .answer_wrapper').length == 1) {
            $('#dans_answer_rows .answerDelete').attr('disabled', 'disabled');
        }

        formatAnswers();
        drawAnswerScores();
    });

    $('#dans_answer_rows').on('afterPaste', '.answer_template', function(e) {
        var templateHTML = $(this).htmleditor('getValue');
        var $template = $('<div>' + templateHTML + '</div>');

        var $templateListElements = false;
        if ($template.find('li').length > 0) $templateListElements = $template.find('li');

        var $templateTableRows = false;
        if ($template.find('tr').length > 0) $templateTableRows = $template.find('tr');

        var $templateDivs = false;
        if ($template.children('div:not(.equation)').length > 1) $templateDivs = $template.children('div');

        var splitHTML = [];
        if ($templateListElements !== false) {

            $templateListElements.each(function() {
                splitHTML.push($(this).html());
            });

        } else if ($templateTableRows !== false) {

            $templateTableRows.each(function() {
                var columns = [];
                $(this).children('td').each(function() {
                    columns.push('<span>' + $(this).html() + '</span>');
                });
                splitHTML.push(columns.join(' '));
            });

        } else if ($templateDivs.length > 1) {

            $templateDivs.each(function() {
                splitHTML.push($(this).html());
            });

        } else {

            splitHTML.push(templateHTML);

        }

        $(this).htmleditor('setValue', splitHTML[0]);

        var value;
        if (activePart.type.substr(0, 2) == 'nr') {
            value = parseFloat($('#dans_answer_rows .answer_score>input').last().val());
        } else value = 0;

        var $newRows = [];
        for (var i = 1; i < splitHTML.length; ++i) {

            // Update answer list            

            var answerID = getNewID(4);
            addAnswerRow({
                id: answerID,
                template: splitHTML[i]
            });   

            // Update scoring data

            if (value != 0.0) {
                var scoringData = JSON.parse($('#scoring_json').val());
                scoringData.push({id: answerID, value: value});
                $('#scoring_json').val(JSON.stringify(scoringData));
            }
        }

        var $firstRow = $(this).closest('.answer_wrapper');
        while ($newRows.length > 0) {
            var $thisRow = $newRows.pop();
            $thisRow.insertAfter($firstRow);
        }

        formatAnswers();
        drawAnswerScores();
    });

    $('#dans_answer_rows').on('keydown', '.answer_template', function(e) {
		if (e.key === 'Tab') {

            e.preventDefault();

            var $thisRow = $(this).closest('.answer_wrapper');
            var $answerTemplates = $thisRow.find('.answer_template');
            var thisIndex = $answerTemplates.index($(this));

            if (thisIndex != $answerTemplates.length - 1) $answerTemplates[thisIndex + 1].focus();
            else $(this).closest('.answer_wrapper').find('.answer_score>input').focus();

        } else if (e.key === 'Enter') {

            e.preventDefault();

            var $answerTemplates = $('#dans_answer_rows').find('.answer_template');
            var thisIndex = $answerTemplates.index($(this));

            var $thisAnswer;
            if (thisIndex + 1 < $answerTemplates.length) {
                $thisAnswer = $answerTemplates.eq(thisIndex + 1);
            } else {
                var $thisRow = addEmptyAnswer();
                $thisAnswer = $thisRow.find('.answer_template').eq(0);
            }

            $thisAnswer.htmleditor('initSelection');
            $thisAnswer.focus();
    
            formatAnswers();
            drawAnswerScores();

        }
    });

    $('#dans_answer_rows').on('keydown', '.answer_score>input', function(e) {
		if ((e.key === 'Tab') || (e.key === 'Enter')) {
            e.preventDefault();

            var $thisRow = $(this).closest('.answer_wrapper').next('.answer_wrapper');
            if ($thisRow.length == 0) {
                $thisRow = addEmptyAnswer();
            }

            var $thisAnswer = $thisRow.find('.answer_template').eq(0);
            $thisAnswer.htmleditor('initSelection');
            $thisAnswer.focus();
    
            formatAnswers();
            drawAnswerScores();
        }
    });

    $(document).on('savedom', '#dans_answer_rows .answer_template', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });

    $('#dans_answer_rows .answer_score').on('change', function(e) {
		window.onbeforeunload = function() {
		    return true;
		};
    });

    function keySave() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		var valueError = false;

        // Clear blurWaiting and focusWaiting, then handle that manually

        focusWaiting = [];
        blurWaiting = [];

        $('#dans_answer_rows .answer_rationale').each(function() {
            if ($(this).find('input').val().length > 0) $(this).show();
            else $(this).hide();
        });
        $('#dans_answer_rows .answer_template').htmleditor('makeInactive');

        formatAnswers();

        $('#dans_answer_rows .answer_template').each(function() {

            // Trim whitespace (including HTML) from left and right of answers

            var value = $(this).htmleditor('getValue');

            var trimLeft = /^(<br\s*\/?>|\s|&nbsp;)+/;
            var trimRight = /(<br\s*\/?>|\s|&nbsp;)+$/;
            value = value.replace(trimLeft, '').replace(trimRight, '');

            if (value.length == 0) {

                $(this).validationEngine('showPrompt', '* This field is required', 'error', 'bottomLeft', true);
                valueError = true;

            } else if (activePart.type.substr(0, 2) == 'nr') {

                var $contents = $(this).contents();
                var textParts = '';
                var numVariables = 0;
                var invalidElements = false;
                $contents.each(function() {
                    if ($(this)[0].nodeType == 3) textParts += $(this)[0].nodeValue;  // Check for text nodes
                    else if ($(this).hasClass('block_variable')) numVariables++;
                    else invalidElements = true;
                });

                var textLength = textParts.length;

                if (invalidElements) {
                    $(this).validationEngine('showPrompt', "* Value must not include formatting", 'error', 'bottomLeft', true);
                    valueError = true;
                } else if ((textLength > 0) && (numVariables > 0)) {
                    if (activePart.type == 'nr_selection') {
                        testExpression = /^[\d,\?]*$/;
                        if (!testExpression.test(textParts)) {
                            testMessage = "* Value must contain only digits or '?'";
                            $(this).validationEngine('showPrompt', testMessage, 'error', 'bottomLeft', true);
                            valueError = true;
                        }
                    } else if (activePart.type != 'nr_fraction') {
                        $(this).validationEngine('showPrompt', "* Must contain a value only", 'error', 'bottomLeft', true);
                        valueError = true;
                    } else if (textParts != '/') {
                        $(this).validationEngine('showPrompt', "* Value must be a fraction", 'error', 'bottomLeft', true);
                        valueError = true;
                    }
                } else if ((numVariables > 1) && (activePart.type != 'nr_selection')) {
                    $(this).validationEngine('showPrompt', "* Value can not include more than one variable", 'error', 'bottomLeft', true);
                    valueError = true;
                } else if (textLength > 0) {
                    var testExpression, testMessage;
                    if (activePart.type == 'nr_scientific') {
                        testExpression = /^-?[1-9](\.\d+)?[Ee][+\-]?\d+$/;
                        testMessage = "* Value must be in scientific notation. For example, 6.37&times;10<sup>6</sup> would be entered as 6.37e6.";
                    } else if (activePart.type == 'nr_selection') {
                        testExpression = /^[\d\?]*$/;
                        testMessage = "* Value must contain only digits or '?'";
                    } else if (activePart.type == 'nr_fraction') {
                        testExpression = /^-?([1-9]\d*|0)([\/.]\d+)?$/;
                        testMessage = "* Value must be a fraction";
                    } else {
                        testExpression = /^-?([1-9]\d*|0)(\.\d+)?$/;
                        testMessage = "* Value must be a decimal number";
                    }
                    
                    if (!testExpression.test(textParts)) {
                        $(this).validationEngine('showPrompt', testMessage, 'error', 'bottomLeft', true);
                        valueError = true;
                    }
                }	

            }
		});

        var newScoredResponses = JSON.parse($('#scoring_json').val());

        if ($('#dans_scoring_header').is(':visible')) {
            for (var i = 0; i < newScoredResponses.length; ++i) {
                if (('ids' in newScoredResponses[i]) && (newScoredResponses[i].ids.length == 0)) {
                    var $thisDetail = $('#dans_scoring_rows .scoring_row').eq(i).find('.mcq_detail_response');
                    $thisDetail.validationEngine('showPrompt', '* Key entries must include at least one answer', 'error', 'bottomLeft', true);
                    valueError = true;
                }
            }
        }

        if (valueError) {

            var newHeight = $('#dans_answer_rows').height();
            $('#dans_answer_rows .formError').each(function() {
                var promptBottomPosition = $(this).position().top + $(this).outerHeight() + 5;
                if (promptBottomPosition > newHeight) newHeight = promptBottomPosition;
            });

            $('#dans_answer_rows').height(newHeight);

        } else {

            if ($form.validationEngine('validate')) {
				newQuestionData = JSON.parse(JSON.stringify(questionData));
				newQuestionSettings = false;

				var index;
				for (index = 0; index < newQuestionData.Parts.length; ++index) {
					if (newQuestionData.Parts[index].id == activePart.id) break;
				}

				if (index < newQuestionData.Parts.length) {
					var thisPart = newQuestionData.Parts[index];

                    // Update answers

                    var newAnswers = [];

                    $('#dans_answer_rows .answer_wrapper').each(function() {
                        var $answerFields = $(this).find('.answer_template');

                        var newEntry = {
                            id: $(this).attr('data-answer-id')
                        };

                        if (thisPart.type == 'mc_table') {
                            var answerList = [];
                            $answerFields.each(function() {
                                answerList.push($(this).htmleditor('getValue'));
                            });
                            newEntry.templates = answerList;
                        } else {
                            newEntry.template = $answerFields.eq(0).htmleditor('getValue');
                        }

                        var rationale = $(this).find('.answer_rationale input').val();
                        if (rationale.length > 0) newEntry.rationale = rationale;

                        newAnswers.push(newEntry);
                    });

					if ('Common' in newQuestionData) {
						newQuestionData.Common.Answers = newAnswers;
					} else thisPart.Answers = newAnswers;

                    // Update scored responses

                    thisPart.ScoredResponses = newScoredResponses;

                    // Remove any scored responses that use now-missing answers

					if ('Common' in newQuestionData) {
                        var allowedIDs = [];
                        for (var i = 0; i < newQuestionData.Common.Answers.length; ++i) {
                            allowedIDs.push(newQuestionData.Common.Answers[i].id);
                        }
                        for (var i = 0; i < newQuestionData.Parts.length; ++i) {
                            if (newQuestionData.Parts[i].type == 'context') continue;

                            var index = 0;
                            while (index < newQuestionData.Parts[i].ScoredResponses.length) {
                                var thisScoredResponse = newQuestionData.Parts[i].ScoredResponses[index];
                                if ('id' in thisScoredResponse) {
                                    if (allowedIDs.indexOf(thisScoredResponse.id) == -1) {
                                        newQuestionData.Parts[i].ScoredResponses.splice(index, 1);
                                    } else ++index;
                                } else ++index;
                            }
                        }
                    }

					// Save variables
	
					setWizardData(newQuestionData, HtmlEdit.getEditorVariables());

					// Save question

					if (pendingAJAXPost) return false;
					showSubmitProgress($dialog);
					pendingAJAXPost = true;
					
					saveQuestionData()
					.then(function() {
						$dialog.dialog("close");
			
						$.gritter.add({
							title: "Success",
							text: "Answer saved.",
							image: "/img/success.svg"
						});
					})
					.catch(function() {
						hideSubmitProgress($dialog);
					});
				} else showSubmitError($dialog);                
            }
        }
    }

    $('#editAnswers_dialog').dialog({
		autoOpen: false,
		width: 600,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
		 	initSubmitProgress($(this));
		}, close: function(event) {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			$('form').validationEngine('hideAll');
			window.onbeforeunload = null;
		}
    });

    $('#editAnswers_dialog').find('button:contains("Cancel")').click(function(){
        $('#editAnswers_dialog').dialog("close");
	});

	$('#editAnswers_dialog').find('button:contains("Save")').click(function(){
		var contextedSave = $.proxy(keySave, $('#editAnswers_dialog'));
		contextedSave();
	});

    $('#dans_add_answer').on('click', function() {
        var $thisRow = addEmptyAnswer();

        // Update interface

        var $firstAnswer = $thisRow.find('.answer_template').eq(0);
        $firstAnswer.htmleditor('initSelection');
        $firstAnswer.focus();

        formatAnswers();
        drawAnswerScores();
    });

    $('#dans_show_key').on('click', function() {
        var scoringVisible = $('#dans_scoring_header').is(':visible');
        updateScoreElements(!scoringVisible);
    });

	$(document).on('focus', '.rubric_html, .answer_template, .answer_value, .wiz_calculated_conditional .edit_parent', function(e) {
		e.preventDefault();

        var $element;
        if ($(this).is('.wiz_calculated_conditional .edit_parent')) $element = $(this).find('.variable_equation');
        else $element = $(this);

        var blurIndex = blurWaiting.indexOf($element);
		if (blurIndex >= 0) blurWaiting.splice(blurIndex, 1);
		focusWaiting.push($element);
	});
	
	$(document).on('blur', '.rubric_html, .answer_template, .answer_value, .answer_rationale input, .wiz_calculated_conditional .edit_parent', function(e) {
        var $element;
        if ($(this).is('.wiz_calculated_conditional .edit_parent')) $element = $(this).find('.variable_equation');
        else if ($(this).is('.answer_rationale input')) $element = $(this).closest('.answer_rationale');
        else $element = $(this);

        var focusIndex = focusWaiting.indexOf($element);
		if (focusIndex >= 0) focusWaiting.splice(focusIndex, 1);
		blurWaiting.push($element);
	});
	
	$(document).on('click keyup', function(e) {
		
		// Handle blur and focus for htmleditor and equationeditor fields on click so that we don't (for example)
		// move dialog buttons out of the way when a field shrinks.
		
		setTimeout(function() {
            var needsReposition = false;
			while (blurWaiting.length > 0) {
				var $element = blurWaiting.pop();
				var didBlur = false;
				if ($element.is('.html_editor')) {
					didBlur = $element.htmleditor('makeInactive');
				} else if ($element.is('.eqn_editor')) {
					$element.equationeditor('makeInactive');
				} else if ($element.is('.answer_rationale')) {
					didBlur = true;
				}

                if (didBlur) {
                    $('#dans_answer_rows .answer_rationale').each(function() {
                        if (($(this).find('input').val().length == 0) && !$(this).find('input').is(':focus')) {
                            $(this).hide();
                        }
                    });
                    needsReposition = true;
                }
			}
			while (focusWaiting.length > 0) {
				var $element = focusWaiting.pop();
				if ($element.is('.html_editor')) $element.htmleditor('makeActive');
				else if ($element.is('.eqn_editor')) $element.equationeditor('makeActive');

                if ($element.is('.answer_template, .answer_value')) {
                    $element.closest('.answer_wrapper').find('.answer_rationale').show();
                    needsReposition = true;
                }
			}
            if (needsReposition) formatAnswers();
		}, 0);
	});

	$('#editRubric_form').dialog({
		autoOpen: false,
		width:700,
		height:535,
		modal: true,
		resizable: true,
		minWidth:700,
		minHeight:400,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
				$(this).dialog("close");
			},
			'Save': function() {
				rubricSave();
			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
			$('#drub_rubric_edit').htmleditor('initSelection');
			initSubmitProgress($(this));
		}, close: function(event) {
			hideSubmitProgress($(this));
		}, beforeClose: function() {
			$('form').validationEngine('hideAll');
			window.onbeforeunload = null;
		}
    });
    
	var disableEditor = function(editor) {
		editor.setOptions({
			readOnly: true,
			highlightActiveLine: false,
			highlightGutterLine: false
		});	
		editor.renderer.$cursorLayer.element.style.opacity = 0;
	}

	var enableEditor = function(editor) {
		editor.setOptions({
			readOnly: false,
			highlightActiveLine: true,
			highlightGutterLine: true
		});	
		editor.renderer.$cursorLayer.element.style.opacity = 1;
	}
	
    /* *
     * * Editing context template
     * */
     
	$('#startEditContext').click(function(e) {
		if (!canEdit) return;

		if ('Common' in questionData) {
			$('#djsq_answer_columns').val($('#jsq_answer_columns').val());
			$('#djsq_shuffle_context_answers').prop('checked', $('#jsq_shuffle_context_answers').prop('checked'));

	    	if ($('#jsq_answer_columns').val() == 0) {
		    	$('#djsq_shuffle_context_answers').attr('disabled', 'disabled');
				$('#djsq_shuffle_context_answers').next('label').addClass('disabled');
	    	} else {
		    	$('#djsq_shuffle_context_answers').removeAttr('disabled');
				$('#djsq_shuffle_context_answers').next('label').removeClass('disabled');
	    	}
		}

		const tableData = getWizardData();
		setCommonContext(tableData);

		$('#djsq_context_edit').htmleditor('setValue', $('#jsq_context_template').html());
		$('#djsq_context_edit').htmleditor('initSelection');
		
		$('#editContext_form').dialog('open');
    });

    /* *
     * * Editing question template
     * */
     
	$('#startEditQuestion').click(function(e) {
		e.preventDefault();
		if (!canEdit) return;

		editPart = JSON.parse(JSON.stringify(activePart));

		openEditQuestion();
	});

	var openEditQuestion = async function() {
		if (editPart.type == 'context') {
			$('#editQuestion_form').dialog('option', 'title', 'Edit Additional Context');
		} else $('#editQuestion_form').dialog('option', 'title', 'Edit Question');

		await drawPartOptions(editPart, 'djsq');

		const tableData = getWizardData();
		setCommonContext(tableData);

		$('#djsq_question_edit').htmleditor('setValue', editPart.template);
		$('#djsq_question_edit').htmleditor('initSelection');

		$('#editQuestion_form').dialog('open');
    }

	$('#djsq_nr_digits').on('change', function() {
		editPart.TypeDetails.nrDigits = parseInt($('#djsq_nr_digits').val());
	});

	$('#djsq_allowSmallErrors').on('click', function() {
		if ($('#djsq_allowSmallErrors').prop('checked') == true) {
			if (userDefaults.Question.TypeDetails.nr.fudgeFactor > 0.0) {
				editPart.TypeDetails.fudgeFactor = userDefaults.Question.TypeDetails.nr.fudgeFactor;
			} else editPart.TypeDetails.fudgeFactor = 1;
		} else editPart.TypeDetails.fudgeFactor = 0;

		drawPartOptions(editPart, 'djsq');
	});

	$('#djsq_allowFactorOfTen').on('click', function() {
		if ($('#djsq_allowFactorOfTen').prop('checked') == true) {
			if (userDefaults.Question.TypeDetails.nr.tensValue > 0.0) {
				editPart.TypeDetails.tensValue = userDefaults.Question.TypeDetails.nr.tensValue;
			} else editPart.TypeDetails.tensValue = 0.5;
		} else editPart.TypeDetails.tensValue = 0.0;
		
		drawPartOptions(editPart, 'djsq');
	});

	$('#djsq_allowPartialMatch').on('click', function() {
		if ($('#djsq_allowPartialMatch').prop('checked')) {
			if (userDefaults.Question.TypeDetails.nr.partialValue > 0.0) {
				editPart.TypeDetails.partialValue = userDefaults.Question.TypeDetails.nr.partialValue;
			} else editPart.TypeDetails.partialValue = 0.5;
		} else editPart.TypeDetails.partialValue = 0.0;

		drawPartOptions(editPart, 'djsq');
	});

	$('#djsq_markByDigits').on('click', function() {
		editPart.TypeDetails.markByDigits = $(this).prop('checked');
		drawPartOptions(editPart, 'djsq');
	});

	$('#djsq_sigDigsBehaviour').on('change', function() {
		editPart.TypeDetails.sigDigsBehaviour = $('#djsq_sigDigsBehaviour').val() 
		if (editPart.TypeDetails.sigDigsBehaviour == 'Strict') {
			editPart.TypeDetails.sigDigsValue = 0.0;
		} else editPart.TypeDetails.sigDigsValue = 1.0;

		drawPartOptions(editPart, 'djsq');
	});
			
	$('#djsq_style').on('change', function() {
		var newStyle = $(this).val();
		if (newStyle == 'mc') editPart.type = 'mc';
		else if (newStyle == 'nr') editPart.type = 'nr_decimal';
		else if (newStyle == 'wr') editPart.type = 'wr';

		initPartType();
	});

	$('#djsq_type').on('change', function() {
		editPart.type = $(this).val();
		initPartType();
	});

	$('#djsq_prompt_text').on('input', function() {
		editPart.TypeDetails.prompt.text = $('#djsq_prompt_text').val();
	});

	var initPartType = function() {

		if (editPart.type != 'mc_table') {
			$('#question_edit .block_bigBlank').html('&nbsp;');
			$('#djsq_question_edit').htmleditor("option", "labelTypes", []);
		}
		
		if ('Common' in questionData) {

			delete editPart.TypeDetails;

		} else if (editPart.type == 'mc') {

			editPart.TypeDetails = {
				shuffleAnswers: true
			}

		} else if (editPart.type == 'mc_table') {

			editPart.TypeDetails = {
				shuffleAnswers: true,
				prompt:  { type: 'simple' },
				labels: ['']
			}

			setTimeout(function() {
				$('#djsq_labelline_0').focus().trigger('click');
			}, 0);

		} else if (editPart.type == 'mc_truefalse') {

			delete editPart.TypeDetails;

			$('#djsq_tfAnswer').val('True');

		} else if ((editPart.type == 'nr_decimal') || (editPart.type == 'nr_scientific')) {

			editPart.TypeDetails = {
				nrDigits: 4,
				prompt: { type: userDefaults.Question.TypeDetails.nr.promptType },
				fudgeFactor: userDefaults.Question.TypeDetails.nr.fudgeFactor,
				tensValue: userDefaults.Question.TypeDetails.nr.tensValue,
				sigDigsBehaviour: userDefaults.Question.TypeDetails.nr.sigDigsBehaviour,
				sigDigsValue: userDefaults.Question.TypeDetails.nr.sigDigsValue
			};

			$('#djsq_numberOptions').show();
			$('#djsq_selectionOptions').hide();

		} else if (editPart.type == 'nr_selection') {

			editPart.TypeDetails = {
				nrDigits: 4,
				markByDigits: userDefaults.Question.TypeDetails.nr.markByDigits,
				partialValue: userDefaults.Question.TypeDetails.nr.partialValue,
				ignoreOrder: userDefaults.Question.TypeDetails.nr.ignoreOrder
			};

			$('#djsq_numberOptions').hide();
			$('#djsq_selectionOptions').show();

		} else if (editPart.type.substr(0, 2) == 'nr') {

			editPart.TypeDetails = {
				nrDigits: 4,
				prompt: { type: userDefaults.Question.TypeDetails.nr.promptType }
			}

			$('#djsq_numberOptions').hide();
			$('#djsq_selectionOptions').hide();

		} else if (editPart.type.substr(0, 2) == 'wr') {

			editPart.TypeDetails = {
				criteria: [],
				height: 1.0
			};

			editPart.TypeDetails.criteria = userDefaults.Question.TypeDetails.wr.criteria;

		    $('#startEditPrompt').attr('disabled', 'disabled');
			rebuildWRCriteria();

		} else delete editPart.TypeDetails;
		
		drawPartOptions(editPart, 'djsq');

		$('#djsq_question_edit').focus();
	}
	
	window.checkUniqueCriteria = function(field, rules, i, options) {
		var aIndex = 0;
		while ($('#djsq_label_' + aIndex).length > 0) {
			var aLabel = $('#djsq_label_' + aIndex).val();
			for (var bIndex = 0; bIndex < aIndex; ++bIndex) {
				var bLabel = $('#djsq_label_' + bIndex).val();
				if (aLabel === bLabel) {
					return options.allrules.checkUniqueCriteria.alertText;
				}
			}
			++aIndex;
		}
	}

	$(document).on('click', '.wrCriteriaAdd', function() {
		rebuildTypeDetails();
		var index = parseInt($(this).attr('data-index')) + 1;
		editPart.TypeDetails.criteria.splice(index, 0, {label: '', value: 1});
   	    rebuildWRCriteria();
		$('#djsq_label_' + index).focus();
    });
    
	$(document).on('click', '.wrCriteriaDelete', function() {
		rebuildTypeDetails();
		var index = parseInt($(this).attr('data-index'));
		editPart.TypeDetails.criteria.splice(index, 1);
   	    rebuildWRCriteria();
    });

    /** Rebuild input fields from the criteria string for a WR question **/
    
    var rebuildWRCriteria = function() {
		var criteria = editPart.TypeDetails.criteria;

	    var add_to = "";
		for (var j = 0; j < criteria.length; ++j) {
			var label = criteria[j].label;
			var value = criteria[j].value;

			add_to += '<div class="options_row">'
			add_to += '<input class="wr_criterion defaultTextBox2';
			if (j == criteria.length - 1) add_to += ' validate[funcCall[checkUniqueCriteria],custom[noPound]]';
			add_to += '" style="width:105px;" type="text" id="djsq_label_' + j + '" value="' + label + '"/>';
			add_to += '<label style="text-align:right; margin-left:10px;" for="djsq_value_' + j + '">Out of:&nbsp</label> \
                <input class="validate[required,custom[isPosInteger],min[1],max[9]] defaultTextBox2 intSpinner wr_outof" style="width:30px !important;" maxlength="1" type="text" id="djsq_value_' + j + '" value="' + value + '"/>';
			add_to += '<button title="Add" class="actions_button add_icon wrCriteriaAdd" style="margin-left:10px;" data-index=' + j + '>&nbsp;</button>';
			add_to += '<button title="Delete" class="actions_button delete_icon wrCriteriaDelete" ' + ((criteria.length == 1) ? 'disabled ' : '') + ' data-index=' + j + '>&nbsp;</button>';
			add_to += '</div>';
		}
		$('#djsq_criteriaWrapper').html(add_to);

		$('#djsq_criteriaWrapper .intSpinner').spinner({
			step: 1,
			min: 1
		});
    }
    
	$(document).on('click', '.labellineAdd', function() {
		rebuildTypeDetails();
		var index = parseInt($(this).attr('data-index')) + 1;
		editPart.TypeDetails.labels.splice(index, 0, "");
		rebuildLabelLines();
		$('#djsq_labelline_' + index).focus().trigger('click');
    });
    
	$(document).on('click', '.labellineDelete', function() {
		rebuildTypeDetails();
		var index = parseInt($(this).attr('data-index'));
		editPart.TypeDetails.labels.splice(index, 1);
   	    rebuildLabelLines();
    });

	$(document).on('keydown', '.wr_criterion', function(e) {
		if (e.key === 'Enter') {
			e.preventDefault();
            var $thisRow = $(this).closest('.options_row');
            if ($thisRow.next().length > 0) $thisRow.next().find('.wr_criterion').focus();
            else $thisRow.find('.wrCriteriaAdd').trigger('click');
        } else if (e.key === 'Tab') {
			e.preventDefault();
            $(this).closest('.options_row').find('.wr_outof').focus();
        }
    });

    $(document).on('keydown', '.wr_outof', function(e) {
		if ((e.key === 'Tab') || (e.key === 'Enter')) {
			e.preventDefault();
            var $thisRow = $(this).closest('.options_row');
            if ($thisRow.next().length > 0) $thisRow.next().find('.wr_criterion').focus();
            else $thisRow.find('.wrCriteriaAdd').trigger('click');
        }
    });

	$('#djsq_nr_digits').on('input', function() {
		var stringValue = $(this).val().trim();
		if ((stringValue != '') && !Number.isNaN(stringValue)) {
			editPart.TypeDetails.nrDigits = parseInt(stringValue);
			drawPartOptions(editPart, 'djsq');
		}
	});

	$('#djsq_show_prompt_nr').on('change', function() {
		if ($(this).val() != 'none') {
			editPart.TypeDetails.prompt = { type: $(this).val() };
			if (editPart.TypeDetails.prompt.type == 'text') editPart.TypeDetails.prompt.text = '';	
		} else delete editPart.TypeDetails.prompt;

		drawPartOptions(editPart, 'djsq');
		
		if (('prompt' in editPart.TypeDetails) && (editPart.TypeDetails.prompt.type == 'text')) {
			$('#djsq_prompt_text').focus();
		}
	});

	function rebuildTypeDetails() {
		var newData;

		if ('Common' in questionData) {
			newData = false;
		} else if (editPart.type == 'mc') {
			newData = {
				shuffleAnswers: $('#djsq_shuffle_answers').prop('checked')
			};
		} else if (editPart.type == 'mc_table') {
			newData = {
				shuffleAnswers: $('#djsq_shuffle_answers').prop('checked')
			};

			if ($('#djsq_show_prompt_mc').prop('checked')) {
				newData.prompt = { type: 'simple' };
			}

			newData.labels = [];
			for (var i = 0; $('#djsq_labelline_' + i).length > 0; ++i) {
				newData.labels.push($('#djsq_labelline_' + i).htmleditor('getValue'));
			}
		} else if (editPart.type.substr(0, 2) == 'nr') {
		 	newData = {
			 	nrDigits: parseInt($('#djsq_nr_digits').val())
			};
			if ($('#djsq_show_prompt_nr').val() != 'none') {
				newData.prompt = { type: $('#djsq_show_prompt_nr').val() };
				if (newData.prompt.type == 'text') {
					newData.prompt.text = $('#djsq_prompt_text').val();
				}	
			}
			if ((editPart.type == 'nr_decimal') || (editPart.type == 'nr_scientific')) {
				newData.fudgeFactor = parseFloat($('#djsq_fudgeFactor').val());
				newData.tensValue = parseFloat($('#djsq_tensValue').val());
				newData.sigDigsBehaviour = $('#djsq_sigDigsBehaviour').val();
				newData.sigDigsValue = parseFloat($('#djsq_sigDigsValue').val());
			} else if (editPart.type == 'nr_selection') {
				newData.markByDigits = $('#djsq_markByDigits').prop('checked');
				newData.partialValue = parseFloat($('#djsq_partialValue').val());
				newData.ignoreOrder = $('#djsq_ignoreOrder').prop('checked');
			}
	 	} else if (editPart.type == 'wr') {
			newData = {
				height: parseFloat($('#djsq_wr_height').val()),
				criteria: []
			};
			for (var i = 0; $('#djsq_label_' + i).length > 0; ++i) {
				var thisLabel = $('#djsq_label_' + i).val();
				var thisScore = parseInt($('#djsq_value_' + i).val());
				newData.criteria.push({label: thisLabel, value: thisScore});
			}
			if ('prompt' in editPart.TypeDetails) {
				newData.prompt = editPart.TypeDetails.prompt;
			}
	 	} else {
			newData = false;
	 	}

		if (newData !== false) editPart.TypeDetails = newData;
		else delete editPart.TypeDetails;
	}

	function rebuildLabelLines() {
		var labelLines = editPart.TypeDetails.labels;
		if (labelLines.length == 0) labelLines = [''];

		const tableData = getWizardData();
		setCommonContext(tableData);

		var index = 0;
		for (index = 0; index < labelLines.length; ++index) {
			var $thisEditor = $('#djsq_labelline_' + index);
			if ($thisEditor.length == 0) {
				var add_to = 
					'<div style="margin-bottom:5px;" class="label_line_wrapper"> \
						<div style="width:220px; display:inline-flex; vertical-align:top;"> \
							<div style="padding:5px; font-family:\'Arial\'; font-size: 12px; height:auto;" id="djsq_labelline_' + index + '" class="djsq_label_view label_line"></div> \
						</div> \
						<div style="display:inline-block; vertical-align:top; margin-top:6px;">';
				add_to += '<button title="Add" class="actions_button add_icon labellineAdd" style="margin-left:10px;" data-index=' + index + '>&nbsp;</button>';
				add_to += '<button title="Delete" class="actions_button delete_icon labellineDelete" data-index=' + index + '>&nbsp;</button>';
				add_to += '</div></div>';

				$('#djsq_label_wrapper').append(add_to);
				var $thisEditor = $('#djsq_labelline_' + index);
				
				$thisEditor.htmleditor({'height': 'auto'});
	
				$thisEditor.htmleditor('hideAllButtons');
				$thisEditor.htmleditor('showElement', 'edit_button_normal');
				$thisEditor.htmleditor('showElement', 'edit_button_superscript');
				$thisEditor.htmleditor('showElement', 'edit_button_subscript');
				$thisEditor.htmleditor('showElement', 'edit_button_symbol');
				$thisEditor.htmleditor('showElement', 'edit_button_settings');
			}
			
			$thisEditor.htmleditor('setValue', labelLines[index]);
			$thisEditor.blur();
		}
		while ($('#djsq_labelline_' + index).length > 0) {
			$('#djsq_labelline_' + index).closest('.label_line_wrapper').remove();
			++index;
		}

		if (labelLines.length == 1) $('.labellineDelete').attr('disabled', 'disabled');
		else $('.labellineDelete').removeAttr('disabled');
		
		$('#djsq_question_edit').htmleditor("option", "labelTypes", labelLines);
	}
	
	$(document).on('focus', '.label_line', function(e) {
		e.preventDefault();
		if ($(this).is('.html_editor')) {
			var label_id = $(this).attr('id');
			var blurIndex = blurWaiting.indexOf(label_id);
			if (blurIndex >= 0) blurWaiting.splice(blurIndex, 1);
			focusWaiting.push($(this));
		}
	});
	
	$(document).on('blur', '.label_line', function(e) {
		if ($(this).is('.html_editor')) {
			var label_id = $(this).attr('id');
			var focusIndex = focusWaiting.indexOf(label_id);
			if (focusIndex >= 0) focusWaiting.splice(focusIndex, 1);
			blurWaiting.push($(this));
		}
	});
	
	$(document).on('savedom', '.label_line', function(e) {
		var labelTypes = [];
		$('.label_line').each(function() {
			labelTypes.push($(this).htmleditor('getValue'));
		});
		$('#djsq_question_edit').htmleditor("option", "labelTypes", labelTypes);
	});

	$(document).on('keydown', '.label_line', function(e) {
		if (e.key === 'Enter') {

			e.preventDefault();
            var $thisRow = $(this).closest('.label_line_wrapper');
			if ($thisRow.next().length > 0) $thisRow.next().find('.label_line').focus().trigger('click');
			else $thisRow.find('.labellineAdd').trigger('click');

		} else if ((e.key === 'Tab') || (e.key === 'ArrowDown')) {

            e.preventDefault();
            var $thisRow = $(this).closest('.label_line_wrapper');
			if ($thisRow.next().length > 0) $thisRow.next().find('.label_line').focus().trigger('click');
			else $('#djsq_question_edit').focus().trigger('click');

        } else if (e.key === 'ArrowUp') {

			e.preventDefault();
            var $thisRow = $(this).closest('.label_line_wrapper');
			if ($thisRow.prev().length > 0) $thisRow.prev().find('.label_line').focus().trigger('click');
		}
	});

    function settingsSave() {
		var $dialog = $(this);
		var $form = $dialog.find('form');

		$('#djsq_answer_location').removeAttr('disabled');
		$('#djsq_answer_location_label').removeClass("disabled");

	 	if ($form.validationEngine('validate')) {
			var oldQuestionDataString = JSON.stringify(questionData);

			newQuestionData = JSON.parse(oldQuestionDataString);
			newQuestionSettings = {
				name: $('#djsq_name').val()
			};

			if (!$('#djsq_open_checkbox')[0].hasAttribute('disabled')) {
				if ($('#djsq_open_checkbox').prop('checked')) {
					newQuestionSettings.open_status = userID;
				} else newQuestionSettings.open_status = 0;
			}

			if (pendingAJAXPost) return false;
			showSubmitProgress($dialog);
			pendingAJAXPost = true;

			if (canWriteCode && ($('#djsq_engine_type').val() != newQuestionData.Engine.type)) {
				newQuestionData.Engine.type = $('#djsq_engine_type').val();	
			}

			if (($('#djsq_answer_location').val() == 'context') && !('Common' in newQuestionData)) {

				// Gather unique answers then remove from parts

				var uniqueAnswers = {};
				var numShuffled = 0;
				var numUnshuffled = 0;
				for (var i = 0; i < newQuestionData.Parts.length; ++i) {
					if (newQuestionData.Parts[i].type == 'context') continue;

					if (newQuestionData.Parts[i].TypeDetails.shuffleAnswers) numShuffled++;
					else numUnshuffled++;

					delete newQuestionData.Parts[i].TypeDetails;

					for (var j = 0; j < newQuestionData.Parts[i].Answers.length; ++j) {
						var answerID = newQuestionData.Parts[i].Answers[j].id;
						delete newQuestionData.Parts[i].Answers[j].id;

						var answerString = JSON.stringify(newQuestionData.Parts[i].Answers[j]);
						var answerCopy = JSON.parse(answerString);

						var hash = SparkMD5.hash(answerString);

						if (!(hash in uniqueAnswers)) {
							uniqueAnswers[hash] = {
								data: answerCopy,
								ids: []
							}
						}
						uniqueAnswers[hash].ids.push(answerID);
					}

					delete newQuestionData.Parts[i].Answers;
				}

				// Attach unique answers to question

				newQuestionData.Common = {
					answerColumns: 1,
					shuffleAnswers: (numShuffled >= numUnshuffled),
					Answers: []
				};

				for (var hash in uniqueAnswers) {
					var newAnswerID = getNewID(4);

					// Update scored responses
					
					for (var i = 0; i < newQuestionData.Parts.length; ++i) {
						if (newQuestionData.Parts[i].type == 'context') continue;

						for (var j = 0; j < newQuestionData.Parts[i].ScoredResponses.length; ++j) {
							if ('id' in newQuestionData.Parts[i].ScoredResponses[j]) {
								var oldAnswerID = newQuestionData.Parts[i].ScoredResponses[j].id;
								if (uniqueAnswers[hash].ids.indexOf(oldAnswerID) >= 0) {
									newQuestionData.Parts[i].ScoredResponses[j].id = newAnswerID;
								}
							} else if ('ids' in newQuestionData.Parts[i].ScoredResponses[j]) {
								for (var k = 0; k < newQuestionData.Parts[i].ScoredResponses[j].ids.length; ++k) {
									var oldAnswerID = newQuestionData.Parts[i].ScoredResponses[j].ids[k];
									if (uniqueAnswers[hash].ids.indexOf(oldAnswerID) >= 0) {
										newQuestionData.Parts[i].ScoredResponses[j].ids[k] = newAnswerID;
									}
								}	
							}
						}
					}
					
					// Save answer

					var questionAnswer = uniqueAnswers[hash].data;
					questionAnswer.id = newAnswerID;
					newQuestionData.Common.Answers.push(questionAnswer);
				}

			} else if (($('#djsq_answer_location').val() == 'parts') && ('Common' in newQuestionData)) {

				// Copy answers from main question element to parts, and remove original

				var answersString = JSON.stringify(newQuestionData.Common.Answers);
				var shuffleAnswers = newQuestionData.Common.shuffleAnswers;
				delete newQuestionData.Common;

				for (var i = 0; i < newQuestionData.Parts.length; ++i) {
					if (newQuestionData.Parts[i].type == 'context') continue;

					newQuestionData.Parts[i].TypeDetails = { shuffleAnswers: shuffleAnswers };
					newQuestionData.Parts[i].Answers = JSON.parse(answersString);

					var idMap = {};
					for (var j = 0; j < newQuestionData.Parts[i].Answers.length; ++j) {
						var oldAnswerID = newQuestionData.Parts[i].Answers[j].id;
						var newAnswerID = getNewID(4);

						newQuestionData.Parts[i].Answers[j].id = newAnswerID;
						idMap[oldAnswerID] = newAnswerID;
					}

					for (var j = 0; j < newQuestionData.Parts[i].ScoredResponses.length; ++j) {
						if ('id' in newQuestionData.Parts[i].ScoredResponses[j]) {
							var oldAnswerID = newQuestionData.Parts[i].ScoredResponses[j].id;
							newQuestionData.Parts[i].ScoredResponses[j].id = idMap[oldAnswerID];
						} else if ('ids' in newQuestionData.Parts[i].ScoredResponses[j]) {
							for (var k = 0; k < newQuestionData.Parts[i].ScoredResponses[j].ids.length; ++k) {
								var oldAnswerID = newQuestionData.Parts[i].ScoredResponses[j].ids[k];
								newQuestionData.Parts[i].ScoredResponses[j].ids[k] = idMap[oldAnswerID];
							}	
						}
					}
				}

			}

			if ($('#djsq_shuffle_parts').prop('checked') != newQuestionData.Settings.shuffleParts) {
				newQuestionData.Settings.shuffleParts = $('#djsq_shuffle_parts').prop('checked');
			}

			var newQuestionDataString = JSON.stringify(newQuestionData);
			if (newQuestionDataString == oldQuestionDataString) newQuestionData = false;

			saveQuestionData()
			.then(function() {
				$dialog.dialog("close");
	
				$.gritter.add({
					title: "Success",
					text: "Settings saved.",
					image: "/img/success.svg"
				});
			})
			.catch(function() {
				hideSubmitProgress($dialog);
			});
		}
    }

	function cancelQuestion() {
		if (pendingAJAXPost) return false;
		showSubmitProgress($(this));
		pendingAJAXPost = true;

		var urlVars = getUrlVars();

		ajaxFetch('/Questions/delete/' + questionID, {
			method: 'POST'
		}).then(async deleteResponse => {
			const deleteResponseData = await deleteResponse.json();
			if (!deleteResponseData.success) {
				throw new Error("Unable to delete question");
			}
	
			if ('a' in urlVars) {

				// Question is being added from an assessment template. Clean that up.

				var assessmentID = parseInt(urlVars.a);
				var handleResponse = await ajaxFetch('/Assessments/delete_handle/' + assessmentID, {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						question_handle_id: questionHandleID
					})
				});

				const handleResponseData = await handleResponse.json();								
				if (!handleResponseData.success) {
					throw new Error("Unable to remove question handle");
				}
	
				assessmentVersionID = handleResponseData.assessment_version_id;

				if (typeof window?.opener?.deleteQuestionCallback === "function") {
	
					var callbackData = {
						question_id: questionID,
						question_handle_id: questionHandleID
					}

					if (assessmentVersionID !== false) {
						callbackData.assessment_version_id = assessmentVersionID;
					}
							
					window.opener.deleteQuestionCallback(callbackData);
					window.close();
	
				}
			}

		}).then(function() {

			addClientFlash({
				title: "Success",
				text: "New question cancelled.",
				image: "/img/success.svg"
			});

			window.location = '/Questions/index';

		}).catch(function() {

			showSubmitError();

		});
	}
    
	$('#editSettings_form').dialog({
		autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { 
	            if (canCancel == 1) cancelQuestion();
		        else $(this).dialog("close");
	        },
			'Save': settingsSave
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
		 	initSubmitProgress($(this));
			$('#djsq_name').focus();
		}, close: function(){
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
    });

	$('#startEditDetails').click(function(e) {
		e.preventDefault();
		if (!canEdit) return;

		$('#djsq_name').val(questionSettings.name);
		if (canWriteCode) $('#djsq_engine_type').val(questionData.Engine.type);

		$('#djsq_shuffle_parts').prop('checked', questionData.Settings.shuffleParts);
		$('#djsq_answer_location').val(('Common' in questionData) ? 'context' : 'parts');
		
		if (questionSettings.open_status != 0) {
			$('#djsq_open_checkbox').prop('checked', true);
			if (questionSettings.open_status == userID) {
				$('#djsq_open_checkbox').removeAttr('disabled');
				$('#djsq_open_checkbox_label').removeClass('disabled');
			} else {
				$('#djsq_open_checkbox').attr('disabled', 'disabled');
				$('#djsq_open_checkbox_label').addClass('disabled');
			}
		} else {
			$('#djsq_open_checkbox').prop('checked', false);
			$('#djsq_open_checkbox').removeAttr('disabled');
			$('#djsq_open_checkbox_label').removeClass('disabled');
		}

		if ($('#djsq_answer_location').val() == 'parts') {
			var mcOnly = true;
			for (var i = 0; i < questionData.Parts.length; ++i) {
				if (questionData.Parts[i].type == 'context') continue;
				if (questionData.Parts[i].type != 'mc') mcOnly = false;
			}
			if (mcOnly) {
				$('#djsq_answer_location').removeAttr('disabled');
				$('#djsq_answer_location_label').removeClass("disabled");
			} else {
				$('#djsq_answer_location').attr('disabled', 'disabled');
				$('#djsq_answer_location_label').addClass("disabled");
			}
		}

		$('#editSettings_form').dialog('open');
		if (showSettings) $('#djsq_name').select();
		else $('#djsq_name').focus();
    });

    /* *
     * * initialize dialog box for code editor
     * */
     
	$('#editCode_form').dialog({
		autoOpen: false,
		width:600,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
            'Cancel': function() { $(this).dialog("close"); },
			'Save': codeSave
		}, open: function(event) {
		 	var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		 	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-danger btn_font');
		 	$buttonPane.find('button:contains("Save")').addClass('btn btn-primary btn_font');
			initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
    });

	$('#startEditCode').click(function(e) {
		e.preventDefault();
		if (!canEdit) return;

		codeEditor.setValue(codeView.getValue(), -1);
		$('#editCode_form').dialog('open');
	   	codeEditor.resize();
    });

	function codeSave() {
		var $dialog = $(this);

		newQuestionData = JSON.parse(JSON.stringify(questionData));
		newQuestionSettings = false;

		newQuestionData.Engine.js = codeEditor.getValue();

		if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		saveQuestionData()
		.then(function() {
			$dialog.dialog("close");

			$.gritter.add({
				title: "Success",
				text: "Code saved.",
				image: "/img/success.svg"
			});
		})
		.catch(function() {
			hideSubmitProgress($dialog);
		});
    }
	
	// Initialize statistics filters
	
	$('#stats_update').on('mousedown', function(e) {
		e.preventDefault();
		
		$('#statistics_graphs').html("<div style='font-style:italic'>Loading statistics</div>");
		$('#item_statistics_data').html("<div style='font-style:italic'>Loading statistics</div>");
		$('#statistics_n').html('0');

		showSubmitProgress($('#stats_button_wrapper'));

		StatsCommon.loadFilteredStatistics([questionID])
		.then(function() {
			StatsCommon.rebuildItemStatistics(questionID, questionData, 'Numbers');
			hideSubmitProgress($('#stats_button_wrapper'));
		});
	});
	
	function updateHighlighting() {
		$('.answer_row').removeClass('item_highlighted');

		var partIndex = 1;
		var discriminationParts = [];
		var answerParts = [];
		for (var rawIndex = 0; rawIndex < questionData.Parts.length; ++rawIndex) {
			var partData = questionData.Parts[rawIndex];
			if (partData.type == 'context') continue;

			if ((questionID in StatsCommon.fullStatistics) && ('QuestionParts' in StatsCommon.fullStatistics[questionID]) && 
				(partData.id in StatsCommon.fullStatistics[questionID].QuestionParts)) {
				var partStatistics = StatsCommon.fullStatistics[questionID].QuestionParts[partData.id];

                var totalCount = 0;
                for (var i = 0; i < partStatistics.length; ++i) {
                    for (var j = 0; j < partStatistics[i].length; ++j) {
                        totalCount += partStatistics[i][j].n;
                    }
                }

				var partAnswers;
				if ('Common' in questionData) partAnswers = questionData.Common.Answers;
				else partAnswers = partData.Answers;

				var discrimination = StatsCommon.getDiscrimination(partStatistics, partData);
	
				if ((discrimination !== false) && (totalCount > 100)) {
					if (discrimination < 0.15) discriminationParts.push(partIndex);

                    var hasBadAnswer = false;
					if (partData.type.substring(0, 2) == 'mc') {
						var hasMultipleScored = false;
						for (var i = 0; i < partData.ScoredResponses.length; ++i) {
							if ('ids' in partData.ScoredResponses[i]) hasMultipleScored = true;
						}
						
						if (!hasMultipleScored) {

							for (var answerIndex = 0; answerIndex < partAnswers.length; ++answerIndex) {
								var answerData = partAnswers[answerIndex];
								var $answerWrapper = $('.answer_row[data-answer-id="' + answerData.id + '"]');
		
                                var thisCount = 0;
								for (var i = 0; i < partStatistics.length; ++i) {
                                    for (var j = 0; j < partStatistics[i].length; ++j) {
										if ('ids_matched' in partStatistics[i][j]) {
											var ids = partStatistics[i][j].ids_matched;
											if ((ids.length == 1) && (ids[0] == answerData.id)) {
												thisCount += partStatistics[i][j].n;
											}
										}
                                    }
								}
                                var percent = thisCount * 100 / totalCount;
		
								if (percent == 0.0) {
									$answerWrapper.bubbletip({
										html: 'This answer has not been selected by students.'
									});
									hasBadAnswer = true;
								} else if (percent < 5.0) {
									$answerWrapper.bubbletip({
										html: 'This answer has been selected by only ' + percent.toFixed(1) + '% of students.'
									});
									hasBadAnswer = true;
								} else if ($answerWrapper.is('.item_highlighted')) {
									$answerWrapper.bubbletip('destroy');
								}
							}						
						}
					}
					
					if (hasBadAnswer) answerParts.push(partIndex);
				}
			}

			partIndex++;
		}
		
		var messageHTML = "";
		if (discriminationParts.length > 0) {
			messageHTML += "<div>";
			if (questionData.Parts.length == 1) messageHTML += "This question has ";
			else if (discriminationParts.length == 1) messageHTML += "Part " + discriminationParts[0] + " has ";
			else {
				messageHTML += "Parts";
				for (var i = 0; i < discriminationParts.length; ++i) {
					if (i == discriminationParts.length - 1) messageHTML += " and";
					else if (i > 0) messageHTML += ",";
					messageHTML += " " + discriminationParts[i];
				}
				messageHTML += " have ";
			}
			messageHTML += "low discrimination. The language used in the question or answers may be confusing even \
				to stronger students. <a href='https://www.youtube.com/watch?v=LkMErsoLAhI' target='_blank'>Click \
				here</a> to learn more about average score and discrimination.</div>";
		}
		if (answerParts.length > 0) {
			if (discriminationParts.length > 0) {
				messageHTML += "<div style='margin-top:10px;'>In addition, some answers in ";
			} else messageHTML += "<div>Some answers in "
			
			if (questionData.Parts.length == 1) messageHTML += "this question";
			else if (answerParts.length == 1) messageHTML += "part " + answerParts[0];
			else {
				messageHTML += "parts";
				for (var i = 0; i < answerParts.length; ++i) {
					if (i == answerParts.length - 1) messageHTML += " and";
					else if (i > 0) messageHTML += ",";
					messageHTML +=  " " + answerParts[i];
				}
			}

			messageHTML += " are selected by very few students, so tell you and your students little about what they \
				understand and what they don't. ";
			if (questionData.open_status != 0) messageHTML += 'You can help improve the open bank by modifying these \
				to include more effective distractors! ';
			messageHTML += 'Hover over highlighted answers or click "Item statistics" on the left to learn \
				more.</div>';
		}
		
		if (messageHTML.length > 0) {
			$('#highlighting_message').html(messageHTML);
			$('#messages_panel').show();
		} else $('#messages_panel').hide();
	}

    // Smart versioning

    $('#smart_version_input').on('keydown', function(e) {
		if (e.key === 'Enter') {
            e.preventDefault();
            $('#smart_version_send').trigger('click');
        }
    });

    function updateVersionStatus(requestID) {
		ajaxFetch('/Questions/get_version_status/' + requestID)
		.then(async response => {
			const responseData = await response.json();
		
            if (responseData.status == 'success') {

                $('#versioning_wrapper .wait_icon').css('opacity', 0);
    
                var isValid = true;
				if (!('data_string' in responseData)) isValid = false;
				else if (!('data_hash' in responseData)) isValid = false;
				else if (responseData.data_hash != SparkMD5.hash(responseData.data_string)) isValid = false;

                if (isValid) {

                    $('#smart_version_input').removeAttr('disabled');
                    $('#smart_actions_dropdown').removeAttr('disabled');
                    $('#smart_version_undo').attr('disabled', 'disabled');
                    $('#smart_version_redo').attr('disabled', 'disabled');
        
                    questionData = JSON.parse(responseData.data_string);
                    delete StatsCommon.fullStatistics[questionID];
    
                    drawView();
    
                    ++versionIndex;
                    versionData = versionData.slice(0, versionIndex);
                    versionData.push(JSON.stringify(questionData));
    
                    $('#smart_version_undo').removeAttr('disabled');
    
                    $('#smart_version_input').val('');
                    $('#smart_version_send').addClass('disabled-link');
                    $('#smart_version_save').removeClass('disabled-link');
        
                    $.gritter.add({
                        title: "Success",
                        text: 'New version built. Click "Actions > Save changes" to apply changes.',
                        image: "/img/success.svg"
                    });
    
                    window.onbeforeunload = function() {
                        return true;
                    };
            
                } else {

                    $('#smart_version_input').removeAttr('disabled');
                    $('#smart_actions_dropdown').removeAttr('disabled');
    
                    $.gritter.add({
                        title: "Error",
                        text: "There was a problem processing your request.",
                        image: "/img/error.svg"
                    });

                }

            } else if (responseData.status == 'failed') {

                $('#smart_version_input').removeAttr('disabled');
                $('#smart_actions_dropdown').removeAttr('disabled');
                $('#versioning_wrapper .wait_icon').css('opacity', 0);
    
                $.gritter.add({
                    title: "Error",
                    text: "There was a problem processing your request.",
                    image: "/img/error.svg"
                });
    
            } else {

                setTimeout(function() {
                    updateVersionStatus(requestID);
                }, 2000);    

            }
            
        }).catch(function() {

            setTimeout(function() {
                updateVersionStatus(requestID);
            }, 2000);

        });
    }

    $('#smart_version_send').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

		if (pendingAJAXPost) return false;
        pendingAJAXPost = true;

        $('#smart_version_input').attr('disabled', 'disabled');
        $('#smart_actions_dropdown').attr('disabled', 'disabled');
        $('#versioning_wrapper .wait_icon').css('opacity', 1);

		ajaxFetch('/Questions/version_start', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				request: $('#smart_version_input').val(),
				data_string: JSON.stringify(questionData)
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {

                var usedText = responseData.smart_used.toString() + ' requests';
                if (responseData.smart_used == 0) usedText = 'no requests';
                else if (responseData.smart_used == 1) usedText = '1 request';
            
                $('#smart_used').html(usedText);

                updateVersionStatus(responseData.request_id);

            } else {

				throw new Error("Unable to start request.");

			}

        }).catch(function(error) {

            $('#smart_version_input').removeAttr('disabled');
            $('#smart_actions_dropdown').removeAttr('disabled');
            $('#versioning_wrapper .wait_icon').css('opacity', 0);

            $.gritter.add({
                title: "Error",
                text: error.message,
                image: "/img/error.svg"
            });

        });
    });

    $('#smart_version_save').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

        newQuestionData = questionData;

        if (pendingAJAXPost) return false;
        pendingAJAXPost = true;

        $('#smart_version_input').attr('disabled', 'disabled');
        $('#smart_actions_dropdown').attr('disabled', 'disabled');
        $('#versioning_wrapper .wait_icon').css('opacity', 1);

        saveQuestionData()
        .then(function() {

            $('#smart_version_input').removeAttr('disabled');
            $('#smart_actions_dropdown').removeAttr('disabled');
            $('#versioning_wrapper .wait_icon').css('opacity', 0);

            $('#smart_version_send').addClass('disabled-link');
            $('#smart_version_save').addClass('disabled-link');
    
            window.onbeforeunload = null;

            $.gritter.add({
                title: "Success",
                text: "Smart assistant changes saved.",
                image: "/img/success.svg"
            });
        });
    });

    $('#smart_version_undo').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

        if (versionIndex > 0) {
            
            versionIndex--;
            var dataString = versionData[versionIndex];
            questionData = JSON.parse(dataString);

            drawView();
    
            $('#smart_version_input').val('');
            $('#smart_version_send').addClass('disabled-link');

            $('#smart_version_redo').removeAttr('disabled');
            if (versionIndex == 0) {
                $('#smart_actions_dropdown').attr('disabled', 'disabled');
                $('#smart_version_save').addClass('disabled-link');
                $('#smart_version_undo').attr('disabled', 'disabled');

                window.onbeforeunload = null;
            }    
        }
    });

    $('#smart_version_redo').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

        if (versionIndex < versionData.length - 1) {
            
            versionIndex++;
            var dataString = versionData[versionIndex];
            questionData = JSON.parse(dataString);

            drawView();
    
            $('#smart_version_input').val('');
            $('#smart_version_send').addClass('disabled-link');

            $('#smart_actions_dropdown').removeAttr('disabled');
            $('#smart_version_save').removeClass('disabled-link');
            $('#smart_version_undo').removeAttr('disabled');

            window.onbeforeunload = function() {
                return true;
            };

            if (versionIndex == versionData.length - 1) {
                $('#smart_version_redo').attr('disabled', 'disabled');
            }
    
            window.onbeforeunload = null;
        }
    });

    /* *
     * * Initial setup
     * */
     
    $('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : "bottomLeft"
    }).submit(function(e){ e.preventDefault() });

	$('#djsq_question_edit').htmleditor({
		defaultVariableType: 'randomized'
	});
	
	$('#djsq_context_edit').htmleditor({
		defaultVariableType: 'randomized'
	});
	var $contextAnwers = $('<div id="djsq_answer_wrapper" class="contextAnswers" style="padding:10px; opacity:0.3;"></div>');
	$contextAnwers.insertAfter($('#djsq_context_edit'));
	
	$('#drub_rubric_edit').htmleditor({
		defaultVariableType: 'randomized'
	});

	$('#editCalculated_form .variable_equation').each(function() {
		$(this).equationeditor();
		$(this).equationeditor('option', 'restrictEquation', true);

		$(this).equationeditor('hideElement', 'edit_button_text');
		$(this).equationeditor('hideElement', 'edit_button_symbol');
		$(this).equationeditor('hideElement', 'edit_button_accent');
		$(this).equationeditor('hideElement', 'edit_button_sup_sub');
		$(this).equationeditor('hideElement', 'edit_button_matrix');
		$(this).equationeditor('hideElement', 'edit_button_other');
		$(this).equationeditor('hideElement', 'edit_button_brackets');
	});
	
	$('#wiz_calculated_equation, #wiz_conditional_then, #wiz_conditional_else').equationeditor('option', 'blockedKeys', [].concat(
		'abcedfghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
		['%', '=', '>', '<', '{', '[', '_', ',', "'"]
	));

	$('#wiz_conditional_if').equationeditor('option', 'blockedKeys', [].concat(
		'abcedfghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
		['%', '{', '[', '_', ',', "'"]
	));

    /* *
     * * initialize note for first part added
     * */
     
	$('#editPart_note').dialog({
		autoOpen: false,
		width:600,
		modal: true,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, buttons: {
			'Thanks!': function() {
				if ($('#partnote_hide').prop('checked')) {

					ajaxFetch('/Users/hide_tip/questions_intro', {
						method: 'POST'
					}).then(async response => {
						pendingAJAXPost = false;
						const responseData = await response.json();
						if (responseData.success) showPartNote = false;
					}).catch(function() {
						showSubmitError();
					});
				}
				$(this).dialog('close');
				if (showSettings) $("#startEditDetails").trigger("click");
			}
		}, open: function(event) {
		 	$('.ui-dialog-buttonpane').find('button:contains("Thanks!")').addClass('btn btn-primary btn_font');
		}, close: function() {
			$('form').validationEngine('hideAll');
		}
    });
    
    $('#editPart_note').parent().find(".ui-dialog-buttonpane").append(
    	'<div style="float:left; margin:10px 10px 0px;"> \
			<div class="labeled-input"> \
				<input type="checkbox" class="normal_chkbx" id="partnote_hide" /><label style="text-align:left;" for="partnote_hide">Don\'t show this message again</label> \
			</div> \
		</div>'
    );

	$('#view_menu').menu({
		disabled: true
	});

	initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});
		window.location.href = '/';
	});

});

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as RenderTools from '/js/assessments/render_tools.js?7.0';
import * as AssessmentsRender from '/js/assessments/render.js?7.0';

var shuffle = function(data) {
    for (var i = 0; i < data.length; ++i) {
        var j = i + Math.floor(Math.random() * (data.length - i));
        if (i != j) {
            var temp = data[i];
            data[i] = data[j];
            data[j] = temp;	
        }
    }
}

document.addEventListener('DOMContentLoaded', async function() {
    
    await AssessmentsRender.initialize();

    var $target = $('#question_preview_wrapper .question_wrapper');

    // Shuffle as required

    AssessmentsRender.shuffleQuestionParts(questionData);

    if ('Common' in questionData) {
        if (questionData.Common.shuffleAnswers) {
            shuffle(questionData.Common.Answers);
        }
    } else {
        for (var k = 0; k < questionData.Parts.length; ++k) {
            if ((questionData.Parts[k].type == 'mc') || (questionData.Parts[k].type == 'mc_table')) {
                if (questionData.Parts[k].TypeDetails.shuffleAnswers) {
                    shuffle(questionData.Parts[k].Answers);
                }
            }
        }
    }

    // Build a workspace for variable values if required

    if (('Engine' in questionData) && ('data' in questionData.Engine)) {
        var variableRows = "<div id='variable_rows'>";

        var tableData = questionData.Engine.data;

        var allVariables = [];
        for (var type in tableData) {
            if (tableData.hasOwnProperty(type) && (type != 'deleted')) {
                allVariables = allVariables.concat(tableData[type]);
            }
        }

        for (var i = 0; i < allVariables.length; ++i) {
            variableRows += "<div>" + escapeHTML(allVariables[i].displayName) + "</div>";
            variableRows += "<div><span class='block_variable' data-variable-name='" + allVariables[i].variableName + "'>" + 
                allVariables[i].displayName + "</span></div>";
            variableRows += "<div></div>";
            variableRows += "<div class='variable_range' data-variable-name='" + allVariables[i].variableName + "'></div>";
        }

        variableRows += "</div>";
        $target.append(variableRows);
    }

    // Build a workspace for scored responses

    var scoredRows = "<div id='scored_rows'>";
    var hasAnswers = false;

    for (var partIndex = 0; partIndex < questionData.Parts.length; ++partIndex) {
        var partData = questionData.Parts[partIndex];

        if (partData.type.substr(0, 2) == 'mc') {

            var answerMap = {};
            var allAnswers = ('Common' in questionData) ? questionData.Common.Answers : partData.Answers;
            var answerLabel = "A".charCodeAt(0);
            for (var answerIndex = 0; answerIndex < allAnswers.length; ++answerIndex) {
                var answerID = allAnswers[answerIndex].id;
                if (partData.type == 'mc_truefalse') {
                    answerMap[answerID] = allAnswers[answerIndex].template[0];
                } else {
                    answerMap[answerID] = String.fromCharCode(answerLabel++);
                }
            }

            if (partData.ScoredResponses.length > 0) hasAnswers = true;
            for (var scoredIndex = 0; scoredIndex < partData.ScoredResponses.length; ++scoredIndex) {
                scoredRows += "<div class='answers_table_row' style='margin-bottom:5px;'>";

                if (scoredIndex == 0) {
                    scoredRows += "<div class='answer_preview_number'><span class='preview_question_number' data-part-id='" + 
                        partData.id + "'></span></div>";
                } else scoredRows += "<div class='answer_preview_number'></div>";
                
                scoredRows += "<div class='answer_preview_value'>" + partData.ScoredResponses[scoredIndex].value + "</div>";

                scoredRows += "<div class='answer_preview_answer'>";
                if ('id' in partData.ScoredResponses[scoredIndex]) {
                    scoredRows += answerMap[partData.ScoredResponses[scoredIndex].id];
                } else if ('ids' in partData.ScoredResponses[scoredIndex]) {
                    for (var j = 0; j < partData.ScoredResponses[scoredIndex].ids.length; ++j) {
                        if (j > 0) {
                            if (j < partData.ScoredResponses[scoredIndex].ids.length - 1) scoredRows += ", "
                            else scoredRows += " and ";
                        }
                        scoredRows += answerMap[partData.ScoredResponses[scoredIndex].ids[j]];
                    }							
                }					
                scoredRows += "</div>";

                scoredRows += "</div>";
            }
                
        } else if (partData.type.substr(0, 2) == 'nr') {
            
            var answerMap = {};
            for (var answerIndex = 0; answerIndex < partData.Answers.length; ++answerIndex) {
                var answerID = partData.Answers[answerIndex].id;
                answerMap[answerID] = partData.Answers[answerIndex].template;
            }

            if (partData.ScoredResponses.length > 0) hasAnswers = true;
            for (var scoredIndex = 0; scoredIndex < partData.ScoredResponses.length; ++scoredIndex) {
                scoredRows += "<div class='answers_table_row' style='margin-bottom:5px;'>";

                if (scoredIndex == 0) {
                    scoredRows += "<div class='answer_preview_number'><span class='preview_question_number' data-part-id='" + 
                        partData.id + "'></span></div>";
                } else scoredRows += "<div class='answer_preview_number'></div>";
                
                scoredRows += "<div class='answer_preview_value'>" + partData.ScoredResponses[scoredIndex].value + "</div>";

                scoredRows += "<div class='answer_preview_answer'>";
                if ('id' in partData.ScoredResponses[scoredIndex]) {
                    scoredRows += answerMap[partData.ScoredResponses[scoredIndex].id];
                }
                
                var extra = "";
                if (partData.type == 'nr_selection') {
                    if (partData.TypeDetails.markByDigits) extra += ", marks are per digit";
                    if (partData.TypeDetails.ignoreOrder) extra += ", any order";
                } else if ('fudgeFactor' in partData.TypeDetails) {
                    var fudgeFactor = parseFloat(partData.TypeDetails.fudgeFactor);
                    if (fudgeFactor != 0.0) {
                        if (fudgeFactor < 1.0) fudgeFactor *= 100.0;
                        extra += ", within " + fudgeFactor + "%";
                    }
                }
                if (extra.length > 0) scoredRows += extra;

                scoredRows += "</div>";

                scoredRows += "</div>";
            }
                
        } else if (partData.type == 'wr') {

            if (('Rubric' in partData) && ('template' in partData.Rubric)) {
                scoredRows += "<div class='answers_table_row' style='margin-bottom:5px;'>";
                scoredRows += "<div class='answer_preview_number'><span class='preview_question_number' data-part-id='" + 
                    partData.id + "'></span></div>";
                scoredRows += "<div class='answer_preview_value'>WR</div>";

                scoredRows += "<div class='answer_preview_answer'>";
                scoredRows += partData.Rubric.template;
                scoredRows += "</div>";

                scoredRows += "</div>";
                hasAnswers = true;
            }

        }
    }

    if (hasAnswers) {
        scoredRows += "</div>";
        $target.append(scoredRows);
    }

    // Load render style and render assessment

    await RenderTools.setRenderStyle('Numbers');
    await AssessmentsRender.renderItemToDiv(questionData, $target);

    // Move variable rows to their correct location

    if ($('#variable_rows').length > 0) {
        $('#variables_table').append($('#variable_rows').children());
        $('#variable_rows').remove();
        $('#variables_wrapper').show();
    }

    // Move scored responses to their correct location

    if ($('#scored_rows').length > 0) {
        $('#answers_table').append($('#scored_rows').children());
        $('#scored_rows').remove();
        $('#answers_wrapper').show();
    }

    // Finish rendering question

    $('#question_preview_wrapper .part_wrapper').each(function() {
        if ($(this).find('.question_answer_mc, .question_answer_table').length > 0) {
            RenderTools.setMCImageWidths($(this));
        }

        var setWidths = false;
        if ($(this).attr('data-type') == 'mc') {
            var imagesOnly = true;
            $(this).find('.answer_text').each(function() {
                if ($(this).children().length == 1) {
                    if (!$(this).children().eq(0).is('img')) imagesOnly = false;
                } else imagesOnly = false;
            });
            if (imagesOnly) setWidths = true;
        }

        if (setWidths) {
            if ($(this).find('.question_answer_mc').length > 0) RenderTools.setMCWidths($(this));
        } else $(this).find('.question_answer_mc').css('width', '100%');
    });

    await RenderTools.renderStyles['Numbers'].renumber($('#question_preview_wrapper'));	

    // Transfer question numbers to variable and scored response tables

    $('.preview_question_number').each(function() {
        var partID = $(this).attr('data-part-id');
        var number = $target.find('.part_wrapper[data-part-id="' + partID + '"] .question_number').html();
        $(this).html(number);
    });

    // Sample variable values

    setTimeout(function() {
        RenderTools.sampleVariables(questionData);
    }, 0);
});
/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as IndexCommon from '/js/common/index.js?7.0';
import * as Folders from '/js/common/folders.js?7.0';
import * as PreviewCommon from '/js/common/preview.js?7.0';

function drawIndexRow(data) {

	// Add a row in the index table for this entry

	var canCopy = 1;
	var canDelete = 0;
	if (('Permissions' in data) && (data['Permissions'].indexOf('can_delete') >= 0)) canDelete = 1;

	var html = "<tr data-question-id='" + data['Question']['id'] + "' ";
	if ('UserQuestion' in data) html += " data-userquestion-id='" + data['UserQuestion']['id'] + "'";
	html += "data-can-copy='" + canCopy + "' data-can-delete='" + canDelete + "'>";

	html += '<td style="width:15px; text-align:center;">' + 
		'<input type="checkbox" class="item_select" style="margin:1px 0px;" autocomplete="off" />' + 
		'</td>';

	if ('User' in data) {
		html += '<td style="width:250px;">' + data['User']['email'] + '</td>';
	}

	if ('UserQuestion' in data) {
    	var date = dayjs.utc(data['UserQuestion']['moved']).local();
        html += '<td style="width:120px;">' + date.format('YYYY-MM-DD HH:mm') + '</td>';
    }

	html += '<td><div style="display:flex; flex-direction:row; align-items:flex-start; gap:5px;">';
	if ('UserQuestion' in data) html += '<div class="index_folder_link" data-folder-id="' + data['UserQuestion']['folder_id'] + '"></div>';
	if (data['question_type'] != null) html += '<div class="index_type_label" data-type="' + data['question_type'] + '"></div>';
	if (('UserQuestion' in data) && (data['UserQuestion']['is_new'] >= 2)) html += '<div class="index_new_label label label-info">New</div>';
	html += '<div class="index_item_name">' + 
		(('QuestionHandle' in data) ? escapeHTML(data['QuestionHandle']['name_alias']) : escapeHTML(data['Question']['name'])) +
		'</div>';
	html += '</div></td>';

	html += "</tr>";

	$('.index_table').append(html);
}

function finishIndex() {
	if (IndexCommon.activeNode === false) {

		$('#share_dropdown').removeAttr('disabled');
		$('#sendCopy_button').removeClass('always-disabled-link');
		$('#newItem_button').hide();

	} else {
			
		// Update button availability

		if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_export') >= 0) {
			$('#share_dropdown').removeAttr('disabled');
			$('#sendCopy_button').removeClass('always-disabled-link');
		} else {
			$('#share_dropdown').attr('disabled', 'disabled');
			$('#sendCopy_button').addClass('always-disabled-link');
		}

		// Set up right-side tab buttons

		$('.tab_right').hide();
		if (IndexCommon.activeNode.id == IndexCommon.roots['Trash'].id) $('#emptyTrash_button').show();
		else {
			var canAdd = Folders.isItemFolder(treeData, IndexCommon.activeNode.id);
			if (IndexCommon.activeNode.data.content_permissions.all.indexOf('can_add') == -1) canAdd = false;
			if (canAdd) {
				$('#newItem_button .tab_icon').css('background-image', 'url("/img/tab_icons/add_item_icon.svg")');
				$('#newItem_button .tab_text').html('New<br />Question');
				$('#newItem_button').show();
			} else $('#newItem_button').hide();
		}

		// Check for an empty index

		if ($('.index_table tr').length == 0) {
			var html = "";
			html += '<tbody><tr class="disabled"><td colspan="5">';
			if (IndexCommon.activeNode.data.folder_id == 'open') {
				html += '<div style="display:flex;">';
				html += '<div style="flex:0 0 30px; padding:3px;"><img src="/img/info.svg" /></div>';
    			html += '<div style="flex:1 1 100%; padding-left:10px; line-height:normal; font-size:14px; font-family:Arial, Helvetica, sans-serif;">';
				html += 'Click "Search" to find questions in the open bank. Questions in the open bank have been contributed by other teachers ';
				html += '&mdash; please use them respectfully.';
			    html += '</div>';
				html += '</div>';
			} else {
				html += '<div style="padding-left:10px;"><i>No questions to show here.';
				if (IndexCommon.activeNode.id != IndexCommon.roots['Trash'].id) html += ' Click "New Question" in the menu bar to create one.';	
				html += '</i></div>';
			}
			html += '</td></tr></tbody>';
			$('.index_table').html(html);
		}
	}

	// Modify sorting criteria

    var urlVars = getUrlVars();

	var optionsHTML = '';
    if ((IndexCommon.activeNode !== false) && (IndexCommon.activeNode.data.folder_id == 'open')) {

        $('#sort_type').attr('disabled', 'disabled');
		$('#sort_direction_wrapper').hide();

        $('#show_limit').val('10');
        $('#show_limit').attr('disabled', 'disabled');
		
		optionsHTML = '<option>Relevance</option>';

        delete urlVars.sort;
        delete urlVars.direction;
        setUrlVars(urlVars);

	} else {

		$('#sort_type').removeAttr('disabled');
		$('#sort_direction_wrapper').show();

        $('#show_limit').val('10');
        $('#show_limit').removeAttr('disabled');

        optionsHTML = 
            '<option value="QuestionHandle.name_alias" data-default-direction="asc">Name</option> \
            <option value="UserQuestion.moved" data-default-direction="desc" selected>Created</option>';
	}

	$('#sort_type').html(optionsHTML);
    if ('sort' in urlVars) $('#sort_type').val(urlVars.sort);
}

var previewQuestions = [];
var loadingQuestionPreview = false;

document.addEventListener("DOMContentLoaded", async function() {

    IndexCommon.initialize({ drawIndexRow, finishIndex });
    await PreviewCommon.initialize();

	$('.preview_button').on('click', function() {
		var numChecked = 0;
		var questionID = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				questionID = $(this).closest('tr').attr('data-question-id');
				numChecked++;
			}
		});
		
		if (numChecked == 1) {
			$('#preview_dialog').dialog('open');
			PreviewCommon.updateQuestionPreview(questionID);
		}
	});

	$('#preview_dialog').dialog({
		autoOpen: false,
		width: 600,
		height:350,
		modal: true,
		resizable: true,
		minWidth:600,
		minHeight:350,
		position: {
			my: "center center",
			at: "center center",
			of: window
		}, open: function(event) {
			$(this).find('button').blur();
		}, close: function(){
			$('form').validationEngine('hideAll');
		}, beforeClose: function() {
			window.onbeforeunload = null;
		}
    });

	$(document).on('keydown', function(e) {
        if ($('#preview_dialog').is(':visible')) {
            if (e.key === 'Escape') {
                e.preventDefault();
                $('#preview_close').trigger('click');	
            } else if (e.key === ' ') {
                e.preventDefault();
                $('#preview_close').trigger('click');	
            } else if ((e.key === 'ArrowLeft') || (e.key === 'ArrowUp')) {
                e.preventDefault();
                $('#preview_prev').trigger('click');	
            } else if ((e.key === 'ArrowRight') || (e.key === 'ArrowDown')) {
                e.preventDefault();
                $('#preview_next').trigger('click');
            }
        } else if (($('.ui-dialog:visible').length == 0) && !$(e.target).is("input:not([readonly]), textarea")) {
            if (e.key === ' ') {
                e.preventDefault();
                $('.preview_button:visible').trigger("click");
            }
        }
	});

	$('#preview_prev').click(function() {
		if (this.hasAttribute('disabled')) return;
		$('.preview_nav').attr('disabled', true);

        var $activeRow = $('.item_select:checked').closest('tr');
        if ($activeRow.length == 1) {
            IndexCommon.highlightPrevious($activeRow).then(function() {
                $('.item_select:checked').trigger('click');
                var $newRow = $('tr.doc-hover');
                $newRow.trigger('click');

                var questionID = $newRow.attr('data-question-id');
                PreviewCommon.updateQuestionPreview(questionID);
                $('.preview_nav').removeAttr('disabled');
            }).catch(function() {
                $('.preview_nav').removeAttr('disabled');
            });;    
        }
	});

	$('#preview_next').click(function() {
		if (this.hasAttribute('disabled')) return;
		$('.preview_nav').attr('disabled', true);

        var $activeRow = $('.item_select:checked').closest('tr');
        if ($activeRow.length == 1) {
            IndexCommon.highlightNext($activeRow).then(function() {
                $('.item_select:checked').trigger('click');
                var $newRow = $('tr.doc-hover');
                $newRow.trigger('click');

                var questionID = $newRow.attr('data-question-id');
                PreviewCommon.updateQuestionPreview(questionID);
                $('.preview_nav').removeAttr('disabled');
            }).catch(function() {
                $('.preview_nav').removeAttr('disabled');
            });
        }
	});

	$('#preview_close').click(function() {
		$('#preview_dialog').dialog('close');
	});

	function editQuestion($tableRow) {
		var $questionID = $tableRow.attr('data-question-id');
		var redirectString = '/Questions/build/' + $questionID;
		
		if ($tableRow[0].hasAttribute('data-userquestion-id')) {
			var $userQuestionID = $tableRow.attr('data-userquestion-id');
			redirectString = redirectString + '?u=' + $userQuestionID;
		}
		
		window.location.href = redirectString;
	}

	$('.editQuestion_button').click(function(e){
		e.preventDefault();
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked == 1) editQuestion($selectedCheckbox.closest('tr'));
	});
		
	$(document).on('dblclick', '.index_table tbody tr', function(e) {
		e.preventDefault();
		
		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});
		
		if (numChecked == 0) editQuestion($(this));
		else if (numChecked == 1) {
			var $selectedRow = $selectedCheckbox.closest('tr');
			if ($selectedRow[0] == $(this)[0]) editQuestion($(this));
		}
	});

	var urlVars = getUrlVars();
	if ('search' in urlVars) {
		var search = urlVars.search.replace(/\+/g, '%20')
		$('#search_input').val(decodeURIComponent(search));
	}
	if ('where' in urlVars) {
		var where = urlVars.where.replace(/\+/g, '%20')
		$('#search_type').val(decodeURIComponent(where));
	}

	$('.banner_button').on('click', function(e) {
		e.preventDefault();
		
		$(this).prop('disabled', true);

		var set_open_created = ajaxFetch('/Users/set_open_created/', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ 
				value: 1
			})
		}).then(response => response.json());

		var share_created = ajaxFetch('/Users/open_quick/', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				source: $('#banner_wrapper').attr('data-banner-name')
			})
		}).then(response => response.json());

		Promise.all([set_open_created, share_created])
		.then(([result_set, result_share]) => {
			if (result_set.success && result_share.success) {
				setTimeout(function() {
					$("#banner_wrapper").animate({
						'height': 0, 
						'margin-top': 0, 
						'margin-bottom': 0
					}, {
						'duration': 500, 'complete': function() { 
							$("#banner_wrapper").remove(); 
						}
					});
				}, 3000);
		
				$.gritter.add({
					title: "Thank you!",
					text: "Including your questions, there are now " +
						result_share.num_open.toLocaleString() + " in the open bank.",
					image: "/img/success.svg"
				});					
				
		        var end = Date.now() + 3000;
		        var interval = setInterval(function () {
		          if (Date.now() > end) {
		            return clearInterval(interval);
		          }
		
		          confetti({
		            startVelocity: 30,
		            spread: 360,
		            ticks: 60,
		            shapes: ['square'],
		            origin: {
		              x: Math.random(),
		              y: Math.random() - 0.2
		            }
		          });
		        }, 200);
	        } else {
				$.gritter.add({
					title: "Error",
					text: "Problem sharing questions",
					image: "/img/error.svg"
				});					
	        }
        });
	});

});


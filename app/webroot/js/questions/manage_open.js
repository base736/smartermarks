/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as Folders from '/js/common/folders.js?7.0';

$(document).ready(function() {
	
	$('#save_default').on('click', function() {
    	if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		ajaxFetch('/Users/set_open_created/', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				value: $('#open_created').prop('checked') ? 1 : 0
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				$.gritter.add({
					title: "Success",
					text: "Updated user preferences",
					image: "/img/success.svg"
				});					
			}
			
		});
	});
	
	$('#open_quick').on('click', function() {
    	if (pendingAJAXPost) return false;
		pendingAJAXPost = true;

		ajaxFetch('/Users/open_quick/', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				source: 'manage_open_tool'
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
	        if (responseData.success) {
		        $('#open_empty_wrapper').css('display', 'block');
		        $('#open_quick_wrapper').css('display', 'none');

				if ($('#folderPanel').jstree('get_selected').length > 0) {
					var json = Folders.getSelectedNode("folderPanel");
					var folder_id = json.data.folder_id;
					updateQuestions(folder_id);
				}

				$.gritter.add({
					title: "Thank you!",
					text: "Including your questions, there are now " +
					responseData.num_open.toLocaleString() + " in the open bank.",
					image: "/img/success.svg"
				});					
				
		        var end = Date.now() + 3000;
		        var interval = setInterval(function () {
		          if (Date.now() > end) {
		            return clearInterval(interval);
		          }
		
		          confetti({
		            startVelocity: 30,
		            spread: 360,
		            ticks: 60,
		            shapes: ['square'],
		            origin: {
		              x: Math.random(),
		              y: Math.random() - 0.2
		            }
		          });
		        }, 200);
	        }
	    });
	});
	
	$('#update_open_bank').on('click', function() {
		if ($('#folderPanel').jstree('get_selected').length > 0) {
			$('#folderPanel').validationEngine('hideAll');
			
	    	if (pendingAJAXPost) return false;
			pendingAJAXPost = true;
	
			var json = Folders.getSelectedNode("folderPanel");
			var folder_id = json.data.folder_id;
	
			ajaxFetch('/Questions/update_open_bank/', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					folder_id: folder_id,
					new_policy: $('#new_policy').val(),
					created_only: ($("#created_only").prop('checked') ? 1 : 0)
				})
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
	
				if (responseData.success) {
	
					var json = Folders.getSelectedNode("folderPanel");
					var folder_id = json.data.folder_id;
					updateQuestions(folder_id);
					
					if (responseData.new_policy == 'open') {
						$.gritter.add({
							title: "Thank you!",
							text: "Including your questions, there are now " + 
								responseData.num_open.toLocaleString() + " in the open bank.",
							image: "/img/success.svg"
						});					
					} else {
						$.gritter.add({
							title: "Success",
							text: "Questions made private where possible. Skipped " + 
								responseData.skipped.toLocaleString() + 
								" question" + ((responseData.skipped == 1) ? '' : 's') + ".",
							image: "/img/success.svg"
						});					
					}
					
					if (responseData.private_count > 0) {
				        $('#open_empty_wrapper').css('display', 'none');
						$('#private_count').html(responseData.private_count.toLocaleString());
				        $('#open_quick_wrapper').css('display', 'flex');
					} else {
				        $('#open_empty_wrapper').css('display', 'block');
				        $('#open_quick_wrapper').css('display', 'none');
					}
				}
				
			});
		} else {
			$('#folderPanel').validationEngine('showPrompt', '* Select a folder with questions to update', 'error', 'bottomLeft', true);
		}
	});
	
	if ($('#treeData').html().length) {
		var $treeData = JSON.parse($('#treeData').html());
		$(".folderTree").each(function() {
			Folders.initFolderTree($(this), $treeData);
			$(this).jstree('close_all');
			$(this).jstree('deselect_all');
		});
	}

	function updateQuestions(folder_id) {
		ajaxFetch('/UserQuestions/get_open/' + folder_id)
		.then(async response => {
			const responseData = await response.json();

	        if (responseData.unshared == 0) $('#questions_count').html('(all open)');
	        else if (responseData.unshared == responseData.count) $('#questions_count').html('(all private)');
	        else $('#questions_count').html('(' + responseData.unshared + ' private)');
	        
	        var newContent = "";
	        for (var i = 0; i < responseData.data.length; ++i) {
		        var icon;
		        if (responseData.data[i].policy == 'open') icon = 'question_open';
		        else if (responseData.data[i].policy == 'locked') icon = 'question_locked';
		        else icon = 'question_private';
		        
		        newContent += "<tr class='question_row'><td style='line-height:20px;'><div style='display:inline-block; margin-right:3px;'><img class='type_icon' style='width:22px;' src='/img/icons/" + icon + ".png' /></div><span class='question_name'>" + responseData.data[i].name + "</span></td></tr>";
	        }
	        if (responseData.count > responseData.data.length) {
		        newContent += "<tr class='disabled'><td style='border:none;'><i>&hellip;Another " + (responseData.count - responseData.data.length) + " questions not shown</i></td></tr>";
	        }
	        $('#folder_questions').html(newContent);
			$('#folder_questions').parent().animate({ scrollTop: 0 }, "fast");
		});
	}

	$("#folderPanel").bind("select_node.jstree", function (event, data) {
		var folder_id = data.node.data.folder_id;
		updateQuestions(folder_id);
	});
	
	$('#new_policy').on('change', function() {
		if ($('#new_policy').val() == 'private') $('#open_note').show();
		else $('#open_note').hide();
	});

    $('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : "bottomLeft"
    }).submit(function(e){ e.preventDefault() });

	if (openCreated) $('#open_created').prop('checked', true);
	else $('#open_created').prop('checked', false);

});

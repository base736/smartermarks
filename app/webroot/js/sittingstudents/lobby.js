/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as SittingStudentsCommon from '/js/sittingstudents/common.js?7.0';

$(document).ready(function() {	
	$('#cancelButton').on('click', function() {
		window.location.href = '/';
	});

	$('#startButton').on('click', function() {
		if ($(this).hasClass('disabled')) return;

		var $form = $('#inputs_form');
		if ($form.validationEngine('validate')) {
			var postData = {};
			
			if ($('.data_input').length > 0) {
				var inputData = {};
				$('.data_input').each(function() {
					inputData[$(this).attr('data-key')] = $(this).val();
				});
				postData.input_data = JSON.stringify(inputData);
			}

			if (pendingAJAXPost) return false;
			showSubmitProgress($('#nav_buttons'));
			pendingAJAXPost = true;
			
			if ($('#save_input').prop('checked') && (userData != null)) {
				postData.user_id = userData.id;
			}

			ajaxFetch('/SittingStudents/save_details/' + sittingStudent_id, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(postData)
			}).then(async response => {
				pendingAJAXPost = false;
				const responseData = await response.json();
	
				if (responseData.success) {

					if (responseData.needsID) {
						hideSubmitProgress($('#nav_buttons'));
						$('.data_input[data-key="' + join_field + '"]').validationEngine('showPrompt', '* Value not in class list', 'error', 
							'bottomLeft', true);
					}
					if (responseData.needsCode) {
						hideSubmitProgress($('#nav_buttons'));
						$('.data_input[data-key="Start code"]').validationEngine('showPrompt', '* Bad start code', 'error', 
							'bottomLeft', true);
					}
					if (responseData.canOpen) {
                        Cookies.set('lastAuthorized', dayjs().format(), { expires: 1 });
						window.location.href = '/SittingStudents/view_open/' + sittingStudent_id;
					}
				} else showSubmitError($('#nav_buttons'));

			}).catch(function() {
				showSubmitError($('#nav_buttons'));
			});
		}
	});

    $(document).on('input', '.data_input[data-key="Start code"]', function(e) {
		$(this).val($(this).val().toUpperCase());
    });

	var updateWaitTime = function() {
		var secondsRemaining = Math.ceil((assessmentStart - Date.now()) / 1000.0);
		if (secondsRemaining > 0.0) {
			if (secondsRemaining < 3600) {
				var minutes = Math.ceil(secondsRemaining / 60.0);
				$('#waitTime').text(minutes + " minute" + ((minutes == 1) ? '' : 's'));
			} else if (secondsRemaining < 86400) {
				var hours = Math.floor(secondsRemaining / 3600.0);
				var minutes = Math.ceil((secondsRemaining - 3600 * hours) / 60.0);
				$('#waitTime').text(hours + " hour" + ((hours == 1) ? '' : 's') + ' ' +
					minutes + " minute" + ((minutes == 1) ? '' : 's'));
			} else {
				var days = Math.round(secondsRemaining / 86400.0);
				$('#waitTime').text(days + " day" + ((days == 1) ? '' : 's'));
			}
	
			$('#readyNotice').hide();			
			$('#waitNotice').show();			
		} else {
			$('#waitNotice').hide();
			$('#readyNotice').show();
			
			$('#startButton').text('Start now');
			$('#startButton').removeClass('disabled');
		}
	}
	
	var sittingDetails = SittingStudentsCommon.getSittingDetails(sittingStudent);
	$('#sitting_details').html(sittingDetails);

	if (dataLines.length > 0) {
		var formHTML = "";
		for (var i = 0; i < dataLines.length; ++i) {
			formHTML += 
				'<div class="labeled-input" style="width:100%;"> \
					<label class="data_label">' + dataLines[i] + ':</label> \
					<input class="data_input validate[required] defaultTextBox2" data-key="' + dataLines[i] + '" \
						value="' + ((dataLines[i] in details) ? details[dataLines[i]] : '') + '" type="text" ' +
						((dataLines[i] in details) ? 'disabled' : '') + '/> \
				</div>';
		}
		$('#inputs_form').html(formHTML);
		$('#inputs_wrapper').show();
	}
	
	if (needsUser) {
		$('#save_wrapper').show();
		$('#save_label').text('Save assessment to my user (' + userData.email + ')');
	}

	var maxWidth = 0;
	$('.data_label').each(function() {
		if ($(this).width() > maxWidth) maxWidth = $(this).width();
	});
	$('.data_label').width(maxWidth);

    $('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : "bottomLeft"
    }).submit(function(e){ e.preventDefault() });

	initSubmitProgress($('#nav_buttons'));
	
	updateWaitTime();
	setInterval(updateWaitTime, 1000);
});

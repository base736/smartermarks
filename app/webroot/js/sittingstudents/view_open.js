/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as SittingStudentsCommon from '/js/sittingstudents/common.js?7.0';
import * as SittingsCommon from '/js/sittings/common.js?7.0';
import * as ScrollingCommon from '/js/common/scrolling.js?7.0';
import * as VersioningCommon from '/js/common/versioning.js?7.0';
import * as HtmlEdit from '/js/HTMLEditor/html_edit.js?7.0';
import * as RenderTools from '/js/assessments/render_tools.js?7.0';

import { Annotation, ColourPicker } from '/js/Annotation/annotation.js?7.0';

$.extend($.ui.dialog.prototype.options, {
	resizable: false
});

$.fn.reverse = [].reverse;

var assessmentData = null;
var questionData = null;
var sittingData = null;

var annotationObjects = [];

// Minimal (and hopefully highly compatible) browser fingerprint

function BrowserFP() {}

BrowserFP.value = null;

BrowserFP.get = function() {
	if (this.value == null) {
		try {

			var dataList = {};
	
			var date = new Date();
			date.setTime(0);
			dataList.dateFormat = date.toLocaleString();
	
			dataList.userAgent = window.navigator.userAgent;
	
			dataList.availSize = [window.screen.availWidth, window.screen.availHeight];
			dataList.availSize.sort();
	
			this.value = SparkMD5.hash(JSON.stringify(dataList));

		} catch (e) {
	
			this.value = '';
	
		}	
	}

	return this.value;
}

var item_click_resolve = function() {};
var item_click_reject = function() {};

document.addEventListener("DOMContentLoaded", async function() {

    await SittingsCommon.initialize();
    ScrollingCommon.initialize();
    await RenderTools.initialize();
    await HtmlEdit.initialize();

	var nameLinesData = null;
	var presentation = null;
	var responseOnline_id = null;
	var responseTimeout = null;
	var expiredTimeout = null;
	var windowFocussed = true;

    var numberingType = false;

	var $assessment_wrapper = null;
	
	var notification_timers = [];
	for (var i = 0; i < notificationTimes.length; ++i) {
		notification_timers.push({
			time: notificationTimes[i],
			timeout: null
		});
	}
	
	var assessmentEnd = null;
		
	var createResponseOnline = function() {

		// Unrender equations
		
		RenderTools.unrenderEquations($assessment_wrapper);

        // Get layout sections
		
		var partEntries = VersioningCommon.getPartData($assessment_wrapper, assessmentData, questionData);

		var layoutSections = [];
		for (var i = 0; i < assessmentData.Sections.length; ++i) {
			if (assessmentData.Sections[i].type == 'page_break') continue;

			var sectionData = assessmentData.Sections[i];

			var sectionEntry = {
				id: sectionData.id,
				Content: []
			};

			for (var j = 0; j < assessmentData.Sections[i].Content.length; ++j) {
				if (assessmentData.Sections[i].Content[j].type == 'shuffle_break') continue;
				var itemData = assessmentData.Sections[i].Content[j];

				var questionHandleID;
				if (itemData.type == 'item') {
					questionHandleID = itemData.question_handle_id;
				} else if (itemData.type == 'item_set') {
					var renderedIndex = ('rendered_index' in itemData) ? itemData.rendered_index : 0;
					questionHandleID = itemData.question_handle_ids[renderedIndex];
				}

				for (var k = 0; k < questionData[questionHandleID].json_data.Parts.length; ++k) {
					var partID = questionData[questionHandleID].json_data.Parts[k].id;
					for (var p = 0; p < partEntries.length; ++p) {
						var isMatch = true;
						if (partEntries[p].Source.question_handle_id != questionHandleID) isMatch = false;
						else if (partEntries[p].Source.part_id != partID) isMatch = false;

						if (isMatch) {
							partEntries[p].id = getNewID(4);
							sectionEntry.Content.push(partEntries[p]);
						}
					}
				}
			}

			layoutSections.push(sectionEntry);
		}

		// Check for negative answers in calculated responses

		$assessment_wrapper.find('.hidden_answer_list .question_answer').each(function() {
            if ($(this).text()[0] == '-') {
				$(this).closest('.preview_section_wrapper').attr('data-allow-negative', '');
			}
		});

		// Remove hidden answers

		$assessment_wrapper.find('.hidden_answer_list').remove();

		// Save HTML and layout	

		var layoutJSON = JSON.stringify(layoutSections);
		var versionHTML = $assessment_wrapper.html();

		ajaxFetch('/SittingStudents/create_attempt/' + sittingStudent_id, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				layout_hash: SparkMD5.hash(layoutJSON),
				layout_sections: layoutJSON,
				html_hash: SparkMD5.hash(versionHTML),
				html: versionHTML
			})
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();

            if (responseData.success) {
				if ('time_remaining' in responseData) initTimer(responseData.time_remaining);
				else $('#time_panel').hide();
	            responseOnline_id = responseData.response_online_id;

				// Finish building assessment view

				finishAssessmentView();

				if (presentation != 'all') {
					$('.menu_item[data-index="0"]').trigger('click');
				}
			} else {

				addClientFlash({
					title: "Error",
					text: "Unable to start attempt.",
					image: "/img/error.svg"
				});

				window.onbeforeunload = null;
		        window.location.replace('/');
	        }

		}).catch(function() {
			window.onbeforeunload = null;
	        window.location.replace('/');
		});
	}
	
	var finishAssessmentView = function() {

		// Clean up data

		assessmentData = false;
		questionData = false;
		sittingData = false;

		// Send enter event

		logEvent('enter');

		// Build left bar

		SittingStudentsCommon.drawLeftBar($assessment_wrapper);

		if (presentation == 'all') {
			finishRender($assessment_wrapper);
		} else {
			$assessment_wrapper.find('.question_wrapper').hide();
			$assessment_wrapper.find('.preview_header').hide();
		}

		// Add name lines if needed

		SittingsCommon.addNameLines($assessment_wrapper, nameLinesData);

		// Attach htmleditor to wr fields

		$assessment_wrapper.find('.wr_input_field').each(function() {
			$(this).htmleditor({'height': 'auto'});
			$(this).htmleditor('hideElement', 'edit_button_variable');
			$(this).htmleditor('hideElement', 'edit_button_blank');
			$(this).blur();
		});
		$(document).trigger('click');

		$('#wr_buffer').htmleditor();

		// Finish preparing view

		$assessment_wrapper.css('opacity', '1.0');

		$('#empty_preview').hide();
		$('#navigation_wrapper').show();
		$('.submit_wrapper').show();

        ScrollingCommon.resetLeftPanel();

		$('.custom-buttonpane').each(function() {
			initSubmitProgress($(this));
		});
	}

	var finishRender = async function($target) {
		await RenderTools.renderEquations($target);
		
		$target.find('.question_answer').css('width', '100%');
		$target.find('.part_wrapper:visible').each(function() {
			if ($(this).find('.question_answer_mc, .question_answer_table').length > 0) {
				RenderTools.setMCImageWidths($(this));
			}
		});
	}

	var saveAnnotation = function(questionElement) {
		const contentID = questionElement.getAttribute('data-content-id');
		var thisAnnotations = {};
		$(questionElement).children('.annotation_wrapper').each(function() {
			const thisPaths = this.innerHTML.trim();
			if (thisPaths.length > 0) thisAnnotations[$(this).attr('data-tool-name')] = thisPaths;
		});

		var sendAnnotations = {};
		sendAnnotations[contentID] = thisAnnotations;

		var url = '/SittingStudents/save_annotations/' + sittingStudent_id;
		var data = {
			response_online_id: responseOnline_id,
			annotations: JSON.stringify(sendAnnotations)
		};

		if ("sendBeacon" in navigator) {
			var formData = new FormData();
			for (var key in data) formData.append(key, data[key]);
			navigator.sendBeacon(url, formData);
		} else {
			ajaxFetch(url, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(data)
			});
		}
	}
	
	var initialize = async function() { 

		// Build annotation toolbar

		const $annotationToolWrapper = $(annotation_tool_wrapper);
		$annotationToolWrapper.append(`
			<div class="annotation_set_type selected" data-type="none">
				<img src="/img/icons/annotation_off.svg" style="flex:none; width:24px; height:24px;" />
				<span style="flex:1 1 100%;">Annotation off</span>
			</div>
		`);

		for (const [toolName, toolData] of Object.entries(Annotation.tools)) {
			$annotationToolWrapper.append(`
				<div class="annotation_set_type" data-type="${toolName}">
					<img class="annotation_icon" src="${toolData.iconURL}" />
					<div style="flex:1 1 100%;">${toolData.description}</div>
				</div>
			`);
		}

		$annotationToolWrapper.append(`
			<div id="colour_picker_toggle" class="annotation_set_type" data-type="colour_picker" data-action="open">
				<img src="/img/icons/annotation_colour.svg" style="flex:none; width:24px; height:24px;" />
				<span style="flex:1 1 100%;">Show pen details</span>
			</div>
		`);

		ColourPicker.initialize();
		ColourPicker.$dialog.on("dialogopen", function() {
            $('#colour_picker_toggle').find('span').text('Hide pen details');
			$('#colour_picker_toggle').attr('data-action', 'close');
        }).on("dialogclose", function() {
            $('#colour_picker_toggle').find('span').text('Show pen details');
			$('#colour_picker_toggle').attr('data-action', 'open');
        });

		// Get attempt and render preview

		const response = await ajaxFetch('/SittingStudents/get_open/' + sittingStudent_id);
		const responseData = await response.json();

		if (responseData.success) {
			$assessment_wrapper = $('#assessment_preview_wrapper');

			sittingData = responseData.Sitting;
			$('#document_title').html(sittingData.name);	

			nameLinesData = responseData.StudentDetails;
			presentation = sittingData.View.presentation;

			if ('Version' in responseData) {

				var urlVars = getUrlVars();

				var questionHandleID = false;
				if ('h' in urlVars) {
					questionHandleID = urlVars.h;
					delete urlVars.h;
				}

				var partID = false;
				if ('p' in urlVars) {
					partID = urlVars.p;
					delete urlVars.p;
				}

				setUrlVars(urlVars);

				// Continue working on an existing attempt

				responseOnline_id = responseData.response_online_id;

				numberingType = responseData.Version.settings.numberingType;

				await SittingsCommon.previewFromHTML($assessment_wrapper, responseData.Version.html, responseData.Version.settings, sittingData.View);
				finishAssessmentView();

				if ('responses' in responseData) {
					var result = SittingsCommon.fillResponses($assessment_wrapper, responseData.responses);
					if (!result) {
						$.gritter.add({
							title: "Error",
							text: "Unable to read responses.",
							image: "/img/error.svg"
						});					
					}
				}

				if (presentation == 'next') {

					var maxIndex = ('max_index' in responseData) ? responseData.max_index : 0;
					$('.items_panel .menu_item[data-index="' + maxIndex + '"]').trigger('click');

				} else if ((questionHandleID !== false) && (partID !== false)) {

					$('.items_panel .menu_item[data-question-handle-id="' + questionHandleID + '"]' + 
						'[data-part-id="' + partID + '"]').trigger('click');

				} else {

					$('.items_panel .menu_item').eq(0).trigger('click');

				}

				// Initialize timer

				if ('time_remaining' in responseData) initTimer(responseData.time_remaining);
				else $('#time_panel').hide();
	
			} else if ('Assessment' in responseData) {
				
				// Create a new attempt

				assessmentData = responseData.Assessment;
				numberingType = assessmentData.Settings.numberingType;

				questionData = {};
				for (var id in responseData.Questions) {
					if (responseData.Questions.hasOwnProperty(id)) {
						var questionHandleID = responseData.Questions[id].question_handle_id;
						questionData[questionHandleID] = responseData.Questions[id];
					}
				}
	
				await SittingsCommon.previewConstruct($assessment_wrapper, assessmentData, questionData, sittingData.View);
				createResponseOnline();
				
			} else {

				throw new Error("Unable to start attempt.");
				
			}

			var annotations = {};
			if ('annotations' in responseData) {
				annotations = responseData.annotations;
			}

			$('.question_wrapper').each(function() {
				annotationObjects.push(new Annotation(this, saveAnnotation));
				const contentID = $(this).attr('data-content-id');
				if (contentID in annotations) {
					SittingsCommon.drawAnnotations($(this), annotations[contentID]);
				}
			});

		} else {

			var message = "Unable to start attempt.";
			if ('message' in responseData) message = responseData.message;

			throw new Error(message);
			
		}
	}

	$(document).on('click', '.annotation_set_type', function() {
		if ($(this).attr('data-type') == 'colour_picker') {
			
			if ($(this).attr('data-action') == 'open') {
				ColourPicker.open();
			} else if ($(this).attr('data-action') == 'close') {
				ColourPicker.close();
			}

		} else {

			$('.annotation_set_type').removeClass('selected');
			$(this).addClass('selected');
	
			var newToolName = false;
			if ($(this).attr('data-type') == 'none') {
				$('.question_wrapper').removeClass('drawing');
			} else {
				$('.question_wrapper').addClass('drawing');
				newToolName = $(this).attr('data-type');
			}
	
			for (let annotation of annotationObjects) {
				annotation.setTool(newToolName);
				if (Annotation.tools[newToolName].defaults) {
					const newDefaults = Annotation.tools[newToolName].defaults;
					ColourPicker.setColour(newDefaults.stroke);
					ColourPicker.setPenWidth(newDefaults.strokeWidth);
				}
			}

		}
	});

	$(document).on('change', '.annotation_colour_input', function() {
		const toolName = $(this).closest('.annotation_set_type').attr('data-type');
		const toolData = Annotation.tools[toolName];
		toolData.colour = $(this).val();
	});
	
	$(document).on('click', '.leave_assessment', function() {
		var responsesJSON = JSON.stringify(getResponses());

		$('.custom-buttonpane').each(function() {
			showSubmitProgress($(this));
		});

		var url = '/SittingStudents/save_data/' + sittingStudent_id;
		var data = {
			response_online_id: responseOnline_id,
			fingerprint: BrowserFP.get(),
			client_time: Date.now(),
			responses_hash: SparkMD5.hash(responsesJSON),
			responses: responsesJSON
		};
		
		ajaxFetch(url, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(data)
		}).then(async response => {
			const responseData = await response.json();

			if (responseData.success) {	
				
				var $questionWrappers = $assessment_wrapper.find('.part_wrapper');	  
				var responses = getResponses();

				var blankQuestions = [];
				var index = 0;
				$questionWrappers.each(function() {
					if (responses[index].value.trim().length == 0) {
						var thisNumber = $(this).find('.question_number').html().slice(0, -1);
						if (numberingType == 'Alpha') {
							var $contextWrapper = $(this).prevAll('.context_wrapper:first');
							if ($contextWrapper.length > 0) {
								thisNumber = $contextWrapper.find('.question_number').html().slice(0, -1) + thisNumber;
							}
						} else if (numberingType == 'Type') {
							thisNumber = $(this).attr('data-type').substr(0, 2).toUpperCase() + thisNumber;
						}
						blankQuestions.push(thisNumber);
					}
					++index;
				});

				if (blankQuestions.length == 0) {
					$('#leaveAssessment_start').text('All questions confirmed answered.');
				} else {
					var startText = 'No answer for ';
					if (blankQuestions.length == 1) startText += 'question ';
					else startText += 'questions ';
					for (var i = 0; i < blankQuestions.length; ++i) {
						if ((i > 0) && (i == blankQuestions.length - 1)) startText += " and ";
						else if (i > 0) startText += ", ";
						startText += blankQuestions[i];
					}
					startText += ". ";

					$('#leaveAssessment_start').text(startText);
				}

				$('.custom-buttonpane').each(function() {
					hideSubmitProgress($(this));
				});
		
				$('#leaveAssessment_dialog').dialog('open');

			} else if (('badSitting' in responseData) && responseData.badSitting) {

				window.location.replace('/student_removed');

			} else if (('isClosed' in responseData) && responseData.isClosed) {

				addClientFlash({
					title: "Error",
					text: "Assessment closed.",
					image: "/img/error.svg"
				});

				window.location.replace('/');

			} else $('#save_warning').show();
		}).catch(function(error) {
			$('#save_warning').show();
		});
	});
	
	var closeAttempt = function() {
		var $dialog = $(this);

	    if (pendingAJAXPost) return false;
		showSubmitProgress($dialog);
		pendingAJAXPost = true;

		ajaxFetch('/SittingStudents/close/' + sittingStudent_id, {
			method: 'POST'
		}).then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
		
			if (responseData.success) {
				window.onbeforeunload = null;
				window.location.href = '/SittingStudents/view_closed/' + sittingStudent_id;
			} else showSubmitError($dialog);

		}).catch(function() {
			showSubmitError($dialog);
		});
	}

	$('#leaveAssessment_dialog').dialog({
        autoOpen: false,
		width: 375,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				$(this).dialog("close");
			}, 'Submit and close': closeAttempt
		},
		open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Submit and close")').addClass('btn btn-primary btn_font');
	        $buttonPane.find('button').blur();
	        initSubmitProgress($(this));
		}, close: function() {
			$('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$(document).on('click', '#sections_menu .menu_item', function(e) {
        e.preventDefault();

		var sectionID = $(this).attr('data-section-id');
		SittingStudentsCommon.selectSection(sectionID);
    });

    $(document).on('click', '.items_panel .menu_item', function(e) {
		e.stopPropagation();

		if (!$(this).is('.active')) {
			var questionHandleID = $(this).attr('data-question-handle-id');
			var partID = $(this).attr('data-part-id');
			var $target = $('#assessment_preview_wrapper .question_wrapper[data-question-handle-id="' + questionHandleID + '"] .part_wrapper[data-part-id="' + partID + '"]');

			var itemCheckPromise;
			if (presentation == 'all') {

				itemCheckPromise = Promise.resolve();

			} else {

				itemCheckPromise = new Promise(function(resolve, reject) {
					var $target_item = $target.closest('.question_wrapper');
					var $visible_items = $assessment_wrapper.find('.question_wrapper:not(.shuffle_break):visible');
	
					item_click_resolve = async function() {
						$target.trigger('selected');
	
						$assessment_wrapper.find('.question_wrapper').hide();
						$assessment_wrapper.find('.preview_header').hide();	
					
						$target_item.show();
						var $target_header = $target_item.prevAll('.preview_header:first');
						$target_header.show();

						finishRender($assessment_wrapper);

						if ($visible_items.length == 0) $target = $();
						else if ($target.prevAll('.part_wrapper').length == 0) {
							if ($target_header.length > 0) $target = $target_header;
						}
	
						updateNavButtons();
	
						resolve();
					};
	
					item_click_reject = function(message) {
						if (typeof message == 'undefined') reject();
						else reject(new Error(message));
					}
	
					var $items = $assessment_wrapper.find('.question_wrapper:not(.shuffle_break)');
					if ($items.filter(':visible').length > 0) {
						var canClick = false;
						if (presentation == 'free') canClick = true;
						else if ($target_item.is(':visible')) canClick = true;
						else {
							var itemIndex = $items.index($target_item);
							if (itemIndex > 0) {
								if ($items.eq(itemIndex - 1).is(':visible')) canClick = true;
							}
						}
		
						if (canClick) {
							if ((presentation == 'next') && !($target.is(':visible'))) {
								var needsAnswer = false;
								var needsWarning = true;
								$assessment_wrapper.find('.part_wrapper').each(function() {
									var trimmedResponse = getResponse($(this)).trim();
									if ($(this).is(':visible')) {
										if (trimmedResponse.length == 0) needsAnswer = true;	
									} else if (trimmedResponse.length > 0) needsWarning = false;
								});
	
								if (needsAnswer) $('#emptyItem_dialog').dialog('open');
								else if (needsWarning) $('#nextItem_dialog').dialog('open');
								else item_click_resolve();
							} else item_click_resolve();
						} else item_click_reject('This assessment does not allow you to move to the selected question.');
					} else item_click_resolve();
				});

			}

			itemCheckPromise.then(function() {
	
				// Select the item in the assessment preview
				
				SittingStudentsCommon.selectQuestion(questionHandleID, partID);
	
				if ($target.length > 0) {
					if ($target.prev().is('.context_wrapper')) {
						$target = $target.prevAll('.context_header:first');
					}
			
					var questionTop = $target.offset().top;
					$("html, body").animate({
						scrollTop: questionTop - 10
					}, 1000);		
				}
			}).catch(error => {
				if (typeof error !== 'undefined') {
					$.gritter.add({
						title: "Error",
						text: error.message,
						image: "/img/error.svg"
					});						
				}
			});
		}
	});

	var updateNavButtons = function() {
		var $firstItem = $assessment_wrapper.find('.question_wrapper').first();
		var $lastItem = $assessment_wrapper.find('.question_wrapper').last();
	
		if ($firstItem.is(':visible')) $('.item_previous').hide();
		else $('.item_previous').show();
	
		if ($lastItem.is(':visible')) $('.item_next').hide();
		else $('.item_next').show();

		var allAnswered = true;
		var othersAnswered = true;
		$assessment_wrapper.find('.part_wrapper').each(function() {
			var response = getResponse($(this));
			if (response.trim().length == 0) {
				if (!$(this).is(':visible')) othersAnswered = false;
				allAnswered = false;
			}
		});

		if (presentation == 'all') {
			$('.leave_assessment').show();
		} else if ((presentation == 'free') && othersAnswered) {
			$('.leave_assessment').show();
			if (allAnswered) $('.leave_assessment').removeAttr('disabled');
			else $('.leave_assessment').attr('disabled', 'disabled');
		} else if ((presentation == 'next') && $lastItem.is(':visible')) {
			$('.leave_assessment').show();
		} else $('.leave_assessment').hide();
	}
	
	$(document).on('click', '.item_previous', function() {
		var $previous = false;
		$assessment_wrapper.find('.question_wrapper:not(.shuffle_break)').reverse().each(function() {
			if ($(this).is(':visible')) $previous = false;
			else if ($previous === false) $previous = $(this);
		});
		if ($previous.length == 1) {
			var questionHandleID = $previous.attr('data-question-handle-id');
			var partID = $previous.find('.part_wrapper').eq(0).attr('data-part-id');
			var $partMenuItem = $('.items_menu .menu_item[data-question-handle-id="' + questionHandleID + '"][data-part-id="' + partID + '"]');
			$partMenuItem.trigger('click');
			updateNavButtons();
		}
	});

	$(document).on('click', '.item_next', function() {
		var $next = false;
		$assessment_wrapper.find('.question_wrapper:not(.shuffle_break)').each(function() {
			if ($(this).is(':visible')) $next = false;
			else if ($next === false) $next = $(this);
		});
		if ($next !== false) {
			var questionHandleID = $next.attr('data-question-handle-id');
			var partID = $next.find('.part_wrapper').eq(0).attr('data-part-id');
			var $partMenuItem = $('.items_menu .menu_item[data-question-handle-id="' + questionHandleID + '"][data-part-id="' + partID + '"]');
			$partMenuItem.trigger('click');
			updateNavButtons();
		}
	});

	var getResponse = function($part_wrapper) {
		var response = '';
		var questionStyle = $part_wrapper.attr('data-type').substr(0, 2);
		if (questionStyle == 'mc') {
			var $textInput = $part_wrapper.find('input[type="text"].online_response');
			var $checkboxes = $part_wrapper.find('input[type="checkbox"].online_response');
			if ($textInput.length == 1) {
				response = $textInput.val().toLowerCase();
			} else if ($checkboxes.length > 0) {
				var responseCharCode = "a".charCodeAt(0);
				$checkboxes.each(function() {
					if ($(this).prop('checked')) {
						var responseLetter = String.fromCharCode(responseCharCode);
						response += responseLetter;
					}
					++responseCharCode;
				});
			}
		} else if (questionStyle == 'nr') {
			$part_wrapper.find('.online_response').each(function() {
				if ($(this).val().length == 0) response += ' ';
				else response += $(this).val();
			});
		} else if (questionStyle == 'wr') {
			var responseHTML = $part_wrapper.find('.wr_input_field').html();
			$('#wr_buffer').html(responseHTML);
			response = $('#wr_buffer').htmleditor('getValue');
		}
		
		return response;
	}

	var getResponses = function() {
		var responses = [];
		$assessment_wrapper.find('.part_wrapper').each(function() {
			var thisEntry = {
				type: $(this).attr('data-type'),
				value: getResponse($(this))
			};
			if ($(this).find('.reasoning_wrapper').length > 0) {
				var $wrapper = $(this).find('.reasoning_wrapper');
				var responseHTML = $wrapper.find('.wr_input_field').html();
				$('#wr_buffer').html(responseHTML);
				var notes = $('#wr_buffer').htmleditor('getValue');
				if (notes.length > 0) thisEntry.notes = notes;
			}
			
			responses.push(thisEntry);
		});
		return responses;
	}
		
	var saveResponses = function(sendAsBeacon) {
		var responsesJSON = JSON.stringify(getResponses());

		var url = '/SittingStudents/save_data/' + sittingStudent_id;
		var data = {
			response_online_id: responseOnline_id,
			fingerprint: BrowserFP.get(),
			client_time: Date.now(),
			responses_hash: SparkMD5.hash(responsesJSON),
			responses: responsesJSON
		};
		
		if (!sendAsBeacon) {
			ajaxFetch(url, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(data)
			}).then(async response => {
				const responseData = await response.json();
				
				if (responseData.success) {

					updateNavButtons();
					if ('time_remaining' in responseData) initTimer(responseData.time_remaining);
					else $('#time_panel').hide();
					SittingsCommon.showAndFade($('#save_success'));

				} else if (('badSitting' in responseData) && responseData.badSitting) {

					window.location.replace('/student_removed');

				} else if (('isClosed' in responseData) && responseData.isClosed) {

					addClientFlash({
						title: "Error",
						text: "Assessment closed.",
						image: "/img/error.svg"
					});

					window.location.replace('/');

				} else $('#save_warning').show();
			}).catch(function(error) {
				$('#save_warning').show();
			});
		} else if ("sendBeacon" in navigator) {
			var formData = new FormData();
			for (var key in data) formData.append(key, data[key]);
			navigator.sendBeacon(url, formData);
		} else {
			ajaxFetch(url, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(data)
			});
		}
	}
	
	var initTimer = function(timeRemaining) {
		var daySeconds = 86400; // Mostly to avoid 32-bit limit on setTimeout

		if (timeRemaining < daySeconds) {
			timeRemaining *= 1000;
			assessmentEnd = Date.now() + timeRemaining;
			
			var oldTimeout = expiredTimeout;
			expiredTimeout = setTimeout(timeExpired, timeRemaining);
			if (oldTimeout != null) clearTimeout(oldTimeout);
			
			for (var i = 0; i < notification_timers.length; ++i) {
				oldTimeout = notification_timers[i].timeout;
	
				var delay = timeRemaining - notification_timers[i].time * 60000;
				if (delay > 0) {
					notification_timers[i].timeout = setTimeout(function(index) {
						showNotification(notification_timers[index].time);
					}, delay, i);
				}
				
				if (oldTimeout != null) clearTimeout(oldTimeout);
			}
		
			setInterval(updateTimer, 1000);
			$('#time_panel').show();
		} else {
			var days = Math.floor(timeRemaining / daySeconds);
			$('#timer_text').text(days + " day" + ((days == 1) ? '' : 's'));

			var refreshTime = timeRemaining - days * daySeconds;
			expiredTimeout = setTimeout(function() {
				saveResponses(false);
			}, refreshTime * 1000);
		}
	}
	
	var showNotification = function(time) {
		$('#time_warning_minutes').html(time + ' minute' + ((time == 1) ? '' : 's'));
		SittingsCommon.showAndFade($('#time_warning'));
	};
	
	var updateTimer = function() {
		var secondsRemaining = (assessmentEnd - Date.now()) / 1000.0;
		if (secondsRemaining < 0) secondsRemaining = 0;
		
		if (secondsRemaining < 60) {
			var seconds = Math.round(secondsRemaining);
			$('#timer_text').text(seconds + " second" + ((seconds == 1) ? '' : 's'));
		} else if (secondsRemaining < 3600) {
			var minutes = Math.round(secondsRemaining / 60.0);
			$('#timer_text').text(minutes + " minute" + ((minutes == 1) ? '' : 's'));
		} else if (secondsRemaining < 86400) {
			var hours = Math.floor(secondsRemaining / 3600.0);
			var minutes = Math.round((secondsRemaining - 3600 * hours) / 60.0);
			$('#timer_text').text(hours + " hour" + ((hours == 1) ? '' : 's') + ' ' +
				minutes + " minute" + ((minutes == 1) ? '' : 's'));
		} else {
			var days = Math.round(secondsRemaining / 86400.0);
			$('#timer_text').text(days + " day" + ((days == 1) ? '' : 's'));
		}
	}
	
	var timeExpired = function() {
		window.onbeforeunload = null;
        window.location.href = '/SittingStudents/view_closed/' + sittingStudent_id;
	}

	$(document).on('change', 'input[type="checkbox"].online_response', function(e) {
		queueSave($(this), 500);
	});

	$(document).on('savedom', '.wr_input_field', function(e) {
		queueSave($(this), 5000);
	});

	$(document).on('blur', '.wr_input_field', function(e) {
		if (responseTimeout != null) queueSave($(this), 500);
	});

	$(document).on('saveData', '.online_response', function(e) {
		queueSave($(this), 500);
	});

	var queueSave = function($target, delay) {
		var $part_wrapper = $target.closest('.part_wrapper');
		var questionHandleID = $part_wrapper.closest('.question_wrapper').attr('data-question-handle-id');
		var partID = $part_wrapper.attr('data-part-id');
		var $partMenuItem = $('.items_menu .menu_item[data-question-handle-id="' + questionHandleID + '"][data-part-id="' + partID + '"]');
		
		if (getResponse($part_wrapper).trim().length != 0) {
			$partMenuItem.find('.status_pip').css('background-image', 'url("/img/icons/pip-checked.svg")');
		} else $partMenuItem.find('.status_pip').css('background-image', 'url("/img/icons/pip-empty.svg")');

		$('#save_success').hide();
		$('#save_warning').hide();
		if (responseTimeout != null) clearTimeout(responseTimeout);
		responseTimeout = setTimeout(function() {
			saveResponses(false);
			responseTimeout = null;
		}, delay);
	}

	window.onunload = function() {
		if (responseTimeout !== null) {
			clearTimeout(responseTimeout);
			saveResponses(true);
		}		
	}
	
	$('#save_retry').on('click', function() {
		$('#save_warning').hide();
		if (responseTimeout != null) {
			clearTimeout(responseTimeout);
			responseTimeout = null;
		}
		saveResponses(false);
	});
	
	$(document).on('click', '.tool_toggle', function() {
		const $panel = $(this).closest('.tool_panel');
		const $header = $panel.find('.tool_header');
		const $wrapper = $panel.find('.tool_wrapper');

		if ($(this).text() == 'Hide') {
			$wrapper.hide();
			$header.css('border-bottom-left-radius', '5px');
			$header.css('border-bottom-right-radius', '5px');
			$(this).text('Show');
		} else {
			$wrapper.show();
			$header.css('border-bottom-left-radius', '0px');
			$header.css('border-bottom-right-radius', '0px');
			$(this).text('Hide');
		}
	});

	var logEvent = function(eventName, details) {
		if (responseOnline_id != null) {
			details = (typeof details !== 'undefined') ?  details : {};
			details.clientTime = Date.now();
			details.eventName = eventName;
	
			var url = '/SittingStudents/log_event/' + sittingStudent_id;
			var data = {
				response_online_id: responseOnline_id,
				fingerprint: BrowserFP.get(),
				details: JSON.stringify(details)
			};
	
			if ("sendBeacon" in navigator) {
				var formData = new FormData();
				for (var key in data) formData.append(key, data[key]);
				navigator.sendBeacon(url, formData);
			} else {
				ajaxFetch(url, {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify(data)
				});
			}	
		}
	}

	$(window).on('focus', function (e) {
		if (!windowFocussed) {
			windowFocussed = true;
			logEvent('focus');			
		}
	});

	$('#nextItem_dialog').dialog({
        autoOpen: false,
		width: 450,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				$(this).dialog("close");
				item_click_reject();
			},
			'Continue': function() {
				$(this).dialog("close");
				item_click_resolve();
			}
		},
		open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Continue")').addClass('btn btn-primary btn_font');
	        $buttonPane.find('button').blur();
		}, close: function() {
			$('form').validationEngine('hideAll');
		}
	});	

	$('#emptyItem_dialog').dialog({
        autoOpen: false,
		width: 450,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				$(this).dialog("close");
				item_click_reject();
			},
			'Skip question': function() {
				$(this).dialog("close");
				item_click_resolve();
			}
		},
		open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
			$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
			$buttonPane.find('button:contains("Skip question")').addClass('btn btn-danger btn_font');
	        $buttonPane.find('button').blur();
		}, close: function() {
			$('form').validationEngine('hideAll');
		}
	});

	$(document).on('selected', '.part_wrapper', function(e) {
		logEvent('select', {
			index: parseInt($assessment_wrapper.find('.part_wrapper').index($(this)))
		})
	});

	$(window).on('blur', function (e) {
		if (windowFocussed) {
			windowFocussed = false;
			logEvent('blur');
		}
	});

	$(document).on('paste', '.wr_input_field', function(e) {
		logEvent('paste');			
	})

	$(document).on('copy', function(e) {
		logEvent('copy');			
	})

	initialize()
/*	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});

		window.onbeforeunload = null;
		window.location.replace('/');
	});*/

});

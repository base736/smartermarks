/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as IndexCommon from '/js/common/index.js?7.0';
import * as SittingStudentsCommon from '/js/sittingstudents/common.js?7.0';

function drawIndexRow(data) {

	// Add a row in the index table for this entry

	var canDelete = 1;

	var html = "<tr data-sitting-student-id='" + data['SittingStudent']['id'] + "' " +
		"data-sitting-status='" + data['status'] + "' " +
		"data-num-open='" + data['responses_open'] + "' " +
		"data-num-closed='" + data['responses_closed'] + "' ";
		
	if (data['Sitting']['json_data']['Access']['attempt_limit'] != 'none') {
		html += "data-num-attempts='" + data['Sitting']['json_data']['Access']['attempt_limit'] + "' ";
	}
	html += "data-can-delete='" + canDelete + "'>";

	html += '<td style="width:15px;">' + 
		'<input type="checkbox" class="item_select" style="margin:1px 0px;" autocomplete="off" />' + 
		'</td>';

	html += '<td valign="top"><div style="display:flex; flex-direction:column; gap:5px;">';
	html += '<div style="display:flex; flex-direction:row; gap:5px;">';
	if (data['SittingStudent']['is_new'] >= 2) html += "<div class='index_new_label label label-info'>New</div>";
	html += '<div class="index_item_name"><b>' + escapeHTML(data['Sitting']['name']) + '</b></div>';
	html += '</div>';
	html += '<div style="flex:1 1 auto;">' + SittingStudentsCommon.getSittingDetails(data) + '</div>';
	html += '</div></td>';

	html += "</tr>";	
	$('.index_table').append(html);
}

function finishIndex() {
	
	// Check for an empty index

	if ($('.index_table tr').length == 0) {
		var html = "";
		html += '<tbody><tr class="disabled"><td colspan="5"><div style="padding-left:10px;">';
		html += '<i>No assessments to show here.</i></div></td></tr></tbody>';
		$('.index_table').html(html);
	}
}

$(document).ready(function() {

    IndexCommon.initialize({ drawIndexRow, finishIndex });

	$(document).on('modifyItemActions', function(event) {
		var $selectedRow = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				if ($selectedRow != null) return true;
				else $selectedRow = $(this).closest('tr');
			}
		});
		
		if ($selectedRow != null) {
			var numOpen = parseInt($selectedRow.attr('data-num-open'));
			var numClosed = parseInt($selectedRow.attr('data-num-closed'));

			var numAttempts = null;			
			if ($selectedRow[0].hasAttribute('data-num-attempts')) {
				numAttempts = parseInt($selectedRow.attr('data-num-attempts'));
			}
			
			var canOpen = false;
			
			if ($selectedRow.attr('data-sitting-status') == 'closed') {
				$('#view_open_button').text('Start a new attempt');
				$('#view_open_button').addClass('disabled-link');
			} else if (numOpen > 0) {
				$('#view_open_button').text('Continue previous attempt');
				$('#view_open_button').removeClass('disabled-link');
				canOpen = true;
			} else {
				$('#view_open_button').text('Start a new attempt');
				if ((numAttempts == null) || (numClosed < numAttempts)) {
					$('#view_open_button').removeClass('disabled-link');
					canOpen = true;
				} else $('#view_open_button').addClass('disabled-link');
			}
			
			if (numClosed > 0) {
				$('#view_closed_button').removeClass('disabled-link');
				canOpen = true;
			} else $('#view_closed_button').addClass('disabled-link');
			
			if (!canOpen) {
				$('#open_button').attr('disabled', 'disabled');
			} else $('#open_button').removeAttr('disabled');
		}
	});

	$('#view_open_button').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});

		if (numChecked == 1) {
			var sittingStudent_id = $selectedCheckbox.closest('tr').attr('data-sitting-student-id');
			window.location.href = '/SittingStudents/start/' + sittingStudent_id;
		}
	});
	
	$('#view_closed_button').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled-link')) return;

		var numChecked = 0;
		var $selectedCheckbox = null;
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				$selectedCheckbox = $(this);
				numChecked++;
			}
		});

		if (numChecked == 1) {
			var sittingStudent_id = $selectedCheckbox.closest('tr').attr('data-sitting-student-id');
			window.location.href = '/SittingStudents/view_closed/' + sittingStudent_id;
		}
	});
		
	var urlVars = getUrlVars();
	if ('search' in urlVars) {
		var search = urlVars.search.replace(/\+/g, '%20')
		$('#search_input').val(decodeURIComponent(search));
	}
	/*
	if ('where' in urlVars) {
		var where = urlVars.where.replace(/\+/g, '%20')
		$('#search_type').val(decodeURIComponent(where));
	}
	*/
});


/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

import * as SittingStudentsCommon from '/js/sittingstudents/common.js?7.0';
import * as SittingsCommon from '/js/sittings/common.js?7.0';
import * as ScrollingCommon from '/js/common/scrolling.js?7.0';
import * as RenderTools from '/js/assessments/render_tools.js?7.0';

$.extend($.ui.dialog.prototype.options, {
	resizable: false
});

var sittingData;

document.addEventListener("DOMContentLoaded", async function() {
		
    await SittingsCommon.initialize();
    ScrollingCommon.initialize();
    await RenderTools.initialize();

	var nameLinesData = null;
	
	var initialize = async function() { 
		const response = await ajaxFetch('/SittingStudents/get_closed/' + sittingStudentID);
		const responseData = await response.json();

		if (responseData.success) {

			sittingData = responseData.Sitting;

			$('#document_title').html(sittingData.name);
			
			if (responseData.show_questions) nameLinesData = responseData.details;
			else $('.view_button[data-view-type="preview"]').remove();

			if (responseData.show_answers) $('.view_button[data-view-type="answers"]').show();
			else $('.view_button[data-view-type="answers"]').remove();

			if (responseData.show_scoring || responseData.show_outcomes) {
				$('.view_button[data-view-type="results"]').show();
			} else if ($('.view_button').length > 1) {
				$('.view_button[data-view-type="results"]').remove();
			}

			if ($('.view_button').length > 1) $('#navigation_pane').show();
			else $('#navigation_pane').hide();

			if (('closed_responses' in responseData) && (responseData.closed_responses.length > 0)) {

				for (var i = 0; i < responseData.closed_responses.length; ++i) {
					var created = dayjs.utc(responseData.closed_responses[i].created).local();
					var formatted = created.format('MMMM D, YYYY') + ' at ' + created.format('h:mm A');
					var $newAttemptItem = '<li><div class="menu_item" id="attempt_item_' + responseData.closed_responses[i].id + 
						'" data-response-online-id="' + responseData['closed_responses'][i].id + '">' +
						'<div class="menu_text">' + formatted + '</div></div></li>';
					$('#attempts_menu').append($newAttemptItem);
				}
				
				var numResponses = responseData.closed_responses.length;
				await renderAttempt(responseData.closed_responses[numResponses - 1].id);

				var urlVars = getUrlVars();
				var $viewButton = false;
				if ('view' in urlVars) {
					$viewButton = $('.view_button:visible[data-view-type="' + urlVars.view + '"]');
					if ($viewButton.length == 0) $viewButton = false;
				}
				
				if ($viewButton !== false) $viewButton.trigger('click');
				else $('.view_button').first().trigger('click');

			} else {

				throw new Error("No completed attempts to show.");

			}
			
		} else {

			throw new Error("Unable to show attempts.");
			
		}
	}

	$(".view_button").click(function(e) {
		e.preventDefault();
		if (!$("#view_menu").menu("option", "disabled")) {
			var urlVars = getUrlVars();
			urlVars.view = $(this).attr('data-view-type');
			setUrlVars(urlVars);

			$(".view_button").removeClass("ui-state-active");
			$(this).addClass("ui-state-active");

			drawView();
		}
	});

	var drawView = function() {
		$('.right_panel').hide();
		$('#navigation_wrapper').hide();

		var urlVars = getUrlVars();
		if (urlVars.view == 'results') {
			$('#results_panel').show();
		} else if (urlVars.view == 'preview') {
			$('#preview_panel').show();
			$('#navigation_wrapper').show();
		} else if (urlVars.view == 'answers') {
			$('#answers_panel').show();
		}

        ScrollingCommon.resetLeftPanel();
	}

	$('#open_index').on('click', function() {
		window.location.replace('/SittingStudents/index');
	});
	
	$('#open_new').on('click', function() {
		window.location.replace('/SittingStudents/view_open/' + sittingStudentID);
	});
	
	$(document).on('click', '#attempts_menu .menu_item', function(e) {
        e.preventDefault();
	
		var responseOnline_id = $(this).attr('data-response-online-id');
		renderAttempt(responseOnline_id)
		.then(function() {
			$('.view_button').first().trigger('click');
		});
    });
    
    var renderAttempt = async function(responseOnline_id) {
		$('#attempts_menu .menu_item').removeClass('active');
		var $targetItem = $('#attempts_menu .menu_item[data-response-online-id="' + responseOnline_id + '"]');
		$targetItem.addClass('active');

		$('#assessment_preview_wrapper').html('');
		$('#section_menu').html('');
		$('#empty_preview').show();

		const queryString = new URLSearchParams({
			response_online_id: responseOnline_id
		}).toString();

		ajaxFetch(`/SittingStudents/get_attempt/${sittingStudentID}?${queryString}`)
		.then(async response => {
			pendingAJAXPost = false;
			const responseData = await response.json();
	
			if (responseData.success) {
	
				if (responseData.show_scoring || responseData.show_outcomes) {
					SittingsCommon.scoreAttempt(responseData);
				}
	
				var isWeighted = false;
				if (responseData.show_scoring) {
					if (!('weighted' in responseData.scores)) isWeighted = false;
					else if (responseData.scores.weighted == 0) isWeighted = false;
					else isWeighted = true;
				}
	
				if ('Version' in responseData) {
					$('#assessment_preview_wrapper').css('opacity', '0.0');
					await SittingsCommon.previewFromHTML($('#assessment_preview_wrapper'), responseData.Version.html, responseData.Version.settings, sittingData.View);
	
					SittingStudentsCommon.drawLeftBar($('#assessment_preview_wrapper'));
						
					$('#empty_preview').hide();
					$('#assessment_preview_wrapper').css('opacity', '1.0');		
					
					if ('responses' in responseData) {
						var result = SittingsCommon.fillResponses($('#assessment_preview_wrapper'), responseData.responses);
	
						if (!result) {
							$.gritter.add({
								title: "Error",
								text: "Unable to read responses.",
								image: "/img/error.svg"
							});					
						}
					}
	
					await RenderTools.renderEquations($('#assessment_preview_wrapper'));
	
					if (responseData.show_scoring) {
						SittingsCommon.markAssessment($('#assessment_preview_wrapper'), responseData.scores.questions, 
							isWeighted);
					}
	
					if (nameLinesData != null) {
						SittingsCommon.addNameLines($('#assessment_preview_wrapper'), nameLinesData);
					}
	
					$('.view_button[data-view-type="preview"]').show();
					$('#outcomesResults').closest('table').removeClass('noQuestions');
				} else {
					$('.view_button[data-view-type="preview"]').hide();
					$('#outcomesResults').closest('table').addClass('noQuestions');
				}
	
				if ('key_sections' in responseData) {
					$('#student_key_wrapper').html(SittingsCommon.getKeyHTML(responseData.key_sections));
					RenderTools.renderEquations($('#student_key_wrapper'));
					$('#student_key_wrapper').show();
				}
	
				$('.results_wrapper').hide();
				if (!responseData.show_scoring && !responseData.show_outcomes) {
					$('#empty_wrapper').show();
				} else {
					if (responseData.show_scoring) {
						var outOf = responseData.scores.outOf;
						
						var totalScore = 0.0;
						for (var i = 0; i < responseData.scores.questions.length; ++i) {
							if (!('omitted' in responseData.scores.questions[i])) {
								totalScore += responseData.scores.questions[i].score;
							}
						}
	
						if (isWeighted) {
							var percentage = (100.0 * totalScore / outOf).toFixed(0);
							$('#score_text').html(percentage + '%');
						} else $('#score_text').html(totalScore + "/" + outOf);
						$('#score_wrapper').show();
					}
	
					if ('wr_unscored' in responseData) $('#wr_warning').show();
					else $('#wr_warning').hide();
					
					if (responseData.show_outcomes && ('outcomes' in responseData) && (responseData.outcomes.length > 0)) {
						SittingsCommon.buildOutcomesTable($('#outcomesResults'), responseData.outcomes);
						$('#outcomes_wrapper').show();
					}
				}
				
				$('.online_response').attr('disabled', 'disabled');
	
				drawView();
	
				ScrollingCommon.resetLeftPanel();
	
			} else {
	
				window.location.replace('/SittingStudents/index');
	
			}
		});
    }
    
	$(document).on('click', '#sections_menu .menu_item', function(e) {
        e.preventDefault();

		var sectionID = $(this).attr('data-section-id');
		SittingStudentsCommon.selectSection(sectionID);
    });

	$(document).on('click', '.items_panel .menu_item', function(e) {
		e.stopPropagation();

		if (!$(this).is('.active')) {
			var questionHandleID = $(this).attr('data-question-handle-id');
			var partID = $(this).attr('data-part-id');

			// Select the item in the assessment preview
			
			SittingStudentsCommon.selectQuestion(questionHandleID, partID);

			var $target = $('#assessment_preview_wrapper .question_wrapper[data-question-handle-id="' + questionHandleID + '"] .part_wrapper[data-part-id="' + partID + '"]');
			if ($target.prev().is('.context_wrapper')) $target = $target.prevAll('.context_header:first');
	
			var questionTop = $target.offset().top;
			$("html, body").animate({
				scrollTop: questionTop - 10
			}, 1000);		
		}
	});

    $(document).on('click', '.show_outcome', function(e) {
		e.preventDefault();

		$('.part_highlighted').removeClass('part_highlighted');

	    var questionIndices = $(this).closest('tr').attr('data-question-indices').split(',');
	    var $questions = $('#assessment_preview_wrapper .part_wrapper');
	    for (var i = 0; i < questionIndices.length; ++i) {
		    $questions.eq(parseInt(questionIndices[i])).addClass('part_highlighted');
	    }
	    
	    $('.view_button[data-view-type="preview"]').trigger('click');

	    SittingStudentsCommon.selectQuestion(questionIndices[0]);

		var $target = $('.part_highlighted').first();
		if ($target.prev().is('.context_wrapper')) $target = $target.prevAll('.context_header').first();
		
		var questionTop = $target.offset().top;
		$("html, body").animate({
			scrollTop: questionTop - 10
		}, 1000);
    });

	$('#assessment_preview_wrapper').on('click', function(e) {
		$('.part_highlighted').removeClass('part_highlighted');
	});

	$('#view_menu').menu();

    $('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : 'bottomLeft'
    }).submit(function(e){ e.preventDefault() });
    
	initialize()
	.catch((error) => {
		addClientFlash({
			title: "Error",
			text: error.message,
			image: "/img/error.svg"
		});

		window.onbeforeunload = null;
		window.location.replace('/');
	});

});

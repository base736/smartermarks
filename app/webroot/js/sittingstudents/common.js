 /**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

var getSittingDetails = function(data) {
	var html = "";

	var closed = ('closed' in data) ? dayjs.utc(data['closed']).local() : false;

	if ((closed !== false) && dayjs().isAfter(closed)) {
		var closedFormatted = closed.format('MMMM D, YYYY') + ' at ' + closed.format('h:mm A');
		html += "<div>Closed " + closedFormatted + "</div>";
	} else {
		html += "<div>";
		if (data['Sitting']['json_data']['Access']['start']['type'] == 'date') {
			var start = dayjs.utc(data['Sitting']['json_data']['Access']['start']['date']).local();
			html += "Available " + start.format('MMMM D, YYYY') + ' at ' + start.format('h:mm A');
		} else if (data['Sitting']['json_data']['Access']['start']['type'] == 'code') {
			html += "Start code required";
		} else {
			html += "Available now";
		}

		if (closed !== false) {
			var closedFormatted = closed.format('MMMM D, YYYY') + ' at ' + closed.format('h:mm A');
			html += " until " + closedFormatted + '.';
		} else {
			html += '.';
		}
		html += "</div>";

		var numOpen = data['responses_open'];
		var numClosed = data['responses_closed'];
		var numNotClosed = false;
	
		html += "<div>";
		if (data['Sitting']['json_data']['Access']['attempt_limit'] != 'none') {
			var numAttempts = data['Sitting']['json_data']['Access']['attempt_limit'];
			var numRemaining = numAttempts - numOpen - numClosed;
			if (numRemaining < 0) numRemaining = 0;
			numNotClosed = numAttempts - numClosed;
			
			if (numRemaining == 0) html += "No attempts remaining";
			else html += numRemaining + " attempt" + ((numRemaining == 1) ? '' : 's') + " remaining";
			
			if (numRemaining + numOpen == 0) html += ".";
			if (numOpen > 0) html += ", " + numOpen + " in progress, ";
			else if (numRemaining > 0) html += ", ";
		} else {
			html += "Unlimited attempts, ";
		}
		
		if (numNotClosed === 0) {
		} else if (data['Sitting']['json_data']['Access']['duration'] == 'none') {
			html += "no time limit.";
		} else {
			html += data['Sitting']['json_data']['Access']['duration'] + " minute time limit";
			html += (numNotClosed == 1) ? "." : " each.";
		}
		html += "</div>";
	
		if ('student_note' in data['Sitting']['json_data']) {
			html += "<div>" + data['Sitting']['json_data']['student_note'] + "</div>";
		}
	}

	return html;
}

var drawLeftBar = function($target) {	
	$('#sections_menu').empty();
	$('.items_panel').remove();

	var activeSectionID = null;
	var activeHandleID = null;
	var activePartID = null;

	var urlVars = getUrlVars();
	if ('s' in urlVars) {
		activeSectionID = urlVars.s;
	} else if (('h' in urlVars) && ('p' in urlVars)) {
		activeHandleID = urlVars.h;
		activePartID = urlVars.p;
	}

	var index = 0;
	$target.find('.preview_section_wrapper').each(function() {
		if ($(this).hasClass('page_break')) return;
		
		var header = $(this).find('.preview_header').text();
		var sectionID = $(this).attr('data-section-id');
	
		var sectionItemHTML = 
			'<li><div class="menu_item" data-section-id="' + sectionID + '">' +
			'<div class="menu_text">' + escapeHTML(header) + '</div>';
		sectionItemHTML += '</div></li>';
	
	    $('#sections_menu').append(sectionItemHTML);
	    
	    var itemsPanelHTML =
	    	'<div class="items_panel" data-section-id="' + sectionID + '" style="display:none;"> \
	    		<div class="window_bar">' + $(this).find('.preview_header').text() + '</div> \
				<div class="window_content" style="padding:5px;"> \
					<div class="layout_panel"> \
						<ul class="items_menu menu_wrapper">';
	    
	    $(this).find('.part_wrapper').each(function() {

			// Get question number

			var firstPart = $(this).prevAll('.context_wrapper').eq(0).find('.question_number').text();
			if (firstPart == '&nbsp;') firstPart = '';
			else firstPart = firstPart.slice(0, -1);
			var secondPart = $(this).children('.question_number').text().slice(0, -1);

			// Build question entry in left panel

			var partID = $(this).attr('data-part-id');
			var questionHandleID = $(this).closest('.question_wrapper').attr('data-question-handle-id');
			itemsPanelHTML += '<li><div class="menu_item" data-index="' + (index++) + '" data-question-handle-id="' + questionHandleID + '" data-part-id="' + partID + '">';
			itemsPanelHTML += '<div class="status_pip"></div>';

			if ((questionHandleID == activeHandleID) && (partID == activePartID)) {
				activeSectionID = sectionID;
			}
			
			itemsPanelHTML += '<div class="menu_text">' + firstPart + secondPart + '. ' +
				$(this).find('.question_prompt').text() + '</div></div></li>';
	    });

		itemsPanelHTML += '</ul></div></div></div>';
	
		$('#navigation_wrapper').append(itemsPanelHTML);
	});

	$('.items_panel .status_pip').css('background-image', 'url("/img/icons/pip-empty.svg")');

	if (activeSectionID != null) {
		if (activeHandleID != null) selectQuestion(activeHandleID, activePartID);
		else selectSection(activeSectionID);
	}
}

var selectSection = function(sectionID) {
	$('#sections_menu .menu_item').removeClass('active');
	$('#sections_menu .menu_item[data-section-id="' + sectionID + '"]').addClass('active');

	$('.items_panel').hide();
	$('.items_panel[data-section-id="' + sectionID + '"]').show();

	$('.items_menu .menu_item').removeClass('active');

	var urlVars = getUrlVars();
	urlVars.s = sectionID;
	delete urlVars.h;
	delete urlVars.p;
	setUrlVars(urlVars);
}

var selectQuestion = function(questionHandleID, partID) {
	var urlVars = getUrlVars();
	urlVars.h = questionHandleID;
	urlVars.p = partID;
	delete urlVars.s;
	setUrlVars(urlVars);

	var $menu_item = $('.items_menu .menu_item[data-question-handle-id="' + questionHandleID + '"][data-part-id="' + partID + '"]');
	var $items_panel = $menu_item.closest('.items_panel');
	var sectionID = $items_panel.attr('data-section-id');

	// Unhighlight sections except for the active one

	$('#sections_menu .menu_item').removeClass('active');
	$('#sections_menu .menu_item[data-section-id="' + sectionID + '"]').addClass('active');
   
	// Open item panel

	$('.items_panel').hide();
	$items_panel.show();

   	// Highlight the selected item link and unhighlight others

	$('.items_menu .menu_item').removeClass('active');
	$menu_item.addClass('active');
}

export { getSittingDetails, 
    drawLeftBar, 
    selectSection, selectQuestion 
};
<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'DefaultPages', 'action' => 'index'));
	Router::connect('/login', array('controller' => 'Users', 'action' => 'login'));
	Router::connect('/about', array('controller' => 'DefaultPages', 'action' => 'about'));
	Router::connect('/privacyPolicy', array('controller' => 'DefaultPages', 'action' => 'privacyPolicy'));
	Router::connect('/userAgreement', array('controller' => 'DefaultPages', 'action' => 'userAgreement'));
	Router::connect('/home', array('controller' => 'DefaultPages', 'action' => 'home'));
	Router::connect('/teacher', array('controller' => 'DefaultPages', 'action' => 'teacher'));
	Router::connect('/student', array('controller' => 'DefaultPages', 'action' => 'student'));
	Router::connect('/unauthorized', array('controller' => 'DefaultPages', 'action' => 'unauthorized'));
	Router::connect('/welcome', array('controller' => 'DefaultPages', 'action' => 'welcome'));
	Router::connect('/contact', array('controller' => 'DefaultPages', 'action' => 'contact'));
	Router::connect('/awaiting_approval', array('controller' => 'DefaultPages', 'action' => 'awaiting_approval'));
	Router::connect('/awaiting_validation', array('controller' => 'DefaultPages', 'action' => 'awaiting_validation'));
	Router::connect('/student_create', array('controller' => 'DefaultPages', 'action' => 'student_create'));
	Router::connect('/student_validate', array('controller' => 'DefaultPages', 'action' => 'student_validate'));
	Router::connect('/student_removed', array('controller' => 'DefaultPages', 'action' => 'student_removed'));
	Router::connect('/js_required', array('controller' => 'DefaultPages', 'action' => 'js_required'));
	Router::connect('/build_pdf/*', array('controller' => 'DefaultPages', 'action' => 'build_pdf'));
	Router::connect('/upload_image', array('controller' => 'DefaultPages', 'action' => 'upload_image'));
	
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
//	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on 
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<link rel="stylesheet" type="text/css" href="/css/bubble_tip.css?7.0" />
<script type="text/javascript" src="/js/common/bubble_tip.js?7.0"></script>

<script type="text/javascript">
	var sittingID = <?php echo $sitting_id; ?>;
	var assessmentID = <?php echo $assessment_id; ?>;
	var userID = <?php echo $user['id']; ?>;
    var isGenerated = false;
    var isOnline = true;
	var canEdit = <?php echo $canEdit; ?>;
	var canViewResults = <?php echo $canViewResults; ?>;
	var canCancel = <?php echo $canCancel; ?>;
	var showSettings = <?php echo $showSettings; ?>;

    var showCorrelation = <?php echo $user['correlations_beta'] ? 'true' : 'false'; ?>;

	var userDefaults = <?php echo $user['defaults']; ?>;

    var classLists = <?php echo json_encode($classLists); ?>;
</script>

<script type="module" src="/js/sittings/view.js?7.0"></script>

<!-- Top bar -->

<div class="action_bar" style="margin-bottom:5px;">
	<div class="action_left" style="color:#ccc;">
		<div id="document_title"></div>
	</div>
	
	<div class="action_right" style="align-self:flex-start;">
		<?php
		if ($canEdit) {
		?>
		<button id="sitting_options" type="button" class="btn btn-dark" style="width:80px;" disabled><span>Settings</span></button>
		<?php
		}
		?>
	</div>
</div>

<!-- Left Panel-->

<div id="panel_wrapper" style="position:relative; width:890px; height:auto;">
	<div id="left_panel" style="width:295px; position:relative; float:left;">
	
		<!-- {* navigation *} -->
		<div class="window_bar">Views</div>
		<div class="window_content" style="padding:10px;">
			<ul id="view_menu" style="padding:5px;" >
				<li class="view_button" data-view-type="preview"><a href="#">Assessment template</a></li>
				<li class="view_button" data-view-type="access"><a href="#">Student access</a></li>
				<li class="view_button" data-view-type="results"<?php echo $canViewResults ? '' : ' class="ui-state-disabled"' ?>><a href="#">Scoring and results</a></li>
				<li class="view_button" data-view-type="statistics"><a href="#">Assessment statistics</a></li>
			</ul>
		</div>
	
		<!-- {* results views *} -->
		<div class="left_details" id="results_views" style="display:none;">
		    <div class="window_bar">Scoring and Results</div>
		    <div class="window_content" style="padding:10px;">
				<ul id="results_view_menu" style="padding:5px;" >
					<li class="results_view_button" data-view-type="written" style="display:none;"><a href="#">Written response scoring</a></li>
					<li class="results_view_button" data-view-type="summary"><a href="#">Sitting summary</a></li>
					<li class="results_view_button" data-view-type="student"><a href="#">Student results</a></li>
				</ul>
		    	<div id="label_select_wrapper" class="labeled-input" style="margin:10px 0px 0px; width:auto; display:none;">
					<label style="width:auto; display:inline-block; text-align:left; font-size:14px;">List students by:</label>
					<select class="defaultSelectBox" style="width:auto; display:inline-block;" id="student_label_select">
					</select>
				</div>
		    </div>
		</div>
	
		<!-- {* sections *} -->
		<div class="left_details" id="navigation_wrapper" style="display:none;">
			<div class="window_bar">Sections</div>
			<div class="window_content" style="padding:5px;">
				<div class="layout_panel">
					<div class="empty_layout" style="width:255px; padding:10px; color:#eee; font-size:13px; font-style:italic; display:none;">No sections built for this assessment. Click "Add Section" below to begin.</div>
					<ul id="sections_menu" class="menu_wrapper"></ul>
				</div>
			</div>
		</div>
		
		<!-- {* students *} -->
		<div class="left_details" id="students_wrapper" style="display:none;">
			<div class="window_bar">Students</div>
			<div class="window_content" style="padding:5px;">
				<div class="layout_panel">
					<ul id="student_menu" class="menu_wrapper" style="min-height:0px;"></ul>
				</div>
			</div>
		</div>

		<!-- {* written response questions *} -->
		<div class="left_details" id="written_wrapper" style="display:none;">
			<div class="window_bar">Written response questions</div>
			<div class="window_content" style="padding:5px;">
				<div class="layout_panel">
					<ul id="written_menu" class="menu_wrapper"></ul>
				</div>
			</div>
		</div>
		
	</div>
	
	<!-- Right Panel-->
	
	<div style="width:585px; float:right;">
		<div class="right_panel" id="loading_panel" style="display:block;">
			<div class="window_bar">Loading sitting</div>
			<div class="window_content" style="background-color:white; padding:10px;">
				<div class="wait_icon"></div>
				<i>Loading&hellip;</i>
			</div>
		</div>

		<div class="right_panel" id="preview_panel">
			<div class="window_bar">Assessment template</div>
			<div class="window_content" style="padding:10px; background:white;">
				<div id="version_controls" style="margin-bottom:20px; width:100%; display:none;">
					<div style="width:100%; display:flex; justify-content:space-between; align-items:center;">
						<div style="margin-right:10px;"><img src="/img/info.svg" style="width:60px;" /></div>
						<div>
							Note: This sitting will not shuffle each student attempt, but will use a saved common version.
						</div>
						<div id="version_wrapper" style="flex:none; width:230px; display:flex; justify-content:flex-end; white-space:nowrap;" class="custom-buttonpane">
							<div class="btn-group" style="display:inline-block;">
								<button id="version_shuffle" class="btn" disabled>Shuffle</button>
								<button id="version_save" class="btn" disabled>Save</button>
							</div>
							<div class="btn-group" style="display:inline-block;">
								<button id="version_print" class="btn" disabled>Print</button>
							</div>
						</div>
					</div>
				</div>
				<div id="version_shuffling" style="font-style:italic; margin-top:20px;">
					<div class="wait_icon"></div><i>Shuffling assessment...</i>
				</div>
				<div id="assessment_preview_wrapper" class="assessment_wrapper template_view student_view parts_formatted" style="margin-top:20px; min-height:95px; opacity:0.0;">
				</div>
			</div>
		</div>

		<div class="right_panel" id="access_panel">
			<div class="window_bar">Student accounts</div>
			<div class="window_content" style="width:585px; overflow:visible;">
				<table id="accounts_table" class="panel_table actions_table">
					<thead>
						<tr>
							<td>Email address</td>
							<td>Actions</td>
						</tr>
					</thead>
					<tbody>
						<tr class="header_empty">
							<td colspan=2><i>No student accounts in this sitting. Click "Add Students" to add students.</i></td>
						</tr>
						<tr id="header_bounced_accounts" class="table_internal_heading" style="display:none;">
							<td colspan=2>Invalid accounts (email bounced)</td>
						</tr>
						<tr id="header_new_accounts" class="table_internal_heading" style="display:none;">
							<td colspan=2>New accounts (email not validated)</td>
						</tr>
						<tr id="header_validated_accounts" class="table_internal_heading" style="display:none;">
							<td colspan=2>Validated accounts</td>
						</tr>
					</tbody>
				</table>
				<div style="padding:10px;">
					<?php 
                    if ($canEdit && $canViewResults) {
                    ?>
                    
                    <div class="item_menu btn-group editButton">
                        <button class="btn btn-primary item_menu_button" data-toggle="dropdown">
                            Add Students
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href='#' id='addStudentEmails'>By email address</a></li>
                            <li><a href='#' id='addClassList'>From class list</a></li>
                        </ul>
                    </div>

                    <?php
                    }
                    ?>

					<div class="clear"></div>
				</div>
			</div>

			<div class="window_bar">Shareable link</div>
			<div class="window_content" style="padding:10px;">
				<div style="display:flex; flex-direction:column; height:100%;">
					<div id="tok_empty_wrapper" style="font-style:italic; margin:10px 0px 20px;">
						Shareable link has not been enabled. Click "Edit Shareable Link" to set one up.
					</div>
					<div id="tok_detail_wrapper" style="display:none;">
						<div class="labeled-input" style="width:auto; flex:none; veritcal-align:center; margin-bottom:0px;">
							<label style="display:inline-block; text-align:left; width:65px;">Link:</label><img class="link_copy" style="display:inline-block; cursor:pointer; width:15px; margin-right:5px; " src="/img/icons/copy.svg" /><div class="tok_link"></div>
						</div>

						<div class="labeled-input" style="width:auto; flex:none; display:flex; align-items:flex-start; margin-top:10px">
							<label for="tok_identifier" style="flex:none; text-align:left; width:65px; position:relative; top:3px;">Identifier:</label>
							<div style="flex:1 1 100%;">
								<select class="defaultSelectBox pad5px identifier_select" style="height:auto; width:210px; display:block;" id="tok_identifier" disabled></select>
								<input class="validate[required] defaultTextBox2" type="text" style="width:196px; display:none;" id="tok_custom" disabled />
							</div>
						</div>
						<div class="labeled-input" style="width:100%; flex:none;">
							<label style="width:auto; text-align:left;">Allowed values (one per line):</label>
							<textarea id="tok_identifier_list" class="defaultTextBox2" style="resize:none; box-sizing:border-box; width:100%; min-height:100px;" disabled></textarea>
						</div>
					</div>
					<div>
						<?php if ($canEdit && $canViewResults) { ?><button id="editLink_button" class="btn btn-primary editButton">Edit Shareable Link</button><?php } ?>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="right_panel" id="student_panel" style="display:none;">

			<div class="window_bar"><span id="student_title"></span></div>
		    <div class="window_content" style="padding:10px;">
				<div class="results_status" id="no_students" style="font-style:italic;">No students in this sitting. Add students by clicking on "Student access" on the left.</div>
				<div class="results_status" id="no_student_selected" style="font-style:italic;">No student selected. Select a student from the list on the left to show more information.</div>
	    		<div class="results_status" id="empty_student_data" style="font-style:italic;">No attempts to show for this student.</div>
	    		<div class="results_status" id="loading_student_data" style="font-style:italic;">
					<div class="wait_icon"></div>
					<i>Loading student information&hellip;</i>
				</div>
	    		<div id="attempt_overview_wrapper" style="display:none;">
					<div id="student_reload_wrapper" class="custom-buttonpane" style="float:right; margin-bottom:10px;">
						<button id="student_reload" class="btn"><span style="margin-right:4px; font-size:20px;">&#x27F3;</span>Reload</button>
					</div>
					<div style="clear:both;"></div>
					<table class="panel_table actions_table" style="border:1px solid #d7d7d7;">
			            <thead>
			                <tr>
			                    <td width="13%">Attempt</td>
			                    <td>Started</td>
			                    <td width="22%">Time taken</td>
			                    <td width="17%" class="td_score">Score</td>
								<td>Actions</td>
			                </tr>
			            </thead>
			            <tbody id="attempts_rows"></tbody>
			    	</table>
	    		</div>
		    </div>

			<div id="student_details_window" style="display:none;">
				<div class="window_bar">
					<div style="display:flex; padding:0px; width:100%; align-items:center; justify-content:space-between;">
						<div style="flex:none;"><span>Details for </span><span class="student_attempt_title"></span></div>
						<div style="flex:none;">
							<span style="color:white; margin-right:5px;">Showing:</span>
							<select id="student_data_select" style="width:110px; line-height:25px; height:25px; padding:2px 6px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
					    		<option value="assessment" selected>assessment</option>
					    		<option value="key">answer key</option>
					    		<option value="outcomes">outcomes</option>
					    		<option value="events">events</option>
					    	</select>
					    </div>
					</div>
				</div>
			    <div class="window_content" style="width:585px; overflow:hidden;">
					<div id="empty_details" style="width:auto; padding:10px; font-size:13px; font-style:italic;">No details to show here.</div>
			    	<div id="student_outcomes_wrapper" class="student_data_wrapper" style="display:none; background:white;">
						<table class="panel_table noQuestions">
				            <thead>
				                <tr>
				                    <td>Name</td>
				                    <td width="15%">Score</td>
				                    <td width="17%" align="center">Questions</td>
				                </tr>
				            </thead>
				            <tbody id="teacherOutcomes"></tbody>
				    	</table>
				    </div>
					<div id="student_assessment_wrapper" class="student_data_wrapper" style="display:none; background:white;">
						<div style="margin:10px; float:left; font-weight:bold;">Click images to zoom or rotate</div>
						<button id="student_assessment_print" class="btn" style="margin:10px; float:right;">Print</button>
						<div style="clear:both;"></div>
				    	<div id="student_assessment_inner" class="assessment_wrapper template_view student_view parts_formatted" style="background-color:white; padding:10px;"></div>
					</div>
					<div id="student_key_wrapper" class="student_data_wrapper" style="display:none; background:white; padding:10px;">
					</div>
					<div id="student_events_wrapper" class="student_data_wrapper" style="padding:15px; display:none;">
						<div style="width:100%; height:450px; box-sizing:border-box; padding:10px; background-color:white; 
							display:grid; grid-template-columns: 20px auto 20px; grid-template-rows: 20px auto 20px; 
							justify-content:stretch;">
							<div style="grid-column:2; grid-row:1; text-align:center; font-size:16px;">Responses and Browser Tab Events</div>
							<div style="grid-column:2; grid-row:2; position:relative;">
								<div id="student_events_graph" style="width:100%; height:100%;"></div>
								<div id="student_events_legend" class="legend_wrapper"></div>
								<button id="student_events_reset" class="btn">Reset</button>
							</div>
							<div style="grid-column:1; grid-row:2; text-align:center; font-size:16px; writing-mode:vertical-rl; transform:rotate(180deg);">Question number</div>
							<div style="grid-column:2; grid-row:3; text-align:center; font-size:16px;">Time (click and drag to zoom)</div>
						</div>
						<div id="student_events_other" style="margin-top:10px;">
							<div style="font-size:16px; margin-bottom:5px;">Other events:</div>
							<table class="panel_table" style="border:1px solid #d7d7d7;">
								<thead>
									<tr>
										<td>Event time</td>
										<td width="25%">Event type</td>
									</tr>
								</thead>
								<tbody id="events_rows"></tbody>
							</table>
						</div>
					</div>
			    </div>
			</div>
		</div>

		<div class="right_panel" id="written_panel" style="display:none;">
			<div class="window_bar">Scoring controls</div>
		    <div class="window_content" style="padding:10px;">
				<div id="no_written_selected" style="font-style:italic;">No question selected. Select a written response question from the list on the left to show more information.</div>
	    		<div id="loading_written_data" style="font-style:italic; display:none;">
					<div class="wait_icon"></div>
					<i>Loading responses&hellip;</i>
				</div>
	    		<div id="written_controls_wrapper" style="display:none;">
					<div style="width:100%; display:flex; justify-content:space-between;">
						<div style="flex:none; width:auto;">
							<div class="labeled-input" style="display:block;">
								<input type="checkbox" class="normal_chkbx wr_filter_input" id="wr_show_closed" />
								<label for="wr_show_closed" style="text-align:left; width:auto; display:inline-block;">Show only closed attempts</label>
							</div>
							<div class="labeled-input" style="display:block;">
								<input type="checkbox" class="normal_chkbx wr_filter_input" id="wr_show_completed" checked />
								<label for="wr_show_completed" style="text-align:left; width:auto; display:inline-block;">Show only completed responses</label>
							</div>
							<div class="labeled-input" style="display:block;">
								<input type="checkbox" class="normal_chkbx wr_filter_input" id="wr_show_unscored" checked />
								<label for="wr_show_unscored" style="text-align:left; width:auto; display:inline-block;">Show only unscored responses</label>
							</div>
							<div class="labeled-input" style="display:block;">
								<input type="checkbox" class="normal_chkbx wr_filter_input" id="wr_shuffle" checked />
								<label for="wr_shuffle" style="text-align:left; width:auto; display:inline-block;">Shuffle responses</label>
							</div>
							<div class="labeled-input" style="display:block;">
								<input type="checkbox" class="normal_chkbx wr_filter_input" id="wr_show_label" />
								<label for="wr_show_label" style="text-align:left; width:auto; display:inline-block;">Show student <span class="student_label_lower"></span></label>
							</div>
						</div>
						<div style="flex:none; width:auto; display:flex; flex-direction:column; align-items:flex-end;">
							<div style="flex:none;">
								<button class='btn' id='wr_previous'>◀ Prev</button>
								<button class='btn' id='wr_next'>Next ▶</button>
							</div>
							<div id="wr_no_responses" style="flex:none; margin-top:10px;">No matching responses</div>
							<div id="wr_response_label" style="flex:none; margin-top:10px; display:none;">Showing response <span id='wr_response_index'></span>/<span id='wr_response_count'></span></div>
						</div>
					</div>
				</div>
			</div>

			<div id="written_details_window" style="display:none;">
				<div class="window_bar">
					<span style="position:relative;">Scoring for</span>
					<span id="student_scoring_title" style="position:relative;"></span>
				</div>
				<div class="window_content" style="width:585px; min-height:50px;">
					<div id="item_score_wrapper" style="padding:10px;"></div>
					<div style="width:auto; padding:0px 10px 10px;">
						<label style="display:block; font-size:14px;">Comment (optional):</label>
						<div id="wr_comment" class="wr_input_field template_view"></div>
					</div>
				</div>

				<div class="window_bar">
					<div style="padding:0px; width:100%; height:100%; display:flex; align-items:center; justify-content:space-between;">
						<div style="flex:none;">Response details</div>
						<div style="flex:none;">
							<span style="vertical-align:middle;">Showing:</span>
							<select id="written_details_select" style="width:110px; height:24px; padding:2px; margin:0px; vertical-align:middle; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
								<option value="response" selected>response</option>
								<option value="rubric">rubric</option>
							</select>
						</div>
					</div>
				</div>
				<div class="window_content" style="width:585px;">
					<div id="item_view_wrapper" class="assessment_wrapper template_view student_view parts_formatted" style="background-color:white; padding:10px;"></div>
					<div id="item_rubric_wrapper" class="template_view" style="background-color:white; padding:10px; display:none;"></div>
				</div>
			</div>
		</div>

		<div class="right_panel" id="summary_panel" style="display:none;">

			<div class="window_bar">Sitting controls</div>
		    <div class="window_content" style="padding:10px;">
				<div id="loading_summary" style="font-style:italic;">
					<div class="wait_icon"></div>
					<i>Loading results summary&hellip;</i>
				</div>
		    	<div id="sitting_controls_wrapper" style="display:none;">
					<div style="width:100%; display:flex; align-items:center;">
						<select id="sitting_status" style="flex:0 0 80px; margin-right:10px;">
							<option value="open">Open</option>
							<option value="closed">Closed</option>
						</select>
						<div id="sitting_open" style="flex:1 1 auto;">
							This sitting is open. Choose "Closed" to close it, preventing students from submitting further responses.
						</div>
						<div id="sitting_closed" style="flex:1 1 auto;">
							This sitting is closed. Students can no longer start new attempts, and existing attempts are closed by default.
						</div>
					</div>
				</div>
		    </div>

			<div id="results_wrapper" style="display:none;">
				<div class="window_bar">Results summary</div>
				<div class="window_content" style="padding:10px;">
					<div id="empty_summary" style="font-style:italic;">No attempts to show for this sitting.</div>
					<div id="summary_wrapper" style="display:none;">
						<div id="sitting_reload_wrapper" class="custom-buttonpane" style="float:right; margin-bottom:10px;">
							<div class="btn-group" style="margin-right:5px; display:inline-block;">
								<button id="export_dropdown" class="btn dropdown-toggle" data-toggle="dropdown" href="#">
									Export
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu text-left">
									<li><a href="#" id="export_csv">Export as CSV</a></li>
								</ul>
							</div>
							<button id="sitting_reload" class="btn"><span style="margin-right:4px; font-size:20px;">&#x27F3;</span>Reload</button>
						</div>
						<div style="clear:both;"></div>
						<table class="panel_table actions_table" style="text-align:center; table-layout:fixed; border:1px solid #d7d7d7;">
							<thead>
								<tr>
									<td><div style="display:flex; justify-content:space-between; align-items:center;">
										<div class="student_label_upper"></div>
										<div style="line-height:normal;">
											<div class="column_sort" data-direction="asc" style="display:block;">▲</div>
											<div class="column_sort" data-direction="desc" style="display:block;">▼</div>
										</div>
									</div></td>
									<td width="15%"><div style="display:flex; justify-content:space-between; align-items:center;">
										<div>Attempts</div>
										<div style="line-height:normal;">
											<div class="column_sort" data-direction="asc" style="display:block;">▲</div>
											<div class="column_sort" data-direction="desc" style="display:block;">▼</div>
										</div>
									</div></td>
									<td width="32%" class="td_latest"><div style="display:flex; justify-content:space-between; align-items:center;">
										<div>Latest attempt</div>
										<div style="line-height:normal;">
											<div class="column_sort" data-direction="asc" style="display:block;">▲</div>
											<div class="column_sort" data-direction="desc" style="display:block;">▼</div>
										</div>
									</div></td>
									<td width="17%" class="td_score"><div style="display:flex; justify-content:space-between; align-items:center;">
										<div>Best score</div>
										<div style="line-height:normal;">
											<div class="column_sort" data-direction="asc" style="display:block;">▲</div>
											<div class="column_sort" data-direction="desc" style="display:block;">▼</div>
										</div>
									</div></td>
									<td>Actions</td>
								</tr>
							</thead>
							<tbody id="summary_rows"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="right_panel" id="statistics_panel" style="display:none;">
			<div class="window_bar">Filters</div>
		    <div class="window_content" style="padding:10px; display:flex; justify-content:space-between; align-items:flex-start;">
			    <div>
					<div class="labeled-input" style="width:auto;">
						<label style="text-align:left; width:auto; padding-right:5px;">Include</label><select class="defaultSelectBox stats_filter_input" style="width:150px;" id="stats_included_sources">
							<option value="All" selected>all response types</option>
							<option value="Sitting">online sittings only</option>
							<option value="ResponsePaper">paper versions only</option>
						</select>
					</div>
					<div class="labeled-input" style="width:auto;">
						<div style="display:inline-block; vertical-align:top;">
							<input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_date" value="1" />
						</div><div style="display:inline-block; vertical-align:top; position:relative; top:2px;">
							<label for="stats_filter_by_date" style="text-align:left; width:auto;">Include only responses after date:</label>
							<input class="datepicker defaultTextBox2" type="text" id="stats_min_date"/>
						</div>
					</div>
					<div class="labeled-input" style="width:auto;">
						<input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_user" value="1" /><label for="stats_filter_by_user" style="text-align:left; width:auto;">Include only my responses</label>
					</div>
					<div class="labeled-input" style="width:auto;">
						<input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_sitting" value="1" /><label for="stats_filter_by_sitting" style="text-align:left; width:auto;">Include responses for this sitting only</label>
					</div>
			    </div>
			    <div>
					<div class="btn-group" style="display:inline-block;">
						<button class="btn buildButton" id="stats_update">Update statistics</button>
					</div>
					<div class="btn-group" style="display:inline-block;">
						<button class="btn buildButton" id="stats_print" >Print</button>
					</div>
			    </div>
		    </div>

			<div class="window_bar">Assessment</div>
		    <div class="window_content" style="padding:10px; background-color:white">
			    <div id="statistics_graphs" class="assessment_wrapper">
			    </div>
		    </div>

			<div class="window_bar">Selected item</div>
		    <div class="window_content" style="padding:10px;">
		    	<div>
				    <div class="sub_panel_title">Item</div>
				    <div id="statistics_item" class="assessment_wrapper template_view suppress_inputs sub_panel_content" style="padding:10px 5px; min-height:50px; background-color:white;">
					    <i>No question selected.</i>
				    </div>
				</div>
			    <div id="item_statistics_wrapper" style="display:none; margin-top:10px;">
				    <div class="sub_panel_title">Statistics (<span id="statistics_n"></span> total responses)</div>
				    <div class="sub_panel_content" id="item_statistics_data" style="padding:10px 5px; background-color:white;">
				    </div>
			    </div>
		    </div>
		</div>
	</div>
	<div style="clear:both;"></div>
</div>

<div id="scoring_warnings" style="z-index:9999;" >
	<div id="save_success" style="display:none;">
		Saved.
	</div>

	<div id="save_warning" style="display:none;">
		<div style="display:inline-block;">Unable to save changes &mdash; check your internet connection.</div>
		<button id="save_retry" type="button" class="btn btn-small" style="margin-left:20px;">Try again</button>
	</div>
</div>

<!-- Image view panel -->

<div id="image_view_panel" style="display:none;">
	<div id="image_view_bar">
		<div>
			<button title="Zoom in" class="image_view_button zoom_in_icon">&nbsp;</button>
			<button title="Zoom out" class="image_view_button zoom_out_icon">&nbsp;</button>

			<button title="Rotate clockwise" class="image_view_button rotate_cw_icon" style="margin-left:20px;">&nbsp;</button>
			<button title="Rotate counter-clockwise" class="image_view_button rotate_ccw_icon">&nbsp;</button>
		</div>
		<div>
			<button title="Close" class="image_view_button close_image_icon">&nbsp;</button>
		</div>
	</div>
	<div id="image_view_wrapper">
		<div id="image_view_inner"></div>
	</div>
</div>

<!-- Dialog boxes-->

<div class="hidden">

<!-- Item context menus -->

	<div id="question_menu_template">
		<div class="item_menu_wrapper" style="height:35px; padding:5px;">
            <div style="flex:1 1 100%;"></div>
			<div class="item_menu btn-group" style="flex:none; width:auto;">
				<button class="btn btn-small item_menu_button" data-toggle="dropdown">
					Edit
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-right">
					<?php if ($canEdit) { ?>
					<li><a href='#' class='item_menu_scoring'>Change scoring</a></li>
					<li><a href='#' class='item_menu_reasoning'></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>

	<div id="attempt_menu_template">
        <div class="item_menu_wrapper" style="height:35px; padding:5px;">
            <div style="flex:1 1 100%;"></div>
			<div class="item_menu btn-group" style="flex:none; width:auto;">
				<button class="btn btn-small item_menu_button" data-toggle="dropdown">
					Comment
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-right">
					<?php if ($canEdit) { ?>
					<li><a href='#' class='item_menu_edit_comment'></a></li>
					<li><a href='#' class='item_menu_delete_comment'>Delete comment</a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>

<!-- Edit settings dialog -->

	<div id="editSettings_form" class="dialogbox"  title="Edit Settings">
		<form class="formular" accept-charset="utf-8">
			<input type="hidden" id="warnings_json" />
			<div class="dialog_wrapper" id="editDocument_wrapper_left" style="padding:10px 15px 10px 10px; border-bottom-left-radius: 5px; width:405px; height:auto;">
				<div class="labeled-input" style="width:100%">
					<label for="dset_name" style="width:80px; display:inline-block; text-align:left;">Name:</label>
					<input class="validate[required] defaultTextBox2" type="text" id="dset_name" style="width:270px;"/>
				</div>
				<div class="labeled-input" style="width:100%">
					<label for="start_type" style="width:80px; display:inline-block; text-align:left;">Opens:</label>
					<select class="defaultSelectBox" style="width:140px; display:inline-block;" id="start_type">
						<option value="none">Now</option>
						<option value="date">Specified date/time</option>
						<option value="code">With start code</option>
					</select>
					<div style="display:inline-block; vertical-align:top;">
						<div id="start_none_details" class="start_details" style="display:none;">
						</div>
						<div id="start_date_details" class="start_details" style="display:none;">
							<input class="datepicker defaultTextBox2 validate[required]" style="width:125px; margin:0px; display:block;" type="text" id="start_date"/>
							<input class="validate[required]" style="width:125px; margin:5px 0px 0px; display:block; font-family:Arial; font-size:12px;" type="time" id="start_time" />
						</div>
						<div id="start_code_details" class="start_details" style="display:none;">
							<input style="width:125px; margin:0px;" class="defaultTextBox2 validate[required]" type="text" id="start_code"/>
						</div>
					</div>
				</div>
				<div class="labeled-input" style="width:100%">
					<label for="end_type" style="width:80px; display:inline-block; text-align:left;">Closes:</label>
					<select class="defaultSelectBox" style="width:140px;" id="end_type">
						<option value="none">Manually</option>
						<option value="date">Specified date/time</option>
					</select>
					<div style="display:inline-block; vertical-align:top;">
						<div id="end_none_details" class="end_details" style="display:none;">
						</div>
						<div id="end_date_details" class="end_details" style="display:none;">
							<input class="datepicker defaultTextBox2 validate[required]" style="width:125px; margin:0px; display:block;" type="text" id="end_date"/>
							<input class="validate[required]" style="width:125px; margin:5px 0px 0px; display:block; font-family:Arial; font-size:12px;" type="time" id="end_time" />
						</div>
					</div>
				</div>
				<div class="labeled-input" style="width:100%">
					<label for="duration_type" style="width:80px; display:inline-block; text-align:left; position:relative;">Time limit:</label>
					<select class="defaultSelectBox" style="width:140px;" id="duration_type">
						<option value="none">None</option>
						<option value="defined">Specified</option>
					</select>
					<div id="duration_details" style="display:inline-block;">
						<input style="width:35px; margin:0px;" class="validate[required,min[1]] defaultTextBox2" type="number" min="1" id="duration_number"/><label style="margin:0px 5px; display:inline-block;">minutes</label>
					</div>
				</div>
				<div class="labeled-input" style="width:100%">
					<label for="attempts_type" style="width:80px; display:inline-block; text-align:left;">Attempts:</label>
					<select class="defaultSelectBox" style="width:140px;" id="attempts_type">
						<option value="defined">Specified</option>
						<option value="unlimited">Unlimited</option>
					</select>
					<div id="attempts_details" style="display:inline-block;">
						<input style="width:35px; margin:0px;" class="defaultTextBox2" type="number" min="1" id="attempts_number"/><label id="attempts_label" style="margin:0px 5px; display:inline-block;"><</label>
					</div>
				</div>
				<div class="labeled-input" style="width:100%">
					<label for="student_note" style="width:80px; display:inline-block; text-align:left; position:relative; top:3px;">Student note:</label>
					<div style="display:inline-block; vertical-align:top;">
						<input style="width:270px; margin:0px;" class="defaultTextBox2" type="text" id="student_note"/>
						<div style="font-size:12px; font-style:italic;">(will show in student index with above details)</div>
					</div>
				</div>
				<div id="editDocument_buttons" class="ui-dialog-buttonset custom-buttonpane" style="float:right; margin-top:5px;">
				<button type="button" class="btn btn-cancel btn_font">Cancel</button>
				<button type="button" class="btn btn-save btn_font">Save</button>
			</div>
			</div><div id="editDocument_wrapper_right" style="display:inline-block; vertical-align:top; background:rgb(215, 231, 238); border-bottom-right-radius:5px; width:324px; height:auto; border-left:1px solid #ccc;">
				<div style="width:100%; margin-left:-1px; height:40px; background:rgb(235, 235, 235); position:relative; border-bottom:1px solid #ccc;">
					<div class="document_settings_tab selected" data-wrapper-id="tab_sitting_wrapper">Sitting</div>
                    <?php
                    if (in_array('scoring', explode(',', $user['smart_allow']))) {
                    ?>
					<div class="document_settings_tab" data-wrapper-id="tab_scoring_wrapper">Scoring</div>
                    <?php
                    }
                    ?>
					<div class="document_settings_tab" data-wrapper-id="tab_results_wrapper">Results</div>
				</div>

				<div id="tab_sitting_wrapper" class="settings_wrapper">
					<div style="width:300px; margin-bottom:20px;">
						<div class="labeled-input" style="width:auto;">
							<input type="checkbox" class="normal_chkbx" id="shuffle_attempts" value="1" /><label for="shuffle_attempts" style="text-align:left; width:auto;">Shuffle each attempt</label>
						</div>
						<div class="labeled-input" style="width:100%; box-sizing:border-box; display:flex; align-items:center;">
							<label for="presentation" style="flex:none; width:auto;">Item presentation:</label>
							<select class="defaultSelectBox" style="flex:1 1 100%; padding-left:px;" id="presentation">
								<option value="all">Full assessment</option>
								<option value="free">Single item (free navigation)</option>
								<option value="next">Single item (next only)</option>
							</select>
						</div>
						<div class="labeled-input" style="width:100%; margin-bottom:5px; box-sizing:border-box; display:flex; align-items:center;">
							<label for="notes_policy" style="flex:none; width:auto;">Add reasoning prompt</label>
							<select class="defaultSelectBox" style="flex:1 1 100%; padding-left:px;" id="notes_policy">
								<option value="selected">on selected questions</option>
								<option value="all">on all MC/NR questions</option>
							</select>
						</div>
						<div id="notes_label_wrapper" class="labeled-input" style="padding-left:20px; width:100%; box-sizing:border-box; display:flex; align-items:center;">
							<label for="notes_label" style="flex:none; width:auto;">Label:</label>
							<input class="defaultTextBox2" type="text" id="notes_label" style="flex:1 1 100%;"/>
						</div>
					</div>
					<div id="warnings_wrapper" class="labeled-input" style="width:100%">
						<label for="warnings" style="width:auto; display:block; text-align:left; margin-bottom:5px !important;">Remaining time notifications:</label>
						<div id="warnings" style="width:100%;"></div>
					</div>
				</div>
				<div id="tab_scoring_wrapper" class="settings_wrapper" style="display:none;">
                    <div class="labeled-input" style="width:100%; margin-bottom:5px; box-sizing:border-box; display:flex; align-items:center;">
                        <label for="smart_wr_policy" style="flex:none; width:auto;">Smart WR scoring:</label>
                        <select class="defaultSelectBox" style="flex:1 1 100%; padding-left:px;" id="smart_wr_policy">
                            <option value="none">never (manual always)</option>
                            <option value="selected">on selected questions</option>
                            <option value="all">on all WR questions</option>
                        </select>
                    </div>
                </div>
				<div id="tab_results_wrapper" class="settings_wrapper" style="display:none;">
					<div class="labeled-input" style="width:100%; box-sizing:border-box;">
						<label style="text-align:left; width:auto; display:block;">Choose an item to edit its display settings:</label>
						<ul id="results_menu" style="padding:5px;" >
							<li data-item-key='scoring'><a href='#'><div class='item_check'></div><span>Scoring</span></a></li>
							<li data-item-key='outcomes'><a href='#'><div class='item_check'></div><span>Outcomes</span></a></li>
							<li data-item-key='questions'><a href='#'><div class='item_check'></div><span>Question book</span></a></li>
							<li data-item-key='answers'><a href='#'><div class='item_check'></div><span>Answer key</span></a></li>
						</ul>
					</div>
					<div style="display:grid; grid-template-columns: auto auto; grid-template-rows: auto auto; align-item:start; justify-content:space-between;">
						<div class="labeled-input item_status_wrapper" style="width:auto; grid-column:1; grid-row:1; margin-bottom:5px !important;">
							<input id="item_status" type="checkbox"><label for="item_status" style="text-align:left; width:auto;">Show in results</label>
						</div>
						<div class="labeled-input item_detail_wrapper" style="width:auto; grid-column:1; grid-row:2;">
							<select class="defaultSelectBox" style="width:140px; display:block; margin-bottom:5px !important;" id="item_show_type">
								<option value="auto">When submitted</option>
								<option value="close">When sitting closes</option>
								<option value="date">From date/time</option>
							</select>
							<div id="item_show_details" style="display:block;">
								<input class="datepicker defaultTextBox2 validate[required]" style="width:125px; margin:0px; display:block;" type="text" id="item_show_date"/>
								<input class="validate[required]" style="width:125px; margin:5px 0px 0px; display:block; font-family:Arial; font-size:12px;" type="time" id="item_show_time" />
							</div>
						</div>
						<div class="item_detail_wrapper" style="grid-column:2; grid-row:1;">
							<label style="text-align:left; width:auto;">Stop showing:</label>
						</div>
						<div class="labeled-input item_detail_wrapper" style="width:auto; grid-column:2; grid-row:2;">
							<select class="defaultSelectBox" style="width:140px; display:block; margin-bottom:5px !important;" id="item_hide_type">
								<option value="none">Never</option>
								<option value="date">At date/time</option>
							</select>
							<div id="item_hide_details" style="display:block;">
								<input class="datepicker defaultTextBox2 validate[required]" style="width:125px; margin:0px; display:block;" type="text" id="item_hide_date"/>
								<input class="validate[required]" style="width:125px; margin:5px 0px 0px; display:block; font-family:Arial; font-size:12px;" type="time" id="item_hide_time" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

<!-- Edit email dialog -->

	<div id="editEmail_dialog" class="dialogbox" title="Update student email">
		<div class="dialog_wrapper">
			<form class="formular" accept-charset="utf-8">
				<p style="min-height:50px;">
					Enter a new email address for the student below, then click "Save" to save the changes and send a new 
					email verification to the student.
					</p>
				<div class="labeled-input" style="width:100%;">
					<label for="new_email" style="display:block; text-align:left; margin:5px 0px 5px !important; with:auto;">Email address:</label>
					<input class="validate[required, custom[email]] defaultTextBox2" maxlength="100" type="text" id="new_email" style="width:340px;" />
				</div>
			</form>
		</div>
	</div>

<!-- Send verification dialog -->

	<div id="sendVerification_dialog" class="dialogbox" title="Re-send email verification">
		<div class="dialog_wrapper">
			<p style="min-height:50px;">
				Click "Send" to send another email verification link to "<span id="verification_email"></span>" now.
			</p>
		</div>
	</div>

<!-- Add students from class list dialog -->

    <div id="addClassList_dialog" class="dialogbox" title="Add from class list">
		<div class="dialog_wrapper">
            <p>Choose a class list below, then select or unselect students as needed. Click "Add" to add the 
            selected students to this sitting.</p>

            <?php echo $this->element("class_list_common"); ?>
            
		</div>
	</div>

<!-- Add students by email dialog -->

	<div id="addStudentEmails_form" class="dialogbox"  title="Add students by email">
		<div class="dialog_wrapper" style="height:100%;">
			<form class="formular" style="height:100%;" accept-charset="utf-8">
				<p>
				To give your students access to this sitting through an account, enter their email addresses
				below, then click "Add". Accounts will be created for students who do not have one already &mdash;
				once they have clicked the verification link in the welcome email, they will be able to log in to
				view sittings.</p>

				<div class="labeled-input" style="width:auto;">
					<label style="width:auto; text-align:left; display:block; margin-bottom:5px !important;">Email addresses:</label>
					<textarea class="validate[required] defaultTextBox2" style="box-sizing:border-box; width:100%; min-height:200px; resize:none;" id="dstu_emails"></textarea>
				</div>
			</form>
		</div>
	</div>

<!-- Delete attempt dialog -->

	<div id="deleteAttempt_dialog" class="dialogbox" title="Delete Attempt">
		<div class="dialog_wrapper">
			<p style="min-height:50px;">Permanently delete Attempt <span id="delete_attempt_number"></span> and all associated student 
			responses? This action can not be undone.</p>
		</div>
	</div>

<!-- Remove student dialog -->

	<div id="removeStudent_dialog" class="dialogbox" title="Remove Student">
		<div class="dialog_wrapper">
			<p id="removeStudent_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Change token access dialog -->

<div id="editLink_form" class="dialogbox"  title="Shareable link">
		<div class="dialog_wrapper" style="height:100%;">
			<p>
			Shareable links allow your students to access this sitting without having a SmarterMarks account.
			When the link is enabled, they will be able to access the sitting either by entering the link, or
			using the access code "<span id="link_code"></span>" from the login page.
			</p>
			<p>
			To enable the link, select "Enable link" below, then choose an identifier (for example, student ID) 
			and allowed values. Students will need to enter one of these values to access the sitting.
			</p>
			<form class="formular" style="height:100%; margin-top:20px;" accept-charset="utf-8">
				<div style="display:flex; flex-direction:column; height:100%;">
					<div class="labeled-input" style="width:auto; flex:none; vertical-align:center; margin-bottom:0px;">
						<label style="display:inline-block; text-align:left; width:65px;">Link:</label><img class="link_copy" style="display:inline-block; cursor:pointer; width:15px; margin-right:5px;" src="/img/icons/copy.svg" /><div class="tok_link"></div>
					</div>
					<div class="labeled-input" style="margin-left:70px;">
						<input type="checkbox" value="1" class="normal_chkbx" id="enable_link" /><label for="enable_link" style="color:#222;">Enable link</label>
					</div>
					<div id="identifier_wrapper">
						<div class="labeled-input" style="width:auto; flex:none; display:flex; align-items:flex-start;">
							<label for="dtok_identifier" style="flex:none; text-align:left; width:65px; position:relative; top:3px;">Identifier:</label>
							<div style="flex:1 1 100%;">
								<select class="defaultSelectBox pad5px identifier_select" style="height: 28px; width: 210px; display:block;" id="dtok_identifier"></select>
								<input class="validate[required] defaultTextBox2" type="text" style="width:196px; display:none;" id="dtok_custom" />
							</div>
						</div>
						<div style="flex:none; margin-top:10px !important;">
							<label style="width:auto; text-align:left;">Allowed values (one per line):</label>
						</div>
						<div style="width:100%; flex:1 1 100%;">
							<textarea id="dtok_identifier_list" class="defaultTextBox2" style="box-sizing:border-box; width:100%; min-height:100px; resize:none;"></textarea>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- Open sitting dialog -->

	<div id="openSitting_dialog" class="dialogbox" title="Open sitting">
		<div class="dialog_wrapper">
			<p style="min-height:50px;">Select a new closing policy for this sitting, then click "Open sitting" to continue.</p>

			<div class="labeled-input" style="width:auto;">
				<label for="sitting_closeType" style="width:103px; text-align:left; padding-right:0px;">Open this sitting:</label><select class="defaultSelectBox pad5px" style="width:240px" id="sitting_closeType">
					<option value="Specified">Until a specified date and time</option>
					<option value="Never" selected>Until closed manually</option>
				</select>
			</div>
			<div id="sitting_close_details" style="display:none; margin-left:100px;">
				<input class="datepicker defaultTextBox2" style="width:125px; margin:0px; display:block;" type="text" id="sitting_close_date"/>
				<input style="width:125px; margin:5px 0px 0px; display:block; font-family:Arial; font-size:12px;" type="time" id="sitting_close_time" />
			</div>
		</div>
	</div>

<!-- Close sitting dialog -->

	<div id="closeSitting_dialog" class="dialogbox" title="Close sitting">
		<div class="dialog_wrapper">
			<p style="min-height:50px;">Are you sure you want to close this sitting? Once the sitting has been closed, students will not be able to submit new responses.</p>
		</div>
	</div>

<!-- Open attempt dialog -->

<div id="openAttempt_dialog" class="dialogbox" title="Open attempt">
	<div class="dialog_wrapper">
		<p style="min-height:50px;">Select a new closing policy for this attempt, then click "Open attempt" to continue.</p>

		<div class="labeled-input" style="width:auto;">
			<label for="attempt_closeType" style="width:100px; text-align:left; padding-right:0px;">Open attempt:</label><select class="defaultSelectBox pad5px" style="width:240px" id="attempt_closeType">
				<option value="Default" selected>Until the default date and time</option>
				<option value="Specified">Until a specified date and time</option>
				<option value="Never">Until closed manually</option>
			</select>
		</div>
		<div id="attempt_close_details" style="display:none; margin-left:100px;">
			<input class="datepicker defaultTextBox2" style="width:125px; margin:0px; display:block;" type="text" id="attempt_close_date"/>
			<input style="width:125px; margin:5px 0px 0px; display:block; font-family:Arial; font-size:12px;" type="time" id="attempt_close_time" />
		</div>
	</div>
</div>

<!-- Edit MC scoring dialog -->

	<div id="mc_scoring_dialog" class="dialogbox" title="Change scoring">
		<div class="dialog_wrapper">
			<form class="formular" id="mcDetailsForm" accept-charset="utf-8">
				<div class="labeled-input" style="width:auto;">
					<label id="dmcq_scoring_label" for="dmcq_scoring" style="width:auto; text-align:left;">Scoring</label>
					<select class="defaultSelectBox pad5px" style="height: 28px; width: 120px" id="dmcq_scoring">
						<option value="Default">Regular scoring</option>
						<option value="Omit">Omit question</option>
						<option value="Bonus">Bonus question</option>
					</select>
				</div>
	
				<div id="dmcq_detailWrapper" style="margin-top:5px; margin-bottom:5px;">
					<div class="labeled-input" style="width:100%;">
						<label style="display:block; text-align:left;">Scored responses</label>
						<div id="dmcq_answer_list" style="width:100%;"></div>
					</div>
					<span class="input_subtext" id="dmcq_description" style="width:100%; min-height:15px; margin-top:5px; margin-bottom:5px;"></span>
				</div>
			</form>
		</div>
	</div>

<!-- Edit NR scoring dialog -->

	<div id="nr_scoring_dialog" class="dialogbox" title="Change scoring">
		<div class="dialog_wrapper">
			<form class="formular" accept-charset="utf-8">
				<input type="hidden" id="dnq_has_calculated" value="" />
				
				<div class="labeled-input" style="width:auto;">
					<label id="dnq_scoring_label" for="dnq_scoring" style="width:auto; text-align:left;">Scoring</label>
					<select class="defaultSelectBox pad5px" style="height: 28px; width: 120px" id="dnq_scoring">
						<option value="Default">Regular scoring</option>
						<option value="Omit">Omit question</option>
						<option value="Bonus">Bonus question</option>
					</select>
				</div>
	
				<div id="dnq_detailWrapper">
					<div id="nrOptions">
						<div style="clear:both; height:5px;"></div>
						<label for="dnq_answerWrapper" style="text-align:left;">Scored responses</label>
						<div id="dnq_answerWrapper"></div>
						<div id="numberOptions">
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" value="1" class="normal_chkbx" id="dnq_allowSmallErrors" /><label for="dnq_allowSmallErrors">Allow small errors</label>
							</div>
							<div id="dnq_fudgeFactorWrapper" class="labeled-input">
								<label for="dnq_fudgeFactor" style="margin-left:25px;">Tolerance (%):&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[100]] defaultTextBox2" style="width:134px !important;" type="text" id="dnq_fudgeFactor" />
							</div>
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" value="1" class="normal_chkbx" id="dnq_allowFactorOfTen" \><label for="dnq_allowFactorOfTen">Give part marks for factor-of-10</label>
							</div>
							<div id="dnq_tensValueWrapper" class="labeled-input">
								<label for="dnq_tensValue" style="margin-left:25px;">Multiplier:&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2" data-prompt-position="topLeft" style="width:167px !important;" type="text" id="dnq_tensValue" />
							</div>
							<div class="labeled-input" style="margin:10px 0px 5px;">
								<label style="text-align:left; padding:0px;">If significant digits in student response are incorrect:</label>
							</div>
							<div class="labeled-input" style="margin-left:15px;">
								<select class="defaultSelectBox pad5px" style="height: 28px; width: 145px" id="dnq_sigDigsBehaviour">
									<option value="Round">Round as needed</option>
									<option value="Zeros" selected>Add zeros as needed</option>
									<option value="Strict">Mark as incorrect</option>
								</select>
								<div id="dnq_sigDigsValueWrapper">
									<span style="font-size:13px; font-family:Arial, Helvetica, sans-serif;">and multiply score by&nbsp;</span><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2" data-prompt-position="topLeft:-275,30" style="width:25px !important; margin:0px;" type="text" id="dnq_sigDigsValue" />
								</div>
							</div>
						</div>
						<div id="selectionOptions">
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" value="1" class="normal_chkbx" id="dnq_markByDigits" /><label for="dnq_markByDigits">Marks are per correct digit</label>
							</div>
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" value="1" class="normal_chkbx" id="dnq_allowPartialMatch" /><label for="dnq_allowPartialMatch">Allow partial match</label>
							</div>
							<div id="dnq_partialValueWrapper" class="labeled-input">
								<label for="dnq_partialValue" style="margin-left:25px;">Multiplier:&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2" style="width:167px !important;" type="text" id="dnq_partialValue" />
							</div>
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" value="1" class="normal_chkbx" id="dnq_ignoreOrder" /><label for="dnq_ignoreOrder">Ignore selection order</label>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- Edit WR scoring dialog -->

	<div id="wr_scoring_dialog" class="dialogbox" title="Change scoring">
		<div class="dialog_wrapper" style="min-height:60px;">
			<form class="formular" accept-charset="utf-8">
				<div class="labeled-input" style="width:auto;">
					<label id="dwq_scoring_label" for="dwq_scoring" style="width:auto; text-align:left;">Scoring</label>
					<select class="defaultSelectBox pad5px" style="height: 28px; width: 120px" id="dwq_scoring">
						<option value="Default">Regular scoring</option>
						<option value="Omit">Omit question</option>
						<option value="Bonus">Bonus question</option>
					</select>
				</div>

				<div class="labeled-input" style="width:auto;">
					<label for="dwq_criteriaWrapper" style="text-align:left; display:inline-block;">Criteria:</label>
					<div id="dwq_criteriaWrapper" style="width:100%; margin:5px 0px 0px;"></div>
				</div>

				<div id="dwq_warning" style="display:none">
					<div style="display:inline-block; width:30px; padding:3px; vertical-align:top;"><img src="/img/warning.svg" /></div>
					<div style="display:inline-block; width:300px; padding-left:5px; vertical-align:top; line-height:normal; font-size:13px; font-family:Arial, Helvetica, sans-serif;">
						<div>Note: If the criteria for this question are changed, any scores already entered for it will be deleted.</div>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- Delete comment dialog -->

	<div id="deleteComment_dialog" class="dialogbox" title="Delete Comment">
		<div class="dialog_wrapper">
			<input type="hidden" id="deleteComment_index" value="" />
			<p style="min-height:50px;">Permanently delete the teacher comment for this response?</p>
		</div>
	</div>

<!-- Edit comment dialog -->

	<div id="editComment_dialog" class="dialogbox" title="Edit comment">
		<div class="dialog_wrapper" style="margin-bottom:0px; height:100%;">
			<form class="formular" style="margin-bottom:0px; height:100%;" accept-charset="utf-8">
				<input type="hidden" id="editComment_index" value="" />
				<div style="display:flex; flex-direction:column; height:100%;">
					<div class="labeled-input" style="width:100%; flex:1 1 auto; display:flex; flex-direction:column;">
						<div style="flex: 1 1 0px;" id="editComment_editor" class="template_view"></div>
					</div>
				</div>
			</div>
		</form>	     
	</div>


<!-- Export CSV dialog -->

	<div id="exportCSV_dialog" class="dialogbox" title="Export as CSV">
		<div class="dialog_wrapper" style="margin-bottom:0px; height:100%;">
			<div style="font-size:14px;">Include in CSV:</div>
			<div id="include_list" style="margin-left:10px; margin-top:5px;"></div>

			<div style="font-size:14px; margin-top:10px;">First row:</div>
			<div id="csv_layout" style="margin-top:5px;"></div>
		</div>
	</div>

</div>

<?php echo $this->element('html_edit'); ?>

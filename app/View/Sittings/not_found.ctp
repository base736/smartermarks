<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width:500px; height:auto; margin:50px auto;">
	<div class="rounded_white_wrapper_inner">

		<h3>Sitting not found</h3>
		
		<p>The sitting you attempted to join could not be found. Please check the link you used to get here.</p>
	</div>
</div>

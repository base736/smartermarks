<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
echo $this->element('layout_head', array(
    'include_print_element' => true,
    'include_build_version' => true
));
?>

<script>

var pageSettings = <?php echo json_encode($page_settings); ?>;

</script>

<script type="module" src="/js/common/build_version.js?7.0"></script>

<style>
	body, html {
		-webkit-print-color-adjust: exact;
		background-color: transparent; 
		margin: 0px;
	}

	@page  
	{ 
    	size: auto;
	}
</style>

</head>

<body style="background-color:none;">
	<?php
	$pageWidth = 215.9;
	if ($page_settings['pageSize'] == 'a4') $pageWidth = 210.0;
	$pageWidth -= $page_settings['marginSizes']['left'] * 25.4;
	$pageWidth -= $page_settings['marginSizes']['right'] * 25.4;
	?>
	
	<div id="print_wrapper" class="assessment_wrapper template_view student_view parts_formatted
		<?php if (isset($wrapper_classes)) echo ' ' . $wrapper_classes; ?>"
		style="width:<?php echo number_format($pageWidth, 2, '.', ''); ?>mm;">
		<?php echo $assessmentHTML; ?>
	</div>
</body>

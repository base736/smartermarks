<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<?php 
	if (empty($user['hidden_tips'])) $hiddenTips = array();
	else $hiddenTips = explode(";", $user['hidden_tips']); 
?>

<link rel="stylesheet" type="text/css" href="/css/bubble_tip.css?7.0" />
<script type="text/javascript" src="/js/common/bubble_tip.js?7.0"></script>

<script type="text/javascript">
	var userID = <?php echo $user['id'];?>;
	var canWriteCode = <?php echo $user['can_write_code'] == 1 ? 'true' : 'false'; ?>;
	var userDefaults = <?php echo $user['defaults']; ?>;

    var showCorrelation = <?php echo $user['correlations_beta'] ? 'true' : 'false'; ?>;

	var questionHandleID = <?php echo isset($question_handle_id) ? $question_handle_id : 'false'; ?>;
	var assessmentItemCount = <?php echo $assessment_item_count; ?>;

	var canEdit = <?php echo $canEdit; ?>;
	var canCancel = <?php echo $canCancel; ?>;
	var showSettings = <?php echo $showSettings; ?>;
	var showPartNote = <?php echo in_array("questions_intro", $hiddenTips) ? 'false' : 'true'; ?>;

	var userdataPrefix = "<?php
		$secrets = Configure::read('secrets'); 
		echo $secrets['s3']['userdata_prefix'];
	?>";

	var questionFolderId = <?php echo empty($questionFolderId) ? 'null' : $questionFolderId; ?>;
	var communityID = <?php echo empty($communityId) ? 'null' : $communityId; ?>;
</script>

<script type="application/json" id="treeData"><?php echo empty($jsonTree) ? '' : $jsonTree; ?></script>

<script type="module" src="/js/questions/build.js?7.0"></script>

<!-- Top bar -->

<div class="action_bar" style="margin-bottom:5px;">
	<div class="action_left" style="width:700px; color:#ccc;">
		<div style="display:table-cell; vertical-align:middle;" id="question_title"></div>
	</div>
	<div class="action_right" style="align-self:flex-start;">
		<?php
		if ($canEdit) {
		?>
		<button id="startEditDetails" type="button" class="btn btn-dark exclusive_button" disabled><span>Settings</span></button>
		<?php
		}
		?>
		<button id="preview" type="button" class="btn btn-dark exclusive_button" disabled><span>Preview</span></button>
	</div>
</div>

<!-- Versioning input -->

<?php
if ($canEdit) {

    if (!empty($user['smart_usage'])) {
        $smart_usage = json_decode($user['smart_usage'], true);
    } else $smart_usage = array();

    $smart_used = 0;
    $date_cutoff = strtotime("-1 month");

    foreach ($smart_usage as $index => $entry) {
        $date = strtotime($entry['date']);
        if ($date >= $date_cutoff) ++$smart_used;
    }

    if ($smart_used == 0) $used_text = 'no requests';
    else if ($smart_used == 1) $used_text = '1 request';
    else $used_text = $smart_used . ' requests';
?>

<div id="versioning_wrapper" class="edit_control smart_edit_control">
    <div style="background:rgba(255, 255, 255, 0.7); padding:10px; margin:5px 0px; border:1px solid #ccc; border-radius:5px;">
        <div style="display:flex; flex-direction:row; align-items:center; gap:10px;">
            <label style="flex:none; width:auto; margin:0px; ">Your request:</label>
            <div style="position:relative; flex:1 1 100%; margin:0px;">
                <input class="defaultTextBox2" id="smart_version_input" style="margin:0px; box-sizing:border-box; height:30px; width:100%;" type="text" autocomplete="off" />
                <div id="suggestion_list" style="position:absolute; display:none;">
                    <div class="suggestion_header">Examples:</div>
                    <div class="suggestion_row">Please translate all text into <div class="example_blank"></div></div>
                    <div class="suggestion_row">Please write a new version with the same questions and answers, but different wording.</div>
                    <div class="suggestion_row">Please modify to give a similar version about <div class="example_blank"></div></div>
                    <div class="suggestion_row">Please modify to use language at a grade level at or below grade <div class="example_blank"></div></div>
                    <div class="suggestion_row">Please modify this question to use randomized values and calculated responses.</div>
                </div>
            </div>
            <div class='wait_icon' style="flex:none; margin:0px; opacity:0;"></div>
            <div class="btn-group">
                <button id="smart_actions_dropdown" class="btn dropdown-toggle item_action_one" data-toggle="dropdown" href="#" disabled>
                    Actions 
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu pull-right">
                    <li><a href="#" id="smart_version_send" class="disabled-link smart_action">Send request</a></li>
                    <li><a href="#" id="smart_version_save" class="disabled-link smart_action">Save changes</a></li>
                </ul>
            </div>
        </div>
        <div style="margin-top:10px; display:flex; flex-direction:row; align-items:center; gap:10px;">
            <div style="flex:1 1 100%; font-size:12px; font-style:italic;">
            We've upgraded the AI engine &mdash; results may take a minute for long tasks.
<!--                Note: Smart assistant requests are currently limited to 50 per month.
                You have made <span id="smart_used"><?php echo $used_text; ?></span> in the past month. -->
            </div>
            <div class="btn-group" style="flex:none; width:auto;">
                <button id="smart_version_undo" class="btn btn-mini" disabled>&#x25C0;</button>
                <button id="smart_version_redo" class="btn btn-mini" disabled>&#x25B6;</button>
            </div>
        </div>
    </div>
</div>

<?php
}
?>

<!-- Left Panel-->

<div id="panel_wrapper" style="position:relative; width:890px; height:auto;">
	<div id="left_panel" style="width:295px; position:relative; float:left;">

        <!-- {* navigation panel *} -->
        <div class="window_bar">Views</div>
        <div class="window_content" style="padding:10px;">
            <ul id="view_menu" style="padding:5px;" >
                <li class="view_button" data-view-type="templates"><a href="#">Questions and answers</a></li>
                <li class="view_button" data-view-type="engine"><a href="#">Engine code</a></li>
                <li class="view_button" data-view-type="variables"><a href="#">Constants and variables</a></li>
                <li class="view_button" data-view-type="statistics"><a href="#">Item statistics</a></li>
            </ul>

            <div id="edit_mode_wrapper" class="labeled-input" style="margin:10px 0px 0px; width:auto;">
                <label style="width:auto; display:inline-block; text-align:left; font-size:14px;">Edit mode:</label>
                <select class="defaultSelectBox" style="width:130px; display:inline-block;" id="edit_mode_select" disabled>
                    <option value='manual' selected>Manual edit</option>
                    <option value='smart'>Smart assistant</option>
                </select>
            </div>

        </div>

        <!-- {* question parts *} -->
        <div class="question_details_wrapper templates_wrapper" id="parts_wrapper" style="margin-top:10px;">
            <div class="window_bar">Parts</div>
            <div class="window_content" style="padding:5px;">
                <div class="layout_panel">
                    <ul id="parts_menu" class="menu_wrapper"></ul>

                    <?php
                    if ($canEdit) {
                    ?>

                    <div class="edit_control manual_edit_control" style="margin-top:10px;">
                        <span style="color:#cccccc; font-size:13px; vertical-align:middle; margin-left:4px; margin-right:6px;">Add:</span>
                        <div class="btn-group" style="margin-left:2px;">
                            <button class="btn btn-small btn-dark" id="addPart">Part</button>
                        </div><div class="btn-group" style="margin-left:2px;">
                            <button class="btn btn-small btn-dark" id="addContext">Context</button>
                        </div>
                    </div>

                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

<!-- Right Panel-->

    <div style="width:585px; float:right;">

        <div id="loading_panel" style="display:block;">
            <div class="window_bar">Loading question</div>
            <div class="window_content" style="background-color:white; padding:10px;">
                <div><div class="wait_icon"></div><i>Loading&hellip;</i></div>
            </div>
        </div>

        <div class="question_details_wrapper templates_wrapper">

            <!-- {* notifications *} -->
            <div id="messages_panel" style="padding-bottom:10px;">
                <div class="window_bar">Messages</div>
                <div class="window_content">
                    <div id="highlighting_message" class="item_highlighted" style="margin:10px; padding:10px; color:#555; border:1px solid #ccc; border-radius:5px;">
                    </div>
                </div>
            </div>
        
            <!-- {* context template *} -->
            <div id="context_panel">
                <div class="window_bar">Context</div>
                <div class="window_content" style="padding:10px;">
                    <div class="context_answer_block" style="display:none;">
                        <div class="labeled-input" style="width:auto; display:inline-block;">
                            <label style="text-align:left; width:auto; display:inline-block; padding-right:5px;">Answers:</label>
                            <select class="defaultSelectBox pad5px" style="height:28px; width:100px; margin-bottom:5px;" id="jsq_answer_columns" disabled>
                                <option value='hide'>not shown</option>
                                <option value='1'>in 1 column</option>
                                <option value='2'>in 2 columns</option>
                                <option value='3'>in 3 columns</option>
                                <option value='4'>in 4 columns</option>
                                <option value='5'>in 5 columns</option>
                            </select>
                        </div>
                        <div class="labeled-input" style="width:auto; display:inline-block; margin-left:40px;">
                            <input type="checkbox" class="normal_chkbx" id="jsq_shuffle_context_answers" disabled /><label style="text-align:left;" for="jsq_shuffle_context_answers">Shuffle answers</label>
                        </div>
                    </div>

                    <div style="box-sizing:border-box; width:100%; padding:10px; height:auto; background-color:#FFF; color:#555; fill:#555; margin:0px; border:1px solid #ccc; border-radius:5px; min-height:42px; height:auto;" class="template_view">
                        <div id="jsq_context_template"></div>
                        <div class='contextAnswers' style='margin-top:10px;'></div>
                    </div>
                    
                    <?php 
                    if ($canEdit) {
                    ?>
                    <div style="float:right; margin-top:10px;" class="manual_edit_control">
                        <button class="btn btn-primary exclusive_button" id="startEditContext">Edit</button>
                    </div>
                    <div style="clear:both;"></div>
                    <?php
                    }
                    ?>					
                </div>
            </div>
            
            <!-- {* question template *} -->
            <div id="question_panel">
                <div class="window_bar"><span id="part_window_title"></span></div>
                <div id="jsq_options_wrapper" class="window_content" style="padding:10px;">
                    <div class="question_options_wrapper">
                        <div class="question_options_left">
                            <div class="options_grid type_block mc_block nr_block wr_block" style="gap:5px 10px; margin-bottom:10px;">
                                <label for="jsq_type" class="option_label">Type:</label>
                                <select id="jsq_style" class="defaultSelectBox pad5px option_input" style="height:28px; width:150px;" disabled>
                                    <option value='mc'>Multiple choice</option>
                                    <option value='nr'>Numeric response</option>
                                    <option value='wr'>Written response</option>
                                </select>
                                <div style="grid-column:1/1;"></div>
                                <select id="jsq_type" class="defaultSelectBox pad5px option_input" style="height:28px; width:150px;" disabled></select>
                            </div>
                            <div class="options_grid" style="gap:10px 10px;">
                                <div class="type_block mc_block shuffle_wrapper option_combined">
                                    <input id="jsq_shuffle_answers" style="flex:none;" type="checkbox" disabled />
                                    <label style="flex:none; line-height:normal; margin-left:5px;" for="jsq_shuffle_answers">Shuffle answers</label>
                                </div>

                                <label class="type_block nr_block option_label">Blanks:</label>
                                <input class="type_block nr_block defaultTextBox2 option_input" style="width:20px;" id="jsq_nr_digits" type="text" disabled />

                                <label class="type_block nr_block option_label" for="jsq_show_prompt_nr">Prompt:</label>
                                <select class="type_block nr_block defaultSelectBox pad5px option_input" style="width:150px;" id="jsq_show_prompt_nr" disabled>
                                    <option value='none'>No response prompt</option>
                                    <option value='simple'>Show simple prompt</option>
                                    <option value='digits'>Show with digits</option>
                                    <option value='text'>Custom</option>
                                </select>
                                
                                <div class="type_block mc_table_block option_combined">
                                    <input id="jsq_show_prompt_mc" style="flex:none;" type="checkbox" disabled />
                                    <label style="flex:none; line-height:normal; margin-left:5px;" for="jsq_show_prompt_mc">Show response prompt</label>
                                </div>
                                
                                <label class="type_block wr_block option_label">Height:</label>
                                <div class="type_block wr_block option_input" style="display:flex; flex-direction:row; align-items:center;">
                                    <input id="jsq_wr_height" class="defaultTextBox2" style="width:25px;" type="text" disabled />
                                    <label style="margin-left:5px;">in</label>
                                </div>
                            </div>
                        </div>
                        <div class="question_options_right">
                            <div class="type_block mc_block">
                                <div class="type_block mc_table_block">
                                    <div style="margin-top:4px;">
                                        <label style="display:inline-block;">Labels:</label>
                                        <div id="jsq_label_wrapper" style="display:inline-block; font-size:13px; line-height:20px; color:#494949"></div>
                                    </div>
                                </div>
                                <div id="jsq_mc_truefalse_block" class="type_block">
                                    <div class="option_checkbox_row" style="margin-bottom:5px;">
                                        <div class="option_checkbox_left">
                                            <label for="jsq_tfAnswer" style="margin-right:5px;">Answer:</label>
                                            <select id="jsq_tfAnswer" class="defaultSelectBox pad5px" style="height:28px; width:70px" disabled>
                                                <option value="True">True</option>
                                                <option value="False">False</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="type_block nr_block" style="width:100%;">
                                <div id="jsq_numberOptions" style="width:100%; display:flex; flex-direction:column; align-items:flex-start;">
                                    <div class="option_checkbox_row" style="margin-bottom:5px;">
                                        <div class="option_checkbox_left">
                                            <input id="jsq_allowSmallErrors" type="checkbox" disabled />
                                            <label for="jsq_allowSmallErrors">Allow small errors</label>
                                        </div>
                                        <div class="option_checkbox_right">
                                            <label for="jsq_fudgeFactor" class="fudgeFactor_block" style="margin-right:5px;">Tolerance (%):</label>
                                            <input id="jsq_fudgeFactor" class="defaultTextBox2 fudgeFactor_block" data-prompt-position="bottomLeft:-270" style="width:35px;" type="text" disabled />
                                        </div>
                                    </div>
                                    <div class="option_checkbox_row" style="margin-bottom:15px;">
                                        <div class="option_checkbox_left">
                                            <input type="checkbox" id="jsq_allowFactorOfTen" disabled \>
                                            <label for="jsq_allowFactorOfTen" disabled>Part marks for factor-of-10</label>
                                        </div>
                                        <div class="option_checkbox_right">
                                            <label for="jsq_tensValue" class="tensValue_block" style="margin-right:5px;">Multiplier:</label>
                                            <input id="jsq_tensValue" class="defaultTextBox2 tensValue_block" data-prompt-position="bottomLeft:-270" style="width:35px;" type="text" disabled />
                                        </div>
                                    </div>
                                    <div style="flex:none; height:auto; width:100%;">
                                        <label style="text-align:left; width:auto; margin-bottom:3px;">If significant digits in student response are incorrect:</label>
                                        <div class="labeled-input" style="width:100%; display:flex; flex-direction:row; justify-content:space-between; align-items:center;">
                                            <select class="defaultSelectBox pad5px" style="flex:none; width:155px;" id="jsq_sigDigsBehaviour" disabled>
                                                <option value="Round">Round as needed</option>
                                                <option value="Zeros" selected>Add zeros as needed</option>
                                                <option value="Strict">Mark as incorrect</option>
                                            </select>
                                            <span class="sigDigsValue_block" style="flex:none; width:auto; font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#494949;">and multiply score by&nbsp;</span>
                                            <input id="jsq_sigDigsValue" class="sigDigsValue_block defaultTextBox2" data-prompt-position="bottomLeft:-270" style="flex:none; width:25px; margin:0px;" type="text" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div id="jsq_selectionOptions">
                                    <div class="option_checkbox_row">
                                        <div class="option_checkbox_left">
                                            <input type="checkbox" id="jsq_markByDigits" disabled />
                                            <label for="jsq_markByDigits">Marks are per correct digit</label>
                                        </div>
                                    </div>
                                    <div class="option_checkbox_row">
                                        <div class="option_checkbox_left">
                                            <input id="jsq_allowPartialMatch" type="checkbox" disabled />
                                            <label for="jsq_allowPartialMatch">Allow partial match</label>
                                        </div>
                                        <div id="jsq_partialValueWrapper" class="option_checkbox_right">
                                            <label for="jsq_partialValue">Multiplier:</label>
                                            <input id="jsq_partialValue" class="defaultTextBox2" style="width:35px;" type="text" disabled />
                                        </div>
                                    </div>
                                    <div class="option_checkbox_row">
                                        <div class="option_checkbox_left">
                                            <input id="jsq_ignoreOrder" type="checkbox" disabled />
                                            <label for="jsq_ignoreOrder">Ignore selection order</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="type_block wr_block" style="margin-left:10px;">
                                <div style="margin-top:4px; margin-bottom:10px;">
                                    <label style="display:inline-block;">Criteria:</label>
                                    <div id="jsq_criteriaWrapper" style="display:inline-block; font-size:13px; line-height:20px; color:#494949"></div>
                                </div>
                                <div style="margin-top:4px;">
                                    <label style="display:inline-block; vertical-align:middle; margin-right:5px;">Form prompt:</label>
                                    <select class="defaultSelectBox pad5px" style="display:inline-block; vertical-align:middle; height:28px; width:70px" id="jsq_promptType" disabled>
                                        <option value="none">None</option>
                                        <option value="text" selected>Text</option>
                                        <option value="image">Image</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="box-sizing:border-box; width:100%; padding:10px; height:auto; background-color:#FFF; color:#555; fill:#555; margin-top:10px; border:1px solid #ccc; border-radius:5px; min-height:42px; height:auto;" id="jsq_question_template" class="template_view"></div>

                    <div id="jsq_prompt_wrapper" class="type_block nr_block labeled-input" style="width:100%; display:flex; flex-direction:row; align-items:center; margin:5px 0px 0px;">
                        <label style="flex:none; width:auto;">Prompt text:</label>
                        <input style="flex:1 1 100%;" class="validate[required] defaultTextBox2" type="text" id="jsq_prompt_text" disabled />
                    </div>

                    <?php 
                    if ($canEdit) {
                    ?>
                    <div style="float:right; margin-top:10px;" id="question_edit_wrapper" class="manual_edit_control">
                        <button class="btn btn-primary exclusive_button" id="startEditQuestion">Edit</button>
                    </div>
                    <div style="clear:both;"></div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            
            <!-- {* answers *} -->
            <div id="answer_panel">
                <div class="window_bar">Answers</div>
                <div class="window_content" id="categoryPanel">
                    <table class="panel_table">
                        <thead>
                            <tr>
                                <td width="10%" align="center" class="answer_label_column">Label</td>
                                <td>Answer</td>
                                <td width="12%" align="center" class="answer_value_column">Marks</td>
                            </tr>
                        </thead>
                        <tbody id="answerlist">
                        </tbody>
                    </table>
                    <div id="key_wrapper" style="padding:10px;">
                        <div class="labeled-input" style="width:100%;">
                            <label style="text-align:left; margin:5px 0px !important;">Key:</label>
                            <div id="question_key_table">
                                <table style="width:auto;">
                                    <thead>
                                        <tr>
                                            <td style="padding-bottom:5px;">Response</td>
                                            <td width="30px"></td>
                                            <td style="padding-bottom:5px;" width="40px">Marks</td>
                                        </tr>
                                    </thead>
                                    <tbody id="scored_responses">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <?php 
                    if ($canEdit) {
                    ?>
                    <div style="padding:10px;" class="manual_edit_control">
                        <button style="float:right;" class="btn btn-primary" id="openEditAnswers">Edit</button>
                        <div class="clear"></div>
                    </div>
                    <?php
                    }
                    ?>
                    
                </div>
            </div>

            <!-- {* rubric *} -->
            <div id="rubric_panel">
                <div class="window_bar">Rubric</div>
                <div class="window_content" id="categoryPanel" style="padding:10px;">
                    <div style="box-sizing:border-box; width:100%; padding:10px; height:auto; background-color:#FFF; color:#555; fill:#555; margin:0px; border:1px solid #ccc; border-radius:5px; min-height:42px; height:auto;" id="rub_rubric_template" class="template_view"></div>
                    <div style="box-sizing:border-box; width:100%;" id="rub_grid_wrapper"></div>

                    <?php 
                    if ($canEdit) {
                    ?>
                    <div style="margin-top:10px;" class="manual_edit_control">
                        <button style="float:right;" class="btn btn-primary" id="openEditRubric">Edit</button>
                        <div class="clear"></div>
                    </div>
                    <?php
                    }
                    ?>

                </div>
            </div>
            
        </div>

        <div class="question_details_wrapper" id="wizard_wrapper" style="margin-bottom:10px;">

            <!-- {* constants *} -->
            <div class="window_bar">Constants</div>
            <div class="window_content">
                <table id="constants_table" class="variables_table panel_table actions_table" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td width="317px">Value</td>
                            <td>Actions</td>
                        </tr>
                    </thead>
                    <tbody id="constantList"></tbody>
                </table>

                <?php 
                if ($canEdit) {
                ?>
                <div style="float:right; margin-right:10px; margin-bottom:10px;" class="manual_edit_control">
                    <button class="btn btn-primary exclusive_button" id="startAddConstant">Add Constant</button>
                </div>
                <div style="clear:both;"></div>
                <?php
                }
                ?>
            </div>

            <!-- {* generated variables *} -->
            <div class="window_bar">Randomized variables</div>
            <div class="window_content">
                <table id="randomized_table" class="variables_table panel_table actions_table" 
                    style="table-layout:fixed; margin-bottom:10px;">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td style="text-align:center;" width="80px">Minimum</td>
                            <td style="text-align:center;" width="80px">Maximum</td>
                            <td style="text-align:center;" width="70px">Step</td>
                            <td width="90px">Rounding</td>
                            <td style="text-align:center;">Actions</td>
                        </tr>
                    </thead>
                    <tbody id="randomizedList"></tbody>
                </table>

                <?php 
                if ($canEdit) {
                ?>
                <div style="float:right; margin-right:10px; margin-bottom:10px;" class="manual_edit_control">
                    <button class="btn btn-primary exclusive_button" id="startAddRandomized">Add Randomized</button>
                </div>
                <div style="clear:both;"></div>
                <?php
                }
                ?>
            </div>

            <!-- {* calculated variables *} -->
            <div class="window_bar">Calculated variables</div>
            <div class="window_content">

                <div id="variable_error_wrapper" style="width:100%; display:grid; grid-template-columns: auto 1fr; 
                    grid-column-gap:20px; border-bottom:1px solid #d7d7d7; padding:20px;">
                    <div style="padding-right:0px; justify-self:center;">
                        <img style="height:50px;" src="/img/error.svg" />
                    </div>
                    <div>
                        <p>Errors were encountered while building test shuffles:</p>
                        <ul id="variable_error_list"></ul>
                    </div>
                </div>

                <table id="calculated_table" class="variables_table panel_table actions_table" 
                    style="table-layout:fixed; margin-bottom:10px;">
                    <thead>
                        <tr>
                            <td width="120px">Name</td>
                            <td>Details</td>
                            <td width="85px">Actions</td>
                        </tr>
                    </thead>
                    <tbody id="calculatedList"></tbody>
                </table>

                <?php 
                if ($canEdit) {
                ?>
                <div style="float:right; margin-right:10px; margin-bottom:10px;" class="manual_edit_control">
                    <button class="btn btn-primary exclusive_button" id="startAddCalculated">Add Calculated</button>
                </div>
                <div style="clear:both;"></div>
                <?php
                }
                ?>
            </div>

            <!-- {* deleted variable *} -->

            <div style="display:none;">
                <div class="window_bar">Deleted</div>
                <div class="window_content">
                    <table id="deleted_table">
                        <thead>
                            <tr>
                                <td>Name</td>
                            </tr>
                        </thead>
                        <tbody id="deletedList"></tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="question_details_wrapper" id="engine_wrapper">

            <!-- {* modules *} -->
            <div class="window_bar">Modules</div>
            <div class="window_content">
                <table class="panel_table">
                    <thead>
                        <tr>
                            <td width="20%">Name</td>
                            <td>Description</td>
                        </tr>
                    </thead>
                    <tbody id="importList"></tbody>
                </table>
            </div>
        
            <!-- {* code *} -->
            <div class="window_bar">Code</div>
            <div class="window_content" style="padding:10px;">
                <label for="code_text" style="text-align:left;">Engine code:</label>
                <div id="code_text" style="margin-bottom:10px; border: 1px solid #d1e1ed !important; padding:5px;"></div>	

                <?php 
                if ($canEdit) {
                ?>
                <div style="float:right;" class="manual_edit_control">
                    <button class="btn btn-primary exclusive_button" id="startEditCode">Edit</button>
                </div>
                <div style="clear:both;"></div>
                <?php
                }
                ?>
            </div>
        </div>
        
        <div class="question_details_wrapper" id="statistics_wrapper">

            <div class="window_bar">Filters</div>
            <div class="window_content" style="padding:10px; display:flex; justify-content:space-between; align-items:flex-start;">
                <div>
                    <div class="labeled-input" style="width:auto;">
                        <label style="text-align:left; width:auto; padding-right:5px;">Include</label><select class="defaultSelectBox stats_filter_input" style="width:150px;" id="stats_included_sources">
                            <option value="All" selected>all response types</option>
                            <option value="Sitting">online sittings only</option>
                            <option value="ResponsePaper">paper versions only</option>
                        </select>
                    </div>
                    <div class="labeled-input" style="width:auto;">
                        <div style="display:inline-block; vertical-align:top;">
                            <input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_date" />
                        </div><div style="display:inline-block; vertical-align:top; position:relative; top:2px;">
                            <label for="stats_filter_by_date" style="text-align:left; width:auto;">Include only responses after date:</label>
                            <input class="datepicker defaultTextBox2" type="text" id="stats_min_date"/>
                        </div>
                    </div>
                    <div class="labeled-input" style="width:auto;">
                        <input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_user" /><label for="stats_filter_by_user" style="text-align:left; width:auto;">Include only my responses</label>
                    </div>
                </div>
                <div id="stats_button_wrapper" class="custom-buttonpane" style="display:inline-block;">
                    <div class='wait_icon' style="margin:0px; display:none;"></div>
                    <div class="btn-group" style="display:inline-block;">
                        <button class="btn" id="stats_update">Update statistics</button>
                    </div>
                </div>
            </div>
        
            <div class="window_bar">Item statistics</div>
            <div class="window_content" style="padding:10px; overflow:hidden; box-sizing:border-box;">
                <div class="sub_panel_title">Item</div>
                <div id="statistics_item" class="assessment_wrapper template_view sub_panel_content" style="padding:10px 5px; min-height:50px; background-color:white;">
                    <i>Loading question&hellip;</i>
                </div>

                <div id="item_statistics_wrapper">
                    <div class="sub_panel_title">Statistics (<span id="statistics_n"></span> total responses)</div>
                    <div class="sub_panel_content" id="item_statistics_data" style="padding:10px 5px; background-color:white;">
                        <i>Loading statistics&hellip;</i>
                    </div>
                </div>
            </div>

        </div>
            
    </div>
	<div style="clear:both;"></div>
</div>

<!-- Dialog boxes-->

<div class="hidden">

<!-- {* edit settings dialog *} -->

	<div id="editSettings_form" class="dialogbox"  title="Edit Settings">
		<form class="formular" id="settings_edit" accept-charset="utf-8">
			<div class="dialog_wrapper">
				<div class="labeled-input">
					<label for="djsq_name" style="width:30px; display:inline-block; text-align:left;">Title</label>
					<input id="djsq_name" class="validate[required] defaultTextBox2" type="text" style="width:290px"/>
				</div>
				<?php
				if ($user['can_write_code'] == 1) {
				?>
				<div class="labeled-input">
					<label for="djsq_engine_type" style="text-align:left; width:90px; display:inline-block;">Engine type:</label>
					<select id="djsq_engine_type" class="defaultSelectBox pad5px" style="height: 28px; width: 200px; display:inline-block;">
						<option value='wizard'>Wizard (default)</option>
						<option value='code'>Javascript code</option>
					</select>
				</div>
				<?php
				}
				?>
				<div class="labeled-input">
					<label id="djsq_answer_location_label" for="djsq_answer_location" style="text-align:left; width:90px; display:inline-block;">Answers given</label>
					<select id="djsq_answer_location" class="defaultSelectBox pad5px" style="height: 28px; width: 200px; display:inline-block;">
						<option value='parts'>with question parts (default)</option>
						<option value='context'>in context (matching questions)</option>
					</select>
				</div>
				<div class="labeled-input">
					<input type="checkbox" class="normal_chkbx" id="djsq_shuffle_parts" /><label style="text-align:left; width:auto;" for="djsq_shuffle_parts">Shuffle parts for multi-part items</label>
				</div>
				<div class="labeled-input">
					<input type="checkbox" class="normal_chkbx" id="djsq_open_checkbox" /><label id="djsq_open_checkbox_label" style="text-align:left; width:auto;" for="djsq_open_checkbox">Share to open question bank</label>
				</div>
			</div>
		</form>
	</div>

<!-- {* edit constant dialog *} -->

	<div id="editConstant_form" class="dialogbox"  title="Edit Constant">
		<div class="dialog_wrapper">
		<form class="formular" id="constant_edit" accept-charset="utf-8">
			<input type="hidden" id="wiz_constant_variable_name" class="variable_name" />
			<div>
				<div class="labeled-input">
					<label for="wiz_constant_display_name" style="width:50px; display:inline-block; text-align:left;">Name: </label>
					<input class="validate[required,funcCall[checkVariableName]] defaultTextBox2" type="text" id="wiz_constant_display_name" style="width:280px"/>
				</div>
				<div class="labeled-input">
					<label for="wiz_constant_value" style="width:50px; display:inline-block; text-align:left;">Value: </label>
					<input class="validate[required,custom[isNumber]] defaultTextBox2" type="text" id="wiz_constant_value" style="width:280px"/>
				</div>
			</div>
		</form>
		</div>
	</div>

<!-- {* edit randomized variable dialog *} -->

	<div id="editRandomized_form" class="dialogbox"  title="Edit Randomized Variable">
		<div class="dialog_wrapper">
		<form class="formular" id="randomized_edit" accept-charset="utf-8">
			<input type="hidden" id="wiz_randomized_variable_name" class="variable_name" />
			<div>
				<div class="labeled-input">
					<label for="wiz_randomized_display_name" style="width:60px; display:inline-block; text-align:left;">Name: </label>
					<input class="validate[required,funcCall[checkVariableName]] defaultTextBox2" type="text" id="wiz_randomized_display_name" style="width:270px"/>
				</div>
				<div class="labeled-input">
					<label for="wiz_randomized_subtype" style="width:60px; display:inline-block; text-align:left;">Type: </label>
					<select class="defaultSelectBox pad5px" style="height:30px !important; width:140px;" id="wiz_randomized_subtype">
						<option value='randomized'>From range</option>
						<option value='from_list'>From list</option>
					</select>
				</div>
				<div class="wiz_randomized_details wiz_randomized_randomized">
					<div class="labeled-input">
						<label for="wiz_randomized_min" style="width:60px; display:inline-block; text-align:left;">Minimum: </label>
						<input class="validate[required,custom[isNumber]] defaultTextBox2" type="text" id="wiz_randomized_min" style="width:75px"/>
					</div>
					<div class="labeled-input">
						<label for="wiz_randomized_max" style="width:60px; display:inline-block; text-align:left;">Maximum: </label>
					<input class="validate[required,custom[isNumber]] defaultTextBox2" type="text" id="wiz_randomized_max" style="width:75px"/>
					</div>
					<div class="labeled-input">
						<label for="wiz_randomized_step" style="width:60px; display:inline-block; text-align:left;">Step: </label>
						<input class="validate[required,custom[isNumber]] defaultTextBox2" type="text" id="wiz_randomized_step" style="width:75px"/>
					</div>
					<div class="labeled-input">
						<label style="width:60px; display:inline-block; text-align:left;">Format:</label>
						<select class="defaultSelectBox pad5px" style="height:30px !important; width:140px;" id="wiz_randomized_sdType">
							<option value='SigDigs'>Scientific / decimal</option>
							<option value='Scientific'>Scientific only</option>
							<option value='Decimals'>Decimal only</option>
							<option value='Fraction'>Fraction</option>
						</select>
						<div id="wiz_randomized_sd_wrapper" style="display:inline-block;">
							<label style="display:inline-block; width:auto; margin:0px; padding:0px;">to</label>
							<input id="wiz_randomized_sd" class="defaultTextBox2 intSpinner" type="text" />
							<label id="wiz_randomized_sdLabel" style="display:inline-block; width:auto; margin:0px; padding:0px;"></label>
						</div>
					</div>
				</div>
				<div class="wiz_randomized_details wiz_randomized_from_list">
					<div class="labeled-input">
						<label for="wiz_randomized_list" style="width:60px; display:inline-block; text-align:left;">List: </label>
						<input class="validate[required] defaultTextBox2" type="text" id="wiz_randomized_list" style="width:270px"/>
					</div>
				</div>
			</div>
		</form>
		</div>
	</div>

<!-- {* edit calculated variable dialog *} -->

	<div id="editCalculated_form" class="dialogbox"  title="Edit Calculated Variable">
		<div class="dialog_wrapper">
		<form class="formular" id="calculated_edit" accept-charset="utf-8">
			<input type="hidden" id="wiz_calculated_variable_name" class="variable_name" />
			<div>
				<div class="labeled-input">
					<label for="wiz_calculated_display_name" style="width:60px; display:inline-block; text-align:left;">Name: </label><input class="validate[required,funcCall[checkVariableName]] defaultTextBox2" type="text" id="wiz_calculated_display_name" style="width:300px"/>
				</div>
				<div class="labeled-input">
					<label for="wiz_calculated_subtype" style="width:60px; display:inline-block; text-align:left;">Type: </label><select class="defaultSelectBox pad5px" style="height:30px !important; width:140px;" id="wiz_calculated_subtype">
						<option value='calculated'>From formula</option>
						<option value='conditional'>Conditional</option>
					</select>
				</div>
				<div class="wiz_calculated_details wiz_calculated_calculated">
					<div class="labeled-input">
						<label for="wiz_calculated_equation" style="width:60px; display:inline-block; text-align:right; vertical-align:top; position:relative; top:43px;">=</label><div id="wiz_calculated_equation" class="variable_equation" style="width:312px;"></div>
					</div>
				</div>
				<div class="wiz_calculated_details wiz_calculated_conditional">
					<div class="labeled-input" style="margin-left:65px;">
						<label for="wiz_conditional_if" style="width:auto; display:block; text-align:left;">If&hellip;</label>
						<div id="wiz_conditional_if" class="variable_equation" style="width:285px;"></div>
					</div>
					<div class="labeled-input" style="margin-left:65px;">
						<label for="wiz_conditional_then" style="width:auto; display:block; text-align:left;">Then&hellip;</label>
						<div id="wiz_conditional_then" class="variable_equation" style="width:285px;"></div>
					</div>
					<div class="labeled-input" style="margin-left:65px;">
						<label for="wiz_conditional_else" style="width:auto; display:block; text-align:left;">Otherwise&hellip;</label>
						<div id="wiz_conditional_else" class="variable_equation" style="width:285px;"></div>
					</div>
				</div>
				<div class="labeled-input">
					<label style="width:60px; display:inline-block; text-align:left; position:relative; top:4px;">Format:</label><div style="vertical-align:top;">
						<div>
							<select class="defaultSelectBox pad5px" style="height:30px !important; width:140px; margin:0px;" id="wiz_calculated_sdType">
								<option value='SigDigs'>Scientific / decimal</option>
								<option value='Scientific'>Scientific only</option>
								<option value='Decimals'>Decimal only</option>
								<option value='Fraction'>Fraction</option>
							</select>
							<div id="wiz_calculated_sd_wrapper" style="display:inline-block;">
								<label style="display:inline-block; width:auto; margin:0px; padding:0px;">to</label>
								<input id="wiz_calculated_sd" class="defaultTextBox2 intSpinner" type="text" />
								<label id="wiz_calculated_sdLabel" style="display:inline-block; width:auto; margin:0px; padding:0px;"></label>
							</div>
						</div>
						<div id="wiz_calculated_round_wrapper">
							<input type="checkbox" class="normal_chkbx" id="wiz_calculated_round" /><label for="wiz_calculated_round"  style="width:auto; display:inline-block; text-align:left">Round immediately</label>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
	</div>

<!-- {* edit code dialog *} -->

	<div id="editCode_form" class="dialogbox" title="Edit Code">
		<div class="dialog_wrapper">
			<div id="code_editor" style="width:560px; height:250px; border: 1px solid #d1e1ed !important; padding:5px;"></div>	
		</div>
	</div>
	
<!-- {* edit context dialog *} -->

	<div id="editContext_form" class="dialogbox" title="Edit Context">
		<div class="dialog_wrapper" style="height:100%;">
			<form class="formular" id="context_edit" accept-charset="utf-8" style="height:100%;">
				<input type="hidden" id="djsq_context_handle_id" /> 
				<input type="hidden" id="djsq_context_template" /> 
				<div style="display:flex; flex-direction:column; height:100%;">
					<div class="context_answer_block" style="flex:none;">
						<div class="labeled-input" style="width:auto; display:inline-block;">
							<label style="text-align:left; width:auto; display:inline-block; padding-right:5px;">Answers:</label>
							<select class="defaultSelectBox pad5px" style="height:28px; width:100px; margin-bottom:5px;" id="djsq_answer_columns">
								<option value='hide'>not shown</option>
								<option value='1'>in 1 column</option>
								<option value='2'>in 2 columns</option>
								<option value='3'>in 3 columns</option>
								<option value='4'>in 4 columns</option>
								<option value='5'>in 5 columns</option>
							</select>
						</div>
						<div class="labeled-input" style="width:auto; display:inline-block; margin-left:40px;">
							<input type="checkbox" class="normal_chkbx" id="djsq_shuffle_context_answers" /><label style="text-align:left;" for="djsq_shuffle_context_answers">Shuffle answers</label>
						</div>
					</div>
					<div class="labeled-input" style="width:100%; flex:auto; display:flex; flex-direction:column;">
						<div style="flex: 1 1 0px;"  id="djsq_context_edit" class="template_view"></div>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- {* edit question dialog *} -->

	<div id="editQuestion_form" class="dialogbox" title="Edit Question">
		<div class="dialog_wrapper" style="height:100%;">
			<form class="formular" id="question_edit" accept-charset="utf-8" style="height:100%;">
				<input type="hidden" id="djsq_last_prompt" />
				<div id="djsq_options_wrapper" style="width:100%; display:flex; flex-direction:column; height:100%;">
					<div class="question_options_wrapper" style="width:580px; margin-bottom:10px;">
						<div class="question_options_left">
							<div class="options_grid type_block mc_block nr_block wr_block" style="gap:5px 10px; margin-bottom:10px;">
								<label for="djsq_type" class="option_label">Type:</label>
								<select id="djsq_style" class="defaultSelectBox pad5px option_input" style="height:28px; width:150px;">
									<option value='mc'>Multiple choice</option>
									<option value='nr'>Numeric response</option>
									<option value='wr'>Written response</option>
								</select>
								<div style="grid-column:1/1;"></div>
								<select id="djsq_type" class="defaultSelectBox pad5px option_input" style="height:28px; width:150px;"></select>
							</div>
							<div class="options_grid" style="gap:10px 10px;">
								<div class="type_block mc_block shuffle_wrapper option_combined">
									<input id="djsq_shuffle_answers" style="flex:none;" type="checkbox" />
									<label style="flex:none; line-height:normal; margin-left:5px;" for="djsq_shuffle_answers">Shuffle answers</label>
								</div>

								<label class="type_block nr_block option_label">Blanks:</label>
								<input class="validate[required] type_block nr_block defaultTextBox2 option_input" style="width:20px;" id="djsq_nr_digits" type="text" />

								<label class="type_block nr_block option_label" for="djsq_show_prompt_nr">Prompt:</label>
								<select class="type_block nr_block defaultSelectBox pad5px option_input" style="width:150px;" id="djsq_show_prompt_nr">
									<option value='none'>No response prompt</option>
									<option value='simple'>Show simple prompt</option>
									<option value='digits'>Show with digits</option>
									<option value='text'>Custom</option>
								</select>
								
								<div class="type_block mc_table_block option_combined">
									<input id="djsq_show_prompt_mc" style="flex:none;" type="checkbox" />
									<label style="flex:none; line-height:normal; margin-left:5px;" for="djsq_show_prompt_mc">Show response prompt</label>
								</div>
								
								<label class="type_block wr_block option_label">Height:</label>
								<div class="type_block wr_block option_input" style="display:flex; flex-direction:row; align-items:center;">
									<input id="djsq_wr_height" class="defaultTextBox2 marginSpinner" style="width:35px;" type="text" />
									<label style="margin-left:5px;">in</label>
								</div>
							</div>
						</div>
						<div class="question_options_right">
							<div class="type_block mc_table_block">
								<div style="margin-bottom:5px;">
									<label style="display:inline-block; vertical-align:top; position:relative; top:6px;">Labels:</label>
									<div id="djsq_label_wrapper" style="display:inline-block; vertical-align:top; font-size:13px; line-height:20px; color:#494949"></div>
								</div>
							</div>
							<div id="djsq_mc_truefalse_block" class="type_block">
								<div class="option_checkbox_row" style="margin-bottom:5px;">
									<div class="option_checkbox_left">
										<label for="djsq_tfAnswer" style="margin-right:5px;">Answer:</label>
										<select id="djsq_tfAnswer" class="defaultSelectBox pad5px" style="height:28px; width:70px">
											<option value="True">True</option>
											<option value="False">False</option>
										</select>
									</div>
								</div>
							</div>
							<div class="type_block nr_block" style="width:100%;">
								<div id="djsq_numberOptions" style="width:100%; display:flex; flex-direction:column; align-items:flex-start;">
									<div class="option_checkbox_row" style="margin-bottom:5px;">
										<div class="option_checkbox_left">
											<input id="djsq_allowSmallErrors" type="checkbox" />
											<label for="djsq_allowSmallErrors">Allow small errors</label>
										</div>
										<div class="option_checkbox_right">
											<label for="djsq_fudgeFactor" class="fudgeFactor_block" style="margin-right:5px;">Tolerance (%):</label>
											<input id="djsq_fudgeFactor" class="validate[required] defaultTextBox2 fudgeFactor_block" data-prompt-position="bottomLeft:-270" style="width:35px;" type="text" />
										</div>
									</div>
									<div class="option_checkbox_row" style="margin-bottom:15px;">
										<div class="option_checkbox_left">
											<input type="checkbox" id="djsq_allowFactorOfTen" \>
											<label for="djsq_allowFactorOfTen">Part marks for factor-of-10</label>
										</div>
										<div class="option_checkbox_right">
											<label for="djsq_tensValue" class="tensValue_block" style="margin-right:5px;">Multiplier:</label>
											<input id="djsq_tensValue" class="validate[required] defaultTextBox2 tensValue_block" data-prompt-position="bottomLeft:-270" style="width:35px;" type="text" />
										</div>
									</div>
									<div style="flex:none; height:auto; width:100%;">
										<label style="width:auto; margin-bottom:3px;">If significant digits in student response are incorrect:</label>
										<div class="labeled-input" style="width:100%; display:flex; flex-direction:row; justify-content:space-between; align-items:center;">
											<select class="defaultSelectBox pad5px" style="flex:none; width:155px;" id="djsq_sigDigsBehaviour">
												<option value="Round">Round as needed</option>
												<option value="Zeros" selected>Add zeros as needed</option>
												<option value="Strict">Mark as incorrect</option>
											</select>
											<span class="sigDigsValue_block" style="flex:none; width:auto; font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#494949;">and multiply score by&nbsp;</span>
											<input id="djsq_sigDigsValue" class="validate[required] defaultTextBox2 sigDigsValue_block" data-prompt-position="bottomLeft:-270" style="flex:none; width:25px; margin:0px;" type="text" />
										</div>
									</div>
								</div>
								<div id="djsq_selectionOptions">
									<div class="option_checkbox_row">
										<div class="option_checkbox_left">
											<input type="checkbox" id="djsq_markByDigits" />
											<label for="djsq_markByDigits">Marks are per correct digit</label>
										</div>
									</div>
									<div class="option_checkbox_row">
										<div class="option_checkbox_left">
											<input id="djsq_allowPartialMatch" type="checkbox" />
											<label for="djsq_allowPartialMatch">Allow partial match</label>
										</div>
										<div id="djsq_partialValueWrapper" class="option_checkbox_right">
											<label for="djsq_partialValue">Multiplier:</label>
											<input id="djsq_partialValue" class="validate[required] defaultTextBox2d" data-prompt-position="bottomLeft:-270" style="width:35px;" type="text" />
										</div>
									</div>
									<div class="option_checkbox_row">
										<div class="option_checkbox_left">
											<input id="djsq_ignoreOrder" type="checkbox" />
											<label for="djsq_ignoreOrder">Ignore selection order</label>
										</div>
									</div>
								</div>
							</div>
							<div class="type_block wr_block" style="margin-left:10px;">
								<div style="min-height:30px; margin-bottom:10px;">
									<label style="display:inline-block; vertical-align:top; position:relative; top:6px;">Criteria:</label>
									<div id="djsq_criteriaWrapper" style="display:inline-block; vertical-align:top; font-size:13px; line-height:20px; color:#494949"></div>
								</div>
								<div>
									<label style="display:inline-block; vertical-align:middle; margin-right:5px;">Form prompt:</label>
									<select class="defaultSelectBox pad5px" style="display:inline-block; vertical-align:middle; height:28px; width:70px" id="djsq_promptType">
										<option value="none">None</option>
										<option value="text" selected>Text</option>
										<option value="image">Image</option>
									</select>
									<button id="startEditPrompt" class="btn btn-mini btn_font" style="display:inline-block; margin-left:10px;">Edit</button>
								</div>
							</div>
						</div>
					</div>
					<div class="labeled-input" style="width:100%; flex:auto; display:flex; flex-direction:column;">
						<div style="flex: 1 1 0px;" id="djsq_question_edit" class="template_view"></div>
					</div>
					<div id="djsq_prompt_wrapper" class="type_block nr_block labeled-input" style="width:100%; display:flex; flex-direction:row; align-items:center; margin:0px;">
						<label style="flex:none; width:auto;">Prompt text:</label>
						<input style="flex:1 1 100%;" class="validate[required] defaultTextBox2" type="text" id="djsq_prompt_text" />
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- {* text prompt edit dialog *} -->

	<div id="editPromptText_dialog" class="dialogbox" title="Edit Prompt Text">
		<div class="dialog_wrapper" style="position:relative;">
			<div class="labeled-input" style="width:auto; margin-bottom:0px;">
				<label style="text-align:left; width:auto; display:block;">Prompt text:</label>
				<textarea class="defaultTextBox2" style="height:100px; width:465px;" id="djsq_wr_prompt_text"></textarea>
			</div>
		</div>
	</div>

<!-- {* image prompt edit dialog *} -->

	<div id="editPromptImage_dialog" class="dialogbox" title="Edit Prompt Image">
		<div class="dialog_wrapper">
			<form class="formular" id="prompt_image_add" accept-charset="utf-8">
				<input type="hidden" id="djsq_prompt_image_name" />
				<input type="hidden" name="jobName" id="prompt_image_target">
				<input type="hidden" id="prompt_image_extension">
	
				<div class="labeled-input" style="width:auto; margin-bottom:0px;">
					<label style="text-align:left; width:auto; display:block;">Prompt image:</label>
					<div>
						<div id="djsq_prompt_image" style="width:120px; height:120px; padding:5px; background:white; border:1px solid #ccc; display:inline-block; vertical-align:top;"><div id="djsq_prompt_image_blank" style="width:100%; height:100%; color:#ccc; text-align:center; line-height:110px;">No image</div></div>
						<div style="display:inline-block; margin-left:10px; vertical-align:top;">
							<div class="labeled-input" style="width:auto;">
								<label style="text-align:left; width:45px; display:inline-block; margin-bottom:10px; vertical-align:middle;">Height:</label>
								<input style="vertical-align:middle;" class="validate[max[10.0],min[0.5],required] marginSpinner" type="text" id="djsq_prompt_image_height" />
								<label style="display:inline-block; width:auto; margin-bottom:10px; vertical-align:middle;">in</label>
							</div>
							<div id="prompt_fileInterface" style="position:relative; height:80px; width:350px; margin-top:20px !important;">
								<div id="prompt_filePrompt" style="position:absolute; height:100%; width:100%; border-radius: 15px; opacity:0.0; border-style:solid; border-width:3px; border-color:black; text-align:center;">
									<h2>Drop your file here</h2>
								</div>
								<div id="prompt_inputs" style="position:absolute; height:100%; width:100%; opacity:1.0;">
									<div style="padding-bottom:10px;">
										<div id="prompt_select_button" class="btn fileinput-button" style="height:20px; display:inline-block; vertical-align:middle;">
											Select new
											<input id="prompt_fileupload" type="file" name="files[]" accept="image/jpeg|image/png">
                                        </div>
										<div id="prompt_filename" style="padding-left:10px; font-family:Arial, Helvetica, sans-serif; font-size:14px; width:220px; display:inline-block; white-space:nowrap; overflow:hidden; text-overflow:ellipsis; vertical-align:middle;">No file selected</div>
									</div>
									<div style="clear:both; height:20px; padding-bottom:10px;">
										<div id="prompt_upload_button" class="btn fileinput-button pull-left" disabled>Upload</div>
										<div id="prompt_progressbar" class="pull-right" style="width:250px; margin-right:10px; display:none;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- {* delete answers confirmation dialog *} -->

	<div id="confirmation_dialog" class="dialogbox" title="Delete all answers">
		<div class="dialog_wrapper" style="display:flex; flex-flow:space-between; align-items:center; gap:10px;">
			<div style="flex:1 1 auto;">
				<img style="height:50px;" src="/img/warning.svg" />
			</div>
			<div style="flex:1 1 100%; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">
				Changing the question type or number of labels will delete any answers you have defined for this part. Click "Save" to continue, or "Cancel" to return to the editor.
			</div>
		</div>
	</div>

<!-- {* edit key dialog *} -->

    <div id="editAnswers_dialog" class="dialogbox" title="Edit Answers">
		<div class="dialog_wrapper">
            <form class="formular" accept-charset="utf-8">
                <input type="hidden" id="scoring_json" />
                <div id="dans_answer_header" style="display:flex; gap:10px; align-items:flex-end; font-size:14px; margin-bottom:10px;"></div>
                <div id="dans_answer_rows" style="padding:0px;"></div>
                <div id="dans_scoring_header" style="font-size:14px; margin:20px 0px 10px;">Scored responses:</div>
                <div id="dans_scoring_rows" style="padding-right:20px;"></div>
                <div id="dans_scoring_description" class="input_subtext" style="width:100%; margin:10px 0px 5px;"></div>
            </form>

            <div style="margin-top:10px; flex:none; display:flex; justify-content:space-between; align-items:center;">
                <div>
                    <button id="dans_add_answer" type="button" class="btn btn_font"><span class="add_icon text_icon" style="vertical-align:middle; margin-right:5px;"></span><span style="vertical-align:middle;">Add answer</span></button>
                    <button id="dans_show_key" type="button" class="btn btn_font">Show key</button>
                </div>
                <div class='custom-buttonpane' style="margin:6px 0px;">
                    <button type="button" class="btn btn-cancel btn_font">Cancel</button>
                    <button type="button" class="btn btn-save btn_font">Save</button>
                </div>
            </div>
		</div>
	</div>

<!-- {* edit rubric dialog *} -->

	<div id="editRubric_form" class="dialogbox" title="Edit Rubric">
		<div class="dialog_wrapper" style="height:100%;">
			<form class="formular" id="rubric_edit" accept-charset="utf-8" style="height:100%;">
				<div style="display:flex; flex-direction:column; align-items:flex-start; width:100%; height:100%;">
    				<div class="labeled-input" style="flex: 0 0 auto; width:100%;"> 
						<label style="text-align:left; width:auto; display:inline-block; padding-right:5px;">Rubric type:</label>
						<select class="defaultSelectBox pad5px" style="height:28px; width:100px;" id="drub_type">
							<option value='free'>Free text</option>
							<option value='structured'>Structured</option>
						</select>
                    </div>
                    <div id="rubric_template_wrapper" class="rubric_free" style="flex: 1 1 100%; width:100%; display:flex;">
    					<div style="flex:1 1 0px; width:100%; box-sizing:border-box; background:white; position:relative;" id="drub_rubric_edit" class="template_view"></div>
                    </div>
                    <div id="rubric_grid_wrapper" class="rubric_structured" style="flex: 1 1 100%; width:100%; overflow-y:scroll;"></div>
				</div>
			</form>
		</div>
	</div>

<!-- Delete part dialog -->

	<div id="deletePart_dialog" class="dialogbox" title="Delete Question Part">
		<div class="dialog_wrapper">
			<p id="deletePart_text" style="min-height:50px;"></p>
			<input style="display:none;" type="text" id="deletePart_id"/>
		</div>
	</div>

<!-- First part edit dialog -->

	<div id="editPart_note" class="dialogbox" title="A note about parts">
		<div class="dialog_wrapper">
			<div>
				<p>This tool will let you build or edit a single question. Each question contains an optional context followed by one or more related parts, as shown below. Assessments are built from many questions.</p>
				<p>When you are done editing the first part of this question, you can add more parts by clicking the "Add Part" button in the left bar, or begin building your next question by clicking the "New Question" button at the top right of the window.</p>
				<p>For more information, check out our short <a href="https://www.youtube.com/watch?v=K0xy5gn5toY" target="_blank">tutorial video</a>.</p>
			</div>
			<div style="width:80%; margin:10px auto 0px; padding:20px; background:white;"><img style="width:100%;" src="/img/item_example.png" /></div>
		</div>
	</div>

<!-- Edit item dialog -->

	<div id="saveType_dialog" class="dialogbox" title="Modify or version question">
		<div class="dialog_wrapper">
			<div style="display:flex; flex-direction:row; align-items:flex-start; gap:10px;">
				<div style="flex:none; width:40px;"><img src="/img/info.svg" /></div>
				<div style="flex:1 1 100%; font-size:14px;">
					<div>
						<p><b>Note:</b> This question is being used in <span id="assessment_item_count"></span> assessment templates.</p>
						<p>Modifying this question will change all of these templates. Choose "Modify all" below to continue, or "New version" 
						to create a new version of the question for this assessment instead.</p>
					</div>
					<div id="saveType_buttons" style="display:flex; flex-direction:row; justify-content:space-between; padding:10px 10px 10px 0px;">
						<div style="flex:none; width:auto;">
							<button id="saveType_modify" type="button" class="btn btn_font">Modify all</button>
							<button id="saveType_version" type="button" class="btn btn_font">New version</button>
						</div>
						<div style="flex:none; width:auto;">
							<button id="saveType_cancel" type="button" class="btn btn-cancel btn_font">Cancel</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

		
<!-- Target folder dialog -->

	<div id="targetFolder_dialog" class="dialogbox" title="Save new version">
		<div class="dialog_wrapper">
			<div class="labeled-input">
				<label style="text-align:left; display:inline-block; width:40px;">Name:</label>
				<input class="validate[required] defaultTextBox2" style="width:293px;" type="text" id="target_name" />
			</div>
			<div class="labeled-input">
				<label style="text-align:left;">Save to folder:</label><div style="clear:both;"></div>
			</div>
			<div style="width:100%; background:white; border:solid 1px #abcfda;">
				<div class="folderTree" data-prefix="tree_targetFolder_" id="targetFolderParent" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px">
				</div>
			</div>
		</div>
	</div>

	<!-- {* version conflict dialog *} -->

	<div id="version_conflict_dialog" class="dialogbox" title="Version conflict">
		<div class="dialog_wrapper" style="display:flex; flex-flow:space-between; align-items:flex-start; gap:10px;">
			<div style="flex:1 1 auto; margin-top:5px;">
				<img style="height:50px;" src="/img/error.svg" />
			</div>
			<div style="flex:1 1 100%; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">
				<p>It looks like this question is being edited from another window, or by another teacher.</p>
				<p>This page will need to be reloaded, clearing your changes, before you can continue. Click "Go back" 
				to close this dialog, or "Reload" to reload the page.</p>
			</div>
		</div>
	</div>

</div>

<?php echo $this->element('html_edit'); ?>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript" src="/js/common/confetti.js"></script>
<script type="module" src="/js/questions/index.js?7.0"></script>

<div class="action_bar<?php if (!$adminView) echo ' right_half' ?>">
	<div class="action_left">
		<div class="checkbox_arrow"></div>
		<div class="index_actions trash_actions" style="display:none;">
			<div class="btn-group">							
				<button type="button" class="btn btn-dark item_action_many moveItem_button"><span>Move</span></button>
			</div>
		</div>
		<div class="index_actions open_actions" style="display:none;">
			<div class="btn-group">							
				<button type="button" class="btn btn-dark item_action_one preview_button"><span>Preview</span></button>
			</div>
			<div class="btn-group">							
				<button type="button" class="btn btn-dark item_action_many copyItem_button"><span>Copy</span></button>
			</div>
		</div>
		<div class="index_actions default_actions" style="display:none;">
			<div class="btn-group">
				<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
					Build
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu text-left">
					<li><a href="#" class="editQuestion_button item_action_one">Edit question</a></li>
					<li><a href="#" id="duplicateItem_button" class="item_action_many <?php echo (!isset($user['role']) || $user['role'] != 'admin') ? 'needs_copy' : 'always-disabled-link'; ?>">Duplicate</a></li>
				</ul>
			</div>

			<div class="btn-group">							
				<button type="button" class="btn btn-dark item_action_one preview_button"><span>Preview</span></button>
			</div>

			<div class="btn-group">
				<div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Manage 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" class="<?php echo (!isset($user['role']) || $user['role'] != 'admin') ? "item_action_many needs_delete moveItem_button" : "disabled"; ?>">Move to folder</a></li>
						<li><a href="#" class="<?php echo (!isset($user['role']) || $user['role'] != 'admin') ? 'item_action_many copyItem_button' : 'always-disabled-link'; ?>">Copy to folder</a></li>
						<li><a href="#" id="deleteItem_button" class="item_action_many needs_delete">Move to trash</a></li>
					</ul>
				</div>
				<div class="btn-group">
					<button id="share_dropdown" class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Share 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="sendCopy_button" class="item_action_many needs_copy">Send a copy</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="action_right">
		<div class="btn-group">
			<button class="btn btn-dark search_dropdown dropdown-toggle" href="#">
				Search 
				<span class="caret"></span>
			</button>
			<div class="pull-right search_menu">
				<?php
				if (!$adminView) {
				?>	

				<div id="search_options" style="margin-bottom:5px;">
					<div style="vertical-align:middle; display:inline-block;">Search:</div>
					<div style="vertical-align:middle; display:inline-block;">
						<select id="search_type" style="margin:0px; width:160px;">
							<option value="folder">in this folder</option>
							<option value="subfolders">with sub-folders</option>
							<option value="all">in all folders</option>
						</select>
					</div>
				</div>

				<?php
				}
				?>
				
				<div id="search_wrapper" style="position:relative; width:auto;">
					<input class="search_text" type="text" />
					<div class="search_icon"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="grid-column:right; grid-row:main; display:flex; flex-direction:column;">
	<div class="window_bar<?php if (!$adminView) echo ' right_half' ?>" style="flex:none; margin-top:1px; padding-left:10px;">
        <div style="width:5px">&nbsp;</div>
		<?php
		if (isset($user['role']) && $user['role'] == 'admin') {
		?>	
		<div style="width:250px;">Owner</div>
		<?php
		}
		?>
		<div id="created_header" style="width:120px;">Created</div>
		<div style="width:auto;">Name</div>

		<div class="sort_wrapper">
			<div id="sort_direction_wrapper" style="vertical-align:middle; display:inline-block; line-height:normal;">
				<div class="header_sort" data-direction="asc" style="display:block;">▲</div>
				<div class="header_sort" data-direction="desc" style="display:block;">▼</div>
			</div>
			<span style="white-space:nowrap;">Sort by</span>
			<select id="sort_type"></select>
            <?php
            if (!$adminView) {
            ?>
			<span style="margin-left:10px;">Show</span>
			<select id="show_limit">
                <option value="10">10</option>
                <option value="all">all</option>
			</select>
            <?php
            }
            ?>
		</div>
	</div>
	<div id="index_right" class="index_body<?php if (!$adminView) echo ' right_half' ?>" style="flex:1 1 100%; overflow:hidden; min-height:300px; width:100%;">
		<table class="index_table selection_table">
		</table>
		<div id="footer_wrapper" style="display:flex; justify-content:space-between; height:auto; margin:20px 10px;">
			<div id="count_wrapper" style="color:#75797c;"></div>
			<div id="pagination_wrapper" class="pagination_wrapper"></div>
		</div>
	</div>
</div>

<div class="hidden">

<!-- Preview dialog -->

<div id="preview_dialog" class="dialogbox" title="Question preview">
	<div class="dialog_wrapper" style="height:100%; flex:auto; display:flex; flex-direction:column;">
		<div style="flex:1 1 0px; background:white; padding:10px; overflow-y:scroll; border-radius:5px; border:1px solid #ccc; font-size:14px; min-height:200px;">
			<div id="preview_loading" style="display:none;"><div class="wait_icon"></div>Loading&hellip;</div>
			<div id="preview_stats_wrapper" style="margin-bottom:15px;"></div>
			<div id="preview_question_wrapper" class="assessment_wrapper template_view">
				<div class="question_wrapper"></div>
			</div>
		</div>
		<div style="margin-top:10px; flex:none; display:flex; justify-content:space-between; align-items:center;">
			<div>
				<button id="preview_prev" type="button" class="preview_nav btn btn_font">&#x25C0; Prev</button>
				<button id="preview_next" type="button" class="preview_nav btn btn_font">Next &#x25B6;</button>
			</div>
			<div class='custom-buttonpane' style="margin:6px 0px;">
				<button id="preview_close" type="button" class="btn btn-save btn_font">Close</button>
			</div>
		</div>
	</div>
</div>

</div>

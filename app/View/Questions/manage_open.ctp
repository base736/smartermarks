<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript" src="/js/common/confetti.js"></script>
<script type="module" src="/js/questions/manage_open.js?7.0"></script>

<script type="text/javascript">
	var openCreated = <?php echo $open_created; ?>;
</script>

<script type="application/json" id="treeData"><?php echo empty($jsonTree) ? '' : $jsonTree; ?></script>

	<h2 style="margin-bottom:20px;">Manage Open Questions</h2>

	<div class="window_bar">Sharing preferences</div>
	<div class="window_content" style="margin-bottom:20px;">
		<div style="margin:20px;">
			<div style="margin-bottom:20px;">
				Use the checkbox below to share new questions you create to the open question bank by default. You will be able to change this for each question individually using the "Share to open question bank" checkbox in the question's settings.
			</div>
			<div style="display:flex; justify-content:space-between;">
				<div class="labeled-input">
					<input type="checkbox" class="normal_chkbx" id="open_created" /><label for="open_created">Share questions I create in the future by default.</label>
				</div>
				<div>
					<button type="button" id="save_default" class="btn btn-save btn_font">Save changes</button>
				</div>
			</div>
		</div>
	</div>

	<div class="window_bar">Quick share</div>
	<div class="window_content" style="margin-bottom:20px;">
		<div style="margin:20px;">
			<div id="open_quick_wrapper" style="display:<?php echo ($private_count == 0) ? 'none' : 'flex'; ?>; justify-content:space-between;">
				<div style="width:750px;">
					Click "Share now" to share the <span id="private_count"><?php echo number_format($private_count); ?></span> questions which you have already created but not yet shared to the open question bank. You can make detailed changes to your sharing policies using the "Share folders" panel below.
				</div>
				<div>
					<button type="button" id="open_quick" class="btn btn-save btn_font">Share now</button>
				</div>
			</div>
			<div id="open_empty_wrapper" style="display:<?php echo ($private_count == 0) ? 'block' : 'none'; ?>; font-style:italic;">
				All questions you have already created have been shared. To share other questions in your folders and communities, use the "Share folders" panel below.
			</div>
		</div>
	</div>

	<div class="window_bar">Share folders</div>
	<div class="window_content" style="margin-bottom:20px;">
		<form id="open_form">
			<div style="margin:20px;">
				<div style="margin-bottom:20px;">
					Select a folder on the left, choose Open or Private below, then click "Update questions" to apply the new policy to questions in the selected folder and its sub-folders.
				</div>
				<div style="display:flex; margin-bottom:10px; display:flex; justify-content:space-between;">
					<div style="width:300px;">
						<div class="sub_panel_title">Folders</div>
						<div class="sub_panel_content" style="background:white; height:200px; box-sizing:border-box;">
							<div class="folderTree" data-prefix="tree_share_" id="folderPanel" style="box-sizing:border-box; padding:10px; height:100%; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px">
							</div>
						</div>
					</div>
					<div style="width:545px;">
						<div class="sub_panel_title">Questions <span id="questions_count"></span></div>
						<div class="sub_panel_content" id="names_wrapper" style="background:white; height:200px; box-sizing:border-box; overflow-y:scroll; cursor:default;">
							<table id="folder_questions" class="index_table">
								<tr class='disabled'><td style='border:none;'><i>Select a folder to begin</i></td></tr>
							</table>
						</div>
					</div>
				</div>
				<div>
					<div class="labeled-input" style="width:auto;">
						<label style="padding-right:3px;">Set questions in selected folder to</label>
						<select class="userEditSelectBox" id="new_policy" style="display:inline-block; width:80px; font-size:13px;">
							<option value="open">Open</option>
							<option value="private">Private</option>
						</select>
						<div id="open_note" style="display:none; font-size:13px; color:#494949; margin-left:20px;">(Note: Questions that were made open by another user will be skipped.)</div>
					</div>
				</div>
				<div style="display:flex; justify-content:space-between;">
					<div>
						<div class="labeled-input" style="margin-bottom:0px;">
							<input type="checkbox" class="normal_chkbx" id="created_only" /><label for="created_only">Include only questions I created</label>
						</div>
					</div>
					<div style="align-self:flex-end;">
						<button type="button" class="btn btn-save btn_font" id="update_open_bank">Update questions</button>
					</div>
				</div>
			</div>
		</form>
	</div>

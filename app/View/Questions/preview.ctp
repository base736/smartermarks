<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type='text/javascript'>	
    var questionData = <?php echo json_encode($questionData); ?>;
</script>

<script type="module" src="/js/questions/preview.js?7.0"></script>

<div class="rounded_white_wrapper" style="width:900px; margin:50px auto; padding-top:20px; background:white;">
	<div class="rounded_white_wrapper_inner">
		<div id="question_preview_wrapper" class="assessment_wrapper template_view">
			<div class="question_wrapper"></div>
		</div>
	</div>
</div>

<div id="answers_wrapper" class="rounded_white_wrapper" style="width:900px; margin:50px auto 0px; background:white; display:none;">
	<div class="rounded_white_wrapper_inner">
		<h3>Scored responses</h3>
		<div id="answers_table">
			<div style="border-bottom:1px solid black; margin-bottom:5px; width:auto;"><div class='answer_preview_number'>Number</div><div class='answer_preview_value'>Value</div><div class='answer_preview_answer'>Answer</div></div>
		</div>
	</div>
</div>

<div id="variables_wrapper" class="rounded_white_wrapper" style="width:900px; margin:50px auto 0px; background:white; display:none;">
	<div class="rounded_white_wrapper_inner">
		<h3>Constants and Variables</h3>

        <div id="variable_error_wrapper" style="display:none;">
            <div style="width:100%; display:grid; grid-template-columns: auto 1fr; grid-column-gap:20px;">
                <div style="padding-right:0px; justify-self:center;">
                    <img style="height:50px;" src="/img/error.svg" />
                </div>
                <div>
                    <p>Errors were encountered while building test shuffles:</p>
                    <ul id="variable_error_list"></ul>
                </div>
            </div>
        </div>

        <div id="variables_table" class="variable_values_wrapper">
			<div>Name</div>
			<div>Value</div>
			<div style="width:10px;"></div>
            <div style="grid-column-start:4; grid-column-end:5;">Estimated range</div>
			<div style="height:0px; grid-column-start:1; grid-column-end:5; border-bottom:1px solid black;"></div>
		</div>
	</div>
</div>
<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width:100%; background:rgb(255, 255, 255); background:rgba(255, 255, 255, 0.9);">
	<div class="rounded_white_inner" style="position:relative; top:40px; height:495px; margin:40px; padding:0px 20px; overflow:scroll;">
		<div class="policyText">
			<h3>User agreement</h3>			
			<?php echo $this->element('agreement_text'); ?>	
		</div>
	</div>
</div>

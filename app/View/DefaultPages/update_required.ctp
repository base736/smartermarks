<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 600px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>Browser update required</h3>

		<p>In order to offer the functionality we do, SmarterMarks requires a browser that supports ES6. This includes
		all major browsers released in the last 5 years. Please update your browser to the latest version, then try
		accessing the site again.</p>
		
		<p>If you believe you have received this message in error, please 
        contact us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>
	</div>
</div>

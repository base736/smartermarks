<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
    var userID = <?php echo $user_id; ?>;

    $(document).ready(function() {

        $('#resend_verification').on('click', function(e) {
            e.preventDefault();

            fetch('/Users/resend_verification/' + userID, {
                method: 'POST'
            }).then(async response => {
                const responseData = await response.json();

                if (responseData.success) {

                    $.gritter.add({
                        title: "Success",
                        text: "Verification email sent.",
                        image: "/img/success.svg"
                    });

                } else {

                    $.gritter.add({
                        title: "Error",
                        text: "Account already verified.",
                        image: "/img/error.svg"
                    });

                }
            }).catch(function() {
                showSubmitError();
            });
        });
    });
</script>

<div class="rounded_white_wrapper" style="width: 500px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>Awaiting Email Validation</h3>

		<p>A confirmation email has been sent to <?php echo $user_email; ?>.  Please click on the link
		there to confirm your email address.  If you are unable to find the confirmation email, confirm the email
		address above and <a id="resend_verification" href="#">click here</a> 
		to have another email sent.</p>
		
	</div>
</div>

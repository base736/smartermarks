<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type='text/javascript'>
    $(document).ready(function() {
        $('.role_button').on('click', function() {
            role = $(this).attr('data-role');
            Cookies.set('login_role', role, { expires: 180 });
            window.location.href = "/" + role;
        });
    });
</script>

<div style="margin-top:-20px;">

    <div style="float:right; width:350px;"><img src="https://images.smartermarks.com/logo.svg" /></div>
    <div style="clear:both;"></div>

    <div style="display:flex; margin-top:10px;">
        <div style="flex:none; width:240px; margin-right:40px;">
            <div class="home_wrapper">
                <div style="margin:15px 0px;">
                    <div style="margin-bottom:20px; font-family:sans-serif; font-size:16px; font-weight:lighter;">Welcome to SmarterMarks! To get started, choose a role below:</div>
                    <button style="display:block; margin-bottom:10px; width:100%;" class="btn btn-primary role_button" data-role="teacher">Teacher</button>
                    <button style="display:block; margin-bottom:10px; width:100%;" class="btn btn-primary role_button" data-role="student">Student</button>
                </div>
            </div>
        </div>

        <div style="flex:1 1 0; margin-top:35px; font-size:16px;">
            <?php echo $this->element('about'); ?>
        </div>
    </div>
</div>

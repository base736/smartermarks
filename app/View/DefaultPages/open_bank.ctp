<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width:100%; height:auto; background:rgb(255, 255, 255); background:rgba(255, 255, 255, 0.9);">
	<div class="rounded_white_inner" style="height:auto; padding:40px 60px;">
		<div class="policyText">
			
			<h2>Sharing to the open question bank</h2>
			
            <ol>
                <li><a href="#introduction">Introduction</a></li>
                <li><a href="#security">Why sharing is safe</a></li>
                <li><a href="#helping_others">How sharing helps other teachers</a></li>
                <li><a href="#helping_contributors">How sharing helps contributors</a></li>
                <li><a href="#sharing">Ready to share?</a></li>
            </ol>

            <h4 id="introduction">Introduction</h4>

			<p>The open question bank is a collection of <?php echo number_format($numOpen); ?> assessment items covering a wide range
            of subject areas and levels, created and shared by teachers and available to all SmarterMarks users. The bank uses a powerful 
            search engine to help teachers find items that match their subject and outcomes, and give all students the best opportunity
            to demonstrate their knowledge.</p>

            <h4 id="security">Why sharing is safe</h4>

           	<p>Assessment security is at the core of the open question bank. In fact, this is we have an open question bank but no open 
            assessment bank. We believe that while assessments cannot be shared securely, questions can. When the open question bank
            is opened, there is no "default list" of items &mdash; rather, a search must be entered to get results:</p>

            <img src="/img/open_bank/no_search.png" />

            <p>Once a search term has been entered (here we're searching for "globalization"), the results are sorted by relevance
            and performance in the classroom, and do not include information like the author's name, date of creation, or any connection 
            to the assessments they have been used in:</p>

            <img src="/img/open_bank/search_results.png" />

            <p>The goal is that teachers creating assessments using the open bank will be using those questions in new combinations,
            safeguarding assessment security and at the same time building more robust statistics.</p>

            <p>In addition, questions shared to the open question bank are not modified when others borrow them and make changes. Instead, a 
            modifiable copy is made and also shared to the open question bank. Rather than modifying the original, changes to a question make
            the open question bank larger, and help all teachers explore what item styles work best.</p>

            <h4 id="helping_others">How sharing helps other teachers</h4>

			<p>The benefit of sharing to other teachers is probably clear. The open question bank allows new teachers to create assessments
            that give students the best possible opportunity to demonstrate their knowledge, without having to rely on untested items to
            start. For experienced teachers, the open question bank can help with quickly updating an assessment each semester, and when
            building new items is a valuable source of information on what is working in other classrooms.</p>

            <p>The open question bank does this by leading with item statitics, as shown below.</p>

            <img src="/img/open_bank/preview_statistics.png" />

            <p>Item statistics can be used to determine how difficult students are finding a question. Separately, they can also be used
            to determine whether students understand the question &mdash; that is, whether they are being given an opportunity to demonstrate
            what they know. You can learn more about item statistics in <a href="https://youtu.be/LkMErsoLAhI" target="_blank">this video</a>.</p>

            <p>In addition, previews highlight any issues with items, as shown below:</p>

            <img src="/img/open_bank/preview_highlighting.png" />

            <p>Highlighting multiple choice alternatives that don't represent common student misunderstandings, for example, helps teachers
            target changes to an item, building items which give students better feedback and which we hope will ultimately wind up at the top
            of future searches.</p>

            <h4 id="helping_contributors">How sharing helps contributors</h4>

			<p>Sharing also helps the teachers who share their items. Item statistics are limited in their power by two factors:</p>

            <ul>
                <li><b>Small sample sizes</b>, since a single teacher might typically use an item with 100-150 students in a school year; and</li>
                <li><b>Confounding factors</b>, since an item might always be used by the same teacher, or as part of the same assessment.</li>
            </ul>

            <p>Sharing items can dramatically improve sample sizes, taking items from tens or hundreds of uses per year to thousands. This
            improves all statistics, but is perhaps most important in building items that test very low or high difficulty outcomes, or 
            multiple choice alternatives that address less common misceptions.</p>

            <p>In addition, sharing items alleviates many common confounding factors. All item statistics are based on assumptions about the
            assessment those items are being used in &mdash; for example, that students aren't able to get the answer from another question
            in the assessment. Averaging statistics over different assessments done in different schools removes the effect of those
            confounding factors, making it easier to diagnose problems with individual items and with the assessments in which they are used.</p>

            <h4 id="sharing">Ready to share?</h4>

            <p>The <a href="/questions/manage_open">Manage Open Questions tool</a> makes it easy to share questions to the open bank, and to 
            change how you share later on. To begin, click on the link above or on "Manage open questions" in the user menu, accessed by 
            clicking on your name at the top-right of any SmarterMarks page. This will open the Manage Open Questions tool:</p>

            <img src="/img/help/manage_open_questions_top.png" />

            <p>To share questions you create in the future to the open question bank by default, or to reverse that change, click the checkbox
            in the "Sharing preferences" panel, then click "Save changes:":</p>

            <img src="/img/help/manage_open_save_changes.png" />

            <p>To share all questions you have already created, click the "Share now" button in the "Quick share" panel:</p>

            <img src="/img/help/manage_open_share_now.png" />

            <p>You can find more information on sharing using the open question bank by <a href="/help/view/open_question_bank">clicking on 
            this link</a>.</p>

		</div>
	</div>
</div>

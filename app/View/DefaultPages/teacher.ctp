<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

    $(document).ready(function() {

        var complete_signup = function() {
            var $dialog = $(this);
            var $form = $dialog.find('form');

            if ($form.validationEngine('validate')) {
                if (pendingAJAXPost) return false;
                showSubmitProgress($dialog);
                pendingAJAXPost = true;

                fetch('/Users/signup', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        name: $('#create_name').val(),
                        school: $('#create_school').val(),
                        email: $('#create_email').val(),
                        timezone: dayjs.tz.guess()
                    })
                }).then(async response => {
                    pendingAJAXPost = false;
                    const responseData = await response.json();

                    hideSubmitProgress($dialog);

                    if (responseData.success) {
                        window.location.href = responseData.redirect;
                    } else if ('isStudent' in responseData) {
                        window.location.href = '/student_create';
                    } else if ('message' in resporesponseDatanse) {
                        $('#create_email').validationEngine('showPrompt', responseData.message, 'error', 'bottomLeft', true);
                        if (('showForgot' in responseData) && (responseData.showForgot)) $('#forgot_link').show();
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "Error signing up. Please try again.",
                            image: "/img/error.svg"
                        });
                    }
                    
                }).catch(function() {
                    hideSubmitProgress($dialog);
                });
            }
        }

        $('#signup_dialog').dialog({
            autoOpen: false,
            width: 320,
            modal: true,
            position: {
                my: "center",
                at: "center",
                of: window
            }, buttons: {
                'Cancel': function() {
                    $(this).dialog("close");
                },
                'Sign up': complete_signup
            }, open: function(event) {
                var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
                $buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
                $buttonPane.find('button:contains("Sign up")').addClass('btn btn-save btn_font');
                $buttonPane.find('button').blur();
                initSubmitProgress($(this));
            }, close: function() {
                $('form').validationEngine('hideAll');
                hideSubmitProgress($(this));
            }
        });	
        
        $('#signup_button').on('click', function() {
            $('#signup_dialog').dialog('open');
        });

        $('.change_role_link').on('click', function(e) {
            e.preventDefault();
            Cookies.remove('login_role');
            window.location.href = '/home';
        });

        $('.login_submit').on('click', function() {
            if ($('.remember_me').prop('checked')) {
                if (typeof(Cookies.get('remember_me_cookie')) == 'undefined') {
                    Cookies.set('remember_me_cookie', $('.login_email').val());
                }
            }
        });

        $('.remember_me').on('click', function() {
            if (!$(this).prop('checked')) Cookies.remove('remember_me_cookie');
        });

        var initEmail = Cookies.get('remember_me_cookie');
        if (typeof(initEmail) != 'undefined') {
            $('.remember_me').prop('checked', true);
            $('.login_email').val(initEmail).trigger('refresh');
            $('.login_password').focus();
        } else {
            $('.remember_me').prop('checked', false);
        }
        
        $('form').validationEngine({
            promptPosition : "bottomLeft"
        });
        
        $('.login_submit').on('click', function() {
            showSubmitProgress($('.login_button_wrapper'));

            start_login(function(response) {
                window.location.href = '/';
            }, null);
        });
    });
    
</script>

<div style="margin-top:-20px;">

    <div style="float:right; width:350px;"><img src="https://images.smartermarks.com/logo.svg" /></div>
    <div style="clear:both;"></div>

    <div style="display:flex; margin-top:10px;">
        <div style="flex:none; width:240px; margin-right:40px; display:flex; flex-direction:column; align-items:center;">
            <div class="login_wrapper" style="position:relative;">
                <div style="z-index:1; position:absolute; top:-13px; left:50px; width:140px; height:auto; text-align:center; font-family:arial, sans-serif; font-size:24px; font-weight:bold; transform: scale(.9, 1); color:#0054cc">
                    TEACHERS
                </div>
                <svg style="z-index:0; position:relative; overflow:visible; position:absolute; top:0px; left:0px; width:100%; height:100%;" preserveAspectRatio="none" viewBox="0 0 240 391">
                    <path fill="#EDEDE699" stroke="#4F4E4AFF" stroke-width="4" vector-effect="non-scaling-stroke"
                        d = "m 190,0 
                            A 50,50 0,0,1 240,50
                            V 341
                            A 50,50 0,0,1 190,391
                            H 50
                            A 50,50 0,0,1 0,341
                            V 50
                            A 50,50 0,0,1 50,0" />
                </svg>
                <div style="z-index:1; position:relative; width:100%; height:100%;">
                    <div style="margin-top:15px; margin-bottom:10px; line-height:1.3; font-family:sans-serif; font-size:16px; font-weight:lighter;">
                        Already have an account? Sign in below!
                    </div>

                    <div class="login_field">
                        <div class="label_wrapper"><label for="email_teacher">Email</label></div>
                        <input class="login_email" name="email" type="email" value=""/>
                    </div>

                    <div style="display:flex; justify-content:space-between; align-items:center;">
                        <div class="custom-buttonpane login_button_wrapper" style="flex:none;">
                            <button type="button" class="login_submit btn btn-primary">Log in</button>
                            <div class='wait_icon' style="margin:0px; display:none;"></div>
                        </div>
                        <div style="flex:none; display:flex; flex-direction:column; align-items:flex-end;">
                            <div style="flex:none;">
                                <input type="checkbox" id="remember_me_teacher" class="remember_me" style="margin:0px;" value="1" />
                                <label for="remember_me_teacher" style="display:inline-block; margin:0px; font-family:'Times New Roman', Georgia, serif; font-size:9pt;">Remember me</label>
                            </div>
                            <div style="flex:none;">
                                <a href="PasswordResets/send_reset" style="font-family:'Times New Roman', Georgia, serif; font-size:9pt;">Forgot my password</a>
                            </div>
                        </div>
                    </div>

                    <div style="width:100%; margin:25px 0px; display:flex; align-items:center;">
                        <div style="flex:1 1 100%; border-top:4px solid #4F4E4A;"></div>
                        <div style="flex:none; margin:0px 5px; font-weight:bold; font-size:18px;">OR</div>
                        <div style="flex:1 1 100%; border-top:4px solid #4F4E4A;"></div>
                    </div>

                    <div style="margin-top:10px; margin-bottom:15px; line-height:1.3; font-family:sans-serif; font-size:16px; font-weight:lighter;">
                        Sign up for an account now and try SmarterMarks for six months free:
                    </div>

                    <button type="button" id="signup_button" class="btn btn-primary create_account" style="margin-bottom:15px;">Create an account</button>
                </div>
            </div>
            <div style="font-size:12px; margin-top:10px;">
                Not a teacher? <a class='change_role_link' href='#'>Click here</a> to change roles.
            </div>
        </div>
        <div style="flex:1 1 0; margin-top:35px; font-size:16px;">
            <?php echo $this->element('about'); ?>
        </div>
    </div>
</div>

<!-- Dialog boxes-->

<div class="hidden">

<!-- Signup dialog -->

	<div id="signup_dialog" class="dialogbox" title="Create an account">
		<div class="dialog_wrapper">
            <form id="signup_form" style="margin-bottom:0px;" accept-charset="utf-8">
                <div class="labeled-input">
                    <label for="create_name">Your Name</label>
                    <input id="create_name" class="validate[required[onlyNameCharacters]] defaultTextBox2" maxlength="50" type="text" />
                </div>					
                <div class="labeled-input">
                    <label for="create_school">Your School Name</label>
                    <input id="create_school" class="validate[required] defaultTextBox2" maxlength="255" type="text" />
                </div>				
                <div class="labeled-input required">
                    <label for="create_email">Your Email Address At School</label>
                    <input id="create_email" class="validate[required,custom[email] defaultTextBox2" maxlength="100" type="email" required="required"/>
                </div>

                <a id="forgot_link" href="PasswordResets/send_reset" style="display:none; color:#0088cc; font-family:Arial, Helvetica, sans-serif; font-size:13px;">Forgot my Password</a>

                <div style="clear:both;"></div>
            </form>
        </div>
	</div>
</div>

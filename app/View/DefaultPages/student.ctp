<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

    $(document).ready(function() {

        var do_join = function() {
            var join_code = $('#join_code').val();
            if (join_code.length == 5) {
                window.location.href = "/Sittings/join/" + join_code;
            } else {
                $('#join_code').closest('.login_field').validationEngine('showPrompt', '* Enter a valid 5-character code', 'error', 'bottomLeft', true);
            }
        }

        $('#join_button').on('click', do_join);

        $('#join_code').on('keypress', function(e) {
            if (!e) e = window.event;
            var keyCode = e.keyCode || e.which;
            if (keyCode == 13) do_join();
        });

        $('.change_role_link').on('click', function(e) {
            e.preventDefault();
            Cookies.remove('login_role');
            window.location.href = '/home';
        });

        if (typeof(Cookies.get('login_role')) == 'undefined') {
            $('.change_role_link').closest('div').hide();
        }

        $('.login_submit').on('click', function() {
            if ($('.remember_me').prop('checked')) {
                if (typeof(Cookies.get('remember_me_cookie')) == 'undefined') {
                    Cookies.set('remember_me_cookie', $('.login_email').val());
                }
            }
        });

        $('.remember_me').on('click', function() {
            if (!$(this).prop('checked')) Cookies.remove('remember_me_cookie');
        });

        var initEmail = Cookies.get('remember_me_cookie');
        if (typeof(initEmail) != 'undefined') {
            $('.remember_me').prop('checked', true);
            $('.login_email').val(initEmail).trigger('refresh');
            $('.login_password').focus();
        } else {
            $('.remember_me').prop('checked', false);
        }
        
        $('.login_submit').on('click', function() {
            showSubmitProgress($('.login_button_wrapper'));

            start_login(function(response) {
                window.location.href = '/';
            }, function(response) {
                if (('action' in response) && (response.action == 'verify')) {
                    window.location.href = '/student_validate';
                } else {
                    $('.login_password').val('');
                    $('#localLogin_dialog').dialog('close');

                    $('.login_email').val('').focus();
                }
            });
        });
    });
    
</script>

<div style="margin-top:-20px;">

    <div style="float:right; width:350px;"><img src="https://images.smartermarks.com/logo.svg" /></div>
    <div style="clear:both;"></div>

    <div style="display:flex; margin-top:10px;">
        <div style="flex:none; width:240px; margin-right:40px; display:flex; flex-direction:column; align-items:center;">
            <div class="login_wrapper" style="position:relative;">
                <div style="z-index:1; position:absolute; top:-13px; left:50px; width:140px; height:auto; text-align:center; font-family:arial, sans-serif; font-size:24px; font-weight:bold; transform: scale(.9, 1); color:#0054cc">
                    STUDENTS
                </div>
                <svg style="z-index:0; position:relative; overflow:visible; position:absolute; top:0px; left:0px; width:100%; height:100%;" preserveAspectRatio="none" viewBox="0 0 240 391">
                    <path fill="#EDEDE699" stroke="#4F4E4AFF" stroke-width="4" vector-effect="non-scaling-stroke"
                        d = "m 190,0 
                            A 50,50 0,0,1 240,50
                            V 341
                            A 50,50 0,0,1 190,391
                            H 50
                            A 50,50 0,0,1 0,341
                            V 50
                            A 50,50 0,0,1 50,0" />
                </svg>
                <div style="z-index:1; position:relative; width:100%; height:100%;">
                    <div style="margin-top:15px; margin-bottom:10px; line-height:1.3; font-family:sans-serif; font-size:16px; font-weight:lighter;">
                        Sign in using an account your teacher has created:
                    </div>

                    <div class="login_field">
                        <div class="label_wrapper"><label for="email_student">Email</label></div>
                        <input class="login_email" name="email" type="email" value=""/>
                    </div>

                    <div style="display:flex; justify-content:space-between; align-items:center;">
                        <div class="custom-buttonpane login_button_wrapper" style="flex:none;">
                            <button type="button" class="login_submit btn btn-primary">Log in</button>
                            <div class='wait_icon' style="margin:0px; display:none;"></div>
                        </div>
                        <div style="flex:none; display:flex; flex-direction:column; align-items:flex-end;">
                            <div style="flex:none;">
                                <input type="checkbox" id="remember_me_student" class="remember_me" style="margin:0px;" value="1" />
                                <label for="remember_me_student" style="display:inline-block; margin:0px; font-family:'Times New Roman', Georgia, serif; font-size:9pt;">Remember me</label>
                            </div>
                            <div style="flex:none;">
                                <a href="PasswordResets/send_reset" style="font-family:'Times New Roman', Georgia, serif; font-size:9pt;">Forgot my password</a>
                            </div>
                        </div>
                    </div>

                    <div style="width:100%; margin:25px 0px; display:flex; align-items:center;">
                        <div style="flex:1 1 100%; border-top:4px solid #4F4E4A;"></div>
                        <div style="flex:none; margin:0px 5px; font-weight:bold; font-size:18px;">OR</div>
                        <div style="flex:1 1 100%; border-top:4px solid #4F4E4A;"></div>
                    </div>

                    <div style="margin-top:10px; margin-bottom:15px; line-height:1.3; font-family:sans-serif; font-size:16px; font-weight:lighter;">
                    Access an assessment using a code your teacher has shared:
                    </div>

                    <div style="margin-bottom:15px; display:flex; align-items:center;">
                        <div class="login_field" style="flex:1 1 100%;">
                            <div class="label_wrapper"><label for="join_code">Code</label></div>
                            <input autocomplete="off" type="text" maxlength="5" id="join_code" style="margin-bottom:0px;" />
                        </div>
                        <div style="flex:none; margin-left:10px;">
                            <button type="button" id="join_button" class="btn btn-primary">Join</button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="font-size:12px; margin-top:10px;">
                Not a student? <a class='change_role_link' href='#'>Click here</a> to change roles.
            </div>
        </div>
        <div style="flex:1 1 0; margin-top:35px; font-size:16px;">
            <?php echo $this->element('about'); ?>
        </div>
    </div>
</div>

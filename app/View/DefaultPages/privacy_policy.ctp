<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width:100%; height:575px; background:rgb(255, 255, 255); background:rgba(255, 255, 255, 0.9);">
	<div class="rounded_white_inner" style="position:relative; top:40px; height:495px; margin:40px; padding:0px 20px; overflow:scroll;">
		<div class="policyText">
			
			<h3>Personal Information Protection Policy</h3>
			
			<p>SmarterMarks is committed to safeguarding the personal information entrusted to us by our users.  We manage your personal information in accordance with Alberta’s Personal Information Protecation Act, Freedom of Information and Protection of Privacy (FOIP) Act, and other applicable laws.  This policy outlines the principles and practices we follow in protecting your personal information.</p>
			
			<h4>Consent</h4>
			
			<p>We ask for consent to collect, use or disclose users’ personal information.  We may collect, use or disclose users’ personal information without consent only as authorized or required  by law. For example, we may not request consent when the collection, use or disclosure is reasonable for an investigation or legal proceeding, or to collect a debt owed to our organization.</p>
			
			<p>A user may withdraw consent to the use and disclosure of personal information at any time, unless the personal information is necessary for us to fulfil our legal obligations.  We will respect your decision, but we may not be able to provide you with certain services if we do not have the necessary personal information.</p>
			
			<h4>What personal information do we collect?</h4>
			
			<p>We collect only the personal information that we need for the purposes of providing services to our users, such as a user’s name, email address, school name, and billing information.</p>
			
			<p>We inform our users, before or at the time of collecting personal information, of the purposes for which we are collecting the information. We may not provide this notification when a user volunteers information for an obvious purpose, for example, when providing registration details. If you complete an online form and submit it to us through our web site, we use that information for the purposes described on the form.</p>
			
			<p>In addition, when you visit SmarterMarks, our server automatically collects a limited amount of standard information essential to the operation, security and evaluation of the web site. This information includes:</p>
			
			<ul>
			<li>the IP address from which you access SmarterMarks (an IP address is a number that is automatically assigned to your computer whenever you are using the internet);</li>
			<li>the type and version of browser used to access our site; and</li>
			<li>the date and time you access our site</li>
			</ul>
			
			<p>This information is collected in compliance with the FOIP Act. The information is primarily used to improve the security of our accounts and assessments, and as detailed below, is not disclosed to third parties.</p>
			
			<p>Students' personal information is collected only where you have chosen to include it in completed forms. All identifying information, including student names, ids, and any information provided in class lists, can be removed entirely from our servers and backups by deleting the associated score.</p>
			
			<h4>Cookies</h4>
			
			<p>Non-personal information and data may be automatically collected through the standard operation of our servers or through the use of cookies.  Cookies are small files a web site can use to recognize repeat users, making access to and use of SmarterMarks easier, and improving assessment security.  If you do not want information collected through the use of cookies, most browsers will allow you to disable them, though cookies may be necessary to provide you with certain features available from SmarterMarks.</p>
			
			<h4>How do we use personal information?</h4>
			
			<p>SmarterMarks does not sell or otherwise distribute any information collected to third parties.  We use and disclose user personal information only for the purposes for which the information was collected, except as authorized by law.  For example:</p>
			
			<ul>
			<li>A user’s name and email addresses may be used to inform that user of changes to their account or to our policies;</li>
			<li>Information about access to this website is used to protect users’ security and for adminsitration of this site; and</li>
			<li>Financial information is used to collect payments.</li>
			</ul>
			
			<p>Student information is never required.  Where present (for example, if a user chooses to collect ID numbers as part of a response form), it is used only to assist in the organization of results.</p>
			
			<p>If we wish to use or disclose your personal information for any new business purpose, we will ask for your consent.</p>
			
			<h4>How do we safeguard personal information?</h4>
			
			<p>SmarterMarks takes the security of your information and that of your students very seriously.</p>
			
			<p>We retain user personal information only as long as is reasonable to fulfil the purposes for which the information was collected or for legal or business purposes.  For example, uploaded student assessments are retained on our servers only long enough to complete our analysis, and are permanently deleted immediately once reports have been generated.  The reports generated by SmarterMarks contain identifying information, such as student ID numbers, only if the user has explicitly chosen to include that information, and by default do not include any identifying information.</p>
			
			<p>SmarterMarks will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of the personal information we retain. We will notify the Office of the Information and Privacy Commissioner of Alberta, without delay, of a security breach affecting personal information if it creates a real risk of significant harm to individuals.</p>
			
			<h4>Access to records containing personal information</h4>
			
			<p>We make every reasonable effort to ensure that user information is accurate and complete.  We ask that users update their account information if there is a change in that information that may affect their relationship to SmarterMarks, or if there is an error in our information.</p>
			
			<p>Users of SmarterMarks have a right of access to their own personal information in a record that is in our custody or under our control, subject to some exceptions.  If we refuse a request in whole or in part, we will provide the reasons for the refusal. In some cases where exceptions to access apply, we may withhold that information and provide you with the remainder of the record.</p>
			
			<p>You may make a request for access to your personal information by contacting us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.  You must provide sufficient information in your request to allow us to identify the information you are seeking.  You may also request information about our use of your personal information and any disclosure of that information to persons outside our organization.</p>
			
			<p>We will respond to your request within 45 calendar days, unless an extension is granted.  We may charge a reasonable fee to provide information, but not to make a correction.  We will advise you of any fees that may apply before beginning to process your request.</p>
			
			<h4>Questions and complaints</h4>
			
			<p>If you have a question or concern about any collection, use or disclosure of personal information by SmarterMarks, or about a request for access to your own personal information, please contact us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a> or call us at (403)809-9754.</p>
			
			<p>If you are not satisfied with the response you receive, you should contact the Information and Privacy Commissioner of Alberta:</p>
			
			<br/>
			<table style="border:none;">
				<tr><td colspan="2">Office of the Information and Privacy Commissioner of Alberta</td></tr>
				<tr><td colspan="2">Suite 2460, 801 - 6 Avenue, SW</td></tr>
				<tr><td colspan="2">Calgary, Alberta T2P 3W2</td></tr>
				<tr><td>Phone: 403-297-2728</td><td>Toll Free: 1-888-878-4044</td></tr>
				<tr><td>E-mail: <a href="mailto:generalinfo@oipc.ab.ca">generalinfo@oipc.ab.ca</a></td><td>Website: <a href="http://www.oipc.ab.ca">www.oipc.ab.ca</a></td></tr>
			</table>
			
		</div>
	</div>
</div>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>

const data = <?php echo json_encode($data); ?>;

</script>

<script type="module" src="/js/build_pdf/build_pdf.js?7.0"></script>

<div class="rounded_white_wrapper" style="width:600px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner">

		<h3>Building PDF</h3>

		<p>Your document is being generated.  When the processing is complete, you will be 
		automatically forwarded to the PDF.</p>
		
		<br/>

		<div id="progressbar"></div>
		<div id="progresstext" style="text-align:left; width:500px;">Building PDF...</div>

	</div>
</div>

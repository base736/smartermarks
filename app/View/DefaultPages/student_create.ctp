<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 700px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>Student accounts on SmarterMarks</h3>

		<p>It looks like you're trying to create a student account.</p>
		
		<p>If you're a teacher, please use your regular staff email when creating a SmarterMarks account.
		If you're a student, thanks for your interest in SmarterMarks! Student accounts are created 
		automatically when students are added to their first SmarterMarks assessment. If you're having 
		trouble logging in, try the following:</p>

		<ol>
		<li>Check that your teacher has added you to your assessment using your correct school email address. 
		A welcome email was sent when you were added to your first assessment that contains a link you must
		click to verify your email address. An email will not be sent for assessments after the first.</li>
		<li>If you have not verified your email address with us and are expecting a welcome email, check your
		spam folder for an email with the subject "Welcome to SmarterMarks!".</li>
		<li>If you have checked your spam folder and still can't find a welcome email, or if you have
		forgotten your password, <a href="/PasswordResets/send_reset">click this link</a> or click on the 
		"Forgot my password" link below the login prompts. Fill in your school email and click "Submit", and
		we'll send you an email containing a link you can click to get started.</li>
		<li>If none of these works, contact your teacher. Your teacher can give you a shareable link to allow
		you to access the assessment. You may not be able to keep track of your results and feedback, but
		you'll be able to do your assessment today and sort out your account later.</li>
		</ol>

		<div style="float:right;">
			<a href="/"><button class='btn btn-primary'>Back to login</button></a>
		</div>
		<div style="clear:both;"></div>

	</div>
</div>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 500px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>SmarterMarks is undergoing maintenance</h3>

		<p>Sorry for the inconvenience! SmarterMarks will be down briefly this evening as we do a quick update. Please 
        contact us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a> if you have any urgent concerns.</p>
		
		<p>Thank you for your patience.</p>
		
	</div>
</div>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 700px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>JavaScript required</h3>

		<p>It looks like your browser has JavaScript disabled. As a web application, SmarterMarks 
		requires JavaScript in order to function. Please enable JavaScript, then click the button below
		to continue.</p>

		<div style="float:right;">
			<a href="/"><button class='btn btn-primary'>Continue</button></a>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

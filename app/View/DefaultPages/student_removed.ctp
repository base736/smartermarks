<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 600px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>Sitting is unavailable</h3>

		<p>You no longer have access to this assessment. This could be because it has been deleted, or because
		you have been removed from the sitting. If you believe this is an error, contact your teacher for 
		more information.</p>

		<?php 
		if (isset($user['id'])) {
		?>

		<div style="float:right;">
			<a href="/"><button class='btn btn-primary'>Back to index</button></a>
		</div>
		<div style="clear:both;"></div>

		<?php
		}
		?>

	</div>
</div>

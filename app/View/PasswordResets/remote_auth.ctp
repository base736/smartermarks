<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 500px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>Password must be set externally</h3>

		<p>Authentication for this account is done through your school board. 
			
		<?php 
		if ($reset_url !== false) {
		?>

		<a href="<?php echo $reset_url; ?>">Click here</a> to reset your password using your board's tools &mdash;
	
		<?php
		} else {
		?>

		Your board's account management tools must be used to reset your password &mdash;

		<?php
		}
		?>

		once your account has been updated there, you should be able to use your new password on SmarterMarks.</p>
	</div>
</div>

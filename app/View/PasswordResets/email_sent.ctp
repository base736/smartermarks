<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 500px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>Password reset email sent</h3>

		<p>An email has been sent to <?php echo $email; ?> with instructions on how to reset your 
		password. If you are unable to find the email at this address, 
		<a href="https://smartermarks.com/PasswordResets/send_reset">click here</a> to send another.</p>
		
	</div>
</div>

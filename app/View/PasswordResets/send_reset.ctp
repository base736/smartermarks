<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 400px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>Forgot your password?</h3>

		<p>Enter your email address below and we'll send you instructions on how to reset your password.</p>
	 
		<form action="/PasswordResets/send_reset" class="formular" method="post" accept-charset="utf-8" novalidate>
			<div class="labeled_input">
				<label for="PasswordResetEmail">Email Address:</label>
				<input name="data[PasswordReset][email]" class="validate[required, custom[email]] defaultTextBox2" style="box-sizing:border-box; width:100%; height:28px;" type="email" id="PasswordResetEmail" />
			</div>
			<div>
				<div style="float:right;">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
				<div style="clear:both;"></div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

    $('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : "bottomLeft"
    });

	$('#PasswordResetEmail').focus();
    
</script>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width:435px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3><?php echo $title_text; ?></h3>

		<?php 
		if (($ticketUser['User']['entropy'] < 32) && !empty($ticketUser['User']['password'])) {
		?>

		<div style="margin-bottom:20px;">
			<p>Your password must be updated to meet our security requirements. Please 
			choose a new password below. To make your password more secure, you might consider:</p>

			<ul>
				<li>Choosing a longer password, for example a phrase;</li>
				<li>Including both uppercase and lowercase letters; or</li>
				<li>Including digits or special characters like punctuation.</li>
			</ul>
		</div>

		<?php
		}
		?>

		<form method="post">
			<div style="vertical-align:center; margin-bottom:10px;">
				<label for="password" style="display:inline-block; width:115px; margin:0px;">New Password:</label>
				<input id="password" type="password" name="data[PasswordReset][password]" class="defaultTextBox2 validate[required]" style="margin:0px;" />
			</div>
			<div style="vertical-align:center; margin-bottom:10px;">
				<label for="confirmation" style="display:inline-block; width:115px; margin:0px;">Confirm Password:</label>
				<input id="confirmation" type="password" name="data[PasswordReset][confirm_password]" class="defaultTextBox2 validate[required,equals[password]]" style="margin:0px;" />
			</div>
			<div>
				<div style="float: right;">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
				<div style="clear:both;"></div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

	$('#password').focus();

	$('form').validationEngine({
	 	validationEventTrigger: 'submit',
	 	promptPosition : "bottomLeft"
    });

</script>

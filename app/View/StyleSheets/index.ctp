<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<!-- Main area -->

<script>
	var receivedCount = <?php echo isset($receivedCount) ? $receivedCount : '0'; ?>;
</script>

<script type="module" src="/js/stylesheets/index.js?7.0"></script>

<h2 style='margin-bottom:10px;'>Style Sheets</h2>

<div style="width:890px; margin:auto;">
	<div class="action_bar">
		<div class="action_left">
			<div class="checkbox_arrow"></div>
			<div class="btn-group">
                <button id="editSheet_button" type="button" class="btn btn-dark item_action_one"><span>Edit</span></button>
			</div>

			<div class="btn-group">
				<button id="deleteSheet_button" type="button" class="btn btn-dark item_action_many"><span>Delete</span></button>
			</div>

            <div class="btn-group">
                <button id="share_dropdown" class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
                    Share 
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left">
                    <li><a href="#" id="sendCopy_button" class="item_action_many needs_copy">Send a copy</a></li>
                </ul>
            </div>

        </div>
	</div>
	<div class="window_bar" style="margin-top:1px;">
        <div style="width:5px;">&nbsp;</div>
        
        <div style="width:120px;">Created</div>
        <div style="width:120px;">Last Used</div>
        <div style="width:auto;">Name</div>

		<div class="sort_wrapper">
			<div style="vertical-align:middle; display:inline-block; line-height:normal;">
				<div class="header_sort" data-direction="asc" style="display:block;">▲</div>
				<div class="header_sort" data-direction="desc" style="display:block;">▼</div>
			</div>
			<span style="white-space:nowrap;">Sort by</span>
			<select id="sort_type">
                <option value="created" data-default-direction="desc" selected>Created</option>
                <option value="last_used" data-default-direction="desc">Last Used</option>
				<option value="name" data-default-direction="asc">Name</option>
			</select>
		</div>

	</div>
	<div class="window_content">
		<table id="style_sheets" class="index_table selection_table">
            <tbody>
                <tr class="disabled">
                    <td colspan="5">
                        <div style="padding-left:10px;"><div class="wait_icon"></div><i>Loading&hellip;</i></div>
                    </td>
                </tr>
            </tbody>            
        </table>
	</div>
</div>
	
<div class="hidden">

    <!-- Copy received dialog -->

    <div id="receiveCopy_dialog" class="dialogbox" title="Items Received">
		<div class="dialog_wrapper">
			<p id="receiveCopy_text" style="min-height:50px;"></p>
		</div>
	</div>

    <!-- Send copy dialog -->

	<div id="sendCopy_dialog" class="dialogbox" title="Send A Copy">
		<div class="dialog_wrapper">
			<p id="sendCopy_text" style="margin-bottom:20px;"></p>
			
			<form id="sendCopy_form" class="formular" method="post" accept-charset="utf-8">
				<input type="hidden" name="email_list_id" value="send_email_list"/>
				<div class="labeled-input">
					<div style="width:100%; margin-bottom:5px !important;">
						<div style="float:left;"><label style="display:block; text-align:left; margin:3px 0px 0px;">Send to:</label></div>
						<div style="float:right;"><button id="send_selection_type" class="btn-tiny" <?php if (empty($contacts)) echo 'disabled="disabled"' ?> style="font-size:13px;">Choose from list</button></div>
						<div style="clear:both;"></div>
					</div>
					<textarea tabindex="1" name="email_list" id="send_email_list" class="validate[required] defaultTextBox2" style="resize:none; height:100px; width:340px;" type="text"></textarea>
					<div id="send_email_checklist" style="resize:none; height:100px; width:340px; padding:4px 6px; border:1px solid #ccc; border-radius: 4px; background:white; display:none; overflow-y:scroll; overflow-x:hidden;">
						<?php
							if (!empty($contacts)) {
								foreach ($contacts as $contact) {
									echo "<div><input class='send_email_checkbox' type='checkbox' id='send_email_checkbox_" . $contact['id'] . "' data-email='" . $contact['email'] . "' /><label for='send_email_checkbox_" . $contact['id'] . "' style='display:inline-block; width:auto;'>" . $contact['name'] . " (" . $contact['email'] . ")</label></div>";
								}
							}
						?>
					</div>
				</div>
			</form>
		</div>
	</div>
	
    <!-- Delete style sheet dialog -->

    <div id="deleteSheet_dialog" class="dialogbox" title="Delete style sheet">
		<div class="dialog_wrapper" style="min-height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
			<div id="deleteSheet_text"></div>
		</div>
	</div>

    <!-- Edit style sheet dialog -->

	<div id="editSheet_dialog" class="dialogbox" title="Edit style sheet">
        <div class="dialog_wrapper">
            <div style="height:auto; width:100%; display:flex; flex-direction:row; align-items:center;">
                <label style="flex:none; width:auto; margin:0px;">List name:</label>
                <input id="edit_sheet_name" class="validate[required] defaultTextBox2" style="flex:1 1 100%; margin:0px;" type="text" />
            </div>
        </div>
	</div>

</div>
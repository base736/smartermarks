<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="module" src="/js/sittingstudents/index.js?7.0"></script>

<div style="grid-column:right; grid-row:actions" class="action_bar right_half">
	<div class="action_left">
		<div class="checkbox_arrow"></div>
		<div class="index_actions trash_actions">
			<div class="btn-group">							
				<button type="button" class="btn btn-dark item_action_many moveItem_button"><span>Move</span></button>
			</div>
		</div>
		<div class="index_actions default_actions">
			<div class="btn-group">
				<button id="open_button" class="btn btn-dark dropdown-toggle item_action_one" data-toggle="dropdown" href="#">
					Open 
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu text-left">
					<li><a href="#" id="view_open_button">Start a new attempt</a></li>
					<li><a href="#" id="view_closed_button">View results</a></li>
				</ul>
			</div>

			<div class="btn-group">
				<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
					Manage 
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu text-left">
					<li><a href="#" class="<?php echo (!isset($user['role']) || $user['role'] != 'admin') ? "item_action_many needs_delete moveItem_button" : "disabled"; ?>">Move to folder</a></li>
					<li><a href="#" id="deleteItem_button" class="item_action_many needs_delete">Move to trash</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="action_right">
		<div class="btn-group">
			<button class="btn btn-dark search_dropdown dropdown-toggle" href="#">
				Search 
				<span class="caret"></span>
			</button>
			<div class="pull-right search_menu">
				<div id="search_options" style="margin-bottom:5px;">
					<div style="vertical-align:middle; display:inline-block;">Search:</div>
					<div style="vertical-align:middle; display:inline-block;">
						<select id="search_type" style="margin:0px; width:160px;">
							<option value="folder">in this folder</option>
							<option value="subfolders">with sub-folders</option>
							<option value="all">in all folders</option>
						</select>
					</div>
				</div>
				
				<div id="search_wrapper" style="position:relative; width:auto;">
					<input class="search_text" type="text" />
					<div class="search_icon"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="grid-column:right; grid-row:main; display:flex; flex-direction:column;">
	<div class="window_bar right_half" style="flex:none; margin-top:1px;">

        <div style="width:5px;">&nbsp;</div>
        <div style="width:auto; padding-left:20px;">Name and details</div>

		<div class="sort_wrapper">
			<div style="vertical-align:middle; display:inline-block; line-height:normal;">
				<div class="header_sort" data-direction="asc" style="display:block;">▲</div>
				<div class="header_sort" data-direction="desc" style="display:block;">▼</div>
			</div>
			<span style="white-space:nowrap;">Sort by</span>
			<select id="sort_type">
				<option value="Sitting.name" data-default-direction="asc">Name</option>
				<option value="SittingStudent.created" data-default-direction="desc" selected="selected">Created</option>
			</select>
			<span style="margin-left:10px;">Show</span>
			<select id="show_limit">
                <option value="10">10</option>
                <option value="all">all</option>
			</select>
		</div>
	</div>
	<div id="index_right" class="index_body right_half" style="flex:1 1 100%; overflow:hidden; min-height:300px; width:100%;">
		<div class='index_element'>
			<table class="index_table selection_table">
			</table>
			<div id="footer_wrapper" style="display:flex; justify-content:space-between; height:auto; margin:20px 10px;">
				<div id="count_wrapper" style="color:#75797c;"></div>
				<div id="pagination_wrapper" class="pagination_wrapper"></div>
			</div>
		</div>
	</div>
</div>

<div class="hidden">

</div>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var sittingStudent_id = <?php echo $sittingStudent_id; ?>;
	var canEdit = <?php echo $canEdit; ?>;
	var presentation = '<?php echo $presentation; ?>';
	var notificationTimes = <?php echo $warnings; ?>;
</script>

<script type="module" src="/js/sittingstudents/view_open.js?7.0"></script>

<!-- Top bar -->

<div class="action_bar" style="margin-bottom:5px;">
	<div class="action_left" style="color:#ccc;">
		<div id="document_title"></div>
	</div>	
</div>

<!-- Left Panel-->

<div id="panel_wrapper" style="position:relative; width:890px; height:auto;">
	<div id="left_panel" style="width:295px; position:relative; float:left;">

		<!-- {* time remaining *} -->
		<div class="tool_panel" style="margin-bottom:10px;">
		    <div class="window_bar tool_header">
		    	<div style="height:100%; width:100%; padding:0px; display:flex; justify-content:space-between; align-items:center;">
			    	<div>Time remaining</div>
			    	<div>
				    	<button type="button" class="btn btn-mini tool_toggle" style="width:45px;">Hide</button>
				    </div>
			    </div>
		    </div>
		    <div class="window_content tool_wrapper">
				<div id="timer_text" style="padding:10px;">Calculating &hellip;</div>
		    </div>
		</div>
	
		<!-- {* annotation *} -->
		<div class="tool_panel" style="margin-bottom:10px;">
		    <div class="window_bar tool_header">
			<div style="height:100%; width:100%; padding:0px; display:flex; justify-content:space-between; align-items:center;">
			    	<div>Annotation</div>
			    	<div>
				    	<button type="button" class="btn btn-mini tool_toggle" style="width:45px;">Hide</button>
				    </div>
			    </div>
		    </div>

			<div id="annotation_tool_wrapper" class="window_content tool_wrapper" style="display: flex; flex-direction: column; padding:3px;"></div>		

		</div>
	
		<!-- {* sections *} -->
		<div id="navigation_wrapper" style="display:none;">
			<div class="window_bar">Sections</div>
			<div class="window_content" style="padding:5px;">
				<div class="layout_panel">
					<div class="empty_layout" style="width:255px; padding:10px; color:#eee; font-size:13px; font-style:italic; display:none;">No sections built for this assessment. Click "Add Section" below to begin.</div>
					<ul id="sections_menu" class="menu_wrapper"></ul>
				</div>
			</div>
		</div>
		
	</div>
	
	<!-- Right Panel-->
	
	<div style="width:585px; float:right;">
		<div class="window_bar">Assessment view</div>
		<div class="window_content" style="padding:10px; background-color:white;">
			<div id="empty_preview" style="font-style:italic; margin-top:20px;">Loading assessment...</div>
			<div style="float:right; display:none;" class="submit_wrapper custom-buttonpane">
				<?php if ($presentation == 'free') { ?>
					<button class='btn item_previous' style="display:none; vertical-align:middle;"><span style="font-size:80%; margin-right:5px;">◀</span><span>Prev</span></button>
					<button class='btn item_next' style="display:none; vertical-align:middle;"><span>Next</span><span style="font-size:80%; margin-left:5px;">▶</span></button>
				<?php } else if ($presentation == 'next') { ?>
					<button class='btn btn-primary item_next' style="display:none;"><span>Next</span><span style="font-size:80%; margin-left:5px;">▶</span></button>
				<?php } ?>

				<button type="button" class="btn btn-primary leave_assessment" style="margin:0px;">Submit</button>
			</div>
			<div style="clear:both;"></div>
			<div id="assessment_preview_wrapper" class="assessment_wrapper template_view student_view parts_formatted" style="position:relative; margin-top:20px; min-height:95px; opacity:0.0;"></div>
			<div style="float:right; display:none;" class="submit_wrapper custom-buttonpane">
				<?php if ($presentation == 'free') { ?>
					<button class='btn item_previous' style="display:none; vertical-align:middle;"><span style="font-size:80%; margin-right:5px;">◀</span><span>Prev</span></button>
					<button class='btn item_next' style="display:none; vertical-align:middle;"><span>Next</span><span style="font-size:80%; margin-left:5px;">▶</span></button>
				<?php } else if ($presentation == 'next') { ?>
					<button class='btn btn-primary item_next' style="display:none;"><span>Next</span><span style="font-size:80%; margin-left:5px;">▶</span></button>
				<?php } ?>

				<button type="button" class="btn btn-primary leave_assessment" style="margin:0px;">Submit</button>
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>

	<div style="clear:both;"></div>
</div>

<div id="sitting_warnings" style="z-index:9999;" >
	<div id="time_warning" style="display:none;">
		<div style="display:inline-block;"><span id="time_warning_minutes"></span> remaining.</div>
	</div>
	
	<div id="save_success" style="display:none;">
		Saved.
	</div>

	<div id="save_warning" style="display:none;">
		<div style="display:inline-block;">Unable to save changes &mdash; check your internet connection.</div>
		<button id="save_retry" type="button" class="btn btn-small" style="margin-left:20px;">Try again</button>
	</div>
</div>

<!-- Dialog boxes-->

<div class="hidden">

<!-- Written response buffer -->

	<div id="wr_buffer" class="template_view"></div>

<!-- Leave assessment dialog -->

	<div id="leaveAssessment_dialog" class="dialogbox" title="Submit assessment">
		<div class="dialog_wrapper">
			<p id="leaveAssessment_start"></p>
			<p>Are you sure you want to submit this assessment? If you submit the assessment, you will not be able to make changes later.</p>
		</div>
	</div>

<!-- Empty item dialog -->

<div id="emptyItem_dialog" class="dialogbox" title="Question not answered">
		<div class="dialog_wrapper">
			<div style="display:flex; align-items:flex-start;">
				<div style="flex:none; width:auto; margin-right:20px;">
					<img src="/img/warning.svg" style="height:50px;" />
				</div>
				<div style="flex:1 1 100%;">
					<p>You have not answered all parts of this question. For this assessment, you will <b>not</b> be able to return to
					this question later. Click "Cancel" to return to this question now, or "Skip question" to leave it unanswered.</p>
				</div>
			</div>
		</div>
	</div>

<!-- Next item dialog -->

	<div id="nextItem_dialog" class="dialogbox" title="Continuing to the next question">
		<div class="dialog_wrapper">
			<div style="display:flex; align-items:flex-start;">
				<div style="flex:none; width:auto; margin-right:20px;">
					<img src="/img/warning.svg" style="height:50px;" />
				</div>
				<div style="flex:1 1 100%;">
					<p>For this assessment, you will <b>not</b> be able to return to work on earlier questions. 
					Click "Cancel" to return to this question now, or "Continue" to move on.</p>
				</div>
			</div>
		</div>
	</div>

</div>

<?php echo $this->element('html_edit'); ?>

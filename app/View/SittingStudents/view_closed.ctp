<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var sittingStudentID = <?php echo $sittingStudent_id; ?>;
</script>

<script type="module" src="/js/sittingstudents/view_closed.js?7.0"></script>

<!-- Top bar -->

<div class="action_bar" style="margin-bottom:5px;">
	<div class="action_left" style="color:#ccc;">
		<div id="document_title"></div>
	</div>
	
	<div class="action_right" style="align-self:flex-start;">
		<?php
		if (!empty($user['id'])) {
		?>

		<button id="open_index" type="button" class="btn btn-dark" style="margin:5px;">Back to index</button>

		<?php
		} else if ($canStart == 1) {
		?>

		<button id="open_new" type="button" class="btn btn-dark" style="margin:5px;">Start new attempt</button>

		<?php
		}
		?>
	</div>
</div>

<!-- Left Panel-->

<div id="panel_wrapper" style="position:relative; width:890px; height:auto;">
	<div id="left_panel" style="width:295px; position:relative; float:left;">

		<!-- {* attempts *} -->
		<div id="attempts_wrapper">
			<div class="window_bar">Attempts</div>
			<div class="window_content" style="padding:5px;">
				<div class="layout_panel">
					<div class="empty_layout" style="width:255px; padding:10px; color:#eee; font-size:13px; font-style:italic; display:none;">No attempts to show for this assessment.</div>
					<ul id="attempts_menu" class="menu_wrapper"></ul>
				</div>
			</div>
		</div>
		
		<!-- {* navigation *} -->
		<div id="navigation_pane" style="display:none;">
		    <div class="window_bar">Views</div>
		    <div class="window_content" style="padding:5px;">
				<ul id="view_menu" style="padding:5px;" >
					<li class="view_button" data-view-type="results"><a href="#">Your results</a></li>
					<li class="view_button" data-view-type="preview"><a href="#">Assessment view</a></li>
					<li class="view_button" data-view-type="answers"><a href="#">Answer key</a></li>
				</ul>
		    </div>
		</div>
	
		<!-- {* sections *} -->
		<div id="navigation_wrapper" style="margin-top:10px; display:none;">
			<div class="window_bar">Sections</div>
			<div class="window_content" style="padding:5px;">
				<div class="layout_panel">
					<div class="empty_layout" style="width:255px; padding:10px; color:#eee; font-size:13px; font-style:italic; display:none;">No sections built for this assessment. Click "Add Section" below to begin.</div>
					<ul id="sections_menu" class="menu_wrapper"></ul>
				</div>
			</div>
		</div>

	</div>
	
	<!-- Right Panel-->
	
	<div style="width:585px; float:right;">
		<div class="right_panel" id="preview_panel" style="display:none;">
			<div class="window_bar">Assessment view</div>
			<div class="window_content" style="padding:10px; background-color:white;">
	    		<div id="empty_preview" style="font-style:italic; margin-top:20px;">Loading assessment...</div>
				<div id="assessment_preview_wrapper" class="assessment_wrapper template_view student_view parts_formatted" style="margin-top:20px; min-height:95px; opacity:0.0;">
				</div>
			</div>
		</div>
	</div>

	<div style="width:585px; float:right;">
		<div class="right_panel" id="answers_panel" style="display:none;">
			<div class="window_bar">Answer key</div>
			<div class="window_content" style="padding:10px; background-color:white;">
				<div id="student_key_wrapper" class="template_view" style="min-height:95px;">
				</div>
			</div>
		</div>
	</div>

	<div style="width:585px; float:right;">
		<div class="right_panel" id="results_panel">
			<div class="window_bar">Your results</div>
			<div class="window_content" style="padding:10px;">
				<div id="empty_wrapper" class="results_wrapper" style="margin:10px 0px; display:none;">
					<i>No results available for this attempt. Your responses have been saved, but your teacher may not have 
					released results for this assessment yet.</i>
				</div>
				<div id="score_wrapper" class="results_wrapper" style="margin-bottom:20px; display:none;">
					<h3 style="margin:0px;">Score</h3>
					<div id="score_text"></div>
					<div id="wr_warning" style="margin-top:15px; display:none;">Note: Some written response questions have not yet been scored.</div>
				</div>
				<div id="outcomes_wrapper" class="results_wrapper" style="margin-bottom:20px; display:none;">
					<h3 style="margin:0px;">Outcomes</h3>
					<table class="panel_table" style="border:1px double #aaa;">
						<thead>
							<tr>
								<td>Name</td>
								<td width="15%">Score</td>
								<td width="17%" style="text-align:center;">Questions</td>
							</tr>
						</thead>
						<tbody id="outcomesResults"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div style="clear:both;"></div>
</div>

<!-- Dialog boxes-->

<div class="hidden">

</div>

<?php echo $this->element('html_edit'); ?>

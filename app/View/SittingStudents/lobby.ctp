<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
    var sittingStudent_id = <?php echo $sittingStudent_cache['SittingStudent']['id']; ?>;
	var sittingStudent = <?php echo json_encode($sittingStudent_cache); ?>;

	var needsUser = <?php echo (!empty($user['id']) && ($user['role'] == 'student') &&
		empty($sittingStudent_cache['SittingStudent']['user_id'])) ? 'true' : 'false'; ?>;
	var userData = <?php echo empty($user) ? 'null' : json_encode($user); ?>;
	var assessmentStart = Date.now() + <?php echo $waitTime; ?> * 1000.0;
	var canOpen = <?php echo $sittingStudent_cache['SittingStudent']['can_open'] ? 'true' : 'false'; ?>;
	var dataLines = <?php echo json_encode($dataLines); ?>;
	var details = <?php echo json_encode($details); ?>;
	var join_field = <?php echo json_encode($join_field); ?>;
</script>

<script type="module" src="/js/sittingstudents/lobby.js?7.0"></script>

<div class="rounded_white_wrapper" style="width:600px; height:auto; margin:50px auto;">
	<div class="rounded_white_wrapper_inner">

		<h3>Starting a new attempt</h3>
		
		<p>You are starting an attempt for this assessment:</p>

		<div style="margin:10px 20px 20px;">
			<div style="margin-bottom:5px;"><b><?php echo htmlspecialchars($sittingStudent_cache['Sitting']['name']); ?></b></div>
			<div id="sitting_details"></div>
		</div>
				
		<div id="inputs_wrapper" style="display:none;">
			<p>Before you begin, your teacher has asked for the following information:</p>
			<form id="inputs_form" style="margin:10px 20px 20px;"></form>
		</div>
		
		<p id="waitNotice">This assessment will become available in <span id='waitTime'></span>.</p>
		<p id="readyNotice" style="display:none;">Assessment is available now. Click "Start now" to begin.</p>
		
		<div id="save_wrapper" style="display:none;">
			<div class="labeled-input" style="width:auto;">
				<input type="checkbox" class="normal_chkbx" style="margin-right:10px;" id="save_input" /><label id="save_label" for="save_input" style="text-align:left; width:auto; color:#333333; font-size:14px;"></label>
			</div>
		</div>

		<div id="nav_buttons" class="custom-buttonpane" style="float:right;">
			<button id="cancelButton" class="btn btn-cancel btn_font">Cancel</button>
			<button id="startButton" class="btn btn-primary btn_font disabled" style="width:80px;">Waiting&hellip;</button>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<!-- Left Panel-->

<script type="text/javascript">
	$(document).ready(function(){
		$('.submenu').show();
//		var pathArray = window.location.pathname.split('/');
//		$('.submenu[data-parent="' + pathArray[pathArray.length - 1] + '"]').show();
	});
</script>

<div style="display:grid; grid-column-gap: 20px; grid-template-columns:max-content auto; grid-template-rows:auto 900px;">
	<div class="window_bar" style="grid-column:1/1; grid-row:1/1;">Index</div>
	<div class="window_content" style="grid-column:1/1; grid-row:2/2;">
	
		<div class="help_panel" id="indexPanel">
			<ol>
			<li><?php echo $this->Html->link('Introduction', array('controller' => 'help', 'action' => 'view', 'introduction'));?>
				<ol class='submenu' data-parent='introduction'>
				<li><?php echo $this->Html->link('Building a basic assessment', array('controller' => 'help', 'action' => 'view', 'building_a_basic_assessment'));?></li>
				<li><?php echo $this->Html->link('Building a basic response form', array('controller' => 'help', 'action' => 'view', 'building_a_basic_response_form'));?></li>
				</ol>
			</li>
			<li><?php echo $this->Html->link('Building a question', array('controller' => 'help', 'action' => 'view', 'building_a_question'));?>
				<ol class='submenu' data-parent='building_a_question'>
				<li><?php echo $this->Html->link('Question options', array('controller' => 'help', 'action' => 'view', 'question_options'));?></li>
				<li><?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?></li>
				<li><?php echo $this->Html->link('Multiple choice questions', array('controller' => 'help', 'action' => 'view', 'multiple_choice_questions'));?></li>
				<li><?php echo $this->Html->link('Numeric response questions', array('controller' => 'help', 'action' => 'view', 'numeric_response_questions'));?></li>
				<li><?php echo $this->Html->link('Written response questions', array('controller' => 'help', 'action' => 'view', 'written_response_questions'));?></li>
				<li><?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?></li>
				<li><?php echo $this->Html->link('Using variables', array('controller' => 'help', 'action' => 'view', 'using_variables'));?></li>
				<li><?php echo $this->Html->link('Item statistics', array('controller' => 'help', 'action' => 'view', 'item_statistics'));?></li>
				</ol>
			<li><?php echo $this->Html->link('Building an assessment template', array('controller' => 'help', 'action' => 'view', 'building_an_assessment_template'));?>
				<ol class='submenu' data-parent='building_an_assessment_template'>
				<li><?php echo $this->Html->link('Assessment options', array('controller' => 'help', 'action' => 'view', 'assessment_options'));?></li>
				<li><?php echo $this->Html->link('Working with template elements', array('controller' => 'help', 'action' => 'view', 'working_with_template_elements'));?></li>
				<li><?php echo $this->Html->link('Learning outcomes', array('controller' => 'help', 'action' => 'view', 'learning_outcomes'));?></li>
				<li><?php echo $this->Html->link('Building a version', array('controller' => 'help', 'action' => 'view', 'building_a_version'));?></li>
				</ol>
			<li><?php echo $this->Html->link('Building a response form', array('controller' => 'help', 'action' => 'view', 'building_a_response_form'));?>
				<ol class='submenu' data-parent='building_a_response_form'>
				<li><?php echo $this->Html->link('Response form options', array('controller' => 'help', 'action' => 'view', 'response_form_options'));?></li>
				<li><?php echo $this->Html->link('Multiple choice sections', array('controller' => 'help', 'action' => 'view', 'multiple_choice_sections'));?></li>
				<li><?php echo $this->Html->link('Numeric response sections', array('controller' => 'help', 'action' => 'view', 'numeric_response_sections'));?></li>
				<li><?php echo $this->Html->link('Written response sections', array('controller' => 'help', 'action' => 'view', 'written_response_sections'));?></li>
				<li><?php echo $this->Html->link('Versioning response forms', array('controller' => 'help', 'action' => 'view', 'versioning_response_forms'));?></li>
				</ol>
			</li>
			<li><?php echo $this->Html->link('Sharing with other users', array('controller' => 'help', 'action' => 'view', 'sharing_with_other_users'));?>
				<ol class='submenu' data-parent='sharing_with_other_users'>
				<li><?php echo $this->Html->link('Sending a copy', array('controller' => 'help', 'action' => 'view', 'sending_a_copy'));?></li>
				<li><?php echo $this->Html->link('Using communities', array('controller' => 'help', 'action' => 'view', 'using_communities'));?></li>
				<li><?php echo $this->Html->link('The open question bank', array('controller' => 'help', 'action' => 'view', 'open_question_bank'));?></li>
				</ol>
			</li>
			<li><?php echo $this->Html->link('Printing assessment materials', array('controller' => 'help', 'action' => 'view', 'printing_assessments'));?>
				<ol class='submenu' data-parent='printing_assessments_and_forms'>
				<li><?php echo $this->Html->link('Question books', array('controller' => 'help', 'action' => 'view', 'printing_question_books'));?></li>
				<li><?php echo $this->Html->link('Generic response forms', array('controller' => 'help', 'action' => 'view', 'generic_response_forms'));?></li>
				<li><?php echo $this->Html->link('Forms from class lists', array('controller' => 'help', 'action' => 'view', 'forms_from_class_lists'));?></li>
				<li><?php echo $this->Html->link('Answer keys', array('controller' => 'help', 'action' => 'view', 'printing_answer_keys'));?></li>
				</ol>
			</li>
			<li><?php echo $this->Html->link('Scanning and interpreting forms', array('controller' => 'help', 'action' => 'view', 'scanning_completed_forms'));?>
				<ol class='submenu' data-parent='scanning_completed_forms'>
				<li><?php echo $this->Html->link('Uploading scanned forms', array('controller' => 'help', 'action' => 'view', 'uploading_scanned_forms'));?></li>
				<li><?php echo $this->Html->link('Assessment reports', array('controller' => 'help', 'action' => 'view', 'assessment_reports'));?></li>
				<li><?php echo $this->Html->link('Combined item statistics', array('controller' => 'help', 'action' => 'view', 'combined_statistics'));?></li>
				<li><?php echo $this->Html->link('Working with uploaded scans', array('controller' => 'help', 'action' => 'view', 'working_with_uploaded_scans'));?></li>
				</ol>
			</li>
			<li><?php echo $this->Html->link('Managing assessments and folders', array('controller' => 'help', 'action' => 'view', 'managing_assessments_and_folders'));?></li>
			<li><?php echo $this->Html->link('Managing your account', array('controller' => 'help', 'action' => 'view', 'managing_your_account'));?>
				<ol class='submenu' data-parent='managing_your_account'>
				<li><?php echo $this->Html->link('Editing your account information', array('controller' => 'help', 'action' => 'view', 'editing_your_account_information'));?></li>
				<li><?php echo $this->Html->link('Setting report preferences', array('controller' => 'help', 'action' => 'view', 'setting_report_preferences'));?></li>
				</ol>
			</li>
			</ol>
		</div>
	</div>

	<div class="window_bar" style="grid-column:2/2; grid-row:1/1;"><span id="topicHeader"></span></div>
	<div class="window_content help_panel" style="grid-column:2/2; grid-row:2/2; overflow-y:scroll;">
		<?php include("pages/" . $includeName . ".ctp"); ?>
	</div>
</div>

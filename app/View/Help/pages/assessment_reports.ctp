<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Assessment reports"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#assessment_reports">Assessment reports</a></li>
<li><a href="#adjusting_your_assessment">Adjusting your assessment</a></li>
<li><a href="#warnings_and_errors">Warnings and errors</a></li>
</ol>

<h1 id="assessment_reports">Assessment reports</h1>

<p>The results of each analysis are presented in a table as below:</p>

<img src="<?php echo $imagePath; ?>/scores_view.png" />

<p>SmarterMarks generates three different types of reports. Click on the view button next to each to download it:</p>

<ul style="list-style:disc">
<li style="margin-bottom: 10px;"><i>Student reports</i>, containing the information you requested when you set up the assessment. Student reports are presented in the order in which the assessments appear in the uploaded PDF. If a student ID field was used, each report will be tagged with the corresponding student ID; otherwise, for privacy reasons, they are marked only with their position in the uploaded PDF.</li>
<li style="margin-bottom: 10px;">A <i>teacher summary</i> containing an analysis of the assessment and of students' performance on it. The teacher summary includes basic statistics such as mean score, reliability, and discrimination for each item, as well as more advanced analyses such as collusion detection. By default, the teacher summary includes an explanation of each item at the end of the report.  Teacher summaries are fully configurable through <?php echo $this->Html->link('user settings', array('controller' => 'help', 'action' => 'view', 'setting_report_preferences'));?>.</li>
<li style="margin-bottom: 10px;"><i>Response summaries</i>, which include a summary of each student's responses in XML format (readable by Microsoft Excel) and a grade summary in CSV format (which can be imported into many grade book systems). The XML summary will be of interest primarily to users who want to do additional processing of their own.</li>
</ul>

<h1 id="adjusting_your_assessment">Adjusting your assessment</h1>

<p>The buttons at the top-right of the assessment results allow you to perform a few tasks you may want to do while browsing your results:</p>

<ul style="list-style:disc">
<li>The <b>Go to</b> menu lets you navigate to the assessment builder to re-key or otherwise adjust your assessment, or to the list of scans for this assessment, for example to perform a new analysis.</li>
<li>The <b>Rescore</b> button lets you re-analyze this assessment after you have made adjustments without having to upload your PDF again.</li>
</ul>

<h1 id="warnings_and_errors">Warnings and errors</h1>

<p>If any warnings or errors were encountered in processing your scanned response forms, those will be summarized here as well:</p>

<img src="<?php echo $imagePath; ?>/scores_view_warning.png" />

<p><b>Warnings</b> are less severe than errors.  When they appear for just one page in an assessment, they may indicate that something about that page was suspicious &mdash; perhaps the student's use of white space on the form made processing less certain.  When they appear for many pages, though, or for all pages, they likely indicate that the wrong PDF has been uploaded, or that something about the file made processing unreliable.</p>

<p><b>Errors</b> are more severe, and indicate that a page could not be processed at all. If they are present for a single page, they may indicate that something that was not a response form (student rough work, for example) was inadvertently scanned with the response forms. If errors are present on all pages, they indicate either that the wrong PDF has been uploaded, or that the PDF was unreadable. In this case, you should check your PDF to ensure that it is for the correct assessment, and that scanner settings are appropriate.</p>


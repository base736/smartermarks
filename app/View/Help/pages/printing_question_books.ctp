<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Question books"
</script>

<h1>Answer keys</h1>

<p>Question books can be generated for assessments built from a question bank on SmarterMarks. There are two ways to generate question books.</p>

<p>If you are editing your assessment, click the "Print" button at the top right and choose "Question book" from the menu:</p>

<img src="<?php echo $imagePath; ?>/preview_question_book.png" />

<p>Alternatively, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select an assessment and click on "Question book" under the "Print" menu to generate a response form:</p>

<img src="<?php echo $imagePath; ?>/print_question_book.png" />

<p>This will open the "Build question book" dialog:</p>

<img src="<?php echo $imagePath; ?>/print_questions_dialog.png" />

<p>Here, you can choose whether to include a response form with the generated question book, and where it should be added. Three options are available:</p>

<ul>
	<li><b>Question book only</b>, which prints only the question book, allowing you to generate response forms separately.</li>
	<li><b>Response form at the front</b>, which prints the response form before the question book. If the response form has an odd number of pages, an extra page will be added to ensure that the question book starts on its own page.</li>
	<li><b>Response form at the back</b>, which prints the response form after the question book. If the question book has an odd number of pages, an extra page will be added to ensure that the response form starts on its own page.</li>
</ul>

<p>The default choice can be changed in your user settings &mdash; see the <?php echo $this->Html->link('Setting report preferences', array('controller' => 'help', 'action' => 'view', 'setting_report_preferences'));?> section for more information. Once you have made your selection, click "Build" to generate your question book in a new tab.</p>

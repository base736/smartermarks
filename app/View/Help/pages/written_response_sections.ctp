<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Written response sections"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#adding_a_written_response_section">Adding a written response section</a></li>
<li><a href="#section_options">Section options</a></li>
<li><a href="#editing_and_deleting_written_response_questions">Editing and deleting written response questions</a></li>
<li><a href="#scoring_written_response_questions">Scoring written response questions</a></li>
</ol>

<h1 id="adding_a_written_response_section">Adding a written response section</h1>

<p>To edit a response form, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment in the Forms and Versions tab and click on "Edit form" under the "Build" menu:</p>

<img src="<?php echo $imagePath; ?>/edit_form.png" />

<p>To add a written response section to a page, click the "Add Section" drop-down on the desired page and select "Written response section":</p>

<img src="<?php echo $imagePath; ?>/add_wr_section.png" />

<p>A new written response section will be created, and the "WR Section Details" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/new_wr_section.png" />

<p>Use the <b>Number of Questions</b> field to enter the number of questions to be included when the section is created. For information on the other options, see the <a href="#section_options">section options</a> section below.</p>

<p>Click "Save" to save the section and continue.</p>

<h1 id="section_options">Section options</h1>

<p>To edit a response form, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment in the Forms and Versions tab and click on "Edit form" under the "Build" menu:</p>

<img src="<?php echo $imagePath; ?>/edit_form.png" />

<p>View the options for a written response section from anywhere in the editor by clicking  on the section's title in the left panel:</p>

<img src="<?php echo $imagePath; ?>/wr_section_options_view.png" />

<p>To edit the options, click on the "Edit Now" button:</p>

<img src="<?php echo $imagePath; ?>/wr_section_options_edit.png" />

<p>A number of options can be changed from the dialog that appears.</p>

<p>The <b>Section Title</b> field gives the title to appear at the top of this section on response forms.</p>

<p>If the <b>Include space for student responses</b> checkbox is checked, an adjustable space will be left on the response form for students to answer each written response question, and you will have the option of including question text on the form, as shown below:</p>

<img src="<?php echo $imagePath; ?>/wr_with_spaces.png" />

<p>If it is left unchecked, only the score sections will be included on the response form:</p>

<img src="<?php echo $imagePath; ?>/wr_no_spaces.png" />

<p>The <b>Start Numbering At</b> field gives the starting point for question numbering in this section.</p>

<p>The <b>Number of Columns</b> field specifies the number of columns into which questions should be arranged. If space has been left for student responses, only one column is allowed.</p>

<p>Click "Save" to save any changes to the section options.</p>

<h1 id="editing_and_deleting_written_response_questions">Editing and deleting written response questions</h1>

<p>To edit a written response question, click the <span class="edit_icon text_icon"></span> icon under "Actions" next to the question:</p>

<img src="<?php echo $imagePath; ?>/wr_question_edit.png" />

<p>If space has been included for student responses, the following dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/wr_edit_with_spaces.png" />

<p>By default, questions are numbered starting at the question number given and proceeding in sequence from there (for example, "21, 22, 23"). To customize question numbers, breaking a question into parts 'a', 'b', and so on, select the desired question label from the <b>Question</b> drop-down at the top left.</p>

<p>To omit this question, choose "Omit question" from the <b>Scoring</b> drop-down.</p>

<p>The <b>Height</b> field specifies the height of the response area in inches, including any space taken by the prompt.</p>

<p>The <b>Prompt type</b> field allows you to choose the type of prompt (if any) that will be included next to the question number in the response form. There are three options:</p>

<ul>
<li>Selecting <b>None</b> will leave the response area blank, allowing students to use the whole space for their answer.</li>
<li>Selecting <b>Text</b> will display the given text next to the question number. This can be used, for example, to include the question on the response form.</li>
<li>Selecting <b>Image</b> will display an image next to the question number. For example, this can be used to give students a graph to complete.
</ul>

<p>If a text prompt is selected, the <b>Text</b> field will be displayed below, as shown in the image above. This field allows you to include a prompt before the response area. If an image prompt is selected, the dialog will instead look like that shown below:</p>

<img src="<?php echo $imagePath; ?>/wr_edit_image_prompt.png" />

<p>Here, you can click "Select new" to select an image, "Upload" to upload it and show a preview, and set the image height as it will appear on the response form.</p>

<p>Criteria are specified in the <b>Criteria</b> section. Each criterion has a label to indicate what criterion is being scored and a maximum score. To add new criteria, click on the "+" to the right. To remove a criterion, click on the "-" next to it.</p>

<p>If space has not been included for student responses, a simpler dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/wr_edit_no_spaces.png" />

<p>The options are as described above. Click "Save" to save any changes to the question options.</p>

<p>To delete a question, click the <span class="delete_icon text_icon"></span> icon next to the question.</p>

<h1 id="scoring_written_response_questions">Scoring written response questions</h1>

<p>Each written response question includes a "Teacher use only" scoring section. After students have returned their response forms, this section is used by the teacher to score written response questions. Fill in the appropriate bubbles before scanning the response forms. For example, in the section below, "Axis labels" has been given a score of 1.5, "Correct slope" a score of 2.0, and "Correct intercept" a score of 0.0, for a total of 3.5/5.</p>

<img src="<?php echo $imagePath; ?>/wr_scoring.png" style="display:block; width:300px; margin:20px auto;" />

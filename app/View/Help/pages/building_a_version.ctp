<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Building a version"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#shuffling_questions">Shuffling questions</a></li>
<li><a href="#previewing_an_assessment">Previewing an assessment</a></li>
<li><a href="#building_a_version">Building a version</a></li>
</ol>

<h1 id="shuffling_questions">Shuffling questions</h1>

<p>Questions can be shuffled by clicking on the "Shuffle" button a the top of the assessment preview:</p>

<img src="<?php echo $imagePath; ?>/assessment_preview_shuffle.png" />

<p>Shuffling questions also shuffles question parts and answers where allowed, and recalculates any random and calculated variables. To return the questions to the order shown in the layout on the left, reload the page or click the "Sort" button:</p>

<img src="<?php echo $imagePath; ?>/assessment_preview_sort.png" />

<h1 id="previewing_an_assessment">Previewing an assessment</h1>

<p>To build a PDF with the same layout that the final version will have, without creating the assessment version itself, click the "Preview" button at the top of the assessment:</p>

<img src="<?php echo $imagePath; ?>/assessment_preview_preview.png" />

<p>The preview will open in a new tab:</p>

<img src="<?php echo $imagePath; ?>/version_preview.png" />

<p>The assessment preview can be used to confirm that the generated question book will have the desired format before building a full version.</p>

<h1 id="building_a_version">Building a version</h1>

<p>To build the version of the assessment that's currently being displayed, click the "Build" button at the top of the assessment:</p>

<img src="<?php echo $imagePath; ?>/assessment_preview_build.png" />

<p>If the assessment is free of errors, the "Build version" dialog will open:</p>

<img src="<?php echo $imagePath; ?>/build_version.png" />

<p>Any warnings (for example, if no correct answer has been given for a question) will be displayed here. In this dialog, you can edit the default title you specified earlier to make it unique to this version. For example, you may add a version number after the title:</p>

<img src="<?php echo $imagePath; ?>/build_version_filled.png" />

<p>When you are ready to build the version, choose a location for the version and click "Build". In a new tab, you will be forwarded to the settings for your new assessment:</p>

<img src="<?php echo $imagePath; ?>/build_version_settings.png" />

<p>Give the version a name in the "Save As" field, adjust any other settings as required, and click "Save" to finish generating the version.</p>

<p>Limited editing can be done on the assessment version after it has been built. For example, the key can be changed, and questions omitted or made bonus questions, but the layout cannot be modified. To edit an assessment version after it has been generated, click on the "Forms and Versions" tab in the top bar:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Click on the assessment version you would like to edit, then click on the "Build" button and choose "Edit form", or double click on the assessment version.</p>

<img src="<?php echo $imagePath; ?>/assessment_version_edit.png" />

<p>For more information on editing assessment versions and response forms, see the <?php echo $this->Html->link('Building a response form', array('controller' => 'help', 'action' => 'view', 'building_a_response_form'));?> section.</p>

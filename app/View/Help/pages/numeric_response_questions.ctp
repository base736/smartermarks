<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Numeric response questions"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#decimal_and_scientific_notation_questions">Decimal and scientific notation questions</a></li>
<li><a href="#fraction_questions">Fraction questions</a></li>
<li><a href="#selection_questions">Selection questions</a></li>
</ol>

<h1 id="decimal_and_scientific_notation_questions">Decimal and scientific notation questions</h1>

<p>Decimal response questions ask students for a numerical response:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/numeric_response_decimal_example.png" />

<p>Scientific notation questions are similar, but allow for very large or very small values:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/numeric_response_scientific_example.png" />

<p>To build a decimal question, select "Numeric response" and "Decimal" from the Type selectors:</p>

<img src="<?php echo $imagePath; ?>/numeric_response_decimal.png" />

<p>Scientific notation questions can be selected by choosing "Scientific" from the second selector. For both types, the question editor allows you to configure the following:</p>

<ul>
<li style="margin-bottom: 10px;">The <b>Blanks</b> field allows you to select the number of columns given for the answer. Note that this will include the decimal for decimal answer questions.</li>
<li style="margin-bottom: 10px;">The <b>Prompt</b> field allows you to choose what type of response prompt, if any &ndash; for example, "Record your answer in the numerical-response section of the answer sheet" &ndash; will be displayed after the question text.</li>
<li style="margin-bottom: 10px;">Select <b>Allow Small Errors</b> to give full marks for responses which are within some tolerance of the correct response (for example, to forgive rounding errors). When the checkbox is selected, a "Tolerance" field will appear.  Enter the percent error to allow, relative to the correct response.</li>
<li style="margin-bottom: 10px;">Select <b>Part Marks For Factor-of-10</b> to give part marks for responses which differ from the correct response by a factor of ten (that is, which are 10, 100, etc. times too small or large).  When the checkbox is selected, a "Multiplier" field will appear.  Enter the multiplier that should be applied to the full value of the question if the response is out by a factor of ten.</li>
<li style="margin-bottom: 10px;">The last option allows you to change SmarterMarks' behaviour when a student gives a response with the incorrect number of significant digits (for example, answering "1.8" when the key is "1.80"). Selecting <b>Round as needed</b> will round the response and keyed answer to their common significant digits, so "1.8" will be a correct response for both "1.80" and "1.83". Selecting <b>Add zeros as needed</b> will add zeros to achieve a common number of significant digits, so "1.8" will be a correct response for "1.80" but not "1.83". Finally, selecting <b>Mark as incorrect</b> will make no modifications, so "1.8" will be correct only if the keyed answer is exactly "1.8". You can also choose to give reduced marks if modifications are required.</li>
</ul>

<p>For more information on building answers, see the <?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?> section.</p>

<h1 id="fraction_questions">Fraction questions</h1>

<p>Fraction questions ask students for a fractional answer. For example:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/numeric_response_fraction_sample.png" />

<p>Response form sections that include a fraction question automatically allow students to enter a "/" as part of their answer, as shown below:</p>

<div style="margin-bottom:10px; background-color:white; width:100%;">
	<img style="width:30%;" src="<?php echo $imagePath; ?>/numeric_response_fraction_bubbles.png" />
</div>

<p>To build a fraction question, select "Numeric response" and "Fraction" from the Type selectors:</p>

<img src="<?php echo $imagePath; ?>/numeric_response_fraction.png" />

<p>For fraction questions, the editor allows you to configure the following:</p>

<ul>
<li style="margin-bottom: 10px;">The <b>Blanks</b> field allows you to select the number of columns given for the answer.</li>
<li style="margin-bottom: 10px;">The <b>Prompt</b> field allows you to choose what type of response prompt, if any &ndash; for example, "Record your answer in the numerical-response section of the answer sheet" &ndash; will be displayed after the question text.</li>
</ul>

<p>Fraction questions must include a "/" symbol in their answers. For more information on building answers, see the <?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?> section.</p>

<h1 id="selection_questions">Selection questions</h1>

<p>Selection questions ask students to select several answers from a list, usually given in the context. An example is given below:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/numeric_response_selection_example.png" />

<p>To build a selection question, select "Numeric response" and "Selection" from the Type selectors:</p>

<img src="<?php echo $imagePath; ?>/numeric_response_selection.png" />

<p>For selection questions, the editor allows you to configure the following:</p>

<ul>
<li style="margin-bottom: 10px;">The <b>Blanks</b> field allows you to select the number of columns given for the answer &mdash; that is, the maximum number of selections students can make.</li>
<li style="margin-bottom: 10px;">The <b>Prompt</b> field allows you to choose what type of response prompt, if any, will be displayed after the question text. The default for selection questions is no response prompt.</li>
<li style="margin-bottom: 10px;">Select <b>Marks are per correct digit</b> to give the marks indicated in answers for <i>each correct digit</i>; for example, for an answer value of 0.5 and a keyed answer '1095', the response '1085' would receive 1.5 marks, and '1095' would receive 2.0 marks.</li>
<li style="margin-bottom: 10px;">Select <b>Allow Partial Match</b> to give part marks for incorrect responses which match at least half of the selections in a correct answer. When the checkbox is selected, a "Multiplier" field will appear.  Enter the multiplier that should be applied to the full value of the question for partially correct responses.</li>
<li style="margin-bottom: 10px;">Select <b>Ignore Selection Order</b> ignore the order of the selections &mdash; for example, if '367' and '736' should both be considered correct.</li>
</ul>

<p>For more information on building answers, see the <?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?> section.</p>

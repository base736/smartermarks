<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Item statistics"
</script>

<h1 id="viewing_statistics">Item statistics</h1>

<p>Statistics for each question can be viewed from the question editor, combining results from several classes, and for shared questions, even several schools. To view the statistics for a question, open the question from the "Question Bank" tab and choose "Item Statistics" from the view pane at the top left:</p>

<img src="<?php echo $imagePath; ?>/item_statistics_button.png" />

<p>This will open the item statistics:</p>

<img src="<?php echo $imagePath; ?>/item_statistics_overview.png" />

<p>At the top of the statistics view, you can set filters for the data to be displayed. These include <b>filtering by date</b>, to include only scans that were uploaded after a given date, and <b>filtering by user</b>, to include only scans you uploaded.</p>

<p>To apply changes to any of these settings, click the "Update statistics" button:</p>

<img src="<?php echo $imagePath; ?>/item_statistics_update.png" />

<p>Below the filters, item statistics will be shown when they have loaded:</p>

<img src="<?php echo $imagePath; ?>/item_statistics.png" />

<p>Item statistics are generated from the data included by your chosen filters. For each part of the selected item, they include average score and discrimination, as well as a response distribution for multiple choice questions or numeric response questions with more than one scored answer.</p>

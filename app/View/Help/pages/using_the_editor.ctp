<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Using the editor"
</script>

<h1>Outline</h1>

<p>Contexts, questions, and answers are all edited using variations of the same editor. This editor gives you the tools you need to lay out your assessments, while at the same time allowing for SmarterMarks to apply consistent formatting across your assessments. Tools available include:</p>

<ol>
<li><a href="#text_formatting">Text formatting</a></li>
<li><a href="#text_alignment">Text alignment</a></li>
<li><a href="#lists">Lists</a></li>
<li><a href="#variables">Variables</a></li>
<li><a href="#blanks">Blanks</a></li>
<li><a href="#tables">Tables</a></li>
<li><a href="#images">Images</a></li>
<li><a href="#equations">Equations</a></li>
<li><a href="#symbols">Symbols</a></li>
<li><a href="#code">Code</a></li>
</ol>

<h1 id="text_formatting">Text formatting</h1>

<p>Text can be formatted using the text formatting buttons in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_formatting.png" />

<p>From the left, the buttons in the formatting section allow you to:</p>

<ul>
<li>Remove formatting from text;</li>
<li>Make text bold;</li>
<li>Put text in italics;</li>
<li>Underline text;</li>
<li>Make text a subscript; or</li>
<li>Make text a superscript.</li>
</ul>

<p>To reformat a section of text, select the text to be formatted and click the appropriate button in the top bar.</p>

<h1 id="text_alignment">Text alignment</h1>

<p>Text can be left-aligned or centered using the text alignment buttons in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_alignment.png" />

<p>Select text and click a button to change its alignment, or just click an alignment button to change the alignment at the cursor.</p>

<h1 id="lists">Lists</h1>

<p>Bulleted and numbered lists can be created using the list buttons in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_lists.png" />

<p>Clicking the button reveals a number of options. For bulleted lists, you can choose to use the default bullets, or to format your list as a checklist:</p>

<img src="<?php echo $imagePath; ?>/editor_lists_bulleted.png" />

<p>Numbered lists can use numbers, lowercase or uppercase letters, or lowercase or uppercase roman numerals:</p>

<img src="<?php echo $imagePath; ?>/editor_lists_numbered.png" />

<p>To create a list, select the lines to be turned into a list and click choose a list type from the drop-down. Only one level of list is supported &mdash; for example, you can't use lowercase letters (a,b,c,&hellip;) as a sub-list for a numbered list (1,2,3,&hellip;).</p>

<h1 id="variables">Variables</h1>

<p>Randomized and calculated variables can be added to text using the variables tool in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_variables.png" />

<p>If variables have already been defined, clicking the variables tool will list those:</p>

<img src="<?php echo $imagePath; ?>/editor_variables_list.png" />

<p>Otherwise, it will open the variable editor. For more information on the use of variables, see the <?php echo $this->Html->link('Using variables', array('controller' => 'help', 'action' => 'view', 'using_variables'));?> section.</p>

<h1 id="blanks">Blanks</h1>

<p>Blanks can be added using the blanks button in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_blanks.png" />

<p>Click the blanks button to insert a blank. If you are building a table-style (i/ii) multiple choice question, clicking the blanks button will display a list of the labels you have defined:</p>

<img src="<?php echo $imagePath; ?>/editor_blanks_expanded.png" />

<p>Choose a label from the list to insert it below.</p>

<h1 id="tables">Tables</h1>

<p>Tables can be added using the tables button in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_tables.png" />

<p>Clicking on the tables button will display the table sizing tool:</p>

<img src="<?php echo $imagePath; ?>/editor_tables_expanded.png" />

<p>Hover the mouse over the tool to choose a table size and click to insert a table, or click "More tables&hellip;" for more options.</p>

<img src="<?php echo $imagePath; ?>/editor_tables_resize.png" />

<p>To change the size of the columns in a table, hover over the border you would like to move, click and drag left or right. To change the size of the table, change the vertical alignment of the contents of a cell or cells, or to show and hide borders, select the table or the cells you would like to change and click the settings button at the top right:</p>

<img src="<?php echo $imagePath; ?>/editor_tables_edit_button.png" />

<p>This will open the table settings dialog. Here, you can set the size of the table, the vertical alignment of cell contents, or show and hide borders either individually or in groups.</p>

<img src="<?php echo $imagePath; ?>/editor_tables_settings.png" />

<p>When you are done making changes, click "Done".</p>

<h1 id="images">Images</h1>

<p>Images can be added using the images button in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_images.png" />

<p>Clicking the images button will open the "Add image" dialog:</p>

<img src="<?php echo $imagePath; ?>/editor_images_add.png" />

<p>Click "Select file" to choose a file to upload, then choose a width. Widths are given as a percentage of the width of the image's container. For example, an image with a width of 50% placed in a table will take up 50% of its table cell, while the same image placed in the main text will take up 50% of the width of the context, question, or answer.<p>

<img src="<?php echo $imagePath; ?>/editor_images_add_filled.png" />

<p>When you are done, click "Upload" to upload and insert the image</p>

<img src="<?php echo $imagePath; ?>/editor_images_example.png" />

<p>Images can be resized after they have been inserted. To resize an image, click on the image to select it. You can then resize the image in the editor by clicking and dragging the resize handle at the bottom right:</p>

<img src="<?php echo $imagePath; ?>/editor_images_handle.png" />

<p>Alternatively, after selecting the image, you can click on the settings button at the top right.</p>

<img src="<?php echo $imagePath; ?>/editor_images_settings.png" />

<p>This will open the settings dialog:</p>

<img src="<?php echo $imagePath; ?>/editor_images_edit_settings.png" />

<p>Enter a new width, then click "Done" to resize the image.</p>

<h1 id="equations">Equations</h1>

<p>Equations can be added using the equations tool in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_equations.png" />

<p>Clicking on the equations tool will open the "Edit Equation" dialog:</p>

<img src="<?php echo $imagePath; ?>/editor_equations_edit.png" />

<p>The equation editor offers a number of tools to help you easily build the equations you need. If you don't see the tools you need, let us know at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a> and we'll work to add them as soon as possible!</p>

<p>From left to right, the tools are:</p>

<ol>
<li>The <b>text tool</b>. Letters in equations are automatically formatted as variables. To insert plain text, click the text tool and type your text into the resulting box.</li>
<li><b>Symbols</b>, including operators like "greater than or equal to". As with the main editor, clicking the symbols button opens a palette of available symbols. Click on the one you need to insert it into the equation.</li>
<li><b>Accents</b>, including vector symbols. Select the symbol you would like to give an accent, then click on the accents button to open a palette of frequently-used accents.</li>
<li><b>Stacked fractions</b>. Click on the button to insert a stacked fraction, then enter your numerator and denominator into the boxes given.</li>
<li><b>Superscripts</b>, <b>subscripts</b>, and <b>both</b>. Superscripts can also be created by typing a caret ("^") in the editor; subscripts can be created by typing an underscore ("_").</li>
<li><b>Square roots</b> and <b>other radicals</b>.</li>
<li><b>Matrices</b>, which can also be used for combinatorial notation.</li>
<li><b>Operators</b>, including sums, limits, and integrals.</li>
</ol>

<p><b>Parentheses</b>, <b>brackets</b>, and <b>square brackets</b> can be inserted by typing '(', '{', and '[' respectively; <b>absolute value</b> symbols can be inserted by typing '|'. The left and right arrows can be used to move the cursor through the equation.</p>

<p>In addition to the shortcuts above, a number of tools and all symbols can be accessed using keyboard commands. The keyboard command for each symbol is shown below the mouse pointer when the mouse is hovered over it in the symbols list (for example, "\alpha" or "\rightarrow"). Keyboard commands for the other tools are given below:</p>

<div id="shortcuts_list">
	<div style="display:inline-block; vertical-align:top; width:50%;">
		<ul>
		<li><span>Hat accent:</span>\hat</li>
		<li><span>Bar accent:</span>\bar</li>
		<li><span>Vector accent:</span>\vec</li>
		<li><span>Dot accent:</span>\dot</li>
		<li><span>Text:</span>\text</li>
		</ul>
	</div><div style="display:inline-block; vertical-align:top; width:50%;">
		<ul>
		<li><span>Fraction:</span>\frac</li>
		<li><span>Square root:</span>\sqrt</li>
		<li><span>Left brace:</span>\lbrace</li>
		<li><span>Sum:</span>\sum</li>
		<li><span>Limit:</span>\lim</li>
		<li><span>Integral:</span>\int</li>
		</ul>
	</div>
</div>

<p>To enter a command, begin with the leading "\". The keyboard command bar will appear:</p>

<img src="<?php echo $imagePath; ?>/equation_commands.png" />

<p>As you type the command, it will appear in the bar. When a recognized command is completed, the symbol or element will be inserted automatically. To cancel the command, press escape. If you need to use the backslash character in your question, simply type "\\" and it will be inserted.</p>

<p>Click "Save" when you are done editing the equation to insert it into the editor.</p>

<img src="<?php echo $imagePath; ?>/editor_equations_inserted.png" />

<p>To edit an equation that has already been created, click it to select it in the editor:</p>

<img src="<?php echo $imagePath; ?>/editor_equations_selected.png" />

<p>With the equation selected, click on the settings button at the top right to open the equation editor.</p>

<h1 id="symbols">Symbols</h1>

<p>Symbols can be added using the symbols button in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_symbols.png" />

<p>Clicking on the symbols button opens a list of available symbols:</p>

<img src="<?php echo $imagePath; ?>/editor_symbols_expanded.png" />

<p>Click on a symbol to insert it in the editor. Don't see the symbol you're looking for? Let us know at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>!</p>

<p>Symbols can also be inserted using keyboard commands. The keyboard command for each symbol is shown below the mouse pointer when the mouse is hovered over it in the symbols list (for example, "\alpha" or "\rightarrow"). To enter a command, begin with the leading "\". The keyboard command bar will appear:</p>

<img src="<?php echo $imagePath; ?>/html_commands.png" />

<p>As you type the command, it will appear in the bar. When a recognized command is completed, the character will be inserted automatically. To cancel the command, press escape. If you need to use the backslash character in your question, simply type "\\" and it will be inserted.</p>

<h1 id="code">Code</h1>

<p>Code, or other monospaced text, can be added using the code button in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_code.png" />

<p>Clicking on the code button creates a new code area in the text:</p>

<img src="<?php echo $imagePath; ?>/editor_code_empty.png" />

<p>Code areas allow indenting using tabs, and allow easy entry of computer code:</p>

<img src="<?php echo $imagePath; ?>/editor_code_filled.png" />

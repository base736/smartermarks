<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Uploading scanned forms"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#uploading_from_your_computer">Uploading from your computer</a></li>
</ol>

<h1 id="uploading_from_your_computer">Uploading from your computer</h1>

<p>To upload and mark new scans from your computer, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment for which you will be uploading responses, and choose "Mark new scans" from the "Score" menu:</p>

<img src="<?php echo $imagePath; ?>/mark_new_scans.png" />

<p>You will be redirected to the upload page:</p>

<img src="<?php echo $imagePath; ?>/scans_upload.png" />

<p>In the "Details" field, enter any details you wish to associate with this set of scans. For example, if you are processing the results of several teachers' classes, or of a few late students, the details field would be a good place to record that. The details field is optional.</p>

<p>Next, click "Select PDF" to select the PDF containing scanned response forms, or drag the PDF over this window to select it.  Click "Upload".  A progress bar will appear showing the upload progress. Once your PDF has been uploaded, you will be automatically redirected toward a page showing the progress of the analysis:</p>

<img src="<?php echo $imagePath; ?>/processing.png" />

<p>Once this page is showing, you may return to the Forms and Versions tab to upload other scans, or wait while this one processes.</p>

<p>If there are warnings or errors, these will be indicated below the progress bar. Warnings and errors may indicate problems with the PDF you uploaded (you may, for example, have uploaded the wrong PDF), or they may simply reflect abnormalities a well-processed page which the system is suggesting may be worth a second look.</p>

<p>Once processing is complete, you will be automatically redirected toward the <?php echo $this->Html->link('assessment reports', array('controller' => 'help', 'action' => 'view', 'assessment_reports'));?> section for this set of scans, which will also include more information on any warnings and errors that were encountered.</p>

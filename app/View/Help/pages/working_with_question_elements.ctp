<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Working with question elements"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#editing_the_question_context">Editing the question context</a></li>
<li><a href="#adding_and_editing_question_parts">Adding, editing, and deleting question parts</a></li>
<li><a href="#adding_and_editing_answers">Adding, editing, and deleting answers</a></li>
<li><a href="#creating_a_detailed_key">Creating a detailed key</a></li>
<li><a href="#editing_wr_rubric">Editing a written response rubric</a></li>
</ol>

<h1 id="editing_the_question_context">Editing the question context</h1>

<p>To edit the context for a question, click on the "Edit" button in the context pane:</p>

<img src="<?php echo $imagePath; ?>/context_edit_button.png" />

<p>This will open the "Edit Context" dialog.</p>

<img src="<?php echo $imagePath; ?>/context_edit.png" />

<p>For more information on using the editor, see the <?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?> section. When you are done, click "Save" to save your changes.</p>

<p>For questions that include the answers in the context, the "Edit Context" dialog allows you to set parameters relevant to those answers as well:</p>

<img src="<?php echo $imagePath; ?>/context_edit_2.png" />

<p>The selector on the left can be used to choose the number of columns into which answers are arranged, or to hide answers &mdash; for example, when the options for a matching question are already indicated in an image. When they are displayed, you can also control whether context answers are shuffled using the checkbox on the right.</p>

<h1 id="adding_and_editing_question_parts">Adding, editing, and deleting question parts</h1>

<p>To edit a question part, first select the part you want to edit from the Parts list on the left, if it is not already selected:</p>

<img src="<?php echo $imagePath; ?>/part_select_button.png" />

<p>Click the "Edit" button in the question pane to edit this part.</p>

<img src="<?php echo $imagePath; ?>/part_edit_button.png" />

<p>This will open the "Edit question" dialog:

<img src="<?php echo $imagePath; ?>/part_edit.png" />

<p>For more information on using the editor, see the <?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?> section.</p>
	
<p>In addition to editing the question text, the "Edit question" dialog allows you to change the type and behaviour of the question. There are three main types of question:</p>

<ul>
<li><?php echo $this->Html->link('Multiple choice questions', array('controller' => 'help', 'action' => 'view', 'multiple_choice_questions'));?>, including regular (a,b,c,&hellip;), response table (i/ii), and true/false questions;</li>
<li><?php echo $this->Html->link('Numeric response questions', array('controller' => 'help', 'action' => 'view', 'numeric_response_questions'));?>, including decimal, scientific notation, fraction, and selection questions; and</li>
<li><?php echo $this->Html->link('Written response questions', array('controller' => 'help', 'action' => 'view', 'written_response_questions'));?>.</li>
</ul>

<p>Click on the links above for more information on controlling the behaviour of each question type.</p>

<p>To add a new part to a question, click the "Add Part" button on the left.</p>

<img src="<?php echo $imagePath; ?>/add_part_button.png" />

<p>This will open a new editor. To remove a question part, click on the delete button (<span class="delete_icon text_icon"></span>) next to the part in the Parts pane on the left.</p>

<h1 id="adding_and_editing_answers">Adding, editing, and deleting answers</h1>

<p>For written response questions, answers are given in the form or a rubric. Editing the rubric is described in the <a href="/help/view/working_with_question_elements#editing_wr_rubric">"Editing a written response rubric"</a> section below. For all other question types, click the "Add New Answer" button at the bottom right of the answers pane to add a new answer to a question part:</p>

<img src="<?php echo $imagePath; ?>/answer_add_button.png" />

<p>This will open the Edit Answer dialog:</p>

<img src="<?php echo $imagePath; ?>/answer_add.png" />

<p>For more information on using the editor, see the <?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?> section.</p>

<p>The allowed content of answers depends on the type of question:</p>
<ul>
<li>For <b>multiple choice questions</b>, answers can include most of the elements allowed in question parts &mdash; for example, formatted text, variables, images, and equations;</li>
<li>For <b>decimal NR questions</b>, answers can be a variable or a number like "28.5";</li>
<li>For <b>scientific NR questions</b>, answers can be a variable or a number in scientific notation, using "e" to indicate the exponent &mdash; for example, 3.58&times;10<sup>-5</sup> would be entered as 3.58e-5;</li>
<li>For <b>fraction NR questions</b>, answers are given as a fraction like "3/17"; and finally,</li>
<li>For <b>selection NR questions</b>, answers are given using digits only, and optionally including the wildcard "?" to indicate that any digit is acceptable.</li>
</ul>

<p>In addition to the answer text, the Edit Answer dialog allows you to change the value of the answer (that is, the number of marks students will be given for choosing this answer) and an optional rationale (how the answer was obtained). Simple answer keys can be set using the value field; for more detailed keys that give marks for several responses or require more than one selection in a multiple choice section, see the "Creating a detailed key" section below.</p>

<p>To edit an existing answer, click on the edit button (<span class="edit_icon text_icon"></span>) next to the answer in the Answers pane. To copy an answer, click on the copy button (<span class="copy_icon text_icon"></span>). To delete an answer, click on the delete button (<span class="delete_icon text_icon"></span>).

<h1 id="creating_a_detailed_key">Creating a detailed key</h1>

<p>In addition to the method above, multiple choice keys can be edited by clicking the "Edit key" button at the bottom of the Answers pane:</p>

<img src="<?php echo $imagePath; ?>/edit_key_button.png" />

<p>Clicking on this button will open the Edit key dialog.</p>

<img src="<?php echo $imagePath; ?>/edit_key_plain.png" />

<p>Simple keys can be edited by clicking on the checkbox corresponding to the correct answer and assigning marks on the right. Th Edit key dialog also allows you to assign marks for more than one answer. To add a scored response, click the "+" button on the right and fill in the new row:</p>

<img src="<?php echo $imagePath; ?>/edit_key_two_answers.png" />

<p>Above, full marks will be given to students who answer "a", while half marks will be given to those who answer "b". Multiple choice questions can also be keyed to require that more than one bubble be filled in:</p>

<img src="<?php echo $imagePath; ?>/edit_key_compound_answer.png" />

<p>Above, students will receive marks only if they fill in both "a" and "b" for this question.</p>

<p>To remove a scored response from the list, click the "-" button on the right of the response. When you are done making changes to the key, click "Save".</p>

<h1 id="editing_wr_rubric">Editing a written response rubric</h1>

<p>Written response questions are scored using a rubric, which can be printed as part of the answer key. To edit the rubric for a written response question, click on "Edit" at the bottom right of the rubric pane:</p>

<img src="<?php echo $imagePath; ?>/rubric_edit_button.png" />

<p>This will open the Edit Rubric dialog:</p>

<img src="<?php echo $imagePath; ?>/rubric_edit.png" />

<p>When you are done making changes to the rubric, click "Save".</p>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Printing assessment materials"
</script>

<h1>Overview</h1>

<p>Once an assessment has been built, the following assessment materials can be generated in PDF format:</p>

<ul style="list-style:disc">
<li><?php echo $this->Html->link('Question books', array('controller' => 'help', 'action' => 'view', 'printing_question_books'));?> for assessments which were generated on SmarterMarks;</li>
<li><?php echo $this->Html->link('Generic response forms', array('controller' => 'help', 'action' => 'view', 'generic_response_forms'));?>, optionally with spaces for student information;</li>
<li><?php echo $this->Html->link('Response forms from class lists', array('controller' => 'help', 'action' => 'view', 'forms_from_class_lists'));?>, with student information encoded on the form; and</li>
<li><?php echo $this->Html->link('Answer keys', array('controller' => 'help', 'action' => 'view', 'printing_answer_keys'));?>, generated as a filled-in response form.</li>
</ul>

<h1>Using response forms</h1>

<p>SmarterMarks is designed to be robust, with an error rate of less than 1 in 10,000 when tested on real-world responses. As a result, you have a great deal of flexibility in how you use our response forms.  For example:</p>

<ul style="list-style:disc">
<li style="margin-bottom:10px"><b>Assessments need not be completed in pencil</b>. While we suggest that students use something they can erase to complete forms, forms completed in pen will scan as well as those completed in pencil.</li>
<li style="margin-bottom:10px"><b>Blank space is working space</b>. Any blank space on the form can be used by student to show rough work, to answer written response questions, or to doodle.</li>
<li style="margin-bottom:10px"><b>Forms can be customized</b>. For short quizzes, cut the response form out and paste it at the bottom of your quiz. For assessments including a written response section, paste your written response questions at the bottom of the form. For sponsored events, modify the PDF to include sponsor logos.</li>
</ul>

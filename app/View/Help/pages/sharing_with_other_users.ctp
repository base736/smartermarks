<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Sharing with other users"
</script>

<h1>Overview</h1>

<p>SmarterMarks offers three ways to share your work with other users:</p>

<ol>
<li><?php echo $this->Html->link('Sending a copy', array('controller' => 'help', 'action' => 'view', 'sending_a_copy'));?>, for sharing single items when you don't expect to send more later.</li>
<li><?php echo $this->Html->link('Using communities', array('controller' => 'help', 'action' => 'view', 'using_communities'));?>, which make it easier to share with the same group repeatedly or with large groups (for example, all teachers in your district or colleagues around the province).</li>
<li><?php echo $this->Html->link('The open question bank', array('controller' => 'help', 'action' => 'view', 'open_question_bank'));?>, which allows you to share the questions you've created with all SmarterMarks users, and to find proven questions from a wide variety of subject areas.</li>
</ol>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Scanning and interpreting forms"
</script>

<h1>Overview</h1>

<p>Reading your students' responses into SmarterMarks is as easy as scanning them using the equipment your school likely already has and uploading them to our system.</p>

<p>SmarterMarks will work accurately with a wide variety of scanning hardware and settings, with only two requirements:</p>

<ul style="list-style:disc">
<li><b>Scanned assessments must be uploaded in PDF format</b>.</li>
<li><b>Your scanner must be in "image" mode, and not in "OCR" or "text" mode</b>. If you're not sure that you're using the right settings, take a look at your students' scanned response forms.  Often incorrect settings will cause even erased answers to appear black.  If you can mark it, we probably can too.</li>
</ul> 

<p>The remainder of this section is broken into four parts:</p>

<ol style="list-style:lower-latin">
<li><?php echo $this->Html->link('Uploading scanned forms', array('controller' => 'help', 'action' => 'view', 'uploading_scanned_forms'));?>. How to upload your scanned PDFs to our system and begin the analysis.</li>
<li><?php echo $this->Html->link('Assessment reports', array('controller' => 'help', 'action' => 'view', 'assessment_reports'));?>. SmarterMarks generates reports for teachers and students. In this section, we give an overview of what each offers.</li>
<li><?php echo $this->Html->link('Combined item statistics', array('controller' => 'help', 'action' => 'view', 'combined_statistics'));?>. Using the statistics gathered for assessments built on SmarterMarks to improve sample size and predict assessment statistics.</li>
<li><?php echo $this->Html->link('Working with uploaded scans', array('controller' => 'help', 'action' => 'view', 'working_with_uploaded_scans'));?>. How to view the results of previously uploaded assessments, edit scanned responses, and build new analyses.</li>
</ol> 

<h1>A note on privacy</h1>

<p>Concerned about your students' privacy? So are we. SmarterMarks does not store scanned PDFs. We do store mark analyses and summaries, which do not identify students by default, for your convenience. In addition, we store anonymized student responses (stripped of name, ID, and QR code information) for questions built on SmarterMarks to allow us to generate item statistics. In cases where privacy is a particularly sensitive issue, we recommend the following:</p>

<ul>
<li>Delete scores when you are done with them. Student reports and response summaries are the only place where student information is stored on SmarterMarks, and deleting the score removes that information from our servers.</li>
<li>Disable student ID and name fields on response forms, or have students write only their first name and last initial on responses.</li>
</ul>

<p>See our <?php echo $this->Html->link('privacy policy', array('controller' => null, 'action' => 'privacyPolicy')); ?> for more information.</p>

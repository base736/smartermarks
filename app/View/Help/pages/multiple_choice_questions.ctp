<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Multiple choice questions"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#regular_multiple_choice_questions">Regular multiple choice questions (a,b,c,&hellip;)</a></li>
<li><a href="#reponse_table_questions">Response table questions (i/ii)</a></li>
<li><a href="#true_false_questions">True / false questions</a></li>
</ol>

<h1 id="regular_multiple_choice_questions">Regular multiple choice questions (a,b,c,&hellip;)</h1>

<p>Regular multiple choice questions can take one of two forms. First, they can have their answers next to each question part, as shown below:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/multiple_choice_default_example.png" />

<p>They can also have their answers shown in a common context, for example in matching questions:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/multiple_choice_context_example.png" />

<p>For more information on building multiple choice questions with answers given in the context, see the <?php echo $this->Html->link('Question options', array('controller' => 'help', 'action' => 'view', 'question_options'));?> section. To build a regular multiple choice question, select "Multiple choice" and "Default (a,b,c,&hellip;)" from the Type selectors:</p>

<img src="<?php echo $imagePath; ?>/multiple_choice_regular.png" />

<p>Regular multiple choice questions have only one option. To shuffle answers when a new assessment version containing this question is generated, check "Shuffle answers".</p>

<p>For more information on building answers, see the <?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?> section.</p>

<h1 id="reponse_table_questions">Response table questions (i/ii)</h1>

<p>Response table questions are multiple choice questions that give their answers in a table, as shown below:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/multiple_choice_table_example.png" />

<p>Response table questions can be useful for testing the relationship between two concepts. To build a response table question, select "Multiple choice" and "Response table" from the Type selectors:</p>

<img src="<?php echo $imagePath; ?>/multiple_choice_response_table.png" />

<p>The checkboxes on the left allow you to control whether or not the answers for the question are shuffled when an assessment version is generated, and whether a response prompt ("The statement above&hellip;" in the example) is added before the response table.</p>
	
<p>On the right, the labels used for each blank are defined. Enter each label as required, clicking the "+" button to add a new label, and the "-" button to remove one. For example, for the example above, the setup might look like this:</p>

<img src="<?php echo $imagePath; ?>/multiple_choice_response_table_filled.png" />

<p>For more information on building answers, see the <?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?> section.</p>

<h1 id="true_false_questions">True / false questions</h1>

<p>True/false questions appear as below in assessments:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/true_false_example.png" />

<p>To build a true/false question, select "Multiple choice" and "True / false" from the Type selectors:</p>

<img src="<?php echo $imagePath; ?>/multiple_choice_true_false.png" />

<p>The correct answer for a multiple choice question is defined in the question. Choose the appropriate answer using the "Answer" selector.</p>

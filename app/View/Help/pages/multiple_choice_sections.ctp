<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Multiple choice sections"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#adding_a_multiple_choice_section">Adding a multiple choice section</a></li>
<li><a href="#editing_multiple_choice_section_options">Editing multiple choice section options</a></li>
<li><a href="#basic_section_options">Basic section options</a></li>
<li><a href="#editing_a_multiple_choice_key">Editing a multiple choice key</a></li>
<li><a href="#changing_question_numbers">Changing question numbers</a></li>
<li><a href="#omitting_questions">Omitting questions</a></li>
<li><a href="#changing_question_values">Changing question values</a></li>
<li><a href="#answers_with_more_than_one_selection">Answers with more than one selection</a></li>
<li><a href="#defining_multiple_scored_answers">Defining multiple scored answers</a></li>
</ol>

<h1 id="adding_a_multiple_choice_section">Adding a multiple choice section</h1>

<p>To edit a response form, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment in the Forms and Versions tab and click on "Edit form" under the "Build" menu:</p>

<img src="<?php echo $imagePath; ?>/edit_form.png" />

<p>To add a multiple choice section to a page, click the "Add Section" drop-down on the desired page and select "Multiple choice section":</p>

<img src="<?php echo $imagePath; ?>/add_mc_section.png" />

<p>A new multiple choice section will be created, and the "MC Section Details" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/new_mc_section.png" />

<p>For more information on multiple choice section options and entering a key, see the <a href="#basic_section_options">basic section options</a> and <a href="#editing_a_multiple_choice_key">editing a multiple choice key</a> sections below.

<h1 id="editing_multiple_choice_section_options">Editing multiple choice section options</h1>

<p>To edit a response form, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment in the Forms and Versions tab and click on "Edit form" under the "Build" menu:</p>

<img src="<?php echo $imagePath; ?>/edit_form.png" />

<p>View the options for a multiple choice section from anywhere in the editor by clicking  on the section's title in the left panel:</p>

<img src="<?php echo $imagePath; ?>/mc_section_options_view.png" />

<p>To edit the options, click on the "Edit Now" button:</p>

<img src="<?php echo $imagePath; ?>/mc_section_options_edit.png" />

<h1 id="basic_section_options">Basic section options</h1>

<p>To edit basic options, first bring up the multiple choice section options dialog as described in the <a href="#editing_multiple_choice_section_options">Editing multiple choice section options</a> section. A number of options can be changed from the dialog that appears.</p>

<p>The <b>Section Title</b> field gives the title to appear at the top of this section on response forms.</p>

<p>The <b>Start Numbering At</b> field gives the starting point for question numbering in this section.</p>

<p>The <b>Number of Columns</b> field specifies the number of columns into which questions should be arranged. Some choices may result in an empty column &mdash; for example, 16 questions distributed over 5 columns will result in an empty fifth column.</p>

<p>The <b>Number of Choices</b> field gives the number of choices ("a", "b", "c", and so on) for each question in this section.</p>

<p>The <b>Number of Questions</b> field gives the number of questions in this section.</p>

<p>Click "Save" to save any changes to the section options.</p>

<h1 id="editing_a_multiple_choice_key">Editing a multiple choice key</h1>

<p>To edit the key for a multiple choice section, first bring up the multiple choice section options dialog as described in the <a href="#editing_multiple_choice_section_options">Editing multiple choice section options</a> section.</p>

<p>To begin editing the key, click on the first question you would like to edit. Entering a response will automatically move the cursor to the next question &mdash; there's no need to press tab or an arrow key.  If you would like to <a href="#answers_with_more_than_one_selection">require more than one selection</a> for an answer (for example, "a and b" as a single response), or <a href="#defining_multiple_scored_answers">define multiple scored answers</a> (for example, "a" for full points or "b" for half points), see the sections linked here.</p>

<p>Click "Save" to save any changes to the key.</p>

<h1 id="changing_question_numbers">Changing question numbers</h1>

<p>By default, questions are numbered starting at the question number given and proceeding in sequence from there (for example, "21, 22, 23"). To customize question numbers, breaking a question into parts 'a', 'b', and so on, begin by bringing up the multiple choice section options dialog as described in the <a href="#editing_multiple_choice_section_options">Editing multiple choice section options</a> section. In the key, click on the question number for the question you wish to edit to bring up the "Question Details" dialog:</p>

<img src="<?php echo $imagePath; ?>/mc_question_details.png" />

<p>Select the desired question label from the "Question" drop-down at the top left.</p>

<p>Click "Done" when you have made your change. Finally, click "Save" on the section options dialog to save your changes.</p>

<h1 id="omitting_questions">Omitting questions</h1>

<p>There are two ways to omit questions from a multiple choice section. Both begin by bringing up the multiple choice section options dialog as described in the <a href="#editing_multiple_choice_section_options">Editing multiple choice section options</a> section.  Then either:</p>

<ul>
<li>Click on the response for the question you would like to omit. Delete the response, leaving that entry in the key blank.  Or</li>
<li>Click on the question number for the question you would like to omit. In the question details dialog, choose "Omit question" from the <b>Scoring</b> drop-down.</li>
</ul>

<p>Click "Save" to save any changes to the key.</p>

<h1 id="changing_question_values">Changing question values</h1>

<p>To change the point value of a question in a multiple choice section, first bring up the multiple choice section options dialog as described in the <a href="#editing_multiple_choice_section_options">Editing multiple choice section options</a> section.  In the key, click on the question number for the question you wish to edit to bring up the "Question Details" dialog:</p>

<img src="<?php echo $imagePath; ?>/mc_question_details.png" />

<p>Enter a new value in the "Marks" field, and click "Done". Finally, click "Save" on the section options dialog to save any changes.</p>

<h1 id="answers_with_more_than_one_selection">Answers with more than one selection</h1>

<p>To create a question with a response requiring more than one selection (for example, "a and b" as a single response), first bring up the multiple choice section options dialog as described in the <a href="#editing_multiple_choice_section_options">Editing multiple choice section options</a> section.  In the key, click on the question number for the question you wish to edit to bring up the "Question Details" dialog:</p>

<img src="<?php echo $imagePath; ?>/mc_question_details.png" />

<p>To specify that more than one selection must be made for this question, click the checkbox next to each selection.  For example, an answer of "a and b" worth 1 mark would be specified like this:</p>

<img src="<?php echo $imagePath; ?>/mc_question_two_choices.png" />

To give marks for several individual answers, see the next section. When you are done, click "Done" to return to the section options dialog. Finally, click "Save" on the section options dialog to save any changes.</p>

<h1 id="defining_multiple_scored_answers">Defining multiple scored answers</h1>

<p>To specify several scored answers for a multiple choice question (for example, "b" for full points or "c" for half points), first bring up the multiple choice section options dialog as described in the <a href="#editing_multiple_choice_section_options">Editing multiple choice section options</a> section.  In the key, click on the question number for the question you wish to edit to bring up the "Question Details" dialog:</p>

<img src="<?php echo $imagePath; ?>/mc_question_details.png" />

<p>To add a new scored response for this question, click the "+" next to an existing response.  A new response will be added:</p>

<img src="<?php echo $imagePath; ?>/mc_question_two_answers.png" />

<p>Select any checkboxes required for this response and assign a value &mdash; the question above, for example, will give 1 mark for a response of "b" and 0.5 marks for "c". See the <a href="#answers_with_more_than_one_selection">Answers with more than one selection</a> section for more information on responses such as "a and b".</p>

<p>To remove a response, click the "-" next to that response.</p>

<p>When you are done, click "Done" to return to the section options dialog. Finally, click "Save" on the section options dialog to save any changes.</p>

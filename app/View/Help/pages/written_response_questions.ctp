<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Written response questions"
</script>

<h1 id="written_response_questions">Written response questions</h1>

<p>Written response questions allow teachers to manually score responses, then enter their marks on the response forms before scanning them to have them included in SmarterMarks statistics. An example is given below:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/written_response_example.png" />

<p>For this question, the written response section of the response form might look like this:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/written_response_form.png" />

<p>To build a written response question, select "Written response" in the Type selector:</p>

<img src="<?php echo $imagePath; ?>/written_response.png" />

<p>Written response questions allow for the height of the response area and the names and values of the criteria to be defined here.</p>

<p>The <b>Height</b> field gives the space that will be allowed for students to write their answers. Depending on how the assessment using this question is configured, this space can be included either in the question book, on the answer sheet, or in neither place.</p>

<p>Response <b>Criteria</b> can be edited, added using the "+" button, and removed using the "-" button. Each criterion is assigned a score, so that the maximum score for the whole question is the sum of the scores for its criteria. For example, the question above would be configured as shown below:</p>

<img src="<?php echo $imagePath; ?>/written_response_filled.png" />

<p>This question includes three criteria with a total score of 6. The configuration of your criteria can leave students to know what is expected of them (with one "Score" criterion), or can help guide them, like sharing a rubric.</p>

<p>The <b>Form prompt</b> field allows you to include a prompt for students on the response form . There are three options:</p>
	
<ul>
<li>Selecting <b>None</b> will leave the response area blank, allowing students to use the whole space for their answer.</li>
<li>Selecting <b>Text</b> will display the given text next to the question number. This can be used, for example, to include the question on the response form.</li>
<li>Selecting <b>Image</b> will display an image next to the question number. For example, this can be used to give students a graph to complete.
</ul>

<p>Selecting a new option, or clicking the "Edit" button next to an existing one, will bring up a new dialog that allows you to modify the prompt. For text prompts, the dialog will appear as below:</p>

<img src="<?php echo $imagePath; ?>/written_response_text.png" />

<p>Here, you can enter plain text (without formatting) that will appear next to the question number on generated response forms. For image prompts, the following dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/written_response_image.png" />

<p>Click "Select new" to select an image and "Upload" to upload it and show a preview. The "Height" field is used to set the image height as it will appear on the response form.</p>

<p>From either dialog, click "Done" to return to the question editor.</p>

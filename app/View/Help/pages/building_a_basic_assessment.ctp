<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Building a basic assessment"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#creating_a_question">Creating a question</a></li>
<li><a href="#creating_a_new_assessment">Creating a new assessment</a></li>
<li><a href="#assessing_learning_outcomes">Assessing learning outcomes</a></li>
<li><a href="#versioning_an_assessment">Versioning an assessment</a></li>
<li><a href="#next_steps">Next steps</a></li>
</ol>

<h1 id="creating_a_question">Creating a question</h1>

<p>Before we can build an assessment template, we will need questions to put in that template. Click on the "Question Bank" tab to begin:</p>

<img src="<?php echo $imagePath; ?>/question_bank_tab.png" />

<p>then click on the "New Question" button at the top right:</p>

<img src="<?php echo $imagePath; ?>/questions_index_new.png" />

<p>A new question will be created, and the "Edit Settings" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/question_edit_settings.png" />

<p>The "Edit settings" dialog includes four settings:<p>

<ul>
<li>In the <b>Title</b> field, enter the name under which the question should be saved. This is the name the question will appear under in the question bank tab; it will not appear on assessments.</li>
<li>The <b>Answers given</b> selector allows you to choose whether multiple choice answers will be given with question parts (the default) or in the question's context (as in a matching question, for example). We'll leave this at its default value here.</li>
<li>The <b>Shuffle parts</b> checkbox allows you to control whether the parts in a scenario containing multiple parts are re-ordered when an assessment is shuffled.</li>
<li>Finally, the <b>Share to open question bank</b> checkbox allows you to share this question to the open question bank, or to make it private. For more information on the open question bank, see the <?php echo $this->Html->link('Open question bank', array('controller' => 'help', 'action' => 'view', 'open_question_bank'));?> section.</li>
</ul>

<p>Click "Save" to save the question and continue.</p>

<p>Every question in SmarterMarks is built from components as shown below:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/question_example.png" />

<p>The <b>context</b> gives the background information needed for this set of questions, and is optional. The context is a good place to include large amounts of information that would clutter the question stem, or to include information which is common to several parts.</p>

<p>The <b>parts</b> are the questions associated with this item. A question can have many parts, or just one. Parts can be all of the same type (multiple choice, numeric response, and written response), or of mixed type.</p>

<p>A question is not a section. Questions should be grouped as parts of a single item only if they share a common context. We'll look at building sections containing several questions later on.</p>

<p>To edit the context for this question, click on the "Edit" button in the context pane:</p>

<img src="<?php echo $imagePath; ?>/context_edit_button.png" />

<p>This will open the context editor:</p>

<img src="<?php echo $imagePath; ?>/context_edit.png" />

<p>The editor is a powerful tool that will let you easily:</p>

<ul>
<li>Apply formatting to text, including bold, italics, and underline; superscripts and subscripts; and text centering</li>
<li>Create numbered and bulleted lists in several formats</li>
<li>Build variables into your questions, allowing you to create randomized questions and calculated answers</li>
<li>Add and format tables with visible or invisible borders</li>
<li>Include images, equations, and symbols in your questions</li>
</ul>

<p>For more information on the editor, visit the <?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?> section. In this section, we'll build a simple question including an image. Enter some introductory text, then on a new line click the text centering button (<img style="height:12px; margin:0px 2px;" src="/img/editor/center.svg" />):</p>

<img src="<?php echo $imagePath; ?>/context_edit_filled.png" />

<p>Click on the add image button (<img style="height:14px; margin:0px 2px;" src="/img/editor/image.svg" />) to open the add image dialog:</p>

<img src="<?php echo $imagePath; ?>/add_image.png" />

<p>Click "Select file" to choose a file to upload, and enter a width. Image widths are given as a percentage of the width of container the image is in &mdash; here, the width of the question. Entering a width of 50% gives the context shown below:</p>

<img src="<?php echo $imagePath; ?>/context_filled.png" />

<p>Images can be edited after they have been created without uploading again. To learn more, see the <?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?> section.</p>

<p>Click "Save" to save the context and continue.</p>

<p>The editor for each part is the same as that for the context. To edit the pre-built first part for this question, click on the "Edit" button in the question pane:</p>

<img src="<?php echo $imagePath; ?>/question_edit_button.png" />

<p>This will open the Edit Question dialog:</p>

<img src="<?php echo $imagePath; ?>/question_edit.png" />

<p>Here you can set the question type and a number of settings specific to each question type &mdash; here, whether or not the answers for this multiple choice question will be shuffled when the assessment is shuffled. Six question types are available:</p>

<ul>
<li><?php echo $this->Html->link('Multiple choice', array('controller' => 'help', 'action' => 'view', 'multiple_choice_questions'));?> questions, including regular multiple choice, response table (i/ii) questions, and true/false questions;</li>
<li><?php echo $this->Html->link('Numeric response', array('controller' => 'help', 'action' => 'view', 'numeric_response_questions'));?> questions, including decimal, scientific notation, and selection questions; and</li>
<li><?php echo $this->Html->link('Written response', array('controller' => 'help', 'action' => 'view', 'written_response_questions'));?> questions.
</ul>

<p>For more information on the details of each question type, follow the links above. Here, we will create a simple multiple choice question with the text shown below:</p>

<img src="<?php echo $imagePath; ?>/question_edit_filled.png" />

<p>When you're done, click "Save" to save this part and continue. In this section, we'll build a single-part question. For more information on adding parts to a question, see the <?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?> section.</p>

<p>To add an answer to this question, click the "Add New Answer" button:</p>

<img src="<?php echo $imagePath; ?>/add_answer_button.png" />

<p>This will open the "Edit Answer" dialog:</p>

<img src="<?php echo $imagePath; ?>/answer_edit.png" />

<p>Here, you can enter:</p>

<ul>
<li>an <b>answer</b> to this question, including most of the formatting options available in the context and part editors, as well as variables, images, equations, and symbols;</li>
<li>the <b>value</b>, or number of marks given for this response; and</li>
<li>optionally, the <b>rationale</b> &mdash; the process by which distractors are obtained.</li>
</ul>

<p>Our first answer is shown below:</p>

<img src="<?php echo $imagePath; ?>/answer_edit_filled.png" />

<p>When you're done, click "Save" to save this answer and continue. Other answers can be entered similarly:</p>

<img src="<?php echo $imagePath; ?>/answers_pane.png" />

<p>The "Edit Key" button allows for more complex keys &mdash; for example, requiring students to choose two answers. For more information, check out the <?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?> section. In this example, we'll leave the question as it is.</p>

<p>Questions can be previewed as they are built by clicking on the "Preview" button at the top right of the window:</p>

<img src="<?php echo $imagePath; ?>/question_preview_button.png" />

<p>This will open a question preview in a new tab. At the top of the preview is a rendering of the question much as it will appear on an assessment:</p>

<img src="<?php echo $imagePath; ?>/question_preview_top.png" />

<p>At the bottom of the preview is a summary of the scored responses for the question:</p>

<img src="<?php echo $imagePath; ?>/question_preview_bottom.png" />

<p>To build another question, click on the "New Question" button in the top bar as you did at the start of this section. When you're ready to build an assessment using your questions, continue below!</p>

<h1 id="creating_a_new_assessment">Creating a new assessment</h1>

<p>To begin building an assessment template, click on the "Assessment Templates" tab:</p>

<img src="<?php echo $imagePath; ?>/assessment_templates_tab.png" />

<p>then click on the "New Assessment" button at the top right:</p>

<img src="<?php echo $imagePath; ?>/new_assessment_button.png" />

<p>A new question will be created, and the "Edit Assessment Details" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/assessment_details.png" />

<p>The "Edit Assessment Details" dialog allows you to change a number of formatting options for the assessment. Here, we will set only a few basic options. For information on other assessment options, see the <?php echo $this->Html->link('Assessment options', array('controller' => 'help', 'action' => 'view', 'assessment_options'));?> section.</p>

<p>In the <b>Save As</b> field, enter a name under which this assessment should be saved. This is the name the assessment will appear under in the assessment templates tab; it will not appear on the assessment.</p>

<p>In the <b>Default Title</b> field, enter a starting title for this assessment. When you build versions, you will be given an opportunity to edit this to build the title as it will appear on each version. Your title can have multiple lines, and can contain any characters you like.</p>

<p>Finally, define any <b>name lines</b> you would like to appear at the top-right of the generated response forms.</p>

<p>When you are done, click "Save" to save the assessment and continue.</p>

<p>Click on the "Add Section" button to build the first section:</p>

<img src="<?php echo $imagePath; ?>/section_add_dropdown.png" />

<p>Sections may contain multiple choice, numeric response, or written response questions. It is also possible to create mixed-type sections (see the <?php echo $this->Html->link('Assessment options', array('controller' => 'help', 'action' => 'view', 'assessment_options'));?> section for more information). Here, we will build a multiple choice section. Choose "Multiple choice section" from the list:</p>

<img src="<?php echo $imagePath; ?>/section_add_menu_mc.png" />

<p>This will open the "Section Details" dialog:</p>

<img src="<?php echo $imagePath; ?>/section_edit.png" />

<p>Here, you can show or hide the section title, force the section to start on a new page, and choose whether questions in this section will be shuffled when a new version of the assessment is created. We will leave all of the settings at their default values. Click "Save" to continue.</p>

<p>This will create a new multiple choice section. New items can be either added from the question bank or created and linked directly to the assessment. To add items from the question bank to this section, click "Add Items" and select "Choose from bank":</p>

<img src="<?php echo $imagePath; ?>/add_items_bank.png" />

<p>This will open the Add Items dialog:</p>

<img src="<?php echo $imagePath; ?>/add_items.png" />

<p>Here you can choose items to be added to the section. The dialog is divided into three parts:</p>

<ul>
<li>The panel at the top left allows you to navigate any folders you have built in the Question Bank tab, as well as the open question bank. For more information on the open question bank, see the <?php echo $this->Html->link('Open question bank', array('controller' => 'help', 'action' => 'view', 'open_question_bank'));?> section.</li>
<li>Questions from the selected folder are shown in the panel at the top right. Here you can also search for questions in the current folder and filter by question type. Questions are shown here only if they are of a compatible type (here, multiple choice questions) and have not yet been used in this assessment.</li>
<li>At the bottom, a preview is rendered for the question over which the pointer is currently hovering.</li>
</ul>

<p>Questions in the top right panel are listed by default from least to most recently used, with the date on which student data was uploaded for each given on the right. The sort type and direction can be controlled using the "Sort by" selector and the up and down arrows to its left.</p>

<p>One or more questions can be selected from the panel at the top right by clicking on them; selected questions will be indicated with a checkmark. When you have selected the questions you would like added, click the "Add" button at the bottom right to add them to the assessment.</p>

<p>The assessment builder will now include the added questions:</p>

<img src="<?php echo $imagePath; ?>/assessment_section_display.png" />

<p>On the right is a rendering of the assessment as it is currently built. On the left is an outline of the sections of the assessment, and the questions in the active section. A number of changes can be made easily from here:</p>

<ul>
<li>To <b>select a section</b>, click it in the "Sections" panel on the left.</li>
<li>To <b>open a question</b> for editing in a new tab, click the edit icon (<span class="edit_icon text_icon"></span>) next to its name in the panels on the left, or double-click it in the preview on the right.</li>
<li>To <b>remove a question</b> from the assessment, click the delete icon (<span class="delete_icon text_icon"></span>) next to its name in the panels on the left. Alternatively, hover over the item in the preview and click the item menu button (<span style="color:#aaa; margin:0px 2px;">▾</span>) that appears at the top-right of the item, then choose "Remove item from section".</li>
<li>To <b>rearrange items</b>, drag them within their section in the panel on the left.</li>
<li>To <b>keep items together</b> when the assessment is shuffled, click the "Add Shuffle Break" button next to the "Add Items" button. This will create a new shuffle break at the bottom of the section that can be dragged into place. When items are shuffled, they will not be allowed to cross shuffle breaks.</li>
</ul>

<p>Below, we have moved some questions and added two shuffle breaks:</p>

<img src="<?php echo $imagePath; ?>/assessment_section_modified.png" />

<p>Additional sections, either of the same type or another type, can be added and edited similarly.</p>

<h1 id="assessing_learning_outcomes">Assessing learning outcomes</h1>

<p>Before we build versions of this assessment, we can define learning outcomes that will be reported along with each student's total mark when the assessment is analyzed. To begin, choose "Learning outcomes" in the Views panel on the left:</p>

<img src="<?php echo $imagePath; ?>/learning_goals_button.png" />

<p>Choose a section, then a question, from the panels on the left. The question will be displayed in the preview on the right:</p>

<img src="<?php echo $imagePath; ?>/learning_goals_top.png" />

<p>Click on the "Add New Category" button below this to create a new learning outcome:</p>

<img src="<?php echo $imagePath; ?>/add_category_button.png" />

<p>This will open the "Manage category" dialog:</p>

<img src="<?php echo $imagePath; ?>/manage_category.png" />

<p>Here, you can give the new category a name. For questions with multiple parts, this dialog will also include a set of checkboxes that allow you to select the question parts which are included in the new category.</p>

<p>Click "Save" to save the category and continue. The new category will be shown in the list:</p>

<img src="<?php echo $imagePath; ?>/category_filled.png" />

<p>On the right of each row, buttons allow you to edit the category (<span class="edit_icon text_icon"></span>), delete it (<span class="delete_icon text_icon"></span>), or highlight all questions of this type in the preview (<span class="view_icon text_icon"></span>).</p>

<p>To add an existing category to a question, first select the question, then click on the add icon (<span class="add_icon text_icon"></span>) next to the category you would like to add.</p>

<h1 id="versioning_an_assessment">Versioning an assessment</h1>

<p>To build versions of the assessment, choose "Sections and questions" from the Views panel on the left to return to the assessment builder:</p>

<img src="<?php echo $imagePath; ?>/sections_and_questions_button.png" />

<p>Versions can be generated, previewed, and built using the buttons at the top of the builder:</p>

<img src="<?php echo $imagePath; ?>/version_buttons.png" />

<p>To begin, click the "Shuffle" button to generate a new version. Shuffling will rearrange questions, question parts, and multiple choice responses as selected in the options previously set. It will also regenerate any random or calculated variables.</p>

<p>Before generating the version, preview it using the "Preview" button. This will produce a PDF with the same layout that the final version will have, without creating the assessment version itself. The preview will open in a new tab:</p>

<img src="<?php echo $imagePath; ?>/version_preview.png" />

<p>Examine the preview to ensure it has the formatting you would like. If necessary, return to the assessment builder, shuffle the assessment again, and preview. Once you are satisfied with the formatting, close the preview tab and click "Build" to build this version of the assessment. If the assessment is free of errors, the "Build version" dialog will open:</p>

<img src="<?php echo $imagePath; ?>/build_version.png" />

<p>Any warnings (for example, if no correct answer has been given for a question) will be displayed here. In this dialog, you can edit the default title you specified earlier to make it unique to this version. For example, you may add a version number after the title:</p>

<img src="<?php echo $imagePath; ?>/build_version_filled.png" />

<p>When you are ready to build the version, choose a location for the version and click "Build". In a new tab, you will be forwarded to the settings for your new assessment:</p>

<img src="<?php echo $imagePath; ?>/build_version_settings.png" />

<p>Give the version a name in the "Save As" field, adjust any other settings as required, and click "Save" to finish generating the version. For more information on editing assessment versions and response forms, see the <?php echo $this->Html->link('Building a response form', array('controller' => 'help', 'action' => 'view', 'building_a_response_form'));?> section.</p>

<h1 id="next_steps">Next steps</h1>

<p>Once you have built an assessment version, you are ready to print the question book and response forms, then read in scanned responses from your students. To read more about sharing your assessments with other users, visit the <?php echo $this->Html->link('Sharing with other users', array('controller' => 'help', 'action' => 'view', 'sharing_with_other_users'));?> section. To get started with printing question books and forms, visit the <?php echo $this->Html->link('Printing assessments', array('controller' => 'help', 'action' => 'view', 'printing_assessments'));?> section.  Once you have your response forms back from your students, visit the <?php echo $this->Html->link('Scanning completed forms', array('controller' => 'help', 'action' => 'view', 'scanning_completed_forms'));?> section for more information on scanner settings and uploading your scanned forms.</p>

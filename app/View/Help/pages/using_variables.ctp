<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Using variables"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#overview">Overview</a></li>
<li><a href="#adding_random_variables">Adding random variables</a></li>
<li><a href="#adding_calculated_variables">Adding calculated variables</a></li>
<li><a href="#using_already_defined_variables">Using already-defined variables</a></li>
<li><a href="#editing_variables">Editing variables</a></li>
</ol>

<h1 id="overview">Overview</h1>

<p>The use of randomized and calculated variables allows you to build questions which use different values each time they are shuffled. An example of a question using variables is shown below:</p>

<img src="<?php echo $imagePath; ?>/variables_example_builder.png" />

<p>Here "distance" and "speed" in the context of the question are random variables defined to be within some range. The variable "time" in the answer is a calculated variable, determined from the values of the other two. When the question is generated, specific values are substituted for each variable tag:</p>

<img src="<?php echo $imagePath; ?>/variables_example_preview.png" />

<p>Variables can help you more comprehensively version your assessments. In this section, we cover the building and editing of variables.</p>

<h1 id="adding_random_variables">Adding random variables</h1>

<p>To add a random variable to the context, parts, or answers of a question, open the element for editing and click on the variables button in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_variables.png" />

<p>If variables have already been defined and are listed, click "New variable" at the top of the menu. This will open the "Add variable" dialog:</p>

<img src="<?php echo $imagePath; ?>/editor_variables_edit.png" />

<p>This dialog allows you to define four properties for the random variable:</p>

<ul>
<li>The <b>minimum</b> value that the variable can take;</li>
<li>The <b>maximum</b> value that the variable can take;</li>
<li>The <b>step</b> for this variable; and</li>
<li>The number of <b>decimals or significant digits</b> to which it will be rounded.</li>
</ul>

<p>An example is shown below:</p>

<img src="<?php echo $imagePath; ?>/editor_variables_random.png" />

<p>This variable will take a random value between 15 and 30 with a step of 1 &mdash; so one of 15, 16, 17, &hellip;, 29, or 30, but not 16.2, for example &mdash; and be displayed to 3 significant digits.</p>

<p>When you are done, click "Add" to save the variable and insert it in the editor.</p>

<h1 id="adding_calculated_variables">Adding calculated variables</h1>

<p>To add a calculated variable to the context, parts, or answers of a question, open the element for editing and click on the variables button in the top bar:</p>

<img src="<?php echo $imagePath; ?>/editor_variables.png" />

<p>If variables have already been defined and are listed, click "New variable" at the top of the menu. This will open the "Add variable" dialog:</p>

<img src="<?php echo $imagePath; ?>/editor_variables_edit.png" />

<p>Choose "Calculated" from the Type selector:</p>

<img src="<?php echo $imagePath; ?>/editor_variables_calculated_blank.png" />

<p>Calculated variables are defined by an equation which can include other defined variables, and a number of decimals or significant digits to which the variable should be rounded. In addition, calculated variables can be rounded to the given number of decimals or significant digits immediately after being calculated &mdash; for example, if they will be given in the question and used for subsequent calculations &mdash; or only for display.</p>

<p>For example, the calculation below sets the variable "time" to the value of "distance" divided by the value of "speed":</p>

<img src="<?php echo $imagePath; ?>/editor_variables_calculated_filled.png" />

<p>The equation editor here is similar to that used in the main editor, and all of the controls listed in the Equations part of the <?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?> section work here as well. In addition, the equation editor here has three tools not present in the main equation editor.</p>

<img src="<?php echo $imagePath; ?>/editor_variables_variables.png" />

<p>The variables tool allows you to add already-defined variables to the calculation. Click on a variable from the drop-down menu to insert it into the equation.</p>

<img src="<?php echo $imagePath; ?>/editor_variables_constants.png" />

<p>The constants tool allows you to add common constants. Click on a constant from the drop-down menu to insert it into the equation.</p>

<img src="<?php echo $imagePath; ?>/editor_variables_functions.png" />

<p>Finally, the functions tool allows you to add trigonometric and logarithmic functions. All trigonometric functions work in degrees. Click on a function to insert it into the equation.</p>

<p>When you are done, click "Add" to save the variable and insert it in the editor.</p>

<h1 id="using_already_defined_variables">Using already-defined variables</h1>

<p>Once defined, variables can be used more than once in a question. To insert an existing variable into the text, click on the variables tool in the editor:</p>

<img src="<?php echo $imagePath; ?>/editor_variables_using_button.png" />

<p>If some variables have already been defined, a drop-down menu will appear listing those, along with a "New variable" button at the top.</p>

<img src="<?php echo $imagePath; ?>/editor_variables_using.png" />

<p>To insert a variable at the cursor, click on the variable's name in the list.</p>

<h1 id="editing_variables">Editing variables</h1>

<p>Once created, variables can be modified or deleted if needed. To edit a variable, select "Constants and variables" from the view panel on the left:</p>

<img src="<?php echo $imagePath; ?>/constants_and_variables_button.png" />

<p>The constants and variables view lists all constants and variables which are defined for this question.</p>

<img src="<?php echo $imagePath; ?>/constants_and_variables.png" />

<p>To edit a constant or variable, click on the edit button (<span class="edit_icon text_icon"></span>) in its row. To copy a constant or variable, click on the copy button (<span class="copy_icon text_icon"></span>). To delete a constant or variable, click on the delete button (<span class="delete_icon text_icon"></span>).</p>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Using communities"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#joining_and_leaving_a_community">Joining and leaving a community</a></li>
<li><a href="#viewing_community_policices">Viewing community policies</a></li>
<li><a href="#creating_and_editing_communities">Creating and editing communities</a></li>
<li><a href="#sharing_to_a_community">Sharing to a community</a></li>
</ol>

<h1 id="joining_and_leaving_a_community">Joining and leaving a community</h1>

<p>Membership in communities is obtained through the community's administrators, or through other members if the community allows it. If you are a community member and have received a request for membership, see the <a href="#creating_and_editing_communities">Creating and editing communities</a> section for more information on adding members.</p>

<p>To leave a community, select the community from the folders list in any index:</p>

<img src="<?php echo $imagePath; ?>/community_select.png" />

<p>Then choose "Unsubscribe" from the Community menu:</p>

<img src="<?php echo $imagePath; ?>/community_unsubscribe.png" />

<p>If you are the last administrator in a community that still has other members, you must select another administrator or remove the other members before unsubscribing. If you are the last user in a community, click the "Delete" option in place of "Unsubscribe " to delete the community and its contents.</p>

<h1 id="viewing_community_policies">Viewing community policies</h1>

<p>Communities are highly customizable. For example, a community might allow all users to edit items, or only some; it might allow users to copy items out of the community, or it might not; it might allow all users to view assessment results, or only the user who uploaded the scans. It can be important to know the policies of the communities you are a part of, and to know how to contact the community administrators.</p>

<p>To view a community's policies and other settings, select the community from the folders list in any index then select "View settings" from the Community menu:</p>

<img src="<?php echo $imagePath; ?>/community_view_menu.png" />

<p>This will open the "Community details" dialog:</p>

<img src="<?php echo $imagePath; ?>/community_view_dialog.png" />

<p>Here you can find the list of administrators on the left, and the community policies on the right. Click "Done" to close the window.</p>

<h1 id="creating_and_editing_communities">Creating and editing communities</h1>

<p>To create a new community, click on "Community" and select "New community":</p>

<img src="<?php echo $imagePath; ?>/community_create.png" />

<p>This will open the "Community details" dialog:</p>

<img src="<?php echo $imagePath; ?>/community_edit.png" />

<p>Begin by choosing a community name:</p>

<img src="<?php echo $imagePath; ?>/community_edit_filled.png" />

<p>To add members, click the "Add new members" button:</p>

<img src="<?php echo $imagePath; ?>/community_add_members.png" />

<p>This will open the "Add A Member" dialog:</p>

<img src="<?php echo $imagePath; ?>/community_member_dialog.png" />

<p>Enter the email addresses of SmarterMarks users who you would like to add as members. Users with whom you have not shared before must be entered manually; to add a user you have already shared with, you can click on the "Choose from list" button at the top right and select those users from the list.</p>

<p>When you are done, click "Add" to add these members to the list.</p>

<p>Members can be either administrators or other members:</p>

<ul>
<li><b>Administrators</b> can add other users, change community policies, and can edit and delete anything in a community. There should be only a few administrators even for a large community.</li>
<li><b>Other members</b> are determined by the community policies on the right. These policies allow you to control which users can add, delete, and edit items in the community; whether items can be copied out of the community; and who can view scores for PDFs uploaded to the community.</li>
</ul>

<p>To change members between "administrator" and "other member", use the arrows to the right of their email address in the members list. To remove a member, click on the delete icon (<img style="width:10px; vertical-align:baseline; margin:0px 2px;" src="/img/icons/delete-icon.svg" />) next to their email address.</p>

<p>When you are done making changes to the community, click "Save" to save it.</p>

<h1 id="sharing_to_a_community">Sharing to a community</h1>

<p>Communities can contain response forms, assessment templates, and questions, organized in folders within the community. Items shared to a community are <i>copied</i> there, so that changes made within the community will not affect the originals.</p>

<p>Items can be shared to a community either individually or by folder. To share individual items, select one or more items, then click "Copy items to community" in the "Share" menu:</p>

<img src="<?php echo $imagePath; ?>/community_copy_item.png" />

<p>This will open the "Copy To Community" dialog:</p>

<img src="<?php echo $imagePath; ?>/community_copy_item_dialog.png" />

<p>Choose a community, or a folder within a community, and click "Copy" to copy the selected items.</p>

<p>To copy all items and subfolders in a folder to a community, select the folder from the list on the left and click "Copy folder to community" in the "Share" menu:</p>

<img src="<?php echo $imagePath; ?>/community_copy_folder.png" />

<p>You will not be able to copy the folder while items in the index are selected, so make sure all checkboxes in the index are unchecked before copying folders. Clicking "Copy folder to community" will open the "Copy folder" dialog:</p>

<img src="<?php echo $imagePath; ?>/community_copy_folder_dialog.png" />

<p>In addition to choosing a target folder, you can choose whether to copy the selected folder and its contents, or only the contents. Choose a community, or a folder within a community, and click "Copy" to copy the selected items.</p>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Assessment options"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#editing_assessment_options">Editing assessment options</a></li>
<li><a href="#save_name_and_title">Assessment name and title</a></li>
<li><a href="#setting_name_lines">Setting name lines</a></li>
<li><a href="#page_size_and_margins">Page size and margins</a></li>
<li><a href="#adding_a_logo">Adding a logo</a></li>
<li><a href="#formatting_questions">Formatting questions</a></li>
<li><a href="#setting_assessment_language">Setting assessment language</a></li>
<li><a href="#scoring_options">Scoring options</a></li>
</ol>

<h1 id="editing_assessment_options">Editing assessment options</h1>

<p>To access the options for an assessment template, click on the "Settings" button in the assessment builder:</p>

<img src="<?php echo $imagePath; ?>/assessment_options_button.png" />

<p>This will open the Assessment Details dialog:</p>

<img src="<?php echo $imagePath; ?>/assessment_options_edit.png" />

<h1 id="save_name_and_title">Assessment name and title</h1>

<p>To set the assessment name and title, first bring up the assessment options dialog as described in the <?php echo $this->Html->link('Editing assessment options', array('controller' => 'help', 'action' => 'view', 'editing_assessment_options'));?> section.</p>

<p>In the <b>Save As</b> field, you can edit the name under which the assessment is saved. This is the name the assessment will appear under in the assessment templates tab; it will not appear on the assessment.</p>

<p>In the <b>Default Title</b> field, you can edit the starting title for this assessment. When you build versions, you will be given an opportunity to edit this to build the title as it will appear on each question book and response form. Your title can have multiple lines, and can contain any characters you like.</p>

<p>Click "Save" to save any changes to the assessment options.</p>

<h1 id="setting_name_lines">Setting name lines</h1>

<p>The name lines appear at the top-right of the question book and each page of the response form.</p>

<p>To customize the name lines to require different or additional student information, first bring up the assessment options dialog as described in the <?php echo $this->Html->link('Editing assessment options', array('controller' => 'help', 'action' => 'view', 'editing_assessment_options'));?> section.</p>

<p>The name lines field is at the bottom left of the assessment options dialog. Name lines can be added using the "+" button, or removed using the "-" button. Click "Save" when you are done to save any changes to the assessment options.</p>

<h1 id="page_size_and_margins">Page size and margins</h1>

<p>It can sometimes be helpful to shrink the default margins. To set margins for an assessment, first bring up the assessment options dialog as described in the <a href="#editing_assessment_options">Editing assessment options</a> section.  Each margin can be set in the fields given on the right side.  All margin sizes are in inches.</p>

<p>Click "Save" to save any changes to assessment formatting.</p>

<h1 id="adding_a_logo">Adding a logo</h1>

<p>If you have uploaded a logo (see the <a href="setting_report_preferences">Setting report preferences</a> section for more information), you can choose to include it in the title of this assessment.</p>

<p>To include a logo, first bring up the assessment options dialog as described in the <?php echo $this->Html->link('Editing assessment options', array('controller' => 'help', 'action' => 'view', 'editing_assessment_options'));?> section. Then select the "Show logo on assessment" checkbox on the right side of the dialog. Click "Save" to save any changes to assessment formatting.</p>

<h1 id="formatting_questions">Formatting questions</h1>

<p>To change the formatting of questions in an assessment, first bring up the assessment options dialog as described in the <?php echo $this->Html->link('Editing assessment options', array('controller' => 'help', 'action' => 'view', 'editing_assessment_options'));?> section.</p>

<p>Three options at the bottom right of the dialog control the formatting of questions in this assessment.</p>

<p>The <b>Numbering</b> selector allows you to choose how items and parts are numbered in this assessment. When it is set to "Numbered sequentially", questions will be numbered 1,2,3,&hellip; regardless of whether they are new questions or parts of the same question. An example is shown below:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/assessment_numbered_example.png" />

<p>When the numbering selector is set to "Number by type", each question type (multiple choice, numeric response, and written response) will be numbered separately, and tags will be included before numeric response and written response questions to indicate the type of question.</p>

<p>When the numbering selector is set to "Label parts a, b, c, &hellip;", item parts will be designated by letters, while questions will be numbered, as shown below:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/assessment_letters_example.png" />

<p>The <b>MC format</b> selector allows you to control the formatting of multiple choice questions"</p>
<ul>
<li>When "One column" is selected, multiple choice answers will be given in a single column.</li>
<li>When "Automatic columns" is selected, SmarterMarks will attempt to determine the most compact arrangement for the answers to all multiple choice sections &mdash; for example, putting all answers on one line when possible.</li>
<li>Finally, when "Pyramid shuffled" is selected, any multiple choice question for which shuffled is enabled will force answers to be  in order either of increasing length, or of decreasing length.</li>
</ul>

<p>Finally, the <b>WR responses</b> selector allows you to choose whether the working space given by a question's height is left on the response form, on the question sheet, or in neither place.</p>

<h1 id="setting_assessment_language">Setting assessment language</h1>

<p>A number of elements of the assessment are automatically generated -- for example, the "Use the following questions&hellip;" prompt above each context box. To change the language used for these elements, first bring up the assessment options dialog as described in the <?php echo $this->Html->link('Editing assessment options', array('controller' => 'help', 'action' => 'view', 'editing_assessment_options'));?> section.</p>

<p>The language is set using the "Language" selector at the bottom-right of the settings dialog. SmarterMarks currently supports English and French elements. To request another language, email us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>

<h1 id="scoring_options">Scoring options</h1>

<p>Assessments can be scored either out of the total score of all questions, or as a weighted average of section scores. To change the way an assessment is scored, first bring up the assessment options dialog as described in the <?php echo $this->Html->link('Editing assessment options', array('controller' => 'help', 'action' => 'view', 'editing_assessment_options'));?> section.</p>

<p>Scoring options are found under the "Scoring" tab at the top:</p>

<img src="<?php echo $imagePath; ?>/assessment_scoring_options_tab.png" />

<p>Click on the tab to bring up the scoring options:</p>

<img src="<?php echo $imagePath; ?>/assessment_scoring_options.png" />

<p>Under "Score type", there are two options for scoring:</p>

<ul>
<li><b>Out of total</b>, for which assessments are scored as the total number of points the student earns divided by the total number possible; and
<li><b>Weighted sections</b>, for which each section is scored out of its total, but assessment scores are calculated as the weighted average of section scores.
</ul>

<p>Selecting weighted section scoring will show a control similar to the below if sections have been built:</p>

<img src="<?php echo $imagePath; ?>/assessment_scoring_weighted.png" />

<p>Here, weights can be entered for each section on the left. On the right, bars are shown that indicate the percentage of the assessment score that will be allotted to each section. Weights do not need to add to 100 &mdash; percentages are always calculated using each weight divided by the total of all weights.</p>

<p>Click "Save" to save any changes to scoring.</p>

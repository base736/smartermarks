<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Building a response form"
</script>

<h1>Overview</h1>

<p>In addition to the options discussed in the <a href="building_a_basic_response_form">Building a basic response form</a> section, a number of other simple and advanced options and functions are available in SmarterMarks.  Here, they are divided into four categories:</p>

<ul style="list-style:disc">
<li><?php echo $this->Html->link('Response form options', array('controller' => 'help', 'action' => 'view', 'response_form_options'));?>, including titles, report customization, and formatting.</li>
<li><?php echo $this->Html->link('Multiple choice sections', array('controller' => 'help', 'action' => 'view', 'multiple_choice_sections'));?>, including how to edit a key, omit questions, and use more advanced question styles.</li>
<li><?php echo $this->Html->link('Numeric response sections', array('controller' => 'help', 'action' => 'view', 'numeric_response_sections'));?>, including the use of more advanced question styles.</li>
<li><?php echo $this->Html->link('Versioning response forms', array('controller' => 'help', 'action' => 'view', 'versioning_response_forms'));?> using version maps.</li>
</ul>


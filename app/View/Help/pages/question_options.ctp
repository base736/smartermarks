<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Question options"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#editing_question_options">Editing question options</a></li>
<li><a href="#setting_answer_location">Setting answer location</a></li>
<li><a href="#shuffling_question_parts">Shuffling question parts</a></li>
<li><a href="#sharing_to_open_bank">Sharing to the open question bank</a></li>
</ol>

<h1 id="editing_question_options">Editing question options</h1>

<p>To edit a question, first click on the "Question Bank" tab:</p>

<img src="<?php echo $imagePath; ?>/question_bank_tab.png" />

<p>Click on the question you would like to edit, then select "Edit question" from the "Build" menu:</p>

<img src="<?php echo $imagePath; ?>/edit_question.png" />

<p>Alternatively, double-click the question to begin editing.</p>

<p>Edit the options for a question from anywhere in the editor by clicking on the "Settings" button in the question's title bar:</p>

<img src="<?php echo $imagePath; ?>/question_options_view.png" />

<p>This will open the "Edit settings" dialog:</p>

<img src="<?php echo $imagePath; ?>/question_settings.png" />

<h1 id="setting_answer_location">Setting answer location</h1>

<p>Multiple choice questions can be formatted with their answers in one of two places. For most multiple choice questions, answers should be given with the question parts, as shown below:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/answers_with_parts.png" />

<p>For matching questions, however, it can be useful to give a single set of answers for several parts:</p>

<img style="margin-bottom:10px;" src="<?php echo $imagePath; ?>/answers_in_context.png" />

<p>To change location of a question's answers, first bring up the question settings dialog as described in the <a href="#editing_question_options">Editing question options</a> section.</p>

<p>Using the <b>Answers given</b> selector, choose to place answers either with question parts or in the context. If this field is disabled, you may have question parts already built which are not multiple choice questions. Similarly, once you have chosen to put answers in the context, you will only be able to add multiple choice parts to this question.</p>

<p>Click "Save" to save any changes to question settings.</p>

<p>With answers displayed in the context, the context pane of the builder will appear as below:</p>

<img src="<?php echo $imagePath; ?>/answers_in_context_display.png" />

<p>The number of columns used for answers and whether the answers are shuffled can now be set in the context options, accessed by clicking the "Edit" button at the bottom of the context pane.</p>

<h1 id="shuffling_question_parts">Shuffling question parts</h1>

<p>To turn shuffling of a question's parts on or off, first bring up the question settings dialog as described in the <a href="#editing_question_options">Editing question options</a> section. Shuffling can then be turned on or off using the "Shuffle parts for multi-part items" checkbox.</p>

<p>Click "Save" to save any changes to question settings.</p>

<h1 id="sharing_to_open_bank">Sharing to the open question bank</h1>

<p>The open question bank is a place to share the questions you've created with all SmarterMarks users, and to find proven questions from a wide variety of subject areas. Questions can be shared to the open question bank either in bulk, as described in the <?php echo $this->Html->link('Open question bank', array('controller' => 'help', 'action' => 'view', 'open_question_bank'));?> section, or individually. The settings for each question allow you to share questions and to make them private.</p>

<p>To share a question to the open question bank, or to make it private, first bring up the question settings dialog as described in the <a href="#editing_question_options">Editing question options</a> section. The "Share to open question bank" checkbox can be used to share the question or to make it private. If the checkbox is disabled, and shows in grey, the question has been shared by another user, and only that user will be able to make it private again.</p>

<p>Click "Save" to save any changes to question settings.</p>


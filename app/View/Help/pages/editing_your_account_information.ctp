<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Editing your account information"
</script>

<p>To edit your account information, click on "Settings" under the drop-down at the top right corner of the window:</p>

<img src="<?php echo $imagePath; ?>/my_account.png" />

<p>You will be redirected to the "Your account settings" page. At the top of this page you will find your account settings:</p>

<img src="<?php echo $imagePath; ?>/edit_account_settings.png" />

<p><b>Your Name</b> should give your first and last name for a single-user account, or the group name for a group account.</p>

<p><b>Your School Name</b> should give your full school or organization name.</p>

<p><b>Your Email Address</b> gives the email address you use when logging into SmarterMarks, and which we will use to contact you.  If you change your email address, verification (your response to an email sent by our site) will be required before you can use your account again.</p>

<p>A few times a year, SmarterMarks sends out news to its users regarding new features. Use the <b>Include me in email updates</b> checkbox to control your subscription to these updates.</p>

<p>SmarterMarks gives times throughout the site in your local time zone.  When you first logged in, the site used your IP address to guess your time zone.  You can edit it manually under the <b>Your Time Zone</b> selectors.</p>

<p>If you wish to change your password, click <b>Change password</b> at the bottom right of the frame.</p>

<p>When you are done, click the "Save changes" button at the bottom of the page to save the changes you have made.</p>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Introduction"
</script>

<h1>Our documentation</h1>

<p>On the left, you'll find an index to the full documentation for our site. Click on any link for more information. SmarterMarks can be used to score your existing assessments, or to build, version, and score assessments created on this site.</p>
	
<p>Looking for more information on building assessments using SmarterMarks, or administering assessments online? The video below is a recording of a PD session we presented earlier this year. In the video description, you'll find links to specific topics.</p>

<iframe width="520" height="292" style="border:5px solid white; margin-bottom:10px;" src="https://www.youtube.com/embed/5UZM0OtazG4?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

<p>If you would like to use SmarterMarks with an existing assessment, we can do that too! For more information, start with the <?php echo $this->Html->link('Building a basic response form', array('controller' => 'help', 'action' => 'view', 'building_a_basic_response_form'));?> section, or with the tutorial video below.</p>

<iframe width="520" height="292" style="border:5px solid white; margin-bottom:10px;" src="https://www.youtube.com/embed/8GY-1RjFCrU?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

<h1>Can't find an answer to your question?</h1>

<p>Still stuck? Send us an email at <?php echo $this->Html->link('support@smartermarks.com', 'mailto:support@smartermarks.com', array('style' => 'font-weight: normal; font-size: 14px; color: #713309;'));?>, and we'll do everything we can to straighten things out quickly.</p>

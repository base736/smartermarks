<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Setting report preferences"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#changing_report_layout">Changing report layout</a></li>
<li><a href="#changing_default_settings">Changing default settings</a></li>
</ol>

<h1 id="changing_report_layout">Changing report layout</h1>

<p>To customize the information shown on teacher and student reports, and to enable the use of a logo on generated PDFs, click on "Settings" under the drop-down at the top right corner of the window:</p>

<img src="<?php echo $imagePath; ?>/my_account.png" />

<p>You will be redirected to the "Your account settings" page. Click on the "Show" button next to "Report and form settings" to reveal the settings frame:</p>

<img src="<?php echo $imagePath; ?>/edit_report_settings.png" />

<p>On the left, you can add and remove sections from your teacher reports. Most sections are labeled as they are in the teacher reports. In addition to the information provided by default in our reports, we also produce a number of statistics which are experimental &mdash; that is, statistics which we are currently testing to determine if they might be useful. If you would like to see these statistics, elect "Experimental measures" in the list.</p>

<p>Below this, you can choose how item analyses are sorted &mdash; by question number, average score, or discrimination.</p>

<p>On the right, you will find several settings for the layout of teacher and student reports:</p>
<ul>
	<li><b>Sort outcomes</b> controls the sorting of scores on your defined learning outcomes in teacher and student reports. Outcomes may be sorted alphabetically or by score.</li>
	<li><b>Sort student reports</b> controls the order in which student reports are presented. Student reports can be printed in their scanned order or by the student's name and ID if available. Regardless of how reports are sorted, their order in the scanned PDF will always be included in the report header.</li>
	<li><b>Student report layout</b> controls the layout of student reports on the printed pages. By default, if several reports are included on each page, they will be printed sequentially &mdash; with the second report following the first on the page, for example. This can be inconvenient if you are using a guillotine cutter to separate student reports. Choosing "Guillotine layout" will place the second report <i>behind</i> the first instead, so that reports can be easily cut and stacked.
</ul>

<p>Below these settings, you can also upload a logo to be used (optionally) on response forms. To include a logo, click the checkbox next to "Include a logo in response forms". A dialog box will open allowing you to upload your logo. Logos must be in either PNG or JPG format, and must be smaller than 100 kb in size. Once a logo has been uploaded, you can also choose the height with which it will be printed on response forms.</p>

<p>When you are done, click the "Save changes" button at the bottom of the page to save the changes you have made.</p>

<h1 id="changing_default_settings">Changing default settings</h1>

<p>To change the default settings used when a new assessment is created, click on "Settings" under the drop-down at the top right corner of the window:</p>

<img src="<?php echo $imagePath; ?>/my_account.png" />

<p>You will be redirected to the "Your account settings" page. Click on the "Show" button next to "Default settings" to reveal the default settings frame:</p>

<img src="<?php echo $imagePath; ?>/edit_default_settings.png" />

<p>Here, you can change the values used when new assessments or sections are created. Default settings are arranged under the following headings:</p>

<ul>
	<li><b>Forms and assessments</b>, including page size, margins, layout, and student information;</li>
	<li><b>Student reports</b>, including layout, report type, displayed elements, and outomes levels;</li>
	<li><b>Multiple choice sections</b>, including number of columns and choices;</li>
	<li><b>Numeric response sections</b>, including field style, number of columns and digits, response type, and marking options; and </li>
	<li><b>Written response sections</b>, including student response spaces and scoring criteria.</li>
</ul>

<p>Settings under each heading are labeled and arranged as they are in the respective form settings panel. Updated settings will be used as default values for any new assessments, but will not affect existing assessments.</p>

<p>When you are done, click the "Save changes" button at the bottom of the page to save the changes you have made.</p>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Working with template elements"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#adding_and_editing_a_preamble">Adding and editing a preamble</a></li>
<li><a href="#building_sections">Building sections</a></li>
<li><a href="#adding_items_from_the_question_bank">Adding items from the question bank</a></li>
<li><a href="#creating_new_items_for_an_assessment">Creating new items for an assessment</a></li>
<li><a href="#managing_items">Managing items</a></li>
<li><a href="#creating_shuffle_breaks">Creating shuffle breaks</a></li>
</ol>

<h1 id="adding_and_editing_a_preamble">Adding and editing a preamble</h1>

<p>The preamble is an assessment element that is printed before the first question of the assessment. Preambles might include instructions to students, important equations, or other information. To add a preamble to an assessment template, click the "Add Preamble" button in the assessment builder:</p>

<img src="<?php echo $imagePath; ?>/preamble_add_button.png" />

<p>This will open the "Edit preamble" dialog.</p>

<img src="<?php echo $imagePath; ?>/preamble_edit.png" />

<p>For more information on using the editor, see the <?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?> section. When you are done, click "Save" to save your changes. The preamble will be shown in the assessment preview and in the layout on the left.</p>

<img src="<?php echo $imagePath; ?>/preamble_preview.png" />

<p>To edit a preamble after it has been added, click on the edit button (<span class="edit_icon text_icon"></span>) next to the preamble in the layout. To delete the preamble, click on the delete button (<span class="delete_icon text_icon"></span>).

<h1 id="building_sections">Building sections</h1>

<p>To create a new section, click on the "Add section" dropdown:</p>

<img src="<?php echo $imagePath; ?>/section_add_dropdown.png" />

<p>The section types menu will be displayed:</p>

<img src="<?php echo $imagePath; ?>/section_add_menu.png" />

<p>Click on a section type (or "Mixed-type section" to allow several types) to open the "Section Details" dialog:</p>

<img src="<?php echo $imagePath; ?>/section_edit.png" />

<p>Here you can edit the section title and set three properties for the section:</p>

<ul>
<li>You can choose to <b>show the title as a header</b> at the start of the section.</li>
<li>You can <b>start the section on a new page</b>. This can be particularly useful if you are including the space for written responses in the question booklet rather than on the response form. Starting a written response section on a new page will allow you to more easily separate it from the rest of the assessment later.</li>
<li>You can choose to <b>shuffle questions</b> in this section when a version is generated, subject to the constraints imposed by any shuffle breaks you add.</li>
</ul>

<p>When you are done, click "Save" to save your changes. The new section will be shown in the assessment preview and in the layout on the left.</p>

<img src="<?php echo $imagePath; ?>/section_created.png" />

<p>To edit a section after it has been added, click on the edit button (<span class="edit_icon text_icon"></span>) next to the section in the layout. To delete a section, click on the delete button (<span class="delete_icon text_icon"></span>).</p>

<h1 id="adding_items_from_the_question_bank">Adding items from the question bank</h1>

<p>To add items to a section from your question bank, click on the section in the layout on the left, click the "Add Items" menu at the bottom of the section, and select "Choose from bank":</p>

<img src="<?php echo $imagePath; ?>/add_items_bank.png" />

<p>This will open the Add Items dialog:</p>

<img src="<?php echo $imagePath; ?>/add_items.png" />

<p>Here you can choose items to be added to the section. The dialog is divided into three parts:</p>

<ul>
<li>The panel at the top left allows you to navigate any folders you have built in the Question Bank tab, as well as the open question bank. For more information on the open question bank, see the <?php echo $this->Html->link('Open question bank', array('controller' => 'help', 'action' => 'view', 'open_question_bank'));?> section.</li>
<li>Questions from the selected folder are shown in the panel at the top right. Here you can also search for questions in the current folder and filter by question type. Questions are shown here only if they are of a compatible type (here, multiple choice questions) and have not yet been used in this assessment.</li>
<li>At the bottom, a preview is rendered for the question over which the pointer is currently hovering.</li>
</ul>

<p>Questions in the top right panel are listed by default from least to most recently used, with the date on which student data was uploaded for each given on the right. The sort type and direction can be controlled using the "Sort by" selector and the up and down arrows to its left.</p>

<p>One or more questions can be selected from the panel at the top right by clicking on them; selected questions will be indicated with a checkmark. When you have selected the questions you would like added, click the "Add" button at the bottom right to add them to the assessment.</p>

<p>The assessment builder will now include the added questions:</p>

<img src="<?php echo $imagePath; ?>/assessment_section_display.png" />

<h1 id="creating_new_items_for_an_assessment">Creating new items for an assessment</h1>

<p>To create new items and link them directly to an assessment section, click on the section in the layout on the left, click the "Add Items" menu at the bottom of the section, and select "Create new item":</p>

<img src="<?php echo $imagePath; ?>/add_items_create.png" />

<p>A new question will be created in a new tab, and the "Edit Settings" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/question_edit_settings.png" />

<p>Modify any settings as required, then click "Save" to add the question to the assessment. Any changes made to this question will now be reflected in the assessment that's open in your original tab. To add more questions to the same section, click the "New Question" button in the top bar of the new tab.</p>

<h1 id="managing_items">Managing items</h1>

<p>To edit a question in an assessment, click on the edit button (<span class="edit_icon text_icon"></span>) next to the section in the layout. The question will be opened in a new tab. Any changes made to the question should be reflected in the original assessment tab &mdash; when you are done editing the question, close the new tab to return to the assessment builder.</p>
	
<p>To delete a section, click on the delete button (<span class="delete_icon text_icon"></span>) next to the question.

<p>Questions that have been added to an assessment can also be managed using the question's context menu. When your mouse is hovering over a question in the preview, a disclosure button appears:</p>

<img src="<?php echo $imagePath; ?>/question_hover_disclosure.png" />

<p>Click on the disclosure button to display the context menu:</p>

<img src="<?php echo $imagePath; ?>/question_hover_menu.png" />

<p>Here, you can:</p>

<ul>
<li><b>Edit the item</b> in a new tab, as described above.</li>
<li><b>Replace the item from a question bank</b>. The item will be replaced with a selected item, optionally keeping any attached outcomes if possible.</li>
<li><b>Replace the item with a new copy</b>. This will create a new copy of the question in your Home folder, and link the copy to this assessment. Changes made through the assessment, or to the new copy, will not be reflected in the original question.</li>
<li><b>Remove the item</b> from this section.</li>
</ul>

<p>Click anywhere outside of the menu to hide the menu.</p>

<h1 id="creating_shuffle_breaks">Creating shuffle breaks</h1>

<p>Shuffle breaks can be used to control the shuffling of questions within a section. When questions are shuffled, they will not cross shuffle breaks. To add a shuffle break to a section, click the "Add Shuffle Break" button in the layout:</p>

<img src="<?php echo $imagePath; ?>/shuffle_break_button.png" />

<p>A new shuffle break will be created at the end of the section:</p>

<img src="<?php echo $imagePath; ?>/shuffle_break_created.png" />

<p>Drag the shuffle break within the section to place it where it is required.</p>

<img src="<?php echo $imagePath; ?>/shuffle_break_moved.png" />

<p>To delete a shuffle break, click on the delete button (<span class="delete_icon text_icon"></span>) to the right of the shuffle break.</p>

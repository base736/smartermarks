<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Forms from class lists"
</script>

<h1>Forms from class lists</h1>

<p>In addition to generic response forms, SmarterMarks can build a set of response forms corresponding to a class list. There are two ways to generate response forms from a class list.</p>

<p>If you are still editing your assessment, click the "Print" button at the top right and choose "From class list" from the menu:</p>

<img src="<?php echo $imagePath; ?>/preview_class_list.png" />

<p>Alternatively, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select an assessment and click on "From class list" under the "Print" menu to generate a response form:</p>

<img src="<?php echo $imagePath; ?>/pdf_from_class_list.png" />

<p>In both cases, you will be redirected to the upload page:</p>

<img src="<?php echo $imagePath; ?>/upload_class_list.png" />

<p>Here you can enter or upload your class list.  There are three ways to do this:</p>

<ul style="list-style:disc">
<li>Enter the list manually in the text field marked "Class list".</li>
<li>Copy the class list from another file and paste it in the "Class list" text field.  This has the advantage that you can re-use the same class list for future assessments.</li>
<li>Click "Select file" to choose a text file containing your class list, or drag the class list to the upload window, then press "Upload".</li>
</ul>

<p>Your class list can contain student names, IDs, or both. SmarterMarks will do its best to find each, ignoring any text placed in parentheses. Once you are satisfied with the class list as it appears in the "Class list" text field, click "Submit" to begin generating the response forms.</p>

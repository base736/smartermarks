<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Learning outcomes"
</script>

<h1>Adding and editing learning outcomes</h1>

<p>To add or edit learning outcomes for an assessment, choose "Learning outcomes" from the view pane at the top left:</p>

<img src="<?php echo $imagePath; ?>/learning_goals_button.png" />

<p>Choose a section, then a question, from the panels on the left. The question will be displayed in the preview on the right:</p>

<img src="<?php echo $imagePath; ?>/learning_goals_top.png" />

<p>Click on the "Add New Category" button below this to create a new learning outcome:</p>

<img src="<?php echo $imagePath; ?>/add_category_button.png" />

<p>This will open the "Manage category" dialog:</p>

<img src="<?php echo $imagePath; ?>/manage_category.png" />

<p>Here, you can give the new category a name. For questions with multiple parts, this dialog will also include a set of checkboxes that allow you to select the question parts which are included in the new category.</p>

<p>Click "Save" to save the category. The new category will be shown in the list:</p>

<img src="<?php echo $imagePath; ?>/category_filled.png" />

<p>To edit a category after it has been added, click the edit button (<span class="edit_icon text_icon"></span>) to the right of the category. To delete a category, click the delete button (<span class="delete_icon text_icon"></span>)

<p>To add an existing category to a question, first select the question, then click on the add icon (<span class="add_icon text_icon"></span>) next to the category you would like to add:</p>

<img src="<?php echo $imagePath; ?>/learning_outcome_add.png" />

<p>The view button (<span class="view_icon text_icon"></span>) allows you to highlight all questions which belong to a particular learning outcome. Clicking on the view button for an outcome will return you to the assessment preview with all questions of that type highlighted.</p>

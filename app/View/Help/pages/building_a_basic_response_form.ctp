<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Building a basic response form"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#creating_a_new_assessment">Creating a new response form</a></li>
<li><a href="#building_a_multiple_choice_section">Building a multiple choice section</a></li>
<li><a href="#building_a_numeric_response_section">Building a numeric response section</a></li>
<li><a href="#assessing_specific_learning_goals">Assessing specific learning goals</a></li>
<li><a href="#next_steps">Next steps</a></li>
</ol>

<h1 id="creating_a_new_assessment">Creating a new response form</h1>

<p>In this section, we'll walk through the creation of a response form for an assessment that was not built in SmarterMarks. For an introduction to building assessments in SmarterMarks, see the <?php echo $this->Html->link('Building a basic assessment', array('controller' => 'help', 'action' => 'view', 'building_a_basic_assessment'));?> section. For information on editing response forms generated with a SmarterMarks assessment, see the <?php echo $this->Html->link('Building a response form', array('controller' => 'help', 'action' => 'view', 'building_a_response_form'));?> section.</p>

<p>To begin building a new response form, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>then click on the "New Form" button at the top right:</p>

<img src="<?php echo $imagePath; ?>/assessments_index_new.png" />

<p>A new response form will be created, and the "Edit Assessment Details" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/new_assessment.png" />

<p>The "Edit Assessment Details" dialog allows you to change a number of formatting options for the response form. Here, we will set only a few basic options. For information on other assessment options, see the <?php echo $this->Html->link('Response form options', array('controller' => 'help', 'action' => 'view', 'response_form_options'));?> section.</p>

<p>In the <b>Save As</b> field, enter a name under which this response form should be saved. This is the name the form will appear under in the Forms and Versions tab; it will not appear on the response forms or analyses.</p>

<p>In the <b>Assessment Title</b> field, enter the title as it should appear on student response forms. Your title can have multiple lines, and can contain any characters you like.</p>

<p>Finally, select what (if any) student information will be gathered using bubble fields on the response form.  Student name and ID fields are available, with customizable labels and sizes.

<p>Click "Save" to save the assessment and continue.</p>

<h1 id="building_a_multiple_choice_section">Building a multiple choice section</h1>

<p>To add a multiple choice section to a page, click the "Add Section" drop-down on the desired page and select "Multiple choice section":</p>

<img src="<?php echo $imagePath; ?>/add_mc_section.png" />

<p>A new multiple choice section will be created, and the "MC Section Details" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/new_mc_section.png" />

<p>As with the assessment options, we will set up only a basic multiple choice section here. In addition to the parameters listed in the dialog above, it is possible to set up multiple choice questions with more than one scored response, or with a response that requires more than one choice ("a and b", for example), and to customize question numbering. For information on these and other options for multiple choice sections, see the <?php echo $this->Html->link('Multiple choice sections', array('controller' => 'help', 'action' => 'view', 'multiple_choice_sections'));?> section.</p>

<p>In the <b>Number of Questions</b> field, enter the number of multiple choice questions to be included in this section.  A grid of boxes will appear below for entering the key for this section.  To enter your key, start at the first box and enter the letter for each response. As you enter responses, the cursor will move automatically to the next box.</p>

<img src="<?php echo $imagePath; ?>/new_mc_section_filled.png" />

<p>Answers do not need to be entered before response forms are generated.</p>

<p>When you are done, click "Save" to save the section and continue.</p>

<p>The settings for a section, and for multiple choice sections the key, can be edited after the section has been built by clicking on the "Edit" button at the bottom of the "Selected MC Section" pane:</p>

<img src="<?php echo $imagePath; ?>/mc_section_edit_button.png" />

<h1 id="building_a_numeric_response_section">Building a numeric response section</h1>

<p>Numeric response sections can include decimal, scientific notation, and selection questions (more detail on these below). To add a numeric response section to a page, click the "Add Section" drop-down on the desired page and select "Numeric response section":</p>

<img src="<?php echo $imagePath; ?>/add_nr_section.png" />

<p>A new numeric response section will be created, and the "NR Section Details" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/new_nr_section.png" />

<p>As with the multiple choice section, a number of options are available that will not be discussed here. For information on these options, see the <?php echo $this->Html->link('Numeric response sections', array('controller' => 'help', 'action' => 'view', 'numeric_response_sections'));?> section.</p>

<p>In the <b>Number of Questions</b> field, enter the number of numeric response questions to be included in this section. A table will appear below which can be used to easily set up basic numeric response questions:</p>

<img src="<?php echo $imagePath; ?>/new_nr_section_questions.png" />

<p>Question details do not need to be set before response forms are generated.</p>

<p>When you are done, click "Save" to save the section and continue.</p>

<p>The settings for a numeric response section can be edited as they were for a multiple choice section, by clicking the "Edit" button for the section. In addition, numeric response questions can be edited individually after they are created to set other options. Click on the <span class="edit_icon text_icon"></span> icon next to a question to edit its details:</p>

<img src="<?php echo $imagePath; ?>/nr_question_edit.png" />

<p>The "Manage Question" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/nr_question_decimal.png" />

<p>In the example above, we have selected a decimal-response question with an answer of "12.5".  A number of simple and more advanced options are available; for information on numeric response sections, see the <?php echo $this->Html->link('Numeric response sections', array('controller' => 'help', 'action' => 'view', 'numeric_response_sections'));?> section.</p>

<p>Once you have finished editing a question's details, click "Save" to save it.</p>

<p>Once entered, numeric response questions may be edited or deleted using the icons to the left of each question. Edit a question by clicking on the edit icon (<span class="edit_icon text_icon"></span>), or delete it by clicking on the delete icon (<span class="delete_icon text_icon"></span>).</p>
<p>Additional questions can be added after a section has been created by clicking "Add New Question" at the bottom of the Questions pane:</p>

<img src="<?php echo $imagePath; ?>/add_new_question.png" />

<h1 id="assessing_specific_learning_goals">Assessing specific learning outcomes</h1>

<p>SmarterMarks can assess students not only on their overall score, but also on how well they understand specific learning outcomes.  Questions may be included in one learning outcome, several learning outcomes, or none at all. Learning outcomes are optional, and can be added after an assessment has been completed by students.</p>

<p>To begin building learning outcomes, select a section from the panel on the left. Scroll to the bottom of the window, and click on "Add New Outcome":</p>

<img src="<?php echo $imagePath; ?>/category_add.png" />

<p>The "Manage Learning Outcome" dialog will appear.  Enter a name for this learning outcome and select any questions in this section which should be included. If you wish to add a stretch of many questions, click on the first question, then shift-click on the last.</p>

<img src="<?php echo $imagePath; ?>/new_category_filled.png" />

<p>When you are done, click "Save" to save the learning outcome. Learning outcomes may be edited and deleted using the "Actions" icons. Once a learning outcome has been specified in one section, it will appear in every section. Click the <span class="add_icon text_icon"></span> icon next to a learning outcome to add it to the current section.</p>

<h1 id="next_steps">Next steps</h1>

<p>Once you have built an assessment, you are ready to print response forms, then read in scanned responses from your students. To read more about creating new versions of your assessment, or sharing it with other users, visit the <?php echo $this->Html->link('Copying, sharing, and versioning assessments', array('controller' => 'help', 'action' => 'view', 'copying_sharing_and_versioning_assessments'));?> section. To get started with printing forms, visit the <?php echo $this->Html->link('Printing assessments', array('controller' => 'help', 'action' => 'view', 'printing_assessments'));?> section.  Once you have your response forms back from your students, visit the <?php echo $this->Html->link('Scanning completed forms', array('controller' => 'help', 'action' => 'view', 'scanning_completed_forms'));?> section for more information on scanner settings and uploading your scanned forms.</p>

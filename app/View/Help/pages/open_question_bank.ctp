<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="The open question bank"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#browsing_open">Browsing the open bank</a></li>
<li><a href="#using_questions">Using questions from the open bank</a></li>
<li><a href="#adding_questions">Adding questions to the open bank</a></li>
</ol>

<h1 id="browsing_open">Browsing the open bank</h1>

<p>The open question bank is a place to share the questions you've created with all SmarterMarks users, and to find proven questions from a wide variety of subject areas. Questions in the open question bank have been contributed by other SmarterMarks users, and are backed by classroom statistics, so that they can be sorted according to which most effectively reflect student knowledge of the subject.</p>

<p>The open question bank can be browsed from the main question index. To access the open bank, first click on the "Question Bank" tab:</p>

<img src="<?php echo $imagePath; ?>/question_bank_tab.png" />

<p>Select the open question bank from the folder list on the left:</p>

<img src="<?php echo $imagePath; ?>/open_bank_folder_index.png" />

<p>then click on the "Search" button:</p>

<img src="<?php echo $imagePath; ?>/search_button_index.png" />

<p>Enter the term you'd like to search for, then hit enter to search:</p>

<img src="<?php echo $imagePath; ?>/search_filled_index.png" />

<p>Matching questions from the open question bank will be shown in the panel on the right, listed from the highest quality (questions that have a consistent record of accurately reflecting student knowledge) to the lowest:<p>

<img src="<?php echo $imagePath; ?>/search_results_index.png" />

<p>To view a question, select the question and click "View":</p>

<img src="<?php echo $imagePath; ?>/search_view.png" />

<p>To save one or more questions to your own folders, where you'll be able to edit them, select the questions and click "Copy":</p>

<img src="<?php echo $imagePath; ?>/search_copy.png" />

<p>This will open the Copy Item dialog:</p>

<img src="<?php echo $imagePath; ?>/search_copy_dialog.png" />

</p>Choose a folder and click "Copy" to create a copy of the selected questions in that folder. Items from the open question bank can be added to an assessment from your own folders, or from the open bank directly as described in the next section.</p>

<h1 id="using_questions">Using questions from the open bank</h1>

<p>To add questions from the open bank to an assessment, click "Add Items" and select "Choose from bank":</p>

<img src="<?php echo $imagePath; ?>/add_items_bank.png" />

<p>This will open the Add Items dialog:</p>

<img src="<?php echo $imagePath; ?>/add_items.png" />

<p>Select the open question bank from the folder list on the left:</p>

<img src="<?php echo $imagePath; ?>/open_bank_folder.png" />

<p>then click on the "Search" button on the right:</p>

<img src="<?php echo $imagePath; ?>/search_button.png" />

<p>Enter the term you'd like to search for, then hit enter to search:</p>

<img src="<?php echo $imagePath; ?>/search_filled.png" />

<p>Matching questions from the open question bank will be shown in the panel on the right, listed from the highest quality (questions that have a consistent record of accurately reflecting student knowledge) to the lowest:<p>

<img src="<?php echo $imagePath; ?>/search_results.png" />

<p>As with questions from your own folders, select questions from the list, then enter another search to find more questions. When you are done, click "Add" to add the questions selected.</p>

<p>As with communities, when a question is added from the open question bank then edited, changes will be reflected only in your copy of the question, so you can modify any questions you add to suit your needs without making changes to the original.</p>

<h1 id="adding_questions">Adding questions to the open bank</h1>

<p>If you've found the open question bank useful, or if you're having a hard time finding questions for your subject area, we'd love to have you contribute to the bank! Individual qestions can be added individually, as described in the <?php echo $this->Html->link('Question options', array('controller' => 'help', 'action' => 'view', 'question_options'));?> section.</p>

<p>The "Manage open questions" tool helps you add folders of questions to the open bank, or to set a policy that shares the questions you create in the future. To access the tool, first click on the "Question Bank" tab:</p>

<img src="<?php echo $imagePath; ?>/question_bank_tab.png" />

<p>Click on "Share", then "Manage open questions":</p>

<img src="<?php echo $imagePath; ?>/manage_open_questions.png" />

<p>This will open the Manage Open Questions tool:</p>

<img src="<?php echo $imagePath; ?>/manage_open_questions_top.png" />

<p>To share questions you create in the future to the open question bank by default, or to reverse that change, click the checkbox in the "Sharing preferences" panel, then click "Save changes:":</p>

<img src="<?php echo $imagePath; ?>/manage_open_save_changes.png" />

<p>To share all questions you have already created, click the "Share now" button in the "Quick share" panel:</p>

<img src="<?php echo $imagePath; ?>/manage_open_share_now.png" />

<p>The "Share folders" panel at the bottom allows you to share other folders, or to fine-tune what you have already shared:</p> 

<img src="<?php echo $imagePath; ?>/manage_open_questions_bottom.png" />

<p>To share the contents of a folder and its subfolders, or to make them private, select the folder from the list at the left:</p>

<img src="<?php echo $imagePath; ?>/manage_open_list.png" />

<p>A list of questions in the selected folders will be shown on the right. Questions are tagged according to whether they are private ( <span style="width:15px; height:15px; vertical-align:text-bottom; display:inline-block; background-image:url('/img/icons/question_private.png'); background-size:cover;"></span> ) or open ( <span style="width:15px; height:15px; vertical-align:text-bottom; display:inline-block; background-image:url('/img/icons/question_open.png'); background-size:cover;"></span> ). Open questions that show a lock ( <span style="width:22px; height:15px; vertical-align:text-bottom; display:inline-block; background-image:url('/img/icons/question_locked.png'); background-size:cover;"></span> ) have been shared by another user, and can be made private only by that user.</p>

<p>To change the policy (private or open) for the questions shown, choose the new policy using the controls below the folder list, optionally check the "Include only questions I created" checkbox to restrict changes to questions you created, then click "Update questions" to make the requested changes:</p>

<img src="<?php echo $imagePath; ?>/manage_open_update_questions.png" />

<p>Your changes will be reflected in the question list on the right:</p>

<img src="<?php echo $imagePath; ?>/manage_open_opened.png" />

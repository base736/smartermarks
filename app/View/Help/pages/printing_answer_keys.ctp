<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Answer keys"
</script>

<h1>Answer keys</h1>

<p>SmarterMarks generate a PDF answer key for your assessments, presented as a filled-in response form. There are two ways to generate answer keys.</p>

<p>If you are still editing your assessment, click the "Print" button at the top right and choose "Answer key" from the menu:</p>

<img src="<?php echo $imagePath; ?>/preview_answer_key.png" />

<p>Alternatively, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select an assessment and click on "Answer key" under the "Print" menu to generate a response form:</p>

<img src="<?php echo $imagePath; ?>/print_answer_key.png" />

<p>Your answer key will be generated in a new tab.</p>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Building an assessment template"
</script>

<h1>Overview</h1>

<p>SmarterMarks' assessment builder allows you to easily build and version assessments, letting you focus on learning outcomes rather than formatting. In this section, the following are detailed:</p>

<ul style="list-style:disc">
<li><?php echo $this->Html->link('Assessment options', array('controller' => 'help', 'action' => 'view', 'assessment_options'));?>, including the organization and appearance of assessments.</li>
<li><?php echo $this->Html->link('Working with template elements', array('controller' => 'help', 'action' => 'view', 'working_with_template_elements'));?>, including the preamble, sections, items, and shuffle breaks.</li>
<li><?php echo $this->Html->link('Learning outcomes', array('controller' => 'help', 'action' => 'view', 'learning_outcomes'));?>, including adding, editing, and viewing.</li>
<li><?php echo $this->Html->link('Building a version', array('controller' => 'help', 'action' => 'view', 'building_a_version'));?>, including shuffling, previewing, and versioning.</li>
</ul>


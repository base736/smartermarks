<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Generic response forms"
</script>

<h1>Generic response forms</h1>

<p>There are two ways to generate a generic response form for your assessments.</p>

<p>If you are still editing your assessment, click the "Print" button at the top right and choose "Generic form" from the menu:</p>

<img src="<?php echo $imagePath; ?>/preview_generic.png" />

<p>A response form PDF will open in a new tab. Alternatively, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select an assessment and click on "Generic form" under the "Print" menu to generate a response form:</p>

<img src="<?php echo $imagePath; ?>/generic_pdf.png" />

<p>Your response form will be generated in a new tab.</p>

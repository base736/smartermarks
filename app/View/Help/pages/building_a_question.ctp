<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Building a question"
</script>

<h1>Overview</h1>

<p>SmarterMarks offers a wide range of options for customizing questions, in addition to those discussed in the <?php echo $this->Html->link('Building a basic assessmnt', array('controller' => 'help', 'action' => 'view', 'building_a_basic_assessment'));?> section. In this section, the following are explored in greater detail:</p>

<ul style="list-style:disc">
<li><?php echo $this->Html->link('Question options', array('controller' => 'help', 'action' => 'view', 'question_options'));?>, including answer location and control over the shuffling of question parts.</li>
<li><?php echo $this->Html->link('Working with question elements', array('controller' => 'help', 'action' => 'view', 'working_with_question_elements'));?>, including editing the question context and modifying question parts, answers, and keys.</li>
<li><?php echo $this->Html->link('Multiple choice questions', array('controller' => 'help', 'action' => 'view', 'multiple_choice_questions'));?>, including the building of regular multiple choice, response table, and true / false questions.</li>
<li><?php echo $this->Html->link('Numeric response questions', array('controller' => 'help', 'action' => 'view', 'numeric_response_questions'));?>, including the building of decimal, scientific, and selection questions.</li>
<li><?php echo $this->Html->link('Written response questions', array('controller' => 'help', 'action' => 'view', 'written_response_questions'));?>.</li>
<li><?php echo $this->Html->link('Using the editor', array('controller' => 'help', 'action' => 'view', 'using_the_editor'));?>, including text formatting and alignment, building lists and tables, and adding images, equations, and symbols.</li>
<li><?php echo $this->Html->link('Using variables', array('controller' => 'help', 'action' => 'view', 'using_variables'));?>, including the use of random and calculated variables.</li>
</ul>


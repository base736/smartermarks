<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Sending a copy"
</script>

<h1 id="sending_a_copy">Sending a copy</h1>

<p>Copies of assessments, assessment templates, and questions can be sent to other users. When a copy is sent, any changes to the copy will not be reflected in the original &mdash; that is, the copy is independent of the original. Below, the process of copying an assessment is outlined; the same menus appear in the "Assessment Templates" and "Questions" tabs.</p>

<p>To send a copy of one or more assessments, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Use the checkboxes to the left of each item to select the assessments you want to send, then select "Send a Copy" from the Share menu:</p>

<img src="<?php echo $imagePath; ?>/send_a_copy.png" />

<p>The "Send a Copy" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/send_copy_dialog.png" />

<p>Enter the email addresses of SmarterMarks users to whom you would like to send a copy of the selected assessments. Users with whom you have not shared before must be entered manually; to send a copy to a user you have already shared with, you can click on the "Choose from list" button at the top right and select those users from the list.</p>

</p>Click "Send" to send a copy of each selected assessment to the users chosen.</p>

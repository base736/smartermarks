<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Numeric response sections"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#adding_a_numeric_response_section">Adding a numeric response section</a></li>
<li><a href="#section_options">Section options</a></li>
<li><a href="#editing_and_deleting_numeric_response_questions">Editing and deleting numeric response questions</a></li>
<li><a href="#basic_question_options">Basic question options</a></li>
<li><a href="#changing_question_values">Changing question values</a></li>
<li><a href="#defining_multiple_scored_answers">Defining multiple scored answers</a></li>
</ol>

<h1 id="adding_a_numeric_response_section">Adding a numeric response section</h1>

<p>To edit a response form, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment in the Forms and Versions tab and click on "Edit form" under the "Build" menu:</p>

<img src="<?php echo $imagePath; ?>/edit_form.png" />

<p>To add a numeric response section to a page, click the "Add Section" drop-down on the desired page and select "Numeric response section":</p>

<img src="<?php echo $imagePath; ?>/add_nr_section.png" />

<p>A new numeric response section will be created, and the "NR Section Details" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/new_nr_section.png" />

<p>The dialog is divided into two parts. For information on the options on the left, see the <a href="#section_options">section options</a> section below. In addition to these, when a new numeric response section is created, the "NR Section Details" dialog shows a <b>Number of Questions</b> field on the right. When the number of numeric response questions to be included is entered in this field, a table will appear below:</p>

<img src="<?php echo $imagePath; ?>/new_nr_section_questions.png" />

<p>Use this table to quickly build simple numeric response questions. Question details do not need to be set before response forms are generated</p>

<p>Click "Save" to save the section and continue.</p>

<h1 id="section_options">Section options</h1>

<p>To edit a response form, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment in the Forms and Versions tab and click on "Edit form" under the "Build" menu:</p>

<img src="<?php echo $imagePath; ?>/edit_form.png" />

<p>View the options for a numeric response section from anywhere in the editor by clicking  on the section's title in the left panel:</p>

<img src="<?php echo $imagePath; ?>/nr_section_options_view.png" />

<p>To edit the options, click on the "Edit Now" button:</p>

<img src="<?php echo $imagePath; ?>/nr_section_options_edit.png" />

<p>A number of options can be changed from the dialog that appears.</p>

<p>The <b>Section Title</b> field gives the title to appear at the top of this section on response forms.</p>

<p>The <b>Start Numbering At</b> field gives the starting point for question numbering in this section.</p>

<p>The <b>Number of Columns</b> field specifies the number of columns into which questions should be arranged.</p>

<p>The <b>Number of Digits</b> field gives the number of digits (including decimals) for each question in this section.</p>

<p>Click "Save" to save any changes to the section options.</p>

<h1 id="editing_and_deleting_numeric_response_questions">Editing and deleting numeric response questions</h1>

<p>To edit a numeric response question, click the <span class="edit_icon text_icon"></span> icon under "Actions" next to the question:</p>

<img src="<?php echo $imagePath; ?>/nr_question_edit.png" />

<p>To delete the question, click the <span class="delete_icon text_icon"></span> icon.</p>

<h1 id="basic_question_options">Basic question options</h1>

<p>To edit the basic options for a numeric response question, click the <span class="edit_icon text_icon"></span> icon under "Actions" next to the question as shown in the <a href="#editing_and_deleting_numeric_response_questions">Editing and deleting numeric response questions</a> section. This will open the "Manage Question" dialog. If the question you are editing has not yet been given a type, the dialog will appear as below:</p>

<img src="<?php echo $imagePath; ?>/nr_question_notype.png" />

<p>The <b>Type</b> selector chooses the type of question.  SmarterMarks supports four question types:</p>

<ul style="list-style:disc">
<li style="margin-bottom: 10px;"><i>Decimal</i> questions have a response specified in decimal notation (for example, "48.2"), though they do not need to include a decimal point, and cannot have a decimal point as their first or last character. Decimal responses can be negative only if the NR field style is set to "Allow negative answers" in settings. If a response has fewer digits than the space given, SmarterMarks will accept the answer whether it is placed to the left, to the right, or elsewhere in the response field.</li>
<li style="margin-bottom: 10px;"><i>Scientific</i> questions have a response specified in scientific notation.  The associated question may, for example, state that "The force on the astronaut is <i>a</i>.<i>bc</i>&times;10<sup><i>d</i></sup> N. The values of <i>a</i>, <i>b</i>, <i>c</i> and <i>d</i> are ____, ____, ____ and ____."  Here, if the student gets an answer of 8.21&times;10<sup>4</sup> N, he or she would enter "8214" in the response form. Scientific notation responses are entered into the key in scientific notation (for example, the previous answer would be keyed as "8.21e4").  They may have more than one exponent digit (for example, <i>a</i>.<i>b</i>&times;10<sup><i>cd</i></sup>) but, like decimal responses, must not be negative.</li>
<li style="margin-bottom: 10px;"><i>Fraction</i> questions allow students to enter fractional responses (for example, "3/5") and automatically include a "/" symbol in the response form.</li>
<li style="margin-bottom: 10px;"><i>Selection</i> questions ask students to select items from a list, placing their responses in the first column, second column, and so on. In addition to digits, selection questions can include the wildcard "?" to indicate that any digit is acceptable.</li>
</ul>

<p>Once a type has been selected, a number of options unique to each question type will become available. For example, the options for a decimal question are shown below:</p>

<img src="<?php echo $imagePath; ?>/nr_question_decimal.png" />

<p>By default, questions are numbered starting at the question number given and proceeding in sequence from there (for example, "21, 22, 23"). To customize question numbers, breaking a question into parts 'a', 'b', and so on, select the desired question label from the <b>Question</b> drop-down at the top left.</p>

<p>To omit this question, choose "Omit question" in the <b>Scoring</b> drop-down.</p>

<p>The <b>Answer</b> field should contain the correct response for this question in a format appropriate to the question type (see the list above).</p>

<p>For fraction questions, the options above are the only ones included. Other question types also allow additional options to be set.  For <i>Decimal</i> and <i>Scientific</i> questions, the following options are available:</p>

<ul style="list-style:disc">
<li style="margin-bottom: 10px;">Select <b>Allow Small Errors</b> to give full marks for responses which are within some tolerance of the correct response (for example, to forgive rounding errors). When the checkbox is selected, a "Tolerance" field will appear.  Enter the percent error to allow, relative to the correct response.</li>
<li style="margin-bottom: 10px;">Select <b>Part Marks For Factor-of-10</b> to give part marks for responses which differ from the correct response by a factor of ten (that is, which are 10, 100, etc. times too small or large).  When the checkbox is selected, a "Multiplier" field will appear.  Enter the multiplier that should be applied to the full value of the question if the response is out by a factor of ten.</li>
<li style="margin-bottom: 10px;">The last option allows you to change SmarterMarks' behaviour when a student gives a response with the incorrect number of significant digits (for example, answering "1.8" when the key is "1.80"). Selecting <b>Round as needed</b> will round the response and keyed answer to their common significant digits, so "1.8" will be a correct response for both "1.80" and "1.83". Selecting <b>Add zeros as needed</b> will add zeros to achieve a common number of significant digits, so "1.8" will be a correct response for "1.80" but not "1.83". Finally, selecting <b>Mark as incorrect</b> will make no modifications, so "1.8" will be correct only if the keyed answer is exactly "1.8". You can also choose to give reduced marks if modifications are required.</li>
</ul>

<p>For <i>Selection</i> questions, the following options are available:</p>

<ul style="list-style:disc">
<li style="margin-bottom: 10px;">Select <b>Marks are per correct digit</b> to give the marks indicated for <i>each correct digit</i>; for example, for a 'Marks' value of 0.5 and a keyed answer '1095', the response '1085' would receive 1.5 marks, and '1095' would receive 2.0 marks.</li>
<li style="margin-bottom: 10px;">Select <b>Allow Partial Match</b> to give part marks for incorrect responses which match at least half of the selections in the correct answer. When the checkbox is selected, a "Multiplier" field will appear.  Enter the multiplier that should be applied to the full value of the question for partially correct responses.</li>
<li style="margin-bottom: 10px;">Select <b>Ignore Selection Order</b> ignore the order of the selections &mdash; for example, if '367' and '736' should both be considered correct.</li>
</ul>

<p>Click "Save" to save any changes to the question.</p>

<h1 id="changing_question_values">Changing question values</h1>

<p>To change the marks given for a numeric response question, click the <span class="edit_icon text_icon"></span> icon under "Actions" next to the question as shown in the <a href="#editing_and_deleting_numeric_response_questions">Editing and deleting numeric response questions</a> section.  This will open the "Manage Question" dialog.</p>

<p>Next to each scored response is a "Marks" field.  Enter the marks that should be given for a matching response here, and click "Save" to save any changes to the question.</p>

<h1 id="defining_multiple_scored_answers">Defining multiple scored answers</h1>

<p>To allow multiple scored responses for a numeric response question, click the <span class="edit_icon text_icon"></span> icon under "Actions" next to the question as shown in the <a href="#editing_and_deleting_numeric_response_questions">Editing and deleting numeric response questions</a> section.  This will open the "Manage Question" dialog.</p>

<p>To add a new scored response for this question, click the "+" next to an existing response.  A new response will be added:</p>

<img src="<?php echo $imagePath; ?>/nr_question_two_answers.png" />

<p>Enter the response to be scored and assign a value &mdash; the question above, for example, will give 1 mark for a response of "48.2" and 0.5 marks for "24.1".</p>

<p>To remove a response, click the "-" next to that response.</p>

<p>When you are done, click "Save" to save any changes to the question.</p>

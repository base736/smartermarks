<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Working with uploaded scans"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#browsing_uploaded_scans">Browsing uploaded scans</a></li>
<li><a href="#editing_scanned_responses">Editing scanned responses</a></li>
<li><a href="#viewing_previous_analyses">Viewing previous analyses</a></li>
<li><a href="#creating_a_new_analysis">Creating a new analysis</a></li>
</ol>

<h1 id="browsing_uploaded_scans">Browsing uploaded scans</h1>

<p>To access results from previously scanned PDFs, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment for which you want to view scans, and click on "Browse scans" under the "Scan" menu:</p>

<img src="<?php echo $imagePath; ?>/browse_scans.png" />

<p>You will be sent to a list of stored scans for this assessment:</p>

<img src="<?php echo $imagePath; ?>/scans_index.png" />

<h1 id="editing_scanned_responses">Editing scanned responses</h1>

<p>Bring up a list of previous scans for your assessment as described in the <a href="#browsing_uploaded_scans">browsing uploaded scans</a> section. Select the scan you would like to edit, and click the "Edit" button:</p>

<img src="<?php echo $imagePath; ?>/edit_scan_button.png" />

<p>This will open the "Edit Scan" dialog:</p>

<img src="<?php echo $imagePath; ?>/edit_scan.png" />

<p>From this dialog, you can edit the scan details as well as each student's responses.</p>

<p>To edit a student's scanned responses, select their assessment using either the "Responses for assessment" selector at the left, or the search box below it, which will search the student names and IDs if they are present. If there are multiple matches to a search term, you will be able to navigate between them using the arrows to the right of the search box.</p>

<p>Make any changes you would like to one or more student responses. When you are done, click "Save" to save all changes.</p>

<h1 id="viewing_previous_analyses">Viewing previous analyses</h1>

<p>Bring up a list of previous scans for your assessment as described in the <a href="#browsing_uploaded_scans">browsing uploaded scans</a> section. Select a single scan you would like to view the results for, and click the "View" button:</p>

<img src="<?php echo $imagePath; ?>/view_most_recent.png" />

<p>You will be sent to the results of the most recent analysis for this scan. See the <a href="/help/view/assessment_reports">Assessment Reports</a> section for more details.</p>

<h1 id="creating_a_new_analysis">Creating a new analysis</h1>

<p>SmarterMarks has a versatile scoring engine which allows for the analysis of one scan, of several scans for the same document, and of defined subsets of any set of scans.</p>

<p>To begin a new analysis, bring up a list of previous scans for your assessment as described in the <a href="#browsing_uploaded_scans">browsing uploaded scans</a> section. Select the scan or scans you would like to analyze, and click the "New" button:</p>

<img src="<?php echo $imagePath; ?>/new_analysis.png" />

<p>This will open the "New analysis" dialog:</p>
	
<img src="<?php echo $imagePath; ?>/new_analysis_simple.png" />

<p>To build an analysis of the full set of responses selected, click "Build". When the analysis is complete, you will be forwarded to the results window. See the <a href="/help/view/assessment_reports">Assessment Reports</a> section for more details on this display.</p>

<p>Analyses can also be performed using only a subset of the responses. For example, a demographic question might be included at the end of an assessment, but excluded from the scores and statistics by selecting "Omit question" from the <b>Scoring</b> drop-down in the question's options, or excluded from the total by selecting "Bonus question" there. Analyses can then be built for a data set that is filtered according to students' responses to these questions. To filter the selected data, click "Add filters":</p>

<img src="<?php echo $imagePath; ?>/new_analysis_filters.png" />

<p>Select a section, question number, and response to be included in the analysis. Filters can be added or removed using the "+" and "-" buttons on the right, and combined either by requiring all filters to be true, or by requiring any of them to be true, using the selector above.</p>

<p>To build the selected analysis, click "Build". When the analysis is complete, you will be forwarded to the results window.</p>

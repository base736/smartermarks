<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Versioning response forms"
</script>

<p>Version maps can simplify the versioning of response forms, and are produced by many test generating programs. Version maps specify which questions in a new version correspond to each question in the original version.  A version map for a simple 5-question assessment might be given as a table like this one:</p>

<div style="display:table; margin:0 auto; padding:10px;"><table class="version_map" border="1">
<tr><td><b>A</b></td><td><b>B</b></td><td><b>C</b></td><td><b>D</b></td></tr>
<tr><td>1</td><td>4</td><td>5</td><td>2</td></tr>
<tr><td>2</td><td>5</td><td>4</td><td>1</td></tr>
<tr><td>3</td><td>2</td><td>2</td><td>5</td></tr>
<tr><td>4</td><td>1</td><td>3</td><td>3</td></tr>
<tr><td>5</td><td>3</td><td>1</td><td>4</td></tr>
</table></div>

<p>According to this version map, for example, question 1 of version A becomes question 4 in version B, while question 2 of version A becomes question 5 of version B.</p>

<p>To apply a version map to an assessment, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Select the assessment in the Forms and Versions tab and choose "New version map" from the Build menu:</p>

<img src="<?php echo $imagePath; ?>/new_version_map.png" />

<p>This will open the "Version map" dialog:</p>

<img src="<?php echo $imagePath; ?>/version_map.png" />

<p>To create version B in our simple example, we would enter the numbers 4, 5, 2, 1, and 3 &mdash; that is, the column under 'B' &mdash; in the fields given. Once you have entered the version map, click "Build" to build the new version.</p>

<p>There are two important restrictions to version maps in SmarterMarks:</p>

<ul style="list-style:disc">
<li>Questions can be shuffled within a section, but sections must remain in the same order; and </li>
<li>While SmarterMarks will shuffle questions and learning outcomes, it will not change keyed responses for each question. If the keyed responses are are different in your versions, you will need to re-enter the key for those sections.</li>
</ul>

<p>Once you have created your first version map, confirm that the questions have been shuffled correctly. Some maps may be given as the reverse of the above &mdash; that is, specifying which question in the old version corresponds to each question in the new.  If you find that this is the case for your version maps, select the "Reverse the version map" checkbox in the "Version map" dialog to correct it.</p>

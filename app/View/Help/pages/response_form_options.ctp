<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Response form options"
</script>

<h1>Outline</h1>

<ol>
<li><a href="#editing_assessment_options">Editing response form options</a></li>
<li><a href="#save_name_and_title">Assessment name and title</a></li>
<li><a href="#displaying_an_id_field">Displaying an ID field</a></li>
<li><a href="#setting_margins">Setting margins</a></li>
<li><a href="#adding_a_logo">Adding a logo</a></li>
<li><a href="#printing_many_forms_per_page">Printing many forms per page</a></li>
<li><a href="#setting_response_form_language">Setting response form language</a></li>
<li><a href="#scoring_options">Scoring options</a></li>
<li><a href="#customizing_student_reports">Customizing student reports</a></li>
</ol>

<h1 id="editing_assessment_options">Editing response form options</h1>

<p>To edit a response form, click on the "Forms and Versions" tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_tab.png" />

<p>Click on the assessment in the table, then choose "Edit form" under the "Build" menu:</p>

<img src="<?php echo $imagePath; ?>/edit_form.png" />

<p>Edit the options for a response form from anywhere in the editor by clicking on the "Settings" button in the form's title bar:</p>

<img src="<?php echo $imagePath; ?>/assessment_options_view.png" />

<p>This will open the "Assessment details" dialog:</p>

<img src="<?php echo $imagePath; ?>/new_assessment.png" />

<h1 id="save_name_and_title">Assessment name and title</h1>

<p>To change the assessment name and title, first bring up the assessment options dialog as described in the <a href="#editing_assessment_options">Editing assessment options</a> section.</p>

<p>In the <b>Save As</b> field, you can edit the name under which the assessment is stored in your Forms and Versions tab.  This name is not included on response forms.</p>

<p>The <b>Assessment Title</b> field can be used to set the title displayed on each response form:</p>

<img src="<?php echo $imagePath; ?>/preview_title.png" />

<p>Assessment titles may include several lines of text.</p>

<p>Click "Save" to save any changes to assessment options.</p>

<h1 id="displaying_an_id_field">Displaying a name or ID field</h1>

<p>To set the display of a name or ID field on response forms, first bring up the assessment options dialog as described in the <a href="#editing_assessment_options">Editing assessment options</a> section.</p>

<p>A student name or ID field similar to that shown below can optionally be included in each response form:</p>

<img src="<?php echo $imagePath; ?>/preview_id_section.png" />

<p>If it is included, each student's name or ID number will be printed on the individual student reports, and the Response Summary (see the <a href="assessment_reports">Assessment reports</a> section for more information) will tag each student's results with their ID. This can be useful when processing results from a large class.</p>

<p>To include a name field, select the "Field for student name" checkbox and enter the label and required number of letters for the field.  To include an ID field, select the "Field for student ID" checkbox and enter the label and required number of digits for the student ID.</p>

<p>Click "Save" to save any changes to the assessment options.</p>

<h1 id="setting_margins">Setting margins</h1>

<p>It can sometimes be helpful to shrink the default margins, especially when an assessment combines two or more lines of numeric response questions with a multiple choice section. To set margins for response forms, first bring up the assessment options dialog as described in the <a href="#editing_assessment_options">Editing assessment options</a> section.  Each margin can be set in the fields given on the right side.  All margin sizes are in inches.</p>

<p>Click "Save" to save any changes to assessment formatting.</p>

<h1 id="adding_a_logo">Adding a logo</h1>

<p>If you have uploaded a logo (see the <a href="setting_report_preferences">Setting report preferences</a> section for more information), you can choose to include it at the top-left corner of this response form by checking the "Show logo on response form" checkbox.</p>

<h1 id="printing_many_forms_per_page">Printing many forms per page</h1>

<p>For small response forms, it can be useful to print two or more forms on each page, for example:</p>

<img src="<?php echo $imagePath; ?>/preview_two_forms.png" />

<p>Each page can be cut into individual response forms and processed just as regular-sized forms are.</p>

<p>To set the number of forms printed on each page, first bring up the assessment options dialog as described in the <a href="#editing_assessment_options">Editing assessment options</a> section.  Enter the number of copies to be included on each page in the "Copies Per Page" field on the right side. Be sure to check the preview as discussed in the <a href="generic_response_forms">Generic response forms</a> section to ensure that formatting options you've chosen give a suitable response form.</p>

<p>Click "Save" to save any changes to assessment options.</p>

<h1 id="setting_response_form_language">Setting response form language</h1>

<p>To change the language used when building response forms and reports, first bring up the assessment options dialog as described in the <?php echo $this->Html->link('Editing response form options', array('controller' => 'help', 'action' => 'view', 'editing_assessment_options'));?> section.</p>

<p>The language is set using the "Language" selector at the bottom-right of the settings dialog. SmarterMarks currently supports English and French languages. To request another language, email us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>

<h1 id="scoring_options">Scoring options</h1>

<p>Response forms can be scored either out of the total score of all questions, or as a weighted average of section scores. To change the way an assessment is scored, first bring up the assessment options dialog as described in the the <a href="#editing_assessment_options">Editing assessment options</a> section.</p>

<p>Scoring options are found under the "Scoring" tab at the top:</p>

<img src="<?php echo $imagePath; ?>/form_scoring_options_tab.png" />

<p>Click on the tab to bring up the scoring options:</p>

<img src="<?php echo $imagePath; ?>/form_scoring_options.png" />

<p>If this response form has been generated from an assessment built on SmarterMarks, it may show a score type of "Custom weighting". This indicates that the scoring was based on sections different from those on the form, and can't be changed after the assessment was built. Otherwise, under "Score type", there are two options for scoring:</p>

<ul>
<li><b>Out of total</b>, for which assessments are scored as the total number of points the student earns divided by the total number possible; and
<li><b>Weighted sections</b>, for which each section is scored out of its total, but assessment scores are calculated as the weighted average of section scores.
</ul>

<p>Selecting weighted section scoring will show a control similar to the below if sections have been built:</p>

<img src="<?php echo $imagePath; ?>/form_scoring_weighted.png" />

<p>Here, weights can be entered for each section on the left. On the right, bars are shown that indicate the percentage of the assessment score that will be allotted to each section. Weights do not need to add to 100 &mdash; percentages are always calculated using each weight divided by the total of all weights.</p>

<p>Click "Save" to save any changes to scoring.</p>

<h1 id="customizing_student_reports">Customizing student reports</h1>

<p>To customize student reports, first bring up the assessment options dialog as described in the <a href="#editing_assessment_options">Editing assessment options</a> section and click the "Reports" tab to access options for student reports for this assessment.</p>

<img src="<?php echo $imagePath; ?>/assessment_report_settings.png" />

<p>Using the "Layout" option, you can choose to put between one and five student reports on each printed page, or to allow SmarterMarks to automatically fit as many as possible.</p>

<p>The "Type" option allows you to choose between three student report types:</p>

<ul>
	<li><b>Detailed reports</b>  can include any combination of learning outcomes, student responses, and answer key. In addition to these options, you can choose to use wide learning outcomes in reports. These can be useful when you want to include entire curriculum goals in student reports.</li>
	<li><b>"Outcomes only" reports</b> give only each student's performance on the defined learning outcomes, separated into user-defined bins. No scores or percentages for the assessment are given. A default set of bin names and ranges can be defined in your <a href="setting_report_preferences">user settings</a>, and can be edited for each assessment when the "Outcomes only" report type is selected.
</ul>

<p>Click "Save" to save any changes to the student report options.</p>


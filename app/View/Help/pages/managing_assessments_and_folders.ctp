<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
document.getElementById("topicHeader").innerHTML="Managing assessments and folders"
</script>

<h1>Overview</h1>

<p>The Forms and Versions, Assessment Templates, and Questions tabs in SmarterMarks are built around a folder system that allows you to keep track of items from several courses, over several years, while at the same time making your day-to-day work easier.</p>

<p>In this section, we'll discuss the following in the context of the Forms and Versions tab; other tabs work similarly:</p>

<ol>
<li><a href="#the_folder_system">The folder system</a></li>
<li><a href="#creating_deleting_and_editing_folders">Creating, deleting, and editing folders</a></li>
<li><a href="#moving_and_copying_assessments_into_a_folder">Moving and copying assessments into a folder</a></li>
<li><a href="#duplicating_an_assessment">Duplicating an assessment</a></li>
<li><a href="#searching_for_assessments">Searching for assessments</a></li>
</ol>

<h1 id="the_folder_system">The folder system</h1>

<p>Folders are shown along the left side of the Forms and Versions tab:</p>

<img src="<?php echo $imagePath; ?>/assessments_index_folders.png" />

<p>To view the assessments contained within a folder, click on a folder in the Folders pane. Your account was created with four folders.</p>

<ol>
<li><b>Home</b>. Your home folder is the folder you will start in whenever you log in to SmarterMarks. It is a good place to keep assessments that you are currently working on, or for which you still might receive new scans.</li>
<li><b>Archive</b>. The archive is a place to keep older assessments. You might, for example, create folders within the archive to arrange old assessments by year and course.</li>
<li><b>Communities</b>. The communities folder contains the communities you are subscribed to. Communities allow you to share items with other users as described in the <?php echo $this->Html->link('Sharing with other users', array('controller' => 'help', 'action' => 'view', 'sharing_with_other_users'));?> section.</li>
<li><b>Trash</b>. Whenever you delete an assessment, it is placed in the trash. Deleted assessments are not permanently removed from the system until the trash has been emptied.</li>
</ol>

<p>These default folders cannot be edited or deleted.</p>

<h1 id="creating_deleting_and_editing_folders">Creating, deleting, and editing folders</h1>

<p>Folders can be created, deleted, renamed, and moved using the "Folder" menu at the left::</p>

<img src="<?php echo $imagePath; ?>/folder_menu.png" />

<p>Clicking on "Move folder" will bring up the dialog below:</p>

<img src="<?php echo $imagePath; ?>/folder_move.png" />

<p>To move a folder, choose a new parent folder. Click "Save" when you are done to save any changes made to the folder.</p>

<h1 id="moving_and_copying_assessments_into_a_folder">Moving and copying assessments into a folder</h1>

<p>To move or copy assessments into a new folder, select one or more assessments from the list, then select "Move to folder" or "Copy to folder" from the "Manage" menu:</p>

<img src="<?php echo $imagePath; ?>/move_to_folder.png" />

<p>Clicking on "Move to folder", for example, The "Move Item" dialog will appear:</p>

<img src="<?php echo $imagePath; ?>/assessment_move.png" />

<p>Select a new folder for these assessments and click "Move" to move them.</p>

<h1 id="duplicating_an_assessment">Duplicating an assessment</h1>

<p>To duplicate an assessment (for example, to update a copy for a new semester), select "Duplicate" from the Build menu for that assessment:</p>

<img src="<?php echo $imagePath; ?>/duplicate_form.png" />

<p>A folders dialog will open to give you the option of placing the new copy in a different folder:</p>

<img src="<?php echo $imagePath; ?>/duplicate_form_target.png" />

<p>When you click "Duplicate", you will be forwarded to the assessment editor.</p>

<h1 id="searching_for_assessments">Searching for assessments</h1>

<p>Searching for assessments is done using the search button at the top right of the Forms and Versions tab:</p>

<img src="<?php echo $imagePath; ?>/search.png" />

<p>To search for assessments, click on the button,  enter a term in the field shown, then hit enter. The term entered will be searched for in both the name of each assessment and title displayed on response forms, and any matching assessments shown in the table below.</p>

<p>Searches in the question bank can also be extended to include all subfolders of the selected folder, or all folders available to you, using the drop-down in the search tool:</p>

<img src="<?php echo $imagePath; ?>/search_where.png" />

<p>To reset the search, click on a folder or on the "My Assessments" tab.</p>

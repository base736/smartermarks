<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var documentID = <?php echo $document_id;?>;
	var canEdit = <?php echo $canEdit; ?>;
	var userID = <?php echo $user['id']; ?>;
	var isGenerated = <?php echo $isGenerated; ?>;
	var isOnline = false;
	var canReorder = <?php echo $canReorder; ?>;
	var canCancel = <?php echo $canCancel; ?>;
	var showSettings = <?php echo $showSettings; ?>;
	var userDefaults = <?php echo $user['defaults']; ?>;
    var showCorrelation = <?php echo $user['correlations_beta'] ? 'true' : 'false'; ?>;

	var userdataPrefix = "<?php
		$secrets = Configure::read('secrets'); 
		echo $secrets['s3']['userdata_prefix'];
	?>";

    var classLists = <?php echo json_encode($classLists); ?>;
</script>

<script type="module" src="/js/responseforms/build.js?7.0"></script>

<!-- Top bar -->

<div class="action_bar" style="margin-bottom:5px;">
	<div class="action_left" style="color:#ccc;">
		<div id="document_title"></div>
	</div>
	<div class="action_right" style="align-self:flex-start;">
		<?php
		if ($canEdit) {
		?>
		<button type="button" class="startEditDoc btn btn-dark" style="width:80px;" disabled><span>Settings</span></button>
		<?php
		}
		?>
		<div class="btn-group">
			<button id="print_dropdown" class="btn btn-dark dropdown-toggle item_action_one" data-toggle="dropdown" href="#" disabled>
				Print 
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu pull-right">
				<?php 
				if ($isGenerated) {
				?>
				<li><a href="#" id="buildQuestions_button" class="needs_generated">Question book</a></li>
				<?php
				}
				?>
				<li><a href="#" id="buildGenericPDF_button">Generic form</a></li>
				<li><a href="#" id="buildClassPDF_button">From class list</a></li>
				<li><a href="#" id="buildKey_button">Answer key</a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Left Panel-->

<div id="panel_wrapper" style="position:relative; width:890px; height:auto;">
	<div id="left_panel" style="width:295px; position:relative; float:left;">
	
		<!-- {* navigation *} -->
		<div class="view_panel" <?php if (!$isGenerated) { ?>style="display:none;"<?php } ?>>
		    <div class="window_bar">Views</div>
		    <div class="window_content" style="padding:10px;">
				<ul id="view_menu" style="padding:5px;" >
					<li class="view_button" data-view-type="form"><a href="#">Response form</a></li>
					<?php 
					if ($isGenerated) { 
					?>
					<li class="view_button" data-view-type="questions"><a href="#">Question book</a></li>
					<li class="view_button" data-view-type="statistics"><a href="#">Version statistics</a></li>
					<?php 
					} 
					?>
				</ul>
		    </div>
		</div>
	
		<!-- {* form sections *} -->
		<div id="form_navigation_wrapper" style="display:none;">
			<div class="window_bar">Sections</div>
			<div class="window_content" style="padding:5px; overflow:visible;">
				<div class="layout_panel">
					<div id="empty_layout" style="width:255px; padding:10px; color:#eee; font-size:13px; font-style:italic;">No sections built for this form. Click "Add Section" below to begin.</div>
					<ul id="form_sections_menu" class="menu_wrapper"></ul>
					<?php 
					if ($canEdit) {
					?>

						<div class="edit_control" style="margin-top:10px;">
							<span style="color:#cccccc; font-size:13px; vertical-align:middle; margin-left:4px; margin-right:6px;">Add:</span>
							<?php if (!$isGenerated) { ?><div class="btn-group" style="margin-left:2px;">
								<button class="btn btn-small btn-dark dropdown-toggle" data-toggle="dropdown" href="#">
									Section
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu text-left">
									<li><a href="#" class="openAddMCsection">Multiple choice section</a></li>
									<li><a href="#" class="openAddNRsection">Numeric response section</a></li>
									<li><a href="#" class="openAddWRsection">Written response section</a></li>
								</ul>
							</div><?php } ?><div class="btn-group" style="margin-left:2px;">
								<button class="btn btn-small btn-dark" id="addPageBreak">Page Break</button>
							</div>
						</div>

					<?php 
					}
					?>
					<div style="clear:both"></div>
				</div>
			</div>
		</div>

		<!-- {* question book sections *} -->
		<div id="question_navigation_wrapper" style="display:none;">
			<div id="question_sections_wrapper">
				<div class="window_bar">Sections</div>
				<div class="window_content" style="padding:5px; overflow:visible;">
					<div class="layout_panel">
						<ul id="question_sections_menu" class="menu_wrapper"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Right Panel-->

	<div style="width:585px; float:right;">
		<div class="right_panel" id="loading_panel" style="display:block;">
			<div class="window_bar">Loading form</div>
			<div class="window_content" style="background-color:white; padding:10px;">
				<div><div class="wait_icon"></div><i>Loading&hellip;</i></div>
			</div>
		</div>

		<!-- {* document details *} -->

		<div class="right_panel" id="form_detail_panel">
			<div class="window_bar">Response form settings</div>
			<div class="window_content" style="background-color:white; padding:10px;">

				<div id="document_detail" class="document_detail_container" style="display:none;">
					<form class="formular" id="document_edit" accept-charset="utf-8">
						<div class="labeled-input">
							<label for="doc_title" class="">Save As:</label>
							<input disabled="disabled" class="defaultTextBox2" maxlength="200" type="text" id="doc_save_name"/>
						</div>
						<div class="labeled-input">
							<label for="doc_title" style="vertical-align:top; position:relative; top:5px;">Form Title:</label>
							<textarea disabled="disabled" class="docTitleTextField defaultTextBox2" style="resize:none; height:100px;" id="doc_title_text"></textarea>
							<div style="clear: both;"></div>
						</div>
						<div class="labeled-input" style="margin-top:20px;">
							<label for="bubbles_wrapper" style="vertical-align:top; position:relative; top:3px;">Student Information:</label>
							<div id="bubbles_wrapper" style="width:268px;">
								<div class="options_row">
									<input disabled="disabled" type="checkbox" class="normal_chkbx" id="doc_title_showNameField" /><label for="doc_title_showNameField" style="text-align:left; width:228px;">Field for student name</label>
								</div>
								<div id="doc_nameDetails_container" class="options_row">
									<label for="doc_title_nameLabel">Label</label><input disabled="disabled" id="doc_title_nameLabel" class="defaultTextBox2" style="width:90px !important;" type="text" /><label for="doc_title_nameLetters" style="text-align:right; width:60px;">Letters</label><input disabled="disabled" style="width:20px;" class="defaultTextBox2" type="text" id="doc_title_nameLetters" />
								</div>
								<div class="options_row">
									<input disabled="disabled" type="checkbox" class="normal_chkbx" id="doc_title_showIDField" /><label for="doc_title_showIDField" style="text-align:left; width:228px;">Field for student ID</label>
								</div>
								<div id="doc_idDetails_container" class="options_row">
									<label for="doc_title_idLabel" class="idDigits_label">Label</label><input disabled="disabled" id="doc_title_idLabel" class="defaultTextBox2" style="width:90px !important;" type="text" /><label for="doc_title_idDigits" style="text-align:right; width:60px;" class="idDigits_label">Digits</label><input disabled="disabled" style="width:20px;" class="defaultTextBox2" type="text" id="doc_title_idDigits" />
								</div>
							</div>
						</div>

						<div class="labeled-input" style="margin-top:20px;">
							<label for:"doc_page_size">Page size:</label>
							<select disabled="disabled" class="defaultSelectBox" style="width:100px;" id="doc_page_size">
								<option value="letter">Letter</option>
								<option value="legal">Legal</option>
								<option value="a4">A4</option>
							</select>
						</div>

						<div class="labeled-input" style="margin-top:20px;">
							<label for="doc_copyPerPage" style="vertical-align:top; position:relative; top:5px;">Margins (inches):</label>
							<div>
								<div class="margin_column">
									<div class="options_row" style="margin-bottom:10px;">
										<label for="doc_topMarginSize">Top</label>
										<input disabled="disabled" class="defaultTextBox2 inches" type="text" id="doc_topMarginSize" style="width:40px;" />
									</div>
									<div class="options_row">
										<label for="doc_bottomMarginSize">Bottom</label>
										<input disabled="disabled" class="defaultTextBox2 inches" type="text" id="doc_bottomMarginSize" style="width:40px;" />
									</div>
								</div>
								<div class="margin_column">
									<div class="options_row" style="margin-bottom:10px;">
										<label for="doc_leftMarginSize">Left</label>
										<input disabled="disabled" class="defaultTextBox2 inches" type="text" id="doc_leftMarginSize" style="width:40px;" />
									</div>
									<div class="options_row">
										<label for="doc_rightMarginSize">Right</label>
										<input disabled="disabled" class="defaultTextBox2 inches" type="text" id="doc_rightMarginSize" style="width:40px;" />
									</div>
								</div>
							</div>
						</div>
						
						<div class="labeled-input" style="margin-top:20px;">
							<label style="vertical-align:top; position:relative; top:4px;">Layout:</label>
							<div>
								<div class="options_row">
									<input disabled="disabled" class="defaultTextBox2" type="text" id="doc_copiesPerPage" style="width:20px;" /><label for="doc_copyPerPage" style="margin-left:5px;">response form(s) per page</label>
								</div>
								<div class="options_row">
									<input disabled="disabled" type="checkbox" class="normal_chkbx" id="doc_show_logo" /><label for="doc_show_logo" style="text-align:left; width:228px;">Show logo on response form</label>
								</div>
							</div>
						</div>

                        <div class="labeled-input">
							<label>Score forwarding:</label>
							<select disabled="disabled" class="defaultSelectBox" id="doc_version" style="width:160px;">
                                <option value="3">Enabled</option>
                                <option value="2">Disabled</option>
							</select>
						</div>

						<div class="labeled-input">
							<label>Language:</label>
							<select disabled="disabled" class="defaultSelectBox" id="doc_language" style="width:160px;">
								<option value="English">English</option>
								<option value="French">French</option>
							</select>
						</div>

						<div id="doc_nameLines_wrapper" class="labeled-input" style="margin-top:20px;">
							<label for="doc_title_nameLines" style="vertical-align:top; position:relative; top:4px;">Name lines:</label>
							<textarea style="resize:none; min-height:20px; vertical-align:top;" disabled="disabled" class="defaultTextBox2" type="text" id="doc_title_nameLines"></textarea>
						</div>

						<div class="labeled-input" style="margin-top:20px;">
							<label for="doc_scoring_type">Score type: </label>
							<select disabled="disabled" class="defaultSelectBox" style="width:160px;" id="doc_scoring_type">
								<option value="Total">Out of total</option>
								<option value="Sections">Weighted sections</option>
								<option value="Outcomes">Weighted outcomes</option>
								<?php if ($isGenerated) { ?> 
								<option value="Custom">Custom weighting</option>
								<?php } ?>
							</select>
						</div>
						
						<div id="doc_student_reports_wrapper" class="labeled-input" style="margin-top:20px;">
							<label style="vertical-align:top; position:relative; top:4px;">Student Reports:</label>
							<div>
								<div class="options_row">
									<label for="doc_student_layout" style="text-align:left; width:70px;">Layout</label>
									<select disabled="disabled" class="defaultSelectBox" style="width: 150px" id="doc_student_layout">
										<option value='default'>Automatic</option>
										<option value='singles'>One per page</option>
										<option value='2perpage'>2 per page</option>
										<option value='3perpage'>3 per page</option>
										<option value='4perpage'>4 per page</option>
										<option value='5perpage'>5 per page</option>
									</select>
								</div>
                                <div class="options_row">
                                    <label for="doc_student_weights" style="text-align:left; width:70px;">Weights</label>
                                    <select disabled="disabled" class="defaultSelectBox" style="width: 150px" id="doc_student_weights">
                                        <option value='none'>Hide weights</option>
                                        <option value='show'>Show weights</option>
                                    </select>
                                </div>
                                <div class="options_row">
                                    <label for="doc_student_outcomes" style="text-align:left; width:70px;">Outcomes</label>
                                    <select disabled="disabled" class="defaultSelectBox" style="width: 150px" id="doc_student_outcomes">
                                        <option value='none'>None</option>
                                        <option value='names'>Outcomes only</option>
                                        <option value='numbers'>With question #'s</option>
                                    </select>
                                </div>
                                <div class="options_row">
                                    <label for="doc_student_responses" style="text-align:left; width:70px;">Responses</label>
                                    <select disabled="disabled" class="defaultSelectBox" style="width: 150px" id="doc_student_responses">
                                        <option value='none'>Hide responses</option>
                                        <option value='responses'>Show responses</option>
                                        <option value='key'>Responses and key</option>
                                    </select>
                                </div>
                                <div class="options_row">
    								<input disabled="disabled" class="normal_chkbx" type="checkbox" id="doc_student_incorrectonly" style="margin-left:80px;" /><label for="doc_student_incorrectonly">Incorrect only</label>
                                </div>
                                <div class="options_row">
                                    <label for="doc_student_results" style="text-align:left; width:70px;">Results</label>
                                    <select disabled="disabled" class="defaultSelectBox" style="width: 150px" id="doc_student_results">
                                        <option value='numerical'>Scores and percents</option>
                                        <option value='proficiency'>Proficiency levels</option>
                                        <option value='both'>Both styles</option>
                                    </select>
                                </div>
                                <div id="doc_proficiencies_wrapper" class="options_row objectives_header">
									<label for="doc_levels_wrapper" style="text-align:left; width:70px; vertical-align:top; position:relative; top:3px;">Levels</label>
									<div id="doc_levels_wrapper" style="width:200px;"></div>
								</div>
							</div>
						</div>

						<?php if ($canEdit) { ?><button class="btn btn-primary startEditDoc editButton">Edit Now</button><?php } ?>
					</form>
				</div>
			</div>
		</div>

		<!-- {* mc section details *} -->
		<div class="right_panel mc_details">
			<div class="window_bar">Selected MC Section</div>
			<div class="window_content" style="background-color:white; padding:10px;">

				<div id="mcsection_detail" class="document_detail_container" style="display:none;">
					<form class="formular" id="mcsection_edit" accept-charset="utf-8">
						<div class="labeled-input">
							<label for="mc_title">Section Title</label>
							<input disabled="disabled" class="defaultTextBox2" maxlength="255" type="text" id="mc_header"/>
						</div>
						<div class="labeled-input" id="mc_weight_wrapper">
							<label for="mc_weight_percent">Weight:</label>
							<input class="defaultTextBox2" style="width:35px;" maxlength="200" type="text" id="mc_weight_percent" disabled/><label style="width:auto;" class="disabled">&nbsp;% (see form settings to edit weights)</label>
						</div>
						<div class="labeled-input">
							<label>Response Type</label>
							<select class="defaultSelectBox pad5px" style="height:28px; width:100px" id="mc_mc_type" disabled>
								<option value='Letter'>a, b, c, &hellip;</option>
								<option value='TrueFalse'>True / False</option>
							</select>
							<select class="defaultSelectBox pad5px" style="display:none; height:28px; width:100px" id="mc_mc_type_many" disabled>
								<option>many</option>
							</select>
						</div>
						<div class="labeled-input">
							<label>Number Of Choices</label>
							<input id="mc_mc_choiceCount" class="defaultTextBox2" style="width:35px;" type="text" disabled />
							<input id="mc_mc_choiceCount_many" class="defaultTextBox2" style="display:none; width:35px;" type="text" value="many" disabled />
						</div>
						<div class="labeled-input">
							<label for="mc_columnCount">Number Of Columns</label>
							<input disabled="disabled" class="defaultTextBox2" style="width:35px;" type="text" id="mc_columnCount" />
						</div>
						<div class="labeled-input">
							<label for="mc_startNumber">Start Numbering At</label>
							<input disabled="disabled" class="defaultTextBox2" style="width:35px;" type="text" id="mc_startNumber" />
						</div>
						<div class="labeled-input">
							<label for="mc_questionNum">Number Of Questions</label>
							<input disabled="disabled" class="defaultTextBox2" style="width:35px;" type="text" id="mc_questionCount" />
						</div>
						<div class="labeled-input" style="padding-bottom:10px; overflow: hidden;">
							<label for="mc_detail_answers" style="vertical-align:top; position:relative; top:5px;">Answer Key</label>
							<div id="mc_detail_answers_wrapper">
							<textarea disabled="disabled" class="defaultTextBox2" style="width:200px; height:auto; overflow:auto; margin-bottom:0px;" rows="1" cols="30" id="mc_detail_answers"></textarea>
							<div id="plusLegend" style='clear:both; font-size:8pt; padding-top:0px;'>('+' indicates a detailed answer)</div>
							</div>
						</div>
						<?php if ($canEdit) { ?>
						<button class="btn btn-primary editButton" id="startEditMc">Edit Now</button>
						<div style="clear:both;"></div>
						<?php } ?>
					</form>
				</div>
			</div>
		</div>

		<!-- {* nr section details *} -->
		<div class="right_panel nr_details">
			<div class="window_bar">Selected NR Section</div>
			<div class="window_content" style="background-color:white; padding:10px;">

				<div id="nrsection_detail" class="document_detail_container" style="display:none;">
					<form class="formular" id="nrsection_edit" accept-charset="utf-8">
						<div class="labeled-input">
							<label for="nr_title">Section Title</label>
							<input disabled="disabled" class="validate[required] defaultTextBox2" maxlength="255" type="text" id="nr_header"/>
						</div>
						<div class="labeled-input" id="nr_weight_wrapper">
							<label for="nr_weight_percent">Weight:</label>
							<input class="defaultTextBox2" style="width:35px;" type="text" id="nr_weight_percent" disabled/><label style="width:auto;" class="disabled">&nbsp;% (see form settings to edit weights)</label>
						</div>

						<div class="labeled-input">
							<label>Field Style</label>
							<select disabled="disabled" class="defaultSelectBox" id="nr_nr_style" style="width:200px;">
								<option value="Default">Digits and decimals</option>
								<option value="Negatives">Allow negative answers</option>
								<option value="Fractions">Fractions and negatives</option>
							</select>
						</div>
				
						<div class="labeled-input">
							<label>Number Of Blanks</label>
							<input id="nr_nr_digitCount" class="validate[required] defaultTextBox2" style="width:35px;" type="text" disabled />
							<input id="nr_nr_digitCount_many" class="defaultTextBox2" style="display:none; width:35px;" type="text" value="many" disabled />
						</div>

						<div class="labeled-input">
							<label for="nr_columnCount">Number Of Columns</label>
							<input disabled="disabled" class="defaultTextBox2" style="width:35px;" type="text" id="nr_columnCount" />
						</div>
						
						<?php if ($canEdit) { ?>
						<button class="btn btn-primary editButton" id="startEditNr">Edit Now</button>
						<div style="clear:both;"></div>
						<?php } ?>
					</form>
				</div>
			</div>
		</div>

		<!-- {* wr section details *} -->
		<div class="right_panel wr_details">
			<div class="window_bar">Selected WR Section</div>
			<div class="window_content" style="background-color:white; padding:10px;">

				<div id="wrsection_detail" class="document_detail_container" style="display:none;">
					<form class="formular" id="wrsection_edit" accept-charset="utf-8">
						<div class="labeled-input">
							<label for="wr_title">Section Title</label>
							<input disabled="disabled" class="validate[required] defaultTextBox2" type="text" id="wr_header"/>
						</div>
						<div class="labeled-input" id="wr_weight_wrapper">
							<label for="wr_weight_percent">Weight:</label>
							<input class="defaultTextBox2" style="width:35px;" type="text" id="wr_weight_percent" disabled/><label style="width:auto;" class="disabled">&nbsp;% (see form settings to edit weights)</label>
						</div>

						<div class="labeled-input">
							<label></label>
							<div class="options_row">
								<input disabled="disabled" class="normal_chkbx" type="checkbox" id="wr_wr_includeSpace" /><label for="wr_wr_includeSpace">Include space for student responses</label>
							</div>
						</div>

						<div class="labeled-input">
							<label for="wr_columnCount">Number Of Columns</label>
							<input disabled="disabled" class="defaultTextBox2" style="width:35px;" type="text" id="wr_columnCount" />
						</div>

						<?php if ($canEdit) { ?>
						<button class="btn btn-primary editButton" id="startEditWr">Edit Now</button>
						<div style="clear:both;"></div>
						<?php } ?>
					</form>
				</div>
			</div>
		</div>

		<!-- {* wr questions table *} -->
		<div class="right_panel question_details wr_details">
			<div class="window_bar">Questions</div>
			<div class="window_content" id="wr_questionPanel">

				<table class="panel_table actions_table">
					<thead>
						<tr>
							<td width="10%">Number</td>
							<td>Prompt</td>
							<td width="10%">Out of</td>
							<td width="15%">Actions</td>
						</tr>
					</thead>
					<tbody id="wr_questionlist">
					</tbody>
				</table>
				<div style="padding:10px;">
					<?php if ($canEdit && !$isGenerated) { ?><button class="btn btn-primary editButton" id="openAddWRQuestion">Add New Question</button><?php } ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<!-- {* nr questions table *} -->
		<div class="right_panel question_details nr_details">
			<div class="window_bar">Questions</div>
			<div class="window_content" id="nr_questionPanel">

				<table class="panel_table actions_table">
					<thead>
						<tr>
							<td width="10%">Number</td>
							<td width="15%">Type</td>
							<td width="15%">Answer</td>
							<td align="center" width="45%">Options</td>
							<td width="15%">Actions</td>
						</tr>
					</thead>
					<tbody id="nr_questionlist">

					</tbody>
				</table>
				<div style="padding:10px;">
					<?php if ($canEdit && !$isGenerated) { ?><button class="btn btn-primary editButton" id="openAddNRQuestion">Add New Question</button><?php } ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<!-- {* outcomes table *} -->
		<div class="right_panel" id="outcomes_table">
			<div class="window_bar">Learning Outcomes</div>
			<div class="window_content" id="categoryPanel">

				<table class="panel_table actions_table">
					<thead>
						<tr>
							<td width="30%">Name</td>
							<td style="display:none;">Question data</td>
							<td width="55%">Question Numbers</td>
							<td> Actions</td>
						</tr>
					</thead>
					<tbody id="outcomesList">

					</tbody>
				</table>
				<div style="padding:10px;">
					<?php if ($canEdit) { ?><button class="btn btn-primary editButton" id="openAddOutcome">Add New Outcome</button><?php } ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="right_panel" id="questions_panel">
			<div class="window_bar">Question book</div>
			<div class="window_content" style="padding:10px; background:white;">
				<div id="assessment_preview_wrapper" class="assessment_wrapper template_view parts_formatted" style="margin-top:20px; min-height:95px;"></div>
			</div>
		</div>	

		<div class="right_panel" id="statistics_panel" style="display:none;">
			<div class="window_bar">Filters</div>
		    <div class="window_content" style="padding:10px; margin-bottom:10px; display:flex; justify-content:space-between; align-items:flex-start;">
			    <div>
					<div class="labeled-input" style="width:auto;">
						<div style="display:inline-block; vertical-align:top;">
							<input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_date" />
						</div><div style="display:inline-block; vertical-align:top; position:relative; top:2px;">
							<label for="stats_filter_by_date" style="text-align:left; width:auto;">Include only responses after date:</label>
							<input class="datepicker defaultTextBox2" type="text" id="stats_min_date"/>
						</div>
					</div>
					<div class="labeled-input" style="width:auto;">
						<input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_user" /><label for="stats_filter_by_user" style="text-align:left; width:auto;">Include only my responses</label>
					</div>
					<div class="labeled-input" style="width:auto;">
						<input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_version" /><label for="stats_filter_by_version" style="text-align:left; width:auto;">Include only responses for this version</label>
					</div>
			    </div>
			    <div id="stats_button_wrapper" class="custom-buttonpane" style="display:inline-block;">
					<div class='wait_icon' style="margin:0px; display:none;"></div>
					<div class="btn-group" style="display:inline-block;">
						<button class="btn buildButton" id="stats_update">Update statistics</button>
					</div>
					<div class="btn-group" style="display:inline-block;">
						<button class="btn buildButton" id="stats_print" >Print</button>
					</div>
			    </div>
		    </div>

			<div class="window_bar">Version</div>
		    <div class="window_content" style="margin-bottom:10px; padding:10px; background-color:white;">
			    <div id="statistics_graphs" class="assessment_wrapper"></div>
		    </div>

			<div class="window_bar">Selected question</div>
		    <div class="window_content" style="padding:10px;">
		    	<div>
				    <div class="sub_panel_title">Question</div>
				    <div id="statistics_item" class="assessment_wrapper template_view sub_panel_content" style="padding:10px 5px; min-height:50px; background-color:white;"></div>
				</div>
			    <div id="item_statistics_wrapper" style="display:none; margin-top:10px;">
				    <div class="sub_panel_title">Statistics (<span id="statistics_n"></span> total responses)</div>
				    <div class="sub_panel_content" id="item_statistics_data" style="padding:10px 5px; background-color:white;">
				    </div>
			    </div>
		    </div>
		</div>
	</div>
	<div style="clear:both;"></div>
</div>

<!-- Dialog boxes-->

<div class="hidden">

<!-- Item context menu -->

	<div id="question_menu_template">
		<div class="item_menu_wrapper" style="height:35px; padding:5px;">
            <div style="flex:1 1 100%;"></div>
			<div class="item_menu btn-group" style="flex:none; width:auto;">
				<button class="btn btn-small item_menu_button" data-toggle="dropdown">
					Edit
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-right">
					<?php if ($canEdit) { ?>
					<li><a href='#' class='item_menu_scoring'>Change scoring</a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>

<!-- Print question book dialog -->

	<div id="buildQuestions_dialog" class="dialogbox" title="Build question book">
		<div class="dialog_wrapper">
			<p>Question books can optionally include a response form in the generated PDF. Choose from the options below, then click "Build" to continue.</p>
			<form class="formular" accept-charset="utf-8">
				<input type="hidden" id="buildQuestions_docId" />
                <div class="labeled-input">
					<label style="display:inline-block; width:auto;">Build with:</label>
					<select class="defaultSelectBox" style="width:180px;" id="buildQuestions_form">
						<option value="none">question book only</option>
						<option value="front">response form at the front</option>
						<option value="back">response form at the back</option>
					</select>
                </div>
			</form>
		</div>
	</div>
	
<!-- Delete category dialog -->

	<div id="deleteOutcome_dialog" class="dialogbox" title="Delete Category">
		<div class="dialog_wrapper">
			<p id="deleteOutcome_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Delete section dialog -->

	<div id="deleteSection_dialog" class="dialogbox" title="Delete Section">
		<div class="dialog_wrapper">
			<p id="deleteSection_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Delete WR question dialog -->

	<div id="deleteWRQuestion_dialog" class="dialogbox" title="Delete Question">
		<div class="dialog_wrapper">
			<p id="deleteWRQuestion_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Delete NR question dialog -->

	<div id="deleteNRQuestion_dialog" class="dialogbox" title="Delete Question">
		<div class="dialog_wrapper">
			<p id="deleteNRQuestion_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- {* edit document dialog *} -->

	<div id="editDocument_form" class="dialogbox"  title="Edit Form Details">
		<form class="formular" id="ddocument_edit" accept-charset="utf-8">
			<div class="dialog_wrapper" id="editDocument_wrapper_left" style="padding:10px 15px 10px 10px; border-bottom-left-radius: 5px; width:425px; height:auto;">
				<input type="hidden" id="ddoc_title_nameLines" />
				<input type="hidden" id="ddoc_proficiencyLevels" />
				<input type="hidden" id="ddoc_weights" />

				<div class="labeled-input">
					<label for="ddoc_title" style="vertical-align:top; position:relative; top:5px;">Save As</label>
					<div style="display:inline-block;">
						<input class="validate[required] defaultTextBox2" maxlength="200" type="text" id="ddoc_save_name" style="margin-bottom:5px;"/>
						<span class="input_subtext">Name to be used when saving this form and browsing forms and versions.</span>
					</div>
				</div>
				<div class="labeled-input">
					<label for="ddoc_title_text" style="vertical-align:top; position:relative; top:5px;">Form Title</label>
					<div style="display:inline-block;">
						<textarea class="validate[required] docTitleTextField defaultTextBox2" style="resize:none; margin-bottom:5px;" id="ddoc_title_text"></textarea>
						<span class="input_subtext">Form title to be included on the first page (can have multiple lines)</span>
					</div>
				</div>
				<div class="labeled-input">
					<label for="bubbles_wrapper" style="top:0px; vertical-align:top; position:relative; top:5px;">Student Information</label>
					<div id="bubbles_wrapper" style="width:268px;">
						<div class="options_row">
							<input type="checkbox" class="normal_chkbx" id="ddoc_title_showNameField" /><label for="ddoc_title_showNameField" style="text-align:left; width:228px;">Field for student name</label>
						</div>
						<div id="ddoc_nameDetails_container" class="options_row">
							<label for="ddoc_title_nameLabel" style="display:inline-block;">Label</label><input id="ddoc_title_nameLabel" class="validate[required] defaultTextBox2" style="width:90px !important;" type="text" /><label for="ddoc_title_nameLetters" style="text-align:right; width:60px;">Letters</label><input class="validate[required,custom[isPosInteger]] intSpinner" type="text" id="ddoc_title_nameLetters" />
						</div>
						<div class="options_row">
							<input type="checkbox" class="normal_chkbx" id="ddoc_title_showIDField" /><label for="ddoc_title_showIDField" style="text-align:left; width:228px;">Field for student ID</label>
						</div>
						<div id="ddoc_idDetails_container" class="options_row">
							<label for="ddoc_title_idLabel" class="idDigits_label">Label</label><input id="ddoc_title_idLabel" class="validate[required] defaultTextBox2" style="width:90px !important;" type="text" /><label for="ddoc_title_idDigits" style="text-align:right; width:60px;" class="idDigits_label">Digits</label><input class="validate[required,custom[isPosInteger]] intSpinner" type="text" id="ddoc_title_idDigits" />
						</div>
					</div>
					<div class="labeled-input" style="padding-top:10px;">
						<label for="ddoc_nameLines_wrapper" style="position:relative; top:3px;">Name lines</label>
						<div id="ddoc_nameLines_wrapper" class="options_row" style="width:270px; vertical-align:top;"></div>
					</div>
				</div>
				<div id="editDocument_buttons" class="ui-dialog-buttonset custom-buttonpane" style="float:right; margin-top:5px;">
					<button type="button" class="btn btn-cancel btn_font">Cancel</button>
					<button type="button" class="btn btn-save btn_font">Save</button>
				</div>
			</div><div id="editDocument_wrapper_right" style="display:inline-block; vertical-align:top; background:rgb(215, 231, 238); border-bottom-right-radius:5px; width:289px; height:auto; border-left:1px solid #ccc;">

				<div style="width:290px; margin-left:-1px; height:40px; background:rgb(235, 235, 235); position:relative; border-bottom:1px solid #ccc;">
					<div class="document_settings_tab selected" data-wrapper-id="layout_settings_wrapper">Layout</div>
					<div class="document_settings_tab" data-wrapper-id="scoring_settings_wrapper">Scoring</div>
					<div class="document_settings_tab" data-wrapper-id="ddoc_student_reports_wrapper">Reports</div>
				</div>

				<div id="layout_settings_wrapper" class="settings_wrapper">
					<div class="labeled-input">
						<label style="text-align:left; width:auto;">Page size:</label><select class="defaultSelectBox" style="width:100px;" id="ddoc_page_size">
							<option value="letter">Letter</option>
							<option value="legal">Legal</option>
							<option value="a4">A4</option>
						</select>
					</div>
	
					<div class="labeled-input">
						<label style="text-align:left; display:block; width:100%;">Margins (inches)</label>
						<div>
							<div class="margin_column">
								<div class="options_row" style="margin-bottom:10px; margin-top:5px;">
									<label for="ddoc_topMarginSize">Top</label><input class="validate[required,max[5.5],min[0]] marginSpinner" type="text" id="ddoc_topMarginSize"/>
								</div>
								<div class="options_row">
									<label for="ddoc_bottomMarginSize">Bottom</label><input  class="validate[required,max[5.5],min[0]] marginSpinner" type="text" id="ddoc_bottomMarginSize"/>
								</div>
							</div><div class="margin_column">
								<div class="options_row" style="margin-bottom:10px; margin-top:5px;">
									<label for="ddoc_leftMarginSize">Left</label><input class="validate[required,max[4.25],min[0]] marginSpinner" type="text" id="ddoc_leftMarginSize"/>
								</div>
								<div class="options_row">
									<label for="ddoc_rightMarginSize">Right</label><input class="validate[required,max[4.25],min[0]] marginSpinner" type="text" id="ddoc_rightMarginSize"/>
								</div>
							</div>
						</div>
					</div>
				
					<div class="labeled-input">
						<input class="validate[required,custom[isPosInteger]] intSpinner" type="text" id="ddoc_copiesPerPage"/><label for="ddoc_copyPerPage" style="width:160px;">response form(s) per page</label>
					</div>

					<div class="labeled-input">
						<input type="checkbox" class="normal_chkbx" id="ddoc_show_logo" /><label for="ddoc_show_logo" id="ddoc_show_logo_label" style="text-align:left; width:228px;">Show logo on response form</label>
					</div>

                    <div class="labeled-input">
                        <label style="text-align:left; width:110px;">Score forwarding:</label><select class="defaultSelectBox" id="ddoc_version" style="width:140px;">
                            <option value="3">Enabled</option>
                            <option value="2">Disabled</option>
                        </select>
                    </div>

					<div class="labeled-input">
						<label style="text-align:left; width:110px;">Language:</label><select class="defaultSelectBox" id="ddoc_language" style="width:140px;">
							<option value="English">English</option>
							<option value="French">French</option>
						</select>
					</div>
				</div>

				<div id="scoring_settings_wrapper" class="settings_wrapper" style="display:none;">
					<div class="labeled-input" >
						<label style="width:auto;">Score type: </label><select class="defaultSelectBox" style="width:185px;" id="ddoc_scoring_type">
							<option value="Total">Out of total</option>
							<option value="Sections">Weighted sections</option>
							<option value="Outcomes">Weighted outcomes</option>
							<?php if ($isGenerated) { ?>
							<option value="Custom">Custom weighting</option>
							<?php } ?>
						</select>
					</div>
					<div id="section_weights" style="width:100%; height:330px; overflow-y:scroll;">
					</div>
				</div>
				
				<div id="ddoc_student_reports_wrapper" class="settings_wrapper" style="display:none;">
					<div class="labeled-input">
						<label style="text-align:left; display:block; width:100%;">Student reports</label>
						<div style="margin-left:20px;">
							<div class="options_row" style="margin-top:5px;">
								<label for="ddoc_student_layout" style="text-align:left; width:70px;">Layout</label>
								<select class="defaultSelectBox" style="width: 150px" id="ddoc_student_layout">
									<option value='default'>Automatic</option>
									<option value='singles'>One per page</option>
									<option value='2perpage'>2 per page</option>
									<option value='3perpage'>3 per page</option>
									<option value='4perpage'>4 per page</option>
									<option value='5perpage'>5 per page</option>
								</select>
							</div>
                            <div class="options_row">
                                <label for="ddoc_student_weights" style="text-align:left; width:70px;">Weights</label>
                                <select class="defaultSelectBox" style="width: 150px" id="ddoc_student_weights">
                                    <option value='none'>Hide weights</option>
                                    <option value='show'>Show weights</option>
                                </select>
                            </div>
                            <div class="options_row">
                                <label for="ddoc_student_outcomes" style="text-align:left; width:70px;">Outcomes</label>
                                <select class="defaultSelectBox" style="width: 150px" id="ddoc_student_outcomes">
                                    <option value='none'>Hide outcomes</option>
                                    <option value='names'>Show outcomes</option>
                                    <option value='numbers'>With question #'s</option>
                                </select>
                            </div>
                            <div class="options_row">
                                <label for="ddoc_student_responses" style="text-align:left; width:70px;">Responses</label>
                                <select class="defaultSelectBox" style="width: 150px" id="ddoc_student_responses">
                                    <option value='none'>Hide responses</option>
                                    <option value='responses'>Show responses</option>
                                    <option value='key'>Responses and key</option>
                                </select>
                            </div>
                            <div class="options_row">
    								<input class="normal_chkbx" type="checkbox" id="ddoc_student_incorrectonly" style="margin-left:80px;"/><label for="ddoc_student_incorrectonly">Incorrect only</label>
                                </div>
                            <div class="options_row">
                                <label for="ddoc_student_results" style="text-align:left; width:70px;">Results</label>
                                <select class="defaultSelectBox" style="width: 150px" id="ddoc_student_results">
                                    <option value='numerical'>Scores and percents</option>
                                    <option value='proficiency'>Proficiency levels</option>
                                    <option value='both'>Both styles</option>
                                </select>
                            </div>
							<div id="ddoc_proficiencies_wrapper" class="options_row objectives_header" style="margin-top:10px;">
								<div>
									<div style="width:130px;">Level name</div>
									<div style="width:55px;">From %</div>
								</div>
								<div id="ddoc_levels_wrapper" style="width:250px; vertical-align:top;"></div>
							</div>
						</div>
					</div>
				</div>				
				
			</div>
		</form>		
	</div>

	<!-- {* edit MC question dialog *} -->
	<div id="editMCQuestion_dialog" class="dialogbox" title="Question Details">
		<div class="dialog_wrapper">
		<form class="formular" id="mcDetailsForm" accept-charset="utf-8">
			<div class="labeled-input" style="float:left; width:auto;">
				<label id="dmcq_choices_label" for="dmcq_choices" style="text-align:left; width:50px;">Choices </label>
				<input class="validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner" style="width:50px;" type="text" id="dmcq_choices" />
			</div>
			<div class="labeled-input" style="float:right; width:auto; margin-right:5px;">
				<label id="dmcq_numberingType_label" for="dmcq_numberingType" style="text-align:left; width:auto;">Question</label>
				<select class="defaultSelectBox pad5px" style="height: 28px; width: 70px" id="dmcq_numberingType">
				</select>
			</div>
			<div style="clear:both;"></div>

			<div class="labeled-input" style="float:left; width:auto;">
				<label id="dmcq_type_label" for="dmcq_type" style="text-align:left; width:50px;">Type </label>
				<select class="defaultSelectBox pad5px" style="height: 28px; width: 100px" id="dmcq_type">
					<option value='Letter'>a, b, c, &hellip;</option>
					<option value='TrueFalse'>True / False</option>
				</select>
			</div>
			<div class="labeled-input" style="float:right; width:auto; margin-right:5px;">
				<label id="dmcq_scoring_label" for="dmcq_scoring" style="width:auto; text-align:left;">Scoring</label>
				<select class="defaultSelectBox pad5px" style="height: 28px; width: 120px" id="dmcq_scoring">
					<option value="Default">Regular scoring</option>
					<option value="Omit">Omit question</option>
					<option value="Bonus">Bonus question</option>
				</select>
			</div>
			<div style="clear:both;"></div>

			<div id="dmcq_detailWrapper" style="margin-top:5px; margin-bottom:5px; padding-right:5px;">
				<div class="labeled-input" style="width:100%; margin:0px;">
					<label style="display:block; text-align:left;">Scored responses</label>
					<div id="dmcq_answer_list" style="width:100%;"></div>
				</div>
				<span class="input_subtext" id="dmcq_description" style="width:100%; min-height:15px; margin-top:5px; margin-bottom:5px;"></span>
			</div>
		</form>
		</div>
	</div>

	<!-- {* add mc section dialog *} -->
	<div id="addMCSection_dialog" class="dialogbox" title="MC Section Details">
		<form class="formular" style="margin-bottom:0px" accept-charset="utf-8">
			<div id="dmc_details_top" class="dialog_wrapper" style="padding-bottom:0px; position:relative;">
				<div class="labeled-input">
					<label for="dmc_title">Section Title</label>
					<input class="defaultTextBox2 validate[required]" style="width:300px;" maxlength="255"  type="text" id="dmc_header"/>
				</div>

				<div class="details_column" style="vertical-align:top;">
					<div class="labeled-input">
						<label for="dmc_columnCount">Number Of Columns</label>
						<select class="defaultSelectBox" style="width: 60px" id="dmc_columnCount">
							<option value='auto'>auto</option>
							<option value='1'>1</option>
							<option value='2'>2</option>
							<option value='3'>3</option>
							<option value='4'>4</option>
							<option value='5'>5</option>
							<option value='6'>6</option>
							<option value='7'>7</option>
							<option value='8'>8</option>
							<option value='9'>9</option>
							<option value='10'>10</option>
						</select>
					</div>

					<div class="labeled-input">
						<label class="freeze-generated" id="dmc_startNumber_label" for="dmc_startNumber">Start Numbering At</label>
						<input class="freeze-generated validate[required,custom[isPosInteger]] validate[required] defaultTextBox2 intSpinner" type="text" id="dmc_startNumber" />
					</div>

					<div class="labeled-input">
						<label class="freeze-generated" id="dmc_questionCount_label" for="dmc_questionCount">Number Of Questions</label>
						<input class="freeze-generated validate[required,custom[isNonNegativeInteger]] defaultTextBox2 intSpinner" type="text" id="dmc_questionCount" />
					</div>
				</div>

				<div class="details_column" style="width:270px; vertical-align:top;">
					<div class="labeled-input">
						<label class="freeze-generated" id="dmc_mc_type_label" for="dmc_mc_type">Response Type</label>
						<select class="freeze-generated defaultSelectBox pad5px" style="height:28px; width:100px" id="dmc_mc_type">
							<option value='Letter'>a, b, c, &hellip;</option>
							<option value='TrueFalse'>True / False</option>
						</select>
						<select class="freeze-generated defaultSelectBox pad5px" style="display:none; height:28px; width:100px" id="dmc_mc_type_many" disabled>
							<option>many</option>
						</select>
					</div>

					<div class="labeled-input">
						<label class="freeze-generated" id="dmc_mc_choiceCount_label" for="dmc_mc_choiceCount">Number Of Choices</label>
						<input id="dmc_mc_choiceCount" class="freeze-generated validate[required,min[1],max[26]] defaultTextBox2 intSpinner" type="text" />
						<input id="dmc_mc_choiceCount_many" class="freeze-generated defaultTextBox2" style="display:none; width:40px;" type="text" value="many" disabled />
					</div>
					
					<div id="dmc_options_note"></div>
				</div>
			
				<div class="ui-dialog-buttonset custom-buttonpane" style="position:absolute; bottom:10px; right:15px;">
					<button type="button" class="btn btn-cancel btn_font">Cancel</button>
					<button type="button" class="btn btn-save btn_font">Save</button>
				</div>
			</div>
			<div id="dmc_details_bottom" class="dialog_wrapper" style="background:rgb(215, 231, 238); position:relative;">
				<div class="labeled-input">
					<div>
						<label style="display:inline-block; text-align:left; margin-bottom:0px; width:auto;">Answer Key (optional)</label>			
						<button class="btn btn-primary btn-mini btn_font" id="mcKeyClearAll">Clear all</button>

						<div style='font-size:8pt; margin-bottom:10px;'>(Click on question number for more options)</div>
					</div>
					<div id="mc_answer_key" style="width: 610px;"></div>
				</div>
			</div>
		</form>	     
	</div>

	<!-- {* add nr section dialog *} -->
	<div id="addNRSection_dialog" class="dialogbox" title="NR Section Details">
		<form class="formular" accept-charset="utf-8">
			<div id="nr_detail_wrapper" class="dialog_wrapper" style="padding:10px 15px 10px 10px; border-bottom-left-radius: 5px; width:370px; height:310px; position:relative;">
				<div class="labeled-input">
					<label for="dnr_header">Section Title</label>
					<input class="defaultTextBox2 validate[required]" maxlength="255" type="text" id="dnr_header"/>
				</div>
	
				<div class="labeled-input">
					<label for="dnr_nr_style">Field Style</label>
					<select class="defaultSelectBox" id="dnr_nr_style" style="width:214px;">
						<option value="Default">Digits and decimals</option>
						<option value="Negatives">Allow negative answers</option>
						<option value="Fractions">Allow fractions and negatives</option>
					</select>
				</div>
		
				<div class="labeled-input">
					<label for="dnr_nr_digitCount">Number Of Blanks</label>
					<input class="validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner" type="text" id="dnr_nr_digitCount" />
				</div>
	
				<div class="labeled-input">
					<label for="dnr_columnCount">Number Of Columns</label>
					<select class="defaultSelectBox" style="width: 60px" id="dnr_columnCount">
						<option value='auto'>auto</option>
						<option value='1'>1</option>
						<option value='2'>2</option>
						<option value='3'>3</option>
						<option value='4'>4</option>
						<option value='5'>5</option>
						<option value='6'>6</option>
						<option value='7'>7</option>
						<option value='8'>8</option>
						<option value='9'>9</option>
						<option value='10'>10</option>
					</select>
				</div>
	
				<div class="labeled-input">
					<label for="dnr_startNumber">Start Numbering At</label>
					<input class="validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner" type="text" id="dnr_startNumber" />
				</div>
	
				<div class="ui-dialog-buttonset custom-buttonpane" style="position:absolute; bottom:10px; right:15px;">
					<button type="button" class="btn btn-cancel btn_font">Cancel</button>
					<button type="button" class="btn btn-save btn_font">Save</button>
				</div>
			</div><div id="nr_create_wrapper" class="dialog_wrapper" style="background:rgb(215, 231, 238); padding:10px 10px 10px 15px; border-bottom-right-radius: 5px; width:300px; height:310px;">
				
				<label style="text-align:left;">Create simple NR questions (optional)</label>
				<div class="labeled-input" style="margin-bottom:20px;">
					<input class="validate[custom[isNonNegativeInteger],max[50]] defaultTextBox2 intSpinner" value="0" data-prompt-position="topLeft" type="text" id="dnr_questionCount" />
					<label for="dnr_questionCount" style="width:200px; text-align:left; margin-left:5px;">Number Of Questions</label>
				</div>
				
				<div style="height:200px; padding:5px; overflow-y: scroll; border:1px solid #4a9cda;" id="dnr_question_list"></div>
 			</div>
			
		</form>
	</div>

	<!-- {* edit nr section dialog *} -->
	<div id="editNRSection_dialog" class="dialogbox" title="NR Section Details">
		<form class="formular" style="margin-bottom:0px" accept-charset="utf-8">
			<div class="dialog_wrapper" style="padding-bottom:0px; position:relative;">
				<div class="labeled-input">
					<label>Section Title</label>
					<input class="defaultTextBox2 validate[required]" maxlength="255" type="text" id="enr_header"/>
				</div>

				<div class="labeled-input">
					<label>Field Style</label>
					<select class="defaultSelectBox" id="enr_nr_style" style="width:274px;">
						<option value="Default">Digits and decimals</option>
						<option value="Negatives">Allow negative answers</option>
						<option value="Fractions">Fractions and negatives</option>
					</select>
				</div>
	
				<div class="details_column" style="vertical-align:top;">
					<div class="labeled-input">
						<label class="freeze-generated" id="enr_startNumber_label">Start Numbering At</label>
						<input class="freeze-generated validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner" type="text" id="enr_startNumber" />
					</div>

					<div class="labeled-input">
						<label>Number Of Columns</label>
						<select class="defaultSelectBox" style="width: 60px" id="enr_columnCount">
							<option value='auto'>auto</option>
							<option value='1'>1</option>
							<option value='2'>2</option>
							<option value='3'>3</option>
							<option value='4'>4</option>
							<option value='5'>5</option>
							<option value='6'>6</option>
							<option value='7'>7</option>
							<option value='8'>8</option>
							<option value='9'>9</option>
							<option value='10'>10</option>
						</select>
					</div>		
				</div>
	
				<div class="details_column" style="width:195px; margin-left:20px; vertical-align:top;">
					<div class="labeled-input">
						<label class="freeze-generated" for="enr_questionCount">Number Of Questions</label>
						<input class="freeze-generated validate[required,custom[isNonNegativeInteger]] defaultTextBox2 intSpinner" value="0" data-prompt-position="topLeft" type="text" id="enr_questionCount" />
					</div>

					<div class="labeled-input">
						<label class="freeze-generated" id="enr_nr_digitCount_label" style="width:130px;">Number Of Blanks</label>
						<input id="enr_nr_digitCount" class="freeze-generated validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner" type="text" />
						<input id="enr_nr_digitCount_many" class="freeze-generated defaultTextBox2" style="display:none; width:40px;" type="text" value="many" disabled />
					</div>
					
					<div id="enr_options_note" style="margin-left:20px;"></div>
				</div>
				<div style="margin:10px 5px; height:30px;">
					<div class="ui-dialog-buttonset custom-buttonpane" style="float:right;">
						<button type="button" class="btn btn-cancel btn_font">Cancel</button>
						<button type="button" class="btn btn-save btn_font">Save</button>
					</div>
				</div>
			</div>
		</form>	     
	</div>

	<!-- {* edit NR question dialog *} -->
	<div id="editNRQuestion_dialog" class="dialogbox" title="Manage NR Question">
		<div class="dialog_wrapper">
			<form class="formular" accept-charset="utf-8">
				<input type="hidden" id="dnq_item_data" value="" />
				
				<div class="labeled-input" style="float:left; width:auto;">
					<label id="dnq_digitCount_label" for="dnq_digitCount" style="width:40px; text-align:left;">Blanks </label>
					<input class="validate[required,custom[isPosInteger],min[1]] defaultTextBox2 intSpinner" style="width:50px;" type="text" id="dnq_digitCount" />
				</div>
				<div class="labeled-input" style="float:right; width:auto; margin-right:5px;">
					<label id="dnq_numberingType_label" style="text-align:left; width:auto;">Question</label>
					<select class="defaultSelectBox pad5px" style="height: 28px; width: 70px" id="dnq_numberingType">
					</select>
				</div>
				<div style="clear:both;"></div>
	
				<div class="labeled-input" style="float:left; width:auto;">
					<label id="dnq_answerFormat_label" for="dnq_answerFormat" style="width:40px; text-align:left;">Type</label>
					<select class="defaultSelectBox pad5px" style="height: 28px; width: 100px" id="dnq_answerFormat">
						<option value="nr">No type</option>
						<option value="nr_decimal" selected>Decimal</option>
						<option value="nr_scientific">Scientific</option>
						<option value="nr_fraction">Fraction</option>
						<option value="nr_selection">Selection</option>
					</select>
				</div>
				<div class="labeled-input" style="float:right; width:auto; margin-right:5px;">
					<label id="dnq_scoring_label" for="dnq_scoring" style="width:auto; text-align:left;">Scoring</label>
					<select class="defaultSelectBox pad5px" style="height: 28px; width: 120px" id="dnq_scoring">
						<option value="Default">Regular scoring</option>
						<option value="Omit">Omit question</option>
						<option value="Bonus">Bonus question</option>
					</select>
				</div>
				<div style="clear:both;"></div>
	
				<div id="dnq_detailWrapper">
					<div id="nrOptions">
						<div style="clear:both; height:5px;"></div>
						<label for="dnq_answerWrapper" style="text-align:left;">Scored responses</label>
						<div id="dnq_answerWrapper"></div>
						
						<div id="numberOptions">
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" class="normal_chkbx" id="dnq_allowSmallErrors" /><label for="dnq_allowSmallErrors">Allow small errors</label>
							</div>
							<div id="dnq_fudgeFactorWrapper" class="labeled-input">
								<label for="dnq_fudgeFactor" style="margin-left:25px;">Tolerance (%):&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[100]] defaultTextBox2" style="width:134px !important;" type="text" id="dnq_fudgeFactor" />
							</div>
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" class="normal_chkbx" id="dnq_allowFactorOfTen" \><label for="dnq_allowFactorOfTen">Give part marks for factor-of-10</label>
							</div>
							<div id="dnq_tensValueWrapper" class="labeled-input">
								<label for="dnq_tensValue" style="margin-left:25px;">Multiplier:&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2" data-prompt-position="topLeft" style="width:167px !important;" type="text" id="dnq_tensValue" />
							</div>
							<div class="labeled-input" style="margin:10px 0px 5px;">
								<label style="text-align:left; padding:0px;">If significant digits in student response are incorrect:</label>
							</div>
							<div class="labeled-input" style="margin-left:15px;">
								<select class="defaultSelectBox pad5px" style="height: 28px; width: 145px" id="dnq_sigDigsBehaviour">
									<option value="Round">Round as needed</option>
									<option value="Zeros" selected>Add zeros as needed</option>
									<option value="Strict">Mark as incorrect</option>
								</select>
								<div id="dnq_sigDigsValueWrapper">
									<span style="font-size:13px; font-family:Arial, Helvetica, sans-serif;">and multiply score by&nbsp;</span><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2" data-prompt-position="topLeft:-275,30" style="width:25px !important; margin:0px;" type="text" id="dnq_sigDigsValue" />
								</div>
							</div>
						</div>
						<div id="selectionOptions">
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" class="normal_chkbx" id="dnq_markByDigits" /><label for="dnq_markByDigits">Marks are per correct digit</label>
							</div>
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" class="normal_chkbx" id="dnq_allowPartialMatch" /><label for="dnq_allowPartialMatch">Allow partial match</label>
							</div>
							<div id="dnq_partialValueWrapper" class="labeled-input">
								<label for="dnq_partialValue" style="margin-left:25px;">Multiplier:&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2" style="width:167px !important;" type="text" id="dnq_partialValue" />
							</div>
							<div class="labeled-input" style="margin-bottom:0px;">
								<input type="checkbox" class="normal_chkbx" id="dnq_ignoreOrder" /><label for="dnq_ignoreOrder">Ignore selection order</label>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- {* add wr section dialog *} -->
	<div id="editWRSection_dialog" class="dialogbox" title="WR Section Details">
		<div class="dialog_wrapper">
			<form class="formular" accept-charset="utf-8">
				<div class="labeled-input">
					<label for="dwr_header">Section Title</label>
					<input class="defaultTextBox2 validate[required]" maxlength="255" type="text" id="dwr_header"/>
				</div>
	
				<div class="labeled-input">
					<input type="checkbox" class="normal_chkbx" id="dwr_wr_includeSpace" /><label for="dwr_wr_includeSpace">Include space for student responses</label>
				</div>
	
				<div class="details_column">
					<div class="labeled-input">
						<label class="freeze-generated" for="dwr_startNumber">Start Numbering At</label>
						<input class="freeze-generated validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner" type="text" id="dwr_startNumber" />
					</div>
					<div class="labeled-input">
						<label for="dwr_columnCount">Number Of Columns</label>
						<input class="validate[required,custom[isPosInteger]] defaultTextBox2 intSpinner" data-prompt-position="topLeft" type="text" id="dwr_columnCount" />
					</div>
				</div>
	
				<div class="details_column" style="margin-left:10px; vertical-align:top;">
					<div class="labeled-input" id="dwr_questionCount_wrapper">
						<label class="freeze-generated" for="dwr_questionCount">Number Of Questions</label>
						<input class="freeze-generated validate[required,custom[isNonNegativeInteger]] defaultTextBox2 intSpinner" value="0" data-prompt-position="topLeft" type="text" id="dwr_questionCount" />
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- {* add new WR question dialog *} -->
	<div id="editWRQuestion_dialog" class="dialogbox" title="Manage WR Question">
		<div class="dialog_wrapper" style="height:100%;">
			<form class="formular" accept-charset="utf-8">
				<div style="display:flex; flex-direction:column; height:100%;">
					<div style="flex:none; display:flex; flex-direction:row; justify-content:space-between; width:100%;">
						<div class="labeled-input" style="width:auto;">
							<label id="dwq_numberingType_label" for="dwq_numberingType">Question </label>
							<select class="defaultSelectBox pad5px" style="height: 28px; width: 70px" id="dwq_numberingType">
							</select>
						</div>
						<div class="labeled-input" style="width:auto;">
							<label id="dwq_scoring_label" for="dwq_scoring" style="width:auto; text-align:left;">Scoring</label>
							<select class="defaultSelectBox pad5px" style="height: 28px; width: 120px" id="dwq_scoring">
								<option value="Default">Regular scoring</option>
								<option value="Omit">Omit question</option>
								<option value="Bonus">Bonus question</option>
							</select>
						</div>
					</div>
		
					<div id="dwq_detailWrapper" style="flex:1 0 auto; display:flex; flex-direction:column;">
						<div id="dwq_spaceWrapper"  style="display:flex; flex-direction:column;">
							<div style="flex:none; display:flex; flex-direction:row; justify-content:space-between; width:100%;">
								<div class="labeled-input" style="width:auto;">
									<label for="dwq_height" style="text-align:left; width:50px;">Height:</label>
									<input class="validate[required,max[10.0],min[0.5]] marginSpinner" value="0" data-prompt-position="topLeft" type="text" id="dwq_height" /> <label>in</label>
								</div>
								<div class="labeled-input" style="width:auto;">
									<label style="text-align:left; width:75px;">Prompt type:</label>
									<select class="defaultSelectBox pad5px" style="height: 28px; width: 70px" id="dwq_prompt_type">
										<option value="none">None</option>
										<option value="text" selected>Text</option>
										<option value="image">Image</option>
									</select>
								</div>
							</div>
							<div id="dwq_text_wrapper" style="flex:1 0 auto; display:flex; flex-direction:column;">
								<div class="labeled-input" style="flex:1 0 auto; display:flex; flex-direction:column; width:100%;">
									<label for="dwq_prompt_text" style="flex:none; text-align:left; width:auto;">Prompt text:</label>
									<textarea class="validate[required] defaultTextBox2" style="flex: 1 0 auto; box-sizing:border-box; width:100%; resize:none; min-height:100px;" id="dwq_prompt_text"></textarea>
								</div>			
							</div>
							<div id="dwq_image_wrapper" style="flex:1 0 auto; display:flex; flex-direction:column;">
								<input type="hidden" id="dwq_prompt_image_name" />
								<input type="hidden" class="uploadParameter" name="jobName" id="prompt_image_target">
								<input type="hidden" id="prompt_image_extension">

								<div class="labeled-input" style="flex:1 0 auto; display:flex; flex-direction:column; width:auto;">
									<label style="text-align:left;">Prompt image:</label>
									<div style="flex:1 0 auto; display:flex; flex-direction:row;">
										<div id="dwq_prompt_image" style="flex:1 0 120px; padding:5px; background:white; border:1px solid #ccc; position:relative; box-sizing:border-box;"><div id="dwq_prompt_image_blank" style="position:absolute; top:0; bottom:0; left:0; right:0; margin:auto; width:100px; height:30px; line-height:30px; text-align:center; color:#ccc;">No image</div></div>
										<div style="flex:none; margin-left:10px;">
											<div class="labeled-input" style="width:auto;">
												<label style="text-align:left; display:inline-block; margin-bottom:10px; vertical-align:middle;">Image height:</label>
												<input style="vertical-align:middle;" class="validate[max[10.0],min[0.5],required] marginSpinner" type="text" id="dwq_prompt_image_height" />
												<label style="display:inline-block; width:auto; margin-bottom:10px; vertical-align:middle;">in</label>
											</div>
											<div id="prompt_fileInterface" style="position:relative; height:80px; width:230px; margin-top:20px !important;">
												<div id="prompt_filePrompt" style="position:absolute; height:100%; width:100%; border-radius: 15px; opacity:0.0; border-style:solid; border-width:3px; border-color:black; text-align:center;">
													<h2>Drop your file here</h2>
												</div>
												<div id="prompt_inputs" style="position:absolute; height:100%; width:100%; opacity:1.0;">
													<div style="padding-bottom:10px;">
														<div id="prompt_select_button" class="btn btn-small fileinput-button" style="height:20px; display:inline-block; vertical-align:middle;">
															Select new
															<input id="prompt_fileupload" type="file" name="files[]" accept="image/jpeg|image/png">
														</div>
														<div id="prompt_filename" style="padding-left:10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; width:130px; display:inline-block; white-space:nowrap; overflow:hidden; text-overflow:ellipsis; vertical-align:middle;">No file selected</div>
													</div>
													<div style="clear:both; height:20px; padding-bottom:10px;">
														<div id="prompt_upload_button" class="btn btn-small fileinput-button pull-left" disabled>Upload</div>
														<div id="prompt_progressbar" class="pull-right" style="width:150px; height:25px; margin-right:10px; display:none;"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div style="flex:none; width:100%; margin-top:5px;">
							<label for="dwq_criteriaWrapper" style="text-align:left; display:inline-block;">Criteria:</label><button class="btn btn-mini btn_font" style="margin-left:5px;" id="editRubric">Edit rubric</button>

							<div id="dwq_criteriaWrapper" style="width:100%; margin:5px 0px 0px;"></div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- {* edit rubric dialog *} -->

	<div id="editRubric_form" class="dialogbox" title="Edit Rubric">
		<div class="dialog_wrapper" style="height:100%;">
			<form class="formular" id="rubric_edit" accept-charset="utf-8" style="height:100%;">
				<div class="labeled-input" style="display:flex; width:100%; height:100%;">
					<div style="flex: 1 1 0px;" id="dwq_rubric_edit" class="template_view"></div>
				</div>
			</form>
		</div>
	</div>

	<!-- {* add new category dialog *} -->
	<div id="editOutcome_dialog" class="dialogbox" title="Manage Learning Outcome">
		<div class="dialog_wrapper">
			<form class="formular" style="margin-bottom:0px;" accept-charset="utf-8">
				<div class="labeled-input">
					<label style="text-align:left; width:95px;">Outcome Name:</label>
					<input class="validate[required] defaultTextBox2" type="text" id="outcome_name" />
				</div>

				<div class="labeled-input" style="margin-top:20px;">
					<div style="width:100%;">
						<label for="cat_data" style="text-align:left; display:inline-block; width:95px;">Parts included:</label>
						<div style="display:inline-block;">
							<button type="button" class="btn btn-mini btn_font" id="outcomes_checkAll">Check all</button>
							<button type="button" class="btn btn-mini btn_font" id="outcomes_clearAll">Clear all</button>
						</div>
					</div>
					<div id="outcome_selection_list" style="display:block;"> </div>
				</div>
			</form>
		</div>
	</div>

	
	<!-- {* version conflict dialog *} -->

	<div id="version_conflict_dialog" class="dialogbox" title="Version conflict">
		<div class="dialog_wrapper" style="display:flex; flex-flow:space-between; align-items:flex-start; gap:10px;">
			<div style="flex:1 1 auto; margin-top:5px;">
				<img style="height:50px;" src="/img/error.svg" />
			</div>
			<div style="flex:1 1 100%; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">
				<p>It looks like this form is being edited from another window, or by another teacher.</p>
				<p>This page will need to be reloaded, clearing your changes, before you can continue. Click "Go back" 
				to close this dialog, or "Reload" to reload the page.</p>
			</div>
		</div>
	</div>

<!-- Print from class list dialog -->

    <div id="buildClassPDF_dialog" class="dialogbox" title="Print from class list">
		<div class="dialog_wrapper">
            <p>Choose a class list below, then select or unselect students as needed. Click "Print" to print forms for the 
            selected students.</p>

            <?php echo $this->element("class_list_common"); ?>
            
		</div>
	</div>
	
</div>

<?php echo $this->element('html_edit'); ?>

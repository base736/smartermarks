<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
echo $this->element('layout_head', array(
    'include_print_element' => true
));
?>

<script type="text/javascript">

var documentScoring = <?php echo json_encode($scoring_data); ?>;
var documentLayout = <?php echo json_encode($layout_data); ?>;

</script>

<script type="module" src="/js/responseforms/print_key.js?7.0"></script>

<style>
	body {
		-webkit-print-color-adjust:exact;
		background-color:white; 
		margin:0px;
	}

	@page  
	{ 
    	size: auto;
	}
</style>

</head>

<body>
	<div class='key_title'>
		<?php
			foreach ($layout_data['Title']['text'] as $part) {
				echo "<div>" . htmlspecialchars($part) . "</div>";
			}
		?>
		<div>Answer key</div>
	</div>
	<div id="key_wrapper" class="assessment_wrapper template_view"></div>
</body>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->
    
<script type="text/javascript">
	<?php if (!empty($jsonQuestionTree)) { ?> var questionTreeData = <?php echo $jsonQuestionTree; } ?>;
    var classLists = <?php echo json_encode($classLists); ?>;
</script>

<script type="module" src="/js/responseforms/index.js?7.0"></script>

<div style="grid-column:right; grid-row:actions" class="action_bar<?php if (!$adminView) echo ' right_half' ?>">
	<div class="action_left">
		<div class="checkbox_arrow"></div>
		<div class="index_actions trash_actions">
			<div class="btn-group">
				<button type="button" class="btn btn-dark item_action_many moveItem_button"><span>Move</span></button>
			</div>
		</div>
		<div class="index_actions default_actions">
			<div class="btn-group">
				<div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Build 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="editDocument_button" class="item_action_one">Edit form</a></li>
						<li><a href="#" id="duplicateItem_button" class="item_action_many <?php echo (!isset($user['role']) || $user['role'] != 'admin') ? 'needs_copy' : 'always-disabled-link'; ?>">Duplicate</a></li>
						<li><a href="#" id="newVersion_button" class="item_action_one not_generated needs_copy">New version map</a></li>

                        <?php 
                        if (isset($user['role']) && ($user['role'] == 'admin')) { 
                        ?>

                        <li class="dropdown-divider"></li>
                        <li><a href="#" id="downloadXML_button" class="item_action_one">Download XML</a></li>

                        <?php
                        }
                        ?>
                    </ul>
				</div>

				<div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Print 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="buildQuestions_button" class="item_action_one needs_generated">Question book</a></li>
						<li><a href="#" id="buildGenericPDF_button" class="item_action_one">Generic form</a></li>
						<li><a href="#" id="buildClassPDF_button" class="item_action_many">From class list</a></li>
						<li><a href="#" id="buildKey_button" class="item_action_one">Answer key</a></li>
					</ul>
				</div>

				<div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many needs_v3" data-toggle="dropdown" href="#">
						Score
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="newScans_button" class="item_action_many needs_v3">Mark new scans</a></li>
						<li><a href="#" id="browseScans_button" class="item_action_one">Browse scores</a></li>
					</ul>
				</div>
			</div>

			<div class="btn-group">
				<div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Manage 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" class="<?php echo (!isset($user['role']) || $user['role'] != 'admin') ? 'item_action_many needs_delete moveItem_button' : 'always-disabled-link'; ?>">Move to folder</a></li>
						<li><a href="#" class="<?php echo (!isset($user['role']) || $user['role'] != 'admin') ? 'item_action_many copyItem_button' : 'always-disabled-link'; ?>">Copy to folder</a></li>
						<li><a href="#" id="deleteItem_button" class="item_action_many needs_delete">Move to trash</a></li>
					</ul>
				</div>
				<div class="btn-group">
					<button id="share_dropdown" class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Share 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="sendCopy_button" class="item_action_many needs_copy">Send a copy</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="action_right">
		<div class="btn-group">
			<button class="btn btn-dark search_dropdown dropdown-toggle" href="#">
				Search 
				<span class="caret"></span>
			</button>
			<div class="pull-right search_menu">
				<?php
				if (!$adminView) {
				?>	

				<div id="search_options" style="margin-bottom:5px;">
					<div style="vertical-align:middle; display:inline-block;">Search:</div>
					<div style="vertical-align:middle; display:inline-block;">
						<select id="search_type" style="margin:0px; width:160px;">
							<option value="folder">in this folder</option>
							<option value="subfolders">with sub-folders</option>
							<option value="all">in all folders</option>
						</select>
					</div>
				</div>

				<?php
				}
				?>
				
				<div id="search_wrapper" style="position:relative; width:auto;">
					<input class="search_text" type="text" />
					<div class="search_icon"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="grid-column:right; grid-row:main; display:flex; flex-direction:column;">
	<div class="window_bar<?php if (!$adminView) echo ' right_half' ?>" style="flex:none; margin-top:1px; padding-left:10px;">
        <div style="width:5px">&nbsp;</div>
		<?php
		if ($adminView) {
		?>	
		<div style="width:250px;">Owner</div>
		<?php
		}
		?>
		<div style="width:120px;">Created</div>
		<div style="width:auto;">Name</div>

		<div class="sort_wrapper">
			<div style="vertical-align:middle; display:inline-block; line-height:normal;">
				<div class="header_sort" data-direction="asc" style="display:block;">▲</div>
				<div class="header_sort" data-direction="desc" style="display:block;">▼</div>
			</div>
			<span>Sort by</span>
			<select id="sort_type">
				<option value="<?php echo $colName; ?>" data-default-direction="asc">Name</option>
				<option value="<?php echo $colCreated; ?>" data-default-direction="desc" selected="selected">Created</option>
			</select>
            <?php
            if (!$adminView) {
            ?>
			<span style="margin-left:10px;">Show</span>
			<select id="show_limit">
                <option value="10">10</option>
                <option value="all">all</option>
			</select>
            <?php
            }
            ?>
		</div>
	</div>
	<div id="index_right" class="index_body<?php if (!$adminView) echo ' right_half' ?>" style="flex:1 1 100%; overflow:hidden; min-height:300px; width:100%;">
		<table class="index_table selection_table">
		</table>
		<div id="footer_wrapper" style="display:flex; justify-content:space-between; height:auto; margin:20px 10px;">
			<div id="count_wrapper" style="color:#75797c;"></div>
			<div id="pagination_wrapper" class="pagination_wrapper"></div>
		</div>
	</div>
</div>

<div class="hidden">

<!-- Print question book dialog -->

	<div id="buildQuestions_dialog" class="dialogbox" title="Build question book">
		<div class="dialog_wrapper">
			<p>Question books can optionally include a response form in the generated PDF. Choose from the options below, then click "Build" to continue.</p>
			<form>
				<input type="hidden" id="buildQuestions_docId" />
                <div class="labeled-input">
					<label style="display:inline-block; width:auto;">Build with:</label>
					<select class="defaultSelectBox" style="width:180px;" id="buildQuestions_form">
						<option value="none">question book only</option>
						<option value="front">response form at the front</option>
						<option value="back">response form at the back</option>
					</select>
                </div>
			</form>
		</div>
	</div>
	
<!-- New version dialog -->

	<div id="newVersion_dialog" class="dialogbox" title="Version map">
		<div class="dialog_wrapper">
			<p>Version maps can simplify the versioning of assessments, and are produced by many test generating programs. 
			Using a version map, you can build multiple versions of an assessment without the need to re-enter learning 
			outcomes.</p>

			<p>The version map goes from question numbers in the original assessment <span id='version_map_original'></span>
			to those in the new version. For example, if question 1 in the original assessment is the same as question 4 of 
			the new version, you would enter '4' in the first box below.</p>

			<p><b>Note:</b> If answers are also shuffled between versions, you will need to enter a new answer key for the
			generated assessment.</p>

			<form class="formular" style="margin-bottom:0px;" id="versionForm" accept-charset="utf-8">
		 	   <div id="version_data_wrapper"></div>
			</form>
		</div>
	</div>

    <!-- Print from class list dialog -->

    <div id="buildClassPDF_dialog" class="dialogbox" title="Print from class list">
		<div class="dialog_wrapper">
            <p>Choose a class list below, then select or unselect students as needed. Click "Print" to print forms for the 
            selected students.</p>

            <?php echo $this->element("class_list_common"); ?>
            
		</div>
	</div>

</div>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<?php 
	if (empty($user['hidden_tips'])) $hiddenTips = array();
	else $hiddenTips = explode(";", $user['hidden_tips']); 
?>

<link rel="stylesheet" type="text/css" href="/css/bubble_tip.css?7.0" />
<script type="text/javascript" src="/js/common/bubble_tip.js?7.0"></script>

<script type="text/javascript">
	var assessmentID = <?php echo $assessment_id;?>;
	var canAdd = <?php echo $canAdd; ?>;
	var canEdit = <?php echo $canEdit; ?>;
	var canCopy = <?php echo $canCopy; ?>;
	var userID = <?php echo $user['id']; ?>;
	var userEmail = <?php echo json_encode($user['email']); ?>;
	var canCancel = <?php echo $canCancel; ?>;
	var showSettings = <?php echo $showSettings; ?>;
	var userDefaults = <?php echo $user['defaults']; ?>;
	var hasSmartFill = <?php echo in_array('fill', explode(',', $user['smart_allow'])) ? 'true' : 'false'; ?>;
    var showCorrelation = <?php echo $user['correlations_beta'] ? 'true' : 'false'; ?>;

	var userdataPrefix = "<?php 
		$secrets = Configure::read('secrets'); 
		echo $secrets['s3']['userdata_prefix'];
	?>";
	
	var showIntroNote = <?php echo in_array("assessments_build", $hiddenTips) ? '0' : '1'; ?>;
	
	var questionFolderId = <?php echo empty($questionFolderId) ? 'null' : $questionFolderId; ?>;
	var formFolderId = <?php echo empty($formFolderId) ? 'null' : $formFolderId; ?>;
	var sittingFolderId = <?php echo empty($sittingFolderId) ? 'null' : $sittingFolderId; ?>;
	var communityID = <?php echo empty($communityId) ? 'null' : $communityId; ?>;
	
    var classLists = <?php echo json_encode($classLists); ?>;
</script>

<script type="application/json" id="treeData"><?php echo empty($jsonTree) ? '' : $jsonTree; ?></script>
<script type="application/json" id="formTreeData"><?php echo empty($jsonFormTree) ? '' : $jsonFormTree; ?></script>
<script type="application/json" id="sittingTreeData"><?php echo empty($jsonSittingTree) ? '' : $jsonSittingTree; ?></script>

<script type="module" src="/js/assessments/build.js?7.0"></script>

<!-- Top bar -->

<div class="action_bar" style="margin-bottom:5px;">
	<div class="action_left" style="color:#ccc;">
		<div id="document_title"></div>
	</div>
	
	<div class="action_right" style="align-self:flex-start;">
		<?php
		if ($canEdit) {
		?>
		<button id="document_options" type="button" class="btn btn-dark" style="width:80px;" disabled><span>Settings</span></button>
		<?php
		}
		?>
	</div>
</div>

<!-- Left Panel-->

<div id="panel_wrapper" style="position:relative; width:890px; height:auto;">
	<div id="left_panel" style="width:295px; position:relative; float:left;">
	
		<!-- {* navigation *} -->
		<div class="view_panel">
		    <div class="window_bar">Views</div>
		    <div class="window_content" style="padding:10px;">
				<ul id="view_menu" style="padding:5px;" >
					<li class="view_button" data-view-type="preview"><a href="#">Sections and questions</a></li>
					<li class="view_button" data-view-type="outcomes"><a href="#">Learning outcomes</a></li>
					<li class="view_button" data-view-type="statistics"><a href="#">Assessment statistics</a></li>
				</ul>
		    </div>
		</div>
	
		<!-- {* sections *} -->
		<div id="sections_panel" style="display:none;">
			<div class="window_bar">Sections</div>
			<div class="window_content" style="padding:5px; overflow:visible;">
				<div class="layout_panel">
					<div class="empty_layout" style="width:255px; padding:10px; color:#eee; font-size:13px; font-style:italic; display:none;">No sections built for this assessment.<span class="edit_control"> Click "Add Section" below to begin.</span></div>
					<ul id="preamble_menu" class="menu_wrapper" style="display:none;">
						<li><div class="menu_item"><div class="menu_text">Preamble</div><?php if ($canEdit) { ?><button title="Edit" class="edit_control menu_button edit_icon"></button><button title="Delete" class="edit_control menu_button delete_icon"></button></li><?php } ?>
					</ul>
					<ul id="sections_menu" class="menu_wrapper"></ul>

					<?php
					if ($canEdit) {
					?>

					<div class="edit_control" style="margin-top:10px;">
						<span style="color:#cccccc; font-size:13px; vertical-align:middle; margin-left:4px; margin-right:6px;">Add:</span>
						<div class="btn-group" style="margin-left:2px;">
							<button class="btn btn-small btn-dark addSection" data-type="mixed">Section</button>
						</div><div class="btn-group" style="margin-left:2px;">
							<button class="btn btn-small btn-dark" id="addPreamble">Preamble</button>
						</div><div class="btn-group" style="margin-left:2px;">
							<button class="btn btn-small btn-dark addPageBreak">Page Break</button>
						</div>
					</div>
					
					<?php
					}
					?>
				</div>
			</div>
		</div>		
	</div>
	
	<!-- Right Panel-->
	
	<div style="width:585px; float:right;">
		<div class="right_panel" id="loading_panel" style="display:block;">
			<div class="window_bar">Loading assessment</div>
			<div class="window_content" style="background-color:white; padding:10px;">
				<div><div class="wait_icon"></div><i>Loading&hellip;</i></div>
			</div>
		</div>

		<div class="right_panel" id="preview_panel">
			<div class="window_bar">Assessment preview</div>
			<div class="window_content" style="background-color:white; padding:10px;">
			
				<?php 
				if ($canBuild) { 
				?>

                <div style="display:flex; flex-direction:row; gap:10px; width:100%;">

                    <div class="btn-group" style="flex:none; width:auto; margin:0px;">
                        <button id="title_add_notes" class="notes_add btn" href="#" disabled>Add notes</button>
                    </div>

                    <div style="flex:1 1 100%;"></div>
                                    
                    <div class="btn-group" style="flex:none; width:auto; margin:0px;">
                        <button id="reloadButton" class="btn buildButton" href="#" disabled>Reload</button>
                        <button id="shuffleItems" class="btn buildButton" href="#" disabled>Shuffle</button>
                    </div>

                    <div class="btn-group" style="flex:none; width:auto; margin:0px;">
                        <button id="preview_pdf" class="btn buildButton" href="#" disabled>Preview</button>
                    </div>

                    <div class="btn-group" style="flex:none; width:auto; margin:0px;">
                        <button class="btn buildButton" data-toggle="dropdown" href="#" disabled>
                            Build
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#" id="build_version">Build paper version</a></li>

                            <?php if ($user['id'] == 7) { ?>
                            <li><a href="#" id="build_set">Set from class list</a></li>
                            <?php } ?>

                            <li><a href="#" id="build_online">Build online sitting</a></li>
                        </ul>
                    </div>

				</div>
				
				<?php 
				}
				?>
				
				<div id="highlighted_notice" class="item_highlighted" style="border:1px solid #ccc; margin-top:20px; padding:10px; display:none;">
					<p>This assessment contains questions or answers that should be modified to better measure students' level of understanding. Hover over highlighted elements for more information, and double-click questions to make changes.</p>
					<p>New to item statistics? <a href='https://www.youtube.com/watch?v=LkMErsoLAhI' target='_blank'>Click
					here</a> to learn more about average score and discrimination.</p>
				</div>
			
				<div id="assessment_empty_wrapper" style="margin-top:30px; margin-left:10px; font-size:14px; font-style:italic; height:50px; display:none;">This assessment is empty. Use the buttons in the left bar to begin adding content.</div>
				<div id="assessment_preview_wrapper" class="assessment_wrapper template_view" style="margin-top:20px; min-height:95px;">
				</div>
			</div>
		</div>
		
		<div class="right_panel" id="outcomes_panel" style="display:none;">

			<div class="window_bar">Selected Question</div>
		    <div id="outcomes_wrapper" class="window_content" style="padding:10px;">
		    	<div>
				    <div class="sub_panel_title">Question</div>
				    <div id="outcomes_item" class="assessment_wrapper template_view sub_panel_content" style="padding:10px 5px; min-height:50px; background-color:white"></div>
				</div>
			    <div id="outcomes_wrapper" style="margin-top:10px;">
				    <div class="sub_panel_title">Learning outcomes</div>
				    <div class="sub_panel_content">
						<table class="panel_table actions_table">
				            <thead>
				                <tr>
				                    <td>Name</td>
				                    <td width="10%" align="center">Count</td>
				                    <td>Actions</td>
				                </tr>
				            </thead>
				            <tbody id="itemOutcomes"></tbody>
				    	</table>
				        <div style="padding:10px;">
				            <?php if ($canEdit) { ?><button class="btn btn-primary editButton" id="editOutcomes">Edit outcomes</button><?php } ?>
							<div class="clear"></div>
				        </div>
				    </div>
			    </div>
		    </div>
		</div>

		<div class="right_panel" id="statistics_panel" style="display:none;">
			<div class="window_bar">Filters</div>
		    <div class="window_content" style="padding:10px; margin-bottom:10px; display:flex; justify-content:space-between; align-items:flex-start;">
			    <div>
					<div class="labeled-input" style="width:auto;">
						<label style="text-align:left; width:auto; padding-right:5px;">Include</label><select class="defaultSelectBox stats_filter_input" style="width:150px;" id="stats_included_sources">
							<option value="All" selected>all response types</option>
							<option value="Sitting">online sittings only</option>
							<option value="ResponsePaper">paper versions only</option>
						</select>
					</div>
					<div class="labeled-input" style="width:auto;">
						<div style="display:inline-block; vertical-align:top;">
							<input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_date" />
						</div><div style="display:inline-block; vertical-align:top; position:relative; top:2px;">
							<label for="stats_filter_by_date" style="text-align:left; width:auto;">Include only responses after date:</label>
							<input class="datepicker defaultTextBox2" type="text" id="stats_min_date"/>
						</div>
					</div>
					<div class="labeled-input" style="width:auto;">
						<input type="checkbox" class="normal_chkbx stats_filter_input" id="stats_filter_by_user" /><label for="stats_filter_by_user" style="text-align:left; width:auto;">Include only my responses</label>
					</div>
			    </div>
			    <div id="stats_button_wrapper" class="custom-buttonpane" style="display:inline-block;">
					<div class='wait_icon' style="margin:0px; display:none;"></div>
					<div class="btn-group" style="display:inline-block;">
						<button class="btn buildButton" id="stats_update">Update statistics</button>
					</div>
					<div class="btn-group" style="display:inline-block;">
						<button class="btn buildButton" id="stats_print" >Print</button>
					</div>
			    </div>
		    </div>

			<div class="window_bar">Assessment</div>
		    <div class="window_content" style="margin-bottom:10px; padding:10px; background-color:white;">
			    <div id="statistics_graphs" class="assessment_wrapper"></div>
		    </div>

			<div class="window_bar">Selected question</div>
		    <div class="window_content" style="padding:10px;">
		    	<div>
				    <div class="sub_panel_title">Question</div>
				    <div id="statistics_item" class="assessment_wrapper template_view sub_panel_content" style="padding:10px 5px; min-height:50px; background-color:white;"></div>
				</div>
			    <div id="item_statistics_wrapper" style="display:none; margin-top:10px;">
				    <div class="sub_panel_title">Statistics (<span id="statistics_n"></span> total responses)</div>
				    <div class="sub_panel_content" id="item_statistics_data" style="padding:10px 5px; background-color:white;">
				    </div>
			    </div>
		    </div>
		</div>
	</div>
	<div style="clear:both;"></div>
</div>

<!-- Dialog boxes-->

<div class="hidden">

    <!-- Assessment note -->

	<div id="note_template">
        <div class="assessment_note">
            <div style="width:100%; height:100%; display:flex; flex-direction:column; gap:10px;">
                <div style="flex:none; height:auto; width:100%; display:flex; flex-direction:row; justify-content:space-between;">
                    <div style="flex:none; width:auto; font-size:16px; font-weight:bold;">Notes</div>
                    <div style="flex:none; width:auto;">
                        <button title="Previous" class="menu_button prev_icon"></button>
                        <button title="Next" class="menu_button next_icon"></button>
                        <button title="Edit" class="menu_button edit_icon"></button>
                        <button title="Delete" class="menu_button delete_icon"></button>
                    </div>
                </div>
                <div class="note_content" style="flex:1 1 100%; width:100%; margin-bottom:10px;"></div>
            </div>
        </div>
    </div>

    <!-- Item context menu -->

	<div id="item_menu_template">
		<div class="item_menu_wrapper" style="height:35px; padding:5px;">
			<div style="flex:none; width:auto;">
				<div class="set_navigation">
					<div class="btn-group" style="display:inline-block;">
						<button class='btn btn-small set_prev'>◀</button>
						<button class='btn btn-small set_next'>▶</button>
					</div>
					<span class='set_label' style="margin-left:5px; font-weight:bold;"></span>
				</div>
			</div>

            <div style="flex:1 1 100%;"></div>
            
			<div class="item_menu btn-group" style="flex:none; width:auto;">
				<button class="btn btn-small item_menu_button" data-toggle="dropdown">
					Edit
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-right">
                    <li><a href='#' class='item_menu_edit'>View or edit question</a></li>
					<li><a href='#' class='item_menu_copy'>Replace with a new version</a></li>
					<li><a href='#' class='item_menu_bank'>Replace from question bank</a></li>
					<li><a href='#' class='item_menu_remove'>Remove question from section</a></li>
                    <li><a href='#' class='notes_add'>Add notes</a></li>
					<li class="dropdown-divider"></li>
					<li><a href='#' class='item_menu_set'>Create question set</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Remove outcome dialog -->

	<div id="removeOutcome_dialog" class="dialogbox" title="Remove Outcome">
		<div class="dialog_wrapper" style="min-height:70px;">
			<p>Remove the outcome "<span class="cat_delete_name"></span>" for this question?</p>
		</div>
	</div>

	<!-- Manage outcome dialog -->
	
	<div id="manageOutcome_dialog" class="dialogbox" title="Manage Learning Outcome">
		<div class="dialog_wrapper">
			<input type="hidden" id="cat_id" />

			<div style="display:grid; grid-template-columns:105px 1fr; row-gap:10px; margin-bottom:5px; font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:13px;">
				<div style="grid-row:1; grid-column:1; font-family:Arial, Helvetica, sans-serif;">Outcome Name:</div>
				<div id="cat_type" style="grid-row:1; grid-column:2; "></div>
				<div style="grid-row:2; grid-column:1; font-family:Arial, Helvetica, sans-serif;">Parts included:</div>
				<div style="grid-row:2; grid-column:2;">
					<button class="btn btn-mini btn_font" id="outcomes_checkAll">Check all</button>
					<button class="btn btn-mini btn_font" id="outcomes_clearAll">Clear all</button>
				</div>
			</div>
			<div id="outcome_selection_list" style="display:block;"> </div>
		</div>
	</div>

	<!-- Edit outcomes dialog -->
	
	<div id="editOutcomes_dialog" class="dialogbox" title="Edit Learning Outcomes">
		<div class="dialog_wrapper" style="padding-right:15px;">
			<form class="formular" accept-charset="utf-8">
				<input type="hidden" id="outcomes_json" />

				<div>
					<label style="text-align:left; display:inline-block;">Assessment outcomes:</label>
					<img id="outcomes_copy" style="display:inline-block; cursor:pointer; width:15px; margin:0px;" src="/img/icons/copy.svg" />
					<div id="edit_outcomes_list" class="outcomes_list_wrapper"></div>
				</div>

				<div style="margin-top:10px;">
					<label style="text-align:left;">Add outcomes:</label>
					<div class="outcomes_list_wrapper" style="margin-bottom:10px;">
						<div id="outcomes_add_row" class="outcomes_edit_row">
							<textarea type="text" class="edit_outcomes_area" style="max-height:100px; overflow-y:auto;"></textarea>
							<button title="Add" class="actions_button add_icon outcomeAdd">&nbsp;</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- edit settings dialog -->

	<div id="editSettings_form" class="dialogbox"  title="Edit Assessment Details">
		<form class="formular" id="settings_edit" accept-charset="utf-8">
			<div class="dialog_wrapper" id="editSettings_wrapper_left" style="padding:10px 15px 15px 15px; border-bottom-left-radius: 5px; width:395px; height:310px;">
				<input type="hidden" id="dset_title_nameLines" />
				<input type="hidden" id="dset_weights" />
				<div class="labeled-input">
					<label for="ddoc_title" style="width:90px; vertical-align:top; position:relative; top:5px;">Save As</label>
					<div style="display:inline-block;">
						<input class="validate[required] defaultTextBox2" maxlength="255" type="text" id="dset_save_name" style="margin-bottom:5px;"/>
						<span class="input_subtext">Name to be used when saving this assessment and browsing assessments.</span>
					</div>
				</div>
				<div class="labeled-input">
					<label for="dset_title_text" style="width:90px; vertical-align:top; position:relative; top:5px;">Default Title</label>
					<div style="display:inline-block;">
						<textarea class="validate[required] docTitleTextField defaultTextBox2" style="margin-bottom:5px; height:90px;" id="dset_title_text"></textarea>
						<span class="input_subtext">Assessment title to be included on the first page (can have multiple lines)</span>
					</div>
				</div>
				<div class="labeled-input">
					<label style="width:90px; vertical-align:top; position:relative; top:6px;">Name lines</label>
					<div id="dset_nameLines_wrapper" class="options_row" style="margin-top:5px; margin-bottom:0px; width:265px; vertical-align:top;"></div>
				</div>
				<div id="editSettings_buttons" class="ui-dialog-buttonset custom-buttonpane" style="float:right; margin-top:15px;">
					<button type="button" class="btn btn-cancel btn_font">Cancel</button>
					<button type="button" class="btn btn-save btn_font">Save</button>
				</div>
			</div><div id="editSettings_wrapper_right" style="display:inline-block; vertical-align:top; background:rgb(215, 231, 238); border-bottom-right-radius:5px; width:289px; height:auto; border-left:1px solid #ccc;">

				<div style="width:290px; margin-left:-1px; height:40px; background:rgb(235, 235, 235); position:relative; border-bottom:1px solid #ccc;">
					<div class="assessment_settings_tab selected" data-wrapper-id="layout_settings_wrapper">Layout</div>
					<div class="assessment_settings_tab" data-wrapper-id="scoring_settings_wrapper">Scoring</div>
				</div>

				<div id="layout_settings_wrapper" class="settings_wrapper">
					<div class="labeled-input">
						<label style="text-align:left; width:auto;">Page size:</label><select class="defaultSelectBox" style="width:100px;" id="dset_page_size">
							<option value="letter">Letter</option>
							<option value="legal">Legal</option>
							<option value="a4">A4</option>
						</select>
					</div>
	
					<div class="labeled-input">
						<label style="text-align:left; display:block; width:100%;">Margins (inches)</label>
						<div>
							<div class="margin_column">
								<div class="options_row" style="margin-bottom:10px;">
									<label for="dset_topMarginSize">Top</label><input class="validate[max[5.5],min[0]] marginSpinner" type="text" id="dset_topMarginSize"/>
								</div>
								<div class="options_row">
									<label for="dset_bottomMarginSize">Bottom</label><input class="validate[max[5.5],min[0]] marginSpinner" type="text" id="dset_bottomMarginSize"/>
								</div>
							</div><div class="margin_column">
								<div class="options_row" style="margin-bottom:10px;">
									<label for="dset_leftMarginSize">Left</label><input class="validate[max[4.25],min[0]] marginSpinner" type="text" id="dset_leftMarginSize"/>
								</div>
								<div class="options_row">
									<label for="dset_rightMarginSize">Right</label><input class="validate[max[4.25],min[0]] marginSpinner" type="text" id="dset_rightMarginSize"/>
								</div>
							</div>
						</div>
					</div>
				
					<div class="labeled-input">
						<input type="hidden" id="dset_logo_hash" />
						<input type="hidden" id="dset_logo_height" />
						<input type="checkbox" class="normal_chkbx" id="dset_show_logo" /><label for="dset_show_logo" id="dset_show_logo_label" style="text-align:left; width:228px;">Show logo on assessment</label>
					</div>
					<div class="labeled-input" >
						<label style="text-align:left; width:95px;">Numbering: </label><select class="defaultSelectBox" style="width:147px;" id="dset_numbering_type">
							<option value="Numbers">Number sequentially</option>
							<option value="Type">Number by type</option>
							<option value="Alpha">Label parts a, b, c, &hellip;</option>
						</select>
					</div>
					<div class="labeled-input" >
						<label style="text-align:left; width:95px;">MC format: </label><select class="defaultSelectBox" style="width:147px;" id="dset_mc_format">
							<option value="One">One column</option>
							<option value="Auto">Automatic columns</option>
							<option value="Pyramid">Pyramid shuffled</option>
						</select>
					</div>
					<div class="labeled-input wr_section_options">
						<label style="text-align:left; width:95px;">WR responses:</label><select class="defaultSelectBox" style="width:147px;" id="dset_wr_response_location">
							<option value="form">On response form</option>
							<option value="question">On question sheet</option>
							<option value="neither">Elsewhere</option>
						</select>
					</div>
					<div class="labeled-input">
						<label style="text-align:left; width:95px;">Language:</label><select class="defaultSelectBox" style="width:147px;" id="dset_language">
							<option value="English">English</option>
							<option value="French">French</option>
						</select>
					</div>
				</div>

				<div id="scoring_settings_wrapper" class="settings_wrapper" style="display:none;">
					<div class="labeled-input" >
						<label style="width:auto;">Score type: </label><select class="defaultSelectBox" style="width:185px;" id="dset_scoring_type">
							<option value="Total">Out of total</option>
							<option value="Sections">Weighted sections</option>
							<option value="Outcomes">Weighted outcomes</option>
						</select>
					</div>
					<div id="section_weights" style="width:100%; height:275px; overflow-y:auto; overflow-x:hidden;">
					</div>
				</div>
			</div>
		</form>		
	</div>

<!-- Edit note dialog -->

    <div id="editNote_dialog" class="dialogbox" title="Edit note">
		<div class="dialog_wrapper" style="margin-bottom:0px; height:100%;">
			<form class="formular" style="margin-bottom:0px; height:100%;" accept-charset="utf-8">
    			<input type="hidden" id="note_item_id" />
				<div style="display:flex; flex-direction:column; height:100%;">
					<div class="labeled-input" style="width:100%; flex:1 1 auto; display:flex; flex-direction:column;">
						<div style="flex: 1 1 0px;" id="note_edit_view" class="template_view"></div>
					</div>
				</div>
			</div>
		</form>	     
	</div>

<!-- Edit preamble dialog -->

	<div id="editPreamble_form" class="dialogbox" title="Edit preamble">
		<div class="dialog_wrapper" style="margin-bottom:0px; height:100%;">
			<form class="formular" style="margin-bottom:0px; height:100%;" accept-charset="utf-8">
				<div style="display:flex; flex-direction:column; height:100%;">
					<div class="labeled-input" style="width:100%; flex:1 1 auto; display:flex; flex-direction:column;">
						<div style="flex: 1 1 0px;" id="dpre_preamble_edit" class="template_view"></div>
					</div>
				</div>
			</div>
		</form>	     
	</div>

<!-- Delete preamble dialog -->

	<div id="deletePreamble_dialog" class="dialogbox" title="Delete Preamble">
		<div class="dialog_wrapper">
			<p style="min-height:50px;">Delete preamble for this assessment?</p>
		</div>
	</div>

<!-- Add section dialog -->

	<div id="editSection_dialog" class="dialogbox" title="Section Details">
		<form class="formular" style="margin-bottom:0px;" accept-charset="utf-8">
			<input type="hidden" id="dsec_default_title" />
			<input type="hidden" id="dsec_id" />

			<div class="dialog_wrapper" style="padding-bottom:0px; position:relative;">
				<div class="labeled-input">
					<label for="dsec_header" style="text-align:left; width:95px;">Section Title:</label><input class="validate[required] defaultTextBox2" style="width:285px;" type="text" id="dsec_header"/>
				</div>
				<div class="labeled-input" >
						<label style="text-align:left; width:95px;">Question types: </label><select class="defaultSelectBox" style="width:200px;" id="dsec_type">
							<option value="mixed">All types</option>
							<option value="mc">Multiple choice only</option>
							<option value="nr">Numeric response only</option>
							<option value="wr">Written response only</option>
						</select>
					</div>
				<div class="labeled-input" id="dsec_weight_wrapper">
					<label style="text-align:left; width:80px;">Weight:</label><div style="font-family:Arial, Helvetica, sans-serif; font-size:13px;"><span id="dsec_weight"></span>% (see assessment settings to edit weights)</div>
				</div>
				<div style="width:200px; display:inline-block; vertical-align:top;">
					<div class="labeled-input" style="margin-bottom:0px;">
						<input type="checkbox" class="normal_chkbx" id="dsec_show_header" checked="checked" />
						<label for="dsec_show_header">Show title as header</label>
					</div>
					<div class="labeled-input">
						<input type="checkbox" class="normal_chkbx" id="dsec_shuffle_questions" checked="checked" />
						<label for="dsec_shuffle_questions">Shuffle questions</label>
					</div>
				</div>
			</div>
		</form>	     
	</div>

<!-- Delete section dialog -->

	<div id="deleteSection_dialog" class="dialogbox" title="Delete Section">
		<div class="dialog_wrapper">
			<p id="deleteSection_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Add item dialog -->

	<div id="addItem_dialog" class="dialogbox" title="Add Questions" data-selected="[]">
		<div class="dialog_wrapper" style="height:100%; display:flex; flex-direction:column; gap:10px;">
            <div style="flex:0 0 220px; display:flex; flex-direction:row; align-items:flex-start; gap:10px; width:100%;">
				<div style="flex:none; width:250px; height:100%; display:flex; flex-direction:column;">
					<div class="sub_panel_title" style="flex:none; height:auto; width:100%; display:flex; justify-content:space-between;">
						<div style="display:inline-block;">Folders</div>
					</div>
                    <div class="sub_panel_content folderTree" data-prefix="tree_add_" id="folderPanel" style="flex:1 1 0px; width:100%; box-sizing:border-box; background:white; padding:10px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px"></div>
				</div>
                <div style="flex:1 1 100%; height:100%; display:flex; flex-direction:column;">
					<div class="sub_panel_title" style="flex:none; height:auto; display:flex; justify-content:space-between;">
						<div>Questions</div>
						<div>
							<div id="sort_default" style="display:inline-block;">
								<div style="vertical-align:middle; display:inline-block;">
									<div class='item_sort' data-direction="asc">▲</div>
									<div class='item_sort' data-direction="desc">▼</div>
								</div>
								<div style="vertical-align:middle; display:inline-block;">Sort by</div>
								<select id="sort_type" style="height:23px; line-height:23px; margin:0px; padding:2px 3px; width:100px; font-size:13px;" data-direction="asc">
									<option value="last_used" data-default-direction="asc">Last used</option>
									<option value="name" data-default-direction="asc">Name</option>
								</select>	
							</div>
							<div id="sort_search" style="display:inline-block;">
								<div style="vertical-align:middle; display:inline-block;">Sort by</div>
								<select style="height:22px; margin:0px; width:100px; font-size:13px;" data-direction="asc" disabled>
									<option value="relevance" data-default-direction="desc">Relevance</option>
								</select>	
							</div>
							<div style="display:inline-block; margin-left:10px;">
								<div class="btn-group">
									<button class="btn btn-mini search_dropdown dropdown-toggle" style="margin:0px; font-size:13px;">
										Search
										<span class="caret"></span>
									</button>
									<div class="search_menu" id="item_search_menu">
										<div id="search_type_wrapper" style="margin-bottom:5px;">
											<div style="vertical-align:middle; display:inline-block;">Search:</div>
											<div style="vertical-align:middle; display:inline-block;">
												<select id="search_type" style="margin:0px; width:160px;">
													<option value="folder" selected>in this folder</option>
													<option value="subfolders">with sub-folders</option>
													<option value="all">in all folders</option>
												</select>
											</div>
										</div>
										<div style="position:relative; width:auto;">
											<input name="search" style="box-sizing:border-box; width:212px; height:30px; margin:0px;" type="text" id="search_input" />
											<div class='search_icon'></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="sub_panel_content" style="flex:1 1 0px; background:white; display:flex; flex-direction:column;">
						<div id="folder_questions_wrapper" style="flex:1 1 0px; overflow-y:scroll;">
							<table id="folder_questions" class="index_table selection_table">
								<tr class='disabled'><td style='border:none;'><i>Select a folder to begin</i></td></tr>
							</table>
						</div>
						<div style="flex:none; height:25px; display:flex; justify-content:space-between; border-top: 1px solid #ccc; padding:5px 7px 0px; font-family:Lucida Grande,Lucida Sans,Arial,sans-serif; font-size:13px;">
							<div id="type_checkbox_wrapper">
								<label style="display:inline-block; margin:0px;">Show question types:</label>
								<div style="display:inline-block; margin-left:10px;">
									<input type="checkbox" style="height:auto; margin:0px; vertical-align:middle; position:relative; top:-2px;" class="normal_chkbx type_checkbox" data-type="mc" id="show_mc" /><label for="show_mc" style="display:inline-block; margin:0px; margin-left:5px;">MC</label>
								</div>
								<div style="display:inline-block; margin-left:10px;">
									<input type="checkbox" style="height:auto; margin:0px; vertical-align:middle; position:relative; top:-2px;" class="normal_chkbx type_checkbox" data-type="nr" id="show_nr" /><label for="show_nr" style="display:inline-block; margin:0px; margin-left:5px;">NR</label>
								</div>
								<div style="display:inline-block; margin-left:10px;">
									<input type="checkbox" style="height:auto; margin:0px; vertical-align:middle; position:relative; top:-2px;" class="normal_chkbx type_checkbox" data-type="wr" id="show_wr" /><label for="show_wr" style="display:inline-block; margin:0px; margin-left:5px;">WR</label>
								</div>
							</div>
							<div id="hidden_questions"></div>
						</div>
					</div>
				</div>
			</div>
			<div style="flex:1 1 auto; display:flex; flex-direction:column;">
				<div class="sub_panel_title" style="flex:none;">Preview</div>
				<div class="sub_panel_content" style="flex:1 1 0px; background:white; padding:10px; overflow-y:scroll; font-size:14px;">
					<div id="preview_loading" style="display:none;"><div class="wait_icon"></div>Loading&hellip;</div>
					<div id="preview_stats_wrapper" style="margin-bottom:15px;"></div>
					<div id="preview_question_wrapper" class="assessment_wrapper template_view">
						<div class="question_wrapper"></div>
					</div>
				</div>
			</div>
			<div style="flex:none; height:auto; display:flex; justify-content:space-between; align-items:center;">
				<div id="addItem_count">No questions selected.</div>
				<div id="addItem_outcomes_wrapper">
					<input type="checkbox" style="height:auto; margin:0px; vertical-align:middle; position:relative; top:-2px;" class="normal_chkbx" id="addItem_outcomes" /><label for="addItem_outcomes" style="display:inline-block; margin:0px; margin-left:5px; font-size:14px;">Keep outcomes if possible</label>
				</div>
				<div class='custom-buttonpane' style="margin:6px 0px;">
					<button type="button" class="btn btn-cancel btn_font">Cancel</button>
					<button id="addItem_save" type="button" class="btn btn-save btn_font">Add</button>
				</div>
			</div>
		</div>
	</div>

<!-- Delete item dialog -->

	<div id="deleteItem_dialog" class="dialogbox" title="Remove Question">
		<div class="dialog_wrapper">
			<p id="deleteItem_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Build error dialog -->

	<div id="buildError_dialog" class="dialogbox" title="Build error">
		<div class="dialog_wrapper" style="min-height:70px; display:flex; flex-flow:space-between; align-items:flex-start; gap:10px;"">
			<div style="flex:1 1 auto;">
				<img style="height:50px;" src="/img/error.svg" />
			</div>
			<div style="flex:1 1 100%; font-size: 14px; font-family: Arial, Helvetica, sans-serif;">
				<div style="margin-bottom:10px;">Errors were found in this assessment. Click 'Close' to return to the assessment builder to make changes.</div>
				<div id="build_error_text"></div>
			</div>
		</div>
	</div>

<!-- Build version dialog -->

	<div id="buildVersion_dialog" class="dialogbox" title="Build version">
		<form class="formular" style="margin-bottom:0px;" accept-charset="utf-8">
			<input type="hidden" id="version_build_form" />
			<div class="dialog_wrapper">
				<div style="display:inline-block; width:auto; vertical-align:top;">
					<div class="labeled-input" style="width:auto;">
						<label for="version_title_text" style="width:auto; display:block; text-align:left; margin-bottom:5px !important;">Assessment title:</label>
						<div>
							<textarea class="validate[required] docTitleTextField defaultTextBox2" style="width:310px;" id="version_title_text"></textarea>
						</div>
					</div>
					<div class="build_note" style="margin-bottom:15px;">
						<div style="display:inline-block; width:30px; padding:3px; vertical-align:top;"><img src="/img/info.svg" /></div>
					<div style="display:inline-block; width:275px; padding-left:5px; vertical-align:top; line-height:normal; font-size:13px; font-family:Arial, Helvetica, sans-serif;">
							<div class="build_note_text"></div>
						</div>
					</div>
					<div class="build_warning">
						<div style="display:inline-block; width:30px; padding:3px; vertical-align:top;"><img src="/img/warning.svg" /></div>
						<div style="display:inline-block; width:275px; padding-left:5px; vertical-align:top; line-height:normal; font-size:13px; font-family:Arial, Helvetica, sans-serif;">
							<div>The following warnings were found for this assessment:</div>
							<div class="build_warning_text"></div>
						</div>
					</div>
				</div><div style="display:inline-block; width:275px; vertical-align:top; margin-left:20px;">
					<div class="labeled-input" style="width:auto;">
						<label style="width:auto; display:block; text-align:left; margin-bottom:5px !important;">Save to Forms and Versions folder:</label>
						<div style="width:100%; background:white; border:solid 1px #abcfda;">
							<div class="folderTree formFolders" data-prefix="tree_versionFolder_" id="versionFolderParent" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

<!-- Build sitting dialog -->

    <div id="buildSitting_dialog" class="dialogbox" title="Build online sitting">
		<form class="formular" style="margin-bottom:0px;" accept-charset="utf-8">
			<div class="dialog_wrapper">
				<div style="display:inline-block; width:auto; vertical-align:top;">
					<div class="labeled-input" style="width:auto;">
						<label for="sitting_title_text" style="width:auto; display:block; text-align:left; margin-bottom:5px !important;">Sitting title:</label>
						<div>
							<textarea class="validate[required] docTitleTextField defaultTextBox2" style="width:310px;" id="sitting_title_text"></textarea>
						</div>
					</div>
					<div class="build_note" style="margin-bottom:15px;">
						<div style="display:inline-block; width:30px; padding:3px; vertical-align:top;"><img src="/img/info.svg" /></div>
					<div style="display:inline-block; width:275px; padding-left:5px; vertical-align:top; line-height:normal; font-size:13px; font-family:Arial, Helvetica, sans-serif;">
							<div class="build_note_text"></div>
						</div>
					</div>
					<div class="build_warning">
						<div style="display:inline-block; width:30px; padding:3px; vertical-align:top;"><img src="/img/warning.svg" /></div>
						<div style="display:inline-block; width:275px; padding-left:5px; vertical-align:top; line-height:normal; font-size:13px; font-family:Arial, Helvetica, sans-serif;">
							<div>The following warnings were found for this assessment:</div>
							<div class="build_warning_text"></div>
						</div>
					</div>
				</div><div style="display:inline-block; width:275px; vertical-align:top; margin-left:20px;">
					<div class="labeled-input" style="width:auto;">
						<label style="width:auto; display:block; text-align:left; margin-bottom:5px !important;">Save to Online Sittings folder:</label>
						<div style="width:100%; background:white; border:solid 1px #abcfda;">
							<div class="folderTree sittingFolders" data-prefix="tree_sittingFolder_" id="sittingFolderParent" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

<!-- First assessment build dialog -->

	<div id="intro_note" class="dialogbox" title="Building assessments">
		<div class="dialog_wrapper">
			<div>
				<p>Welcome to the assessment builder!</p>
				<p>If you haven't already built questions for this assessment, we recommend that you prepare those before building an assessment. To begin building questions, click on the "Question Bank" button in the top bar, then on the "New Question" button at the top right.</p>
				<p>For more information, check out our short <a href="https://www.youtube.com/watch?v=K0xy5gn5toY" target="_blank">tutorial video</a>.</p>
			</div>
		</div>
	</div>
	
<!-- Target folder dialog -->

	<div id="targetFolder_dialog" class="dialogbox" title="Save question">
		<div class="dialog_wrapper">
			<input type="hidden" id="parent_content_id" />
			<input type="hidden" id="parent_question_handle_id" />
			<input type="hidden" id="target_section_id" />

			<div id="target_name_wrapper">
				<div class="labeled-input">
					<label style="text-align:left; display:inline-block; width:40px;">Name:</label>
					<input class="validate[required] defaultTextBox2" style="width:293px;" type="text" id="target_name" />
				</div>
			</div>
			<div class="labeled-input">
				<label style="text-align:left;">Save to folder:</label><div style="clear:both;"></div>
			</div>
			<div style="width:100%; background:white; border:solid 1px #abcfda;">
				<div class="folderTree" data-prefix="tree_targetFolder_" id="targetFolderParent" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px">
				</div>
			</div>
		</div>
	</div>

	<!-- {* version conflict dialog *} -->

	<div id="version_conflict_dialog" class="dialogbox" title="Version conflict">
		<div class="dialog_wrapper" style="display:flex; flex-flow:space-between; align-items:flex-start; gap:10px;">
			<div style="flex:1 1 auto; margin-top:5px;">
				<img style="height:50px;" src="/img/error.svg" />
			</div>
			<div style="flex:1 1 100%; font-size: 13px; font-family: Arial, Helvetica, sans-serif;">
				<p>It looks like this assessment is being edited from another window, or by another teacher.</p>
				<p>This page will need to be reloaded, clearing your changes, before you can continue. Click "Go back" 
				to close this dialog, or "Reload" to reload the page.</p>
			</div>
		</div>
	</div>

	<!-- Edit question set dialog -->

	<div id="editSet_dialog" class="dialogbox" title="Edit question set">
		<form class="formular" style="margin-bottom:0px;" accept-charset="utf-8">
			<input type="hidden" id="set_content_id" />
			<input type="hidden" id="set_scoring_hash" />

			<div class="dialog_wrapper">
				<div class="labeled-input" style="width:auto; margin-bottom:20px;">
					<label for="set_name" style="text-align:left; width:50px;">Name:</label><input class="validate[required] defaultTextBox2" style="width:410px;" type="text" id="set_name"/>
				</div>
				<div class="sub_panel_title" style="display:flex; justify-content:space-between;">
					<div>Questions</div>
				</div>
				<div class="sub_panel_content" style="height:150px; background:white;">
					<div style="height:100%; overflow-y:scroll;">
						<table id="set_questions_table"></table>
					</div>
				</div>

				<div style="margin-top:10px; flex:none; display:flex; justify-content:space-between; align-items:center;">
					<button id="set_add_questions" type="button" class="btn btn_font">Add questions</button>
					<div class='custom-buttonpane' style="margin:6px 0px;">
						<button type="button" class="btn btn-cancel btn_font">Cancel</button>
						<button type="button" class="btn btn-save btn_font">Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>

    <!-- Smart fill dialog -->

	<div id="smartFill_dialog" class="dialogbox" title="Smart fill">
        <input type="hidden" id="fill_section_id" />

        <div class="dialog_wrapper" style="display:flex; width:100%; height:100%; flex-direction:row; align-items:flex-start; gap:10px;">
            <div style="flex:none; width:250px; height:100%; display:flex; flex-direction:column;">
                <div class="sub_panel_title" style="flex:none; height:auto; width:100%; display:flex; justify-content:space-between;">
                    <div style="display:inline-block;">Folders</div>
                </div>
                <div class="sub_panel_content folderTree" data-prefix="tree_fill_" id="fill_folder" style="flex:1 1 0px; width:100%; box-sizing:border-box; background:white; padding:10px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px"></div>
            </div>
            <div style="flex:1 1 0px; min-width:0px; height:100%; display:flex; flex-direction:column;">
                <div class="sub_panel_title" style="flex:none; height:auto; width:100%; display:flex; justify-content:space-between;">
                    <div style="display:inline-block;">Outcomes</div>
                </div>
                <div class="sub_panel_content" style="flex:1 1 0px; width:100%; box-sizing:border-box; background:white; display:flex; flex-direction:column;">
                    <div id="fill_outcomes_wrapper" style="flex:1 1 0px; width:100%;"></div>
                    <div id="fill_include_wrapper" style="flex:none; height:30px; width:100%; box-sizing:border-box; border-top: 1px solid #ccc; padding:5px 7px 0px; font-family:Lucida Grande,Lucida Sans,Arial,sans-serif; font-size:13px;">
                        <label style="display:inline-block; margin:0px;">Include question types:</label>
                        <div style="display:inline-block; margin-left:10px;">
                            <input type="checkbox" style="height:auto; margin:0px; vertical-align:middle; position:relative; top:-2px;" class="normal_chkbx type_checkbox" data-type="mc" id="include_mc" /><label for="include_mc" style="display:inline-block; margin:0px; margin-left:5px;">MC</label>
                        </div>
                        <div style="display:inline-block; margin-left:10px;">
                            <input type="checkbox" style="height:auto; margin:0px; vertical-align:middle; position:relative; top:-2px;" class="normal_chkbx type_checkbox" data-type="nr" id="include_nr" /><label for="include_nr" style="display:inline-block; margin:0px; margin-left:5px;">NR</label>
                        </div>
                        <div style="display:inline-block; margin-left:10px;">
                            <input type="checkbox" style="height:auto; margin:0px; vertical-align:middle; position:relative; top:-2px;" class="normal_chkbx type_checkbox" data-type="wr" id="include_wr" /><label for="include_wr" style="display:inline-block; margin:0px; margin-left:5px;">WR</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

<!-- Build from class list dialog -->

    <div id="buildSet_dialog" class="dialogbox" title="Build set from class list">
        <div class="dialog_wrapper" style="padding:0px; height:400px; display:flex; flex-direction:column; gap:10px;">    
            <div style="flex:1 1 100%; width:100%; white-space:nowrap; overflow:hidden;">
                <div class="step_wrapper" style="display:inline-flex; flex-direction:row; align-items:flex-start; gap:20px; vertical-align:top;">
                    <div style="flex:1 1 auto; height:100%; display:flex; flex-direction:column; gap:15px;">
                        <div class="labeled-input" style="flex:none; height:auto; width:100%;">
                            <label for="set_title_text" style="width:auto; display:block; text-align:left; margin-bottom:5px !important;">Assessment title:</label>
                            <textarea class="validate[required] docTitleTextField defaultTextBox2" style="box-sizing:border-box; width:100%; height:120px;" id="set_title_text"></textarea>
                        </div>
                        <div class="build_note" style="flex:none; height:auto; width:100%; display:flex; flex-direction:row; align-items:flex-start; gap:5px;">
                            <div style="flex:0 0 30px; padding:3px;"><img src="/img/info.svg" /></div>
                            <div style="flex:1 1 100%; padding-left:5px; line-height:normal; font-size:13px; font-family:Arial, Helvetica, sans-serif;">
                                <div class="build_note_text"></div>
                            </div>
                        </div>
                        <div class="build_warning" style="flex:none; height:auto; width:100%; display:flex; flex-direction:row; align-items:flex-start; gap:5px;">
                            <div style="flex:0 0 30px; padding:3px;"><img src="/img/warning.svg" /></div>
                            <div style="flex:1 1 100%; padding-left:5px; line-height:normal; font-size:13px; font-family:Arial, Helvetica, sans-serif;">
                                <div class="build_warning_text"></div>
                            </div>
                        </div>
                    </div><div class="labeled-input" style="flex:none; width:275px; height:100%;">
                        <label style="width:auto; display:block; text-align:left; margin-bottom:5px !important;">Save to Forms and Versions folder:</label>
                        <div style="width:100%; height:100%; background:white; border:solid 1px #abcfda;">
                            <div class="folderTree formFolders" data-prefix="tree_setFolder_" id="setFolderParent" style="padding:10px; height:100%; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px">
                            </div>
                        </div>
                    </div>
                </div><div class="step_wrapper" style="display:inline-block; vertical-align:top;">
                    <p>Choose a class list below, then select or unselect students as needed. Click "Build" to build versions for the 
                    selected students.</p>

                    <?php echo $this->element("class_list_common"); ?>
                </div>
            </div>
            
            <div class="ui-dialog-buttonset custom-buttonpane" style="flex:none; height:auto; align-self:flex-end; margin:5px 10px 10px;">
                <button id="set_cancel" type="button" class="btn btn-cancel btn_font">Cancel</button>
                <button type="button" id="class_list_continue" class="btn btn_font" style="width:55px;">Next</button>
            </div>

        </div>
	</div>

</div>

<?php echo $this->element('html_edit'); ?>

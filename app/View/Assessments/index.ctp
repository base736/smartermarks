<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	<?php if (!empty($jsonQuestionTree)) { ?> var questionTreeData = <?php echo $jsonQuestionTree; } ?>;
</script>

<script type="module" src="/js/assessments/index.js?7.0"></script>

<div style="grid-column:right; grid-row:actions" class="action_bar<?php if (!$adminView) echo ' right_half' ?>">
	<div class="action_left">
		<div class="checkbox_arrow"></div>
		<div class="index_actions trash_actions">
			<div class="btn-group">							
				<button type="button" class="btn btn-dark item_action_many moveItem_button"><span>Move</span></button>
			</div>
		</div>
		<div class="index_actions default_actions">
			<div class="btn-group">
				<div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Build 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="editDocument_button" class="item_action_one">Edit assessment</a></li>
						<li><a href="#" id="duplicateItem_button" class="item_action_many <?php echo (!isset($user['role']) || $user['role'] != 'admin') ? 'needs_copy' : 'always-disabled-link'; ?>">Duplicate</a></li>
					</ul>
				</div>
				
			</div>

			<div class="btn-group">
				<div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Manage 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" class="<?php echo (!isset($user['role']) || $user['role'] != 'admin') ? 'item_action_many needs_delete moveItem_button' : 'always-disabled-link'; ?>">Move to folder</a></li>
						<li><a href="#" class="<?php echo (!isset($user['role']) || $user['role'] != 'admin') ? 'item_action_many copyItem_button' : 'always-disabled-link'; ?>">Copy to folder</a></li>
						<li><a href="#" id="deleteItem_button" class="item_action_many needs_delete">Move to trash</a></li>
					</ul>
				</div>
				<div class="btn-group">
					<button id="share_dropdown" class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Share 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="sendCopy_button" class="item_action_many needs_copy">Send a copy</a></li>
					</ul>
				</div>
			</div>

			<div class="btn-group">
				<div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Tools 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="export_button" class="item_action_many">Save questions</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="action_right">
		<div class="btn-group">
			<button class="btn btn-dark search_dropdown dropdown-toggle" href="#">
				Search 
				<span class="caret"></span>
			</button>
			<div class="pull-right search_menu">
				<?php
				if (!$adminView) {
				?>	

				<div id="search_options" style="margin-bottom:5px;">
					<div style="vertical-align:middle; display:inline-block;">Search:</div>
					<div style="vertical-align:middle; display:inline-block;">
						<select id="search_type" style="margin:0px; width:160px;">
							<option value="folder">in this folder</option>
							<option value="subfolders">with sub-folders</option>
							<option value="all">in all folders</option>
						</select>
					</div>
				</div>

				<?php
				}
				?>
				
				<div id="search_wrapper" style="position:relative; width:auto;">
					<input class="search_text" type="text" />
					<div class="search_icon"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="grid-column:right; grid-row:main; display:flex; flex-direction:column;">
	<div class="window_bar<?php if (!$adminView) echo ' right_half' ?>" style="flex:none; margin-top:1px; padding-left:10px;">
        <div style="width:5px">&nbsp;</div>
		<?php
		if ($adminView) {
		?>	
		<div style="width:250px;">Owner</div>
		<?php
		}
		?>
		<div style="width:120px;">Created</div>
		<div style="width:auto;">Name</div>

		<div class="sort_wrapper">
			<div style="vertical-align:middle; display:inline-block; line-height:normal;">
				<div class="header_sort" data-direction="asc" style="display:block;">▲</div>
				<div class="header_sort" data-direction="desc" style="display:block;">▼</div>
			</div>
			<span style="white-space:nowrap;">Sort by</span>
			<select id="sort_type">
				<option value="<?php echo $colName; ?>" data-default-direction="asc">Name</option>
				<option value="<?php echo $colCreated; ?>" data-default-direction="desc" selected="selected">Created</option>
			</select>
            <?php
            if (!$adminView) {
            ?>
			<span style="margin-left:10px;">Show</span>
			<select id="show_limit">
                <option value="10">10</option>
                <option value="all">all</option>
			</select>
            <?php
            }
            ?>
		</div>
	</div>
	<div id="index_right" class="index_body<?php if (!$adminView) echo ' right_half' ?>" style="flex:1 1 100%; overflow:hidden; min-height:300px; width:100%;">
		<table class="index_table selection_table">
		</table>
		<div id="footer_wrapper" style="display:flex; justify-content:space-between; height:auto; margin:20px 10px;">
			<div id="count_wrapper" style="color:#75797c;"></div>
			<div id="pagination_wrapper" class="pagination_wrapper"></div>
		</div>
	</div>
</div>

<div class="hidden">
	
<!-- Save questions dialog -->

	<div id="export_dialog" class="dialogbox" title="Save questions to bank">
		<div class="dialog_wrapper">
			<div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>
			<input type="hidden" name="data[Folder][id]" id="exportFolder_id" />
			<div style="display:inline-block; vertical-align:top; width:300px;">
				<div style="display:block; margin-bottom:5px;">
					<div class="labeled-input" style="float:left; width:auto; margin-bottom:0px; margin-top:2px;">
						<label style="text-align:left;">Questions:<span id="export_omitted"></span></label><div style="clear:both;"></div>
					</div>
					<div style="float:right; margin-right:3px;"><button id="export_select_all" class="btn-tiny" style="font-size:13px;">Select all</button></div>
					<div style="clear:both;"></div>
				</div>
				<div id="export_list" style="width:275px; height:200px; padding:10px; background:white; border:solid 1px #abcfda; overflow-y:scroll;">
				</div>
			</div><div style="display:inline-block; vertical-align:top; margin-left:10px; width:270px;">
				<div style="height:25px; margin-bottom:5px;">
					<div class="labeled-input" style="float:left; width:auto; margin-bottom:0px; margin-top:2px;">
						<label style="text-align:left;">Save to folder:</label><div style="clear:both;"></div>
					</div>
					<div style="clear:both;"></div>
				</div>
				<div style="width:100%; background:white; border:solid 1px #abcfda;">
					<div class="folderTree2" data-prefix="tree_exportFolder_" id="exportFolderParent" style="padding:10px; height:200px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px">
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

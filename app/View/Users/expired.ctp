<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width:600px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner">

		<p style="margin-top:10px;"><b>Your account expired on <?php 
			$expiry = new DateTime($user['expires']);
			echo $expiry->format('F j, Y');
		?></b></p>
		
		<p>We're glad you'd like to continue using SmarterMarks!  We're proud to offer flexible 
		payment options, all at a cost of about 5 cents per student assessment, or $50 per year for
		a single user.</p>
		
		<p>Until your account has been renewed, you will not be able to build or modify forms, versions, 
		assessment templates, and questions, or score new student responses. Click the button below to 
		<?php if (empty($user['locked'])) { ?> move your SmarterMarks renewal date to one week from today
		and <?php } ?> learn more about renewal options, or drop us an email at 
		<a href="mailto:support@smartermarks.com">support@smartermarks.com</a> and we'd be happy to offer 
		more information!</p> 		

		<div style="height:50px">
			<a href="/Users/renew_main"><button style="position:relative; top:10px;" class="btn btn-primary pull-right">Renewal options</button></a>
		</div>
	</div>
</div>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

$(document).ready(function() {
	
	$('#agree').on('change', function(e) {
		if ($(this).prop('checked')) $('#continue_button').removeAttr('disabled');
		else $('#continue_button').attr('disabled', 'disabled');
	});
	
	$('#continue_button').on('click', function(e) {
		window.location.href = '/Users/read_agreement/yes';
	});
});

</script>

<div class="rounded_white_wrapper" style="width:800px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner">

		<h3>User Agreement</h3>

		<p>Our user agreement protects your rights and the rights of other users when you use SmarterMarks. It's short &mdash; please take a moment to read it through before continuing. If you have concerns about the user agreement, please contact us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>
		
		<div class="policyText" style="height:auto; background:white; padding:10px; margin:20px 0px;">
			<?php echo $this->element('agreement_text'); ?>
		</div>
		
		<div style="padding-top:10px;">
			<div style="float:left;">
				<input type="checkbox" id="agree" style="margin:0px;" />
				<label for="agree" style="display:inline-block; margin:0px;">I have read and agree to the above.</label>
			</div>
			<div style="float:right;">
				<button id="continue_button" class="btn btn-primary" disabled>Continue</button>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
</div>

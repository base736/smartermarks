<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<?php
if (!isset($details) || ($details == false)) {
?>

<script type='text/javascript'>	

	$(document).ready(function() {
		Cookies.remove('security_email');

		var action = Cookies.get('auth_redirect_action');			
		if (typeof(action) != 'undefined') {
			Cookies.remove('auth_redirect_action');
			if (action == 'close_window') {
				window.close();
			} else if (action == 'redirect_home') {
				window.location.replace('/');			
			}
		} else {
			window.location.replace('/');
		}
	});

</script>

<?php
}
?>

<div class="rounded_white_wrapper" style="width: 500px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
	
		<?php
		if (!isset($details) || ($details == false)) {
		?>

		<h3>Login successful</h3>

		<p>You will be forwarded to SmarterMarks automatically.</p>

		<?php
		} else {
		?>

		<h3>Error in login</h3>
		<pre><?php print_r($details); ?></pre>

		<?php
		}
		?>

	</div>
</div>

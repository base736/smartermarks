<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

var jsRegions = <?php echo json_encode($tzRegions); ?>;
var jsCities = <?php echo json_encode($tzCities); ?>;
var authRole = '<?php echo $user['role']; ?>';
var userData = <?php echo json_encode($user_data); ?>;

var userdataPrefix = "<?php
	$secrets = Configure::read('secrets'); 
	echo $secrets['s3']['userdata_prefix'];
?>";

</script>

<script type="module" src="/js/users/edit.js?7.0"></script>

<div style="width:840px; margin:auto;">
	<form class="formular" id="userEdit_form" accept-charset="utf-8">

		<?php 
			if ($user['role'] == 'admin') {
		?>
	
		<div class="window_bar">Admin settings</div>
		<div class="window_content" style="margin-bottom:20px; padding:20px;">
			<div style="display:flex; width:100%;">
				<div style="flex:1 1 auto;">
					<div class="labeled-input">
						<label style="vertical-align:middle;">Account created:</label>
						<div style="vertical-align:middle; height:20px; padding:0px; line-height:20px; font-family:'Arial'; font-size:12px;" type="text" id="user_created"></div>
					</div>
					<div class="labeled-input">
						<label>User's role:</label><select id="user_role" class="userEditSelectBox">
							<option value="student">Student</option>
							<option value="teacher">Teacher</option>
							<option value="school">School Admin</option>
						</select>
					</div>
					<div class="labeled-input">
						<label style="vertical-align:top; position:relative; top:5px;">Account expiry:</label>
						<div>
							<div class="options_row">
								<input type="checkbox" class="normal_chkbx" id="user_has_expiry"/><label>Account expires</label>
							</div>
							<div class="options_row">
								<input class="datepicker defaultTextBox2" type="text" id="user_expires"/>
							</div>
							<div class="options_row">
								<input type="checkbox" class="normal_chkbx" id="user_is_locked"/><label>Account locked</label>
							</div>
							<div class="options_row">
								<input class="datepicker defaultTextBox2" type="text" id="user_locked"/>
							</div>
						</div>
					</div>
				</div>
				<div style="flex:none;">
					<div class="options_row">
						<label style="width:70px; vertical-align:top; position:relative; top:5px;">Message:</label>
						<textarea id="user_message" class="defaultTextBox2" style="width:260px; height:200px; resize:none;"></textarea>
					</div>					
				</div>
			</div>
			<div style="width:100%;">
				<div class="options_row">
					<label style="margin-bottom:5px !important;">Notes:</label>
					<textarea id="user_notes" class="defaultTextBox2" style="width:100%; height:170px; box-sizing:border-box; resize:none" ></textarea>
				</div>
			</div>

		</div>
		
		<?php
			} else {
		?>

		<input type="hidden" id="user_role" />

		<?php
			}
		?>

		<div class="window_bar">Account settings</div>
		<div class="window_content" style="margin-bottom:20px; padding:20px;">
			<div style="margin-bottom:20px;">
				<label id="expires_label"></label>
			</div>
			<div style="display:flex; width:100%;">
				<div style="flex:1 1 auto;">
					<div class="labeled-input">
						<label>User's Name:</label><input id="user_name" maxlength="50" type="text" class="defaultTextBox2"/>
					</div>			
					<div class="labeled-input">
						<label>User's School Name:</label><input id="user_school" maxlength="255" type="text" class="defaultTextBox2"/>
					</div>			
					<div class="labeled-input">
						<label>User's Email Address:</label><input id="user_email" maxlength="100" type="email" class="validate[required,custom[email]] defaultTextBox2"/>
					</div>
				</div>
				
				<div style="flex:none; display:flex; flex-direction:column; align-items:flex-end;">
					<div style="flex:1 1 auto; width:auto;">
						<div class="labeled-input" style="width:auto;">
							<label style="display:inline-block;">User's Time Zone:</label>
							<div style="display:inline-block; vertical-align:top;">
								<select style="display:block;" class="userEditSelectBox" id="user_tz_region"></select>
								<select style="display:block;" class="userEditSelectBox" id="user_tz_city"></select>
							</div>
						</div>
					</div>
					
					<?php
					if ($can_change_password) {
					?>

					<div style="flex:none;">
						<a href="/Users/editPassword/<?php echo $user_data['id']; ?>">Change password</a>
					</div>

					<?php
					}
					?>
				</div>
			</div>			
		</div>

		<div class="edit_wrapper_teacher">
			<input type="hidden" class="loadedField loadedJSON" id="defaults-Common-Title-nameLines" />

			<div class="window_bar reveal_hide">
				<button type="button" class="reveal_button btn btn-small"></button>
				<span>Teacher and student reports</span>
			</div>
			<div class="window_content" style="margin-bottom:20px;">
				<div style="width:800px; padding:20px;">
					<div style="width:400px; display:inline-block; vertical-align:top;">
						<h3 style="margin-top:0px;">Teacher reports</h3>
						<div style="margin-left:20px;">
							<div style="padding-bottom: 10px">
								<label>Included elements:</label>
							</div>
							<div class="optionsContainer" style="width:300px; height:200px; margin-bottom:20px; padding:10px 20px;">
		
								<label>Assessment profile</label>
								<div style="margin-bottom:5px; margin-left:30px;">
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-graph"/><label for="defaults-Document-Reports-Teacher-graph">Score distribution graph</label>
									</div>
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-minorgraphs"/><label for="defaults-Document-Reports-Teacher-minorgraphs">Difficulty and discrimination graphs</label>
									</div>
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-meansd"/><label for="defaults-Document-Reports-Teacher-meansd">Mean and standard deviation</label>
									</div>
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-median"/><label for="defaults-Document-Reports-Teacher-median">Median score</label>
									</div>
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-cronbach"/><label for="defaults-Document-Reports-Teacher-cronbach">Cronbach's alpha</label>
									</div>
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-collusion"/><label for="defaults-Document-Reports-Teacher-collusion">Collusion analysis</label>
									</div>
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-categories"/><label for="defaults-Document-Reports-Teacher-categories">Learning outcomes summary</label>
									</div>
								</div>
				
								<label>Response profile</label>
								
								<div style="margin-bottom:5px; margin-left:30px;">
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-average"/><label for="defaults-Document-Reports-Teacher-average">Average score</label>
									</div>
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-pearson"/><label for="defaults-Document-Reports-Teacher-pearson">Discrimination</label>
									</div>
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-responses"/><label for="defaults-Document-Reports-Teacher-responses">Responses</label>
									</div>
								</div>
		
								<div style="margin-bottom:5px">
									<div class="labeled-input">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Teacher-experimental"/><label for="defaults-Document-Reports-Teacher-experimental">Experimental measures</label>
									</div>
								</div>
							</div>
							<div class="labeled-input">
								<label>Sort item analysis by:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Reports-Teacher-itemsort" style="width:163px;">
									<option value="question">Question number</option>
									<option value="difficulty">Average score</option>
									<option value="discrimination">Discrimination</option>
								</select>
							</div>
						</div>
					</div><div style="width:400px; display:inline-block; vertical-align:top;">
						<h3 style="margin-top:0px;">Student reports</h3>
						<div style="margin-left:20px;">
							<div class="labeled-input">
								<label>Sort outcomes:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Reports-Teacher-outcomesort" style="width:203px;">
									<option value="alpha">Alphabetically</option>
									<option value="score">By score</option>
								</select>
							</div>
		
							<div class="labeled-input">
								<label>Sort student reports:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Reports-Teacher-reportorder" style="width:203px;">
									<option value="scan">As scanned</option>
									<option value="alpha">By name and ID</option>
								</select>
							</div>
		
							<div class="labeled-input">
								<label>Student report layout:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Reports-Teacher-reportlayout" style="width:203px;">
									<option value="default">Sequential</option>
									<option value="guillotine">Guillotine layout</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="window_bar reveal_hide">
				<button type="button" class="reveal_button btn btn-small"></button>
				<span>Other defaults</span>
			</div>
			<div class="window_content" style="margin-bottom:20px;">
				<div style="width:800px; padding:20px;">
					<div class="window_section">
						<h3 style="margin-top:0px;">Open question bank</h3>
						<div style="margin-left:20px; width:360px; display:inline-block; vertical-align:top;">
							<div class="labeled-input">
								<input type="checkbox" class="normal_chkbx" id="open_created"/><label for="open_created" style="width:300px;">Share questions I create by default.</label>
							</div>
						</div>
					</div>
					<div class="window_section">
						<h3>Browsing items</h3>
						<div style="margin-left:20px; width:360px; display:inline-block; vertical-align:top;">
							<div class="labeled-input">
								<label>Sort by:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Indices-sort" style="width:100px;">
									<option value="created">Created</option>
									<option value="name">Name</option>
								</select>
							</div>
						</div>
					</div>
                    <div class="window_section">
                        <h3>PDF generation</h3>
                        <div style="margin-left:20px; width:360px; display:inline-block; vertical-align:top;">
                            <div class="labeled-input">
                                <label>Page size:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Common-Settings-pageSize" style="width:203px;">
                                    <option value="letter">Letter</option>
                                    <option value="legal">Legal</option>
                                    <option value="a4">A4</option>
                                </select>
                            </div>
                            <div class="labeled-input">
                                <label style="vertical-align:top; position:relative; top:10px;">Margins (inches):</label><div>
                                    <div class="margin_column">
                                        <div class="options_row" style="margin-bottom:10px; margin-top:5px;">
                                            <label for="defaults-Common-Settings-marginSizes-top" style="width:45px;">Top</label><input class="validate[required,max[5.5],min[0]] marginSpinner loadedField loadedFloat" type="text" id="defaults-Common-Settings-marginSizes-top"/>
                                        </div>
                                        <div class="options_row">
                                            <label for="defaults-Common-Settings-marginSizes-bottom" style="width:45px;">Bottom</label><input class="validate[required,max[5.5],min[0]] marginSpinner loadedField loadedFloat" type="text" id="defaults-Common-Settings-marginSizes-bottom"/>
                                        </div>
                                    </div><div class="margin_column" style="padding-left:10px;">
                                        <div class="options_row" style="margin-bottom:10px; margin-top:5px;">
                                            <label for="defaults-Common-Settings-marginSizes-left" style="width:35px;">Left</label><input class="validate[required,max[4.25],min[0]] marginSpinner loadedField loadedFloat" type="text" id="defaults-Common-Settings-marginSizes-left"/>
                                        </div>
                                        <div class="options_row">
                                            <label for="defaults-Common-Settings-marginSizes-right" style="width:35px;">Right</label><input class="validate[required,max[4.25],min[0]] marginSpinner loadedField loadedFloat" type="text" id="defaults-Common-Settings-marginSizes-right"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="window_section">
						<h3>Forms and assessments</h3>
						<div style="margin-left:20px; width:360px; display:inline-block; vertical-align:top;">
                            <div class="labeled-input">
								<label>Form version:</label><select class="userEditSelectBox loadedField loadedInteger" id="defaults-Document-Settings-version" style="width:203px;">
                                    <option value="3">New style</option>
                                    <option value="2">Old style</option>
								</select>
							</div>
							<div class="labeled-input">
								<label>Question books:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Settings-formLocation" style="width:203px;">
									<option value="none">No response form included</option>
									<option value="front">Response form at the front</option>
									<option value="back">Response form at the back</option>
								</select>
							</div>
							<div class="labeled-input" >
								<label>Numbering: </label><select class="userEditSelectBox loadedField loadedText" id="defaults-Assessment-Settings-numberingType" style="width:203px;">
									<option value="Numbers">Number sequentially</option>
									<option value="Type">Number by type</option>
									<option value="Alpha">Label parts a, b, c, &hellip;</option>
								</select>
							</div>
							<div class="labeled-input" >
								<label>MC format: </label><select class="userEditSelectBox loadedField loadedText" id="defaults-Assessment-Settings-mcFormat" style="width:203px;">
									<option value="One">One column</option>
									<option value="Auto">Automatic columns</option>
									<option value="Pyramid">Pyramid shuffled</option>
								</select>
							</div>
							<div class="labeled-input wr_section_options">
								<label>WR responses:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Assessment-Settings-wrResponseLocation" style="width:203px;">
									<option value="form">On response form</option>
									<option value="question">On question sheet</option>
									<option value="neither">Elsewhere</option>
								</select>
							</div>
							<div class="labeled-input">
								<label>Language:</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Common-Settings-language" style="width:203px;">
									<option value="English">English</option>
									<option value="French">French</option>
								</select>
							</div>
						</div><div style="width:400px; margin-left:20px; display:inline-block; vertical-align:top;">
							<div class="labeled-input">
								<label for="defaults-Document-Settings-copiesPerPage" style="width:120px;">Forms per page:</label><input class="validate[required,custom[isPosInteger]] intSpinner loadedField loadedInteger" type="text" id="defaults-Document-Settings-copiesPerPage"/>
							</div>

                            <div class="labeled-input">
                                <label for="logo_wrapper" style="vertical-align:top; position:relative; top:5px; width:120px;">Logo on forms:</label>
                                <div id="logo_wrapper" style="width:258px;">
                                    <div class="options_row">
                                        <input type="hidden" id="defaults-Common-Title-logo-imageHash" />
                                        <input type="checkbox" class="normal_chkbx" id="include_logo" /><label style="text-align:left; width:228px;">Include a logo in response forms</label>
                                    </div>
                                    <div id="logo_image_wrapper"></div>
                                    <div id="height_wrapper" style="margin-top:10px;">
                                        <label for="logo_height" style="display:inline;">Logo height (inches): </label> <input class="validate[min[0.25]] marginSpinner loadedField loadedFloat" type="text" id="defaults-Common-Title-logo-height" style="width:40px;"/>
                                    </div>
                                </div>
                            </div>

                            <div class="labeled-input">
								<label for="bubbles_wrapper" style="vertical-align:top; position:relative; top:5px; width:120px;">Student Information:</label>
								<div id="bubbles_wrapper" style="width:258px;">
									<div class="options_row">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Title-nameField-show" value="1" /><label for="defaults-Document-Title-nameField-show" style="text-align:left; width:228px;">Field for student name</label>
									</div>
									<div class="options_row">
										<label for="defaults-Document-title_nameLabel" style="display:inline-block;">Label</label><input id="defaults-Document-Title-nameField-label" class="validate[required] defaultTextBox2 loadedField loadedText" style="width:90px !important;" type="text" /><label for="defaults-Document-Title-nameField-bubbles" style="text-align:right; width:60px;">Letters</label><input class="validate[required,custom[isPosInteger]] intSpinner loadedField loadedInteger" type="text" id="defaults-Document-Title-nameField-bubbles" />
									</div>
			
									<div class="options_row">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Title-idField-show" value="1" /><label for="defaults-Document-Title-idField-show" style="text-align:left; width:228px;">Field for student ID</label>
									</div>
									<div class="options_row">
										<label for="defaults-Document-Title-idField-label" class="idDigits_label">Label</label><input id="defaults-Document-Title-idField-label" class="validate[required] defaultTextBox2 loadedField loadedText" style="width:90px !important;" type="text" /><label for="defaults-Document-Title-idField-bubbles" style="text-align:right; width:60px;" class="idDigits_label">Digits</label><input class="validate[required,custom[isPosInteger]] intSpinner loadedField loadedInteger" type="text" id="defaults-Document-Title-idField-bubbles" />
									</div>
								</div>
								<div class="labeled-input" style="padding-top:10px;">
									<label for="nameLines_wrapper" style="position:relative; top:5px; width:120px;">Name lines:</label>
									<div id="nameLines_wrapper" class="options_row" style="vertical-align:top;"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="window_section">
						<h3>Student reports</h3>
						<div style="margin-left:20px; width:360px; display:inline-block; vertical-align:top;">
							<div class="labeled-input" style="margin-top:5px;">
								<label>Layout:</label><select class="userEditSelectBox loadedField loadedText" style="width: 203px" id="defaults-Document-Reports-Student-reportsPerPage">
									<option value='default'>Automatic</option>
									<option value='singles'>One per page</option>
									<option value='2perpage'>2 per page</option>
									<option value='3perpage'>3 per page</option>
									<option value='4perpage'>4 per page</option>
									<option value='5perpage'>5 per page</option>
								</select>
							</div>
                            <div class="labeled-input">
                                <label>Weights</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Reports-Student-weights" style="width:203px;">
                                    <option value='none'>Hide weights</option>
                                    <option value='show'>Show weights</option>
                                </select>
                            </div>
                            <div class="labeled-input">
                                <label>Outcomes</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Reports-Student-outcomes" style="width:203px;">
                                    <option value='none'>Hide outcomes</option>
                                    <option value='names'>Show outcomes</option>
                                    <option value='numbers'>With question #'s</option>
                                </select>
                            </div>
                            <div class="labeled-input">
                                <label>Responses</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Reports-Student-responses" style="width:203px;">
                                    <option value='none'>Hide responses</option>
                                    <option value='responses'>Show responses</option>
                                    <option value='key'>Responses and key</option>
                                </select>
                            </div>
                            <div class="labeled-input">
                                <input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Reports-Student-incorrectOnly" style="margin-left:145px;" value="1" /><label for="defaults-Document-Reports-Student-incorrectOnly" style="width:auto;">Incorrect only</label>
                            </div>
                            <div class="labeled-input">
                                <label>Results</label><select class="userEditSelectBox loadedField loadedText" id="defaults-Document-Reports-Student-results" style="width:203px;">
                                    <option value='numerical'>Scores and percents</option>
                                    <option value='proficiency'>Proficiency levels</option>
                                    <option value='both'>Both styles</option>
                                </select>
                            </div>
                						
						</div><div style="width:400px; margin-left:20px; display:inline-block; vertical-align:top;">

							<input type="hidden" class="loadedField loadedJSON" id="defaults-Document-Reports-Student-outcomesLevels" />

							<div class="labeled-input" style="margin-bottom:0px;">
								<label style="vertical-align:top; width:120px;">Proficiency levels:</label><div>
									<div class="objectives_header" style="line-height:20px; margin-bottom:5px;">
										<div style="width:145px;">Level name</div>
										<div style="width:55px;">From %</div>
									</div>
									<div id="user_proficiencyLevels_wrapper"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="window_section">
						<h3>Multiple choice sections</h3>
						<div style="margin-left:20px; width:360px; display:inline-block; vertical-align:top;">
							<div class="labeled-input">
								<label>Number of columns:</label><select class="defaultSelectBox defaults-Document-Sections-columnCount" style="width: 60px">
									<option value='auto'>auto</option>
									<option value='1'>1</option>
									<option value='2'>2</option>
									<option value='3'>3</option>
									<option value='4'>4</option>
									<option value='5'>5</option>
									<option value='6'>6</option>
									<option value='7'>7</option>
									<option value='8'>8</option>
									<option value='9'>9</option>
									<option value='10'>10</option>
								</select>
							</div>
							<div class="labeled-input">
								<label for="defaults-Document-Sections-mc_choiceCount">Number of choices:</label><input class="validate[required,custom[isPosInteger]] intSpinner loadedField loadedInteger" type="text" id="defaults-Document-Sections-mc_choiceCount"/>
							</div>
						</div>
					</div>
					<div class="window_section">
						<h3>Numeric response sections</h3>
						<div style="margin-left:20px; width:360px; display:inline-block; vertical-align:top;">
							<div class="labeled-input">
								<label>Number of columns:</label><select class="defaultSelectBox defaults-Document-Sections-columnCount" style="width: 60px">
									<option value='auto'>auto</option>
									<option value='1'>1</option>
									<option value='2'>2</option>
									<option value='3'>3</option>
									<option value='4'>4</option>
									<option value='5'>5</option>
									<option value='6'>6</option>
									<option value='7'>7</option>
									<option value='8'>8</option>
									<option value='9'>9</option>
									<option value='10'>10</option>
								</select>
							</div>
							<div class="labeled-input">
								<label for="defaults-Question-TypeDetails-nr-nrDigits">Number of digits:</label><input class="validate[required,custom[isPosInteger]] intSpinner loadedField loadedInteger" type="text" id="defaults-Question-TypeDetails-nr-nrDigits"/>
							</div>
							<div class="labeled-input">
								<label for="defaults-Question-TypeDetails-nr-type">Type:</label><select class="userEditSelectBox loadedField loadedText" style="height: 28px; width: 100px" id="defaults-Question-TypeDetails-nr-type">
									<option value="nr_decimal" selected>Decimal</option>
									<option value="nr_scientific">Scientific</option>
									<option value="nr_selection">Selection</option>
									<option value="nr_fraction">Fraction</option>
								</select>
							</div>
							<div class="labeled-input">
								<label for="defaults-Question-TypeDetails-nr-promptType" style="position:relative; top:10px;">Response prompt (except selection):</label><select class="userEditSelectBox loadedField loadedText" style="height: 28px; width: 203px" id="defaults-Question-TypeDetails-nr-promptType">
									<option value="none" selected>No response prompt</option>
									<option value="simple">Show simple prompt</option>
									<option value="digits">Show with digits</option>
								</select>
							</div>
						</div><div style="width:400px; margin-left:20px; display:inline-block; vertical-align:top;">
							<div class="labeled-input" style="margin-bottom:0px;">
								<label style="vertical-align:top; width:120px;">Selection options:</label><div>
									<div class="options_row" style="margin-bottom:0px;">
										<input type="checkbox" value="1" class="normal_chkbx loadedField loadedBoolean" id="defaults-Question-TypeDetails-nr-markByDigits" /><label for="defaults-Question-TypeDetails-nr-markByDigits" style="width:auto;">Marks are per correct digit</label>
									</div>
									<div class="options_row" style="margin-bottom:0px;">
										<input type="checkbox" value="1" class="normal_chkbx" id="allowPartialMatch" /><label for="allowPartialMatch" style="width:auto;">Allow partial match</label>
									</div>
									<div id="partialValueWrapper" class="options_row">
										<label for="defaults-Question-TypeDetails-nr-partialValue" style="margin-left:25px;">Multiplier:&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2 loadedField loadedFloat" style="width:50px !important; margin-right:5px;" type="text" id="defaults-Question-TypeDetails-nr-partialValue" />
									</div>
									<div class="options_row" style="margin-bottom:0px;">
										<input type="checkbox" value="1" class="normal_chkbx loadedField loadedBoolean" id="defaults-Question-TypeDetails-nr-ignoreOrder" /><label for="defaults-Question-TypeDetails-nr-ignoreOrder" style="width:auto;">Ignore selection order</label>
									</div>
								</div>
							</div>
							<div class="labeled-input" style="margin-bottom:0px;">
								<label style="vertical-align:top; width:120px;">Decimal and scientific options:</label><div>
									<div class="options_row" style="margin-bottom:0px;">
										<input type="checkbox" value="1" class="normal_chkbx" id="allowSmallErrors" /><label for="allowSmallErrors">Allow small errors</label>
									</div>
									<div id="fudgeFactorWrapper" class="options_row">
										<label for="defaults-Question-TypeDetails-nr-fudgeFactor" style="margin-left:25px;">Tolerance (%):&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[100]] defaultTextBox2 loadedField loadedFloat" style="width:60px !important; margin-left:5px;" type="text" id="defaults-Question-TypeDetails-nr-fudgeFactor" />
									</div>
									<div class="options_row" style="margin-bottom:0px;">
										<input type="checkbox" value="1" class="normal_chkbx" id="allowFactorOfTen" \><label for="allowFactorOfTen">Give part marks for factor-of-10</label>
									</div>
									<div id="tensValueWrapper" class="options_row">
										<label for="defaults-Question-TypeDetails-nr-tensValue" style="margin-left:25px;">Multiplier:&nbsp;</label><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2 loadedField loadedFloat" data-prompt-position="topLeft" style="width:88px !important; margin-left:5px;" type="text" id="defaults-Question-TypeDetails-nr-tensValue" />
									</div>
									<div class="options_row" style="margin:10px 0px 5px;">
										<label style="text-align:left; padding:0px;">If significant digits are incorrect:</label>
									</div>
									<div class="options_row" style="margin-left:15px;">
										<select class="defaultSelectBox pad5px loadedField loadedText" style="height: 28px; width: 165px" id="defaults-Question-TypeDetails-nr-sigDigsBehaviour">
											<option value="Round">Round as needed</option>
											<option value="Zeros" selected>Add zeros as needed</option>
											<option value="Strict">Mark as incorrect</option>
										</select>
										<div id="sigDigsValueWrapper" style="margin-top:5px !important; display:block;">
											<span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#494949;">and multiply score by&nbsp;</span><input class="validate[required,custom[isPosDecimal],min[0],max[1]] defaultTextBox2 loadedField loadedFloat" data-prompt-position="topLeft" style="width:25px !important; margin:0px;" type="text" id="defaults-Question-TypeDetails-nr-sigDigsValue" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="window_section">
						<h3>Written response sections</h3>
						<div style="margin-left:20px; width:auto; display:inline-block; vertical-align:top;">

							<input type="hidden" class="loadedField loadedJSON" id="defaults-Question-TypeDetails-wr-criteria" />

							<div class="labeled-input" style="margin-bottom:0px; width:auto;">
								<label style="vertical-align:top; position:relative; top:3px;">Student responses:</label><div>
									<div class="options_row" style="margin-bottom:0px;">
										<input type="checkbox" class="normal_chkbx loadedField loadedBoolean" id="defaults-Document-Sections-wr_includeSpace" value="1" /><label for="defaults-Document-Sections-wr_includeSpace" style="text-align:left; width:228px;">Include space on response form</label>
									</div>
									<div class="options_row">
										<label for="defaults-Question-TypeDetails-wr-height">Height (inches):</label>
										<input class="validate[required,max[11.0],min[0.5]] marginSpinner loadedField loadedFloat" value="0" data-prompt-position="topLeft" type="text" id="defaults-Question-TypeDetails-wr-height" />
									</div>
								</div>
							</div>
							<div class="labeled-input" style="margin-top:10px; width:auto;">
								<label for="criteriaWrapper" style="vertical-align:top;">Scoring criteria:</label><div>
									<div class="objectives_header" style="line-height:20px; margin-bottom:5px;">
										<div style="width:130px;">Label</div>
										<div style="width:55px;">Out of</div>
									</div>
									<div id="criteriaWrapper"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="window_section">
						<h3>Variables</h3>
						<div style="margin-left:20px; width:auto; display:inline-block; vertical-align:top;">
							<div class="labeled-input" style="margin-bottom:0px; width:auto;">
								<label for="criteriaWrapper">Show variables as:</label><div>
									<select class="defaultSelectBox pad5px loadedField loadedText" id="defaults-Question-Engine-sigDigsType" style="height:30px !important; width:140px; margin:0px;">
										<option value='SigDigs'>Scientific / decimal</option>
										<option value='Scientific'>Scientific only</option>
										<option value='Decimals'>Decimal only</option>
										<option value='Fraction'>Fraction</option>
									</select>
									<div id="var_sd_wrapper" style="display:inline-block;">
										<label style="display:inline-block; width:auto; margin:0px; padding:0px;">to</label>
										<input id="defaults-Question-Engine-sigDigs" class="defaultTextBox2 intSpinner loadedField loadedInteger" type="text" />
										<label id="var_sdLabel" style="display:inline-block; width:auto; margin:0px; padding:0px;"></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div style="width:100%;">
			<div class="ui-dialog-buttonset custom-buttonpane" style="float:right;">
				<button id="settingsSave" type="button" class="btn btn-primary">Save changes</button>
			</div>
			<div style="clear:both"></div>
		</div>
	</form>
</div>

<!-- Dialog boxes-->

<div class="hidden">

<!-- {* add logo dialog *} -->

	<div id="addLogo_form" class="dialogbox"  title="Upload Logo">
		<div class="dialog_wrapper">
			
		<p>Select an image file to include as a logo, then click "Upload" to continue. Logo files must be in JPG or PNG format.</p>

		<form class="formular" id="logo_add" accept-charset="utf-8">
			<div>
				<input id="logo_target" name="jobName" type="hidden">
				<input id="logo_extension" type="hidden">


				<div style="padding-bottom:10px;">
					<span id="select_button" class="btn btn-primary fileinput-button" style="height:20px;">
						Select file
						<input id="logoUpload" type="file" name="files[]" accept="image/jpeg|image/png">
					</span>
					<span id="logoFilename" style="padding-left:10px; font-family:Arial, Helvetica, sans-serif; font-size:14px;"></span>
				</div>
                <div id="progressbar" style="width:100%;"></div>
			</div>
		</form>
		</div>
	</div>
</div>


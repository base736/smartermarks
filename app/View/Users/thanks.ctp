<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 500px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="text-align: left;">
		
		<h3>Awaiting Approval</h3>

		<p><b>Thank you for signing up for SmarterMarks!</b></p>
		
		<p>An email has been sent to <?php echo $user['email']; ?> to confirm that we have received 
		your account information.</p>
		
		<p>Your account is currently awaiting approval. This may 
		take one or two business days to be completed; you will receive a second email once 
		your account has been approved.</p>
		
	</div>
</div>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width:600px; margin:70px auto;">
	<div class="rounded_white_wrapper_inner">

		<p><b>Your email address has been added to our list.</b></p>
		
		<p>You should receive updates a few times a year from SmarterMarks.  If you would like
		to remove your email address from our list at any time, just email us at 
		<a href="mailto:support@smartermarks.com">support@smartermarks.com</a> and we'll remove
		it as soon as possible.</p>			
	</div>
</div>

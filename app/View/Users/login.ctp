<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<div class="rounded_white_wrapper" style="width: 300px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner">
		
		<h3>Log in to your account</h3>

		<div class="labeled_input">
			<label>Email Address:</label>
			<input class="login_email defaultTextBox2" type="email" />
		</div>	
			
		<div style="padding-top: 10px;">
			<div class="custom-buttonpane login_button_wrapper" style="float:right">
				<button class="login_submit btn btn-primary" type="button">Log in</button>
			</div>
			<div style="padding-top:5px; float:left">
				<?php echo $this->Html->link('Forgot Your Password?', array('controller' => 'PasswordResets', 'action' => 'send_reset'), array('class' => 'forgotPassword')); ?>
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function() {

		initSubmitProgress($('.login_button_wrapper'));
		$('.login_submit').on('click', function() {
            showSubmitProgress($('.login_button_wrapper'));
			start_login(function(response) {
				window.location.href = '/';
			}, null);
		});

	});

</script>

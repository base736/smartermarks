<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var userID = <?php echo empty($user) ? false : $user['id']; ?>;
</script>

<script type="module" src="/js/users/renew_main.js?7.0"></script>

<h2 style='margin-bottom:20px;'>
	Renewing Your Account
</h2>

<div class="rounded_white_wrapper" style="width:100%; margin-bottom:20px;">
	<div class="rounded_white_wrapper_inner" style="text-align:left; box-sizing:border-box; width:100%; padding:20px;">
		<div style="margin-bottom:20px;">
			<p>
			
			<?php
				if (!empty($user['expires'])) {
					$text = (strtotime($user['expires']) < strtotime('now')) ? 'expired' : 'expires';
					$userTimezone = new DateTimeZone($user['timezone']);
					$utcDate = new DateTime($user['expires']);
					$expiry = $utcDate->setTimezone($userTimezone);
					echo 'Your SmarterMarks account	' . $text . ' on ' . $expiry->format('F j, Y') . '. ';
				}
			?>
			
			Renewing your account is easy!</p>

			<?php
			if ($renew_by_email) {
			?>

			<p style="margin-bottom:0px;">Your school board has requested that renewals be initiated internally. To continue using 
			SmarterMarks, please choose an option below and contact your school administration. If you are are having trouble
			finding a SmarterMarks renewal contact within your board, email our business manager at 
			<a href="mailto:billing@smartermarks.com">billing@smartermarks.com</a> for more information.</p>

			<?php
			} else {
			?>

			<ul>
				<li>If you'll be joining a group another user has already set up, that user can add you to 
					their group to renew your account.</li>
				<li>Otherwise, if you'll be paying for your own renewal or that of a new group with a credit card, 
					choose one of the three options below to get started.</li>
			</ul>

			<p style="margin-bottom:0px;">For other payment options, please contact our business manager at 
			<a href="mailto:billing@smartermarks.com">billing@smartermarks.com</a>.</p>

			<?php
			}
			?>

		</div>
	</div>
</div>

<div style="display:grid; grid-template-columns:1fr 1fr 1fr; grid-template-rows:100%; column-gap:20px;">
	<div class="rounded_white_wrapper">
		<div class="rounded_white_wrapper_inner" style="display:flex; flex-direction:column; justify-content:space-between; text-align:left; box-sizing:border-box; width:100%; height:100%; padding:20px;">
			
			<div style="flex:none; width:100%;">
				<h3 style="margin-top:0px;">Individual renewals</h3>

				<div style="width:100%; text-align:center; margin-bottom:20px;">
					<div style="display:inline-block; vertical-align:top; font-size:35px;">
						$<?php 
						$secrets = Configure::read('secrets');
						echo $secrets['billing']['items']['renewal-self-1yr']['amount'];
						?>
					</div>
					<div style="display:inline-block; vertical-align:top; margin-top:3px; text-align:left; font-size:14px;">
						<div>per teacher</div>
						<div>per year</div>
					</div>
				</div>

				<?php
				if (!empty($user) && !$renew_by_email) {
				?>

				<p>Renewing just this account? Click "Renew my account" below to get back to using SmarterMarks in just a couple of clicks!</p>

				<?php
				} else {
				?>

				<p>Renewing fewer than 10 accounts? Renew accounts as required in just a few clicks.</p>

				<?php
				}
				?>

			</div>

			<?php
			if (!empty($user) && !$renew_by_email) {
			?>

			<div style="flex:none; width:100%; margin-top:15px;">
				<button id="renew_individual" class="btn btn-primary payment_button">Renew my account</button>
				<div class='wait_icon' style="display:none;"></div>
			</div>

			<?php
			}
			?>

		</div>
	</div>
	<div class="rounded_white_wrapper">
		<div class="rounded_white_wrapper_inner" style="display:flex; flex-direction:column; justify-content:space-between; text-align:left; box-sizing:border-box; width:100%; height:100%; padding:20px;">
			<div style="flex:none; width:100%;">
				<h3 style="margin-top:0px;">Group renewals</h3>

				<div style="width:100%; text-align:center; margin-bottom:20px;">
					<div style="display:inline-block; vertical-align:top; font-size:35px;">
						$<?php 
						$secrets = Configure::read('secrets');
						echo $secrets['billing']['items']['renewal-group-1yr']['amount'];
						?>
					</div>
					<div style="display:inline-block; vertical-align:top; margin-top:3px; text-align:left; font-size:14px;">
						<div>per teacher</div>
						<div>per year</div>
					</div>
				</div>

				<p>Renewing as part of a group? 
					
				<?php
				if (!empty($user) && !$renew_by_email) {
				?>

				Click "Renew for a group" below to get started! 
				
				<?php
				}
				?>

				For groups of 10 or more teachers, you'll save $<?php
				$secrets = Configure::read('secrets');
				$groupRate = $secrets['billing']['items']['renewal-group-1yr']['amount'];
				$individualRate = $secrets['billing']['items']['renewal-individual-1yr']['amount'];
				echo $individualRate - $groupRate;
				?> per teacher compared to individual renewals.</p>
			</div>

			<?php
			if (!empty($user) && !$renew_by_email) {
			?>

			<div style="flex:none; width:100%; margin-top:15px;">
				<a href="/Users/renew_group"><button class="btn btn-primary payment_button">Renew for a group</button></a>
			</div>

			<?php
			}
			?>

		</div>
	</div>
	<div class="rounded_white_wrapper">
		<div class="rounded_white_wrapper_inner" style="text-align:left; box-sizing:border-box; width:100%; height:100%; padding:20px;">
			<h3 style="margin-top:0px;">School licenses</h3>

			<div style="width:100%; text-align:left; margin-bottom:20px;">
				<div style="display:inline-block; vertical-align:top; margin:5px 5px 5px 15px; text-align:left; font-size:14px;">
					From
				</div>
				<div style="display:inline-block; vertical-align:top; font-size:35px;">
					$<?php 
					$secrets = Configure::read('secrets');
					echo $secrets['billing']['items']['renewal-school-1yr']['amount'];
					?>
				</div>
				<div style="display:inline-block; vertical-align:top; margin-top:3px; text-align:left; font-size:14px;">
					<div>per teacher</div>
					<div>per year</div>
				</div>
			</div>

			<p>In addition to individual and group renewals, we offer school licenses that can represent a 
			significant savings for schools looking to create accounts for all of their teachers.</p>

			<p>For more information, contact us at 
			<a href="mailto:billing@smartermarks.com">billing@smartermarks.com</a>.</p>
		</div>
	</div>
</div>

<?php echo $this->element("renew_common"); ?>

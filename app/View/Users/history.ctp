<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
	function getUrlVars()
	{
	    var vars = {};
	    if (window.location.href.indexOf('?') != -1) {
		    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		    for(var i = 0; i < hashes.length; i++)
		    {
		        var hash = hashes[i].split('=');
		        vars[hash[0]] = hash[1];
		    }
	    }
	    return vars;
	}
	
	$(document).ready(function() {
		$(".datepicker").datepicker({
			dateFormat: "MM d, yy",
			onSelect: function(dateText, inst) {
				$("#dateForm").submit();
			}
		});
		
		$('#dateForm').on('submit', function(e) {
			e.preventDefault();
			
			var fromDate = new Date($('#from_date').datepicker('getDate'));
			var fromString = fromDate.toISOString().substr(0, 10);
			var toDate = new Date($('#to_date').datepicker('getDate'));
			var toString = toDate.toISOString().substr(0, 10);

			var urlVars = getUrlVars();
			delete urlVars['page'];
			urlVars['from'] = fromString;
			urlVars['to'] = toString;
			var newQuery = jQuery.param(urlVars);
			window.location.href = window.location.href.split('?')[0] + "?" + newQuery;
		});
   });
</script>

<!-- Main area -->

<div style="width:890px; margin:auto;">
	<div style="position:relative; width:100%; height:48px; background-color:#2c2c2c; border-top-right-radius:5px; border-top-left-radius:5px; border-bottom-left-radius:5px; border-bottom-right-radius:5px; -moz-border-top-right-radius:5px; -moz-border-top-left-radius:5px; -moz-border-bottom-left-radius:5px; -moz-border-bottom-right-radius:5px; -webkit-border-top-right-radius:5px; -webkit-border-top-left-radius:5px; -webkit-border-bottom-right-radius:5px; -webkit-border-bottom-right-radius:5px;">
		<div style="position:relative; float:left; height:32px; color:#ccc; margin-top:10px; margin-left:10px;">
			<span style="position:relative; top:7px;">History for <?php echo $user_name;?></span>
		</div>
		<div class="search_box" style="position:relative;">
			<form id="dateForm" style="margin-bottom:0px;" accept-charset="utf-8">
				<div style="display:inline-block; color:#ccc; margin-top:10px; margin-right:20px;"><span style="position:relative; bottom:4px;">From:&nbsp;</span><input type="text" id="from_date" class="datepicker" style="width:150px;" value="<?php echo $from_date; ?>"/></div>
				<div style="display:inline-block; color:#ccc; margin-top:10px; margin-right:10px;"><span style="position:relative; bottom:4px;">To:&nbsp;</span><input type="text" id="to_date" class="datepicker" style="width:150px;" value="<?php echo $to_date; ?>"/></div>
			</form>		
		</div>
	</div>
	<div style="position:relative; top:1px; width:100%; height:35px; background-color:#2e4960; border-top-right-radius:5px; border-top-left-radius:5px; border-bottom-left-radius:0px; border-bottom-right-radius:0px; -moz-border-top-right-radius:5px; -moz-border-top-left-radius:5px; -moz-border-bottom-left-radius:0px; -moz-border-bottom-right-radius:0px; -webkit-border-top-right-radius:5px; -webkit-border-top-left-radius:5px; -webkit-border-bottom-right-radius:0px; -webkit-border-bottom-right-radius:0px;">
		<div style="position:relative; float:left; height:100%;">
			<span style="position:absolute; white-space:nowrap; top:10px; left:28px; color:#cccccc;">
				Date
			</span>
			<span style="position:absolute; white-space:nowrap; top:10px; left:155px; color:#cccccc;">
				Assessment
			</span>
			<span style="position:absolute; white-space:nowrap; top:10px; left:492px; color:#cccccc;">
				Details
			</span>
			<span style="position:absolute; white-space:nowrap; top:10px; left:787px; color:#cccccc;">
				Pages
			</span>
			<span style="position:absolute; white-space:nowrap; top:10px; left:843px; color:#cccccc;">
				Errors
			</span>
		</div>
	</div>
	<div style="width:100%; height:auto; background:rgb(255, 255, 255); background:rgba(255, 255, 255, 0.7); border-top-right-radius:0px; border-top-left-radius:0px; border-bottom-left-radius:5px; border-bottom-right-radius:5px; -moz-border-top-right-radius:0px; -moz-border-top-left-radius:0px; -moz-border-bottom-left-radius:5px; -moz-border-bottom-right-radius:5px; -webkit-border-top-right-radius:0px; -webkit-border-top-left-radius:0px; -webkit-border-bottom-right-radius:5px; -webkit-border-bottom-right-radius:5px;">
		<div style="position:relative; overflow:hidden; height:auto;">
			<table class="index_table">
				<tbody>
					<?php
					foreach($scores as $score) {
                    ?>
    					<tr data-score-id="<?php echo $score['Score']['id'];?>"">
				        <td width="0px"></td>
				        <td style="display:none" class="td_docID"><?php echo $score['Score']['id'];?></td>
				        <td width="109px"><?php 
				        	echo $this->Time->format('Y-m-d H:i', $score['Score']['created'], null, 
				        	                         new DateTimeZone(AuthComponent::user('timezone'))); 	        
				        ?></td>
				        <td width="320px"><?php 
				        	if (empty($score['Score']['document_id'])) echo "<i>[deleted]</i>";
				        	else echo $score['Document']['save_name'];
						?></td>
						<td width="278px"><?php 
							if (empty($score['Score']['title'])) echo "<i>[No details]</i>";
							else echo $score['Score']['title'];
						?></td>
						<td width="37px"><?php 
							if (!isset($score['Score']['pageCount'])) echo "--";
							else echo $score['Score']['pageCount'];
						?></td>
						<td width="37px"><?php 
							if (!isset($score['Score']['errorCount'])) echo "--";
							else echo abs($score['Score']['errorCount']);
						?></td>		    
				    </tr>
				    
					<?php
					}
					?>
			    </tbody>
			</table>
			<div style="display:flex; justify-content:space-between; height:auto; margin:20px 10px;">
			    <div class="pagination_wrapper">
			    	<?php
			    	echo $this->Paginator->prev('<', array('class' => 'previous pag_button'), null, array('class' => 'previous disabled pag_button'));
			    	echo $this->Paginator->numbers(array('class' => 'pag_button', 'separator' => ''));
			    	echo $this->Paginator->next('>', array('class' => 'next pag_button'), null, array('class' => 'next disabled pag_button'));
			    	?>
			    </div>
			</div>
		</div>
	</div>
</div>

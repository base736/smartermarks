<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="module" src="/js/users/index.js?7.0"></script>

<!-- Main area -->

	<h2 style='margin-bottom:10px;'>Users</h2>

	<div style="width:890px; margin:auto;">
		<div class="action_bar">
			<div class="action_left">
				<div class="checkbox_arrow"></div>
				<div class="btn-group">
					<button id="approveUser_button" type="button" class="btn btn-dark item_action_many"><span>Approve</span></button>
					<button id="disableUser_button" type="button" class="btn btn-dark item_action_many"><span>Disable</span></button>
				</div>
				<div class="btn-group">
					<button id="deleteUser_button" type="button" class="btn btn-dark item_action_many"><span>Delete</span></button>
				</div>
					<div class="btn-group">
						<button id="userEdit_button" type="button" class="btn btn-dark item_action_one"><span>Edit</span></button>
						<button id="userHistory_button" type="button" class="btn btn-dark item_action_one"><span>History</span></button>
					</div>
			</div>
			<div class="action_right">
				<div class="btn-group">
					<button class="btn btn-dark search_dropdown dropdown-toggle" href="#">
						Search 
						<span class="caret"></span>
					</button>
					<div class="pull-right search_menu">
						<div style="margin-bottom:5px;">
							<div style="vertical-align:middle; display:inline-block;">Search for:</div>
							<div style="vertical-align:middle; display:inline-block;">
								<select id="search_role" style="margin:0px; width:140px;">
									<option value="all" selected>All</option>
									<option value="student">Students</option>
									<option value="teacher">Teachers</option>
									<option value="admin">Admin</option>
								</select>
							</div>
						</div>
						
						<div style="position:relative; width:auto;">
							<input name="search" style="box-sizing:border-box; width:212px; height:30px; margin:0px;" type="text" id="search_input" />
							<div class="search_icon"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="window_bar" style="margin-top:1px;">
            <div style="width:5px;">&nbsp;</div>
            <div style="width:190px;">Name</div>
            <div style="width:285px;">Email</div>
            <div style="width:auto;">School</div>
		</div>
		<div class="window_content" style="height:auto;">
			<table class="index_table selection_table">
				<tbody>
				<?php
				foreach($users as $user) {
				?>
					<tr data-approved='<?php echo $user['User']['approved']; ?>' data-user-id='<?php echo $user['User']['id']; ?>'>
						<td style="width:15px; text-align:center;"><input type="checkbox" class="item_select" autocomplete="off" /></td>
						<?php
						echo "<td style='width:190px;' class='td_userName'>";
						if ($user['User']['role'] == 'admin') {
							echo "<span class='label label-info'>Admin</span> ";
						} else if($user['User']['approved'] == 0) {
							echo "<span class='label label-important'>Disabled</span> ";
						}
						if (empty($user['User']['name']) && ($user['User']['role'] == 'student')) echo '<i>Student account</i>';
						else echo htmlspecialchars($user['User']['name']);
						echo '</td>';
						?>
					
						<td style="width:285px;" class="td_userEmail"><?php echo $user['User']['email'];?></td>
						<td><?php echo htmlspecialchars($user['User']['school']);?></td>
					</tr>
				<?php
				}
				?>
				</tbody>
			</table>
			<div style="display:flex; justify-content:flex-end; height:auto; margin:20px 10px;">
				<div class="pagination_wrapper">
					<?php 
						echo $this->Paginator->prev('<', array('class' => 'previous pag_button'), null, array('class' => 'previous disabled pag_button'));
						echo $this->Paginator->numbers(array('class' => 'pag_button', 'separator' => ''));
						echo $this->Paginator->next('>', array('class' => 'next pag_button'), null, array('class' => 'next disabled pag_button'));
					?>
				</div>
			</div>
		</div>
	</div>

<div class="hidden">

<!-- Delete user dialog -->

	<div id="deleteUser_dialog" class="dialogbox" title="Delete User">
		<div class="dialog_wrapper">
			<p id="deleteUser_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Renew user dialog -->
<!--
	<div id="renewUsers_dialog" class="dialogbox" title="Renew Users">
		<div class="dialog_wrapper">
			<form id="renewUsers_form" class="formular" method="post" accept-charset="utf-8">
				<div style="display:inline-block; vertical-align:top; width:230px;">
					<div class="labeled-input">
						<label for="email_list" style="display:block; text-align:left;">User emails:</label>
						<textarea tabindex="1" name="email_list" id="email_list" class="validate[required] defaultTextBox2" style="resize:none; height:100px; width:200px;" type="text"></textarea>
					</div>
					<div class="labeled-input">
						<div class="options_row" style="margin-bottom:0px; display:block;">
							<input type="checkbox" class="normal_chkbx" name="hasExpiry" id="hasExpiry" />
							<label for="hasExpiry" style="text-align:left;">Account expires</label>
						</div>
						<div class="options_row">
							<input name="data[User][expires]" class="datepicker defaultTextBox2" style="width:200px;" type="text" id="expiry"/>
						</div>
					</div>
					<div class="labeled-input" style="margin-bottom:0px;">
						<div class="options_row" style="margin-bottom:0px; display:block;">
							<input type="checkbox" class="normal_chkbx" name="buildGroup" id="buildGroup" />
							<label for="buildGroup" style="text-align:left;">Renew under contact</label>
						</div>
						<div class="options_row">
							<input name="contactEmail" class="validate[required,funcCall[inUserList]] defaultTextBox2" style="width:200px;" type="text" id="contactEmail"/>
						</div>
					</div>
				</div>
				<div style="display:inline-block; vertical-align:top; width:230px;">
					<div class="labeled-input">
						<label for="renewal_note" style="display:block; text-align:left;">Note:</label>
						<textarea tabindex="1" name="note" id="renewal_note" class="defaultTextBox2" style="resize:none; height:50px; width:325px;" type="text"></textarea>
					</div>
					<div class="labeled-input">
						<label for="new_user_list" style="display:block; text-align:left;">New users (name, school, email):</label>
						<textarea tabindex="1" name="new_user_list" id="new_user_list" class="defaultTextBox2" style="resize:none; height:140px; width:325px;" type="text"></textarea>
					</div>
				</div>
			</form>
		</div>
	</div>
-->

</div>

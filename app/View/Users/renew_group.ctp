<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var groupDetails = <?php echo json_encode($members); ?>;
	var pricing = <?php
		$secrets = Configure::read('secrets');
		$pricing = $secrets['billing'];
		echo json_encode($pricing);
	?>;
	var userRole = "<?php echo $user['role']; ?>";
</script>

<script type="module" src="/js/users/renew_group.js?7.0"></script>

<h2 style='margin-bottom:20px;'>
	<?php echo ($user['role'] == 'admin') ? 'Renew Users' : 'Renew for a Group'; ?>
</h2>

<form id="renewal_form">
	<h3 style="margin-top:20px; margin-bottom:0px;">Group information</h3>
	<div class="rounded_white_wrapper" style="width:100%; margin:0px;">
		<div style="padding:10px;">

			<?php
			if ($user['role'] == 'admin') {
			?>

			<div class="labeled-input" style="width:auto;<?php if ($user['role'] != 'admin') echo ' display:hidden;'; ?>">
				<label for="billing_type" style="width:100px; text-align:left;">Billing type:</label>
				<select class="defaultSelectBox" style="width:150px;" id="billing_type">
					<option value="select" selected>Select one&hellip;</option>
					<option value="renewal-individual-1yr">Individual</option>
					<option value="renewal-group-1yr">Group</option>
					<option value="renewal-school-1yr">School</option>
				</select>
			</div>

			<?php
			}
			?>

			<div class="labeled-input" style="width:auto;">
				<label for="school_name" style="width:100px; text-align:left;">School:</label>
				<input tabindex="2" class="defaultTextBox2 validate[required]" style="width:310px;" maxlength="255" type="text" id="school_name" value="<?php echo htmlentities($school_name); ?>"/>
			</div>

			<div class="labeled-input" style="width:auto;">
				<label for="group_name" style="width:100px; text-align:left;">Group name:</label>
				<input tabindex="2" class="defaultTextBox2 validate[required]" style="width:310px;" maxlength="255" type="text" id="group_name" value="<?php echo htmlentities($group_name); ?>"/>
			</div>

			<table class="panel_table actions_table" style="color:#343434; border:1px solid #d7d7d7; margin-top:15px;">
				<thead>
					<tr>
						<td style="width:30%;">Name</td>
						<td>Email</td>
						<td style="width:20%;">Expires</td>
						<td style="width:10%;">Actions</td>
					</tr>
				</thead>
				<tbody id="userData"></tbody>
			</table>
			<div style="margin-top:10px; width:100%; display:flex; flex-direction:row; justify-content:space-between; align-items:center;">
                <div class="labeled-input" style="width:auto; margin:0px;<?php if ($user['role'] != 'admin') echo ' opacity:0;'; ?>">
                    <label for="prepaid_count" style="width:auto;">Add prepaid renewals:</label>
                    <input id="prepaid_count" class="defaultTextBox2 intSpinner" type="text" />
                </div>
                <button id="addUsers_button" class="btn btn-primary" type="button">Add Users</button>
			</div>
		</div>
	</div>

	<h3 style="margin-top:20px; margin-bottom:0px;">Payment</h3>
	<div class="rounded_white_wrapper" style="width:100%; margin:0px;">
		<div style="padding:10px; display:flex; justify-content:space-between; align-items:flex-start;">
			<div style="flex:none; width:auto;">
				<table id="group_payment_details">
					<tr><td>Renewing for:</td><td><span id="payment_size"></span> teacher<span id="payment_size_suffix"></span> <span id="payment_unpaid"></span></td></tr>
					<tr class="pricing_required"><td>Pricing unit:</td><td>$<span id="payment_rate"></span> per year (<span id="payment_rate_type"></span>)</td></tr>
					<tr><td>New expiry:</td>
					<td><?php if ($user['role'] == 'admin') { ?>
						<input class="datepicker defaultTextBox2" style="margin:0px !important; width:120px;" type="text" id="renewal_expiry"/>
					<?php } else { ?>
						<span id="payment_expires"></span></td></tr>
					<?php } ?></td>
					<tr class="pricing_required"><td>Total:</td><td>$<span id="payment_total"></span> + <?php echo $pricing['tax_name']; ?> 
						(<span id="payment_units"></span> units)</td></tr>
					<tr id="pricing_warning" style="display:none;"><td colspan="2" style="padding-top:15px;"><i>Select a billing type above to show pricing</i></td></tr>
				</table>
			</div>
			<div style="flex:none; width:auto; text-align:right;">
				<div>
					<div class='wait_icon' style="display:none;"></div>
					<button id="renew_button" class="btn btn-primary payment_button" type="button" >Renew Now</button>
				</div>
			</div>
		</div>
	</div>
</form>

<div class="hidden">
</div>

<?php echo $this->element("renew_common"); ?>

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

$(document).ready(function() {
	$('#timezone').val(dayjs.tz.guess());
});

</script>

	<div class="rounded_white_wrapper" style="width: 600px; margin:50px auto;">
    	<div class="rounded_white_wrapper_inner">
    		
			<h3>Create users</h3>

			<p>Enter user information below as a CSV list (name, school, email).</p>
			
	        <form action="/users/add" class="formular" id="addUsers_form" method="post" accept-charset="utf-8">
	        	<input type="hidden" name="_method" value="POST"/>
	        	<input type="hidden" id="timezone" name="timezone" value="POST"/>
	        
				<div style="clear:both; padding-top:10px;">
					<textarea id="user_info" name="user_info" class="defaultTextBox" style="height:150px !important; width:500px !important;"><?php echo $dataString; ?></textarea>
				</div>
				<button type="submit" class="btn btn-primary pull-right">Submit</button>
				<div style="clear:both;"></div>
			</form>
		</div>
	</div>

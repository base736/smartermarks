<html><body>

<p>Your SmarterMarks account has been deactivated. Visit <a href="https://smartermarks.com">smartermarks.com</a>
now to renew your account for the coming semester.</p>

<p>This email is an account update for <?php echo $userData['name']; ?>.  If you believe you have received this message in error, please contact us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.

<p>This is an automatically generated message. Replies are not monitored or answered.</p>

</body></html>

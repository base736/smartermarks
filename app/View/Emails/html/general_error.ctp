<html><body>

<?php
if (!isset($emails)) {
	echo "<div>Error for unknown user:</div>";
} else if (count($emails) == 1) {
	echo "<div>Error for " . $emails[0] . ":</div>";
} else {
	echo "<div>Error for " . count($emails) . " users:</div>";
	foreach ($emails as $email) echo "<div>" . $email . "</div>";
}

foreach ($data as $key => $value) {
?>

<div style="margin-top:10px;">
	<div style="font-weight:bold;"><?php echo $key; ?></div>
	<div><pre><?php print_r($value); ?></pre></div>
</div>

<?php
}
?>

</body></html>

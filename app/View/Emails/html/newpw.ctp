<p>We're sorry you're having problems logging into your account. Please click the link below, or copy and paste the address 
into your web browser, to reset your password:</p>

<a href="https://smartermarks.com/PasswordResets/new_password/<?php echo $token;?>">https://smartermarks.com/PasswordResets/new_password/<?php echo $token;?></a>

<p>This email was sent in response to a password reset request by <?php echo $userData['name']; ?>.  If you did not request this password reset, please let us know at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>

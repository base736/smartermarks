<html><body>

<p>An error has occurred while processing<?php 
    echo isset($payment_id) ? (' payment ID ' . $payment_id) : ' a payment'; 
    echo isset($email) ? (' for ' . $email) : '';
?>.</p>

<p><?php
if (!empty($error_message)) echo $error_message;
else echo 'No error message logged.';
?></p>

</body></html>

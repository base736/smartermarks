<html>
<body style="font-family: Arial, Helvetica, sans-serif; color: black;">

<p>Thank you for continuing with us! A receipt for your payment is included below. If you have any questions, 
please let us know at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a></p>

<table style="width:650px;">
    <tr style="vertical-align:top;">
        <td><img src="https://images.smartermarks.com/logo.png" style="width:300px;" /></td>
        <td style="font-size:36px; font-weight:bold; text-align:right;">
            <p style="margin:0px;">RECEIPT</p>
        </td>
    </tr>
    <tr style="vertical-align:top;">
        <td>
            <?php
            $secrets = Configure::read('secrets');
            echo $secrets['billing']['receipt_header'];
            ?>
            <p>Bill to: <?php echo $email; ?></p>
        </td>
        <td style="text-align:right;">
            <p style="margin-top:0px;">Payment ID: <?php echo str_pad($payment_id, 6, '0', STR_PAD_LEFT); ?><br />
            Date: <?php echo $date; ?></p>
        </td>
</table>

<table style="width:650px; margin-bottom:20px; border:1px solid black; border-collapse:collapse; text-align:right;">
    <thead>
        <tr style="font-weight:bold; border-bottom:1px solid black;">
            <td style="text-align:left; padding:3px 9px;">Item</td>
            <td style='border-left:1px inset #d7d7d7; padding:3px 9px;' width="18%">Quantity</td>
            <td style='border-left:1px inset #d7d7d7; padding:3px 9px;' width="18%">Price</td>
            <td style='border-left:1px inset #d7d7d7; padding:3px 9px;' width="18%">Amount</td>
        </tr>
    </thead>
    <tbody>

    <?php
    $total = 0.0;
    foreach ($details['invoice']['line_items'] as $line_item) {
        $thisAmount = $line_item['count'] * $line_item['price'];
        $total += $thisAmount;
    ?>
        <tr style='vertical-align:top;'>
        <td style='border:1px inset #d7d7d7; padding:3px 9px; text-align:left;'>
        <?php 
            if (!empty($line_item['description_full'])) echo $line_item['description_full'];
            else echo $line_item['description'];
        ?>
        </td>
        <td style='border:1px inset #d7d7d7; padding:3px 9px;' ><?php echo $line_item['count']; ?></td>
        <td style='border:1px inset #d7d7d7; padding:3px 9px;'>$<?php echo number_format($line_item['price'], 2); ?></td>
        <td style='border:1px inset #d7d7d7; padding:3px 9px;'>$<?php echo number_format($thisAmount, 2); ?></td>
        </tr>
    <?php
    }
    ?>

    <tr>
    <td style='border:1px inset #d7d7d7; padding:3px 9px;' colspan='3'>Subtotal</td>
    <td style='border:1px inset #d7d7d7; padding:3px 9px;'>$<?php echo number_format($total, 2); ?></td>
    </tr>

    <?php
    $tax = $total * $details['invoice']['tax_rate'];
    ?>

    <tr>
    <td style='border:1px inset #d7d7d7; padding:3px 9px;' colspan='3'><?php echo $details['invoice']['tax_name']; ?>
        (<?php echo number_format($details['invoice']['tax_rate'] * 100, 0); ?>%)</td>
    <td style='border:1px inset #d7d7d7; padding:3px 9px;'>$<?php echo number_format($tax, 2); ?></td>
    </tr>

    <tr>
    <td style='border:1px inset #d7d7d7; padding:3px 9px; font-weight:bold;' colspan='3'>Total</td>
    <td style='border:1px inset #d7d7d7; padding:3px 9px;'>$<?php echo number_format($details['invoice']['total'] / 100, 2); ?></td>
    </tr>

    <tr>
    <td style='border:1px inset #d7d7d7; padding:3px 9px; font-weight:bold;' colspan='3'>Paid</td>
    <td style='border:1px inset #d7d7d7; padding:3px 9px;'>$<?php echo number_format($details['invoice']['total'] / 100, 2); ?></td>
    </tr>
    
    </tbody>
</table>

<div><?php
echo $secrets['billing']['receipt_footer'];
?></div>

</body></html>

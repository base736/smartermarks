<html><body>

<p>Hi there!</p>

<p>You're receiving this email because your teacher has signed you up for an account with SmarterMarks. SmarterMarks is a tool that can help you learn more from the assessments you write and track your progress through a course. SmarterMarks is free for you to use as a student. Before you can write assessments online with us, you must follow the instructions below to activate your account.</p>

<?php 
if ($userData['auth_method'] == 'local') {
?>

<p>Before you can start using SmarterMarks, we need to confirm this email address. Click on the link below, or copy and paste the address into your web browser, to choose a password and you'll be ready to go.</p>

<a href="https://smartermarks.com/Users/set_password/<?php echo $token; ?>">https://smartermarks.com/Users/set_password/<?php echo $token; ?></a>

<?php
} else {
?>

<p>Before you can start using SmarterMarks, we need to confirm this email address. Click on the link below, or copy and paste the address into your web browser, to confirm your email address and you'll be ready to go.</p>

<a href="https://smartermarks.com/Users/validate_email/<?php echo $token; ?>">https://smartermarks.com/Users/validate_email/<?php echo $token; ?></a>

<?php
}
?>

<p>We hope you find SmarterMarks helpful. We'd love to hear from you at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a> if you have thoughts on how we can do what we do better!</p>

<p>Jason</p>

<p>This email was sent in response to a request by your school. If you have received this email in error, please let us know at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>

</body></html>

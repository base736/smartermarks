<html><body>

<p>Your SmarterMarks account has been activated! SmarterMarks is a tool that can help you:</p>

<ul>
<li>Build assessments, including written response, multiple choice, and numeric response questions;</li>
<li>Administer assessments and interpret the results, generating detailed feedback for you and your students including performance on defined learning outcomes; and</li>
<li>Collaborate with colleagues to build common assessments and improve them from year to year.</li>
</ul>

<?php 
if (!empty($userData['validation_token'])) {
    if ($userData['auth_method'] == 'local') {
?>

<p>Click on the link below, or copy and paste the address into your web browser, to choose a password and start exploring SmarterMarks.</p>

<a href="https://smartermarks.com/Users/set_password/<?php echo $userData['validation_token']; ?>">https://smartermarks.com/Users/set_password/<?php echo $userData['validation_token']; ?></a>

<?php
    } else {
?>

<p>Click on the link below, or copy and paste the address into your web browser, to log in and start exploring SmarterMarks.</p>

<a href="https://smartermarks.com/Users/validate_email/<?php echo $userData['validation_token']; ?>">https://smartermarks.com/Users/validate_email/<?php echo $userData['validation_token']; ?></a>

<?php
    }
}
?>

<p>You can find a tutorial video on creating response forms with SmarterMarks here:</p>

<a href="https://www.youtube.com/watch?v=8GY-1RjFCrU">https://www.youtube.com/watch?v=8GY-1RjFCrU</a>

<p>In addition, SmarterMarks can help you build assessments, making versioning and sharing items easier. There's a tutorial video on building assessments here:</p>

<a href="https://www.youtube.com/watch?v=K0xy5gn5toY">https://www.youtube.com/watch?v=K0xy5gn5toY</a>

<p>If you have any questions as you get started, please let me know!</p>

<p>Jason</p>

<p>This email was sent in response to an account request by <?php echo $userData['name']; ?>. If you have received this email in error, please let us know at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>

</body></html>

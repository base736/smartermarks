<html><body>

<p>Thank you for signing up for SmarterMarks!</p>

<p>Your user information has been received, and your account is currently awaiting approval. 
This may take one or two business days to be completed. You will receive an email once the 
approval process is complete.</p>

<p>If you did not request a SmarterMarks account, or if you have questions regarding your account,
please visit our help page at <a href="https://smartermarks.com/help">https://smartermarks.com/help</a>.</p>

</body></html>

<html><body>

<p>As you may be aware, SmarterMarks currently includes some questions which shuffle lists or images in the context, but for 
which that behaviour can't be modified by teachers. To ensure better compatibility with other features moving forward, we're 
undertaking the job of converting these to a format that's more consistent with the rest of the question bank.</p>

<p>As part of that work, the following questions which you've used in the past have been updated:</p>

<ul>
<?php
foreach ($question_ids as $id) {
?>

<li><a href="https://smartermarks.com/Questions/build/<?php echo $id; ?>">https://smartermarks.com/Questions/build/<?php echo $id; ?></a></li>

<?php
}
?>
</ul>

<p>With the update, questions in the list above will no longer automatically shuffle lists or images in the context, and will 
need to be versioned manually if you would like those lists or images shuffled. We expect to introduce tools later this semester
that will allow you to easily create those versions and bring them together under one assessment template.</p>

<p>My apologies for any inconvenience this causes. If you have any questions about this update, please let me know.</p>

<p>Jason</p>

</body></html>

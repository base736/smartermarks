<html><body>

<p>Before you can continue using SmarterMarks, we need to validate the email address associated with your account. Click on the 
link below, or copy and paste the address into your web browser, to confirm your email address and you'll be ready to go!</p>

<?php 
if (($userData['auth_method'] == 'local') && empty($userData['password'])) {
?>

<a href="https://smartermarks.com/Users/set_password/<?php echo $token; ?>">https://smartermarks.com/Users/set_password/<?php echo $token; ?></a>

<?php
} else {
?>

<a href="https://smartermarks.com/Users/validate_email/<?php echo $token; ?>">https://smartermarks.com/Users/validate_email/<?php echo $token; ?></a>

<?php
}
?>

<p>This email was sent in response to an account change request by <?php echo $userData['name']; ?>.  If you did not request this password reset, please let us know at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>

</body></html>

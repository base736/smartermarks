<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<!-- The main "default" template. All view renders will use this template unless otherwise stated -->

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
	<link rel="icon" href="/favicon/favicon.svg">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
	<link rel="manifest" href="/favicon/site.webmanifest">
	<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/favicon.ico">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-config" content="/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	
	<?php 
	if ($this->action != 'js_required') { 
	?>

	<noscript>
	<meta http-equiv="refresh" content="0; url=/js_required" />
	</noscript>

	<?php
	}
	echo $this->element('layout_head');
	?>

</head>

<body>

<div class="hidden">

<!-- Local login password dialog -->

	<div id="localLogin_dialog" class="dialogbox" title="Enter your password">
		<div class="dialog_wrapper">
			<div id="localLogin_prompt"></div>
			<form>
				<div style="width:100%; display:grid; grid-template-columns:max-content 1fr; gap:10px; align-items:center; ">
					<label style="grid-column:1; grid-row:1; text-align:left; margin:0px;">Email:</label>
					<div style="grid-column:2; grid-row:1; position:relative;">
						<input id="localLogin_email" style="width:100%; height:30px; margin:0px; box-sizing:border-box;" type="text" disabled />
					</div>
					<label style="grid-column:1; grid-row:2; text-align:left; margin:0px;">Password:</label>
					<div style="grid-column:2; grid-row:2; position:relative;">
						<input style="width:100%; height:30px; margin:0px; box-sizing:border-box;" class="login_password" type="password" />
						<button style="position:absolute; right:7px; top:5px;" type="button" title="Show password" class="input_button view_icon_grey">&nbsp;</button>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- Login with redirect dialog -->

	<div id="redirectLogin_dialog" class="dialogbox" title="Log in required">
		<div class="dialog_wrapper">
			<div style="margin-bottom:20px;">
				<p>It looks like you've been away for a while.</p>
				<p>Click "Log in" to open a login window in another tab. Once you have logged in, you will be able to return 
				to this tab to continue where you left off. Click "Cancel" to end your session.</p>
			</div>
		</div>
	</div>

<!-- Set password dialog -->

<div id="setPassword_dialog" class="dialogbox" title="Setting a password">
		<div class="dialog_wrapper">
			<div style="margin-bottom:20px;">
				<p>A password has not been set for this account. This may happen, for example, if:</p>
				<ul>
					<li>This is a new account for which a password has not been set; or</li>
					<li>Your account has previously authenticated through your school board and now does not.</li>
				</ul>
				<p>Click "Set password" below to have instructions sent to your email, including a link you can
				use to create a password.</p>
			</div>
			<form action="/PasswordResets/send_reset" class="formular" method="post" accept-charset="utf-8" novalidate>
				<input id="setPassword_email" name="data[PasswordReset][email]" type="hidden" />
			</form>
		</div>
	</div>

<!-- First redirect dialog -->

	<div id="needsEmail_dialog" class="dialogbox" title="Logging in through your school board">
		<div class="dialog_wrapper">
			<p>Your account is set to sign in through your school board. Click "Continue" to be forwarded to their 
			authentication page &mdash; you will be redirected back to SmarterMarks when that is completed.</p>

			<p>Before we send you off, we would like to collect a security email address for your account. This email
			will be used only to verify your account if you are unable to use single sign-in in the future. Your security 
			email could be a personal email address, or any other email you will always have access to.</p>

			<p>To skip adding a security email, uncheck the box below before clicking "Continue".</p>

			<div class="labeled-input" style="margin:20px 0px 5px; width:auto;">
				<label id="security_email_label" for="security_email" style="display:inline-block; width:auto;">Security email:</label>
				<input id="security_email" type="email" value="" style="width:300px;" />
			</div>
			<div class="labeled-input" style="width:auto;">
				<input type="checkbox" class="normal_chkbx" id="use_security_email" style="display:inline-block;" checked />
				<label for="use_security_email" style="display:inline-block; text-align:left; width:auto;">Include a security email for my account</label>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript" src="/js/common/helpers.js?7.0"></script>

<script type="text/javascript">

$.extend($.ui.dialog.prototype.options, {
	dialogClass: "no-close",
    closeOnEscape: false
});

$.extend($.gritter.options, { 
    position: 'bottom-left', // defaults to 'top-right' but can be 'bottom-left', 'bottom-right', 'top-left', 'top-right' (added in 1.7.1)
	fade_in_speed: 'medium', // how fast notifications fade in (string or int)
	fade_out_speed: 2000, // how fast the notices fade out
	time: 3000 // hang on the screen for...
});

checkES6Support();

// set up flag to suppress button actions while an AJAX call is in progress

var pendingAJAXPost = false;

// Set up general error handler

window.onerror = function(message, source, lineno, colno, error) {
	var showError = true;
	if (message == "Script error.") showError = false;
	else if (message == "Uncaught TypeError: Cannot redefine property: googletag") showError = false;
	else if ((lineno == 1) && (source.indexOf('.js') == -1)) showError = false; // Avoid reporting errors from browser plugins

	if (showError) {

		const errorDetails = {
			message: message,
			source: source,
			line: lineno,
			column: colno
		};

		if (error !== undefined) errorDetails.stack = error.stack;

		const errorDetailsString = JSON.stringify(errorDetails);

		fetch('/DefaultPages/report_error', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				userAgent: navigator.userAgent,
				platform: navigator.platform,
				details: errorDetailsString
			})
		});

	}
}

// Function to redirect to login when an AJAX request fails

var redirectFailedAJAX = function() {
	<?php 
	if (isset($user)) {
	?>

	var user_email = <?php echo json_encode($user['email']); ?>;

	Cookies.set('auth_redirect_action', 'close_window');
	fetch(`/Users/get_auth_redirect?email=${user_email}`)
	.then(async response => {
		pendingAJAXPost = false;
		const responseData = await response.json();

		if (responseData.success) {

			if ('redirect' in responseData) {
				
				$('#redirectLogin_dialog').dialog(
					'option',
					'buttons', {
						'Cancel': function() {
							window.location.href = "/";
						},
						'Log in': function() {
							var login_window = window.open(responseData.redirect);
							$(this).dialog("close");
						}
					}
				);
				$('#redirectLogin_dialog').dialog('open');

			} else {

				login_success = function() {
					$('#localLogin_dialog').dialog('close');
				};
				login_failure = function() {
					$('.login_password').val('').focus();
					hideSubmitProgress($('#localLogin_dialog'))
				};
	
				$('#localLogin_dialog').attr('data-cancel-action', 'logout');
				$('#localLogin_email').val(user_email);
				$('#localLogin_prompt').html('<div style="margin-bottom:20px;"><p>It looks like you\'ve been away for a while.</p> \
					<p>Enter your password below then click "Continue" to stay logged in and continue where you left off, \
					or click "Cancel" to end your session.</p></div>');
				$('#localLogin_dialog').dialog('open');

			}

		} else {

			window.onbeforeunload = null;
			window.location.reload();

		}

	}).catch(function() {

		pendingAJAXPost = false;
		window.onbeforeunload = null;
		window.location.reload();

	});
	
	<?php
	} else {
	?>

	window.onbeforeunload = null;
	window.location.reload();

	<?php
	}
	?>
}

$(document).ready(function() {
	$(document).on('click', '.reveal_button', function(e) {
        e.preventDefault();
        
		var $bar = $(this).closest('.window_bar');
		if ($bar.hasClass('reveal_hide')) {
			$bar.removeClass('reveal_hide').addClass('reveal_show');
		} else $bar.removeClass('reveal_show').addClass('reveal_hide');
	});
	
// Set up resize for column model

	$(window).on('resize', function() {
		var columnWidth = 0.5*$('body').width()-480;
		$('.column_left').css('width', Math.floor(columnWidth));
		$('.column_right').css('width', Math.ceil(columnWidth));
		$('.column_center').css('margin-left', Math.floor(columnWidth));
	}).trigger('resize');      

// Suppress default action for 'delete' (navigating back) when not on an input

	$(document).on("keydown", function (e) {
    	if (e.which === 8 && !$(e.target).is("input:not([readonly]), textarea, .html_editor, .eqn_editor, .class_list_body")) {
			e.preventDefault();
		}
	});

// Set up remote login dialog

	$('#redirectLogin_dialog').dialog({
        autoOpen: false,
		width: 420,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Cancel': function() {
				window.location.href = "/";
		 	},
			'Log in': function() { }
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Cancel")').addClass('btn btn-cancel btn_font');
		   	$buttonPane.find('button:contains("Log in")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

// Set up set password dialog

    $('#setPassword_dialog').dialog({
        autoOpen: false,
		width: 500,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Set password': function() { 

				showSubmitProgress($(this));
				$(this).find('form').submit();

			}
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Set password")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

// Set up first redirect dialog

	var submitSecurityEmail = function() {
		var message = '';
		if ($('#use_security_email').prop('checked')) {

			var loginEmail = $('.login_email').val().trim().toLowerCase();
			var securityEmail = $('#security_email').val().trim().toLowerCase();
			var emailTest = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

			if (securityEmail.length == 0) message = '* Please enter an email address';
			else if (!emailTest.test(securityEmail)) message = '* Not a valid email address';
			else if (securityEmail == loginEmail) message = '* Must not be the same as login email';
			else Cookies.set('security_email', securityEmail);
		} else Cookies.set('security_email', '');

		if (message == '') window.location.href = $('#needsEmail_dialog').attr('data-redirect');
		else $('#security_email').validationEngine('showPrompt', message, 'error', 'bottomLeft', true);
	} 

	$('#needsEmail_dialog').dialog({
        autoOpen: false,
		width: 600,
		modal: true,
		position: {
			my: "center",
			at: "center",
			of: window
		}, buttons: {
			'Continue': submitSecurityEmail
		}, open: function(event) {
			var $buttonPane = $(this).siblings('.ui-dialog-buttonpane');
		   	$buttonPane.find('button:contains("Continue")').addClass('btn btn-save btn_font');
	        $buttonPane.find('button').blur();
			initSubmitProgress($(this));
		}, close: function() {
            $('form').validationEngine('hideAll');
			hideSubmitProgress($(this));
		}
	});

	$('#use_security_email').on('change', function() {
		if ($('#use_security_email').prop('checked')) {
			$('#security_email_label').removeClass('disabled');
			$('#security_email').removeAttr('disabled');
			$('#security_email').focus();
		} else {
			$('#security_email_label').addClass('disabled');
			$('#security_email').attr('disabled', 'disabled');
		}
	});

    // Update auth cookie

    <?php 
    if (!empty($user['id'])) {
    ?>

    Cookies.set('lastAuthorized', dayjs().format(), { expires: 1 });

    <?php
    }
    ?>

	// Show client-side flash

	showClientFlash();

});

</script>

<div style="display:block; width:100%; height:23px; background-image:url('/img/default_layout/blue_bar.png'); background-repeat:repeat-x;">
</div>

<?php
if ((strtolower($this->params['controller']) != 'defaultpages') || !in_array($this->action, array('home', 'teacher', 'student'))) {
	echo $this->element('menu');
}

echo $this->Session->flash();
?>

<!-- Main content -->

<div class="row_wrapper">
<div class="column_left" style="background-image:url('/img/default_layout/background-0.jpg'); background-position:100% 0%; background-repeat:no-repeat;">
	
</div><div class="column_center" style="background-image:url('/img/default_layout/background-1.jpg'); background-position:0% 0%; background-repeat:no-repeat;"><div style="position:relative; padding:35px;">

<?php echo $this->fetch('content'); ?>	

</div></div><div class="column_right" style="background-image:url('/img/default_layout/background-2.jpg'); background-position:0% 0%; background-repeat:no-repeat;"></div>
</div>
	
<!-- Contact us bar -->

<div class="row_wrapper" style="border-style:solid none none; border-color:#a3a39d; border-width:1px;">
<div class="column_left">
	<div style="float:right; width:250px; height:100%;"></div>
</div><div class="column_center" style="padding-bottom:20px;">

<div style="margin:10px 40px;">
	<div style="display:flex; justify-content:space-between; align-items:stretch; margin-bottom:10px;">
<!--		<div style="flex:none; display:flex; align-items:flex-start;">
			<a href="https://twitter.com/smartermarks" target="_blank"><img src="/img/twitter.svg" style="height:30px; display:inline-block; margin-top:3px;" /></a>
			<div style="margin-left: 10px;" class="support_title">FOLLOW US FOR UPDATES!</div>
		</div> -->
		<div style="flex:1 1 0;">
			<div style="display:flex; align-items:flex-start;">
				<div style="flex:1 1 0; position:relative; margin-right:10px;">
					<div style="text-align:right;">
						<div class="support_title" style="margin-bottom:3px;">TECHNICAL SUPPORT</div>
						<div class="support_text">
							Questions or comments about SmarterMarks? Email us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.
						</div>
					</div>
				</div>
				<div style="flex:none; margin-top:3px;">
					<a href="mailto:support@smartermarks.com"><img src="/img/default_layout/mail.svg" style="height:35px;" /></a>
				</div>
			</div>
		</div>
	</div>
	<div style="display:flex; justify-content:flex-end; align-items:flex-end; font-family:'Times New Roman', Georgia, serif; color:#4F4E4A; font-size:9pt; cursor:default;">
		<div>&copy; 2012-2025 SmarterMarks Inc.. All rights reserved. Use of this site constitutes acceptance of our <?php echo $this->Html->link('User Agreement', array('controller' => null, 'action' => 'userAgreement')); ?> and <?php echo $this->Html->link('Privacy Policy', array('controller' => null, 'action' => 'privacyPolicy')); ?>.</div>
	</div>
</div>

</div><div class="column_right">
</div>
</div>

</body>
</html>

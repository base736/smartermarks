<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<!-- Main area -->

<script type="text/javascript">
    var tempID = "<?php echo $temp_id; ?>";
    var cacheDetails = <?php echo json_encode($cache_details); ?>;
</script>

<script type="module" src="/js/scores/set_view.js?7.0"></script>

<div class="rounded_white_wrapper" style="width: 600px; margin:50px auto;">
    <div class="rounded_white_wrapper_inner">

        <h3>Scanned results</h3>

        <div>
            <span>Saved results found in this PDF are given below.</span>
            <span class="output_other">As well, some results were present for unselected or unknown forms/versions. 
            Use the buttons to the right of each to save these or move them into the appropriate form/version.</span>
        </div>

        <div id="results_expected" style="margin-top:15px;">
            <div class="sub_panel_title">Expected results</div>
            <div class="sub_panel_content set_results_table"></div>
        </div>

        <div id="results_other" class="output_other" style="margin-top:15px;">
            <div style="display:flex; align-items:flex-start; gap:20px; ">
                <img style="flex:none; margin-top:15px; width:auto; height:50px;" src="/img/warning.svg" />
                <div style="flex:1 1 100%;">
                    <div class="sub_panel_title">Other results (not yet saved)</div>
                    <div class="sub_panel_content set_results_table"></div>
                </div>
            </div>
        </div>

    </div>
</div>
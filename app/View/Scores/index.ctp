<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="module" src="/js/scores/index.js?7.0"></script>

<?php
if ($documentID === null) {
?>

<script type="text/javascript">
var isAdmin = <?php	echo (isset($user['role']) && $user['role'] == 'admin') ? 'true' : 'false' ?>;
var documentID = false;
</script>

<!-- Main area -->

<h2 style='margin-bottom:10px;'>Results</h2>

<div style="width:890px; margin:auto;">
    <div class="action_bar">
        <div class="action_left">
            <div class="checkbox_arrow"></div>
            <div class="btn-group">
                <button id="viewScore_button" type="button" class="btn btn-dark needs_score item_action_one"><span>View score</span></button>
            </div>
        </div>
    </div>
    <div class="window_bar" style="margin:0px; padding:0px;">
        <div style="width:15px">&nbsp;</div>
        <div style="width:425px;">User</div>
        <div style="width:110px;">Created</div>
        <div style="width:140px;">PDF Hash</div>
        <div style="width:40px;">Pages</div>
        <div style="width:40px;">Errors</div>
    </div>
    <div class="window_content" style="height:auto;">
        <table class="index_table selection_table">
        </table>
		<div id="footer_wrapper" style="display:flex; justify-content:space-between; height:auto; margin:20px 10px;">
			<div id="count_wrapper" style="color:#75797c;"></div>
			<div id="pagination_wrapper" class="pagination_wrapper"></div>
		</div>
    </div>
</div>

<?php
} else {
?>

<script type="text/javascript">
    var isAdmin = <?php	echo (isset($user['role']) && $user['role'] == 'admin') ? 'true' : 'false' ?>;
    var documentID = <?php echo $documentID; ?>;
	var documentLayout = <?php echo $document_layout_string; ?>;
	var documentScoring = <?php echo $document_scoring_string; ?>;
</script>

<div id="scoring_howto_wrapper" class="rounded_white_wrapper" style="width: 890px; margin:20px auto; <?php if ($hideScoringHowto) echo "display:none"; ?>">
	<div class="rounded_white_wrapper_inner">

		<h3>Tip: Working with scans</h3>

		<p>From this index, you can easily view the results of any scan -- just click on the scan and click
		"View" in the button bar. In addition, you can:</p>
		<ul>
			<li><b>Edit the data read from scans you've uploaded.</b> Select a scan and click "Edit" to fix student responses that weren't filled in, or were filled in incorrectly.</li>
			<li><b>Analyze several scans together, or do a "filtered" analysis.</b> Select one or more assessments and click "New" to begin building a new analysis.</li>
			<li><b>Rescore scans without having to upload them again.</b> Click on the "Rescore" button when viewing the results of an analysis to apply changes made to the scans or to the assessment key.</li>
		</ul>
		
		<div class="pull-right" style="padding-top:10px">
			<a id="hide_scoring_howto" class="btn btn-primary">Hide this tip</a>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<!-- Main area -->

<div style="width:890px; margin:auto;">
	<div class="action_bar">
		<div class="action_left">
			<div class="checkbox_arrow"></div>
			<div class="btn-group">
				<button id="editScore_button" type="button" class="btn btn-dark item_action_one"><span>Edit</span></button>
				<button id="viewScore_button" type="button" class="btn btn-dark needs_score item_action_one"><span>View</span></button>

				<button id="newScore_button" type="button" class="btn btn-dark needs_score item_action_many"><span>New</span></button>
			</div>

			<div class="btn-group">
				<button id="deleteScore_button" type="button" class="btn btn-dark item_action_many"><span>Delete</span></button>
			</div>
		</div>
		<?php
		if (isset($documentName)) {
		?>
		<div class="action_right" style="color:#ccc;">Scores for "<?php echo $documentName; ?>"</div>
		<?php
		}
		?>
	</div>
	<div class="window_bar" style="margin-top:1px;">
        <div style="width:5px;">&nbsp;</div>        
        <div style="width:15px;">&nbsp;</div>
		<div class="emails_element" style="width:250px;">Owner</div>
        <div style="width:120px;">Created</div>
        <div style="width:auto;">Details</div>

		<div class="sort_wrapper">
			<div style="vertical-align:middle; display:inline-block; line-height:normal;">
				<div class="header_sort" data-direction="asc" style="display:block;">▲</div>
				<div class="header_sort" data-direction="desc" style="display:block;">▼</div>
			</div>
			<span style="white-space:nowrap;">Sort by</span>
			<select id="sort_type">
				<option value="owner" data-default-direction="asc" class="emails_element">Owner</option>
				<option value="created" data-default-direction="desc" selected>Created</option>
				<option value="details" data-default-direction="asc">Details</option>
			</select>
		</div>

	</div>
	<div class="window_content">
		<table id="scores" class="index_table selection_table">
		</table>
	</div>
</div>
	
<?php
}
?>

<div class="hidden">

<!-- Delete score dialog -->

	<div id="deleteScore_dialog" class="dialogbox" title="Delete Score">
		<div class="dialog_wrapper" style="min-height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
			<div id="deleteScore_text"></div>
		</div>
	</div>

<!-- Edit score dialog -->

	<div id="editScore_dialog" class="dialogbox" title="Edit Score">
		<div class="dialog_wrapper">
			<form id="editScore_form" class="formular" method="post" accept-charset="utf-8">
				<input id="score_id" type="hidden"/>
				<div class="labeled-input">
					<label for="score_title">Details</label>
					<input tabindex="1" id="score_title" class="defaultTextBox2" style="width:635px;" maxlength="255" type="text"/>
				</div>
				<div id="loading_wrapper" style="margin-top:20px;">
					<div class='wait_icon'></div><div style="display:inline-block;"><p style="font-size:13px;"><i>Loading response data&hellip;</i></p></div>
				</div>
				<div id="data_wrapper" style="margin-top:30px; margin-right:5px;">
					<div style="float:left;">
						<div class="labeled-input" style="width:auto;">
							<label for="assessment_number">Responses for assessment</label>
							<input tabindex="2" id="assessment_number" class="int4Spinner" maxlength="4" type="text"/>
							<span id="num_responses" style="font-family:Arial, Helvetica, sans-serif; font-size:13px;"></span>
						</div>
						<div style="width:auto;" class="score-detail-wrapper" id="score_search_wrapper">
							<div style="width:225px; position:relative; display:inline-block;">
								<input tabindex="3" class="defaultTextBox2" id="scores_search" type="text" style="width:210px;"/>
                                <div class='search_icon' style="cursor:default;"></div>
							</div>
							<span id="search_prev" class='search_nav'>&lsaquo;</span>
							<span id="search_next" class='search_nav'>&rsaquo;</span>
						</div>
					</div>
					<div style="float:right;">
						<div class="score-detail-wrapper labeled-input" style="width:auto;" id="score_name_wrapper">
							<label for="score_field_name" style="width:40px;">Name:</label>
							<input tabindex="4" id="score_field_name" class="defaultTextBox2" style="width:250px;" type="text"/>
						</div>
						<div class="score-detail-wrapper labeled-input" style="width:auto;" id="score_id_wrapper">
							<label for="score_field_id" style="width:40px;">ID:</label>
							<input tabindex="5" id="score_field_id" class="defaultTextBox2" style="width:250px;" type="text"/>
						</div>
						<div class="score-detail-wrapper labeled-input" style="width:auto;" id="score_qr_wrapper">
							<label for="score_field_qr" style="width:40px;">QR:</label>
							<input tabindex="4" id="score_field_qr" class="defaultTextBox2" style="width:250px;" type="text"/>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div id="data_wrapper_inner"></div>
				</div>
			</form>
		</div>
	</div>

	<!-- {* New analysis dialog *} -->
	<div id="newAnalysis_dialog" class="dialogbox" title="New Analysis">
		<div class="dialog_wrapper">
		<form id="filters_form" class="formular" method="post" accept-charset="utf-8">
			<input id="na_filter_string" type="hidden"/>
			<div class="labeled-input">
				<label for="na_title">Title</label>
				<input tabindex="1" id="na_title" class="defaultTextBox2" style="width:565px;" maxlength="255" type="text"/>
			</div>
			<p style="margin-bottom:20px; margin-top:20px;">
				Student data can optionally be filtered according to their responses (for example, including only students who answered 'b' to a specific question). To add filters to this analysis, click "Add filters". When you are ready to begin the analysis, click "Build".
			</p>
			<div id="filters_wrapper">
				<span style="display:inline-block;">Include only assessments which satisfy</span>
				<select class="defaultSelectBox" style="width:60px; display:inline-block; margin-bottom:0px;" id="filters_operation"> \
					<option value="all" selected>all</option> \
					<option value="any">any</option> \
				</select>
				<span>of the following:</span>
				<div id="filters_criteria" style="margin-bottom:50px; margin-top:10px;"></div>
			</div>
			<div>
				<button class="btn btn-primary btn_font" id="addFilters_button" style="float:left;">Add filters</button>
				<div class="ui-dialog-buttonset custom-buttonpane" style="float:right; margin-right:5px;">
					<button type="button" class="btn btn-cancel btn_font">Cancel</button>
					<button type="button" class="btn btn-save btn_font">Build</button>
				</div>
			</div>
		</form>
		</div>
	</div>

</div>

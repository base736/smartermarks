<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
    var repeatEvents = <?php echo $repeat_events; ?>;
    var formatXML = <?php echo json_encode($format_xml); ?>;

    var scoreID = <?php echo json_encode($score_id); ?>;
    var cacheHash = <?php echo json_encode($cache_hash); ?>;
    var getDataToken = <?php echo json_encode($get_data_token); ?>;
</script>

<script type="module" src="/js/scores/view_results.js?7.0"></script>

<div class="action_bar" style="margin-left:auto; margin-right:auto; width:600px;">
	<div class="action_left">
		<div style="color:#ccc; width:400px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">Analysis for <?php 
            echo htmlspecialchars($save_name);
		?></div>
	</div>
	<div class="action_right">
		<div class="btn-group">
			<button class="btn btn-dark dropdown-toggle" data-toggle="dropdown" href="#">
				Go to
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu text-left">
				<li><a href="/Documents/build/<?php echo $document_id; ?>" target="_blank">Assessment builder</a></li>
				<li><a href="/Scores/index/<?php echo $document_id; ?>">List of scores</a></li>
			</ul>
		</div>
	</div>
</div>

<div id="message_wrapper" style="width:600px; margin:auto; margin-top:5px;">
    <div class="window_bar">Messages</div>
    <div class="window_content">
        <div class="results_table" style="color:black; grid-template-columns: auto 1fr;">

            <div class="view_error_text results_table_cell message_loading" style="grid-column-start:1; grid-column-end:3;">
                <div class="wait_icon"></div>
				<i>Loading&hellip;</i>
            </div>

            <div class="results_table_cell message_repeat_events" style="padding-right:0px; justify-self:center;">
                <img style="height:60px;" src="/img/lightbulb.svg" />
            </div>
            <div class="results_table_cell message_repeat_events">
                <p>Did you know:</p>
                <p>If you've made changes to your key or edited student responses, you don't need to
                upload your PDF again to rescore it. Changes should be applied here automatically.</p>
            </div>

            <div class="results_table_cell message_warnings" style="padding-right:0px; justify-self:center;">
                <img style="margin-top:5px; height:50px;" src="/img/warning.svg" />
            </div>
            <div class="results_table_cell message_warnings">
			    <p>Warnings were encountered in processing this PDF:</p>
                <ul id="warnings_list"></ul>
			    <p>Warnings may indicate a problem with the assessment, or simply an unusual but successfully 
                processed page. Persistent warnings, or several warnings on one PDF, may indicate problems with 
                scanner settings.</p>
            </div>

            <div class="results_table_cell message_errors" style="padding-right:0px; justify-self:center;">
                <img style="height:50px;" src="/img/error.svg" />
            </div>
            <div class="results_table_cell message_errors">
                <p>Errors were encountered in processing this PDF:</p>
                <ul id="errors_list"></ul>
			    <p>Errors indicate that a page could not be processed.

                <?php if ($format_xml !== false) { ?>
                <b><a href="#" id="show_pdf">Click here</a></b> to view the response form we expected.
                <?php } ?>

                </p>
            </div>
        </div>
    </div>
</div>

<div id="reports_wrapper" style="width:600px; margin:auto; margin-top:5px; display:none;">
    <div class="window_bar">Reports</div>
    <div class="window_content">
        <div class="results_table" style="grid-template-columns: 1fr auto;">

            <div class="results_table_cell"><b>Student reports</b>. Individual reports for each student, including a summary
            of the student's responses and optionally the answer key and a learning goal report. 
            </div>
            <div class="results_table_cell" style="justify-self:end;">
                <a href="/Scores/view_student/<?php echo $score_id; ?>" target="_blank"><button class='btn btn-info'>View</button></a>
            </div>

            <div class="results_table_divider"></div>
            
            <div class="results_table_cell"><b>Teacher Summary</b>. Includes score distribution, collusion analysis, and
            a response analysis for each question in the assessment.
            </div>
            <div class="results_table_cell" style="justify-self:end;">
                <a href="/Scores/view_teacher/<?php echo $score_id; ?>" target="_blank"><button class='btn btn-info'>View</button></a>
            </div>

            <div class="results_table_divider"></div>

            <div class="results_table_cell"><p><b>Response Summary</b>. Two summaries are available:</p>
                <ul>
                    <li>An Excel file containing raw student responses and scores.</li>
                    <li>A simple CSV file containing only student names and total scores, suitable for grade book software like PowerTeacher.</li>
                </ul>
            </div>
            <div id="view_button_wrapper" class="results_table_cell" style="justify-self:end; display:inline-block;">
                <div class='wait_icon' style="opacity:0;"></div>
                <div class="btn-group" style="display:inline-block;">
                    <button class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
                        View 
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="#" id="get_xlsx">Excel Summary</a></li>
                        <li><a href="#" id="get_csv">Simple CSV</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

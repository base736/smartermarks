<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

var isPrint = false;
var scoreID = <?php echo json_encode($score_id); ?>;
var cacheHash = <?php echo json_encode($cache_hash); ?>;
var getDataToken = <?php echo json_encode($get_data_token); ?>;

</script>

<script type="module" src="/js/scores/view_teacher.js?7.0"></script>

<div class="rounded_white_wrapper" style="width:900px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="padding-top:40px; padding-bottom:40px;">

        <div style="display:flex; justify-content:space-between;">
            <div id="report_title" style="flex:0 0 auto;"></div>
            <div style="flex:0 0 auto;">
                <button class="btn btn-primary" id="print_button" >Build PDF</button>
            </div>
        </div>

        <div id="assessment_profile_wrapper">
            <h3 style='margin-top:30px;'>Assessment profile</h3>

            <div id="graph_wrapper">
                <h4>Assessment score distribution:</h3>
                <div id='div_scoreDistribution' style='width:600px; height:300px; margin:0 auto;'></div>
            </div>

            <div id="minor_graphs_wrapper">
                <h4 style='margin-top:50px;'>Average score and discrimination:</h4>
                <div style='width:600px; margin:0 auto;'>
                    <div id='div_difficultyDistribution' style='float:left; width:280px; height:170px;'></div>
                    <div id='div_discriminationDistribution' style='float:right; width:280px; height:170px;'></div>
                    <div style='clear:both;'></div>
                </div>
            </div>

            <div id="statistics_wrapper">
                <h4 style='margin-top:50px;'>Assessment statistics: <button class='btn btn-light btn-mini info_button statistics_info'>Info</button></h4>
            
                <table id="stats_table" class='report_table'>
                    <tr class='tableHeader'><td>Measure</td><td>Value</td></tr>
                </table>
            </div>

            <div id="collusion_wrapper">
                <h4 style='margin-top:50px;'>Collusion analysis: <button class='btn btn-light btn-mini info_button collusion_info'>Info</button></h4>

                <div id='collusion_results'></div>
            </div>
            
            <div id="outcomes_wrapper">
                <h4 style='margin-top:50px;'>Learning outcomes summary: <button class='btn btn-light btn-mini info_button outcomes_info'>Info</button></h4>

                <p>Results of this assessment, reported by learning outcome:</p>
                
                <table id='outcomes_table' class='report_table'>
                    <tr class='tableHeader'><td style='width:350px; text-align:left;'>Outcome</td><td>#</td><td>Mean</td><td>Median</td><td>&alpha;</td></tr>
                </table>
            </div>
        </div>

        <div id="item_analysis_wrapper">
            <h3 style='margin-top:50px;'>Item analysis</h3>

            <div id="highlighted_notice" class="item_highlighted" style="border:1px solid #ccc; margin-bottom:10px; padding:10px; display:none;">
                <p>This assessment contains questions with very low discrimination. Low discrimination may mean that:</p>
                <ul>
                    <li>The question has been miskeyed;</li>
                    <li>The language used in the question is confusing even to stronger students; or</li>
                    <li>Most students answered the question correctly (or incorrectly).</li>
                </ul>
                <p>You may consider modifying these items so that they give better feedback to students. Items of concern are highlighted below.</p>
                <p>New to item statistics? <a href='https://www.youtube.com/watch?v=LkMErsoLAhI' target='_blank'>Click
                here</a> to learn more about average score and discrimination.</p>
            </div>

            <div id='item_statistics_wrapper' style='display:grid; grid-template-columns:auto 1fr auto 1fr; row-gap:10px;'></div>
        </div>

	</div>
</div>

<div class="hidden">

    <!-- {* Statistics info dialog *} -->
    <div id="statisticsInfo_dialog" class="dialogbox info_dialog" title="Assessment statistics">
        <div class="dialog_wrapper">
            <p><b>Mean score</b>: the average overall score on this assessment.</p>
            <p><b>Median score</b>: score for which 50% of students did better, and 50% did worse.</p>
            <p><b>Standard deviation (&sigma;)</b>: the total variation in scores on this assessment due to differences in student knowledge, measurement error, and other factors.</p>
            <p><b>Standard error of measurement</b>: an estimate of the variation in scores due to measurement error alone. This is a measure of the uncertainty in each student's grade.</p>
            <p><b>Cronbach's alpha (&alpha;)</b>: an estimate of the reliability of the assessment, the likelihood that a similar test would produce similar results for each student. For classroom tests, the following benchmarks may be helpful:</p>
            <table class='report_table' style='margin-top:20px;'>
                <tbody>
                    <tr class='tableHeader'><td>Value</td><td>Interpretation</td></tr>
                    <tr><td style='text-align:left'>&lt; 0.6</td><td style='text-align:left'>Assessment needs broad revision.</td>
                    <tr><td style='text-align:left'>0.6&ndash;0.7</td><td style='text-align:left'>Some items likely need improvement.</td>
                    <tr><td style='text-align:left'>0.7&ndash;0.9</td><td style='text-align:left'>Good reliability in a classroom test.</td>
                    <tr><td style='text-align:left'>&gt; 0.9</td><td style='text-align:left'>Excellent reliability.</td>
                </tbody>
            </table>
        </div>
    </div>

    <!-- {* Collusion info dialog *} -->
    <div id="collusionInfo_dialog" class="dialogbox info_dialog" title="Collusion analysis">
        <div class="dialog_wrapper">
            <p>A detailed analysis has been performed of the similarity of students' responses. The analysis draws its statistics from student responses, so that if a response is very common, coincidences there will not be weighted as heavily in the analysis.</p>
            <p>If collusions are detected, a probability will be given that two students seated next to each other would have an equivalent or larger set of responses in common by chance.</p>
            <p><b>Important</b>: Probabilities given assume that students are seated next to one another. In large groups, false positives between students who are not next to one another are common. For example, in a class of 30 students, a probability of 1 in 1,000 will be reported for some pair of students in 35% of assessments even without collusion.</p>
        </div>
    </div>

    <!-- {* Outcomes info dialog *} -->
    <div id="outcomesInfo_dialog" class="dialogbox info_dialog" title="Learning outcomes summary">
        <div class="dialog_wrapper">
            <p>The learning outcomes summary is an analysis of students' performance on the learning outcomes you specified in your assessment. The table gives students' mean and median scores on each topic, along with the reliability of the topic scores. See the &quot;Assessment statistics&quot; section for more information on reliability.</p>
        </div>
    </div>

    <!-- {* Question label info dialog *} -->
    <div id="questionInfo_dialog" class="dialogbox info_dialog" title="Question labels">
        <div class="dialog_wrapper">
            <p>Questions are labeled by section and question number. For example, '1.5' would represent the fifth question in the first section of the assessment.</p>
        </div>
    </div>

    <!-- {* Averages info dialog *} -->
    <div id="averageInfo_dialog" class="dialogbox info_dialog" title="Average score">
        <div class="dialog_wrapper">
            <p>This gives the average score students obtained on each question. Each question style is appropriate to testing concepts of a different difficulty, as shown below:</p>
            <table class='report_table' style='margin-top:20px;'>
                <tbody>
                    <tr class='tableHeader'><td>Format</td><td>Ideal average</td></tr>
                    <tr><td style='text-align:left'>True-false</td><td style='text-align:right'>85%</td>
                    <tr><td style='text-align:left'>3 choice multiple choice</td><td style='text-align:right'>77%</td>
                    <tr><td style='text-align:left'>4 choice multiple choice</td><td style='text-align:right'>73%</td>
                    <tr><td style='text-align:left'>5 choice multiple choice</td><td style='text-align:right'>70%</td>
                    <tr><td style='text-align:left'>Numeric response</td><td style='text-align:right'>60%</td>
                </tbody>
            </table>
            <p style='margin-top:20px;'><a href="https://www.youtube.com/watch?v=LkMErsoLAhI" target="_blank">Click here</a> to learn more about difficulty and discrimination.</p>
        </div>
    </div>

    <!-- {* Discrimination info dialog *} -->
    <div id="pearsonInfo_dialog" class="dialogbox info_dialog" title="Discrimination">
        <div class="dialog_wrapper">
            <p>Students who scored well on an assessment should generally score better than average on well-written questions. Discrimination is a measure of how true that is for each question. For questions with a moderate difficulty, the following benchmarks may be helpful:</p>
            <table class='report_table' style='margin-top:20px;'>
                <tbody>
                    <tr class='tableHeader'><td>Discrimination</td><td>Description</td></tr>
                    <tr><td style='text-align:left'>&lt; 0.2</td><td style='text-align:left'>Poor discrimination. Question may be miskeyed, or the language is confusing even for strong students. These questions should be revised.</td>
                    <tr><td style='text-align:left'>0.2&ndash;0.4</td><td style='text-align:left'>Fair discrimination.</td>
                    <tr><td style='text-align:left'>&gt; 0.4</td><td style='text-align:left'>Good discrimination.</td>
                </tbody>
            </table>
            <p style='margin-top:20px;'><a href="https://www.youtube.com/watch?v=LkMErsoLAhI" target="_blank">Click here</a> to learn more about difficulty and discrimination.</p>
        </div>
    </div>

</div>
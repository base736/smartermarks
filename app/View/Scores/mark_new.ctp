<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script>
var userID = <?php echo $user['id']; ?>;
var documentIDs = <?php echo json_encode(array_keys($documents)); ?>;
var formats = <?php echo json_encode($formats); ?>;

</script>

<script type="module" src="/js/scores/mark_new.js?7.0"></script>

<!-- Main area -->

<div class="rounded_white_wrapper" style="width: 600px; margin:50px auto;">
    <div id="select_panel" class="rounded_white_wrapper_inner">

        <h3>Select your scans</h3>

        <p>Selecting scans for the following <?php echo (count($documents) > 1) ? 'assessments' : 'assessment'; ?>:</p>
        <ul>
            <?php
            foreach ($documents as $document_id => $document_name) {
            ?>
    
            <li><b><?php echo $document_name; ?></b></li> 

            <?php
            }
            ?>
        </ul>
        <p>Click "Select PDF" to choose a PDF containing scanned response forms, or drag and drop a file here. Enter any details you choose, then click "Read PDF".</p>
        
        <br>
        <div id="fileInterface" style="position:relative; height:140px;">
            <div id="inputs" style="position:absolute; height:100%; width:100%; opacity:1.0;">
                <div style="padding-bottom:10px">
                    <span id="select_button" class="btn btn-primary fileinput-button" style="height:20px;">
                        Select PDF
                        <input id="pdf_select" type="file" name="files[]" accept="application/pdf">
                    </span>
                    <span id="filename" style="padding-left:10px;">No file selected</span>
                </div>
                <div style="clear:both;">
                    <div style="padding-bottom:5px;">Details (optional)</div>
                    <input id="details" type="text" class="defaultTextBox" style="width:500px" autocomplete="off" />
                </div>
                <div style="clear:both; height:auto; display:flex; justify-content:flex-end; align-items:center;">
                    <button id="read_button" class="btn btn-primary" style="flex:none;" disabled>Read PDF</button>
                </div>
            </div>
            <div id="fileDropTarget" style="position:absolute; top:-8%; left:-2%; height:112%; width:104%; border-radius:15px; background:white; 
                    border-style:solid; border-width:2px; border-color:black; text-align:center; display:none;">
                <h2>Drop your file here</h2>
            </div>
        </div>
    </div>
    <div id="waiting_panel" class="rounded_white_wrapper_inner" style="display:none;">

        <h3>Processing scans</h3>

        <p>Your scanned PDF is now being processed. Please keep this window open &mdash; you will be automatically 
        forwarded to the results view when the analysis is complete.</p>

        <br/>

        <div id="progressbar"></div>
        <div id="progresstext" style="text-align:left; width:500px;">Starting job&hellip;</div>

        <div style="clear:both; height:auto; display:flex; justify-content:flex-end; align-items:flex-end;">
            <div id="cancel_wait" class='wait_icon' style="flex:none; margin:0px; display:none;"></div>
            <div id="cancel_button" class="btn btn-danger" style="margin-left:10px; flex:none;">Cancel</div>
        </div>

    </div>

</div>
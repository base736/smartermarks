<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

var isPrint = false;
var scoreID = <?php echo json_encode($score_id); ?>;
var cacheHash = <?php echo json_encode($cache_hash); ?>;
var getDataToken = <?php echo json_encode($get_data_token); ?>;

</script>

<script type="module" src="/js/scores/view_student.js?7.0"></script>

<div class="rounded_white_wrapper" style="width:900px; margin:50px auto;">
	<div class="rounded_white_wrapper_inner" style="padding-top:40px; padding-bottom:40px;">

        <div style="display:flex; justify-content:space-between;">
            <div id="reports_title" style="flex:0 0 auto;"></div>
            <div style="flex:0 0 auto;">
                <button class="btn btn-primary" id="print_button" >Build PDF</button>
            </div>
        </div>

		<div id="student_reports"></div>
    </div>
</div>
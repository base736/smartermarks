<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
echo $this->element('layout_head', array(
    'include_print_element' => true
));
?>

<script type="text/javascript">

var isPrint = true;
var getDataToken = <?php echo json_encode($get_data_token); ?>;
var scoreID = <?php echo json_encode($score_id); ?>;
var cacheHash = <?php echo json_encode($cache_hash); ?>;

</script>

<script type="module" src="/js/scores/view_teacher.js?7.0"></script>

<style>
	body, html {
		-webkit-print-color-adjust: exact;
		background-color: transparent;
		margin: 0px;


        font-family: "Times New Roman", Times, serif;
        font-size: 12pt;
    }

    h2 {
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 16pt;
    }

    h3 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16pt;
    }

    h4 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
    }

    .stats_table_element:nth-child(-n + 10) {
        display: table-row;
    }

    .stats_show_more, .info_button {
        display: none;
    }

    #item_statistics_wrapper .item_highlighted {
        background: transparent !important;
    }
</style>

</head>

<body style="background-color:none;">

    <div id="report_title"></div>

    <div id="assessment_profile_wrapper">
        <h3 style='margin-top:30px;'>Assessment profile</h3>

        <div id="graph_wrapper">
            <h4>Assessment score distribution:</h3>
            <div id='div_scoreDistribution' style='width:600px; height:300px; margin:0 auto;'></div>
        </div>

        <div id="minor_graphs_wrapper" style="page-break-after:always">
            <h4 style='margin-top:2em;'>Average score and discrimination:</h4>
            <div style='width:600px; margin:0 auto;'>
                <div id='div_difficultyDistribution' style='float:left; width:280px; height:170px;'></div>
                <div id='div_discriminationDistribution' style='float:right; width:280px; height:170px;'></div>
                <div style='clear:both;'></div>
            </div>
        </div>

        <div id="statistics_wrapper">
            <h4 style='margin-top:2em;'>Assessment statistics: <button class='btn btn-light btn-mini info_button statistics_info'>Info</button></h4>
        
            <table id="stats_table" class='report_table'>
                <tr class='tableHeader'><td>Measure</td><td>Value</td></tr>
            </table>
        </div>

        <div id="collusion_wrapper">
            <h4 style='margin-top:2em;'>Collusion analysis: <button class='btn btn-light btn-mini info_button collusion_info'>Info</button></h4>

            <div id='collusion_results'></div>
        </div>
        
        <div id="outcomes_wrapper">
            <h4 style='margin-top:2em;'>Learning outcomes summary: <button class='btn btn-light btn-mini info_button outcomes_info'>Info</button></h4>

            <p>Results of this assessment, reported by learning outcome:</p>
            
            <table id='outcomes_table' class='report_table'>
                <tr class='tableHeader'><td style='width:350px; text-align:left;'>Outcome</td><td>#</td><td>Mean</td><td>Median</td><td>&alpha;</td></tr>
            </table>
        </div>
    </div>

    <div id="item_analysis_wrapper">
        <h3 style='margin-top:2em;'>Item analysis</h3>

        <div id='item_statistics_wrapper' style='display:flex; flex-wrap:wrap; row-gap:30px;'></div>
    </div>

</body>

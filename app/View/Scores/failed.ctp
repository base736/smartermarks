<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<!-- Main area -->

<script type="text/javascript">
    var formatXML = <?php echo json_encode($format_xml); ?>;
</script>

<script type="module" src="/js/scores/failed.js?7.0"></script>

<div class="action_bar" style="margin-left:auto; margin-right:auto; width:600px;">
	<div class="action_left">
		<div style="color:#ccc; width:400px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">
            Analysis for <?php echo $score_name; ?>
        </div>
	</div>
</div>

<div style="width:600px; margin:auto; margin-top:5px;">
    <div class="window_bar">Messages</div>
    <div class="window_content">
        <div class="results_table" style="color:black; grid-template-columns: auto 1fr;">
            <div class="results_table_cell" style="display:block; padding-right:0px; justify-self:center;">
                <img style="height:50px;" src="/img/error.svg" />
            </div>
            <div class="results_table_cell" style="display:block;">
			    <p>Errors were encountered in processing this PDF:</p>
                <ul>
                    <li><?php echo $cache_details['message']; ?></li>
                </ul>
			    <p>Errors indicate that a PDF could not be processed.

                <?php 
                if (!empty($format_xml)) {
                ?>

                <b><a href="#" id="show_pdf">Click here</a></b> to view the response form we expected.

                <?php
                }
                ?>

                </p>
            </div>
        </div>
    </div>
</div>

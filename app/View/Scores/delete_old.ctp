<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var maxDate = "<?php echo $maxDate; ?>";

	var updateOld = function() {
		var date = dayjs($('#max_date').datepicker('getDate')).utc();
		ajaxFetch('/Scores/create_delete_job', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				maxDate: date.format('YYYY-MM-DD HH:mm:ss')
			})
		}).then(async response => {
			const responseData = await response.json();

			var html = '';
			for (var i = 0; i < responseData.data.length; ++i) {
				html += '<tr><td colspan="2"><b>' + responseData.data[i].title + '</b></td></tr>';							
				for (var j = 0; j < responseData.data[i].rows.length; ++j) {
					var date = dayjs.utc(responseData.data[i].rows[j].created).local();
					html += '<tr><td>' + date.format('MMMM D, YYYY') + '</td>' + 
						'<td>' + responseData.data[i].rows[j].description + '</td></tr>';
				}
			}

			if (html.length == 0) {
				html = '<tr><td colspan="2"><i>No data to show here.</i></td></tr>';
			}

			$('#data_list').html(html);
			$('#data_list').attr('data-token', responseData.token);
		});

	}

	$(document).ready(function() {
		$(".datepicker").datepicker({dateFormat: "MM d, yy"});

		var dateParts = maxDate.match(/(\d+)/g);
		var dateObject = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
		$('#max_date').datepicker('setDate', dateObject);

		$('#confirmation').on('change', function() {
			if ($(this).prop('checked')) $('#delete_scores').removeAttr('disabled');
			else $('#delete_scores').attr('disabled', 'disabled');
		});

		initSubmitProgress($('#delete_wrapper'));
		$('#delete_scores').attr('disabled', 'disabled');

		$('#delete_scores').on('click', function() {
			if ($('#data_list')[0].hasAttribute('data-token')) {
				showSubmitProgress($('#delete_wrapper'));
				ajaxFetch('/Scores/run_delete_job/' + $('#data_list').attr('data-token'), {
					method: 'POST'
				}).then(async response => {
					const responseData = await response.json();

					hideSubmitProgress($('#delete_wrapper'));
					if (responseData.success) {
						$.gritter.add({
							title: "Success",
							text: "Student data deleted.",
							image: "/img/success.svg"
						});
					} else {
						$.gritter.add({
							title: "Error",
							text: "Unable to delete student data.",
							image: "/img/error.svg"
						});
					}

					$('#confirmation').prop('checked', false);
					$('#delete_scores').attr('disabled', 'disabled');
					updateOld();
				});
			} else {
				$.gritter.add({
					title: "Error",
					text: "No date selected or list expired. Click 'Update list' to rebuild.",
					image: "/img/error.svg"
				});
			}
		});

		$('#update_list').on('click', function() {
			$('#confirmation').prop('checked', false);
			$('#delete_scores').attr('disabled', 'disabled');
			updateOld();
		});
	});
</script>

<h2 style="margin-bottom:20px;">Delete Old Scores</h2>

<div class="window_bar">Choose a date</div>
<div class="window_content" style="margin-bottom:20px; padding:20px">
	<div style="margin-bottom:20px;">
		<p>Choose a date before which your student data should be deleted, then click "Update list" to show the matching student data below.</p>

		<p>When you have confirmed that the data shown should be deleted, check the box at the bottom of the list and click "Delete scores" 
		to permanently delete the data. Anonymized responses for multiple choice and numeric response questions will be retained to allow 
		the calculation of item statistics.</p>
	</div>

	<div style="display:flex; justify-content:space-between;">
		<div>
			<div class="labeled-input" style="margin:0px;">
				<label>Delete student data from before:</label><input class="datepicker defaultTextBox2" style="width:125px; margin:0px;" type="text" id="max_date"/>
			</div>
		</div>
		<div>
			<button type="button" id="update_list" class="btn btn-primary btn_font">Update list</button>
		</div>
	</div>
</div>

<div class="window_bar">Data to be deleted</div>
<div class="window_content" style="margin-bottom:20px;">
	<div style="width:100%; max-height:300px; overflow-y:scroll;">
		<table class="panel_table">
			<thead>
				<tr>
					<td width="20%">Date</td>
					<td>Details</td>
				</tr>
			</thead>
			<tbody id="data_list">
				<tr><td colspan="2"><i>Choose a date above and click "Update list" to begin.</i></td></tr>
			</tbody>
		</table>
	</div>
	<div style="padding:20px; display:flex; justify-content:space-between;">
		<div>
			<div class="labeled-input" style="width:auto; margin:0px;">
				<input type="checkbox" class="normal_chkbx" id="confirmation" /><label>I understand that the student data listed will be <b>permanently deleted</b>.</label>
			</div>
		</div>
		<div id="delete_wrapper" class="custom-buttonpane">
			<button type="button" id="delete_scores" class="btn btn-danger btn_font" disabled>Delete scores</button>
		</div>
	</div>
</div>

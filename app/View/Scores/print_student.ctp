<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
echo $this->element('layout_head', array(
    'include_print_element' => true
));
?>

<script type="text/javascript">

var isPrint = true;
var getDataToken = <?php echo json_encode($get_data_token); ?>;
var scoreID = <?php echo json_encode($score_id); ?>;
var cacheHash = <?php echo json_encode($cache_hash); ?>;

var pageSize = '<?php echo $page_size; ?>';
var marginSizes = <?php echo json_encode($margin_sizes); ?>;

</script>

<script type="module" src="/js/scores/view_student.js?7.0"></script>

<style>
	body, html {
		-webkit-print-color-adjust: exact;
		background-color: transparent; 
		margin: 0px;

        font-family: "CMU Serif", "Times New Roman", Times, serif;
        font-size: 12pt;
        line-height: 1.0;
	}

    .student_report_title {
        margin-top: 0px; 
    }

    #student_reports>div {
        page-break-inside: avoid;
    }

	@page  
	{ 
        margin: 0px;
	}
</style>

</head>

<body style="background-color:none;">

    <div id="student_reports" class="printed_report"></div>

</body>

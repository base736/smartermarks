<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

	$(function() {

		var historyData = <?php echo $historyData;?>;

		var options = {
			series: {
				lines: {
					show: true,
					lineWidth: 2
				},
				shadowSize: 0
			},
			xaxis: {
				mode: "time",
				tickLength: 5
			},
			yaxis: {
				position: "left",
				axisLabel: "bar"
			},
			selection: {
				mode: "x"
			},
			grid: {
				hoverable: true
			}
		};

		var plot = $.plot("#mainView", historyData, options);

		var overview = $.plot("#overview", historyData, {
			series: {
				lines: {
					show: true,
					lineWidth: 1
				},
				shadowSize: 0
			},
			xaxis: {
				ticks: [],
				mode: "time"
			},
			yaxis: {
				ticks: [],
				min: 0,
				autoscaleMargin: 0.1
			},
			selection: {
				mode: "x"
			}
		});

		function selectPlot(index) {
			selectedPlot = index;
			
			plot.setData(historyData[index]);
			plot.setupGrid();
			plot.draw();

			overview.setData(historyData[index]);
			overview.setupGrid();
			overview.draw();
		}
		
		$('#viewWeekly_button').click(function() {
			selectPlot(0);
		});

		$('#viewTotal_button').click(function() {
			selectPlot(1);
		});

		// now connect the two

		$("#mainView").bind("plotselected", function (event, ranges) {

			// do the zooming
			$.each(plot.getXAxes(), function(_, axis) {
				var opts = axis.options;
				opts.min = ranges.xaxis.from;
				opts.max = ranges.xaxis.to;
			});
			plot.setupGrid();
			plot.draw();
			plot.clearSelection();

			// don't fire event on the overview to prevent eternal loop

			overview.setSelection(ranges, true);
		});

		$("#overview").bind("plotselected", function (event, ranges) {
			plot.setSelection(ranges);
		});

		$("<div id='tooltip' class='graph-tooltip'></div>").appendTo("body");

		$("#mainView").bind("plothover", function (event, pos, item) {
			if (item) {
				var date = new Date(item.datapoint[0]);
				var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
				var numProcessed = item.datapoint[1];
				$("#tooltip").html(months[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() +  "<br />" + numProcessed.toFixed(0) + " processed")
					.css({top: item.pageY-35, left: item.pageX+10})
					.fadeIn(200);
			} else {
				$("#tooltip").hide();
			}
		});
	});
	
	$(document).ready(function(){
		$('#viewWeekly_button').trigger("click");
	});
	
</script>

<!-- Main area -->
<div class="rounded_white_wrapper" style="width:890px; margin:50px auto; padding:10px;">

	<div style="position:relative; float:right; height:100%; margin-top:5px;">
		<div class="btn-group">
			<button id="viewWeekly_button" type="button" class="btn"><span>Weekly</span></button>
		</div>
		<div class="btn-group" style="margin-right:20px;">
			<button id="viewTotal_button" type="button" class="btn"><span>Total</span></button>
		</div>
	</div>
	<div style="clear:both"></div>

	<div class="graph-container" style="height:500px;">
		<div id="mainView" class="graph-placeholder"></div>
	</div>

	<div class="graph-container" style="height:150px;">
		<div id="overview" class="graph-placeholder"></div>
	</div>	
</div>

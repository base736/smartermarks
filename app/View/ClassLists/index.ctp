<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<!-- Main area -->

<script>
	var receivedCount = <?php echo isset($receivedCount) ? $receivedCount : '0'; ?>;
</script>

<script type="module" src="/js/classlists/index.js?7.0"></script>

<h2 style='margin-bottom:10px;'>Class Lists</h2>

<div style="width:890px; margin:auto;">
	<div class="action_bar">
		<div class="action_left">
			<div class="checkbox_arrow"></div>

            <div class="btn-group">
					<button class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
						Build 
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu text-left">
						<li><a href="#" id="editList_button" class="item_action_one">Edit list</a></li>
						<li><a href="#" id="duplicateList_button" class="item_action_one">Duplicate</a></li>
						<li><a href="#" id="mergeLists_button" class="item_action_many">Merge lists</a></li>
					</ul>
				</div>

			<div class="btn-group">
				<button id="deleteList_button" type="button" class="btn btn-dark item_action_many"><span>Delete</span></button>
			</div>

            <div class="btn-group">
                <button id="share_dropdown" class="btn btn-dark dropdown-toggle item_action_many" data-toggle="dropdown" href="#">
                    Share 
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left">
                    <li><a href="#" id="sendCopy_button" class="item_action_many needs_copy">Send a copy</a></li>
                </ul>
            </div>
		</div>
	</div>
	<div class="window_bar" style="margin-top:1px;">
        <div style="width:5px;">&nbsp;</div>
        
        <div style="width:120px;">Created</div>
        <div style="width:120px;">Last Used</div>
        <div style="width:80px;">Students</div>
        <div style="width:auto;">Name</div>

		<div class="sort_wrapper">
			<div style="vertical-align:middle; display:inline-block; line-height:normal;">
				<div class="header_sort" data-direction="asc" style="display:block;">▲</div>
				<div class="header_sort" data-direction="desc" style="display:block;">▼</div>
			</div>
			<span style="white-space:nowrap;">Sort by</span>
			<select id="sort_type">
                <option value="created" data-default-direction="desc" selected>Created</option>
                <option value="last_used" data-default-direction="desc">Last Used</option>
				<option value="name" data-default-direction="asc">Name</option>
			</select>
		</div>

	</div>
	<div class="window_content">
		<table id="class_lists" class="index_table selection_table">
            <tbody>
                <tr class="disabled">
                    <td colspan="5">
                        <div style="padding-left:10px;"><div class="wait_icon"></div><i>Loading&hellip;</i></div>
                    </td>
                </tr>
            </tbody>            
        </table>
	</div>
</div>
	
<div class="hidden">

    <!-- Copy received dialog -->

    <div id="receiveCopy_dialog" class="dialogbox" title="Items Received">
		<div class="dialog_wrapper">
			<p id="receiveCopy_text" style="min-height:50px;"></p>
		</div>
	</div>
	
    <!-- Send copy dialog -->

	<div id="sendCopy_dialog" class="dialogbox" title="Send A Copy">
		<div class="dialog_wrapper">
			<p id="sendCopy_text" style="margin-bottom:20px;"></p>
			
			<form id="sendCopy_form" class="formular" method="post" accept-charset="utf-8">
				<input type="hidden" name="email_list_id" value="send_email_list"/>
				<div class="labeled-input">
					<div style="width:100%; margin-bottom:5px !important;">
						<div style="float:left;"><label style="display:block; text-align:left; margin:3px 0px 0px;">Send to:</label></div>
						<div style="float:right;"><button id="send_selection_type" class="btn-tiny" <?php if (empty($contacts)) echo 'disabled="disabled"' ?> style="font-size:13px;">Choose from list</button></div>
						<div style="clear:both;"></div>
					</div>
					<textarea tabindex="1" name="email_list" id="send_email_list" class="validate[required] defaultTextBox2" style="resize:none; height:100px; width:340px;" type="text"></textarea>
					<div id="send_email_checklist" style="resize:none; height:100px; width:340px; padding:4px 6px; border:1px solid #ccc; border-radius: 4px; background:white; display:none; overflow-y:scroll; overflow-x:hidden;">
						<?php
							if (!empty($contacts)) {
								foreach ($contacts as $contact) {
									echo "<div><input class='send_email_checkbox' type='checkbox' id='send_email_checkbox_" . $contact['id'] . "' data-email='" . $contact['email'] . "' /><label for='send_email_checkbox_" . $contact['id'] . "' style='display:inline-block; width:auto;'>" . $contact['name'] . " (" . $contact['email'] . ")</label></div>";
								}
							}
						?>
					</div>
				</div>
			</form>
		</div>
	</div>
	
    <!-- Create class list dialog -->

	<div id="createList_dialog" class="dialogbox" title="Create class list">
        <div class="dialog_wrapper" style="padding:0px; height:400px; display:flex; flex-direction:column; gap:10px;">    
            <div style="flex:1 1 100%; width:100%; white-space:nowrap; overflow:hidden;">
                <div class="step_wrapper" style="display:inline-flex; flex-direction:column;">
                    <h3 style="flex:none; height:auto;">Upload your class list</h3>

                    <div style="margin-bottom:10px; flex:none; height:auto; font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:14px;">
                        <p>Import a text file containing your class list, drag and drop a file below, or paste it into the text input. 
                        Class lists should include one student per line. In addition:</p>
                        <ul>
                            <li>Text in parentheses will be ignored when finding student data;</li>
                            <li>Student email addresses, names, and IDs will be identified in what remains if present;</li>
                            <li>Full lines will be retained for form labels if they contain more than the student data.
                        </ul>
                    </div>
                    
                    <div id="fileInterface" style="flex:1 1 100%; position:relative;">
                        <div id="inputs" style="position:absolute; height:100%; width:100%; opacity:1.0; display:flex; flex-direction:column; gap:10px;">
                            <div style="flex:none; height:auto;">
                                <span id="select_button" class="btn btn-primary fileinput-button" style="height:20px;">
                                    Import file
                                    <input id="fileupload" type="file" accept=".csv, text/plain">
                                </span>
                            </div>

                            <div style="flex:1 1 100%; width:100%; display:flex; flex-direction:column;">
                                <label for="list_text" style="flex:none; height:auto; text-align:left;">Class list:</label>
                                <textarea id="list_text" class="defaultTextBox2" style="flex:1 1 100%; width:100%; box-sizing:border-box; margin:0px; resize:none;"></textarea>
                            </div>
                        </div>
                        <div id="fileDropTarget" style="position:absolute; height:100%; width:100%; border-radius:15px; background:white; 
                            border-style:solid; border-width:2px; border-color:black; text-align:center; display:none;">
                            <h2>Drop your file here</h2>
                        </div>
                    </div>
                </div><div class="step_wrapper" style="display:inline-flex; flex-direction:column;">
                    <form class="formular" style="height:100%;" accept-charset="utf-8">
                        <div id="create_list_header" style="height:100%; display:flex; flex-direction:column;">

                            <h3 style="flex:none; height:auto;">Name and edit your list</h3>

                            <div id="create_list_edit" class="list_edit_wrapper" style="height:100%; display:flex; flex-direction:column; gap:10px;">
                                <div style="flex:none; height:auto; width:100%; display:flex; flex-direction:row; align-items:center;">
                                    <label style="flex:none; width:auto; margin:0px;">List name:</label>
                                    <input class="list_name validate[required] defaultTextBox2" style="flex:1 1 100%; margin:0px;" type="text" />
                                    <div style="flex:none; width:auto; margin-left:10px;" class="btn-group">
                                        <button class="btn btn_font dropdown-toggle" data-toggle="dropdown" href="#">
                                            Columns 
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu columns-menu text-left"></ul>
                                    </div>
                                </div>

                                <div class="list_warnings_wrapper" style="flex:none; height:auto; width:100%; margin-bottom:20px; display:flex; flex-direction:row; gap:20px;">
                                    <div style="flex:none; width:auto; padding-right:0px; align-self:flex-start;">
                                        <img style="height:50px; margin-top:5px;" src="/img/warning.svg" />
                                    </div>
                                    <div style="flex:1 1 100%;">
                                        <p>Expected values not found. Click on a cell below to make changes.</p>
                                        <ul class="list_warnings"></ul>
                                    </div>
                                </div>

                                <div class="list_grid_wrapper" style="flex:1 1 100%; border:1px solid #cccccc; border-radius:4px; background:white; overflow-y:auto;"></div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>

            <div class="ui-dialog-buttonset custom-buttonpane" style="flex:none; height:auto; align-self:flex-end; margin:5px 10px 10px;">
                <button id="list_cancel" type="button" class="btn btn-cancel btn_font">Cancel</button>
                <button type="button" id="list_step_next" class="btn btn_font" style="width:55px;">Next</button>
            </div>
        </div>

	</div>

    <!-- Delete class list dialog -->

    <div id="deleteList_dialog" class="dialogbox" title="Delete class list">
		<div class="dialog_wrapper" style="min-height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
			<div id="deleteList_text"></div>
		</div>
	</div>

    <!-- Edit class list dialog -->

	<div id="editList_dialog" class="dialogbox" title="Edit class list">
        <div class="dialog_wrapper" style="height:400px;">
            <form class="formular" style="height:100%;" accept-charset="utf-8">
                <div id="edit_list_edit" class="list_edit_wrapper" style="height:100%; display:flex; flex-direction:column; gap:10px;">
                    <div style="flex:none; height:auto; width:100%; display:flex; flex-direction:row; align-items:center;">
                        <label style="flex:none; width:auto; margin:0px;">List name:</label>
                        <input class="list_name validate[required] defaultTextBox2" style="flex:1 1 100%; margin:0px;" type="text" />
                        <div style="flex:none; width:auto; margin-left:10px;" class="btn-group">
                            <button class="btn btn_font dropdown-toggle" data-toggle="dropdown" href="#">
                                Columns 
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu columns-menu text-left"></ul>
                        </div>
                    </div>

                    <div class="list_warnings_wrapper" style="flex:none; height:auto; width:100%; margin-bottom:20px; display:flex; flex-direction:row; gap:20px;">
                        <div style="flex:none; width:auto; padding-right:0px; align-self:flex-start;">
                            <img style="height:50px; margin-top:5px;" src="/img/warning.svg" />
                        </div>
                        <div style="flex:1 1 100%;">
                            <p>Expected values not found. Click on a cell below to make changes.</p>
                            <ul class="list_warnings"></ul>
                        </div>
                    </div>

                    <div class="list_grid_wrapper" style="flex:1 1 100%; border:1px solid #cccccc; border-radius:4px; background:white; overflow-y:auto;"></div>
                </div>
            </form>
        </div>
	</div>

</div>
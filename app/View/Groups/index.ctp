<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var userDefaults = <?php echo $user['defaults']; ?>;
</script>

<script type="module" src="/js/groups/index.js?7.0"></script>

<div class="hidden">
	<input type="hidden" id="refreshed" value="no">
</div>

<!-- Main area -->

<h2 style='margin-bottom:10px;'>
	Billing Groups
</h2>

<div style="vertical-align:top; display:inline-block; width:890px;">
	<div class="action_bar">
		<div class="action_left">
			<div class="checkbox_arrow"></div>			
			<div class="btn-group">
				<button id="editGroup_button" class="btn btn-dark item_action_one" data-toggle="dropdown" href="#">
					Edit 
				</button>
			</div>
			<div class="btn-group">
				<button id="deleteGroup_button" class="btn btn-dark item_action_one" data-toggle="dropdown" href="#">
					Delete 
				</button>
			</div>
		</div>
		<div class="action_right">
			<div class="btn-group">
				<button class="btn btn-dark search_dropdown dropdown-toggle" href="#">
					Search 
					<span class="caret"></span>
				</button>
				<div class="pull-right search_menu">
					<input id="search_input" style="box-sizing:border-box; width:212px; height:30px; margin:0px;" type="text" />
					<div class="search_icon"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="window_bar" style="margin-top:1px;">
        <div style="width:5px">&nbsp;</div>
		<div style="width:35px;">ID</div>
		<div style="width:120px;">Created</div>
		<div style="width:120px;">Expires</div>
		<div style="width:60px;">Members</div>
		<div style="width:auto;">Name</div>
	</div>
	<div id="index_right" class="index_body" style="overflow:hidden; width:100%;">
		<div class='index_element'>
			<table class="index_table selection_table">
				<tbody>
				<?php
				if (sizeof($groups) == 0) {
				?>
					<tr class="disabled"><td colspan="5"><div style="padding-left:10px;">
					<i>No groups to show here.</i>
					</div></td></tr>
				<?php
				} else {
			
					foreach ($groups as $group) {
					?>
					<tr data-group-id='<?php echo $group['Group']['id']; ?>'>
						<td style="width:15px; text-align:center;"><input type="checkbox" class="item_select" autocomplete="off" /></td>
						<td style="width:35px;"><?php echo $group['Group']['id'];?></td>
						<td style="width:120px;"><?php 
							echo $this->Time->format('Y-m-d H:i', $group['Group']['created'], null, 
													new DateTimeZone(AuthComponent::user('timezone'))); 	        
						?></td>
						<td style="width:120px;"><?php
							echo $this->Time->format('F j, Y', $group['Group']['expires'], null, 
													new DateTimeZone(AuthComponent::user('timezone'))); 	        
						?></td>
						<td style="width:60px;"><?php echo $group['Group']['member_count'];?></td>
						<td>
							<span class="td_itemName" style="display:inline-block"><?php 
								echo htmlspecialchars($group['Group']['name']);
							?></span>
						</td>
					</tr>
				<?php
					}
				}
				?>
				</tbody>
			</table>
			<div style="display:flex; justify-content:space-between; height:auto; margin:20px 10px;">
				<div style="color:#75797c;">
					<span id="search_count" style="display:none;">Searching</span>
					<?php echo number_format($numItems); ?> item<?php echo ($numItems == 1) ? '' : 's'; ?>
				</div>
				<div class="pagination_wrapper">
					<?php
					echo $this->Paginator->prev('<', array('class' => 'previous pag_button'), null, array('class' => 'previous disabled pag_button'));
					echo $this->Paginator->numbers(array('class' => 'pag_button', 'separator' => ''));
					echo $this->Paginator->next('>', array('class' => 'next pag_button'), null, array('class' => 'next disabled pag_button'));
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="hidden">

<!-- Delete group dialog -->

	<div id="deleteGroup_dialog" class="dialogbox" title="Delete Group">
		<div class="dialog_wrapper" style="min-height:70px;">
			<p>Delete the group "<span id='deleteGroup_name'></span>"? This will not change user renewals, but 
			you will no longer be able to view this group.</p>
		</div>
	</div>

</div>

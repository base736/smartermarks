<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var userID = <?php echo $user['id']; ?>;
	var userRole = "<?php echo $user['role']; ?>";

	var groupID = <?php echo $group['Group']['id']; ?>;
	var groupName = <?php echo json_encode($group['Group']['name']); ?>;

    var expires = dayjs.utc("<?php echo $group['Group']['expires']; ?>").local();
    var now = dayjs().local();
</script>

<script type="module" src="/js/groups/view.js?7.0"></script>

<h2 style='margin-bottom:20px;'>
	Billing group
</h2>

<div class="rounded_white_wrapper" style="width:100%; margin:0px;">
	<div style="padding:10px;">
		<div class="labeled-input" style="width:100%;">
			<label style="width:70px; text-align:left; display:inline-block;">Name:</label>
			<div style="display:inline-block;"><?php echo $group['Group']['name']; ?></div>
		</div>

		<div class="labeled-input" style="width:auto;<?php if ($user['role'] != 'admin') echo ' display:hidden;'; ?>">
				<label for="billing_type" style="width:70px; text-align:left;">Billing type:</label>
				<select class="defaultSelectBox" style="width:150px;" id="billing_type" disabled>
					<option value="renewal-individual-1yr" <?php echo ($group['Group']['renewal_type'] == 'renewal-individual-1yr') ? 'selected' : ''; ?>>Individual</option>
					<option value="renewal-group-1yr" <?php echo ($group['Group']['renewal_type'] == 'renewal-group-1yr') ? 'selected' : ''; ?>>Group</option>
					<option value="renewal-school-1yr" <?php echo ($group['Group']['renewal_type'] == 'renewal-school-1yr') ? 'selected' : ''; ?>>School</option>
				</select>
			</div>

		<div class="labeled-input" style="width:100%;">
			<label style="width:70px; text-align:left; display:inline-block;">Expires:</label>
			<div id="expires_text" style="display:inline-block;"></div>
		</div>

        <div id="email_bounced_notice" style="border:1px solid #ccc; background: #f7baba; margin-top:20px; padding:10px; display:none;">
            <div style="margin-bottom:10px;"><b>Invalid email addresses found</b></div>
            <div>Our verification email has bounced for one or more teachers in this group, indicating that the email address given is
            not valid. These accounts are highlighted in red below. Click the email icon 
            (<span class="email_icon text_icon"></span>) next to a highlighted account to try sending the verification email again
            (for example, if the email address was created only recently) or the edit icon (<span class="edit_icon text_icon"></span>)
            to change the email address for the account.</div>
        </div>

		<table class="panel_table actions_table" style="border:1px solid #d7d7d7; color:#343434; margin-top:15px;">
			<thead>
				<tr>
					<td style="width:30%;">Name</td>
					<td>Email</td>
					<td style="width:10%;">Actions</td>
				</tr>
			</thead>
			<tbody id="userData"></tbody>
		</table>

		<?php
		if (!empty($group['Group']['renewal_type']) && !$renew_by_email) {
		?>

		<div style="margin-top:10px; width:100%; display:flex; flex-direction:row; justify-content:space-between; align-items:center;">
            <div>
                <?php 
                $prepaid_count = $group['Group']['prepaid_count'];
                if ($prepaid_count > 0) {
                    echo $prepaid_count . " prepaid renewal" . (($prepaid_count == 1) ? '' : 's') . " available.";
                }
                ?>
            </div>
            <button id="addUsers_button" class="btn btn-primary" type="button">Add Users</button>
		</div>

		<?php
		}
		?>
	</div>
</div>

<div class="hidden">

<!-- Remove member dialog -->

	<div id="removeMember_dialog" class="dialogbox" title="Remove member">
		<div class="dialog_wrapper">
			<p>Remove <span id="remove_user_email"></span> from this group?</p>
			<p><b>Note:</b> Renewals are not refunded when a member is removed from a group.</p>
		</div>
	</div>

<!-- Change admin dialog -->

	<div id="changeAdmin_dialog" class="dialogbox" title="Remove administrator">
		<div id="changeAdmin_wrapper" class="dialog_wrapper" style="min-height:60px;">
			<p>Remove yourself as an administrator for this group?</p>
			<p><b>Note:</b> You will not be able to make further changes to the group once this is done.</p>
		</div>
	</div>

<!-- Edit email dialog -->

	<div id="editEmail_dialog" class="dialogbox" title="Update teacher email">
		<div class="dialog_wrapper">
			<form class="formular" accept-charset="utf-8">
				<p style="min-height:50px;">
					Enter a new email address for the teacher below, then click "Save" to save the changes.
					</p>
				<div class="labeled-input" style="width:100%;">
					<label for="new_email" style="display:block; text-align:left; margin:5px 0px 5px !important; with:auto;">Email address:</label>
					<input class="validate[required, custom[email]] defaultTextBox2" maxlength="100" type="text" id="new_email" style="width:340px;" />
				</div>
			</form>
		</div>
	</div>

<!-- Send verification dialog -->

	<div id="sendVerification_dialog" class="dialogbox" title="Re-send email verification">
		<div class="dialog_wrapper">
			<p style="min-height:50px;">
				Click "Send" to send another email verification link to "<span id="verification_email"></span>" now.
			</p>
		</div>
	</div>

</div>

<?php echo $this->element("renew_common"); ?>

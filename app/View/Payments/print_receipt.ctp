<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
echo $this->element('layout_head', array(
    'include_print_element' => true
));
?>

<script type="text/javascript">

var isPrint = true;
var paymentID = <?php echo json_encode($payment_id); ?>;
var details = <?php echo json_encode($details); ?>;
var createdUTC = <?php echo json_encode($created); ?>;
var email = <?php echo json_encode($email); ?>;
var paymentStatus = <?php echo json_encode($status); ?>;

<?php $secrets = Configure::read('secrets'); ?>
var receiptHeader = <?php echo json_encode($secrets['billing']['receipt_header']); ?>;
var receiptFooter = <?php echo json_encode($secrets['billing']['receipt_footer']); ?>;

</script>

<script type="module" src="/js/payments/view_receipt.js?7.0"></script>

<style>
	body, html {
		-webkit-print-color-adjust: exact;
		background-color: transparent; 
		margin: 0px;
	}

	#receipt_content * {
		font-size: 10pt;
        line-height: 1.2;
	}

	table {
		border-collapse: collapse;
		border-spacing: 0;
	}

</style>

</head>

<body style="background-color:none;">
    <div id="receipt_content"></div>
</body>


<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<h2>Payment Error</h2>

<div class="rounded_white_wrapper" style="width:100%; margin-bottom:20px;">
	<div class="rounded_white_wrapper_inner" style="display:flex; box-sizing:border-box; width:100%; padding:20px;">
		<div style="flex:none; width:auto;">
			<img src="/img/error.svg" style="margin-right:20px; height:50px;"/>
		</div>
		<div style="flex:1 1 100%;">
			<p>We're sorry &mdash; it looks like a problem occurred while processing your payment. 
			<?php if (!empty($error_message)) echo $error_message; ?></p>
			
			<p>Please contact us at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>
			to resolve this issue.</p>
		</div>
	</div>
</div>


<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<h2>Processing payment</h2>

<div class="rounded_white_wrapper" style="width:100%; margin-bottom:20px;">
	<div class="rounded_white_wrapper_inner" style="box-sizing:border-box; width:100%; padding:20px;">
		<p style="margin:0px;">Thank you for continuing with us! Your payment is being processed now. When it has been 
		successfully completed, a receipt will be sent to <?php echo $email; ?> and your 
		account<?php echo $count > 1 ? 's' : ''; ?> renewed.</p>
	</div>
</div>


<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">

var isPrint = false;
var paymentID = <?php echo json_encode($payment_id); ?>;
var details = <?php echo json_encode($details); ?>;
var createdUTC = <?php echo json_encode($created); ?>;
var email = <?php echo json_encode($email); ?>;
var paymentStatus = <?php echo json_encode($status); ?>;

<?php $secrets = Configure::read('secrets'); ?>
var receiptHeader = <?php echo json_encode($secrets['billing']['receipt_header']); ?>;
var receiptFooter = <?php echo json_encode($secrets['billing']['receipt_footer']); ?>;

</script>

<script type="module" src="/js/payments/view_receipt.js?7.0"></script>

<?php
if ($user['role'] != 'admin') {
?>

<div class="rounded_white_wrapper" style="width:100%; margin-bottom:20px; background-color:white;">
	<div class="rounded_white_wrapper_inner" style="box-sizing:border-box; width:100%; padding:20px;">
	<p>Done!</p>
	<p>Your receipt is shown below; a copy has been sent to <?php echo $email; ?> as well. Click on "Print" above if 
	you would like to print a copy of this receipt now for your records.</p>
	</div>
</div>

<?php
}
?>

<div class="rounded_white_wrapper" style="width:100%; margin-bottom:20px; background-color:white;">
	<div class="rounded_white_wrapper_inner" style="box-sizing:border-box; width:100%; padding:20px;">
		<div id="receipt_content"></div>
	</div>
</div>


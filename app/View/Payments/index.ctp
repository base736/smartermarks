<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="module">

import * as TableSelection from '/js/common/table_selection.js?7.0';

$(document).ready(function() {

    TableSelection.initialize();

	$(document).on('modifyItemActions', function(event) {
		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				if ($(this).closest('tr').data('score-id') == -1) $('#viewReceipt_button').attr('disabled', 'true');
			}
		});
	});

	$('#viewReceipt_button').click(function(e) {
		e.preventDefault();
		if ($(this).attr('disabled')) return;

		$('.item_select').each(function() {
			if ($(this).prop('checked')) {
				var $tr = $(this).closest('tr');
				var $paymentID = $tr.data('payment-id');
				window.location = "/payments/view_receipt/" + $paymentID;
			}
		});
	});
});

</script>

<!-- Main area -->

	<h2 style='margin-bottom:10px;'>Payments and Invoices</h2>

	<div style="width:890px; margin:auto;">
		<div class="action_bar">
			<div class="action_left">
				<div class="checkbox_arrow"></div>
				<div class="btn-group">
					<button id="viewReceipt_button" type="button" class="btn btn-dark needs_score item_action_one"><span>View item</span></button>
				</div>
			</div>
		</div>
		<div class="window_bar" style="margin-top:1px; position:relative;">
            <div style="width:5px;">&nbsp;</div>
            <div style="width:50px;">ID</div>
            <div style="width:275px;">User</div>
            <div style="width:120px;">Created</div>
            <div style="width:250px;">Stripe ID</div>
            <div style="width:auto; padding-right:0px; float:right;">Amount</div>
		</div>
		<div class="window_content" style="height:auto;">
			<div style="position:relative; overflow:hidden; height:auto;">
			    <table class="index_table selection_table">
			    	<tbody>
			        <?php
			        if (sizeof($payments) == 0) {
					?>
						<tr class="disabled"><td colspan="6"><div style="padding-left:10px;">
						<i>No payments to show here.</i>
						</div></td></tr>
					<?php
					} else {
				        foreach ($payments as $payment) {
					?>
						<tr data-payment-id="<?php echo $payment['Payment']['id'];?>">
							
							<td style="text-align:center; width:15px;">
								<input type="checkbox" class="item_select" autocomplete="off" />
							</td>
			        	
				            <td style="width:50px;"><?php echo $payment['Payment']['id'];?></td>
				            <td style="width:275px;"><?php echo htmlspecialchars($payment['User']['email']);?></td>
							<td style="width:120px;"><?php 
				        		echo $this->Time->format('Y-m-d H:i', $payment['Payment']['created'], null, 
				        	       	                     new DateTimeZone(AuthComponent::user('timezone'))); 	        
							?></td>
				           	<td style="width:250px;"><?php 
								if ($payment['Payment']['status'] == 'complete') {
									$stripeID = $payment['Payment']['stripe_id'];
                                    if ($stripeID !== null) {
                                        echo '<span style="font-family:\'Lucida Console\', Monaco, monospace;">' . $stripeID . '</span>';
                                    } else {
                                        echo '<i>Prepaid</i>';
                                    }
								 } else echo '<i>Invoice</i>';
							?></td>
							<td style="text-align:right;"><?php
								$details = json_decode($payment['Payment']['details'], true);
								$total = $details['invoice']['total'] / 100;
								echo '$' . number_format($total, 2);
							?></td>
				        </tr>
				    <?php
						}
				    }
				    ?>
				    </tbody>
			    </table>
			    <div style="display:flex; justify-content:flex-end; height:auto; margin:20px 10px;">
				    <div class="pagination_wrapper">
						<?php 
					    	echo $this->Paginator->prev('<', array('class' => 'previous pag_button'), null, array('class' => 'previous disabled pag_button'));
							echo $this->Paginator->numbers(array('class' => 'pag_button', 'separator' => ''));
							echo $this->Paginator->next('>', array('class' => 'next pag_button'), null, array('class' => 'next disabled pag_button'));
				    	?>
				    </div>
				</div>
			</div>
		</div>
	</div>

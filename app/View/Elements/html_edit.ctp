<!--
SmarterMarks HTML editor: WYSIWIG editor for SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
    
	var userdataPrefix = "<?php
		$secrets = Configure::read('secrets'); 
		echo $secrets['s3']['userdata_prefix'];
	?>";

</script>

<!-- Dialog boxes for html_edit.js -->

<div class="hidden">

<!-- {* add drawing dialog *} -->

	<div id="editDrawing_dialog" class="dialogbox"  title="Edit Drawing">
			<div class="dialog_wrapper">
				
			<p>Choose a width for your drawing as a percentage of the parent width, then click "Create".</p>

			<form action="#" class="formular" accept-charset="utf-8">
				<div class="labeled-input" style="width:100%; margin-top:20px">
					<label for="drawing_width" class="" style="width:40px; display:inline-block; text-align:left;">Width:</label>
					<input class="validate[required,custom[isPosDecimal],min[0],max[100]] defaultTextBox2" type="text" id="drawing_width" style="width:50px;"/><label style="margin-left:5px;">%</label>
				</div>
			</form>
		</div>
	</div>

<!-- {* colour picker dialog *} -->

	<div id="colourPicker_dialog" class="dialogbox"  title="Pen Details">
		<div class="dialog_wrapper picker-wrapper">
			<div class="picker-colour-area">
				<div class="picker-colour-area-indicator"></div>
			</div>
			<div class="picker-controls-row">
				<div class="picker-preview-circle"></div>
				<div class="picker-slider-wrapper">
					<div class="picker-slider picker-hue-slider">
						<div class="picker-slider-indicator"></div>
					</div>
					<div class="picker-slider picker-width-slider">
						<div class="picker-slider-indicator"></div>
					</div>
				</div>
			</div>
			<div class="picker-swatch-wrapper"></div>
		</div>
	</div>

<!-- {* add variable dialog *} -->

	<div id="editVariable_dialog" class="dialogbox"  title="Edit Variable">
		<div class="dialog_wrapper">
		<form action="#" class="formular" accept-charset="utf-8">
			<input id="variable_variable_name" class="variable_name" type="hidden" />
			<input id="variable_old_display_name" type="hidden" />
		
			<div class="labeled-input" style="width:100%">
				<label for="variable_display_name" class="" style="display:inline-block; text-align:left; width:70px;">Name:</label><input class="validate[required,funcCall[checkVariableName]] defaultTextBox2" type="text" id="variable_display_name" style="width:275px;"/>
			</div>
			<div id="variable_options">
				<div class="labeled-input">
					<label for="variable_type" style="display:inline-block; text-align:left; width:70px;">Type:</label><select class="defaultSelectBox pad5px" style="height: 28px; width: 110px; margin-right:10px; display:inline-block;" id="variable_type">
						<option value='constants'>Constant</option>
						<option value='randomized'>Random</option>
						<option value='calculated'>Calculated</option>
					</select><select class="defaultSelectBox pad5px" style="height:28px; width:110px; display:inline-block;" id="variable_subtype">
					</select>
				</div>
				<div class="variable_details variable_constants">
					<div class="labeled-input">
						<label for="var_constant_value" style="width:70px; display:inline-block; text-align:left;">Value:</label><input class="validate[required,custom[isNumber]] defaultTextBox2" type="text" id="var_constant_value" style="width:270px"/>
					</div>
				</div>
				<div class="variable_details variable_randomized">
					<div class="labeled-input">
						<label for="var_randomized_min" style="width:70px; display:inline-block; text-align:left;">Minimum:</label><input class="validate[required,custom[isNumber]] defaultTextBox2" type="text" id="var_randomized_min" style="width:75px"/>
					</div>
					<div class="labeled-input">
						<label for="var_randomized_max" style="width:70px; display:inline-block; text-align:left;">Maximum:</label><input class="validate[required,custom[isNumber]] defaultTextBox2" type="text" id="var_randomized_max" style="width:75px"/>
					</div>
					<div class="labeled-input">
						<label for="var_randomized_step" style="width:70px; display:inline-block; text-align:left;">Step:</label><input class="validate[required,custom[isNumber]] defaultTextBox2" type="text" id="var_randomized_step" style="width:75px"/>
					</div>
				</div>
				<div class="variable_details variable_from_list">
					<div class="labeled-input">
						<label for="var_randomized_list" style="width:70px; display:inline-block; text-align:left;">List:</label><input class="defaultTextBox2" type="text" id="var_randomized_list" style="width:275px"/>
					</div>
				</div>
				<div class="variable_details variable_calculated">
					<div class="labeled-input">
						<label for="calculated_equation" style="width:70px; display:inline-block; text-align:right; vertical-align:top; position:relative; top:40px;">=</label><div id="calculated_equation" class="variable_equation" style="width:285px;"></div>
					</div>
				</div>
				<div class="variable_details variable_conditional">
					<div class="labeled-input" style="margin-left:75px;">
						<label for="conditional_if" style="width:auto; display:block; text-align:left;">If&hellip;</label>
						<div id="conditional_if" class="variable_equation" style="width:285px;"></div>
					</div>
					<div class="labeled-input" style="margin-left:75px;">
						<label for="conditional_then" style="width:auto; display:block; text-align:left;">Then&hellip;</label>
						<div id="conditional_then" class="variable_equation" style="width:285px;"></div>
					</div>
					<div class="labeled-input" style="margin-left:75px;">
						<label for="conditional_else" style="width:auto; display:block; text-align:left;">Otherwise&hellip;</label>
						<div id="conditional_else" class="variable_equation" style="width:285px;"></div>
					</div>
				</div>
				<div class="variable_details variable_randomized variable_calculated variable_conditional">
					<div class="labeled-input">
						<label style="width:70px; display:inline-block; text-align:left; position:relative; top:4px;">Format:</label><div style="vertical-align:top;">
							<div>
								<select class="defaultSelectBox pad5px" style="height:30px !important; width:140px; margin:0px;" id="var_sdType">
                                    <option value='SigDigs'>Scientific / decimal</option>
                                    <option value='Scientific'>Scientific only</option>
									<option value='Decimals'>Decimal only</option>
									<option value='Fraction'>Fraction</option>
								</select>
								<div id="var_sd_wrapper" style="display:inline-block;">
									<label style="display:inline-block; width:auto; margin:0px; padding:0px;">to</label>
									<input id="var_sd" class="defaultTextBox2 intSpinner" type="text" />
									<label id="var_sdLabel" style="display:inline-block; width:auto; margin:0px; padding:0px;"></label>
								</div>
							</div>
							<div class="variable_details variable_calculated variable_conditional" id="var_round_wrapper">
								<input type="checkbox" class="normal_chkbx" id="var_round" value="1" /><label for="var_round"  style="width:auto; display:inline-block; text-align:left">Round immediately</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
	</div>

<!-- {* add equation dialog *} -->

	<div id="editEquation_dialog" class="dialogbox"  title="Edit Equation">
		<div class="dialog_wrapper">
		<form action="#" class="formular" accept-charset="utf-8">
			<div class="labeled-input" style="width:100%">
				<label for="equation_text" style="display:block; text-align:left; margin-bottom:5px !important;">Equation:</label>
				<div id="equation_text" style="width:400px; height:auto; margin:5px; padding:0px; overflow-x:hidden; background:white;"></div>
			</div>
		</form>
		</div>
	</div>

<!-- {* table settings dialog *} -->

	<div id="editTable_dialog" class="dialogbox"  title="Edit table">
		<div class="dialog_wrapper">
			<form action="#" class="formular" accept-charset="utf-8">
				<div style="font-style:bold; margin:5px 0px 10px;">Table settings</div>
				<div style="width:50%; display:inline-block; vertical-align:top;">
					<div id="settings_table_size" style="margin-left:10px;">
						<div class="labeled-input" style="width:103px; display:inline-block;">
							<label for="settings_table_rows" style="display:inline-block; text-align:left;">Rows:</label><input class="validate[required,custom[isPosInteger]] defaultTextBox2" type="text" id="settings_table_rows" style="width:20px;"/>
						</div>
						<div class="labeled-input" style="width:auto; display:inline-block;">
							<label for="settings_table_columns" style="display:inline-block; text-align:left;">Columns:</label><input class="validate[required,custom[isPosInteger]] defaultTextBox2" type="text" id="settings_table_columns" style="width:20px;"/>
						</div>
					</div>
					<div id="settings_table_alignment" style="margin-left:10px;">
						<div class="labeled-input" style="width:auto;">
							<label for="vertical_alignment" style="display:inline-block; text-align:left;">Vertical alignment:</label>
							<select class="defaultSelectBox pad5px" style="width:100px;" id="vertical_alignment">
								<option value='top'>Top</option>
								<option value='middle'>Middle</option>
								<option value='bottom'>Bottom</option>
							</select>
						</div>
					</div>
					<div id="settings_table_merge" style="margin-left:10px;">
						<div class="labeled-input" style="width:auto; vertical-align:middle;">
							<input type="checkbox" class="normal_chkbx" id="merge_cells_checkbox" />
							<label id="merge_cells_checkbox_label" for="merge_cells_checkbox" style="display:inline-block; text-align:left;">Merge selected cells</label>
						</div>
					</div>
				</div><div style="display:inline-block; margin-left:30px; vertical-align:top;">
					<div id="settings_table_border">
						<div style="vertical-align:middle; display:block;">
							<input type="checkbox" class="borders_checkbox borders_smart normal_chkbx" style="margin:0px;" id="borders_all" value="1" /><label for="borders_all" style="display:inline-block;"><img src='/img/editor/borders_all.svg' style="width:16px; margin:0px 10px;"/>All borders</label>
						</div>
						<div style="vertical-align:middle; display:block;">
							<input type="checkbox" class="borders_checkbox borders_smart normal_chkbx" style="margin:0px;" id="borders_none" value="1" /><label for="borders_none" style="display:inline-block;"><img src='/img/editor/borders_none.svg' style="width:16px; margin:0px 10px;"/>No borders</label>
						</div>
						<div style="vertical-align:middle; display:block;">
							<input type="checkbox" class="borders_checkbox borders_smart normal_chkbx" style="margin:0px;" id="borders_inside" value="1" /><label for="borders_inside" style="display:inline-block;"><img src='/img/editor/borders_inside.svg' style="width:16px; margin:0px 10px;"/>Inside borders</label>
						</div>
						<div style="vertical-align:middle; display:block;">
							<input type="checkbox" class="borders_checkbox borders_smart normal_chkbx" style="margin:0px;" id="borders_outside" value="1" /><label for="borders_outside" style="display:inline-block;"><img src='/img/editor/borders_outside.svg' style="width:16px; margin:0px 10px;"/>Outside borders</label>
						</div>
						<div style="vertical-align:middle; display:block;">
							<input type="checkbox" class="borders_checkbox borders_single normal_chkbx" style="margin:0px;" id="borders_top" value="1" /><label for="borders_top" style="display:inline-block;"><img src='/img/editor/borders_top.svg' style="width:16px; margin:0px 10px;"/>Top border</label>
						</div>
						<div style="vertical-align:middle; display:block;">
							<input type="checkbox" class="borders_checkbox borders_single normal_chkbx" style="margin:0px;" id="borders_bottom" value="1" /><label for="borders_bottom" style="display:inline-block;"><img src='/img/editor/borders_bottom.svg' style="width:16px; margin:0px 10px;"/>Bottom border</label>
						</div>
						<div style="vertical-align:middle; display:block;">
							<input type="checkbox" class="borders_checkbox borders_single normal_chkbx" style="margin:0px;" id="borders_left" value="1" /><label for="borders_left" style="display:inline-block;"><img src='/img/editor/borders_left.svg' style="width:16px; margin:0px 10px;"/>Left border</label>
						</div>
						<div style="vertical-align:middle; display:block;">
							<input type="checkbox" class="borders_checkbox borders_single normal_chkbx" style="margin:0px;" id="borders_right" value="1" /><label for="borders_right" style="display:inline-block;"><img src='/img/editor/borders_right.svg' style="width:16px; margin:0px 10px;"/>Right border</label>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- {* image settings dialog *} -->

	<div id="editImage_dialog" class="dialogbox" title="Edit Image">
		<div class="dialog_wrapper">
			<form action="#" class="formular" accept-charset="utf-8">
			
			<div class="image_edit_container">
				<div style="margin:5px 0px 10px;">Image settings</div>
			</div>

			<div class="image_insert_container">
				<p>
				<span class="file_upload_wrapper">Select an image file to include for this question, choose...</span><br/>
				<span>a width for the image as a percentage of the parent width, then click "Upload".</span>
				</p>

				<div class="file_upload_wrapper" style="padding-bottom:10px;">
				<span id="select_button" class="btn btn-primary fileinput-button" style="height:20px;">
					Select file
					<input id="image_upload" type="file" name="files[]" accept="image/jpeg|image/png">
				</span>
				<span id="image_filename" style="padding-left:10px; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
					No file selected
				</span>
				</div>
			</div>

			<div class="labeled-input" style="width:100%">
			<label for="image_width" style="width:60px; display:inline-block; text-align:left;">Width:</label>
			<input class="validate[required,custom[isPosDecimal],min[0],max[100]] defaultTextBox2"
					type="text" id="image_width" style="width:50px;" />
			<label style="margin-left:5px;">%</label>
			</div>

			<div id="progresswrapper" class="image_insert_container">
				<label for="progressbar" style="width:60px; margin:0; display:inline-block; text-align:left; vertical-align:middle;">
					Progress:
				</label>
				<div id="progressbar" style="width:400px; display:inline-block; vertical-align:middle;"></div>
			</div>

			</form>
		</div>
	</div>

</div>

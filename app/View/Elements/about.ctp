<div style="height:410px;">
<p>SmarterMarks is an open-source web tool that helps teachers and students do more with online and paper 
assessments. SmarterMarks allows teachers to:</p>
<ul>
<li>Quickly create assessments including multiple choice, numeric response, and written response 
questions. Teachers can use their own questions, or share questions with colleagues using communities 
and an open question bank built from items contributed by other teachers, and backed by classroom 
statistics.</li>
<li>Administer assessments on paper or online without having to duplicate work. For paper assessments, 
teachers can build custom response forms that can be printed and accurately scanned using the equipment 
already in schools.</li>
<li>Easily interpret assessment results to highlight student understandings and pinpoint misconceptions, and to
improve assessment items from year to year.</li>
</ul>
<p>SmarterMarks helps students get more from assessments as well, allowing them to:</p>
<ul>
<li>Collect not just a single cumulative grade from each assessment, but also feedback for individual learning 
outcomes, helping them identify specific strengths and areas for growth.</li>
<li>Keep feedback from online assessments in one place for easy reference as they progress through a
course.</li>
</ul>
</div>

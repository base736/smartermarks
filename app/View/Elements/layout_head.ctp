<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<!-- viewport meta to reset iPhone initial scale -->
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0" /> -->

<title>SmarterMarks<?php if (!empty($title_for_layout)) echo ' :: ' . $title_for_layout; ?></title>

<!-- Set favicon -->
<link href="/favicon.ico" type="image/x-icon" rel="icon">

<!-- Include jQuery -->
<script src="/js/lib/jquery/jquery-1.10.2.min.js"></script>

<!--
****************************************************************************
* The below is not included for printed (non-interactive) pages            *
****************************************************************************
-->

<?php
if (empty($include_print_element)) {
?>

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Exo 2:500" />

<!-- Include jQuery UI -->
<link rel="stylesheet" type="text/css" href="/css/manage_documents/css.css" />
<link rel="stylesheet" type="text/css" href="/css/manage_documents/jquery-ui-1.10.4.custom.min.css?v1.1" />
<script src="/js/lib/jquery-ui/jquery-ui-1.10.4.custom.js"></script>

<!-- Include jQuery validation engine -->
<link rel="stylesheet" type="text/css" href="/css/validationEngine.jquery.css" />
<script src="/js/lib/validationEngine/jquery.validationEngine.min.js"></script>
<script src="/js/lib/validationEngine/jquery.validationEngine-en.js?7.0"></script>

<!-- Include Bootstrap -->
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css?v1.1" />
<script src="/js/lib/bootstrap/bootstrap.min.js"></script>

<!-- Include Gritter -->
<link rel="stylesheet" type="text/css" href="/css/gritter.css" />
<script src="/js/lib/gritter/jquery.gritter.min.js?7.0"></script>

<!-- Include common login functions -->
<script src="/js/common/login.js?7.0"></script>

<?php
}
?>

<!--
****************************************************************************
-->

<!-- Main stylesheet -- must be here to override above styles -->
<link rel="stylesheet" type="text/css" href="/css/style.css?7.0" />

<!-- Include Cookie -->
<script src="/js/lib/cookie/js.cookie.min.js"></script>

<!-- Include Spark MD5 -->
<script src="/js/lib/spark-md5/spark-md5.min.js"></script>

<!-- Include dayjs with UTC extension -->
<script src="/js/lib/dayjs/dayjs.min.js"></script>
<script src="/js/lib/dayjs/plugin/utc.min.js"></script>
<script>dayjs.extend(window.dayjs_plugin_utc)</script>
<script src="/js/lib/dayjs/plugin/timezone.min.js"></script>
<script>dayjs.extend(window.dayjs_plugin_timezone)</script>

<?php

// Only include the below scripts and styling if they're going to be used

if ((strtolower($this->params['controller']) == 'users' && $this->action == 'renew_main') ||
    (strtolower($this->params['controller']) == 'users' && $this->action == 'renew_group') ||
    (strtolower($this->params['controller']) == 'groups' && $this->action == 'view')) {
    ?><script src="https://js.stripe.com/v3/"></script><?php
}

if (!empty($include_build_version) ||
    (strtolower($this->params['controller']) == 'assessments' && $this->action == 'build') ||
    (strtolower($this->params['controller']) == 'sittings' && $this->action == 'view') ||
    (strtolower($this->params['controller']) == 'sittingstudents' && $this->action == 'view_closed') || 
    (strtolower($this->params['controller']) == 'questions' && $this->action == 'build') ||
    (strtolower($this->params['controller']) == 'documents' && $this->action == 'build') ||
    (strtolower($this->params['controller']) == 'questions' && $this->action == 'index') ||
    (strtolower($this->params['controller']) == 'scores' && $this->action == 'view_teacher') ||
    (strtolower($this->params['controller']) == 'scores' && $this->action == 'print_teacher')) {
    ?><script type="text/javascript" src="/js/lib/color/jquery.color-2.1.2.min.js"></script><?php
}

if ((strtolower($this->params['controller']) == 'documents' && $this->action == 'index') ||
    (strtolower($this->params['controller']) == 'assessments' && $this->action == 'index') ||
    (strtolower($this->params['controller']) == 'questions' && $this->action == 'index') || 
    (strtolower($this->params['controller']) == 'sittingstudents' && $this->action == 'index') || 
    (strtolower($this->params['controller']) == 'sittings' && $this->action == 'index') ||
    (strtolower($this->params['controller']) == 'questions' && $this->action == 'manage_open') ||
    (strtolower($this->params['controller']) == 'assessments' && $this->action == 'build') ||
    (strtolower($this->params['controller']) == 'questions' && $this->action == 'build')) {
    ?>
    <link rel="stylesheet" type="text/css" href="/css/themes/default/style.css" />
    <script type="text/javascript" src="/js/lib/jstree/jstree.min.js"></script>
    <?php
}

if ((strtolower($this->params['controller']) == 'scores' && $this->action == 'view_teacher') ||
    (strtolower($this->params['controller']) == 'scores' && $this->action == 'print_teacher')) {
    ?><script type="text/javascript" src="/js/lib/jstat/jstat.min.js"></script><?php
}

if (!empty($include_build_version) ||
    (strtolower($this->params['controller']) == 'questions' && $this->action == 'build') ||
    (strtolower($this->params['controller']) == 'assessments' && $this->action == 'build') ||
    (strtolower($this->params['controller']) == 'documents' && $this->action == 'build') ||
    (strtolower($this->params['controller']) == 'documents' && $this->action == 'print_key') ||
    (strtolower($this->params['controller']) == 'questions' && $this->action == 'preview') ||
    (strtolower($this->params['controller']) == 'sittings' && $this->action == 'view') ||
    (strtolower($this->params['controller']) == 'sittingstudents' && $this->action == 'view_open') ||
    (strtolower($this->params['controller']) == 'sittingstudents' && $this->action == 'view_closed') ||
    (strtolower($this->params['controller']) == 'questions' && $this->action == 'index') ||
    (strtolower($this->params['controller']) == 'scores' && $this->action == 'view_teacher') ||
    (strtolower($this->params['controller']) == 'scores' && $this->action == 'print_teacher')) {
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/EquationEditor/Fonts/TeX/font.css" />
    <link rel="stylesheet" type="text/css" href="/css/eqn_edit.css?7.0" />
    <link rel="stylesheet" type="text/css" href="/css/html_edit.css?7.0" />
    <link rel="stylesheet" type="text/css" href="/css/colour_picker.css?7.0" />
    <?php
}

if (!empty($include_build_version) ||
    (strtolower($this->params['controller']) == 'graphs' && $this->action == 'index') ||
    (strtolower($this->params['controller']) == 'scores' && $this->action == 'view_teacher') ||
    (strtolower($this->params['controller']) == 'scores' && $this->action == 'print_teacher') ||
    (strtolower($this->params['controller']) == 'assessments' && $this->action == 'build') ||
    (strtolower($this->params['controller']) == 'sittings' && $this->action == 'view') ||
    (strtolower($this->params['controller']) == 'documents' && $this->action == 'build')) {
    ?>
    <script type="text/javascript" src="/js/lib/flot/jquery.flot.min.js"></script>
    <script type="text/javascript" src="/js/lib/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="/js/lib/flot/jquery.flot.selection.min.js"></script>
    <?php
}

echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
?>
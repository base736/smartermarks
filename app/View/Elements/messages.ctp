<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<?php
if (empty($user['hidden_tips'])) $hiddenTips = array();
else $hiddenTips = explode(";", $user['hidden_tips']); 

if (!in_array("feb_6_2025_message", $hiddenTips)) {
    ?>
    
    <div id="feb_6_2025_message_wrapper" class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
        <div class="rounded_white_wrapper_inner">
    
            <h3>Convention talks this year!</h3>
    
            <p>Sorry to stack two messages here. As a reminder, you can click "Hide this tip" below to hide either.</p>
    
            <p>In place of our usual exhibit hall appearances, this year I'll be hosting a series of sessions in
            collaboration with Laura Pankratz of (former) Physics 30 exam manager fame. We'll be at three conventions
            this year. Click on the button next to each to see more details:</p>

            <ul>
                <li style="margin-bottom:5px;"><button class="btn btn-mini" onclick="$('#detail_nctca').show()">More info</button> <b>NCTCA (Edmonton)</b>, Friday February 7</li>
                <ul id="detail_nctca" style="margin-bottom:5px; display:none;">
                    <li><i>Making the Most of Outcomes in Assessment</i>: 11:30am–12:30pm, Westin BC Room</li>
                    <li><i>Using Assessment to Improve Teaching</i>: 12:45pm–1:45pm, Westin BC Room</li>
                </ul>
                <li style="margin-bottom:5px;"><button class="btn btn-mini" onclick="$('#detail_catca').show()">More info</button> <b>CATCA (Red Deer)</b>, Friday February 21</li>
                <ul id="detail_catca" style="margin-bottom:5px; display:none;">
                    <li><i>Using Assessment to Improve Teaching</i>: 9:00am–10:00am, Red Deer Polytechnic 1437-30</li>
                    <li><i>Getting Students to Welcome Assessments</i>: 10:20am–11:20am, Red Deer Polytechnic 1437-30</li>
                    <li><i>Using Data to Build Better Assessments</i>: 11:40am–12:40pm, Red Deer Polytechnic 1437-30</li>
                    <li><i>Making the Most of Outcomes in Assessment</i>: 1:00pm–2:00pm, Red Deer Polytechnic 1437-30</li>
                </ul>
                <li style="margin-bottom:5px;"><button class="btn btn-mini" onclick="$('#detail_getca').show()">More info</button> <b>GETCA (Edmonton)</b>, Thursday February 27</li>
                <ul id="detail_getca" style="margin-bottom:5px; display:none;">
                    <li><i>Using Assessment to Improve Teaching</i>: 9:00am–10:00am</li>
                    <li><i>Using Data to Build Better Assessments</i>: 10:20am–11:40pm</li>
                    <li><i>Making the Most of Outcomes in Assessment</i>: 1:00pm–2:00pm</li>
                </ul>
            </ul>
   
            <p>My apologies to our Calgary-area teachers &mdash; we applied but were not picked up for conventions there.
            If you'd like to see us there next year, please let your people know! Also, if you're in the Calgary area 
            and would like to have somebody come by to talk about SmarterMarks (intro or more advanced stuff) and 
            assessment, though, we'd love to hear from you!</p>

            <p>If you have any questions, please let me know at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>.</p>

            <p style="margin-top:20px;">Jason</p>
    
            <div class="pull-right" style="padding-top:10px">
                <button id="hide_feb_6_2025_message" class="btn btn-primary" style="margin-left:10px;">Hide this tip</button>
            </div>
            <div style="clear:both;"></div>
        </div>  
    </div>
    
<?php		
}

if (!in_array("jan_21_2025_message", $hiddenTips)) {
?>

<div id="jan_21_2025_message_wrapper" class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
    <div class="rounded_white_wrapper_inner">

        <h3>Two updates this month</h3>

        <p>Hi all!</p>

        <p>As teachers in Alberta head into their second semester, we've made some updates to SmarterMarks. I wanted to 
        take a moment to highlight two of these...</p>
        <ul>
        <li style="margin-bottom:10px;">Most visibly, we've introduced <b>annotations in online sittings</b>. Students can now freely draw on and
        highlight text as they work through questions, and in the next few weeks should be able use the same tools to answer 
        written response questions. Annotations have been optimized to be very natural to use particularly for students using a
        tablet. We'll be doing a lot of work on online assessments this semester -- if you have features 
        you'd like to see, please let me know!</li>
        <li>In addition, we've made some <b>updates to our code</b> which will help facilitate more updates in
        the coming semester. These code updates should be invisible to teachers, so if you notice anything unusual
        please let me know.</li>
        </ul>
        <p>If you do have suggestions around online assessment, or spot a bug, or just want to say hi &#x1F60A;, you can drop me an email at
        <a href="mailto:support@smartermarks.com">support@smartermarks.com</a>. And thanks to all of our teachers for 
        continuing to choose SmarterMarks!</p>

        <p style="margin-top:20px;">Jason</p>

        <div class="pull-right" style="padding-top:10px">
            <button id="hide_jan_21_2025_message" class="btn btn-primary" style="margin-left:10px;">Hide this tip</button>
        </div>
        <div style="clear:both;"></div>
    </div>  
</div>

<?php		
}
if (!in_array("intro_howto", $hiddenTips)) {
?>

<div id="intro_howto_wrapper" class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
	<div class="rounded_white_wrapper_inner">

		<h3>Welcome to SmarterMarks!</h3>

		<p>Looks like it's your first time here. SmarterMarks has been designed to be quick and easy to use. Still, you may want to take a quick tour of how to build and use assessments. Or perhaps you'll want to learn more later about how to customize your assessments to get the most out of SmarterMarks.</p>
		
		<p>If you're ever looking for some information, click on the "Help" tab in the bar above. Heck, we can take you there right now.</p>
		
		<div class="pull-right" style="padding-top:10px">
			<a href="/Help/view/introduction" class="btn btn-primary">To the tutorials!</a>
			<a id="hide_intro_howto" class="btn btn-primary">Hide this tip</a>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<?php
}
?>
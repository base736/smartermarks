<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var stripePublicKey = "<?php
		$secrets = Configure::read('secrets');
		echo $secrets['stripe']['public_key'];
	?>";
</script>

<!-- Dialog boxes-->

<div class="hidden">

<?php
if (!empty($user)) {
?>

<!-- No account dialog -->

	<div id="studentEmails_dialog" class="dialogbox" title="Student accounts in list">
		<div class="dialog_wrapper">
			<p>The users listed below are currently registered as students. Click "Go back" to edit your list of email 
			addresses, or "Skip" to continue without adding these accounts.</p>
			
			<div id="bad_email_list"></div>
		</div>
	</div>

<!-- Add account dialog -->

	<div id="addAccounts_dialog" class="dialogbox" title="New accounts">
		<div class="dialog_wrapper">
			<p>The users listed below do not yet have a SmarterMarks teacher account. Click "Go back" to edit your list of email 
			addresses, or enter names then click "Continue" to create new accounts.</p>
			
			<form class="formular" accept-charset="utf-8">
                <table id="new_email_table" class="panel_table" style="border:1px solid #aaa;">
                    <thead>
                        <tr>
                            <td style="width:200px;">Name</td>
                            <td>Email</td>
                        </tr>
                    </thead>
                    <tbody id="new_email_rows"></tbody>
                </table>
            </form>
		</div>
	</div>

	<!-- Add users dialog -->

	<div id="addUsers_dialog" class="dialogbox" title="Add Users">
		<div class="dialog_wrapper">
			<p style="margin-bottom:20px;">Enter email addresses for the users you would like to add to this group below<?php if ($user['role'] != 'admin') { ?>, or click "Choose from list" to choose from your contacts<?php } ?>.</p>
			
			<form id="addUsers_form" class="formular" accept-charset="utf-8">
				<input type="hidden" name="email_list_id" value="add_email_list"/>
				<div class="labeled-input" style="width:100%;">
					<div style="width:100%; margin-bottom:5px !important;">
						<div style="float:left;"><label style="display:block; text-align:left; margin:3px 0px 0px;">Users:</label></div>

						<?php
						if (!empty($contacts)) {
						?>

						<div style="float:right;"><button id="add_selection_type" class="btn-tiny" style="font-size:13px;">Choose from list</button></div>

						<?php
						}
						?>

						<div style="clear:both;"></div>
					</div>
					<textarea tabindex="1" name="email_list" id="add_email_list" class="validate[required] defaultTextBox2" style="resize:none; height:100px; width:100%; box-sizing:border-box;" type="text"></textarea>
					<div id="add_email_checklist" style="resize:none; height:100px; width:100%; box-sizing:border-box; padding:4px 6px; border:1px solid #ccc; border-radius: 4px; background:white; display:none; overflow-y:scroll; overflow-x:hidden;">
						<?php
							if (!empty($contacts)) {
								foreach ($contacts as $contact) {
									echo "<div><input class='add_email_checkbox' type='checkbox' id='add_email_checkbox_" . $contact['id'] . "' data-email='" . $contact['email'] . "' /><label for='add_email_checkbox_" . $contact['id'] . "' style='display:inline-block; width:auto;'>" . $contact['name'] . " (" . $contact['email'] . ")</label></div>";
								}
							}
						?>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- Payment dialog -->

	<div id="payment_dialog" class="dialogbox" title="Payment information">
		<div class="dialog_wrapper">
			<p>Enter your payment information below and click "Pay now" to renew your account. Your credit 
			card will be charged as shown below (all prices in <span id="invoice_currency"></span>):</p>

			<table class="invoice_table" style="width:100%; box-sizing:border-box; margin:20px 0px;">
				<thead>
					<tr>
						<td>Item</td>
						<td width="18%">Quantity</td>
						<td width="18%">Price</td>
						<td width="18%">Amount</td>
					</tr>
				</thead>
				<tbody class="invoice_view"></tbody>
			</table>

			<div id="card_wrapper" class="labeled-input" style="width:auto;">
				<label style="margin-bottom:5px !important;">Credit card information:</label>
				<input id="card_name" type="text" class="defaultTextBox2" placeholder="Cardholder name" style="width:390px; margin-bottom:5px !important;"/>
				<div style="width:390px; background:white; padding:7px 6px; border:1px solid #cccccc; border-radius:4px;">
					<div id="card_element"></div>
				</div>
			</div>
			<div id="renew_buttons" class="ui-dialog-buttonset custom-buttonpane" style="display:flex; justify-content:space-between; align-items:center; margin-top:20px;">
				<div style="flex:none;">
					<a href="https://stripe.com/" target="_blank"><img src="/img/powered_by_stripe.svg" height="30"/></a>
				</div>
				<div style="flex:none;">
					<button id="renewal_cancel" type="button" class="btn btn-cancel btn_font">Cancel</button>
					<button id="renewal_pay" type="button" class="btn btn-save btn_font">Pay now</button>
				</div>
			</div>
		</div>
	</div>

<!-- Confirmation dialogs -->

	<div id="admin_confirmation_dialog" class="dialogbox" title="Invoice confirmation">
		<div class="dialog_wrapper">
			<p>Confirm the invoice below, then click "Create" to create it.</p>

			<table class="invoice_table" style="width:100%; box-sizing:border-box; margin:20px 0px;">
				<thead>
					<tr>
						<td>Item</td>
						<td width="18%">Quantity</td>
						<td width="18%">Price</td>
						<td width="18%">Amount</td>
					</tr>
				</thead>
				<tbody class="invoice_view"></tbody>
			</table>
		</div>
	</div>

	<div id="user_confirmation_dialog" class="dialogbox" title="Prepaid credits confirmation">
		<div class="dialog_wrapper" style="display:flex; flex-direction:row; align-items:flex-start;">
            <div style="flex:0 0 30px; padding:3px;"><img src="/img/info.svg" /></div>
            <div style="flex:1 1 100%; padding-left:5px; line-height:normal; font-size:14px; font-family:Arial, Helvetica, sans-serif;">
                Prepaid credits will be used to pay for <span id="confirmation_prepaid"></span>. Click
                "Finish" to apply the credits, or "Cancel" to return to the billing group view.
            </div>
		</div>
	</div>

<?php
}
?>

</div>

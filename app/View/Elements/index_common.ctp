<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<script type="text/javascript">
	var itemType = "<?php echo $itemType; ?>";
	var itemController = itemType + 's';
	var userID = <?php echo $user['id']; ?>;
	var userEmail = <?php echo json_encode($user['email']); ?>;
	var userDefaults = <?php echo $user['defaults']; ?>;
	var receivedCount = <?php echo isset($receivedCount) ? $receivedCount : '0'; ?>;

    var minFolderWidth = 215;
    var maxFolderWidth = 330;

	<?php if (!empty($jsonTree)) { ?> var treeData = <?php echo $jsonTree; } ?>;
</script>

<div class="hidden">
	<input type="hidden" id="refreshed" value="no">
</div>

<?php
if (!empty($user['message'])) { 
?>

<div class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
	<div class="rounded_white_wrapper_inner">

		<h3>An important message from SmarterMarks</h3>

		<?php
			$messageLines = explode("\n", $user['message']);
			foreach ($messageLines as $line) {
				echo "<p>" . $line . "</p>";
			}
		?>
		<div style="clear:both;"></div>
		<div style="height:50px">
			<a href="/users/clear_message"><button style="position:relative; top:10px;" class="btn btn-primary pull-right">Hide message</button></a>
		</div>
	</div>
</div>

<?php 
}

if (isset($isExpired) && ($isExpired & USER_EXPIRED)) { ?>

<div class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
	<div class="rounded_white_wrapper_inner">

		<h3>Your account has expired</h3>

		<p>Your account expired on <?php
			$userTimezone = new DateTimeZone($user['timezone']);
			$utcDate = new DateTime($user['expires']);
			$expiry = $utcDate->setTimezone($userTimezone);
			echo $expiry->format('F j, Y');
		?>. Until it has been renewed, you will not be able to build or modify forms, versions, assessment templates, and questions, or score new student responses. 
		Click the button below to <?php if (empty($user['locked'])) { ?> move your SmarterMarks renewal date to one week from today and <?php } ?> learn more about renewal options, 
		or drop us an email at <a href="mailto:support@smartermarks.com">support@smartermarks.com</a> and we'd be happy to offer more information!</p> 		

		<div style="height:50px">
			<a href="/Users/renew_main"><button style="position:relative; top:10px;" class="btn btn-primary pull-right">Renewal options</button></a>
		</div>
	</div>
</div>

<?php 
}

$banners = array();

if (isset($unshared_count)) {
	if ($user['banner_count'] < 5) {
		if ($user['open_created'] == 1) $banners[] = 'banner_thanks';
		else if ($unshared_count >= 10) $banners[] = 'banner_general';
	} else if ($unshared_count >= 10) {
		if (!empty($user['targeted'])) {
			$targeted = explode(',', $user['targeted']);
			$banners = array_merge($banners, $targeted);
		}
		
		$banners[] = 'banner_general';
		$banners[] = 'banner_online';
		$banners[] = 'banner_stats';
		$banners[] = 'banner_flexible';
	}
}

if (count($banners) > 0) {
	$banner_selected = $banners[array_rand($banners)];
	if ($this->elementExists('Banners/' . $banner_selected)) {
		echo $this->element('Banners/' . $banner_selected);
	} else {
		echo $this->element('Banners/banner_general');
		$this->log("Invalid banner '" . $banner_selected . "' for user " . $user['id'], 'bad_banners');
	}
}

if (strtolower($this->params['controller']) == 'documents') echo $this->element('messages');
?>

<!-- Main area -->

<h2 style='margin-bottom:<?php echo $adminView ? '10px' : '0px'; ?>;'><?php
	if (strtolower($this->params['controller']) == 'documents') echo 'Forms and Versions';
	else if (strtolower($this->params['controller']) == 'assessments') echo 'Assessment Templates';
	else if (strtolower($this->params['controller']) == 'questions') echo 'Question Bank';
	else if (strtolower($this->params['controller']) == 'sittingstudents') echo 'Assessments';
	else if (strtolower($this->params['controller']) == 'sittings') echo 'Online Sittings';
?></h2>

<?php
if (!$adminView) {
?>	

<div style='margin-bottom:10px; min-height:20px; height:auto;' class='folder_path'></div>

<?php
}
?>

<div id="index_wrapper" style="display:grid; grid-template-columns:<?php if (!$adminView) { ?>[left] 225px [resizer] 3px <?php } ?>[right] auto; grid-template-rows:[actions] auto [main];">

<?php
if (!$adminView) {
?>	

	<div style="grid-column:left; grid-row:actions" class="action_bar left_half">
		<div class="action_left">
			<div class="btn-group">
				<button class="btn btn-dark dropdown-toggle folder_dropdown" data-toggle="dropdown" href="#">
					Folder 
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu text-left">
					<li><a href="#" class="folder_create">New folder</a></li>
					<li><a href="#" class="folder_edit">Rename folder</a></li>
					<li><a href="#" class="folder_move">Move folder</a></li>
					<li><a href="#" class="folder_copy">Copy folder</a></li>
					<li><a href="#" class="folder_delete">Delete folder</a></li>
				</ul>
			</div>
			
			<?php 
			if ($user['role'] != 'student') { 
			?>
			
			<div class="btn-group">
				<button class="btn btn-dark dropdown-toggle folder_dropdown" data-toggle="dropdown" href="#">
					Community 
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu text-left">
					<li><a href="#" class="community_create">New community</a></li>
					<li><a href="#" class="community_edit">View settings</a></li>
					<li><a href="#" class="community_unsubscribe">Unsubscribe</a></li>
				</ul>
			</div>
			
			<?php
			}
			?>
		</div>
	</div>
	<div style="grid-column:left; grid-row:main; display:flex; flex-direction:column;">
		<div class="window_bar left_half" style="flex:none; margin-top:1px; padding-left:20px;">Folders</div>
		<div id="index_left" class="index_body left_half" style="flex:1 1 100%; box-sizing:border-box; width:100%;">
			<div class="folderTree" data-prefix="tree_main_" id="treePanel" style="padding-bottom:10px;"></div>
		</div>
	</div>

    <div style="grid-column:resizer; grid-row:actions; background-color:#2c2c2c;"></div>
    <div id="index_resizer" class="index_resizer" style="grid-column:resizer; grid-row:main;"></div>

<?php 
}
?>

	<?php echo $this->element($index_element); ?>
</div>

<div class="hidden">

<!-- Copy received dialog -->

	<div id="receiveCopy_dialog" class="dialogbox" title="Items Received">
		<div class="dialog_wrapper">
			<p id="receiveCopy_text" style="min-height:50px;"></p>
		</div>
	</div>
	
<!-- Unsubscribe main dialog -->

	<div id="unsubscribe_main_dialog" class="dialogbox" title="Unsubscribe from community">
		<div class="dialog_wrapper">
			<p id="unsubscribe_main_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Delete community dialog -->

	<div id="delete_community_dialog" class="dialogbox" title="Delete community">
		<div class="dialog_wrapper">
			<p id="delete_community_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Unsubscribe failure dialog -->

	<div id="unsubscribe_fail_dialog" class="dialogbox" title="Unable to unsusbcribe">
		<div class="dialog_wrapper">
			<p id="unsubscribe_fail_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Edit folder dialog -->

	<div id="editFolder_dialog" class="dialogbox" title="Folder Details">
		<div class="dialog_wrapper" style="min-height:70px;">
			<form class="formular" id="folder_edit" method="post" accept-charset="utf-8">
				<div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>
				<input type="hidden" name="data[Folder][id]" id="dfolder_id" />
				<input type="hidden" name="data[Folder][parent_id]" id="dfolder_parent_id" />
				<div class="labeled-input">
					<label for="dfolder_name" class="">Name</label><input tabindex="1" class="validate[required] defaultTextBox2" maxlength="255" type="text" id="dfolder_name"/>
				</div>
			</form>
		</div>
	</div>

<!-- Move folder dialog -->

	<div id="moveFolder_dialog" class="dialogbox" title="Move folder">
		<div class="dialog_wrapper">
			<form class="formular" id="folder_move" method="post" accept-charset="utf-8">
				<input type="hidden" id="moveFolder_id" />
				<div class="labeled-input" style="width:100%;">
					<label style="text-align:left;">Move to folder:</label>
					<div style="width:100%; background:white; border:solid 1px #abcfda;">
						<div class="folderTree" data-prefix="tree_moveFolder_" id="moveFolderParent" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px"></div>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- Copy folder dialog -->

	<div id="copyFolder_dialog" class="dialogbox" title="Copy folder">
		<div class="dialog_wrapper">
			<form class="formular" method="post" accept-charset="utf-8">
				<input type="hidden" id="copyFolder_id" />
				<div style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:14px;" id="copyFolder_text"></div>
				<div style="width:100%; background:white; border:solid 1px #abcfda;">
					<div class="folderTree" data-prefix="tree_copyFolder_" id="copyFolderParent" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px"></div>
				</div>
				<div class="labeled-input" style="margin-top:10px;">
					<label style="width:auto; display:inline-block;">Copy type:</label>
					<select class="defaultSelectBox" id="copyFolder_type" style="width:283px;">
						<option value="folder">Copy folder and contents</option>
						<option value="contents">Copy contents only</option>
					</select>
				</div>
			</form>
		</div>
	</div>

<!-- Copy waiting dialog -->

	<div id="copyWaiting_dialog" class="dialogbox" title="Copying folder">
		<div class="dialog_wrapper" style="padding:20px 10px;">
			<p>Copying folder contents. For large folders, this may take a minute...</p>
			<div id="copyFolder_progress"></div>
		</div>
	</div>

<!-- Delete folder dialog -->

	<div id="deleteFolder_dialog" class="dialogbox" title="Delete Folder">
		<div class="dialog_wrapper">
			<p id="deleteFolder_text" style="min-height:50px;"></p>
		</div>
	</div>

<!-- Send copy dialog -->

	<div id="sendCopy_dialog" class="dialogbox" title="Send A Copy">
		<div class="dialog_wrapper">
			<p id="sendCopy_text" style="margin-bottom:20px;"></p>
			
			<form id="sendCopy_form" class="formular" method="post" accept-charset="utf-8">
				<input type="hidden" name="email_list_id" value="send_email_list"/>
				<div class="labeled-input">
					<div style="width:100%; margin-bottom:5px !important;">
						<div style="float:left;"><label style="display:block; text-align:left; margin:3px 0px 0px;">Send to:</label></div>
						<div style="float:right;"><button id="send_selection_type" class="btn-tiny" <?php if (empty($contacts)) echo 'disabled="disabled"' ?> style="font-size:13px;">Choose from list</button></div>
						<div style="clear:both;"></div>
					</div>
					<textarea tabindex="1" name="email_list" id="send_email_list" class="validate[required] defaultTextBox2" style="resize:none; height:100px; width:340px;" type="text"></textarea>
					<div id="send_email_checklist" style="resize:none; height:100px; width:340px; padding:4px 6px; border:1px solid #ccc; border-radius: 4px; background:white; display:none; overflow-y:scroll; overflow-x:hidden;">
						<?php
							if (!empty($contacts)) {
								foreach ($contacts as $contact) {
									echo "<div><input class='send_email_checkbox' type='checkbox' id='send_email_checkbox_" . $contact['id'] . "' data-email='" . $contact['email'] . "' /><label for='send_email_checkbox_" . $contact['id'] . "' style='display:inline-block; width:auto;'>" . $contact['name'] . " (" . $contact['email'] . ")</label></div>";
								}
							}
						?>
					</div>
				</div>
			</form>
		</div>
	</div>
	
<!-- Edit community dialog -->

	<div id="editCommunity_dialog" class="dialogbox" title="Community details">
		<div class="dialog_wrapper" style="padding:0px;">
			<form class="formular" id="editCommunity_form" method="post" accept-charset="utf-8">
				<input type="hidden" id="community_id" />
				<input type="hidden" id="community_members_string" />
				<input type="hidden" id="community_folder_id" />
				<div style="display:inline-block; vertical-align:top; width:270px; margin:10px 10px 0px;">
					<div class="labeled-input">
						<label style="text-align:left; width:auto; display:block;">Community name:</label>
						<input style="width:257px;" tabindex="1" id="community_name" class="validate[required] defaultTextBox2" maxlength="255" type="text" />
					</div>
					<div class="labeled-input" style="width:100%; margin-bottom:0px;">
						<div style="width:100%;">
							<label style="margin-top:5px; float:left;">Community members:</label>
							<div class="ui-dialog-buttonset" style="float:right;">
								<button id="addMembers_button" type="button" class="btn-tiny btn_font">Add new members</button>
							</div>
							<div style="clear:both;"></div>
						</div>
						<div id="community_members_list"></div>
					</div>
				</div><div style="display:inline-block; vertical-align:top; width:340px; margin:10px 10px 0px;">
					<div class="labeled-input">
						<label style="text-align:left; width:auto; display:block;">Adding and deleting items:</label>
						<select class="defaultSelectBox policy_select" id="policy_add_delete">
							<option value="admin">Only administrators can add and delete items</option>
							<option value="owner">All members can add; admin and contributor can delete</option>
							<option value="all">All members can add and delete items</option>
						</select>
					</div>
					<div class="labeled-input">
						<label style="text-align:left; width:auto; display:block;">Editing items:</label>
						<select class="defaultSelectBox policy_select" id="policy_edit">
							<option value="admin">Only administrators can edit items</option>
							<option value="owner">Administrators and contributor can edit items</option>
							<option value="all">All members can edit all items</option>
						</select>
					</div>
					<div class="labeled-input">
						<label style="text-align:left; width:auto; display:block;">Copying items from the community:</label>
						<select class="defaultSelectBox policy_select" id="policy_export">
							<option value="closed">Items can not be copied out of the community</option>
							<option value="open">Items can be copied in and out of the community</option>
						</select>
					</div>
					<div class="labeled-input">
						<label style="text-align:left; width:auto; display:block;">Viewing scores:</label>
						<select class="defaultSelectBox policy_select" id="policy_scores">
							<option value="owner">All members can view their own scores only</option>
							<option value="admin">Admin can view all scores</option>
							<option value="all">All members can view all scores</option>
						</select>
					</div>
						<div class="labeled-input">
						<label style="text-align:left; width:auto; display:block;">Adding new members:</label>
						<select class="defaultSelectBox policy_select" id="policy_user_add">
							<option value="admin">Only administrators can add new members</option>
							<option value="member">All members can add new members</option>
<!--							<option value="all">Users can add themselves to this community</option> -->
						</select>
					</div>
				</div> 
			</form>
		</div>
	</div>

<!-- Add member dialog -->

	<div id="addMember_dialog" class="dialogbox" title="Add A Member">
		<div class="dialog_wrapper">
			<form id="addMember_form" class="formular" method="post" accept-charset="utf-8">
				<input type="hidden" name="email_list_id" value="member_email_list"/>
				<div class="labeled-input">
					<div style="width:100%; margin-bottom:5px !important;">
						<div style="float:left;"><label style="display:block; text-align:left; margin:3px 0px 0px;">New members:</label></div>
						<div style="float:right;"><button id="member_selection_type" class="btn-tiny" <?php if (empty($contacts)) echo 'disabled="disabled"' ?> style="font-size:13px;">Choose from list</button></div>
						<div style="clear:both;"></div>
					</div>
					<textarea tabindex="1" name="email_list" id="member_email_list" class="validate[required] defaultTextBox2" style="resize:none; height:200px; width:340px;" type="text"></textarea>
					<div id="member_email_checklist" style="resize:none; height:200px; width:340px; padding:4px 6px; border:1px solid #ccc; border-radius: 4px; background:white; display:none; overflow-y:scroll; overflow-x:hidden;">
						<?php
							if (!empty($contacts)) {
								foreach ($contacts as $contact) {
									echo "<div class='member_mail_row'><input class='member_email_checkbox' type='checkbox' id='member_email_checkbox_" . $contact['id'] . "' data-email='" . $contact['email'] . "' /><label for='member_email_checkbox_" . $contact['id'] . "' style='display:inline-block; width:auto;'>" . $contact['name'] . " (" . $contact['email'] . ")</label></div>";
								}	
							}
						?>
					</div>
				</div>
			</form>
		</div>
	</div>		

<!-- Empty trash dialog -->

	<div id="emptyTrash_dialog" class="dialogbox" title="Delete Item">
		<div class="dialog_wrapper">
			<p id="emptyTrash_text" style="min-height:50px;">Permanently delete all items in the Trash folder?</p>
		</div>
	</div>

<!-- Copy item dialog -->

	<div id="copyItem_dialog" class="dialogbox" title="Copy Item">
		<div class="dialog_wrapper">
			<div style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:14px;" id="copyItem_text"></div>
			<div style="background:white; border:solid 1px #abcfda;">
				<div class="folderTree" data-prefix="tree_copy_" id="copyTargetPanel" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px"></div>
			</div>
		</div>
	</div>


<!-- Move item dialog -->

	<div id="moveItem_dialog" class="dialogbox" title="Move Item">
		<div class="dialog_wrapper">
			<div style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:14px;" id="moveItem_text"></div>
			<div style="background:white; border:solid 1px #abcfda;">
				<div class="folderTree" data-prefix="tree_move_" id="moveTargetPanel" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px"></div>
			</div>
		</div>
	</div>

<!-- Delete item dialog -->

	<div id="deleteItem_dialog" class="dialogbox" title="Delete Item">
		<div class="dialog_wrapper" style="min-height:70px;">
			<p id="deleteItem_text"></p>
			<div id="delete_warning">
				<div style="display:inline-block; width:30px; padding:3px; vertical-align:middle;"><img src="/img/warning.svg" /></div>
				<div style="display:inline-block; width:330px; padding-left:5px; vertical-align:top; line-height:normal; font-size:13px; font-family:Arial, Helvetica, sans-serif;">
					Warning: Because items cannot be copied out of this community, the selected item or items will be <b>deleted immediately</b> rather than sent to the trash.
				</div>
			</div>
		</div>
	</div>

<!-- Duplicate item dialog -->

	<div id="duplicateItem_dialog" class="dialogbox" title="Duplicate Item">
		<div class="dialog_wrapper">
			<form action="#" class="formular" id="copy_form" method="post" accept-charset="utf-8">
				<div style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:14px;" id="duplicateItem_text"></div>
				<div style="background:white; border:solid 1px #abcfda; margin-bottom:10px;">
					<div class="folderTree" data-prefix="tree_duplicate_" id="duplicateTargetPanel" style="padding:10px; height:150px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px"></div>
				</div>
			</form>
		</div>
	</div>

</div>

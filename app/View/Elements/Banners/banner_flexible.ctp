<div id="banner_wrapper" data-banner-name="banner_flexible" class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
	<div class="rounded_white_wrapper_inner" style="width:95%">
		<div style="display:flex; justify-content:flex-start; align-items:center; position:relative;">
			<div style="flex:0 0 60px; height:60px; margin-right:15px;"><img src="/img/banner/banner_flexible.svg" /></div>
			<div style="flex:1 1 auto;" align="justify">
				Thinking about sharing questions to the open bank? Every question you contribute helps build a powerful community resource, so we've made it easy to share as you create, or to share just some of the questions you've built. Click on "Manage" to open the Manage Open Questions tool and share selectively, or click "Share now" to<br />add the questions you've already built, and start sharing new questions you build by default.
			</div>
			<div style="position:absolute; bottom:0px; right:0px;">
				<a href="/Questions/manage_open"><button class="btn-tiny btn-primary btn_font" style="display:inline-block; margin:0px; margin-right:5px;">Manage</button></a>
				<button class="btn-tiny btn-primary btn_font banner_button" style="display:inline-block; margin:0px;">Share now</button>
			</div>
		</div>
	</div>
</div>

<div id="banner_wrapper" data-banner-name="banner_general" class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
	<div class="rounded_white_wrapper_inner" style="width:95%">
		<div style="display:flex; justify-content:flex-start; align-items:center; position:relative;">
			<div style="flex:0 0 60px; height:60px; margin-right:15px;"><img src="/img/banner/banner_general.svg" /></div>
			<div style="flex:1 1 auto;" align="justify">
				We'd like you to consider joining the teachers from across Alberta who have shared questions to the open question bank. 
                Sharing to the open bank improves your item statistics and makes it possible for others to create online assessments and 
                update assessments using proven questions. If you'd like to contribute the questions you build, click "Share<br />now". 
                We'll add the questions you've already built, and set you up to share new questions you build by default.
			</div>
			<button class="btn-tiny btn-primary btn_font banner_button" style="position:absolute; bottom:0px; right:0px; margin:0px;">Share now</button>
		</div>
	</div>
</div>

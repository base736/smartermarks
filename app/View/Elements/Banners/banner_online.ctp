<div id="banner_wrapper" data-banner-name="banner_online" class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
	<div class="rounded_white_wrapper_inner" style="width:95%">
		<div style="display:flex; justify-content:flex-start; align-items:center; position:relative;">
			<div style="flex:0 0 60px; height:60px; margin-right:15px;"><img src="/img/banner/banner_general.svg" /></div>
			<div style="flex:1 1 auto;" align="justify">
				Teachers from across Alberta have shared a total of <?php echo number_format($numOpen); ?> questions to the open question bank. Every question added makes the open bank a more powerful, more secure tool for all teachers &mdash; one that is especially valuable as online assessment becomes important. Please consider clicking "Share now" to contribute your questions. We'll add <br>those you've already built, and set you up to share new questions you build by default.
			</div>
			<button class="btn-tiny btn-primary btn_font banner_button" style="position:absolute; bottom:0px; right:0px; margin:0px;">Share now</button>
		</div>
	</div>
</div>

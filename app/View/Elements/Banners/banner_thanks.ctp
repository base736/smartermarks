<div id="banner_wrapper" data-banner-name="banner_thanks" class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
	<div class="rounded_white_wrapper_inner" style="width:95%">
		<div style="display:flex; justify-content:flex-start; align-items:center; position:relative;">
			<div style="flex:0 0 60px; height:60px; margin-right:15px;"><img src="/img/banner/banner_thanks.svg" /></div>
			<div style="flex:1 1 auto;" align="justify">
				The open bank now includes <?php echo number_format($numOpen); ?> questions shared by teachers from across Alberta. These questions have been used in thousands of assessments already, allowing teachers to build and update assessments using proven questions. This wouldn't be possible without you -- thank you so much for contributing to the open question bank.
			</div>
		</div>
	</div>
</div>

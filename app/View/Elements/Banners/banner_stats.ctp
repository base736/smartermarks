<div id="banner_wrapper" data-banner-name="banner_stats" class="rounded_white_wrapper" style="width: 890px; margin:20px auto;">
	<div class="rounded_white_wrapper_inner" style="width:95%">
		<div style="display:flex; justify-content:flex-start; align-items:center; position:relative;">
			<div style="flex:0 0 60px; height:60px; margin-right:15px;"><img src="/img/banner/banner_stats.svg" /></div>
			<div style="flex:1 1 auto;" align="justify">
				Sharing questions to the open bank doesn't just help other teachers. Questions that are shared gather statistics more quickly, and from a variety of classrooms and assessments, giving you a clearer picture of which items best allow students to demonstrate their level of understanding. To start gathering better statistics today, click "Share now".<br />We'll add the questions you've already built, and set you up to share new questions you build by default.
			</div>
			<button class="btn-tiny btn-primary btn_font banner_button" style="position:absolute; bottom:0px; right:0px; margin:0px;">Share now</button>
		</div>
	</div>
</div>

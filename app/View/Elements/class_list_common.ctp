<div style="display:flex; flex-direction:row; justify-content:space-between; align-items:center; gap:20px; width:100%;">
    <div style="flex:1 1 100%; display:flex; flex-direction:row; align-items:center;">
        <label style="flex:none; width:auto; height:auto; margin:0px;">Class List: </label>
        <select id="class_list_select" class="defaultSelectBox" style="flex:1 1 100%; margin:0px;">
        </select>
    </div>
    <div style="flex:none; width:auto;">
        <button class="btn btn_font" id="class_list_index">Go to class lists</button>
    </div>
</div>

<div id="class_list_info_wrapper" style="display:flex; flex-direction:row; align-items:flex-start; margin-top:20px;">
    <div style="flex:0 0 30px; padding:3px;"><img src="/img/info.svg" /></div>
    <div style="flex:1 1 100%; padding-left:5px; line-height:normal; font-size:14px; font-family:Arial, Helvetica, sans-serif;">
        Looks like you haven't created any class lists yet! Click "Go to class lists" to open your class lists index in a new tab, 
        then click "New Class List" at the top-right to create one. Once you've created a class list, you'll be able to select
        it here.
    </div>
</div>

<div id="class_list_edit_wrapper" style="margin-top:10px;">
    <div style="margin-bottom:5px;">
        <button class="btn btn-mini btn_font check_list_button" id="class_list_check_all">Check all</button>
        <button class="btn btn-mini btn_font check_list_button" id="class_list_clear_all">Clear all</button>
        <button class="btn btn-mini btn_font check_list_button" id="class_list_shuffle">Shuffle</button>
    </div>

    <div id="class_list_wrapper"></div>
</div>
<!--
SmarterMarks
Copyright (C) 2012-2025 SmarterMarks Inc.

This program is free software; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along with this program. If
not, see <http://www.gnu.org/licenses/>
-->

<?php
	$location = strtolower($this->params['controller']) . '/' . strtolower($this->action);
	if (isset($user) && isset($_COOKIE[$user['id'] . '_lastController']) && isset($_COOKIE[$user['id'] . '_lastAction'])) {
		$lastController = $_COOKIE[$user['id'] . '_lastController'];
		$lastAction = $_COOKIE[$user['id'] . '_lastAction'];
		$lastLocation = strtolower($lastController) . '/' . strtolower($lastAction);
	} else $lastLocation = $location;

	$isAdmin = isset($user['role']) && $user['role'] == 'admin';

	$init_tab_row = 0;
	if ($isAdmin && in_array($lastLocation, array('graphs/index', 'users/index', 'scores/index', 
		'payments/index'))) {
		$init_tab_row = 1;
	}
?>

<script type='text/javascript'>
	$(document).ready(function() {
		var tab_row = <?php echo $init_tab_row; ?>;

		$('#tab_scroll_up').on('click', function() {
			if (tab_row > 0) {
				tab_row--;
				$('#tab_button_wrapper').animate({
					top: -tab_row * 115
				}, 300);
			}
		});

		$('#tab_scroll_down').on('click', function() {
			if (tab_row < 1) {
				tab_row++;
				$('#tab_button_wrapper').animate({
					top: -tab_row * 115
				}, 300);
			}
		});

		<?php 
		if (!empty($user['id'])) {
		?>

		$('.tab_button').on('click', function() {
			if (this.hasAttribute('data-controller') && !$(this).hasClass('clicked')) {
				$(this).addClass('clicked');
                Cookies.set('<?php echo $user['id']; ?>_lastController', $(this).attr('data-controller'), { expires:30 });
                Cookies.set('<?php echo $user['id']; ?>_lastAction', $(this).attr('data-action'), { expires:30 });
				window.location = '/' + $(this).attr('data-controller') + '/' + $(this).attr('data-action');
			}
		});

		<?php
		}
		?>
	});
</script>

<!-- Home bar -->

<div class="row_wrapper">
<div class="column_left">
	<div style="float:right; width:250px; height:100%; background-image:url('/img/default_layout/menu_bar.png'); background-repeat:repeat-x;"></div>
</div><div class="column_center" style="overflow:visible; width:960px; height:58px; position:relative; background-image:url('/img/default_layout/menu_bar.png'); background-repeat:repeat-x;">
	<a href="/"><div style="position:absolute; left:35px; bottom:19px;"><img src="/img/default_layout/logo.svg" style="width:172px;" /></div></a>
	<?php
	if (isset($user)) {
	?>
	<div style="float:right; position:relative; top:20px; right:40px;">
		<div class="user_dropdown" style="position:relative; display:inline-block;">
			Logged in as <a class="dropdown-toggle" href="#" data-toggle="dropdown"><?php echo empty($user['name']) ? $user['email'] : $user['name'];?></a> &#9662;

			<ul class="dropdown-menu pull-right" style="top:22px;">

				<li><?php echo $this->Html->link('Settings', array('controller' => 'users', 'action' => 'edit'));?></li>				

				<?php
					$can_renew = true;
                    $secrets = Configure::read('secrets');
                    $autorenew = $secrets['email_autorenew'];
                    foreach ($autorenew as $entry) {
                        if (preg_match($entry['regex'], $user['email']) === 1) $can_renew = false;
                    }

					if ($can_renew) {					
				?>
			
				<li><?php echo $this->Html->link('Renew accounts', array('controller' => 'Users', 'action' => 'renew_main', $user['id']));?></li>

				<?php
				}
				if ($user['role'] == 'teacher') {
				?>
			
                <li><?php echo $this->Html->link('Manage open questions', array('controller' => 'Questions', 'action' => 'manage_open'));?></li>
				<li><?php echo $this->Html->link('Delete old scores', array('controller' => 'Scores', 'action' => 'delete_old'));?></li>

				<?php
				}
				?>

				<li><?php echo $this->Html->link('Log out', array('controller' => 'Users', 'action' => 'logout'));?></li>
			</ul>
		</div>
	</div>
	<?php
	}
	?>
</div><div class="column_right">
	<div style="float:left; width:250px; height:100%; background-image:url('/img/default_layout/menu_bar.png'); background-repeat:repeat-x;"></div>
</div>
</div>

<!-- Colored bar -->

<div class="row_wrapper">
<div class="column_left" style="background-image:url('/img/default_layout/colored_band-0.png'); background-repeat:repeat-y;">
</div><div class="column_center" style="width:960px; height:3px; position:relative; background-image:url('/img/default_layout/colored_band-1.png'); background-repeat:repeat-y;">
</div><div class="column_right" style="background-image:url('/img/default_layout/colored_band-2.png'); background-repeat:repeat-y;"></div>
</div>

<!-- Button bar -->

<?php
$showMenu = true;
if (!isset($user)) $showMenu = false;
else if (in_array($this->action, array('maintenance', 'awaiting_approval', 'awaiting_validation'))) $showMenu = false;
else if (($this->action == 'new_password') && (($user['entropy'] < 32) || empty($user['password']))) $showMenu = false;

if ($showMenu) {

	if ($isAdmin) {
		?>
		<style>
			.tab_button { margin:15px 0px; }
		</style>
		<?php
	}

?>

<div class="row_wrapper" >
<div class="column_left">
	<div style="float:right; width:250px; height:100%; background-color:#070707; background-repeat:repeat-x;"></div>
</div><div class="column_center" style="width:960px; height:<?php echo (!empty($user['role']) && ($user['role'] == 'student')) ? '30px' : '115px'; ?>; position:relative; overflow:hidden; background-image:url('/img/default_layout/actions_background.jpg'); background-repeat:no-repeat;">

	<?php
	if ($isAdmin) {
	?>

	<div id="tab_scroll_wrapper" style="display:inline-flex; flex-direction:column; box-sizing:border-box; justify-content:space-between; vertical-align:top; height:115px; color:white; margin-left:40px; padding:25px 0px; font-size:24px;">
		<div id="tab_scroll_up" style="cursor:pointer;">&#x25B2;</div>
		<div id="tab_scroll_down" style="cursor:pointer;">&#x25BC;</div>
	</div>

	<?php
	}
	?>

	<div id="tab_button_wrapper" style="display:inline-block; vertical-align:top; margin-left:<?php echo $isAdmin ? '20px' : '40px'; ?>; position:absolute; top:-<?php echo $init_tab_row * 115?>px;">
	<?php
	if (isset($user['role']) && ($user['role'] == 'student')) {
	?>
<!--
		<a href="/Sittings/index"><div class="tab_button">
			<div class="tab_icon" style="background-image:url('/img/tab_icons/wallet_icon.svg');"></div>
			<div class="tab_text">Assessments</div>
		</div></a>
-->
	<?php
	} else if (isset($user['role']) && ($user['role'] == 'school')) {
	?>
		<div class="tab_button" data-controller="Groups" data-action="index">
			<div class="tab_icon" style="background-image:url('/img/tab_icons/groups_icon.svg');"></div>
			<div class="tab_text">Billing<br />Groups</div>
		</div>
	<?php
	} else {
	?>
		<div>
			<div class="tab_button" data-controller="Documents" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/wallet_icon.svg');"></div>
				<div class="tab_text">Forms and<br />Versions</div>
			</div>

			<div class="tab_button" data-controller="Sittings" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/wallet_icon.svg');"></div>
				<div class="tab_text">Online<br />Sittings</div>
			</div>

			<div class="tab_button" data-controller="Assessments" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/wallet_icon.svg');"></div>
				<div class="tab_text">Assessment<br />Templates</div>
			</div>

			<div class="tab_button" data-controller="Questions" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/wallet_icon.svg');"></div>
				<div class="tab_text">Question<br />Bank</div>
			</div>

            <?php
            if (!$isAdmin) {
            ?>

            <div class="tab_button" data-controller="ClassLists" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/wallet_icon.svg');"></div>
				<div class="tab_text">Class<br />Lists</div>
			</div>

            <?php
            }
            ?>

			<?php
			if ($isAdmin || ($groupCount > 0)) {
			?>

			<div class="tab_button" data-controller="Groups" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/groups_icon.svg');"></div>
				<div class="tab_text">Billing<br />Groups</div>
			</div>

			<?php
			}
			?>

			<div class="tab_button" data-controller="Help" data-action="view/introduction">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/help_icon.svg');"></div>
				<div class="tab_text">Help</div>
			</div>
		</div>

	<?php
	if ($isAdmin) {
	?>

		<div>
			<div class="tab_button" data-controller="Graphs" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/graph_icon.svg');"></div>
				<div class="tab_text">Graphs</div>
			</div>

			<div class="tab_button" data-controller="Users" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/users_icon.svg');"></div>
				<div class="tab_text">Users</div>
			</div>

			<div class="tab_button" data-controller="Scores" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/results_icon.svg');"></div>
				<div class="tab_text">Results</div>
			</div>

			<div class="tab_button" data-controller="Payments" data-action="index">
				<div class="tab_icon" style="background-image:url('/img/tab_icons/payments_icon.svg');"></div>
				<div class="tab_text">Payments</div>
			</div>
		</div>

	<?php
		}
	}
	?>
	
	</div>

	<?php 
	if (in_array($location, array('documents/index', 'classlists/index', 'stylesheets/index', 'sittings/index', 
        'assessments/index', 'questions/index'))) {
	?>

	<div id="emptyTrash_button" class="tab_button tab_right">
		<div class="tab_icon" style="background-image:url('/img/tab_icons/empty_trash_icon.svg');"></div>
		<div class="tab_text">Empty<br />Trash</div>
	</div>

	<?php
		if (isset($user_locked) && !$user_locked) {
	?>

	<div id="newItem_button" class="tab_button tab_right">
		<div class="tab_icon"></div>
		<div class="tab_text"></div>
	</div>

	<?php 
		}
	} else if (($location == 'questions/build') && !empty($folder_id)) {
	?>

	<div id="newItem_button" class="tab_button tab_right" style="display:block;" data-folder-id="<?php echo $folder_id; ?>">
		<div class="tab_icon" style="background-image:url('/img/tab_icons/add_item_icon.svg');"></div>
		<div class="tab_text">New<br />Question</div>
	</div>

	<?php 
	} else if (($location == 'groups/index') || ($location == 'groups/view')) {
	?>

	<div id="renewGroup_button" class="tab_button" style="float:right; cursor:pointer; margin-right:40px;">
		<div class="tab_icon" style="background-image:url('/img/tab_icons/renew_users_icon.svg');"></div>
		<div class="tab_text">Renew<br />Group</div>
	</div>

	<?php
	} else if ($location  == 'users/index') {
	?>

	<a href="/Users/add/"><div class="tab_button" style="float:right; margin-right:40px;">
		<div class="tab_icon" style="background-image:url('/img/tab_icons/add_users_icon.svg');"></div>
		<div class="tab_text">Add<br />Users</div>
	</div></a>

	<div id="renewUsers_button" class="tab_button" style="float:right; cursor:pointer;">
		<div class="tab_icon" style="background-image:url('/img/tab_icons/renew_users_icon.svg');"></div>
		<div class="tab_text">Renew<br />Users</div>
	</div>	

	<?php
	} else if ($location  == 'payments/view_receipt') {
	?>
	
	<div id="print_div" class="tab_button" style="float:right; cursor:pointer; margin-right:40px;">
		<div class="tab_icon" style="background-image:url('/img/tab_icons/print_icon.svg');"></div>
		<div class="tab_text">Print</div>
	</div>

	<?php
	}
	?>


</div><div class="column_right">
	<div style="float:left; width:250px; height:100%; background-color:#070707; background-repeat:repeat-x;"></div>
</div>
</div>

<?php
}
?>


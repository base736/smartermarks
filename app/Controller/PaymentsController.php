<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class PaymentsController extends AppController {

    public $presetVars = true; // using the model configuration
    public $paginate = array('paramType' => 'querystring');

	public function beforeFilter() {
	    parent::beforeFilter();

	    // Tell the auth component that all actions can be accessed without having to be logged in
		$this->Auth->allow('hook_succeeded');
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
	    }

		if (in_array($this->action, array('index'))) {
			return ($this->Auth->user('role') == 'admin');
		}

		if (in_array($this->action, array('create'))) {
			return true;
		}

		if (in_array($this->action, array('finish', 'finish_invoice', 'view_receipt', 'print_receipt', 'processing', 'error'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$payment_id = $this->request->params['pass'][0];

				$this->Payment->contain();
				$payment = $this->Payment->findById($payment_id);
				if (!empty($payment)) {
					if ($this->Auth->user('role') == 'admin') return true;
					else return ($payment['Payment']['user_id'] == $this->Auth->user('id'));
				} else return false;
			} else return false;
		}

		return false;
	}

	public function index() {
		$this->Payment->contain(array('User'));
		$this->paginate['order'] = array('Payment.created' => 'desc');
		$this->paginate['conditions'] = array(
			'Payment.status' => array('complete', 'invoice')
		);
		$this->paginate['limit'] = 10;
		$this->set('payments', $this->paginate());		
	}

	private function checkPayment($payment_id) {
		App::uses('Email', 'Lib');
        App::import('Vendor', 'Stripe', array('file' => 'Stripe/init.php'));

		$secrets = Configure::read('secrets');
		\Stripe\Stripe::setApiKey($secrets['stripe']['secret_key']);

		$dbo = $this->Payment->getDataSource();
		$dbo->execute('LOCK TABLES payments WRITE, groups WRITE;');
		$payments = $this->Payment->query('SELECT * FROM payments WHERE id=' . $payment_id);

		$failure_status = false;
        $complete_payment = false;

		if (!empty($payments)) {
			$payment = $payments[0]['payments'];
            $details = json_decode($payment['details'], true);

            if ($payment['status'] == 'invoice') {

                $failure_status = 'not_payment';

            } if ($payment['status'] != 'complete') {

                // Check that credit is available as required in billing groups
                
                $prepaid_required = array();
                foreach ($details['purchase']['items'] as $item) {
                    if (!empty($item['prepaid_required'])) {
                        if (!empty($item['group_id'])) {
                            if (!array_key_exists($item['group_id'], $prepaid_required)) {
                                $prepaid_required[$item['group_id']] = 0;
                            }
                            $prepaid_required[$item['group_id']] += $item['prepaid_required'];
                        } else $failure_status = 'no_group_id';
                    }
                }

                if (($failure_status === false) && !empty($prepaid_required)) {
                    $group_ids = array_keys($prepaid_required);
                    $groups = $this->Payment->query('SELECT * FROM groups WHERE id IN (' . implode(',', $group_ids) . ')');

                    $prepaid_counts = array();
                    foreach ($groups as $group) {
                        $prepaid_counts[$group['groups']['id']] = $group['groups']['prepaid_count'];
                    }

                    foreach ($prepaid_required as $group_id => $this_required) {
                        if (array(key_exists($group_id, $prepaid_counts))) {
                            if ($this_required <= $prepaid_counts[$group_id]) $prepaid_counts[$group_id] -= $this_required;
                            else $failure_status = 'prepaid_unavailable';
                        } else $failure_status = 'bad_group_id';
                    }
                }

                // Retrieve Stripe payment if required

                if (($failure_status === false) && !empty($payment['stripe_id'])) {
                    try {

                        $paymentIntent = \Stripe\PaymentIntent::retrieve($payment['stripe_id'], []);
        
                        if ($paymentIntent['status'] == 'canceled') {
                            $failure_status = 'canceled';
                        } else if ($paymentIntent['status'] != 'succeeded') {
                            $failure_status = 'processing';
                        } else if ($details['invoice']['total'] != $paymentIntent['amount']) {
                            $failure_status = 'payment_mismatch';
                        }

                    } catch (Error $e) {

                        $failure_status = 'stripe_failed';

                    }
                }

                if ($failure_status === false) {
                 
                    // Retrieve group prepay if required

                    if (!empty($prepaid_required)) {
                        foreach ($prepaid_counts as $group_id => $prepaid_count) {
                            $this->Payment->query('UPDATE groups SET prepaid_count=' . $prepaid_count . ' WHERE id=' . $group_id);
                        }

                        foreach ($details['purchase']['items'] as &$pItem) unset($pItem['prepaid_required']);
                        $payment['details'] = json_encode($details);
                    }

                    // Get set to complete this payment if it isn't completed already

                    $this->Payment->query('UPDATE payments SET status="complete" WHERE id=' . $payment_id);
                    $payment['status'] = 'complete';

                    $complete_payment = true;

                } else {

                    Email::send($secrets['admin_email'], "SmarterMarks: Payment error", "payment_error", array(
                        'payment_id' => $payment_id,
                        'failure_status' => $failure_status
                    ));

                    // Update payment status

                    $this->Payment->query('UPDATE payments SET status="' . $failure_status . '" WHERE id=' . $payment_id);
                    $payment['status'] = $failure_status;
                } 

            }
		} else $payment = false;

		$dbo->execute('UNLOCK TABLES');

        if ($complete_payment) {

            // Complete purchase

			$this->Payment->apply($details['purchase'], $payment_id);

			// Send email

			$this->loadModel('User');
			$user = $this->User->findById($payment['user_id']);
			$email = $user['User']['email'];

			$userTimezone = new DateTimeZone($user['User']['timezone']);
			$createdTime = new DateTime($payment['created'], $userTimezone);
			$date = $createdTime->format('F j, Y');
	
			$id_padded = str_pad($payment_id, 6, '0', STR_PAD_LEFT);
			Email::send($email, "Your SmarterMarks renewal (payment ID " . $id_padded . ")", "receipt", array(
				'payment_id' => $payment_id,
				'email' => $email, 
				'date' => $date,
				'details' => $details
			));

			$secrets = Configure::read('secrets');
			Email::send($secrets['admin_email'], "SmarterMarks: User renewed", "user_renewed_2", array(
				'payment' => $payment
			));

			// Reload user data in case it has been updated

            if (!empty($this->Auth->user('id'))) {
                $this->User->contain();
                $newUser = $this->User->findById($this->Auth->user('id'));
                $this->Session->write('Auth.User', $newUser['User']);    
            }
		}

		if ($payment !== false) {
			$this->Payment->save($payment);
		}

		return $payment;
	}

	public function hook_succeeded() {
		$this->autoRender = false;

        App::import('Vendor', 'Stripe', array('file' => 'Stripe/init.php'));

		$secrets = Configure::read('secrets');

		$payload = @file_get_contents('php://input');
		$webhook_secret = $secrets['stripe']['hook_succeeded_secret'];
		$signature = $_SERVER['HTTP_STRIPE_SIGNATURE'];

		$message = '';

		$event = null;
		try {
			$event = \Stripe\Webhook::constructEvent($payload, $signature, $webhook_secret);
		} catch(\UnexpectedValueException $e) {
			$message = 'Unexpected value in webhook';
		} catch(\Stripe\Exception\SignatureVerificationException $e) {
			$message = 'Invalid signature in webhook';
		} catch (Error $e) {
			$message = $e->getMessage();
		}
		
		if (!empty($event)) {
			switch ($event->type) {
				case 'payment_intent.succeeded':
					$paymentIntent = $event->data->object;
					$stripe_id = $paymentIntent['id'];

					$this->Payment->contain();
					$payment = $this->Payment->findByStripeId($stripe_id);
                    $payment_id = $payment['Payment']['id'];
                    
					if (!empty($payment)) {
						$payment = $this->checkPayment($payment_id);
					} else $message = 'Payment not found: ' . $stripe_id;

					break;
				default:
					$message = 'Unexpected event type in webhook';
			}	
		}

		if (!empty($message)) {
			$message .= "</p><p>" . json_encode($payload);

			App::uses('Email', 'Lib');

			$secrets = Configure::read('secrets');
			Email::send($secrets['admin_email'], "SmarterMarks: Payment error", "payment_error", array(
				'error_message' => $message
			));
	
			http_response_code(400);
		} else http_response_code(200);
	}

	public function finish($payment_id) {
		$this->autoRender = false;

		$payment = $this->checkPayment($payment_id);

		if (empty($payment)) {
			$this->redirect(array('controller' => 'Payments', 'action' => 'error', $payment_id));
		} else if ($payment['status'] == 'processing') {
			$this->redirect(array('controller' => 'Payments', 'action' => 'processing', $payment_id));
		} else if ($payment['status'] == 'canceled') {
			$this->Flash->success("Payment canceled.");
			$this->redirect(array('controller' => 'Users', 'action' => 'renew_main'));
		} else if ($payment['status'] == 'complete') {
			$this->Flash->success("Payment successful!");
			$this->redirect(array('controller' => 'Payments', 'action' => 'view_receipt', $payment_id));
		} else if ($payment['status'] == 'payment_mismatch') {
			$details = json_decode($payment['details'], true);
			$expected = number_format($details['invoice']['total'] / 100, 2);
			$this->Session->write('payment_error_' . $payment_id, 'The received payment does not match ' .
				'the expected payment of $' . $expected . '.');
			$this->redirect(array('controller' => 'Payments', 'action' => 'error', $payment_id));
		}
	}

	public function finish_invoice($payment_id) {
		$this->autoRender = false;

		$this->Payment->contain();
		$payment = $this->Payment->findById($payment_id);
        
		if (!empty($payment) && ($payment['Payment']['status'] == 'created')) {
			$details = json_decode($payment['Payment']['details'], true);
			$this->Payment->apply($details['purchase'], $payment_id);

			$data = array();
			$data['id'] = $payment_id;
			$data['status'] = 'invoice';
			$this->Payment->save($data);

			$this->Flash->success("Invoice created.");
			$this->redirect(array('controller' => 'Payments', 'action' => 'view_receipt', $payment_id));
		} else {
			$this->Flash->error("Bad payment ID.");
			$this->redirect('/');	
		}
	}

	public function view_receipt($payment_id) {
		$this->set('title_for_layout', 'Your receipt');

		$this->Payment->contain();
		$payment = $this->Payment->findById($payment_id);

		if (!in_array($payment['Payment']['status'], array('complete', 'invoice'))) {
			$this->Flash->error("Payment not complete.");
			$this->redirect_home();
		}

		$details = json_decode($payment['Payment']['details'], true);
		$status = $payment['Payment']['status'];

		$this->loadModel('User');
		$user = $this->User->findById($payment['Payment']['user_id']);
		$email = $user['User']['email'];

		$created = $payment['Payment']['created'];
		
		$this->set(compact('payment_id', 'details', 'created', 'email', 'status'));
	}

	public function print_receipt($payment_id) {
        $this->autoRender = false;
        $this->layout = '';

        $success = true;

		$this->Payment->contain();
		$payment = $this->Payment->findById($payment_id);

		if (!in_array($payment['Payment']['status'], array('complete', 'invoice'))) {
			$this->Flash->error("Payment not complete.");
			$this->redirect_home();
		}

		$details = json_decode($payment['Payment']['details'], true);
		$status = $payment['Payment']['status'];

		$this->loadModel('User');
		$user = $this->User->findById($payment['Payment']['user_id']);
		$email = $user['User']['email'];

		$created = $payment['Payment']['created'];

        $user_defaults = json_decode($this->Auth->user('defaults'), true);

		// Build HTML

        $view = new View($this, false);
        $view->layout = false;
        $view->viewPath='Payments';
		$view->set(compact('payment_id', 'details', 'created', 'email', 'status'));
        $viewHTML = $view->render('print_receipt');

		$page_settings = array(
			'pageSize' => 'letter',
			'marginSizes' => array(
				'top' => 0.5,
				'bottom' => 0.5,
				'left' => 0.5,
				'right' => 0.5
			)
		);

		// Build and return PDF

		$this->html_to_pdf_response($viewHTML, $page_settings);
    }

	public function processing($payment_id) {
		$this->set('title_for_layout', 'Processing payment');

		$this->Payment->contain('User');
		$payment = $this->Payment->findById($payment_id);

		$details = json_decode($payment['Payment']['details'], true);
		$count = 0;
		foreach ($details['purchase']['items'] as $item) {
			$count += count($item['user_ids']);
		}

		$email = $payment['User']['email'];

		$this->set(compact('email', 'count'));
	}

	public function error($payment_id) {
		$this->set('title_for_layout', 'Error processing payment');

		$error_message = $this->Session->consume('payment_error_' . $payment_id);

		App::uses('Email', 'Lib');

		$secrets = Configure::read('secrets');
		Email::send($secrets['admin_email'], "SmarterMarks: Payment error", "payment_error", array(
			'email' => $this->Auth->user('email'), 
			'payment_id' => $payment_id,
			'error_message' => $error_message
		));

		$this->set(compact('error_message'));
	}

	public function create($userID = null) {
		$this->autoRender = false;

		if ($userID == null) $userID = $this->Auth->user('id');

		$secrets = Configure::read('secrets');

		if ($this->Auth->user('role') != 'admin') {
            App::import('Vendor', 'Stripe', array('file' => 'Stripe/init.php'));
			\Stripe\Stripe::setApiKey($secrets['stripe']['secret_key']);
		}

		$resp = array();
		$success = true;

		$itemData = $secrets['billing']['items'];

		$purchase = $this->request->data['purchase'];
		$invoice = array(
			'line_items' => array(),
			'currency' => $secrets['billing']['currency'],
			'tax_name' => $secrets['billing']['tax_name'],
			'tax_rate' => $secrets['billing']['tax_rate']
		);

		$this->loadModel('User');

		$price_total = 0.0;

		$desc_text = "";
		foreach ($purchase['items'] as &$pItem) {
			$needsExpires = ($this->Auth->user('role') != 'admin') || empty($pItem['expires']);
			$needsType = ($this->Auth->user('role') != 'admin') || empty($pItem['type']);

            $prepay_available = 0;

			if (!empty($pItem['group_id'])) {

				$this->loadModel('Group');
				$this->Group->contain();
				$group = $this->Group->findById($pItem['group_id']);
				if (!empty($group)) {
                    $prepay_available = $group['Group']['prepaid_count'];

					if ($needsExpires) {
						$pItem['expires'] = strtotime($group['Group']['expires']);
						$needsExpires = false;
					}
					if ($needsType) {
						$pItem['type'] = $group['Group']['renewal_type'];
						$needsType = false;
					}
				} else $success = false;

			} else if ((array_key_exists('user_ids', $pItem) && (count($pItem['user_ids']) == 1)) && 
                (!array_key_exists('new_prepaid', $pItem) || ($pItem['new_prepaid'] == 0))) {

				$this->User->contain();
				$user = $this->User->findById($pItem['user_ids'][0]);
				$minExpiry = strtotime('-1 month');
				$maxExpiry = strtotime('+1 month');
				$baseExpiry = strtotime($user['User']['expires']);
				if (($baseExpiry < $minExpiry) || ($baseExpiry > $maxExpiry)) $baseExpiry = time();

				if ($needsExpires) {
					$pItem['expires'] = strtotime('+1 year', $baseExpiry);
					$needsExpires = false;
				}
				if ($needsType) {
					$pItem['type'] = 'renewal-individual-1yr';
					$needsType = false;
				}

			}

			// Get unit count

			$todayString = date('Y-m-d H:i:s');

			if (!empty($pItem['user_ids'])) {
				$this->User->contain();
				$users = $this->User->find('all', array(
					'conditions' => array(
						'User.id' => $pItem['user_ids'],
						'User.expires IS NOT NULL'
					)
				));

				$expiries = array();
				foreach ($users as $user) {
					$created_timestamp = strtotime($user['User']['created']);
					$trial_timestamp = strtotime('+6 months', $created_timestamp);
					$expires_timestamp = strtotime($user['User']['expires']);

					if (abs($trial_timestamp - $expires_timestamp) < 10) {

						// User is on a trial account -- renew from today

						$expiries[] = $todayString;

					} else {

						// User has renewed before -- continue from previous renewal

						$expiries[] = date('Y-m-d H:i:s', $expires_timestamp);
					}
				}
			} else $expiries = array();

			if (!empty($pItem['new_users'])) {
				for ($i = 0; $i < count($pItem['new_users']); ++$i) {
					$expiries[] = $todayString;
				}
			}

            if (!empty($pItem['new_prepaid'])) {
				for ($i = 0; $i < $pItem['new_prepaid']; ++$i) {
					$expiries[] = $todayString;
				}
			}

			$milestones = array();
			$thisMilestone = strtotime('-45 days', $pItem['expires']);
			$nowTimestamp = time();
			do {
				$milestones[] = $thisMilestone;
				$thisMilestone = strtotime('-3 months', $thisMilestone);
			} while ($thisMilestone > $nowTimestamp);

            sort($expiries);  // Sort expiries so prepay goes to most expensive first

			$billable_units = 0;
			$prepaid_units = 0;
            $prepaid_count = 0;
			$paid_count = 0;

			foreach ($expiries as $expiry) {
				$expiryTimestamp = strtotime($expiry);
                $this_units = 0.0;
				foreach ($milestones as $milestone) {
					if ($milestone > $expiryTimestamp) {
						$this_units += 0.25;
					}
				}
				if ($this_units > 0) {
                    $paid_count++;
                    if ($prepay_available > 0) {
                        $prepaid_units += $this_units;
                        --$prepay_available;
                        ++$prepaid_count;
                    } else $billable_units += $this_units;
                }
			}

			// Get renewal type if still needed

			if ($needsType) {
				if ($paid_count < 10) $pItem['type'] = 'renewal-individual-1yr';
				else $pItem['type'] = 'renewal-group-1yr';
			}

			$type = $pItem['type'];

			// Get expiry string
			
			$userTimezone = new DateTimeZone($this->Auth->user('timezone'));
			$systemTimezone = new DateTimeZone(date_default_timezone_get());
	
			$expiryTime = new DateTime('@' . $pItem['expires'], $systemTimezone);
			$expiryTime->setTimezone($userTimezone);
			$expiresString = $expiryTime->format('F j, Y');

			// Build invoice details

			if (isset($itemData[$type])) {
				$thisData = $itemData[$type];

				$description_full = $thisData['item_name'] . " (Exp. " . $expiresString . ")";

                if (isset($pItem['group_id']) || isset($pItem['group_name'])) {
                    $numAdded = 0;
                    if (!empty($pItem['user_ids'])) $numAdded += count($pItem['user_ids']);
                    if (!empty($pItem['new_users'])) $numAdded += count($pItem['new_users']);

                    $description_full .= "<br/>" . $numAdded . " user" . (($numAdded == 1) ? '' : 's');
                    if (!empty($pItem['new_prepaid'])) {
                        $description_full .= " and " . $pItem['new_prepaid'] . " prepaid renewal" . 
                            (($pItem['new_prepaid'] == 1) ? '' : 's');
                    }
                }

                if (isset($pItem['group_id'])) {
					
					$group_id = $pItem['group_id'];
	
					$this->loadModel('Group');;
					$this->Group->contain();
					$group = $this->Group->findById($group_id);
					if (!empty($group)) {
                        $description_full .= " added to existing group: " . $group['Group']['name'];

					} else return false;
	
				} else if (isset($pItem['group_name'])) {
	
                    $description_full .= " added to new group: " . $pItem['group_name'];
	
				}

                $total_units = $billable_units + $prepaid_units;

				$desc_html = $thisData['item_name'] . "<br/>Exp. " . $expiresString;
				$desc_text = number_format($total_units, 2) . " × " . $thisData['item_name'] . " (Exp. " . $expiresString . ")";
				$price = $thisData['amount'];

				$invoice['line_items'][] = array(
					'description' => $desc_html,
					'description_full' => $description_full,
					'count' => $total_units,
					'price' => $price
				);
	
				$price_total += $billable_units * $price;

                if ($prepaid_count > 0) {
                    $pItem['prepaid_required'] = $prepaid_count;
                    $invoice['line_items'][] = array(
                        'description' => $prepaid_count . " prepaid credit" . (($prepaid_count == 1) ? '' : 's') . " applied.",
                        'description_full' => $prepaid_count . " prepaid credit" . (($prepaid_count == 1) ? '' : 's') . " applied.",
                        'count' => -$prepaid_units,
                        'price' => $price
                    );
                }

            } else {
				$resp['error'] = "Invalid item";
				$success = false;
			}
		}

		if ($success) {

            $prepaid_total = 0;
            foreach ($purchase['items'] as $item) {
                if (!empty($item['prepaid_required'])) $prepaid_total += $item['prepaid_required'];
            }

            if (($prepaid_total > 0) || ($price_total > 0)) {

                $resp['needs_payment'] = ($price_total > 0);
                if ($prepaid_total > 0) $resp['prepay_used'] = $prepaid_total;

                if (count($purchase['items']) > 1) $desc_text = "Multiple items";

                $price_total = round(100 * $price_total * (1.0 + $secrets['billing']['tax_rate']));
                $invoice['total'] = $price_total;
        
                if (($price_total > 0) && ($this->Auth->user('role') != 'admin')) {
                    try {
                        $paymentIntent = \Stripe\PaymentIntent::create([
                            'amount' => $price_total,
                            'currency' => $secrets['billing']['currency'],
                            'description' => $desc_text
                        ]);
                    } catch (Error $e) {
                        $this->log($e, 'stripe_errors');
                        $success = false;
                        $resp['error'] = $e->getMessage();
                    }
                }

                if ($success) {
                    $resp['invoice'] = $invoice;

                    $data = array();
                    $data['user_id'] = $userID;
                    $data['details'] = json_encode(array(
                        'purchase' => $purchase,
                        'invoice' => $invoice
                    ));
                    $data['status'] = 'created';

                    if (isset($paymentIntent)) {
                        $resp['clientSecret'] = $paymentIntent['client_secret'];
                        $data['stripe_id'] = $paymentIntent['id'];
                    } else {
                        $data['stripe_id'] = null;
                    }

                    $this->Payment->create();
                    $success &= !empty($this->Payment->save($data));
                    $resp['paymentID'] = $this->Payment->id;
                }

            } else {

                $resp['needs_payment'] = false;
                $this->Payment->apply($purchase);
                
            }
        }

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
}


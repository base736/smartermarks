<?php

App::uses('Controller', 'Controller');

// AppController is the "Master" controller. Anything (functions, variables, etc) placed in here are
// accessible by any of the controllers.

class AppController extends Controller {

	// The components used by the entire site
	public $components = array(
//        'Security',
	    'Session',
		'Flash',
        'Auth' => array(
	        'authenticate' => array(
	            'Form' => array(
	                'fields' => array('username' => 'email'),
	                'userModel' => 'User'
	            )
	        )
	    ),
        'RequestHandler',
		'Cookie'
    );
    
	// For gritter
	var $helpers = array('Html'); // Cake 1.3

	public function constructClasses() {
	
	    if (Configure::read('debug') >= 1) {
		    $this->components[] = 'DebugKit.Toolbar';
	    }
	
	    parent::constructClasses();
	}

	// beforeFilter() is ran before every action in the controller. Each controller can have their own before filter
    function beforeFilter() {

		// Set up cookies

		$secrets = Configure::read('secrets');

		$this->Cookie->domain = $_SERVER['SERVER_NAME'];
		$this->Cookie->key = $secrets['cookie']['key'];
    	$this->Cookie->httpOnly = true;
    
    	// Settings for the auth component

		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
		$this->Auth->unauthorizedRedirect = array('controller' => null, 'action' => 'unauthorized');

    	$this->Auth->loginRedirect = array('controller' => null, 'action' => 'index');
    	$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');

    	// The following line tells the Auth Component that each controller is in charge of determining whether or not
    	// the user has permission to view the page or not. By default, unless specified, the user has to be logged in
    	// to access each action

    	$this->Auth->authorize = "Controller";
    	$this->Auth->fields = array('username' => 'email', 'password' => 'password');

		$user = $this->Auth->user();

		if ((!empty($user['id'])) && ($user['role'] != 'admin')) {

			// Redirect if user information is required

			$this->loadModel('User');
			$auth_details = $this->User->get_auth_details($user['email']);

			if ($user['approved'] == 0) {
				if ($this->action != 'awaiting_approval') {
					$this->redirect(array('controller' => 'DefaultPages', 'action' => 'awaiting_approval'));
				}
			} else if (($auth_details['auth_method'] == 'local') && (empty($user['password']) || ($user['entropy'] < 32))) {
				if (!in_array($this->action, array('new_password', 'set_password', 'send_reset'))) {
					$this->loadModel('PasswordReset');
					$token = $this->PasswordReset->getReset($user['id']);
					$this->redirect(array('controller' => 'PasswordResets', 'action' => 'new_password', $token));
				}
			} else if (!empty($user['validation_token'])) {
				if (($this->action != 'awaiting_validation') && ($this->action != 'resend_verification')) {
					$this->redirect(array('controller' => 'DefaultPages', 'action' => 'awaiting_validation'));
				}
			} else if (!array_key_exists('ua_read', $user) || ($user['ua_read'] < 1)) {
				if ($this->action != 'read_agreement') {
					$this->redirect(array('controller' => 'Users', 'action' => 'read_agreement'));
				}
			}
		}

		// Process JSON-encoded requests before Authenticate

		if ($this->RequestHandler->requestedWith('json')) {
			$this->request->data = $this->request->input('json_decode', true);
		}
	}

	// beforeRender() is called before the "View" is rendered
    function beforeRender() {

		$user = $this->Auth->user();

		// Redirect if lack of ES6 support is detected

		if (!in_array(strtolower($this->params['controller']), array('defaultpages', 'users'))) {
			if (isset($_COOKIE['has_import']) && $_COOKIE['has_import'] === 'false') {
				$this->redirect(array('controller' => 'DefaultPages', 'action' => 'update_required'));
			}	
		}

		// Redirect if needed
/*
		if (empty($user['email']) || !in_array($user['email'], array('base736@gmail.com', 'jason@smartermarks.com'))) {
			if (!in_array(strtolower($this->params['controller']), array('defaultpages', 'users'))) {
				$this->redirect(array('controller' => 'DefaultPages', 'action' => 'maintenance'));
			}
		}
*/
    	// If a user is logged in
	    if (!empty($user['id'])) {

	    	// Set user and locked status to be available in the view

			$this->loadModel('User');
			$user_locked = $this->User->isExpired($user['id']) & USER_LOCKED;
			$this->set(compact('user', 'user_locked'));

			// Include group count for this user as well

			$this->loadModel('UserGroup');
			$groupCount = $this->UserGroup->find('count', array(
				'conditions' => array(
					'user_id' => $user['id'],
					'is_admin' => 1,
					'is_active' => 1
				)
			));
	    	$this->set(compact('groupCount'));
		}
	}

	function afterFilter() {

		// Throttle some pages

		if (in_array($this->action, array('home', 'teacher', 'student', 'login', 'ajax_login', 'index', 'join', 'lobby'))) {

			$minTime = 2.0;       // Minimum time between requests in seconds
			$key = session_id();  // Key used to throttle requests
			
			$currentTime = microtime(true);
			$nextRequest = $currentTime;

			$next_string = Cache::read($key, 'next_request');
			if (!empty($next_string)) {
				$next_array = json_decode($next_string, true);
				if (is_array($next_array)) {
					if (!empty($next_array[$this->action])) {
						$nextRequest = max($nextRequest, $next_array[$this->action]);
					}
				} else $next_array = array();
			} else $next_array = array();

			$next_array[$this->action] = $nextRequest + $minTime;
			Cache::write($key, json_encode($next_array), 'next_request');

			$delay = $nextRequest - $currentTime;
			if ($delay > 0.0) usleep($delay * 1e6);	
		}
	}
	
	// isAuthorized is called by the auth component to see if the current user has permission to view the pages.
	// in the controllers we'll manually check each action, but for now, we're give admin access to all.
	public function isAuthorized($user) {
		$this->Session->write('auth_url', $this->request->url);

		if (isset($user['role']) && $user['role'] == 'admin') {
        	return true; //Admin can access every action
	    }

    	return false; // The rest don't
	}

	// Function to force SSL
	public function blackhole($type) {
		if ($_SERVER['SERVER_NAME'] != 'smartermarks.local') {
			if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {

			} else {
				$this->redirect('https://' . $_SERVER['SERVER_NAME'] . $this->here);
			}
		}
	}

	public function html_to_pdf_response($html, $page_settings) {
		$html_filename = "page.html";
		$pdf_filename = "result.pdf";

		$webroot = Configure::read('approot') . 'webroot/';

		do {
			$jobName = uniqid('', true);
		   	$path = "temp/" . $jobName . ".page";
		} while (file_exists($webroot . $path));

		mkdir($webroot . $path);
		chmod($webroot . $path, 0777);

		$handle = fopen($webroot . $path . '/' . $html_filename, 'w');
		fwrite($handle, $html);
		fclose($handle);

		$secrets = Configure::read('secrets');

		$post_data = array(
			'jobName' => $jobName,
			'fileName' => $pdf_filename,
			'contentURL' => $secrets['pdf_server']['internal_host'] . $path . '/' . $html_filename,
			'pageSize' => $page_settings['pageSize'],
			'topMarginSize' => number_format($page_settings['marginSizes']['top'] * 25.4, 2) . 'mm',
			'bottomMarginSize' => number_format($page_settings['marginSizes']['bottom'] * 25.4, 2) . 'mm',
			'leftMarginSize' => number_format($page_settings['marginSizes']['left'] * 25.4, 2) . 'mm',
			'rightMarginSize' => number_format($page_settings['marginSizes']['right'] * 25.4, 2) . 'mm'
		);

		if (array_key_exists('header', $page_settings)) {
			$post_data['header'] = $page_settings['header'];
		}

		$pdf_path = $webroot . 'temp/' . $jobName . '/' . $pdf_filename;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $secrets['pdf_server']['server_url']);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response_json = curl_exec($ch);

		$success = true;
		if ($response_json) {
			$response = json_decode($response_json, true);
			if (!$response['success']) $success = false;	
		} else $success = false;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		if ($success) {

			$pdfData = file_get_contents($pdf_path);

			$this->response->type('pdf');
			$this->response->body($pdfData);
			return $this->response;

		} else {

			$this->response->type('json');
			echo json_encode(array(
				'success' => false
			));
	
		}
	}

	public function updateUserData($newUserData) {
		if (empty($newUserData['id'])) return false;

		$success = true;
		if (!empty($this->User->save($newUserData))) {
			if ($this->Auth->user('id') == $newUserData['id']) {
				$currentUserData = $this->Auth->user();
				$newUserData = array_merge($currentUserData, $newUserData);
				$this->Session->write('Auth.User', $newUserData);
			}
		} else $success = false;
		
		return $success;
	}

	public function redirect_home() {
		/*
		if (empty($this->Session->read('redirect_count'))) $count = 1;
		else $count = $this->Session->read('redirect_count') + 1;
		$this->Session->write('redirect_count', $count);

		if (empty($this->Auth->user('id'))) $userID = 'unknown';
		else $userID = $this->Auth->user('id');
		*/
		$this->redirect('/');
	}
}

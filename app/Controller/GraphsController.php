<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class GraphsController extends AppController {

    public $presetVars = true; // using the model configuration

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return ($this->Auth->user('role') == 'admin');
	    }

		return false;
	}
	
	public function index() {
		ini_set('memory_limit','512M');
        set_time_limit(0);

		$this->loadModel("AdminStatistics");
		$this->loadModel("ResponsePaper");
        $this->loadModel("ResultData");
		$this->loadModel("ResponseOnline");

		$sampleInterval = 86400 * 7;

		$toTime = time();

		// Get cached values if the are any

		$onlineData = array();
		$allData = array();

		$this->AdminStatistics->contain();
		$cachedValues = $this->AdminStatistics->find('all', array(
			'order' => array('AdminStatistics.start')
		));
		
		if (!empty($cachedValues)) {

			$lastEntry = array_pop($cachedValues);
			$startTime = strtotime($lastEntry['AdminStatistics']['start']);
			$working_entry_id = $lastEntry['AdminStatistics']['id'];

			foreach ($cachedValues as $entry) {
				$entryTime = strtotime($entry['AdminStatistics']['start']);
				$entryData = json_decode($entry['AdminStatistics']['json_data'], true);

				$paper_assessments = $entryData['assessments']['paper'];
				$online_assessments = $entryData['assessments']['online'];

				$onlineData[] = array($entryTime * 1000, $online_assessments);
				$allData[] = array($entryTime * 1000, $paper_assessments + $online_assessments);
			}

		} else {

			$this->ResponsePaper->contain();
			$firstRecord = $this->ResponsePaper->find('first', array(
				'order' => array('ResponsePaper.date')
			));

			$dateTime = new DateTime($firstRecord['ResponsePaper']['date']);
            $dateTime->modify('last Sunday');
            $dateTime->setTime(0, 0, 0);
            $startTime = $dateTime->getTimestamp();

			$working_entry_id = false;

		}

		// Get new data

		do {
			$endTime = $startTime + $sampleInterval;

			// Get paper assessment data

			$this->ResponsePaper->contain(array('Score', 'ResultSet'));
			$response_paper_ids = $this->ResponsePaper->find('list', array(
                'fields' => array('ResponsePaper.id'),
				'conditions' => array(
					'ResponsePaper.date >=' => date('Y-m-d H:i:s', $startTime),
					'ResponsePaper.date <' => date('Y-m-d H:i:s', $endTime)
				)
			));
			
			$paper_assessments = 0;
			foreach ($response_paper_ids as $response_paper_id) {
                $this->ResponsePaper->contain(array('Score', 'ResultSet' => 'ResultData'));
                $response_paper = $this->ResponsePaper->findById($response_paper_id);
                $paper_assessments += count($response_paper['ResultSet']['ResultData']);
			}

			// Get online assessment data

			$online_assessments = $this->ResponseOnline->find('count', array(
				'conditions' => array(
					'ResponseOnline.created >=' => date('Y-m-d H:i:s', $startTime),
					'ResponseOnline.created <' => date('Y-m-d H:i:s', $endTime)
				)
			));

			// Store data

			$data = array(
				'start' => date('Y-m-d H:i:s', $startTime),
				'json_data' => json_encode(array(
					'assessments' => array(
						'paper' => $paper_assessments,
						'online' => $online_assessments
					)
				))
			);

			if ($working_entry_id !== false) $data['id'] = $working_entry_id;
			else $this->AdminStatistics->create();

			$this->AdminStatistics->save($data);
			$working_entry_id = false;

			// Get graph data

			$onlineData[] = array($startTime * 1000, $online_assessments);
			$allData[] = array($startTime * 1000, $paper_assessments + $online_assessments);

			$startTime = $endTime;
		} while ($startTime < $toTime);

		// Get total page counts

		$totalOnline = array(array($onlineData[0][0], 0));
		for ($i = 1; $i < count($onlineData); ++$i) {
			$totalOnline[] = array($onlineData[$i][0], $totalOnline[$i - 1][1] + $onlineData[$i][1]);
		}

		$totalAll = array(array($allData[0][0], 0));
		for ($i = 1; $i < count($allData); ++$i) {
			$totalAll[] = array($allData[$i][0], $totalAll[$i - 1][1] + $allData[$i][1]);
		}

		$this->set('historyData', json_encode(array(
			array(
				array(
					'data' => $allData,
					'lines' => array(
						'lineWidth' => 0,
						'fill' => true,
						'fillColor' => "#fbf0d0"
					)
				),
				array(
					'data' => $onlineData,
					'lines' => array(
						'lineWidth' => 0,
						'fill' => true,
						'fillColor' => "#c2cdf0"
					)
				), 
				array(
					'data' => $allData,
					'lines' => array(
						'lineWidth' => 1
					),
					'color' => "#999"
				)
			), 
			array(
				array(
					'data' => $totalAll,
					'lines' => array(
						'lineWidth' => 0,
						'fill' => true,
						'fillColor' => "#fbf0d0"
					)
				),
				array(
					'data' => $totalOnline,
					'lines' => array(
						'lineWidth' => 0,
						'fill' => true,
						'fillColor' => "#c2cdf0"
					)
				), 
				array(
					'data' => $totalAll,
					'lines' => array(
						'lineWidth' => 1
					),
					'color' => "#999"
				)
			)
		)));

	}	
}

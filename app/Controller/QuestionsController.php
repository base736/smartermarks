<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class QuestionsController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();

	    // Tell the auth component that all actions can be accessed without having to be logged in
		$this->Auth->allow('get_statistics', 'version_start', 'will_update_model', 'did_update_model');
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
	    }

		if (in_array($this->action, array('build_waiting', 'preview', 'manage_open'))) {
	    	return true;
	    }

		if (in_array($this->action, array('index', 'empty_trash'))) {
			return !empty($this->Auth->user('id'));
		}

		if (in_array($this->action, array('get_index'))) {
			if ($this->request->query['folder_ids'] === 'open') {
				return true;
			} else if (!empty($this->request->query['folder_ids'])) {
				$this->loadModel('Folder');
				$folder_ids = explode(',', $this->request->query['folder_ids']);
				foreach ($folder_ids as $folder_id) {
					if (is_numeric($folder_id)) {
						$permissions = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
						return in_array('can_view', $permissions['all']);
					} else return false;
				}		
			} else return false;
		}

		if (in_array($this->action, array('get'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$question_id = $this->request->params['pass'][0];
				$permissions = $this->Question->getPermissions($question_id, $this->Auth->user('id'));
				if (!in_array('can_view', $permissions)) return false;
			} else return false;
			
			if (isset($this->request->query['question_handle_id']) && is_numeric($this->request->query['question_handle_id'])) {
				$handleID = $this->request->query['question_handle_id'];
				$this->loadModel("QuestionHandle");
				$this->QuestionHandle->contain();
				$questionHandle = $this->QuestionHandle->findById($handleID);
				if (!empty($questionHandle) && isset($questionHandle['QuestionHandle'])) {
					if ($questionHandle['QuestionHandle']['question_id'] != $question_id) return false;
				}
			}
			
			return true;
		}
		
	    if (in_array($this->action, array('create'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (isset($this->request->data['folder_id']) && is_numeric($this->request->data['folder_id'])) {
				$folderId = $this->request->data['folder_id'];
				$this->loadModel('Folder');
				$permissions = $this->Folder->getContentPermissions($folderId, $this->Auth->user('id'));
				return in_array('can_add', $permissions['all']);
			} else return false;
	    }

		if (in_array($this->action, array('delete'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$question_id = $this->request->params['pass'][0];

				$this->loadModel('QuestionHandle');
				$handle_ids = $this->QuestionHandle->find('list', array(
					'fields' => 'QuestionHandle.id',
					'conditions' => array('QuestionHandle.question_id' => $question_id)
				));
		
				if (count($handle_ids) == 1) {
					$question_handle_id = array_pop($handle_ids);
					$handlePermissions = $this->QuestionHandle->getPermissions($question_handle_id, $this->Auth->user('id'));
					return in_array('can_delete', $handlePermissions);
				} else return false;
			} else return false;
		}

		if (in_array($this->action, array('save'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$question_id = $this->request->params['pass'][0];
				if (isset($this->request->data['question_handle_id'])) {
					$this->loadModel('QuestionHandle');

					$question_handle_id = $this->request->data['question_handle_id'];
					$this->QuestionHandle->contain();
					$questionHandle = $this->QuestionHandle->findById($question_handle_id);
					if (!empty($questionHandle)) {
						if ($questionHandle['QuestionHandle']['question_id'] != $question_id) return false;
					} else return false;

					$permissions = $this->QuestionHandle->getPermissions($question_handle_id, $this->Auth->user('id'));
					return in_array('can_edit', $permissions);
	
				} else return false;				
			} else return false;
		}
		
        if (in_array($this->action, array('get_version_status'))) {
			if (isset($this->request->params['pass'][0])) {
                $requestID = $this->request->params['pass'][0];
                $cache_data = Cache::read($requestID, 'completion_jobs');
                if (!empty($cache_data)) {
                    return ($cache_data['user_id'] == $this->Auth->user('id'));
                } else return false;
            }
        }

		if (in_array($this->action, array('update_open_bank'))) {
			if (isset($this->request->data['folder_id'])) {
				if (is_numeric($this->request->data['folder_id'])) {
					$this->loadModel('Folder');
					$folderId = $this->request->data['folder_id'];
					$permissions = $this->Folder->getContentPermissions($folderId, $this->Auth->user('id'));
					return in_array('can_view', $permissions['all']);
				} else if ($this->request->data['folder_id'] == 'open') {
					return true;
				} else return false;
			} else return false;
		}

		if (in_array($this->action, array('build'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$question_id = $this->request->params['pass'][0];
				$question_handle_id = false;
				
				$this->loadModel('QuestionHandle');
				if (isset($this->request->query['a'])) {

					$assessment_id = $this->request->query['a'];

					$this->loadModel('AssessmentItem');
					$this->AssessmentItem->contain('QuestionHandle');
					$assessment_items = $this->AssessmentItem->find('all', array(
						'conditions' => array('AssessmentItem.assessment_id' => $assessment_id)
					));

					foreach ($assessment_items as $item) {
						if ($item['QuestionHandle']['question_id'] == $question_id) {
							$question_handle_id = $item['QuestionHandle']['id'];
							break;
						}
					}
					if ($question_handle_id === false) return false;

				}
                
                if (isset($this->request->query['u'])) {

					$this->loadModel('UserQuestion');
					$this->UserQuestion->contain('QuestionHandle');
					$userQuestion = $this->UserQuestion->findById($this->request->query['u']);
					if (!empty($userQuestion)) {
                        if ($question_handle_id === false) $question_handle_id = $userQuestion['QuestionHandle']['id'];
                        else if ($question_handle_id != $userQuestion['QuestionHandle']['id']) return false;
                        
						if ($question_id != $userQuestion['QuestionHandle']['question_id']) return false;
					} else return false;

				}
                
                if (isset($this->request->query['h'])) {

					$question_handle_id = $this->request->query['h'];
					
					$this->QuestionHandle->contain();
					$questionHandle = $this->QuestionHandle->findById($question_handle_id);
					if (!empty($questionHandle)) {
						if ($question_id != $questionHandle['QuestionHandle']['question_id']) return false;
					} else return false;

				}

				if ($question_handle_id !== false) {
					$permissions = $this->QuestionHandle->getPermissions($question_handle_id, $this->Auth->user('id'));
				} else $permissions = $this->Question->getPermissions($question_id, $this->Auth->user('id'));

				if (in_array('can_view', $permissions)) return true;
				else {
					$this->Flash->error("Invalid or expired question");
					$this->redirect_home();
				}
			} else return false;
		}

		return false;
	}

	// Index interface for questions

	public function index() {
		$adminView = ($this->Auth->user('role') == 'admin');

		if ($adminView) {
			
			$jsonTree = "";
			$nodes = array();
			$receivedCount = 0;

		} else {

			$this->loadModel('Folder');
			$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'questions');
			$nodes[] = array(
				'id' => md5('open'),
				'data' => array(
					'parent_id' => null,
					'folder_id' => 'open',
					'content_permissions' => array('all' => array('can_view'), 'owner' => array('can_view')),
					'folder_permissions' => array('can_view')
				),
				'text' => 'Open bank',
				'type' => 'open'
			);
			$tree = $this->Folder->getTree($nodes);
			$jsonTree = json_encode($tree);

			$search_folders = array();
			foreach ($nodes as $node) {
				if (is_numeric($node['data']['folder_id'])) $search_folders[] = $node['data']['folder_id'];
			}

			// Get unshared question count

			$this->loadModel('UserQuestion');
			$conditions = 'u.folder_id IN (' . implode(',', $search_folders) . ') AND u.deleted IS NULL';
			$result = $this->UserQuestion->query('
				SELECT COUNT(DISTINCT(q.id)) FROM user_questions u
					JOIN question_handles h ON (h.id = u.question_handle_id)
					JOIN questions q ON (q.id = h.question_id)
				WHERE ' . $conditions . ' AND q.creator_id = ' . $this->Auth->user('id') . ' AND q.open_status = 0'
			);
			$unshared_count = $result[0][0]['COUNT(DISTINCT(q.id))'];
			$this->set(compact('unshared_count'));

			// Find any questions that have been sent to this user (is_new == 3)

			$receivedCount = $this->UserQuestion->find('count', array(
				'conditions' => array(
					'UserQuestion.user_id' => $this->Auth->user('id'),
					'UserQuestion.is_new' => 3
				)
			));
		}

		$this->loadModel('User');
		$isExpired = $this->User->isExpired($this->Auth->user('id'));

		// Get contacts

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$this->User->contain();
		$contactRecords = $this->User->find('all', array(
			'conditions' => array('User.id' => $contact_ids))
		);
		$contacts = array();
		foreach ($contact_ids as $id) {
			foreach ($contactRecords as $record) {
				if ($record['User']['id'] == $id) {
					$contacts[] = array(
						'id' => $record['User']['id'],
						'name' => $record['User']['name'],
						'email' => $record['User']['email']
					);
				}
			}
		}

		// Update banner count and get number of open questions
		
		$this->loadModel('User');
		
		$newUserData = array();
		$newUserData['id'] = $this->Auth->user('id');
		$newUserData['banner_count'] = $this->Auth->user('banner_count') + 1;
		$this->updateUserData($newUserData);

		$numOpen = $this->Question->find('count', array(
			'conditions' => array('Question.open_status !=' => 0)
		));

		// Set variables for template
		
		$this->set(compact('adminView', 'jsonTree', 'isExpired', 'receivedCount', 'contacts', 'numOpen'));

		$this->set('itemType', 'Question');
		$this->set('index_element', '../Questions/index');

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		$this->viewPath = 'Elements';
		$this->render('index_common');
	}

	public function get_index() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$page_params = array();
		if (isset($this->request->query['limit'])) {
			if (is_numeric($this->request->query['limit']) && ($this->request->query['limit'] > 0)) {
				$page_params['limit'] = intval($this->request->query['limit']);
			} else if ($this->request->query['limit'] !== 'all') {
                $success = false;
            }
		} else $page_params['limit'] = 10;

		if (isset($this->request->query['page'])) {
			if (is_numeric($this->request->query['page']) && ($this->request->query['page'] > 0)) {
				$page_params['page'] = intval($this->request->query['page']);
			} else $success = false;
		} else $page_params['page'] = 1;

		$query_params = array('conditions' => array());

		$results = array();
		if (array_key_exists('folder_ids', $this->request->query) && ($this->request->query['folder_ids'] == 'open')) {

			$query_params['conditions'] = array(
				'Question.open_status !=' => 0,
				'Question.name !=' => ''
			);

			$count = $this->Question->find('count', $query_params);
			$resp['count_total'] = $count;

			if (!empty($this->request->query['search'])) {
                $search_text = $this->request->query['search'];

                $search_results = $this->Question->find_weaviate($search_text, array(
                    'folder_id' => 'open',
                    'offset' => ($page_params['page'] - 1) * $page_params['limit'],
                    'limit' => $page_params['limit']
                ));

                if ($search_results !== false) {
                    $query_params['limit'] = 1;
                    $query_params['order'] = 'Question.stats_discrimination DESC';
    
                    foreach ($search_results as $question_content_id => $part_content_id) {
    
                        $query_params['conditions']['Question.question_content_id'] = $question_content_id;
    
                        $this->Question->contain();
                        $this_results = $this->Question->find('all', $query_params);
                        if (!empty($this_results)) {
                            $results[] = $this_results[0];
                        }
                    }
                } else $success = false;
            }

		} else {

			$this->loadModel('UserQuestion');

            $query_params['conditions'] = array('UserQuestion.deleted' => null);
			if (array_key_exists('folder_ids', $this->request->query)) {

				$folder_ids = explode(',', $this->request->query['folder_ids']);
				$query_params['conditions']['UserQuestion.folder_id'] = $folder_ids;

                $count = $this->UserQuestion->find('count', $query_params);
                $resp['count_total'] = $count;
                $resp['count_matching'] = $count;

            } else if ($this->Auth->user('role') == 'admin') {

				$query_params['conditions'][] = ['UserQuestion.folder_id IS NOT NULL'];
                
			} else $success = false;

			$userDefaults = json_decode($this->Auth->user('defaults'), true);

			if (isset($this->request->query['sort'])) {
				$sortBy = $this->request->query['sort'];
			} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
				$sortBy = 'QuestionHandle.name_alias';
			} else {
				$sortBy = 'UserQuestion.moved';
			}

			if (isset($this->request->query['direction'])) {
				$direction = $this->request->query['direction'];
			} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
				$direction = 'asc';
			} else {
				$direction = 'desc';
			}

			$query_params['order'] = array($sortBy => $direction);

			$query_params['contain'] = array('QuestionHandle' => 'Question');
			if ($this->Auth->user('role') == 'admin') {
				$query_params['contain'][] = 'User';
			}

            // Explicitly join questions table to allow search below

            $query_params['joins'] = array(
                array(
                    'table' => 'questions',
                    'alias' => 'Question',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Question.id = QuestionHandle.question_id'
                    )
                )
            );

            if (!empty($this->request->query['search'])) {
				$query_params['conditions'][] = array('OR' => array(
					'QuestionHandle.name_alias LIKE' => '%' . $this->request->query['search'] . '%',
					'Question.id' => $this->request->query['search']
				));
                $count = $this->UserQuestion->find('count', $query_params);
                $resp['count_matching'] = $count;
            }

			$query_params = array_merge($query_params, $page_params);
			$results = $this->UserQuestion->find('all', $query_params);

			foreach ($results as &$pResult) {
				$pResult['Question'] = $pResult['QuestionHandle']['Question'];
				unset($pResult['QuestionHandle']['Question']);
			}

			if ($this->Auth->user('role') != 'admin') {

				// Get permissions for each result

				$folder_permissions = array();
				foreach ($results as &$pResult) {
					$folder_id = $pResult['UserQuestion']['folder_id'];
					if (!array_key_exists($folder_id, $folder_permissions)) {
						$folder_permissions[$folder_id] = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
					}

					if ($pResult['UserQuestion']['user_id'] == $this->Auth->user('id')) {
						$pResult['Permissions'] = $folder_permissions[$folder_id]['owner'];
					} else $pResult['Permissions'] = $folder_permissions[$folder_id]['all'];
				}

				// Update "is_new" for items

				$visible = array();
				foreach ($results as $result) {
					$visible[] = $result['UserQuestion']['id'];
				}
				$this->UserQuestion->updateAll(
					array('UserQuestion.is_new' => 0),
					array('UserQuestion.id' => $visible)
				);
				$this->UserQuestion->updateAll(
					array('UserQuestion.is_new' => 2),
					array(
						'UserQuestion.user_id' => $this->Auth->user('id'),
						'UserQuestion.is_new' => 3
					)
				);
			}

		}

		foreach ($results as &$pResult) {
			$questionType = null;
			$json_data = json_decode($pResult['Question']['json_data'], true);
			foreach ($json_data['Parts'] as $part) {
				if ($part['type'] != 'context') {
					$thisType = substr($part['type'], 0, 2);
					if ($questionType === null) $questionType = $thisType;
					else if ($questionType !== $thisType) $questionType = 'mixed';
				}
			}	
			$pResult['question_type'] = $questionType;
		}
		$resp['results'] = $results;

		if (!empty($query_params['page'])) {
			$resp['page'] = $query_params['page'];
		}
		if (!empty($query_params['limit'])) {
			$resp['limit'] = $query_params['limit'];
		}
		
		$resp['success'] = $success;

		echo json_encode($resp);
	}
	
	public function empty_trash() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;

		$this->loadModel('Folder');
		$folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'questions', 'trash');

		$this->loadModel('UserQuestion');
		$this->UserQuestion->contain(array('QuestionHandle'));
		$trashQuestions = $this->UserQuestion->find('list', array(
			'fields' => array('UserQuestion.id', 'QuestionHandle.question_id'),
			'conditions' => array('UserQuestion.folder_id' => $folder_id)
		));
		
		if (count($trashQuestions) > 0) {
			$db = $this->UserQuestion->getDataSource();
			$this->UserQuestion->updateAll(
				array('UserQuestion.deleted' => $db->value(date('Y-m-d H:i:s'), 'string')),
				array('UserQuestion.id' => array_keys($trashQuestions))
			);

            $this->Question->updateAll(
                array('needs_index' => $db->value(date('Y-m-d H:i:s'), 'string')),
                array('Question.id' => $trashQuestions)
            );
		}

		echo json_encode(
			array(
		   		'success' => $success
		   	)
		);
	}
	
	public function create() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$this->loadModel('User');
		if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {

			$resp['message'] = 'Your account has expired. Unable to create new questions.';
			$success = false;
			
		} else {

			$folder_id = $this->request->data['folder_id'];

			if (array_key_exists('name', $this->request->data)) $save_name = $this->request->data['name'];
			else $save_name = "New Question";

			if (array_key_exists('json_data', $this->request->data)) {

				$json_data = json_decode($this->request->data['json_data'], true);

			} else {

				$json_data = array(
					'Settings' => array(
						'shuffleParts' => false
					),
					'Parts' => array(
						array(
							'id' => bin2hex(random_bytes(4)),
							'type' => 'mc',
							'template' => '',
							'Answers' => array(),
							'ScoredResponses' => array(),
							'TypeDetails' => array(
								'shuffleAnswers' => true
							)
						)
					),
					'Engine' => array(
						'type' => 'wizard',
						'Imports' => array(1),
						'js' => ''
					)
				);

			}

			$questionData = array(
				'creator_id' => $this->Auth->user('id'),
				'name' => $save_name,
				'search_text' => '',
				'last_used' => '',
				'json_data' => json_encode($json_data)
			);

			if (array_key_exists('parent_id', $this->request->data)) {
				$this->Question->contain();
				$parent = $this->Question->findById($this->request->data['parent_id']);
				if (!empty($parent)) {
					$questionData['parent_id'] = $this->request->data['parent_id'];	
					$questionData['open_status'] = $parent['Question']['open_status'];		
				}
			}
            
            if (!array_key_exists('open_status', $questionData)) {
                if ($this->Auth->user('open_created') == 1) {
                    $questionData['open_status'] = $this->Auth->user('id');
                } else $questionData['open_status'] = 0;    
            }

			if ($questionData['open_status'] != 0) {
				$questionData['opened'] = date('Y-m-d H:i:s');
			}

			$this->Question->create();
			if (!empty($this->Question->save($questionData))) {

				$question_id = $this->Question->id;
		
				$handleData = array(
					'question_id' => $question_id,
					'name_alias' => $questionData['name']
				);
				
				$this->loadModel('Folder');
				$targetCommunity = $this->Folder->getCommunityId($folder_id);
				if ($targetCommunity) $handleData['community_id'] = $targetCommunity;
				else $handleData['user_id'] = $this->Auth->user('id');

				$this->loadModel('QuestionHandle');
				$question_handle_id = $this->QuestionHandle->createHandle($handleData, false);
				$response['question_handle_id'] = $question_handle_id;

				$userQuestionData = array(
					'user_id' => $this->Auth->user('id'),
					'folder_id' => $folder_id,
					'question_handle_id' => $question_handle_id,
					'moved' => date('Y-m-d H:i:s'),
					'is_new' => 1
				);
				
				$this->loadModel('UserQuestion');
				$this->UserQuestion->create();
				$success &= !empty($this->UserQuestion->save($userQuestionData));

			} else $success = false;
			
		}

		if ($success) {
			$resp['id'] = $question_id;
			$resp['question_handle_id'] = $question_handle_id;
			$resp['user_question_id'] = intval($this->UserQuestion->id);

			$version_id = md5(microtime() . $question_id);
			Cache::write('questions_' . $question_id, $version_id, 'version_ids');
			$resp['version_id'] = $version_id;

			$resp['Question'] = $this->Question->get($question_id);
			$resp['Statistics'] = $this->Question->getStatistics($question_id);

		}
			
		$resp['success'] = $success;

		echo json_encode($resp);
	}
	
	public function delete($question_id) {
		$this->autoRender = false;
		$this->layout = '';
		
		$resp = array();
		$success = true;

		$success &= $this->Question->delete($question_id);

		$resp['success'] = $success;

        echo json_encode($resp);
	}
	
	// build interface

	public function build_waiting() {
	}

	public function build($question_id) {
		$this->loadModel('AssessmentItem');
		$this->loadModel('UserQuestion');
		$this->loadModel('QuestionHandle');

		// Get a QuestionHandle ID if possible

		if (isset($this->request->query['a'])) {

			$assessment_id = $this->request->query['a'];

			$this->loadModel('AssessmentItem');
			$this->AssessmentItem->contain('QuestionHandle');
			$assessment_items = $this->AssessmentItem->find('all', array(
				'conditions' => array('AssessmentItem.assessment_id' => $assessment_id)
			));

			foreach ($assessment_items as $item) {
				if ($item['QuestionHandle']['question_id'] == $question_id) {
					$question_handle_id = $item['QuestionHandle']['id'];
					break;
				}
			}

			$this->loadModel('Assessment');
			$this->loadModel('Folder');

			$this->Assessment->contain();
			$assessment = $this->Assessment->findById($assessment_id);
			$folder_id = $assessment['Assessment']['folder_id'];
			$communityId = $this->Folder->getCommunityId($folder_id);

		}
        
        if (isset($this->request->query['u'])) {

			$user_question_id = $this->request->query['u'];

			$this->UserQuestion->contain();
			$userQuestion = $this->UserQuestion->findById($user_question_id);
			$question_handle_id = $userQuestion['UserQuestion']['question_handle_id'];

		}
        
        if (isset($this->request->query['h'])) {

			$question_handle_id = $this->request->query['h'];

		}

		if (isset($question_handle_id)) {
			$this->set(compact('question_handle_id'));

			$this->AssessmentItem->contain();
			$assessment_item_count = $this->AssessmentItem->find('count', array(
				'conditions' => array('AssessmentItem.question_handle_id' => $question_handle_id)
			));
		} else $assessment_item_count = 0;
		
		$this->set(compact('assessment_item_count'));

		// Get permissions

		$canCancel = 0;
		$showSettings = 0;
		$folder_id = null;
		if (isset($user_question_id)) {
			$this->loadModel('UserQuestion');
			$this->loadModel('Folder');

			$this->UserQuestion->contain();
			$userQuestion = $this->UserQuestion->findById($user_question_id);

			if ($userQuestion) {
				if ($userQuestion['UserQuestion']['is_new'] == 1) {
					$canCancel = 1;
				}
				if ($userQuestion['UserQuestion']['is_new'] > 0) {
					$userQuestion['UserQuestion']['is_new'] = 0;
					$this->UserQuestion->save($userQuestion);
					$showSettings = 1;
				}

				if (!empty($userQuestion['UserQuestion']['folder_id'])) {
					$folder_id = $userQuestion['UserQuestion']['folder_id'];
				} else {
					$folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'questions', 'home');
				}
			}
		}

		if ($this->Auth->user('role') == 'admin') {

			$canEdit = 1;
			$canCancel = 0;
			$showSettings = 0;

		} else if (isset($question_handle_id)) {

			if (isset($this->request->query['a'])) {

				// Get folder tree stuff

				$this->loadModel('Folder');
				$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'questions');
				$nodes[] = array(
					'id' => 'open',
					'data' => array(
						'parent_id' => null,
						'folder_id' => 'open'
					),
					'text' => 'Open bank',
					'type' => 'open'
				);
				$tree = $this->Folder->getTree($nodes);
				$jsonTree = json_encode($tree);
				
				if ($communityId == null) {
					$questionFolderId = $this->Folder->getUserFolder($this->Auth->user('id'), 'questions', 'home');
				} else {
					$this->loadModel('Community');
					$this->Community->contain();
					$community = $this->Community->findById($communityId);
					$community_ids = json_decode($community['Community']['folder_ids'], true);
					$questionFolderId = $community_ids['questions'];
				}
		
				$this->set(compact('jsonTree', 'communityId', 'questionFolderId'));
			}

			// Get permissions

			$permissions = $this->QuestionHandle->getPermissions($question_handle_id, $this->Auth->user('id'));
			$canEdit = in_array('can_edit', $permissions) ? 1 : 0;

			$this->loadModel('User');
			if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {
				$canEdit = 0;
			}
		} else $canEdit = 0;

		$this->set(compact('canEdit', 'canCancel', 'showSettings', 'folder_id'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}
	
	public function save($question_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$success = true;
	   	$resp = array();

		if (array_key_exists('version_id', $this->request->data)) {
			$sent_version_id = $this->request->data['version_id'];
			$cache_version_id = Cache::read('questions_' . $question_id, 'version_ids');
			if (($cache_version_id === false) || ($sent_version_id == $cache_version_id)) {
				$version_id = md5(microtime() . $question_id);
				Cache::write('questions_' . $question_id, $version_id, 'version_ids');	
				$resp['version_id'] = $version_id;
			} else {
				$resp['error_type'] = 'version_conflict';
				$success = false;
			}
		}

		$has_settings = !empty($this->request->data['settings']);

		$has_json_data = !empty($this->request->data['data_string']);
		if ($has_json_data) {
			$dataString = $this->request->data['data_string'];

			if (empty($this->request->data['data_hash'])) $success = false;
			else if ($this->request->data['data_hash'] != md5($dataString)) $success = false;
			if ($success) $new_data = json_decode($dataString, true);
		}

		if (!$has_settings && !$has_json_data) $success = false;

		if ($success) {
			$this->loadModel('QuestionHandle');

			$question_handle_id = $this->request->data['question_handle_id'];
	
			$this->QuestionHandle->contain('Question');
			$questionHandle = $this->QuestionHandle->findById($question_handle_id);
			if (empty($questionHandle)) $success = false;	
		}

		if ($success) {
			$question_data = array();

			if ($has_settings) {
				if (array_key_exists('name', $this->request->data['settings'])) {
					$this->QuestionHandle->id = $question_handle_id;
					$this->QuestionHandle->saveField('name_alias', $this->request->data['settings']['name']);

					if ($questionHandle['Question']['creator_id'] == $this->Auth->user('id')) {
						$question_data['name'] = $this->request->data['settings']['name'];
					}
				}

				if (array_key_exists('open_status', $this->request->data['settings'])) {
					if (($questionHandle['Question']['open_status'] == 0) || 
						($questionHandle['Question']['open_status'] == $this->Auth->user('id'))) {

						$question_data['open_status'] = $this->request->data['settings']['open_status'];
						if ($question_data['open_status'] == 0) $question_data['opened'] = null;
						else $question_data['opened'] = date('Y-m-d H:i:s');
					}
				}
			}

			if ($has_json_data) {

				if (!empty($new_data['Engine'])) {
					if ($new_data['Engine']['type'] == 'wizard') {
						
						// Rebuild code if we're using a wizard

						App::uses('Wizard', 'Lib');

						if (array_key_exists('data', $new_data['Engine'])) {
							$this->loadModel('JsProgram');
							$wizardData = $new_data['Engine']['data'];
							$new_data['Engine']['js'] = Wizard::getCode($wizardData);
                            $resp['engine_js'] = $new_data['Engine']['js'];
						} else $new_data['Engine']['js'] = '';
					}

					if (!empty($new_data['Engine']['Imports'])) {

						// Translate imports to stored form

						$newImports = array();
						foreach ($new_data['Engine']['Imports'] as $importID => $js) {
							$newImports[] = $importID;
						}
						$new_data['Engine']['Imports'] = $newImports;	
					}
				}

				$old_json_string = $questionHandle['Question']['json_data'];
				$new_json_string = json_encode($new_data);
				if ($new_json_string != $old_json_string) {
					$new_question_id = $this->Question->getModifiable($question_handle_id);
					if ($new_question_id !== false) {

                        if ($new_question_id != $question_id) {
                            $version_id = md5(microtime() . $new_question_id);
                            Cache::write('questions_' . $new_question_id, $version_id, 'version_ids');	
                            $resp['version_id'] = $version_id;
                        }

                        $question_id = $new_question_id;
						$question_data['json_data'] = $new_json_string;

                    } else $success = false;
				}
			}

			if ($success && !empty($question_data)) {
				$question_data['id'] = $question_id;
				$success &= !empty($this->Question->save($question_data));
			}
		}

        $resp['id'] = $question_id;
        $resp['Statistics'] = $this->Question->getStatistics($question_id);
        $resp['success'] = $success;

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function get($question_id) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$question_data = $this->Question->get($question_id);
		if ($question_data !== false) {
			$resp['Question'] = $question_data;

			if (!empty($this->request->query['question_handle_id'])) {
				$question_handle_id = intval($this->request->query['question_handle_id']);

				$this->loadModel('QuestionHandle');
				$this->QuestionHandle->contain();
				$handle = $this->QuestionHandle->findById($question_handle_id);
				$resp['Question']['name'] = $handle['QuestionHandle']['name_alias'];
				$resp['Question']['question_handle_id'] = $question_handle_id;
			}
		} else $success = false;

		$version_id = Cache::read('questions_' . $question_id, 'version_ids');
		if ($version_id === false) {
			$version_id = md5(microtime() . $question_id);
			Cache::write('questions_' . $question_id, $version_id, 'version_ids');	
		}
		$resp['Question']['version_id'] = $version_id;

		$stats_data = $this->Question->getStatistics($question_id);
		if ($stats_data !== false) $resp['Statistics'] = $stats_data;
		else $success = false;

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function preview($question_id = null) {
		$this->Question->contain();
		$question = $this->Question->findById($question_id);

		if (!empty($question)) {
            $questionData = json_decode($question['Question']['json_data'], true);

			if (!empty($questionData['Engine']['Imports'])) {
				$import_ids = $questionData['Engine']['Imports'];
				$questionData['Engine']['Imports'] = $this->Question->expandImports($import_ids);
			}

			$this->set(compact('questionData'));
		} else {
			$this->Flash->error("Invalid question");
			$this->redirect_home();
		}
	}

	public function get_statistics() {
		$this->autoRender = false;
		$this->layout = '';
		
		$results = array();

		if (!empty($this->request->query['question_ids'])) {	
			$question_ids = explode(',', $this->request->query['question_ids']);
			$conditions = array();

			if (array_key_exists('included_sources', $this->request->query)) {
				$conditions['ResultSet.source_model'] = $this->request->query['included_sources'];
			}

			$hasFilters = false;

			if (array_key_exists('filter_minDate', $this->request->query)) {
				$hasFilters = true;
				$conditions['ResultSet.created >'] = $this->request->query['filter_minDate'];
			}
			if (array_key_exists('filter_user_id', $this->request->query)) {
				$hasFilters = true;
				$user_id = $this->request->query['filter_user_id'];
				$conditions['ResultSet.user_id'] = $user_id;
			}

			$this->loadModel('Assessment');
			if (array_key_exists('filter_sitting_id', $this->request->query)) {
				$hasFilters = true;

				$this->loadModel('Sitting');
				$this->Sitting->contain();
				$sitting = $this->Sitting->findById($this->request->query['filter_sitting_id']);
				if ($sitting) $conditions['ResultSet.id'] = $sitting['Sitting']['result_set_id'];
			}
			if (array_key_exists('filter_document_id', $this->request->query)) {
				$hasFilters = true;
				$document_id = $this->request->query['filter_document_id'];

                $this->loadModel('Document');
                $this->Document->contain();
                $document = $this->Document->findByid($document_id);
                $version_data_id = $document['Document']['version_data_id'];

                $this->loadModel('ResultData');
				$result_set_ids = $this->ResultData->find('list', array(
					'fields' => array('ResultData.result_set_id'),
					'conditions' => array('ResultData.version_data_id' => $version_data_id)
				));
				if (!empty($result_set_ids)) {
                    $conditions['ResultSet.id'] = array_values(array_unique($result_set_ids));
                }
			}

			if (!$hasFilters) {
				$conditions['ResultSet.alpha >'] = 0.0;
				$conditions['ResultSet.average_score >'] = 0.3;
				$conditions['ResultSet.average_score <'] = 0.9;
			}

			$results = $this->Question->getStatistics($question_ids, $conditions);
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($results);
	}

    public function version_start() {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$success = true;
	   	$resp = array();
        
        // Check that the user hasn't reached their limit yet

        $this->loadModel('User');
        $user_id = $this->Auth->user('id');
        $user = $this->User->findById($user_id);
        if (!empty($user)) {

            if (!empty($user['User']['smart_usage'])) {
                $smart_usage = json_decode($user['User']['smart_usage'], true);
            } else $smart_usage = array();

            $count = 0;
            $date_cutoff = strtotime("-1 month");
            foreach ($smart_usage as $index => $entry) {
                $date = strtotime($entry['date']);
                if ($date < $date_cutoff) unset($smart_usage[$index]);
                else ++$count;
            }

            if ($count < 1000) {

                $smart_usage[] = array(
                    'type' => 'smart_version',
                    'date' => date('Y-m-d H:i:s')
                );

                $this->updateUserData(array(
                    'id' => $user_id,
                    'smart_usage' => json_encode($smart_usage)
                ));

                $resp['smart_used'] = ++$count;

            } else {
				$resp['error_type'] = 'quota_exceeded';
                $success = false;
            }

        } else $success = false;

        if ($success) {

            if (!empty($this->request->data['request'])) {
                $request = $this->request->data['request'];
            } else $success = false;

            if (!empty($this->request->data['data_string'])) {
                $json_data = json_decode($this->request->data['data_string'], true);
                if (empty($json_data)) $success = false;
            } else $sucess = false;

        }

        // Start shell job for completion

        if ($success) {

			$request_id = uniqid();

            Cache::write($request_id, array(
                'status' => 'pending',
                'user_id' => $this->Auth->user('id'),
                'request' => $request,
                'json_data' => $json_data
            ), 'completion_jobs');

            chdir(Configure::read('approot'));
            putenv('CAKE_APPROOT=' . Configure::read('approot'));
            exec("Console/cake runCompletion submit " . $request_id . " > /dev/null 2>&1 &");
            
            $resp['request_id'] = $request_id;
        }

        $resp['success'] = $success;

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

    public function get_version_status($request_id) {
		$this->autoRender = false;
	   	$this->layout = '';

        $cache_data = Cache::read($request_id, 'completion_jobs');
        if (!empty($cache_data)) $resp = $cache_data;
        else $resp = array('status' => 'not_found');

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
    }

	public function manage_open() {
		$this->loadModel('Folder');
		$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'questions');
		$tree = $this->Folder->getTree($nodes);
		$jsonTree = json_encode($tree);

		$open_created = $this->Auth->user('open_created');

		$search_folders = array();
		foreach ($nodes as $node) $search_folders[] = $node['data']['folder_id'];

		$conditions = 'u.folder_id IN (' . implode(',', $search_folders) . ') AND u.deleted IS NULL';

		$result = $this->Question->query('
			SELECT COUNT(DISTINCT(q.id)) FROM user_questions u
				JOIN question_handles h ON (h.id = u.question_handle_id)
				JOIN questions q ON (q.id = h.question_id)
			WHERE ' . $conditions . ' AND q.creator_id = ' . $this->Auth->user('id') . ' AND q.open_status = 0'
		);
		$private_count = $result[0][0]['COUNT(DISTINCT(q.id))'];

		$this->set(compact('open_created', 'jsonTree', 'private_count'));
	}

	public function update_open_bank() {
		$this->autoRender = false;
		$this->layout = '';

		$folder_id = $this->request->data['folder_id'];
		$new_policy = $this->request->data['new_policy'];
		$created_only = $this->request->data['created_only'];
		
		$resp = $this->request->data;
		$success = true;
		
		$this->loadModel('Folder');
		$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'questions');
		$search_folders = array();
		$edge_folders = array($folder_id);
		do {
			$search_folders = array_merge($search_folders, $edge_folders);
			$new_folders = array();
			foreach ($nodes as $node) {
				if (in_array($node['data']['parent_id'], $edge_folders)) {
					$new_folders[] = $node['data']['folder_id'];
				}
			}
			$edge_folders = $new_folders;
		} while (count($edge_folders) > 0);

		$conditions = 'u.folder_id IN (' . implode(',', $search_folders) . ') AND u.deleted IS NULL';

		if ($new_policy == 'private') {
			$result = $this->Question->query('
				SELECT COUNT(*) FROM user_questions u
					JOIN question_handles h ON (h.id = u.question_handle_id)
					JOIN questions q ON (q.id = h.question_id)
				WHERE ' . $conditions . ' AND q.open_status NOT IN (0, ' . $this->Auth->user('id') . ')'
			);
			$resp['skipped'] = $result[0][0]['COUNT(*)'];
		}
		
		if ($new_policy == 'open') {
			$conditions .= ' AND q.open_status = 0';
			$fields = 'q.open_status = ' . $this->Auth->user('id') . ', ' . 
                'q.opened = "' . date('Y-m-d H:i:s') . '", ' . 
                'q.needs_index = "' . date('Y-m-d H:i:s') . '"';
		} else {
			$conditions .= ' AND q.open_status = ' . $this->Auth->user('id');
			$fields = 'q.open_status = 0, q.opened = NULL, ' . 
              'q.needs_index = "' . date('Y-m-d H:i:s') . '"';
		}
		
		if ($created_only == 1) {
			$conditions .= ' AND q.creator_id = ' . $this->Auth->user('id');
		}

		$this->Question->query('
			UPDATE user_questions u
				JOIN question_handles h ON (h.id = u.question_handle_id)
				JOIN questions q ON (q.id = h.question_id)
			SET ' . $fields . '
			WHERE ' . $conditions
		);

		if ($new_policy == 'open') {
			$resp['num_open'] = $this->Question->find('count', array(
				'conditions' => array('Question.open_status !=' => 0)
			));
		}

		$all_folders = array();
		foreach ($nodes as $node) $all_folders[] = $node['data']['folder_id'];

		$conditions = 'u.folder_id IN (' . implode(',', $all_folders) . ') AND u.deleted IS NULL';

		$result = $this->Question->query('
			SELECT COUNT(DISTINCT(q.id)) FROM user_questions u
				JOIN question_handles h ON (h.id = u.question_handle_id)
				JOIN questions q ON (q.id = h.question_id)
			WHERE ' . $conditions . ' AND q.creator_id = ' . $this->Auth->user('id') . ' AND q.open_status = 0'
		);
		$private_count = $result[0][0]['COUNT(DISTINCT(q.id))'];

		$resp['private_count'] = $private_count;		
		$resp['success'] = $success;
		
		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function will_update_model() {
		$this->autoRender = false;
		$this->layout = '';

        $success = true;

        $secrets = Configure::read('secrets');
        if (!array_key_exists('secret', $this->request->query)) $success = false;
        else if ($this->request->query['secret'] != $secrets['stats_server']['secret']) $success = false;
        else {

			$this->Question->updateAll(
				array('Question.stats_json' => null),
				array()
			);

        }
        		
		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode(array(
            'success' => $success
        ));
    }

    public function did_update_model() {
		$this->autoRender = false;
		$this->layout = '';

        $success = true;

        $secrets = Configure::read('secrets');
        if (!array_key_exists('secret', $this->request->query)) $success = false;
        else if ($this->request->query['secret'] != $secrets['stats_server']['secret']) $success = false;
        else {

			$this->Question->updateAll(
				array(
					'Question.stats_count'          => 0,
					'Question.stats_difficulty'     => null,
					'Question.stats_discrimination' => null,
					'Question.stats_quality'        => 0
				),
				array(
					'Question.stats_json' => null
				)
			);

            chdir(Configure::read('approot'));
            putenv('CAKE_APPROOT=' . Configure::read('approot'));
            exec("Console/cake updateIndex > /dev/null 2>&1 &");

        }
        		
		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode(array(
            'success' => $success
        ));
    }
}


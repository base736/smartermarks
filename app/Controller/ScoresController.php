<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');
	
class ScoresController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
        $this->Auth->allow('get_xml', 'save_results', 'get_data');
	}

	public function isAuthorized($user) {
		if (parent::isAuthorized($user)) {
	        return true;
	    }

		$this->loadModel('Document');

	    if (in_array($this->action, array('delete_old', 'create_delete_job'))) {
		    return true;
		}

		if (in_array($this->action, array('run_delete_job'))) {
	    	if (isset($this->request->params['pass'][0])) {
				$token = $this->request->params['pass'][0];
				$this->loadModel('DeleteJob');
				$this->DeleteJob->contain();
				$job = $this->DeleteJob->findByToken($token);
				if (!empty($job) && ($job['DeleteJob']['user_id'] == $this->Auth->user('id'))) return true;
				else return false;
			} else return false;
		}
		
        if (in_array($this->action, array('set_view', 'failed', 'set_save', 'set_move'))) {
			if (isset($this->request->params['pass'][0])) {
                $temp_id = $this->request->params['pass'][0];
                $cache_details = Cache::read($temp_id, 'scans_temp');
                if (!empty($cache_details)) {
                    $user_id = $cache_details['user_id'];
                    return ($user_id == $this->Auth->user('id'));
                } else return false;
            } else return false;
		}

		if (in_array($this->action, array('delete'))) {
	    	if (!empty($this->request->data['score_ids'])) {
				$this->loadModel("Document");

				foreach ($this->request->data['score_ids'] as $score_id) {
					$this->Score->contain(array('Document'));
					$score = $this->Score->findById($score_id);
					if (!empty($score)) {
						if ($score['Score']['user_id'] != $this->Auth->user('id')) {
							if (!isset($score['Document']['id'])) return false;
							else $documentID = $score['Document']['id'];
	
							$permissions = $this->Document->getPermissions($documentID, $this->Auth->user('id'));
							if (!in_array('all_scores', $permissions)) return false;
						}
					} else return false;
				}
				return true;

			} else return false;
		}

	    if (in_array($this->action, array('mark_new'))) {
	    	if (!empty($this->request->query['ids'])) {
                $document_ids = explode(',', $this->request->query['ids']);
                foreach ($document_ids as $documentID) {
                    $permissions = $this->Document->getPermissions($documentID, $this->Auth->user('id'));
                    if (!in_array('can_view', $permissions)) return false;
                }
                return true;
            } else return false;
        }

        if (in_array($this->action, array('submit'))) {
	    	if (!empty($this->request->data['documentIDs'])) {
                $document_ids = $this->request->data['documentIDs'];
                foreach ($document_ids as $documentID) {
                    $permissions = $this->Document->getPermissions($documentID, $this->Auth->user('id'));
                    if (!in_array('can_view', $permissions)) return false;
                }
                return true;
            } else return false;
        }

		$documentID = null;
	    if (in_array($this->action, array('index'))) {
	    	if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$documentID = $this->request->params['pass'][0];
			} else return ($this->Auth->user('role') == 'admin');
		}

	    if (in_array($this->action, array('get_index', 'create'))) {
			if (!isset($this->request->data['documentID'])) return false;
			else $documentID = $this->request->data['documentID'];
		}

		if (in_array($this->action, array('get', 'save', 'view_results', 'view_teacher', 'print_teacher', 
            'view_student', 'print_student'))) {
	    	if(isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$this->Score->contain(array('Document'));
				$score = $this->Score->findById($this->request->params['pass'][0]);
				if (!empty($score)) {
				  	if ($score['Score']['user_id'] == $this->Auth->user('id')) return true;
				  	else if (!isset($score['Document']['id'])) return false;
				  	else $documentID = $score['Document']['id'];
				} else return false;
			} else if ($this->Auth->user('role') == 'admin') {
	    		return true;
	    	}
		}
		
		$needs_view = array('index');
		$needs_all_scores = array('get', 'save', 'view_results', 'view_teacher', 'print_teacher',
            'view_student', 'print_student');
		
		$this->loadModel("Document");
		$permissions = $this->Document->getPermissions($documentID, $this->Auth->user('id'));
		if (in_array($this->action, $needs_all_scores)) return in_array('all_scores', $permissions);
		else return in_array('can_view', $permissions);
	}

	public function delete_old() {
		$maxDate = date('Y-m-d', strtotime("-6 months"));
		$this->set(compact('maxDate'));
	}

	public function create_delete_job() {
		$this->autoRender = false;
		$this->layout = '';

		$maxDate = $this->request->data('maxDate');
		$maxTimestamp = strtotime($maxDate);

		$tableData = array();
		$jobData = array();

		// Find scores for paper assessments that were built before the given date

		$this->loadModel('Score');

		$scoresData = array();
		$score_ids = array();

		$this->Score->contain('Document');
		$scores = $this->Score->find('all', array(
			'conditions' => array(
				'Score.user_id' => $this->Auth->user('id'),
				'Score.created <' => $maxDate
			)
		));

		foreach ($scores as $score) {
			$description = 'All responses for ';
			if (!empty($score['Document']['save_name'])) {
				$description .= '"' . $score['Document']['save_name'] . '"';
				if (strlen($score['Score']['title']) > 0) $description .= ', ' . $score['Score']['title'];
			} else $description .= 'untitled form';

			$scoresData[] = array(
				'created' => $score['Score']['created'],
				'description' => $description
			);
			$score_ids[] = $score['Score']['id'];
		}
		if (count($scoresData) > 0) {
			$tableData[] = array(
				'title' => 'Paper assessment scores',
				'rows' => $scoresData
			);
			$jobData['Score'] = $score_ids;
		}

		// Find students in online sittings who were added before the given date

		$this->loadModel('Sitting');

		$sittingsData = array();
		$sittingStudentIDs = array();

		$this->Sitting->contain('SittingStudent');
		$sittings = $this->Sitting->find('all', array(
			'conditions' => array(
				'Sitting.user_id' => $this->Auth->user('id'),
				'Sitting.created <' => $maxDate
			)
		));
		foreach ($sittings as $sitting) {
			$matching = 0;
			foreach ($sitting['SittingStudent'] as $sitting_student) {
				$thisTimestamp = strtotime($sitting_student['created']);
				if ($thisTimestamp < $maxTimestamp) {
					$sittingStudentIDs[] = $sitting_student['id'];
					$matching++;
				}
			}

			if ($matching > 0) {
				$description = $matching . ' student' . (($matching == 1) ? '' : 's') . ' in ';
				if (!empty($sitting['Sitting']['name'])) $description .= '"' . $sitting['Sitting']['name'] . '"';
				else $description .= 'untitled sitting';
	
				$sittingsData[] = array(
					'created' => $sitting['Sitting']['created'],
					'description' => $description
				);
			}
		}
		if (count($sittingsData) > 0) {
			$tableData[] = array(
				'title' => 'Students in online sittings',
				'rows' => $sittingsData
			);
			$jobData['SittingStudent'] = $sittingStudentIDs;
		}

		$json_data = json_encode($jobData);
		$token = md5(microtime() . ' ' . $json_data);

		$data = array();
		$data['user_id'] = $this->Auth->user('id');
		$data['token'] = $token;
		$data['json_data'] = $json_data;

		$this->loadModel('DeleteJob');
        $this->DeleteJob->create();
		$this->DeleteJob->save($data);

		$this->DeleteJob->deleteAll(array(
			'DeleteJob.created <' => date('Y-m-d H:i:s', strtotime("-1 day"))
		));

		echo json_encode(
			array(
				'token' => $token,
		   		'data' => $tableData
		   	)
		);
	}

	public function run_delete_job($token) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

		$this->loadModel('DeleteJob');
		$this->DeleteJob->contain();
		$job = $this->DeleteJob->findByToken($token);

		$data = json_decode($job['DeleteJob']['json_data'], true);

		if (isset($data['Score'])) {
			$this->loadModel('Score');
			$this->Score->deleteAll(array(
				'Score.id' => $data['Score']
			), true, true);
		}

		if (isset($data['SittingStudent'])) {
			$this->loadModel('SittingStudent');
			$this->SittingStudent->includeDeleted();
			$this->SittingStudent->deleteAll(array(
				'SittingStudent.id' => $data['SittingStudent']
			), true, true);
			$this->SittingStudent->excludeDeleted();
		}

		echo json_encode(
			array(
		   		'success' => $success
		   	)
		);
	}

    public function get_index() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

        $query_params = array(
            'contain' => array('User')
        );

        if (isset($this->request->data['documentID'])) {
            $document_id = $this->request->data['documentID'];

            $query_params['conditions'] = array('Score.document_id' => $document_id);

            if ($this->Auth->user('role') != 'admin') {
                $this->loadModel("Document");
                $permissions = $this->Document->getPermissions($document_id, $this->Auth->user('id'));
                if (!in_array('all_scores', $permissions)) {
                    $query_params['conditions']['Score.user_id'] = $this->Auth->user('id');
                }
            }

        } else {

            $query_params['order'] = array('Score.id DESC');

        }

		if (isset($this->request->data['limit'])) {
			if (is_numeric($this->request->data['limit']) && ($this->request->data['limit'] > 0)) {
				$query_params['limit'] = intval($this->request->data['limit']);
			} else if ($this->request->data['limit'] !== 'all') {
                $success = false;
            }
		}

		if (isset($this->request->data['page'])) {
			if (is_numeric($this->request->data['page']) && ($this->request->data['page'] > 0)) {
				$query_params['page'] = intval($this->request->data['page']);
			} else $success = false;
		} else $query_params['page'] = 1;

        if ($success) {
			$results = $this->Score->find('all', $query_params);
			foreach ($results as &$pResult) {
                $pResult['Score']['user_email'] = $pResult['User']['email'];
                unset($pResult['User']);
			}
            
			$resp['results'] = $results;
		}

		$resp['page'] = $query_params['page'];
        if (array_key_exists('limit', $query_params)) {
            $resp['limit'] = $query_params['limit'];
        }
		$resp['success'] = $success;

		echo json_encode($resp);
    }

	public function index($documentID = null) {
        if ($documentID != null) {            
            $this->loadModel('Document');
            $this->Document->contain('VersionData');
            $document = $this->Document->findById($documentID);
            $document_name = $document['Document']['save_name'];
            $document_layout_string = $document['Document']['layout_json'];
            $document_scoring_string = $document['VersionData']['scoring_json'];
            $this->set(compact('document_name', 'document_layout_string', 'document_scoring_string'));
        }
            
        if (!empty($this->Auth->user('hidden_tips'))) {
            $hiddenTips = explode(";", $this->Auth->user('hidden_tips'));
            $hideScoringHowto = in_array("scoring_howto", $hiddenTips);
        } else $hideScoringHowto = false;

        $this->set(compact('documentID', 'hideScoringHowto'));
    }

	public function delete() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

        $this->loadModel('ResponsePaper');

		$score_ids = $this->request->data['score_ids'];
        $success &= $this->ResponsePaper->deleteAll(array(
            'ResponsePaper.score_id' => $score_ids
        ), true, true);
    
		echo json_encode(
			array(
				'count' => count($score_ids),
		   		'success' => $success
		   	)
		);
	}
	
	private function get_message_list($details) {
		$list = "<ul>\n";
		foreach ($details as $thisDetails) {
			$list = $list . "<li>" . htmlspecialchars(html_entity_decode($thisDetails['message']));
			if (sizeof($thisDetails['pages']) == 0) {
			} else if (sizeof($thisDetails['pages']) == 1) {
	    		$list = $list . " on page " . $thisDetails['pages'][0] . ".</li>\n";
			} else {
	    		$list = $list . " on pages ";
	    		$pageItems = array();
	    		$i = 0;
	    		while ($i < sizeof($thisDetails['pages'])) {
					$thisNumber = intval($thisDetails['pages'][$i]);
					if ($i == sizeof($thisDetails['pages'])) {
						$pageItems[] = $thisNumber;
					} else {
						for ($j = 1; $i + $j < sizeof($thisDetails['pages']); ++$j) {
							if (intval($thisDetails['pages'][$i + $j]) != $thisNumber + $j) break;
						}
						if ($j > 2) {
							$pageItems[] = $thisNumber . "-" . ($thisNumber + $j - 1);
						} else {
							for ($k = 0; $k < $j; ++$k) {
								$pageItems[] = $thisNumber + $k;
							}
						}
						$i = $i + $j;
					}
				}
				
				if (sizeof($pageItems) == 1) {
					$list = $list . $pageItems[0];
				} else {
					$lastItem = array_pop($pageItems);
					$list = $list . implode(', ', $pageItems) . ' and ' . $lastItem;
				}
				
				$list = $list . ".</li>\n";
			}
		}
		$list = $list . "</ul>\n";
		
		return $list;
	}

    public function get_data($get_data_token) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
        $resp = array();

        $key_details = Cache::read($get_data_token, 'get_data_tokens');
        if (!empty($key_details)) {
            $user_id = $key_details['user_id'];
            $score_id = $key_details['score_id'];

            $this->loadModel('User');
            $this->User->contain();
            $user = $this->User->findById($user_id);
            if (!empty($user)) {
                $user_defaults = json_decode($user['User']['defaults'], true);
            } else $success = false;
    
            $this->Score->contain([
                'ResponsePaper' => [
                    'ResultSet' => 'ResultData'
                ],
                'Document' => [
                    'VersionData' => [
                        'fields' => ['id', 'scoring_json']
                    ]
                ]
            ]);

            $score_data = $this->Score->findById($score_id);
            if (!empty($score_data)) {
    
                $cache_hash = md5(json_encode(array(
                    'userDefaults' => $user_defaults,
                    'scoreData' => $score_data
                )));
    
                $cache_data = Cache::read($user_id, 'report_data');
                if (empty($cache_data)) $cache_data = [];
                $cache_data[$score_id] = $cache_hash;
                Cache::write($user_id, $cache_data, 'report_data');    
        
            } else $success = false;
        } else $success = false;

        $resp['success'] = $success;
        
        if ($success) {
            $resp['cacheHash'] = $cache_hash;
            $resp['userDefaults'] = $user_defaults;
            $resp['scoreData'] = $score_data;
        }
    
		echo json_encode($resp);
    }

	public function view_results($score_id) {

        // Read cache ID

        $user_id = $this->Auth->user('id');
        $cache_data = Cache::read($user_id, 'report_data');
        if (isset($cache_data[$score_id])) $cache_hash = $cache_data[$score_id];
        else $cache_hash = '';

        $get_data_token = md5(microtime() . $score_id);
        Cache::write($get_data_token, array(
            'score_id' => $score_id,
            'user_id' => $user_id
        ), 'get_data_tokens');

        $this->set(compact('score_id', 'cache_hash', 'get_data_token'));

        // Get document elements

		$this->Score->contain(array('ResponsePaper', 'Document'));
		$score = $this->Score->findById($score_id);
		if (empty($score)) {
			$this->Flash->error("Results not found.");
			$this->redirect_home();
		}

        $document_id = $score['Document']['id'];
        $save_name = $score['Document']['save_name'];
		
        $details = json_decode($score['Score']['details_json'], true);
        if (array_key_exists('format', $details)) $format_xml = $details['format'];
        else $format_xml = false;

        // Check if this PDF has been scored successfully before

        if ($score['ResponsePaper']['pdf_hash'] !== null) {
            $max_date = new DateTime($score['ResponsePaper']['date']);
            $max_date->modify('-5 seconds');
            $max_date_string = $max_date->format('Y-m-d H:i:s');

            $this->Score->contain('ResponsePaper');
            $repeat_events = $this->Score->find('count', array(
                'conditions' => array(
                    'ResponsePaper.pdf_hash' => $score['ResponsePaper']['pdf_hash'],
                    'ResponsePaper.date <' => $max_date_string,
                    'Score.pageCount >' => 0,
                    'Score.errorCount' => 0
                )
            ));    
        } else $repeat_events = 0;

		$this->set(compact('document_id', 'save_name', 'repeat_events', 'format_xml'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
    }

    public function view_teacher($score_id) {

        // Read cache ID

        $user_id = $this->Auth->user('id');
        $cache_data = Cache::read($user_id, 'report_data');
        if (isset($cache_data[$score_id])) $cache_hash = $cache_data[$score_id];
        else $cache_hash = '';

        $get_data_token = md5(microtime() . $score_id);
        Cache::write($get_data_token, array(
            'score_id' => $score_id,
            'user_id' => $user_id
        ), 'get_data_tokens');

        $this->set(compact('score_id', 'cache_hash', 'get_data_token'));

        header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: -1");
	}

    public function print_teacher($score_id) {
		$this->autoRender = false;
	   	$this->layout = '';

        $success = true;

        // Read cache ID

        $user_id = $this->Auth->user('id');
        $cache_data = Cache::read($user_id, 'report_data');
        if (isset($cache_data[$score_id])) $cache_hash = $cache_data[$score_id];
        else $cache_hash = '';

        // Build get data token

        $get_data_token = md5(microtime() . $score_id);
        Cache::write($get_data_token, array(
            'score_id' => $score_id,
            'user_id' => $user_id
        ), 'get_data_tokens');

        // Build HTML

		$view = new View($this, false);
		$view->layout = false;
		$view->viewPath='Scores';
        $view->set(compact('score_id', 'cache_hash', 'get_data_token'));
		$viewHTML = $view->render('print_teacher');

        $user_defaults = json_decode($this->Auth->user('defaults'), true);
        $page_settings = $user_defaults['Common']['Settings'];

		// Build and return PDF

        $this->html_to_pdf_response($viewHTML, $page_settings);
	}

    public function view_student($score_id) {
        
        // Read cache ID

        $user_id = $this->Auth->user('id');
        $cache_data = Cache::read($user_id, 'report_data');
        if (isset($cache_data[$score_id])) $cache_hash = $cache_data[$score_id];
        else $cache_hash = '';

        $get_data_token = md5(microtime() . $score_id);
        Cache::write($get_data_token, array(
            'score_id' => $score_id,
            'user_id' => $user_id
        ), 'get_data_tokens');

        $this->set(compact('score_id', 'cache_hash', 'get_data_token'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
    }

    public function print_student($score_id) {
        $this->autoRender = false;
        $this->layout = '';

        $success = true;

        // Read cache ID

        $user_id = $this->Auth->user('id');
        $cache_data = Cache::read($user_id, 'report_data');
        if (isset($cache_data[$score_id])) $cache_hash = $cache_data[$score_id];
        else $cache_hash = '';

        // Build get data token

        $get_data_token = md5(microtime() . $score_id);
        Cache::write($get_data_token, array(
            'score_id' => $score_id,
            'user_id' => $user_id
        ), 'get_data_tokens');

        // Build HTML
        
        $user_defaults = json_decode($this->Auth->user('defaults'), true);

        $page_size = $user_defaults['Common']['Settings']['pageSize'];
        $margin_sizes = $user_defaults['Common']['Settings']['marginSizes'];

        $view = new View($this, false);
        $view->layout = false;
        $view->viewPath='Scores';
        $view->set(compact('score_id', 'cache_hash', 'get_data_token', 'page_size', 'margin_sizes'));
        $viewHTML = $view->render('print_student');

        $page_settings = $user_defaults['Common']['Settings'];

		// Build and return PDF

        $this->html_to_pdf_response($viewHTML, $page_settings);
    }

	public function waiting($job_id) {
		$this->set(compact('job_id'));
	}

    public function set_view($temp_id) {
        $cache_details = Cache::read($temp_id, 'scans_temp');

        if (!empty($cache_details)) {

            $this->set(compact('temp_id', 'cache_details'));

        } else {

            $this->Flash->error("Landing page expired.");
			$this->redirect_home();

        }
    }

    public function failed($temp_id) {
        $cache_details = Cache::read($temp_id, 'scans_temp');

        if (!empty($cache_details)) {

            $score_name = false;
            if (empty($cache_details['response_data'])) {

                $score_name = "unknown form/version";
                $format_xml = '';

            } else {

                $expected_indices = array();
                foreach ($cache_details['response_data'] as $key => $this_details) {
                    if (in_array($this_details['type'], array('expected', 'missing'))) {
                        $expected_indices[] = $key;
                    }
                }
    
                if (count($expected_indices) == 1) {

                    $expected_index = $expected_indices[0];
                    $this_details = $cache_details['response_data'][$expected_index];

                    $document_id = $this_details['documentID'];

                    $this->loadModel('Document');
                    $this->Document->contain();
                    $document = $this->Document->findById($document_id);
                    if (!empty($document)) $score_name = $document['Document']['save_name'];
                    else $score_name = "unknown form/version";

                    if (array_key_exists('formatXML', $this_details)) {
                        $format_xml = $this_details['formatXML'];
                    } else $format_xml = '';

                } else {
                    
                    $score_name = "multiple forms/versions";
                    $format_xml = '';
                    
                }
                
            }

            $this->set(compact('score_name', 'cache_details', 'format_xml'));

        } else {

            $this->Flash->error("Landing page expired.");
			$this->redirect_home();

        }
    }

    public function set_save($temp_id) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

        $cache_details = Cache::read($temp_id, 'scans_temp');
        if (empty($cache_details)) $success = false;

        if (array_key_exists('response_id', $this->request->data)) {
            $response_id = $this->request->data['response_id'];
        } else $success = false;
        
        if ($success) {
            
            $this_key = false;
            foreach ($cache_details['response_data'] as $key => $entry) {
                if ($entry['id'] == $response_id) {
                    $this_key = $key;
                    break;
                }
            }

            if ($this_key !== false) {
            
                $this_data = $cache_details['response_data'][$this_key];

                $errors_pages = array();
                foreach ($this_data['errors'] as $error) {
                    $errors_pages = array_merge($errors_pages, $error['pages']);
                }
                $errors_pages = array_values(array_unique($errors_pages));

                $score_details = $cache_details['score_common'];
                $score_details['document_id'] = $this_data['documentID'];
                $score_details['errorCount'] = count($errors_pages);

                $formatXML = $this_data['formatXML'];
                $responses = $this_data['responses'];

                $score_id = $this->Score->create_engine($score_details, $formatXML, $responses);
                if ($score_id !== false) {

                    $this->Score->contain();
                    $score = $this->Score->findById($score_id);

                    $details = json_decode($score['Score']['details_json'], true);
                    $details_modified = false;

                    if (!empty($entry['warnings'])) {
                        $details['warnings'] = $entry['warnings'];
                        $details_modified = true;
                    }

                    if (!empty($entry['errors'])) {
                        $details['errors'] = $entry['errors'];
                        $details_modified = true;
                    }

                    if ($details_modified) {
                        $this->Score->id = $score_id;
                        $this->Score->saveField('details_json', json_encode($details));
                    }

                    $cache_details['response_data'][$this_key]['type'] = 'expected';
                    $cache_details['response_data'][$this_key]['scoreID'] = $score_id;

                    Cache::write($temp_id, $cache_details, 'scans_temp');
                    $resp['cache_details'] = $cache_details;   

                } else $success = false;    
            } else $success = false;
        }

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		echo json_encode($resp);
    }

    public function set_move($temp_id) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

        $cache_details = Cache::read($temp_id, 'scans_temp');
        if (empty($cache_details)) $success = false;

        if (array_key_exists('response_id', $this->request->data)) {
            $response_id = $this->request->data['response_id'];
        } else $success = false;

        if ($success) {
            $source_key = false;
            foreach ($cache_details['response_data'] as $key => $entry) {
                if ($entry['id'] == $response_id) {
                    $source_key = $key;
                }
            }
            if ($source_key === false) $success = false;
        }

        $target_key = false;
        if (array_key_exists('score_id', $this->request->data)) {

            $score_id = $this->request->data['score_id'];

            $this->Score->contain(array('Document' => 'VersionData'));
            $score = $this->Score->findById($score_id);
            if (!empty($score)) {
                $scoring_data = json_decode($score['Document']['VersionData']['scoring_json'], true);
            } else $success = false;

            foreach ($cache_details['response_data'] as $key => $entry) {
                if (array_key_exists('scoreID', $entry) && ($entry['scoreID'] == $score_id)) {
                    $target_key = $key;
                }
            }   
            if ($target_key === false) $success = false; 

        } else if (array_key_exists('document_id', $this->request->data)) {

            $score_id = false;
            $document_id = $this->request->data['document_id'];

            $this->loadModel('Document');
            $this->Document->contain('VersionData');
            $document = $this->Document->findById($document_id);
            if (!empty($document)) {
                $scoring_data = json_decode($document['VersionData']['scoring_json'], true);
            } else $success = false;

            foreach ($cache_details['response_data'] as $key => $entry) {
                if (array_key_exists('documentID', $entry) && ($entry['documentID'] == $document_id)) {
                    $target_key = $key;
                }
            }
            if ($target_key === false) $success = false; 

        } else $success = false;

        if ($success) {

            // Check responses against question data

            $results = $cache_details['response_data'][$source_key]['results'];

            $response_index = 0;
            foreach ($scoring_data['Sections'] as $section) {
                foreach ($section['Content'] as $question_data) {
                    $question_style = substr($question_data['type'], 0, 2);

                    $response_regex = false;
                    if ($question_style == 'mc') {

                        $response_regex = '/^[';
                        foreach ($question_data['Answers'] as $answer) {
                            $response_regex .= $answer['response'];
                        }
                        $response_regex .= ' ]*$/';

                    } else if ($question_style == 'nr') {

                        $nr_digits = $question_data['TypeDetails']['nrDigits'];
                        $response_regex = '/^[-\.\d\/ ]{' . $nr_digits . '}$/';
        
                    } else if ($question_style == 'wr') {

                        $response_regex = '/^';
                        foreach ($question_data['TypeDetails']['criteria'] as $criterion) {
                            $response_regex .= '[0-' . $criterion['value'] . ' ][\+ ]';
                        }
                        $response_regex .= '$/';

                    }

                    if ($response_regex !== false) {
                        if ($response_index < count($results)) {
                            $success &= preg_match($response_regex, $results[$response_index]);
                        } else $success = false;
                    }

                    ++$response_index;
                }
            }

            if ($response_index !== count($results)) {
                $success = false;
            }
        }

        if ($success) {

            // Create a new score if required

            if ($score_id === false) {

                $score_details = $cache_details['score_common'];
                $score_details['document_id'] = $document_id;
                $score_details['errorCount'] = 0;

                $formatXML = $cache_details['response_data'][$source_key]['formatXML'];
                $responses = array();

                $score_id = $this->Score->create_engine($score_details, $formatXML, $responses);
    
                $cache_details['response_data'][$target_key]['type'] = 'expected';
                $cache_details['response_data'][$target_key]['scoreID'] = $score_id;
                
            }

            // Add responses to the target score

            $this->Score->contain(array('Document', 'ResponsePaper'));
            $score = $this->Score->findById($score_id);

            $score_details = json_decode($score['Score']['details_json'], true);
            $result_set_id = $score['ResponsePaper']['result_set_id'];
            $version_data_id = $score['Document']['version_data_id'];
    
            $this->loadModel('ResultData');

            $result_data = array();
            $result_data['result_set_id'] = $result_set_id;
            $result_data['version_data_id'] = $version_data_id;
            $result_data['results'] = json_encode($results);
            $this->ResultData->create();
            $this->ResultData->save($result_data);

            if (!empty($cache_details['response_data'][$source_key]['details'])) {
                $this_data = $cache_details['response_data'][$source_key];
                $score_details['responses'][$this->ResultData->id] = $this_data['details'];
                $cache_details['response_data'][$target_key]['responses'][] = array(
                    'details' => $this_data['details'],
                    'results' => $this_data['results']
                );
            }

            // Update score details

            $this->Score->id = $score_id;
            $this->Score->saveField('details_json', json_encode($score_details));    

            // Remove source entry from cache details

            unset($cache_details['response_data'][$source_key]);
            $cache_details['response_data'] = array_values($cache_details['response_data']);

            Cache::write($temp_id, $cache_details, 'scans_temp');
            $resp['cache_details'] = $cache_details;
        }

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		echo json_encode($resp);
    }

	public function mark_new() {
		$this->loadModel('Document');
		$this->loadModel('User');

		$this->loadModel('User');
		if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {
			$this->redirect(array('controller' => 'Users', 'action' => 'expired'));
		} 

        $document_ids = explode(',', $this->request->query['ids']);

        foreach ($document_ids as $document_id) {
            $message = $this->Document->canScore($document_id);
            if ($message !== NULL) {
                $this->Flash->error($message);
                $this->redirect_home();
            }
        }

		$this->Document->contain();
		$documents = $this->Document->find('list', array(
            'fields' => array('Document.id', 'Document.save_name'),
            'conditions' => array('Document.id' => $document_ids)
        ));

        $formats = array();
        foreach ($document_ids as $document_id) {
            $formatXML = $this->Document->generateXML($document_id, true);
            $formats[$document_id] = $formatXML;
        }

		$this->set(compact('documents', 'formats'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}
	
    public function get($id) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
        $resp = array();

		$this->Score->contain(array('ResponsePaper' => array('ResultSet' => 'ResultData')));
		$score = $this->Score->findById($id);

		if (!empty($score)) {
            $resp['title'] = $score['Score']['title'];
            $details = json_decode($score['Score']['details_json'], true);

            if (array_key_exists('format', $details)) {
                $resp['format'] = $details['format'];
            }

            if (array_key_exists('responses', $details)) {
                $response_details = $details['responses'];
            } else $response_details = array();

            $resp['responses'] = array();
            foreach ($score['ResponsePaper']['ResultSet']['ResultData'] as $result_data) {
                $result_data_id = $result_data['id'];

                $entry = array(
                    'id' => $result_data_id,
                    'results' => json_decode($result_data['results'], true)
                );

                if (array_key_exists($result_data_id, $response_details)) {
                    $entry['details'] = $response_details[$result_data_id];
                } else $entry['details'] = array();

                $resp['responses'][] = $entry;
            }
        } else $success = false;
		
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		
		echo json_encode($resp);
	}

    public function create() {
		$this->autoRender = false;
		$this->layout = '';

        $success = true;
        $resp = array();

        $score_details = array(
            'user_id' => $this->Auth->user('id'),
            'document_id' => $this->request->data['documentID'],
            'title' => $this->request->data['title'],    
        );

        $formatXML = $this->request->data['formatXML'];
        $responses = json_decode($this->request->data['responses'], true);

        $score_id = $this->Score->create_engine($score_details, $formatXML, $responses);
        if ($score_id !== false) $resp['id'] = $score_id;
        else $success = false;
                
        $resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		echo json_encode($resp);
    }
	
	public function save($id = null) {
		$this->autoRender = false;
		$this->layout = '';

        $success = true;
        $resp = array();

        $this->Score->contain(array('Document', 'ResponsePaper' => array('ResultSet' => 'ResultData')));
		$score = $this->Score->findById($id);

		if (!empty($score)) {
            $responses = json_decode($this->request->data['responses'], true);

            $expired_result_data_ids = array();
            foreach ($score['ResponsePaper']['ResultSet']['ResultData'] as $result_data) {
                $expired_result_data_ids[] = $result_data['id'];
            }

            foreach ($responses as $result) {
                if (array_key_exists('id', $result)) {
                    $key = array_search($result['id'], $expired_result_data_ids);
                    if ($key !== false) unset($expired_result_data_ids[$key]);
                    else $success = false;
                }
            }

            $expired_result_data_ids = array_values($expired_result_data_ids);

        } else $success = false;

        if ($success) {

            $details = json_decode($score['Score']['details_json'], true);
            $details['responses'] = array();

            $this->loadModel('ResultData');
            foreach ($responses as &$pEntry) {
                $data = array(
                    'results' => json_encode($pEntry['results'])
                );

                if (!array_key_exists('id', $pEntry)) {
                    $data['result_set_id'] = $score['ResponsePaper']['result_set_id'];
                    $data['version_data_id'] = $score['Document']['version_data_id'];
                    $this->ResultData->create();
                } else $data['id'] = $pEntry['id'];

                $this->ResultData->save($data);
                $pEntry['id'] = $this->ResultData->id;

                if (!empty($pEntry['details'])) {
                    $details['responses'][$pEntry['id']] = $pEntry['details'];
                }
            }

            if (!empty($expired_result_data_ids)) {
                $this->ResultData->deleteAll(array(
                    'ResultData.id' => $expired_result_data_ids
                ));    
            }

            $resp['responses'] = $responses;

            $data = array();
            $data['id'] = $id;
            $data['title'] = $this->request->data['title'];
            $data['details_json'] = json_encode($details);
            $success &= !empty($this->Score->save($data));
        }

        $resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		echo json_encode($resp);
	}

	public function get_xml() {
		$this->autoRender = false;
		$this->layout = '';

        $success = true;
        $resp = array();

        $this->loadModel('Document');

        // Authenticate request and get some data
        
        $user_id = $this->Auth->user('id');

        if (array_key_exists('document_id', $this->request->query)) {
            $document_id = $this->request->query['document_id'];

            $this->Document->contain();
            $document = $this->Document->findById($document_id);
            $resp['name'] = $document['Document']['save_name'];

            $permissions = $this->Document->getPermissions($document_id, $user_id);
            if (!in_array('can_view', $permissions)) $success = false;
        } else $success = false;

        // Get format XML

        if ($success) {
            $formatXML = $this->Document->generateXML($document_id, true);
            $formatXML = str_replace("\n", '', $formatXML);
            $resp['formatXML'] = $formatXML;
        }

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		
		echo json_encode($resp);
	}

    public function save_results() {
        $this->autoRender = false;
		$this->layout = '';
		
		$success = true;

		// Get the job results

        if (!empty($this->request->data['results'])) {
            $results_json = $this->request->data['results'];
            $results = json_decode($results_json, true);
            if (empty($results)) $success = false;
        } else $success = false;

		// Get common score data

        if (!empty($this->request->data['score_common'])) {
            $score_common_json = $this->request->data['score_common'];
            $score_common = json_decode($score_common_json, true);
            if (empty($score_common)) $success = false;
        } else $success = false;

        // Save results

        if ($success) {
            $score_common['details_json'] = json_encode(array(
                'userAgent' => $_SERVER['HTTP_USER_AGENT']
            ));
            $forward_url = $this->Score->save_results($results, $score_common);
            if ($forward_url === false) $success = false;            
        }

        // Return forwarding URL

        $resp = array();
        $resp['success'] = $success;

        if ($success) {
            $resp['forward_url'] = $forward_url;
        }

        echo json_encode($resp);
	}
}

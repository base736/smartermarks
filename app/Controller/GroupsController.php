<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class GroupsController extends AppController {

	public $components = array('Search.Prg');

    public $presetVars = true; // using the model configuration
    public $paginate = array('paramType' => 'querystring');

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
	    }

		if (in_array($this->action, array('index'))) {
			return true;
		}

		if (in_array($this->action, array('get_members', 'view', 'member_add', 'member_remove', 'member_change', 'set_admin'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$group_id = $this->request->params['pass'][0];

				$this->loadModel('UserGroup');
				$userGroups = $this->UserGroup->find('list', array(
					'fields' => array('UserGroup.id'),
					'conditions' => array(
						'user_id' => $this->Auth->user('id'),
						'group_id' => $group_id,
						'is_admin' => 1
					)
				));
				return (!empty($userGroups));
			} else return false;
		}

		if (in_array($this->action, array('delete'))) {
			if (!empty($this->request->data['itemIds'])) {
				foreach ($this->request->data['itemIds'] as $group_id) {
					$this->loadModel('UserGroup');
					$userGroups = $this->UserGroup->find('list', array(
						'fields' => array('UserGroup.id'),
						'conditions' => array(
							'user_id' => $this->Auth->user('id'),
							'group_id' => $group_id,
							'is_admin' => 1
						)
					));
					if (empty($userGroups)) return false;
				}
				return true;
			} else return false;
		}
		return false;
	}

	// display all groups this user controls
	public function index() {
		
		$adminView = ($this->Auth->user('role') == 'admin');

		// Paginate groups for display
		
		$query_params = array('conditions' => array());
		$query_params['fields'] = array('Group.id', 'Group.id');

		$userDefaults = json_decode($this->Auth->user('defaults'), true);
		if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$query_params['order'] = array('Group.name' => 'asc');
		} else $query_params['order'] = array('Group.created' => 'desc');

		$query_params['group'] = array('Group.id');
	
		$this->loadModel('UserGroup');

		if (!empty($this->request->query['search'])) {
			$searchTerm = $this->request->query['search'];
			$query_params['conditions'][] = array('OR' => array(
				array('Group.name LIKE' => '%' . $searchTerm . '%'),
				array('User.email LIKE' => '%' . $searchTerm . '%'),
				array('User.name LIKE' => '%' . $searchTerm . '%')
			));
		}

		$query_params['conditions']['UserGroup.is_active'] = 1;

		if ($this->Auth->user('role') != 'admin') {
			$query_params['conditions']['UserGroup.user_id'] = $this->Auth->user('id');
			$query_params['conditions']['UserGroup.is_admin'] = 1;
			$query_params['conditions']['Group.is_active'] = 1;
		} else {
			$query_params['conditions']['Group.name !='] = '';
		}

		$pageFound = true;
		try {

			// Get group IDs

			$this->UserGroup->contain(array('Group', 'User'));
			$group_ids = $this->UserGroup->find('list', $query_params);

			// Get group records to show

			$this->paginate['limit'] = 10;
			$this->paginate['conditions'] = array('Group.id' => $group_ids);
			$this->paginate['order'] = $query_params['order'];

			$paginated = $this->paginate('Group');

		} catch (Exception $e) {
			if (get_class($e) == 'NotFoundException') $pageFound = false;
		}

		if ($pageFound === false) {
			if ($this->Session->check('pageNotFound')) unset($this->request->query['page']);
			else if (isset($this->request->query['page']) && ($this->request->query['page'] > 1)) {
				$this->request->query['page'] = $this->request->query['page'] - 1;
				$this->Session->write('pageNotFound', true);
			} else unset($this->request->query['page']);
			$redirectURL = strtok($_SERVER["REQUEST_URI"], '?');
			if (!empty($this->request->query)) $redirectURL .= '?' . http_build_query($this->request->query);
			header('Location: ' . $redirectURL);
		} else $this->Session->delete('pageNotFound');
		
		$numItems = $this->params['paging']['Group']['count'];

		if ($numItems == 1) {
			$this->redirect(array('controller' => 'Groups', 'action' => 'view', $paginated[0]['Group']['id']));
		}

		foreach ($paginated as &$pGroup) {
			$memberCount = $this->UserGroup->find('count', array(
				'conditions' => array('UserGroup.group_id' => $pGroup['Group']['id'])
			));
			$pGroup['Group']['member_count'] = $memberCount;
		}

		$this->set('groups', $paginated);
		
		$colCreated = "Group.created";
		$colName = "Group.name";
		
		$this->set(compact('adminView', 'colCreated', 'colName', 'numItems'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}

	public function get_members($group_id) {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		$this->Group->contain(array('UserGroup' => 'User'));
		$group = $this->Group->findById($group_id);

		// Get member array

		$members = array();
		foreach ($group['UserGroup'] as $userGroup) {
			$members[] = array(
				'id' => intval($userGroup['User']['id']),
				'email' => $userGroup['User']['email'],
				'name' => $userGroup['User']['name'],
				'email_bounced' => intval($userGroup['User']['email_bounced']),
				'is_admin' => intval($userGroup['is_admin']),
				'is_active' => intval($userGroup['is_active'])
			);
		}
		unset($group['UserGroup']);

		$resp['members'] = $members;
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	// view a group
	public function view($group_id) {
		$this->Group->contain();
		$group = $this->Group->findById($group_id);

		// Get expiry

		$expires = $group['Group']['expires'];

		// Get contacts
		
		$contacts = array();
		if ($this->Auth->user('role') != 'admin') {
			if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
			else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

			$this->loadModel('User');
			$this->User->contain();
			$contactRecords = $this->User->find('all', array(
				'conditions' => array('User.id' => $contact_ids))
			);
			foreach ($contact_ids as $id) {
				foreach ($contactRecords as $record) {
					if ($record['User']['id'] == $id) {
						$contacts[] = array(
							'id' => $record['User']['id'],
							'name' => $record['User']['name'],
							'email' => $record['User']['email']
						);
					}
				}
			}
		}

		$this->set(compact('group', 'contacts', 'expires'));

		$renew_by_email = false;

		$secrets = Configure::read('secrets');
		if (isset($secrets['email_no_card'])) {
			foreach ($secrets['email_no_card'] as $entry) {
				if (preg_match($entry['regex'], $this->Auth->user('email')) === 1) $renew_by_email = true;
			}	
		}

		$this->set(compact('renew_by_email'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}

	// ajax call for the group index

	public function member_add($group_id) {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		$user_ids = $this->request->data['user_ids'];

		// Add new users

		$this->loadModel('UserGroup');
		foreach ($user_ids as $user_id) {
			$this->Group->addMember($group_id, $user_id);		
		}

		// Get new member array

		$this->UserGroup->contain('User');
		$userGroups = $this->UserGroup->find('all', array(
			'conditions' => array(
				'UserGroup.group_id' => $group_id,
				'UserGroup.user_id' => $user_ids
			)
		));

		$members = array();
		foreach ($userGroups as $userGroup) {
			$members[] = array(
				'id' => intval($userGroup['User']['id']),
				'email' => $userGroup['User']['email'],
				'name' => $userGroup['User']['name'],
				'is_admin' => intval($userGroup['UserGroup']['is_admin']),
				'is_active' => intval($userGroup['UserGroup']['is_active'])	
			);
		}
		
		$resp['new_users'] = $members;

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	// ajax call for the group view

	public function member_remove($group_id) {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		$user_id = $this->request->data['user_id'];

		$this->loadModel('UserGroup');
		$this->UserGroup->deleteAll(
			array(
				'UserGroup.group_id' => $group_id,
				'UserGroup.user_id' => $user_id
			)
		);

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

    public function member_change($group_id) {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;
        $message = false;

		$user_id = $this->request->data['user_id'];
		$new_email = $this->request->data['new_email'];

        // Make sure the given user_id is a member of this group
        
        $this->loadModel('UserGroup');
		$this->UserGroup->contain();
		$user_group = $this->UserGroup->find('first', array(
			'conditions' => array(
				'UserGroup.group_id' => $group_id,
				'UserGroup.user_id' => $user_id
			)
        ));
        
        if (empty($user_group)) {
            $success = false;
            $message = 'User is not a member of this group';
        }

        if ($success) {
            $this->loadModel('User');

            $this->User->contain();
            $new_user = $this->User->find('first', array(
                'conditions' => array('email' => $new_email)
            ));

            if (empty($new_user)) {

                // Email does not correspond to an existing user. Just change the email address for this user.

                $updated_data = array(
                    'id' => $user_id,
                    'email' => $new_email,
                    'email_bounced' => 0
                );
                $this->User->save($updated_data);

                $this->User->send_validation($user_id);

                $resp['old_id'] = $user_id;
                $resp['new_id'] = $user_id;
                $resp['new_email'] = $new_email;
                $resp['new_email_bounced'] = 0;

                $resp['message'] = 'New account email updated';

            } else {

                // Email corresponds to an existing user. Swap that user into this group and exchange expiry dates.

                $this->User->contain();
                $old_user = $this->User->findById($user_id);

                $updated_old_data = array(
                    'id' => $old_user['User']['id'],
                    'expires' => $new_user['User']['expires']
                );
                $this->User->save($updated_old_data);

                $updated_new_data = array(
                    'id' => $new_user['User']['id'],
                    'expires' => $old_user['User']['expires']
                );
                $this->User->save($updated_new_data);

				$this->UserGroup->id = $user_group['UserGroup']['id'];
				$this->UserGroup->saveField('user_id', $new_user['User']['id']);

                $resp['old_id'] = $old_user['User']['id'];
                $resp['new_id'] = $new_user['User']['id'];
                $resp['new_email'] = $new_email;
                $resp['new_email_bounced'] = $new_user['User']['email_bounced'];

                $resp['message'] = 'Email swapped to existing user';

            }
        }

		$resp['success'] = $success;
        if ($message !== false) $resp['message'] = $message;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	// ajax call for the group view

	public function set_admin($group_id) {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		$user_id = $this->request->data['user_id'];
		$is_admin = $this->request->data['is_admin'];

		$this->loadModel('UserGroup');
		$this->UserGroup->contain();
		$this->UserGroup->updateAll(
			array(
				'UserGroup.is_admin' => $is_admin
			), array(
				'UserGroup.group_id' => $group_id,
				'UserGroup.user_id' => $user_id
			)
		);

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	// ajax call for the group index

	public function delete() {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		$group_ids = $this->request->data['itemIds'];
		foreach ($group_ids as $group_id) {
			$success &= $this->Group->delete($group_id);
		}

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}
}

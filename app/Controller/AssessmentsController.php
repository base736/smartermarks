<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

function type_compare($a, $b) {
	if (count($a['part_ids']) < count($b['part_ids'])) return 1;
	else if (count($a['part_ids']) > count($b['part_ids'])) return -1;
	else if ($a['type'] < $b['type']) return -1;
	else if ($a['type'] > $b['type']) return 1;
	else return 0;
}

class AssessmentsController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {
		
		if (parent::isAuthorized($user)) {
	        return true;
		}

		if (in_array($this->action, array('index', 'empty_trash'))) {
			return !empty($this->Auth->user('id'));
		}

		if (in_array($this->action, array('get_index'))) {
			if (!empty($this->request->query['folder_ids'])) {
				$this->loadModel('Folder');
				$folder_ids = explode(',', $this->request->query['folder_ids']);
				foreach ($folder_ids as $folder_id) {
					if (is_numeric($folder_id)) {
						$permissions = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
						return in_array('can_view', $permissions['all']);
					} else return false;
				}	
			} else return false;
		}

		if (in_array($this->action, array('create'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (isset($this->request->data['folder_id']) && is_numeric($this->request->data['folder_id'])) {
				$folderId = $this->request->data['folder_id'];
				$this->loadModel('Folder');
				$permissions = $this->Folder->getContentPermissions($folderId, $this->Auth->user('id'));
				return in_array('can_add', $permissions['all']);
			} else return true;
		}

	    if (in_array($this->action, array('send', 'copy', 'move'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (isset($this->request->data['item_ids']) && is_array($this->request->data['item_ids'])) {
				foreach ($this->request->data['item_ids'] as $assessment_id) {
					$itemPermissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
						
					if ($this->action == 'send') {
						if (!in_array('can_export', $itemPermissions)) return false;
					} else if (in_array($this->action, array('copy', 'move'))) {
						if (!empty($this->request->data['folder_id'])) {
							$this->loadModel('Folder');
							
							$targetFolderId = $this->request->data['folder_id'];
							$targetPermissions = $this->Folder->getContentPermissions($targetFolderId, 
								$this->Auth->user('id'));

							$this->Assessment->contain();
							$assessment = $this->Assessment->findById($assessment_id);
							$sourceFolderId = $assessment['Assessment']['folder_id'];
	
							if (!in_array('can_add', $targetPermissions['all'])) return false;
							if ($this->Folder->getCommunityId($sourceFolderId) != $this->Folder->getCommunityId($targetFolderId)) {
								if (!in_array('can_export', $itemPermissions)) return false;
							}
							if ($this->action == 'move') {
								if (!in_array('can_delete', $itemPermissions)) return false;
							}
						} else return false;
					} else return false;
				}
				return true;
			} else return false;
		}
		
		if (in_array($this->action, array('replace_handle'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$assessment_id = $this->request->params['pass'][0];
				$itemPermissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
				if (in_array('can_edit', $itemPermissions)) {
					if (isset($this->request->data['old_question_handle_id']) && is_numeric($this->request->data['old_question_handle_id'])) {
						$old_question_handle_id = intval($this->request->data['old_question_handle_id']);
					} else return false;
					
					if (isset($this->request->data['new_question_handle_id']) && is_numeric($this->request->data['new_question_handle_id'])) {
						$this->loadModel('QuestionHandle');
						$new_question_handle_id = intval($this->request->data['new_question_handle_id']);
						$handlePermissions = $this->QuestionHandle->getPermissions($new_question_handle_id, $this->Auth->user('id'));
						return (in_array('can_view', $itemPermissions));
					} else return false;
				} else return false;
			} else return false;
		}

		if (in_array($this->action, array('delete_handle'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
                $assessment_id = $this->request->params['pass'][0];
                if (isset($this->request->data['question_handle_id']) && is_numeric($this->request->data['question_handle_id'])) {
                    $itemPermissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
                    return (in_array('can_edit', $itemPermissions));
				} else return false;
			} else return false;
		}

		if (in_array($this->action, array('delete'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$assessment_id = $this->request->params['pass'][0];
				$itemPermissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
				return (in_array('can_delete', $itemPermissions));
			} else return false;
		}

        if (in_array($this->action, array('smart_fill'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
                $this->loadModel("Assessment");

                $assessment_id = $this->request->params['pass'][0];
                $permissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
                if (!in_array('can_edit', $permissions)) return false;    
            } else return false;
			

            if (isset($this->request->data['folder_id'])) {
                $this->loadModel('Folder');

                $folder_id = $this->request->data['folder_id'];
                if (is_numeric($folder_id)) {
                    $permissions = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
                    if (!in_array('can_view', $permissions['all'])) return false;
                } else if ($folder_id !== 'open') return false;
            } else return false;

            return true;
        }

		$needs_view = array('get', 'build', 'build_version', 'build_preview');
		$needs_edit = array('save');
		if (in_array($this->action, array_merge($needs_view, $needs_edit))) {
			
			$assessment_id = null;
			if ($this->action == 'build_version') {
				if (isset($this->request->data['target_folder']) && is_numeric($this->request->data['target_folder'])) {					
					$this->loadModel('Folder');
					$folderId = $this->request->data['target_folder'];
					$permissions = $this->Folder->getContentPermissions($folderId, $this->Auth->user('id'));
					if (!in_array('can_add', $permissions['all'])) return false;
				} else return false;
			}
			
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$assessment_id = $this->request->params['pass'][0];
			} else return false;
			
			$this->loadModel("Assessment");
			$permissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
			if (in_array($this->action, $needs_edit)) return in_array('can_edit', $permissions);
			else return in_array('can_view', $permissions);
		}
		
		if ($this->action == 'get_unlinked') {
			$this->loadModel("Assessment");
			$assessment_ids = explode(',', $this->request->query['assessment_ids']);
			
			$hasPermission = true;
			foreach ($assessment_ids as $assessment_id) {
				$permissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
				if (!in_array('can_view', $permissions)) $hasPermission = false;
			}
			return $hasPermission;
		}

		return false;
	}

	// Index interface

	public function index() {
		$adminView = ($this->Auth->user('role') == 'admin');

		if ($adminView) {

			$jsonTree = "";
            $jsonQuestionTree = "";

		} else {

			$this->loadModel('Folder');
			$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'assessments');
			$tree = $this->Folder->getTree($nodes);
			$jsonTree = json_encode($tree);

			$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'questions');
			$tree = $this->Folder->getTree($nodes);
			$jsonQuestionTree = json_encode($tree);
            
		}

		$this->loadModel('User');
		$isExpired = $this->User->isExpired($this->Auth->user('id'));

		// Find any assessments that have been sent to this user (is_new == 3)

		if ($this->Auth->user('role') != 'admin') {
			$receivedCount = $this->Assessment->find('count', array(
				'conditions' => array(
					'Assessment.user_id' => $this->Auth->user('id'),
					'Assessment.is_new' => 3
				)
			));
		} else $receivedCount = 0;

		// Get contacts

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$this->User->contain();
		$contactRecords = $this->User->find('all', array(
			'conditions' => array('User.id' => $contact_ids))
		);
		$contacts = array();
		foreach ($contact_ids as $id) {
			foreach ($contactRecords as $record) {
				if ($record['User']['id'] == $id) {
					$contacts[] = array(
						'id' => $record['User']['id'],
						'name' => $record['User']['name'],
						'email' => $record['User']['email']
					);
				}
			}
		}
				
		// Set variables for template
		
		$this->set(compact('adminView', 'jsonTree', 'jsonQuestionTree', 'isExpired', 'receivedCount', 'contacts'));

		$this->set('colCreated', 'Assessment.moved');
		$this->set('colName', 'Assessment.save_name');
		$this->set('itemType', 'Assessment');
		$this->set('index_element', '../Assessments/index');

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		$this->viewPath = 'Elements';
		$this->render('index_common');
	}

	public function get_index() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$query_params = array('conditions' => array(
			'Assessment.deleted' => null
		));
		$userDefaults = json_decode($this->Auth->user('defaults'), true);

		if (isset($this->request->query['sort'])) {
			$sortBy = $this->request->query['sort'];
		} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$sortBy = 'Assessment.save_name';
		} else {
			$sortBy = 'Assessment.moved';
		}

		if (isset($this->request->query['direction'])) {
			$direction = $this->request->query['direction'];
		} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$direction = 'asc';
		} else {
			$direction = 'desc';
		}
		$query_params['order'] = array($sortBy => $direction);
		
		if (isset($this->request->query['folder_ids'])) {

			$folder_ids = explode(',', $this->request->query['folder_ids']);
			$query_params['conditions']['Assessment.folder_id'] = $folder_ids;

            $count = $this->Assessment->find('count', $query_params);
            $resp['count_total'] = $count;
            $resp['count_matching'] = $count;
    
		} else if ($this->Auth->user('role') == 'admin') {

			$query_params['conditions'][] = ['Assessment.folder_id IS NOT NULL'];
			$query_params['contain'] = array('User');

		} else $success = false;

		if (isset($this->request->query['search'])) {
			$query_params['conditions'][] = array('OR' => array(
				'Assessment.save_name LIKE' => '%' . $this->request->query['search'] . '%',
				'Assessment.id' => $this->request->query['search']
			));
			$count = $this->Assessment->find('count', $query_params);
            $resp['count_matching'] = $count;
		}

		if (isset($this->request->query['limit'])) {
			if (is_numeric($this->request->query['limit']) && ($this->request->query['limit'] > 0)) {
				$query_params['limit'] = intval($this->request->query['limit']);
			} else if ($this->request->query['limit'] !== 'all') {
                $success = false;
            }
		} else $query_params['limit'] = 10;

		if (isset($this->request->query['page'])) {
			if (is_numeric($this->request->query['page']) && ($this->request->query['page'] > 0)) {
				$query_params['page'] = intval($this->request->query['page']);
			} else $success = false;
		} else $query_params['page'] = 1;	

		if ($success) {
			$results = $this->Assessment->find('all', $query_params);	

			if ($this->Auth->user('role') != 'admin') {

				// Get permissions for each result

				$folder_permissions = array();
				foreach ($results as &$pResult) {
					$folder_id = $pResult['Assessment']['folder_id'];
					if (!array_key_exists($folder_id, $folder_permissions)) {
						$folder_permissions[$folder_id] = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
					}

					if ($pResult['Assessment']['user_id'] == $this->Auth->user('id')) {
						$pResult['Permissions'] = $folder_permissions[$folder_id]['owner'];
					} else $pResult['Permissions'] = $folder_permissions[$folder_id]['all'];
				}

				// Update "is_new" for items

				$visible = array();
				foreach ($results as $result) {
					$visible[] = $result['Assessment']['id'];
				}
				$this->Assessment->updateAll(
					array('Assessment.is_new' => 0),
					array('Assessment.id' => $visible)
				);
				$this->Assessment->updateAll(
					array('Assessment.is_new' => 2),
					array(
						'Assessment.user_id' => $this->Auth->user('id'),
						'Assessment.is_new' => 3
					)
				);
			}

			$required_keys = array('id', 'moved', 'folder_id', 'save_name', 'is_new');
			foreach ($results as &$pResult) {
				foreach ($pResult['Assessment'] as $key => $value) {
					if (!in_array($key, $required_keys)) unset($pResult['Assessment'][$key]);
				}
			}

			$resp['results'] = $results;
		}

		$resp['page'] = $query_params['page'];
        if (array_key_exists('limit', $query_params)) {
            $resp['limit'] = $query_params['limit'];
        }
		$resp['success'] = $success;

		echo json_encode($resp);
	}

	public function create() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$this->loadModel('User');
		if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {

			$resp['message'] = 'Your account has expired. Unable to create new assessments.';
			$success = false;

		} else {

			$folder_id = $this->request->data['folder_id'];

			if (array_key_exists('json_data', $this->request->data)) {
	
				$json_data = json_decode($this->request->data['json_data'], true);
	
			} else {
	
				$defaults = json_decode($this->Auth->user('defaults'), true);
	
				$json_data = array(
					'Settings' => array(
						'pageSize' => $defaults['Common']['Settings']['pageSize'],
						'marginSizes' => $defaults['Common']['Settings']['marginSizes'],
						'numberingType' => $defaults['Assessment']['Settings']['numberingType'],
						'wrResponseLocation' => $defaults['Assessment']['Settings']['wrResponseLocation'],
						'mcFormat' => $defaults['Assessment']['Settings']['mcFormat'],
						'language' => $defaults['Common']['Settings']['language']
					),
					'Title' => array(
						'text' => array()
					),
					'Sections' => array(),
					'Outcomes' => array(),
					'Scoring' => array('type' => 'Total')
				);
	
                if (!empty($defaults['Common']['Title']['nameLines'])) {
                    $json_data['Title']['nameLines'] = $defaults['Common']['Title']['nameLines'];
                }

				if ($defaults['Common']['Title']['logo']['imageHash'] !== false) {
					$json_data['Title']['logo'] = array(
						'imageHash' => $defaults['Common']['Title']['logo']['imageHash'],
						'height' => $defaults['Common']['Title']['logo']['height']
					);
				}
				
			}
	
			$data = array(
				'save_name' => "New Assessment",
				'user_id' => $this->Auth->user('id'),
				'folder_id' => $folder_id,
				'moved' => date('Y-m-d H:i:s'),
				'is_new' => 1,
				'json_data' => json_encode($json_data)
			);
	
			$this->Assessment->create();
			if (!empty($this->Assessment->save($data))) {
				$resp['id'] = $this->Assessment->id;
			} else $success = false;

		}

		$resp['success'] = $success;

		echo json_encode($resp);
	}

	public function send() {
		$this->autoRender = false;
		$this->layout = '';
		
		$this->loadModel('User');

		$success = true;

		$email_array = $this->request->data['email_list'];
		$item_ids = $this->request->data['item_ids'];

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$toUsers = array();
		foreach ($email_array as $email) {
			$email = trim($email);
			if (strlen($email) == 0) continue;
			
			$this->User->contain();
			$toUser = $this->User->findByEmail($email);
			if ($toUser && ($toUser['User']['role'] == 'teacher')) {
				$toUsers[] = $toUser;
				
				$userID = $toUser['User']['id'];
				if (($key = array_search($userID, $contact_ids)) !== false) {
					unset($contact_ids[$key]);
				}
				array_unshift($contact_ids, $userID);
			} else $success = false;
		}

		// Update contact IDs
		
		$newUserData = array();
		$newUserData['id'] = $this->Auth->user('id');
		$newUserData['contact_ids'] = implode(',', $contact_ids);
		$this->updateUserData($newUserData);

		// Send assessments

		$this->loadModel('Folder');

		foreach ($toUsers as $toUser) {
			$userID = $toUser['User']['id'];
			$folder_id = $this->Folder->getUserFolder($userID, 'assessments', 'home');

			foreach ($item_ids as $assessment_id) {
				$result = $this->Assessment->copyToFolder($assessment_id, $folder_id, $userID);
				if ($result['success']) {
					$this->Assessment->id = $result['assessment_id'];
					$this->Assessment->saveField('is_new', 3);
				} else $success = false;
			}
		}
		
		echo json_encode(
			array(
			    'success' => $success,
				'numSent' => count($item_ids)
			)
		);
	}

	public function copy() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$folder_id = $this->request->data['folder_id'];
		$item_ids = $this->request->data['item_ids'];

		if (isset($this->request->data['is_new'])) {
			$isNew = $this->request->data['is_new'];
		} else $isNew = 2;
		
		foreach ($item_ids as $assessment_id) {
			$result = $this->Assessment->copyToFolder($assessment_id, $folder_id, $this->Auth->user('id'));
			if ($result['success']) {
				$this->Assessment->id = $result['assessment_id'];
				$this->Assessment->saveField('is_new', $isNew);

				if ($result['source_folder_id'] == $result['target_folder_id']) {
					$this->Assessment->contain();
					$assessment = $this->Assessment->findById($result['assessment_id']);
					$this->Assessment->saveField('save_name', $assessment['Assessment']['save_name'] . ' - Copy');
				}
			} else $success = false;
		}

		$resp['success'] = $success;

		if ($success) {
			$resp['count'] = count($item_ids);
			
			if (count($item_ids) == 1) {
				$resp['id'] = $result['assessment_id'];
			} else $resp['folder_id'] = $folder_id;
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function move() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$folder_id = $this->request->data['folder_id'];
		$item_ids = $this->request->data['item_ids'];

		$this->loadModel('Folder');
		$trash_folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'assessments', 'trash');
		$isTrash = ($folder_id == $trash_folder_id);

		foreach ($item_ids as $assessment_id) {
			$data = array(
				'id' => $assessment_id,
				'user_id' => $this->Auth->user('id'),
				'folder_id' => $folder_id,
				'moved' => date('Y-m-d H:i:s')
			);
			if (!$isTrash) $data['is_new'] = 2;

			$success &= !empty($this->Assessment->save($data));
			$success &= $this->Assessment->fixHandles($assessment_id);
		}

		$resp['success'] = $success;

		if ($success) {
			$resp['count'] = count($item_ids);
			$resp['folder_id'] = $folder_id;
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function delete($assessment_id) {
		$this->autoRender = false;
		$this->layout = '';
		
		$resp = array();
		$success = true;

		$success &= $this->Assessment->delete($assessment_id);

		$resp['success'] = $success;

        echo json_encode($resp);
	}

	public function empty_trash() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;

		$this->loadModel('Folder');
		$folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'assessments', 'trash');

		$trashAssessments = $this->Assessment->find('list', array(
			'fields' => array('Assessment.id'),
			'conditions' => array('Assessment.folder_id' => $folder_id)
		));
		
		$deleted_date = date('Y-m-d H:i:s');
		foreach ($trashAssessments as $assessment_id) {
			$this->Assessment->id = $assessment_id;
			$success &= !empty($this->Assessment->saveField('deleted', $deleted_date));
		}

		echo json_encode(
			array(
		   		'success' => $success
		   	)
		);
	}	

	public function build($assessment_id) {

		$assessment = $this->Assessment->findById($assessment_id);

		if (!$assessment) {

			$this->Flash->error("Invalid assessment");
			$this->redirect_home();

		} else if ($this->Auth->user('role') == 'admin') {

			$canAdd = 1;
			$canEdit = 1;
			$canCopy = 1;
			$canCancel = 0;
			$showSettings = 0;
			$canBuild = 0;

            $classLists = array();

		} else {
			
            $this->User->contain('ClassList');
            $user = $this->User->findById($this->Auth->user('id'));
            $classLists = $user['ClassList'];

			$permissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
			$canAdd = in_array('can_add', $permissions) ? 1 : 0;
			$canEdit = in_array('can_edit', $permissions) ? 1 : 0;
			$canCopy = in_array('can_export', $permissions) ? 1 : 0;
			$canBuild = 1;

			$this->loadModel('User');
			if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {
				$canEdit = 0;
				$canBuild = 0;
			}

			$canCancel = 0;
			$showSettings = 0;
			if ($assessment['Assessment']['is_new'] == 1) {             // Document can be cancelled
				$canCancel = 1;
			}
			if ($assessment['Assessment']['is_new'] > 0) {              // Clear sent and copied flags
				$showSettings = 1;

				$this->Assessment->id = $assessment['Assessment']['id'];
				$this->Assessment->saveField('is_new', 0);
			}

			$this->loadModel('Folder');
			$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'questions');
			$nodes[] = array(
				'id' => 'open',
				'data' => array(
					'parent_id' => null,
					'folder_id' => 'open'
				),
				'text' => 'Open bank',
				'type' => 'open'
			);
			$tree = $this->Folder->getTree($nodes);
			$jsonTree = json_encode($tree);
	
			$this->loadModel('Folder');

			$formNodes = $this->Folder->getNodes($this->Auth->user('id'), 'forms');
			$formTree = $this->Folder->getTree($formNodes);
			$jsonFormTree = json_encode($formTree);

			$formFolderId = $this->Folder->getUserFolder($this->Auth->user('id'), 'forms', 'home');
			
			$sittingNodes = $this->Folder->getNodes($this->Auth->user('id'), 'sittings');
			$sittingTree = $this->Folder->getTree($sittingNodes);
			$jsonSittingTree = json_encode($sittingTree);

			$sittingFolderId = $this->Folder->getUserFolder($this->Auth->user('id'), 'sittings', 'home');

			$folder_id = $assessment['Assessment']['folder_id'];
			$communityId = $this->Folder->getCommunityId($folder_id);
			
			if ($communityId == null) {
				$questionFolderId = $this->Folder->getUserFolder($this->Auth->user('id'), 'questions', 'home');
			} else {
				$this->loadModel('Community');
				$this->Community->contain();
				$community = $this->Community->findById($communityId);
				$community_ids = json_decode($community['Community']['folder_ids'], true);
				$questionFolderId = $community_ids['questions'];
			}
	
			$this->set(compact('jsonTree', 'communityId', 'questionFolderId', 'jsonFormTree', 'formFolderId', 
				'jsonSittingTree', 'sittingFolderId'));
		}
		
		$this->set(compact('assessment_id', 'canBuild', 'canEdit', 'canCopy', 'canAdd', 
			'canCancel', 'showSettings', 'classLists'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}
	
	public function build_version($assessment_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;

		// Get scoring data

		$this->Assessment->contain();
		$assessment = $this->Assessment->findById($assessment_id);
		$assessment_data = json_decode($assessment['Assessment']['json_data'], true);
		
        $handle_data = array();

        $html = $this->request->data['html'];
		$scoring_data = json_decode($this->request->data['scoring_sections'], true);

		$this->loadModel('VersionData');
		$success &= $this->VersionData->finishVersion($scoring_data, $html,
			$handle_data, $assessment_data, $this->Auth->user('id'));

		if ($success) {

			// Get layout data

			$defaults = json_decode($this->Auth->user('defaults'), true);

			$layout_data = array(
				'Settings' => array(
                    'version' => $defaults['Document']['Settings']['version'],
					'numberingType' => $assessment_data['Settings']['numberingType'],
					'mcFormat' => $assessment_data['Settings']['mcFormat'],
					'language' => $assessment_data['Settings']['language']
				),
				'Page' => array(
					'pageSize' => $assessment_data['Settings']['pageSize'],
					'marginSizes' => $assessment_data['Settings']['marginSizes'],
					'copiesPerPage' => $defaults['Document']['Settings']['copiesPerPage']
				),
				'Title' => array(
					'text' => explode("\n", $this->request->data['title'])
				),
				'Body' => json_decode($this->request->data['layout_sections'], true),
				'StudentReports' => $defaults['Document']['Reports']['Student']
			);

            if ($layout_data['StudentReports']['responses'] == 'none') {
                unset($layout_data['StudentReports']['incorrectOnly']);
            }
    
            if ($layout_data['StudentReports']['results'] == 'numerical') {
                unset($layout_data['StudentReports']['outcomesLevels']);
            }

			if (array_key_exists('logo', $assessment_data['Title'])) {
				$layout_data['Title']['logo'] = array(
					'imageHash' => $assessment_data['Title']['logo']['imageHash'],
					'height' => $assessment_data['Title']['logo']['height']
				);
			}

			if (array_key_exists('nameLines', $assessment_data['Title'])) {
				$layout_data['Title']['nameLines'] = $assessment_data['Title']['nameLines'];
			}

			if ($defaults['Document']['Title']['idField']['show']) {
				$layout_data['Title']['idField'] = array(
					'label' => $defaults['Document']['Title']['idField']['label'],
					'bubbles' => $defaults['Document']['Title']['idField']['bubbles']
				);
			}

			if ($defaults['Document']['Title']['nameField']['show']) {
				$layout_data['Title']['nameField'] = array(
					'label' => $defaults['Document']['Title']['nameField']['label'],
					'bubbles' => $defaults['Document']['Title']['nameField']['bubbles']
				);
			}

			// Build scoring data
			
			$data = array(
//				'html' => $html,
				'html_gzip' => gzencode($html),
				'scoring_json' => json_encode($scoring_data)
			);

			$this->loadModel('VersionData');
			$this->VersionData->create();
			$success &= !empty($this->VersionData->save($data));
			
		}

		if ($success) {

			// Build document

			$data = array(
				'user_id' => $this->Auth->user('id'),
				'folder_id' => $this->request->data['target_folder'],
				'moved' => date('Y-m-d H:i:s'),
				'is_new' => 1,
				'save_name' => 'New Version',
				'version_data_id' => $this->VersionData->id,
				'layout_json' => json_encode($layout_data)
			);

			$this->loadModel('Document');
			$this->Document->create();
			if (empty($this->Document->save($data))) {
				$this->VersionData->delete($this->VersionData->id);
				$success = false;	
			}
		}

		$resp['success'] = $success;

		if ($success) {
			$resp['documentID'] = $this->Document->id;
		}

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function build_preview($assessment_id) {
		$this->autoRender = false;
	   	$this->layout = '';

		$success = true;

		$this->Assessment->contain();
		$assessment = $this->Assessment->findById($assessment_id);
		$assessment_data = json_decode($assessment['Assessment']['json_data'], true);

		$page_settings = $assessment_data['Settings'];

		$assessmentHTML = $this->request->data['html'];
		
		// Build HTML

		$view = new View($this, false);
		$view->layout = false;
		$view->viewPath='Assessments';
		$view->set(compact('page_settings', 'assessmentHTML'));
		$view->set('mc_format', $page_settings['mcFormat']);
		$view->set('title_for_layout', $assessment['Assessment']['save_name']);
		$viewHTML = $view->render('build_preview');

		if (array_key_exists('header', $this->request->data)) {
			$page_settings['header'] = $this->request->data['header'];
		}

		// Build and return PDF

        $this->html_to_pdf_response($viewHTML, $page_settings);
	}
	
	public function save($assessment_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$success = true;
	   	$resp = array();

		if (array_key_exists('version_id', $this->request->data)) {
			$sent_version_id = $this->request->data['version_id'];
			$cache_version_id = Cache::read('assessments_' . $assessment_id, 'version_ids');
			if (($cache_version_id === false) || ($sent_version_id == $cache_version_id)) {
				$version_id = md5(microtime() . $assessment_id);
				Cache::write('assessments_' . $assessment_id, $version_id, 'version_ids');	
				$resp['version_id'] = $version_id;
			} else {
				$resp['error_type'] = 'version_conflict';
				$success = false;
			}
		}

		$has_save_name = !empty($this->request->data['save_name']);

		$has_json_data = !empty($this->request->data['data_string']);
		if ($has_json_data) {
			$dataString = $this->request->data['data_string'];

			if (empty($this->request->data['data_hash'])) $success = false;
			else if ($this->request->data['data_hash'] != md5($dataString)) $success = false;
			else $new_data = json_decode($dataString, true);
		} else $success = false;

		$this->loadModel('AssessmentItem');

		if ($success && !empty($this->request->data['new_items'])) {
			$new_items = $this->request->data['new_items'];

			// Add any new items

			$this->loadModel('Question');
			$this->loadModel('Folder');
			$this->loadModel('QuestionHandle');

			$handleData = array();
			foreach ($new_items as $new_item) {
				$isGood = !empty($new_item['content_id']);
				if (!empty($new_item['question_handle_id'])) {
				} else if (!empty($new_item['question_id'])) {
					if (!array_key_exists('name', $new_item)) $isGood = false;
					if (!array_key_exists('from_open', $new_item)) $isGood = false;
				} else $isGood = false;
				
				if ($isGood) {

					// Get handle data for this assessment

					if (empty($handleData)) {
						$this->Assessment->contain();
						$assessment = $this->Assessment->findById($assessment_id);
						$folder_id = $assessment['Assessment']['folder_id'];

						$targetCommunity = $this->Folder->getCommunityId($folder_id);
						if ($targetCommunity) $handleData['community_id'] = $targetCommunity;
						else $handleData['user_id'] = $this->Auth->user('id');
					}
					$data = $handleData;

					// Get handle data

                    $name_alias = false;
					if (isset($new_item['question_handle_id'])) {
						$question_handle_id = $new_item['question_handle_id'];

						$this->QuestionHandle->contain();
						$question_handle = $this->QuestionHandle->findById($question_handle_id);
						if (!empty($question_handle)) {
							$question_id = intval($question_handle['QuestionHandle']['question_id']);
							$name_alias = $question_handle['QuestionHandle']['name_alias'];		
							$fromOpen = false;
						} else $success = false;
					} else {
						$question_id = intval($new_item['question_id']);
						$name_alias = $new_item['name'];
						$fromOpen = $new_item['from_open'];
					}
                    $data['name_alias'] = $name_alias;

					if ($success) {

						$data['question_id'] = $question_id;
						$question_handle_id = $this->QuestionHandle->createHandle($data, $fromOpen);
						
						$data = array();
						$data['question_handle_id'] = $question_handle_id;
						$data['assessment_id'] = $assessment_id;

						$this->AssessmentItem->create();
						if (!empty($this->AssessmentItem->save($data))) {

							// Add new item to response

                            $question = $this->Question->get($question_id);
                            if ($name_alias !== false) $question['name'] = $name_alias;
                            $question['question_handle_id'] = $question_handle_id;

							if (!isset($resp['new_items'])) $resp['new_items'] = array();
							$resp['new_items'][] = $question;

							// Finish up

							foreach ($new_data['Sections'] as &$pSection) {
								if ($pSection['type'] == 'page_break') continue;
				
								foreach ($pSection['Content'] as &$pEntry) {
									if ($pEntry['id'] == $new_item['content_id']) {
										if ($pEntry['type'] == 'item') {
											$pEntry['question_handle_id'] = $question_handle_id;
										} else if ($pEntry['type'] == 'item_set') {
											$pEntry['question_handle_ids'][] = $question_handle_id;
										}
									}
								}				
							}

							$this->Question->id = $question_id;
                            $this->Question->saveField('needs_index', date('Y-m-d H:i:s'));

						} else $success = false;
					}
				} else $success = false;				
			}

			$json_string = json_encode($new_data);
			$resp['data_string'] = $json_string;
			$resp['data_hash'] = md5($json_string);
		}

		if ($success) {

			// Check for required AssessmentItems

			$this->AssessmentItem->contain('QuestionHandle');
			$assessment_items = $this->AssessmentItem->find('list', array(
				'fields' => array('QuestionHandle.id', 'AssessmentItem.id'),
				'conditions' => array('AssessmentItem.assessment_id' => $assessment_id)
			));

			foreach ($new_data['Sections'] as &$pSection) {
				if ($pSection['type'] == 'page_break') continue;
				
				foreach ($pSection['Content'] as &$pEntry) {
					$entryType = $pEntry['type'];
					if ($entryType == 'item') {

						if (isset($pEntry['question_handle_id'])) {
							$question_handle_id = $pEntry['question_handle_id'];
							if (isset($assessment_items[$question_handle_id])) {
								unset($assessment_items[$question_handle_id]);
							} else $success = false;
						} else $success = false;

					} else if ($entryType == 'item_set') {

						if (isset($pEntry['question_handle_ids'])) {
							foreach ($pEntry['question_handle_ids'] as $question_handle_id) {
								if (isset($assessment_items[$question_handle_id])) {
									unset($assessment_items[$question_handle_id]);
								} else $success = false;
							}
						} else $success = false;

					}
				}
			}
		}

		if ($success) {

			// Delete AssessmentItems that are no longer being used

			$retired_item_ids = array_values($assessment_items);
			$this->AssessmentItem->deleteAll(array('AssessmentItem.id' => $retired_item_ids), true, true);

			// Save assessment template

			$data = array(
				'id' => $assessment_id,
				'is_new' => 0,
				'json_data' => json_encode($new_data)
			);

			if ($has_save_name) {
				$data['save_name'] = $this->request->data['save_name'];
			}
	
			$success &= !empty($this->Assessment->save($data));
		}

		$resp['id'] = intval($assessment_id);
        $resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function get($assessment_id) {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		// Load assessment data

		$this->Assessment->contain(array('AssessmentItem' => 'QuestionHandle'));
		$assessment = $this->Assessment->find('first', array(
			'conditions' => array('id' => $assessment_id),
		));

		// Remove assessment quetions with no corresponding AssessmentItem

		$allowed_handle_ids = array();
		foreach ($assessment['AssessmentItem'] as $assessment_item) {
			$allowed_handle_ids[] = $assessment_item['question_handle_id'];
		}

        $json_data = json_decode($assessment['Assessment']['json_data'], true);

		$removed_handles = array();
		foreach ($json_data['Sections'] as &$pSection) {
			if ($pSection['type'] == 'page_break') continue;
			
			foreach ($pSection['Content'] as $key => &$pEntry) {
				if ($pEntry['type'] == 'item') $question_handle_ids = array($pEntry['question_handle_id']);
				else if ($pEntry['type'] == 'item_set') $question_handle_ids = $pEntry['question_handle_ids'];
				else continue;

				foreach ($question_handle_ids as $index => $question_handle_id) {
					if (!in_array($question_handle_id, $allowed_handle_ids)) {

                        // Remove item that's not in the set of AssessmentItems

						$removed_handles[] = $question_handle_id;
						unset($question_handle_ids[$index]);
                        if (empty($question_handle_ids)) {

                            // Clean up associated outcomes

                            foreach ($json_data['Outcomes'] as &$pOutcome) {
                                foreach ($pOutcome['parts'] as $part_index => $part) {
                                    if ($part['content_id'] == $pEntry['id']) {
                                        unset($pOutcome['parts'][$part_index]);
                                    }
                                }
                                $pOutcome['parts'] = array_values($pOutcome['parts']);
                            }
                        }
					}	
				}

				if (empty($question_handle_ids)) {
					unset($pSection['Content'][$key]);
				} else if ($pEntry['type'] == 'item_set') {
					if (count($question_handle_ids) == 1) {
						unset($pEntry['question_handle_ids']);
						$pEntry['question_handle_id'] = $question_handle_ids[0];
						$pEntry['type'] = 'item';
					} else {
						$pEntry['question_handle_ids'] = array_values($question_handle_ids);
					}
				}
			}

			$pSection['Content'] = array_values($pSection['Content']);
		}

		if (!empty($removed_handles)) {
			$this->Assessment->id = $assessment_id;
			$this->Assessment->saveField('json_data', json_encode($json_data));

			App::uses('Email', 'Lib');
			Email::queue_error("SmarterMarks: Removing items in assessment", $this->Auth->user('email'), array(
				'assessment_id' => $assessment_id,
				'removed handle IDs' => $removed_handles
			));
		}

		// Add assessment data to response

		$resp['Assessment'] = array(
			'save_name' => $assessment['Assessment']['save_name']
		);

		$itemData = array();
		foreach ($assessment['AssessmentItem'] as $assessmentItem) {
			$question_id = intval($assessmentItem['QuestionHandle']['question_id']);
			$itemData[$question_id] = array(
				'question_handle_id' => intval($assessmentItem['QuestionHandle']['id']),
				'name' => $assessmentItem['QuestionHandle']['name_alias']
			);
		}

		$json_string = json_encode($json_data);
		$resp['Assessment']['data_string'] = $json_string;
		$resp['Assessment']['data_hash'] = md5($json_string);

		$version_id = Cache::read('assessments_' . $assessment_id, 'version_ids');
		if ($version_id === false) {
			$version_id = md5(microtime() . $assessment_id);
			Cache::write('assessments_' . $assessment_id, $version_id, 'version_ids');	
		}
		$resp['Assessment']['version_id'] = $version_id;

		// Load question data

		$this->loadModel('Question');

		$question_ids = array_keys($itemData);
		$questions = $this->Question->get($question_ids);

		foreach ($questions as &$pEntry) {
			$question_id = $pEntry['id'];
			$pEntry = array_merge($pEntry, $itemData[$question_id]);
		}

		$resp['Questions'] = $questions;

		// Load statistics

		$resp['Statistics'] = $this->Question->getStatistics($question_ids);

		// Send response

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function replace_handle($assessment_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$success = true;
	   	$resp = array();
   
		if (isset($this->request->data['old_question_handle_id'])) {
			$old_question_handle_id = intval($this->request->data['old_question_handle_id']);
		} else $success = false;
		
		if (isset($this->request->data['new_question_handle_id'])) {
			$new_question_handle_id = intval($this->request->data['new_question_handle_id']);
		} else $success = false;

		$this->Assessment->contain();
		$assessment = $this->Assessment->findById($assessment_id);
		if (empty($assessment)) $success = false;

		if ($success) {
			$this->loadModel('AssessmentItem');
			$this->AssessmentItem->updateAll(array(
				'AssessmentItem.question_handle_id' => $new_question_handle_id				
			), array(
				'AssessmentItem.assessment_id' => $assessment_id,
				'AssessmentItem.question_handle_id' => $old_question_handle_id
			));
		}

		if ($success) {
			$json_data = json_decode($assessment['Assessment']['json_data'], true);

			foreach ($json_data['Sections'] as &$pSection) {
				if ($pSection['type'] == 'page_break') continue;
				
				foreach ($pSection['Content'] as &$pEntry) {
					if (($pEntry['type'] == 'item') && ($pEntry['question_handle_id'] == $old_question_handle_id)) {
						$pEntry['question_handle_id'] = $new_question_handle_id;
					} else if ($pEntry['type'] == 'item_set') {
						foreach ($pEntry['question_handle_ids'] as $index => $question_handle_id) {
							if ($question_handle_id == $old_question_handle_id) {
								$pEntry['question_handle_ids'][$index] = $new_question_handle_id;
							}
						}
					}
				}
			}

			$this->Assessment->id = $assessment_id;
			$this->Assessment->saveField('json_data', json_encode($json_data));

			$version_id = md5(microtime() . $assessment_id);
			Cache::write('assessments_' . $assessment_id, $version_id, 'version_ids');

            $resp['assessment_version_id'] = $version_id;
		}

        $resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

    public function delete_handle($assessment_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$success = true;
	   	$resp = array();
   
		if (isset($this->request->data['question_handle_id'])) {
			$question_handle_id = intval($this->request->data['question_handle_id']);
		} else $success = false;
		
		$this->Assessment->contain();
		$assessment = $this->Assessment->findById($assessment_id);
		if (empty($assessment)) $success = false;

		if ($success) {
			$this->loadModel('AssessmentItem');
			$this->AssessmentItem->deleteAll(array(
				'AssessmentItem.assessment_id' => $assessment_id,
				'AssessmentItem.question_handle_id' => $question_handle_id
			));
		}

		if ($success) {
			$json_data = json_decode($assessment['Assessment']['json_data'], true);

			foreach ($json_data['Sections'] as &$pSection) {
				if ($pSection['type'] == 'page_break') continue;
				
				foreach ($pSection['Content'] as $i => &$pEntry) {
					if (($pEntry['type'] == 'item') && ($pEntry['question_handle_id'] == $question_handle_id)) {
                        unset($pSection['Content'][$i]);
                        $pSection['Content'] = array_values($pSection['Content']);
					} else if ($pEntry['type'] == 'item_set') {
                        if (($key = array_search($question_handle_id, $pEntry['question_handle_ids'])) !== false) {
                            unset($pEntry['question_handle_ids'][$key]);
                            $pEntry['question_handle_ids'] = array_value($pEntry['question_handle_ids']);
                        }
					}
				}
			}

			$this->Assessment->id = $assessment_id;
			$this->Assessment->saveField('json_data', json_encode($json_data));

			$version_id = md5(microtime() . $assessment_id);
			Cache::write('assessments_' . $assessment_id, $version_id, 'version_ids');

            $resp['assessment_version_id'] = $version_id;
		}

        $resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function get_unlinked() {
		$this->autoRender = false;
	   	$this->layout = '';

		$success = true;
		$resp = array();
		
		$assessment_ids = explode(',', $this->request->query['assessment_ids']);
		$this->Assessment->contain(array('AssessmentItem' => array('QuestionHandle' => 'UserQuestion')));
		$assessments = $this->Assessment->find('all', array(
			'conditions' => array(
				'Assessment.id' => $assessment_ids
			)
		));
		
		$numLinked = 0;
		$unlinkedHandles = array();
		foreach ($assessments as $assessment) {
			foreach ($assessment['AssessmentItem'] as $item) {
				if ($item['question_handle_id'] != null) {
					$isLinked = false;
					foreach ($item['QuestionHandle']['UserQuestion'] as $userQuestion) {
						if ($userQuestion['deleted'] == null) $isLinked = true;
					}
					if (!$isLinked) {
						unset($item['QuestionHandle']['UserQuestion']);
						$unlinkedHandles[] = $item['QuestionHandle'];
					} else $numLinked++;
				}
			}
		}
		
		$resp['numLinked'] = $numLinked;
		$resp['unlinked'] = $unlinkedHandles;
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function smart_fill($assessment_id) {
		$this->autoRender = false;
	   	$this->layout = '';

		$success = true;
		$resp = array();

        $this->Assessment->contain();
        $assessment = $this->Assessment->findById($assessment_id);
        if (!empty($assessment)) {
            $assessment_data = json_decode($assessment['Assessment']['json_data'], true);
        } else $success = false;

        $search_options = array();

        if (array_key_exists('allowed_types', $this->request->data)) {
            $allowed_types = $this->request->data['allowed_types'];
            if (count($allowed_types) < 3) {
                $search_options['allowed_types'] = $allowed_types;
            }
        } else $success = false;

        if (array_key_exists('folder_id', $this->request->data)) {
            $folder_id = $this->request->data['folder_id'];
            $search_options['folder_id'] = $folder_id;
        } else $success = false;

        if (array_key_exists('section_id', $this->request->data)) {
            $section_id = $this->request->data['section_id'];
            for ($section_index = 0; $section_index < count($assessment_data['Sections']); ++$section_index) {
                if ($assessment_data['Sections'][$section_index]['id'] == $section_id) break;
            }
            if ($section_index == count($assessment_data['Sections'])) $success = false;
        } else $success = false;

        if (array_key_exists('outcome_counts', $this->request->data)) {
    		$outcome_counts = $this->request->data['outcome_counts'];
        } else $success = false;

        $this->loadModel('Question');
        $this->loadModel('UserQuestion');

        if ($success) {

            $new_questions = array();
            foreach ($outcome_counts as $counts_entry) {

                $outcome_name = false;
                foreach ($assessment_data['Outcomes'] as $outcomes_entry) {
                    if ($outcomes_entry['id'] == $counts_entry['outcome_id']) {
                        $outcome_name = $outcomes_entry['name'];
                        break;
                    }
                }

                if ($outcome_name !== false) {
                    $search_options['limit'] = $counts_entry['add_count'];
                    $search_results = $this->Question->find_weaviate($outcome_name, $search_options);

                    if ($search_results !== false) {
                        foreach ($search_results as $question_content_id => $part_content_id) {

                            if ($folder_id == 'open') {

                                $this->Question->contain();
                                $raw_result = $this->Question->find('first', array(
                                    'conditions' => array(
                                        'Question.name !=' => '',
                                        'Question.question_content_id' => $question_content_id,
                                        'Question.open_status !=' => 0
                                    ),
                                    'order' => 'Question.stats_discrimination DESC'
                                ));

                                if (!empty($raw_result)) $find_result = $raw_result['Question'];
                                else $find_result = false;
    
                            } else {

                                $this->UserQuestion->contain(array('QuestionHandle' => 'Question'));
                                $raw_result = $this->UserQuestion->find('first', array(
                                    'conditions' => array(
                                        'Question.name !=' => '',
                                        'Question.question_content_id' => $question_content_id,
                                        'UserQuestion.folder_id' => $folder_id
                                    ),
                                    'order' => 'Question.stats_discrimination DESC',
                                    'joins' => array(
                                        array(
                                            'table' => 'questions',
                                            'alias' => 'Question',
                                            'type' => 'LEFT',
                                            'conditions' => array(
                                                'Question.id = QuestionHandle.question_id'
                                            )
                                        )
                                    )
                                ));

                                if (!empty($raw_result)) $find_result = $raw_result['QuestionHandle']['Question'];
                                else $find_result = false;

                            }
        
                            if ($find_result !== false) {
                                $question_data = json_decode($find_result['json_data'], true);
                                $content_ids = json_decode($find_result['content_ids'], true);
            
                                $matching_part_id = false;
                                if (!empty($question_data)) {
                                    foreach ($question_data['Parts'] as $part) {
                                        if ($content_ids[$part['id']] == $part_content_id) {
                                            $matching_part_id = $part['id'];
                                        }
                                    }
                                } else $success = false;
            
                                if ($matching_part_id !== false) {
                                    $question_id = $find_result['id'];
                                    $new_questions[$question_id] = array(
                                        'outcome_id' => $counts_entry['outcome_id'],
                                        'part_id' => $matching_part_id
                                    );
                                } else $success = false;
                            }
    
                        }
                    } else $success = false;
                } else $success = false;
            }
        }

        if ($success) {
            $this->loadModel('Folder');
            $this->loadModel('QuestionHandle');
            $this->loadModel('AssessmentItem');

            $folder_id = $assessment['Assessment']['folder_id'];

            $targetCommunity = $this->Folder->getCommunityId($folder_id);
            if ($targetCommunity) $handleData = array('community_id' => $targetCommunity);
            else $handleData = array('user_id' => $this->Auth->user('id'));

            $new_assessment_items = array();
            foreach ($new_questions as $question_id => $new_entry) {

                $question = $this->Question->get($question_id);

                $data = $handleData;
                $data['question_id'] = $question_id;
                $data['name_alias'] = $question['name'];

                $question_handle_id = $this->QuestionHandle->createHandle($data, true);
                
                $data = array();
                $data['question_handle_id'] = $question_handle_id;
                $data['assessment_id'] = $assessment_id;

                $this->AssessmentItem->create();
                if (!empty($this->AssessmentItem->save($data))) {
                    $new_assessment_items[] = $this->AssessmentItem->id;

                    // Add new item to response

                    $question['question_handle_id'] = $question_handle_id;
                    $question['Statistics'] = $this->Question->getStatistics($question_id);

                    if (!isset($resp['new_items'])) $resp['new_items'] = array();
                    $resp['new_items'][] = $question;

                    // Finish up

                    $content_id = bin2hex(random_bytes(4));

                    $assessment_data['Sections'][$section_index]['Content'][] = array(
                        'id' => $content_id,
                        'type' => 'item',
                        'question_handle_id' => $question_handle_id
                    );

                    foreach ($assessment_data['Outcomes'] as &$pEntry) {
                        if ($pEntry['id'] == $new_entry['outcome_id']) {
                            $pEntry['parts'][] = array(
                                'content_id' => $content_id,
                                'part_id' => $new_entry['part_id']
                            );
                        }
                    }
                } else $success = false;
            }
        }

        if ($success) {

            $json_string = json_encode($assessment_data);
			$resp['data_string'] = $json_string;
			$resp['data_hash'] = md5($json_string);

            $this->Assessment->id = $assessment_id;
            $this->Assessment->saveField('json_data', json_encode($assessment_data));

            $version_id = Cache::read('assessments_' . $assessment_id, 'version_ids');
            if ($version_id === false) {
                $version_id = md5(microtime() . $assessment_id);
                Cache::write('assessments_' . $assessment_id, $version_id, 'version_ids');	
            }
            $resp['version_id'] = $version_id;
    
            foreach ($new_questions as $question_id => $new_entry) {
                $this->Question->id = $question_id;
                $this->Question->saveField('needs_index', date('Y-m-d H:i:s'));
            }

        } else {

            unset($resp['new_items']);

            $this->AssessmentItem->deleteAll(array(
                'id' => $new_assessment_items
            ));

        }

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}    
}

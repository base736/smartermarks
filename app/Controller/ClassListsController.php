<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class ClassListsController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {
		
		if (parent::isAuthorized($user)) {
	        return true;
	    }

        if (in_array($this->action, array('index'))) {
            return true;
        }

	    if (in_array($this->action, array('get'))) {
		    return !empty($this->Auth->user('id'));
	    }

	    if (in_array($this->action, array('save'))) {
            if (empty($this->Auth->user('id'))) return false;

            if (array_key_exists('id', $this->request->data)) {
                $class_list_id = $this->request->data['id'];
                $this->ClassList->contain();
                $class_list = $this->ClassList->findById($class_list_id);

                if (empty($class_list)) return false;
                else return ($class_list['ClassList']['user_id'] == $this->Auth->user('id'));
             } else return true;
	    }

        if (in_array($this->action, array('delete', 'send'))) {
	    	if (!empty($this->request->data['list_ids'])) {
                $list_ids = array();
                foreach ($this->request->data['list_ids'] as $list_id) {
                    if (is_numeric($list_id)) $list_ids[] = intval($list_id);
                    else return false;
                }

                $user_ids = $this->ClassList->find('list', array(
                    'fields' => array('ClassList.id', 'ClassList.user_id'),
                    'conditions' => array('ClassList.id' => $list_ids)
                ));

                foreach ($user_ids as $list_id => $user_id) {
                    if ($user_id != $this->Auth->user('id')) return false;
                }
                return true;

			} else return false;
		}

	}

    public function index() {

        // Get count of received items

        $receivedCount = $this->ClassList->find('count', array(
            'conditions' => array(
                'ClassList.user_id' => $this->Auth->user('id'),
                'ClassList.is_new' => 3
            )
        ));

		// Get contacts

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$this->User->contain();
		$contactRecords = $this->User->find('all', array(
			'conditions' => array('User.id' => $contact_ids))
		);
		$contacts = array();
		foreach ($contact_ids as $id) {
			foreach ($contactRecords as $record) {
				if ($record['User']['id'] == $id) {
					$contacts[] = array(
						'id' => $record['User']['id'],
						'name' => $record['User']['name'],
						'email' => $record['User']['email']
					);
				}
			}
		}
        
        $this->set(compact('receivedCount', 'contacts'));
	}

    public function get() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

        $resp = array();

        $this->ClassList->contain();
        $class_lists = $this->ClassList->find('all', array(
            'conditions' => array('ClassList.user_id' => $this->Auth->user('id'))
        ));

        $this->ClassList->updateAll(
            array('ClassList.is_new' => 0),
            array('ClassList.user_id' => $this->Auth->user('id'))
        );

        $results = array();
        foreach ($class_lists as $class_list) {
            $results[] = $class_list['ClassList'];
        }

		$resp['results'] = $results;
		$resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);        
    }

    public function delete() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

        $resp = array();

        $list_ids = $this->request->data['list_ids'];
        $success &= $this->ClassList->deleteAll(array(
            'ClassList.id' => $list_ids
        ), true, true);

        $resp['count'] = count($list_ids);
		$resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);        
    }    

    public function save() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

        $resp = array();

        $data = array(
            'name' => $this->request->data['name'],
            'json_data' => $this->request->data['json_data']
        );
        if (array_key_exists('id', $this->request->data)) {
            $data['id'] = $this->request->data['id'];
            $data['is_new'] = 0;
        } else {
            $data['user_id'] = $this->Auth->user('id');
            $data['is_new'] = 1;
            $this->ClassList->create();
        }

        $success &= !empty($this->ClassList->save($data));
        
        $resp['id'] = $this->ClassList->id;
		$resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);        
    }    

    public function send() {
		$this->autoRender = false;
		$this->layout = '';
		
		$this->loadModel('User');

		$success = true;

		$email_array = $this->request->data['email_list'];
		$item_ids = $this->request->data['list_ids'];

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$toUsers = array();
		foreach ($email_array as $email) {
			$email = trim($email);
			if (strlen($email) == 0) continue;
			
			$this->User->contain();
			$toUser = $this->User->findByEmail($email);
			if ($toUser && ($toUser['User']['role'] == 'teacher')) {
				$toUsers[] = $toUser;
				
				$userID = $toUser['User']['id'];
				if (($key = array_search($userID, $contact_ids)) !== false) {
					unset($contact_ids[$key]);
				}
				array_unshift($contact_ids, $userID);
			} else $success = false;
		}

		// Update contact IDs
		
		$newUserData = array();
		$newUserData['id'] = $this->Auth->user('id');
		$newUserData['contact_ids'] = implode(',', $contact_ids);
		$this->updateUserData($newUserData);

		// Send class lists

        $this->ClassList->contain();
        $original_items = $this->ClassList->find('all', array(
            'conditions' => array('ClassList.id' => $item_ids)
        ));
        foreach ($original_items as &$pItem) {
            $pItem = $pItem['ClassList'];
            unset($pItem['id']);
            unset($pItem['created']);
            $pItem['last_used'] = null;
            $pItem['is_new'] = 3;
        }

		foreach ($toUsers as $toUser) {
			$userID = $toUser['User']['id'];
            foreach ($original_items as $item) {
                $item['user_id'] = $userID;
                $this->ClassList->create();
                $this->ClassList->save($item);
            }
		}
		
		echo json_encode(
			array(
			    'success' => $success,
				'numSent' => count($item_ids)
			)
		);
	}
}

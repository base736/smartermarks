<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class CommunityUsersController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
	    }
	    
	    if (in_array($this->action, array('unsubscribe'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$communityUser_id = $this->request->params['pass'][0];
				
			    $this->CommunityUser->contain();
			    $communityUser = $this->CommunityUser->findById($communityUser_id);
			    return ($communityUser['CommunityUser']['user_id'] == $this->Auth->user('id'));				
			} else return false;
	    }
		
		return false;
	}

	public function unsubscribe($communityUser_id) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

	    $this->CommunityUser->contain();
		$targetUser = $this->CommunityUser->findById($communityUser_id);
		$user_id = $targetUser['CommunityUser']['user_id'];
		if (empty($targetUser)) $success = false;
		
		if ($success) {
			$communityId = $targetUser['CommunityUser']['community_id'];
			$this->CommunityUser->contain();
			$allUsers = $this->CommunityUser->find('all', array(
				'conditions' => array(
					'CommunityUser.community_id' => $communityId
				)
			));
			
			$isAdmin = false;
			$adminCount = 0;
			$otherCount = 0;
			foreach ($allUsers as $communityUser) {
				if ($communityUser['CommunityUser']['is_admin'] == 1) {
					if ($communityUser['CommunityUser']['user_id'] == $user_id) $isAdmin = true;
					$adminCount++;
				} else $otherCount++;
			}
			
			if ($isAdmin && ($adminCount == 1) && ($otherCount > 0)) $success = false;
			else {
				if (($adminCount == 1) && ($otherCount == 0)) {
					$this->loadModel('Community');
					$data = array();
					$data['id'] = $communityId;
					$data['deleted'] = date('Y-m-d H:i:s');
					$this->Community->save($data);
				}
				$success &= $this->CommunityUser->delete($communityUser_id);
			}
		}

		$resp = array();
		$resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}
}


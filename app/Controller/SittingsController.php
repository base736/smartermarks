<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class SittingsController extends AppController {

    public $presetVars = true; // using the model configuration

	public function beforeFilter() {
	    parent::beforeFilter();
	    
	    $this->Auth->allow('join', 'not_found');
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
		}

		if (in_array($this->action, array('index', 'empty_trash'))) {
			return !empty($this->Auth->user('id'));
		}

		if (in_array($this->action, array('get_index'))) {
			if (!empty($this->request->query['folder_ids'])) {
				$this->loadModel('Folder');
				$folder_ids = explode(',', $this->request->query['folder_ids']);
				foreach ($folder_ids as $folder_id) {
					if (is_numeric($folder_id)) {
						$permissions = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
						return in_array('can_view', $permissions['all']);
					} else return false;
				}	
			} else return false;
		}

		if (in_array($this->action, array('create'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$assessment_id = $this->request->params['pass'][0];

				$this->loadModel('Assessment');
				$permissions = $this->Assessment->getPermissions($assessment_id, $this->Auth->user('id'));
				return in_array('can_view', $permissions);
			} else return false;
		}

		if (in_array($this->action, array('send', 'copy', 'move'))) {
			if (!empty($this->request->data['item_ids'])) {
				foreach ($this->request->data['item_ids'] as $sitting_id) {
					$itemPermissions = $this->Sitting->getPermissions($sitting_id, 
						$this->Auth->user('id'));
						
					if ($this->action == 'send') {
						if (!in_array('can_export', $itemPermissions)) return false;
					} else if (in_array($this->action, array('copy', 'move'))) {
						if (!empty($this->request->data['folder_id'])) {
							$this->loadModel('Folder');
							
							$targetFolderId = $this->request->data['folder_id'];
							$targetPermissions = $this->Folder->getContentPermissions($targetFolderId, 
								$this->Auth->user('id'));
	
							$this->Sitting->contain();
							$sitting = $this->Sitting->findById($sitting_id);
							$sourceFolderId = $sitting['Sitting']['folder_id'];
	
							if (!in_array('can_add', $targetPermissions['all'])) return false;
							if ($this->Folder->getCommunityId($sourceFolderId) != $this->Folder->getCommunityId($targetFolderId)) {
								if (!in_array('can_export', $itemPermissions)) return false;
							}
							if ($this->action == 'move') {
								if (!in_array('can_delete', $itemPermissions)) return false;
							}
						} else return false;
					} else return false;
				}
				return true;
			} else return false;
		}

		$needs_view = array('view', 'get', 'get_results', 'get_written', 'update_scoring', 'print_questions');
		$needs_edit = array('save', 'remove_student', 'add_students', 'delete', 'open', 'close', 'save_results', 'save_version');
		if (in_array($this->action, array_merge($needs_view, $needs_edit))) {
			$sitting_id = null;

			if (in_array($this->action, array('remove_student'))) {
				if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
					$sittingStudent_id = $this->request->params['pass'][0];
					$this->loadModel('SittingStudent');
					$this->SittingStudent->contain();
					$sittingStudent = $this->SittingStudent->findById($sittingStudent_id);
					if ($sittingStudent != null) {
						$sitting_id = $sittingStudent['SittingStudent']['sitting_id'];
					} else return false;
				} else return false;
			} else {
				if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
					$sitting_id = $this->request->params['pass'][0];
				} else return false;
			}

			if ($this->action == 'save_results') {
				if (!empty($this->request->data['results'])) {
					$this->loadModel('ResponseOnline');
					$results = json_decode($this->request->data['results'], true);
					foreach ($results as $id => $result) {
						$attempt_cache = $this->ResponseOnline->getCache($id);
						if (!empty($attempt_cache['sitting_student_id'])) {
							$this->loadModel('SittingStudent');
							$sittingStudent_cache = $this->SittingStudent->getCache($attempt_cache['sitting_student_id']);
							if ($sittingStudent_cache['SittingStudent']['sitting_id'] != $sitting_id) return false;
						} else return false;
					}
				}
			}

			$permissions = $this->Sitting->getPermissions($sitting_id, $this->Auth->user('id'));
			if (in_array($this->action, $needs_edit)) return in_array('can_edit', $permissions);
			else return in_array('can_view', $permissions);
		}
		
		return false;
	}

	// Index interface

	public function index() {
		$adminView = ($this->Auth->user('role') == 'admin');

		if ($adminView) {
			$jsonTree = "";
		} else {
			$this->loadModel('Folder');
			$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'sittings');
			$tree = $this->Folder->getTree($nodes);
			$jsonTree = json_encode($tree);
		}

		$this->loadModel('User');
		$isExpired = $this->User->isExpired($this->Auth->user('id'));

		// Find any sittings that have been sent to this user (is_new == 3)

		if ($this->Auth->user('role') != 'admin') {
			$receivedCount = $this->Sitting->find('count', array(
				'conditions' => array(
					'Sitting.user_id' => $this->Auth->user('id'),
					'Sitting.is_new' => 3
				)
			));
		} else $receivedCount = 0;

		// Get contacts

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$this->User->contain();
		$contactRecords = $this->User->find('all', array(
			'conditions' => array('User.id' => $contact_ids))
		);
		$contacts = array();
		foreach ($contact_ids as $id) {
			foreach ($contactRecords as $record) {
				if ($record['User']['id'] == $id) {
					$contacts[] = array(
						'id' => $record['User']['id'],
						'name' => $record['User']['name'],
						'email' => $record['User']['email']
					);
				}
			}
		}
				
		// Set variables for template
		
		$this->set(compact('adminView', 'jsonTree', 'isExpired', 'receivedCount', 'contacts'));

		$this->set('colCreated', 'Sitting.moved');
		$this->set('colName', 'Sitting.name');
		$this->set('itemType', 'Sitting');
		$this->set('index_element', '../Sittings/index');

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		$this->viewPath = 'Elements';
		$this->render('index_common');
	}

	public function get_index() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$query_params = array('conditions' => array('Sitting.deleted' => null));
		$userDefaults = json_decode($this->Auth->user('defaults'), true);

		if (isset($this->request->query['sort'])) {
			$sortBy = $this->request->query['sort'];
		} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$sortBy = 'Sitting.name';
		} else {
			$sortBy = 'Sitting.moved';
		}

		if (isset($this->request->query['direction'])) {
			$direction = $this->request->query['direction'];
		} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$direction = 'asc';
		} else {
			$direction = 'desc';
		}

		$query_params['order'] = array($sortBy => $direction);
		
		if (isset($this->request->query['folder_ids'])) {

			$folder_ids = explode(',', $this->request->query['folder_ids']);
			$query_params['conditions']['Sitting.folder_id'] = $folder_ids;

            $count = $this->Sitting->find('count', $query_params);
            $resp['count_total'] = $count;
            $resp['count_matching'] = $count;
    
		} else if ($this->Auth->user('role') == 'admin') {

			$query_params['conditions'][] = ['Sitting.folder_id IS NOT NULL'];
            $query_params['contain'] = array('User');

		} else $success = false;

		if (isset($this->request->query['search'])) {
			$query_params['conditions'][] = array('OR' => array(
				'Sitting.name LIKE' => '%' . $this->request->query['search'] . '%',
				'Sitting.id' => $this->request->query['search']
			));
			$count = $this->Sitting->find('count', $query_params);
            $resp['count_matching'] = $count;
		}

		if (isset($this->request->query['limit'])) {
			if (is_numeric($this->request->query['limit']) && ($this->request->query['limit'] > 0)) {
				$query_params['limit'] = intval($this->request->query['limit']);
			} else if ($this->request->query['limit'] !== 'all') {
                $success = false;
            }
		} else $query_params['limit'] = 10;

		if (isset($this->request->query['page'])) {
			if (is_numeric($this->request->query['page']) && ($this->request->query['page'] > 0)) {
				$query_params['page'] = intval($this->request->query['page']);
			} else $success = false;
		} else $query_params['page'] = 1;

		if ($success) {
			$results = $this->Sitting->find('all', $query_params);

			if ($this->Auth->user('role') != 'admin') {

				// Get permissions for each result

				$folder_permissions = array();
				foreach ($results as &$pResult) {
					$folder_id = $pResult['Sitting']['folder_id'];
					if (!array_key_exists($folder_id, $folder_permissions)) {
						$folder_permissions[$folder_id] = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
					}
	
					if ($pResult['Sitting']['user_id'] == $this->Auth->user('id')) {
						$pResult['Permissions'] = $folder_permissions[$folder_id]['owner'];
					} else $pResult['Permissions'] = $folder_permissions[$folder_id]['all'];
				}	

				// Update "is_new" for items

				$visible = array();
				foreach ($results as $result) {
					$visible[] = $result['Sitting']['id'];
				}
				$this->Sitting->updateAll(
					array('Sitting.is_new' => 0),
					array('Sitting.id' => $visible)
				);
				$this->Sitting->updateAll(
					array('Sitting.is_new' => 2),
					array(
						'Sitting.user_id' => $this->Auth->user('id'),
						'Sitting.is_new' => 3
					)
				);
			}

			$resp['results'] = $results;

		}

		$resp['page'] = $query_params['page'];
        if (array_key_exists('limit', $query_params)) {
            $resp['limit'] = $query_params['limit'];
        }
		$resp['success'] = $success;

		echo json_encode($resp);
	}
	
	public function not_found() {
	}

	public function join($join_token = null) {
		$this->autoRender = false;
		$this->layout = '';
		   
		if ($join_token == null) {
			$this->redirect(array('controller' => 'Sittings', 'action' => 'not_found'));
		}

		$this->Sitting->contain();
		$sitting = $this->Sitting->findByJoinToken(strtoupper($join_token));
		if (!empty($sitting)) {
			$sitting_id = $sitting['Sitting']['id'];
			$json_data = json_decode($sitting['Sitting']['json_data'], true);
	
			if (empty($json_data['Access']['token_details']['fieldValues'])) {
				$this->redirect(array('controller' => 'Sittings', 'action' => 'not_found'));
			}

			$this->loadModel('SittingStudent');
	
			$student_token = null;
	
			$openNew = false;
			if ($this->Cookie->check('student_token_' . $sitting_id)) {
				
				// Continue a sitting already joined
				
				$student_token = $this->Cookie->read('student_token_' . $sitting_id);
								
				$this->SittingStudent->contain();
				$sittingStudent = $this->SittingStudent->findByToken($student_token);
				if (!empty($sittingStudent)) {
					$sittingStudent_id = $sittingStudent['SittingStudent']['id'];
					$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);

					$this->SittingStudent->cleanResponses($sittingStudent_id);

					$this->loadModel('ResponseOnline');

					$responsesClosed = 0;
					$openResponse = null;
					foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
						$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
						$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
						if ($timeRemaining <= 0) $responsesClosed++;
						else $openResponse = $attempt_cache;
					}
	
					if (!empty($openResponse)) {
						
						// Continue an attempt in progress
						
						$forward_action = '/SittingStudents/view_open/' . $sittingStudent_id;
						
					} else if ($responsesClosed > 0) {
						
						// View closed attempts, possibly with a button to start another
	
						$forward_action = '/SittingStudents/view_closed/' . $sittingStudent_id;
						
					} else {
						
						// Sitting exists but has no attempts -- go to the lobby
						
						$forward_action = '/SittingStudents/lobby/' . $sittingStudent_id;
					}
				} else {
					$student_token = null;
					$openNew = true;
				}
			} else $openNew = true;

			if ($openNew) {
				
				// Create a new SittingStudent entry
	
				if ($student_token == null) {
					$student_token = md5(uniqid('', true));
					$this->Cookie->write('student_token_' . $sitting_id, $student_token, false, '1 month');
				}
			
				$data = array(
					'sitting_id' => $sitting_id,
					'user_id' => null,
					'folder_id' => null,
					'moved' => null,
					'details' => '',
					'can_open' => 0,
					'token' => $student_token
				);
				
				$this->SittingStudent->create();
				$this->SittingStudent->save($data);
	
				$sittingStudent_id = $this->SittingStudent->id;
				$forward_action = '/SittingStudents/lobby/' . $sittingStudent_id;
			}

			$this->redirect($forward_action);
		} else $this->redirect(array('controller' => 'Sittings', 'action' => 'not_found'));
	}

	// create a new sitting

	public function create($assessment_id = null) {
		$this->autoRender = false;
		$this->layout = '';
		   
	   	$resp = array();
		$success = true;
	
		$this->loadModel('User');
		if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {

			$resp['message'] = 'Your account has expired. Unable to create new sittings.';
			$success = false;
			
		} else {

			$this->loadModel('Assessment');
			$this->Assessment->contain();
			$assessment = $this->Assessment->findById($assessment_id);

			// Create a result set

			$this->loadModel('ResultSet');
			$data = array();
			$data['user_id'] = $this->Auth->user('id');
			$data['source_model'] = 'Sitting';
			$this->ResultSet->create();
			$this->ResultSet->save($data);

			// Create a sitting

			$default_json = array(
				'Access' => array(
					'attempt_limit' => 1,
					'duration' => 0,
					'start' => array(
						'type' => 'date',
						'date' => date('Y-m-d H:i:s'),
					),
					'end' => array(
						'type' => 'none'
					)
				),
				'View' => array(
					'presentation' => 'free',
					'shuffle_attempts' => true,
					'reasoning' => array(
						'location' => 'selected',
						'label' => 'Your reasoning',
						'parts' => array()
					)
				),
                'Scoring' => array(),
				'Results' => array(
					array(
						'type' => 'scoring',
						'show' => array('type' => 'auto'),
						'hide' => array('type' => 'none')
					),
					array(
						'type' => 'outcomes',
						'show' => array('type' => 'auto'),
						'hide' => array('type' => 'none')
					)
				)
			);
		
			$data = array(
				'assessment_id' => null,
				'result_set_id' => $this->ResultSet->id,
				'user_id' => $this->Auth->user('id'),
				'folder_id' => $this->request->data['target_folder'],
				'moved' => date('Y-m-d H:i:s'),
				'is_new' => 1,
				'scoring' => '',
				'name' => $assessment['Assessment']['save_name'],
				'join_token' => $this->Sitting->getJoinToken(),
				'json_data' => json_encode($default_json),
				'can_join' => 0
			);
			
			$this->Sitting->create();
			$success &= !empty($this->Sitting->save($data));

			if ($success) {
				$resp['sitting_id'] = $this->Sitting->id;

				// Create a copy of this assessment to isolate it from changes to the original

				$result = $this->Assessment->copy($assessment_id, array(
					'sitting_id' => $this->Sitting->id
				));
				$success &= $result['success'];
			}

			if ($success) {
				$this->Assessment->contain();
				$assessment = $this->Assessment->findById($result['copyID']);
				$assessment_data = json_decode($assessment['Assessment']['json_data'], true);
				$assessment_data['Title']['text'] = explode("\n", $this->request->data['title']);

				$this->Assessment->id = $result['copyID'];
				$this->Assessment->saveField('json_data', json_encode($assessment_data));

				// Attach the copy to the new sitting
				
				$this->Sitting->saveField('assessment_id', $result['copyID']);
			}
		}

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	

	public function save($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;

		$dataString = $this->request->data['data_string'];

		if (empty($this->request->data['data_hash'])) $success = false;
		else if ($this->request->data['data_hash'] != md5($dataString)) $success = false;
		else $json_data = json_decode($dataString, true);
	
		if ($success) {
			$data = array();
			$data['id'] = $sitting_id;

			if (array_key_exists('save_name', $this->request->data)) {
				$data['name'] = $this->request->data['save_name'];
			}

			$data['json_data'] = $dataString;

			if ($json_data['Access']['end']['type'] == 'date') {
				$data['closed'] = $json_data['Access']['end']['date'];
			} else $data['closed'] = null;

			$data['is_new'] = 0;
			$success &= !empty($this->Sitting->save($data));
		}

		if ($success) {
			$this->loadModel('SittingStudent');
			if ($json_data['Access']['start']['type'] == 'code') {
				$this->SittingStudent->updateAll(
					array('SittingStudent.can_open' => 0),
					array(
						'SittingStudent.sitting_id' => $sitting_id,
						'SittingStudent.started IS NULL'
					)
				);
			} else {
				$this->SittingStudent->updateAll(
					array('SittingStudent.can_open' => 1),
					array(
						'SittingStudent.sitting_id' => $sitting_id,
						'SittingStudent.user_id IS NULL'
					)
				);
			}
						
			$sittingStudent_ids = $this->SittingStudent->find('list', array(
				'fields' => array('SittingStudent.id'),
				'conditions' => array('SittingStudent.sitting_id' => $sitting_id)
			));

			$nowTimestamp = time();

			$this->loadModel('ResponseOnline');
			foreach ($sittingStudent_ids as $sittingStudent_id) {
				$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
				foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
					$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
					if (!empty($attempt_cache) && (($attempt_cache['closed'] == null) || (strtotime($attempt_cache['closed']) > $nowTimestamp))) {
						$startTime = $attempt_cache['created'];
						$closedTime = $this->ResponseOnline->getClosed($startTime, $json_data);
						$attempt_cache['closed'] = $closedTime;
		
						$success &= $this->ResponseOnline->updateCache($attempt_cache);	
					}
				}
			}
		}

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function delete($sitting_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;
	
		if (!$this->Sitting->delete($sitting_id)) $success = false;
	
		$resp['data'] = $this->request->data;
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function add_students($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';
		   
	   	$resp = array();
		$success = true;
        
        if (array_key_exists('classListID', $this->request->data)) {
            $this->loadModel('ClassList');
            $class_list_id = intval($this->request->data['classListID']);
            $this->ClassList->id = $class_list_id;
            $this->ClassList->saveField('last_used', date('Y-m-d H:i:s')); 
        }

        $student_data = $this->request->data['studentData'];

		$this->loadModel('User');
		$this->loadModel('Folder');
		$this->loadModel('SittingStudent');
		$this->loadModel('Sitting');

		$this->Sitting->contain();
		$sitting = $this->Sitting->findById($sitting_id);

		$json_data = json_decode($sitting['Sitting']['json_data'], true);
		
		if ($json_data['Access']['start']['type'] == 'code') $can_open = 0;
		else $can_open = 0;
		
		$skipped = array();
		foreach ($student_data as $this_data) {
            if (array_key_exists('email', $this_data)) {
                $email = $this_data['email'];
                $isValid = (filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE);                
            } else $isValid = false;

			if ($isValid) {
				$this->User->contain();
				$emailMatch = $this->User->findByEmail($email);
				$this->User->contain();
				$signupEmailMatch = $this->User->findBySignupEmail($email);

				$userID = null;
				if (!empty($emailMatch)) {
					if ($emailMatch['User']['role'] == 'student') $userID = $emailMatch['User']['id'];
					else $skipped[] = $email;
				} else if (!empty($signupEmailMatch)) {
					if ($signupEmailMatch['User']['role'] == 'student') $userID = $signupEmailMatch['User']['id'];
					else $skipped[] = $email;
				} else {
					$data = array(
						'signup_email' => $email,
						'email' => $email,
						'password' => '',
						'role' => 'student',
						'name' => '',
						'school' => $this->Auth->user('school'),
						'created_by_id' => $this->Auth->user('id'),
						'approved' => 1,
						'timezone' => $this->Auth->user('timezone'),
						'expires' => NULL,
						'defaults' => $this->User->getDefaults()
					);

					$this->User->create();
					if (!empty($this->User->save($data))) {
						$userID = $this->User->id;
						$this->User->send_validation($userID);
					} else $success = false;
				}

				if ($userID !== null) {
					$existing = $this->SittingStudent->find('first', array(
						'conditions' => array(
							'sitting_id' => $sitting_id,
							'user_id' => $userID
						)
					));
					if (empty($existing)) {
						$data = array(
							'user_id' => $userID,
							'sitting_id' => $sitting_id,
							'folder_id' => $this->Folder->getUserFolder($userID, 'sittingstudents', 'home'),
							'moved' => date('Y-m-d H:i:s'),
							'is_new' => 3,
							'details' => '',
							'can_open' => $can_open
						);
						$this->SittingStudent->create();
						$success &= !empty($this->SittingStudent->save($data));
					}
				}
			} else if (!empty($email)) {
				$skipped[] = $email;
			}
		}

		$resp['account_data'] = $this->Sitting->get_account_data($sitting_id);
		$resp['skipped'] = $skipped;
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function remove_student($sittingStudent_id) {
		$this->autoRender = false;
	   	$this->layout = '';
	   	
	   	$resp = array();
		$success = true;

		$this->loadModel('SittingStudent');

		$data = array();
		$data['id'] = $sittingStudent_id;
		$data['deleted'] = date('Y-m-d H:i:s');
		$success &= !empty($this->SittingStudent->save($data));

		if ($success) {
			$this->loadModel('ResponseOnline');

			$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
			foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
				$attempt = Cache::read($attempt_id, 'responses_online');
				if ($attempt !== false) {
					$attempt['sitting_student_deleted'] = $data['deleted'];
					Cache::write($attempt_id, $attempt, 'responses_online');	
				}
			}	
		}

		$resp['success'] = $success;
		if ($success) $resp['id'] = $sittingStudent_id;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function view($sitting_id = null) {
		$this->Sitting->contain();
		$sitting = $this->Sitting->findById($sitting_id);

		if ($sitting['Sitting']['user_id'] == $this->Auth->user('id')) {
			$canEdit = true;
			$canViewResults = true;
		} else {
			$permissions = $this->Sitting->getPermissions($sitting_id, $this->Auth->user('id'));
			$canEdit = in_array('can_edit', $permissions) ? 1 : 0;
			$canViewResults = in_array('all_scores', $permissions) ? 1 : 0;
		}

		$this->loadModel('User');
		if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {
			$canEdit = 0;
		}

        $this->User->contain('ClassList');
        $user = $this->User->findById($this->Auth->user('id'));
        $classLists = $user['ClassList'];

		if ($sitting['Sitting']['is_new'] == 1) {                    // Sitting can be cancelled
			$canCancel = $canEdit;
			$showSettings = 1;
		} else {
			$canCancel = 0;
			$showSettings = 0;
		}

		$assessment_id = $sitting['Sitting']['assessment_id'];

		$this->set(compact('sitting_id', 'assessment_id', 'canEdit', 'canViewResults', 'canCancel', 'showSettings', 'classLists'));

		$this->loadModel('Assessment');
		$this->Assessment->contain();
		$assessment = $this->Assessment->findById($assessment_id); 
		$assessment_data = json_decode($assessment['Assessment']['json_data'], true);

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}	

	public function get($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';

		$success = true;
		$resp = array();

		$sitting_cache = $this->Sitting->getCache($sitting_id);

		$json_data = $sitting_cache['Sitting']['json_data'];

		$resp['Sitting'] = array(
			'name' => $sitting_cache['Sitting']['name'],
			'json_data' => $json_data,
		);

		$resp['Assessment'] = $sitting_cache['Assessment'];
		$resp['Questions'] = $sitting_cache['Questions'];

		// Get version HTML and settings if required

		if (!$json_data['View']['shuffle_attempts'] && !empty($sitting_cache['Version'])) {
			$resp['Version'] = array(
				'html' => $sitting_cache['Version']['html'],
				'scoring_data' => $sitting_cache['Version']['scoring_data']
			);
		}

		// Add teacher data if allowed

		if (!empty($this->Auth->user('role')) && ($this->Auth->user('role') != 'student')) {

			// Add student data

			$resp['Accounts'] = $this->Sitting->get_account_data($sitting_id);

			// Add join token and needs_version

			$resp['Sitting']['join_token'] = $sitting_cache['Sitting']['join_token'];
			$resp['Sitting']['needs_version'] = ($sitting_cache['Sitting']['version_data_id'] == null);

			// Get question statistics

			$question_ids = array_map(function($value) {
				return $value['id'];
			}, $resp['Questions']);

			$this->loadModel('Question');
			$resp['Statistics'] = $this->Question->getStatistics($question_ids);

			// Add layout sections if needed

			if (!$json_data['View']['shuffle_attempts']) {
				$resp['Version']['layoutSections'] =  $sitting_cache['Version']['scoring_data']['Sections'];
			}

			// Apply secrets

			$secrets = $sitting_cache['Secrets'];

			foreach ($secrets['Assessment'] as $key => $value) {
				$resp['Assessment'][$key] = $value;
			}

			foreach ($resp['Questions'] as &$pQuestionData) {
				$question_handle_id = $pQuestionData['question_handle_id'];
				if (!empty($secrets['Questions'][$question_handle_id])) {
					if (!empty($secrets['Questions'][$question_handle_id]['name'])) {
						$pQuestionData['name'] = $secrets['Questions'][$question_handle_id]['name'];
					}
					foreach ($pQuestionData['json_data']['Parts'] as &$pPart) {
						$part_id = $pPart['id'];
	
						if (isset($secrets['Questions'][$question_handle_id]['Parts'][$part_id])) {
							$partSecrets = $secrets['Questions'][$question_handle_id]['Parts'][$part_id];
							foreach ($partSecrets as $key => $value) {
								$pPart[$key] = $value;
							}
						}
					}
				}
			}
		}

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

        echo json_encode($resp);
	}

	private function getMatchQuality($deviceA, $deviceB) {
		$matchQuality = 0;
		if (isset($deviceA['cookie_id']) && isset($deviceB['cookie_id'])) {
			if ($deviceA['cookie_id'] == $deviceB['cookie_id']) $matchQuality = 3;
			else {
				if (isset($deviceA['fingerprint']) && isset($deviceB['fingerprint'])) {
					if ($deviceA['fingerprint'] == $deviceB['fingerprint']) $matchQuality++;
				}
				if (isset($deviceA['ip_hash']) && isset($deviceB['ip_hash'])) {
					if ($deviceA['ip_hash'] == $deviceB['ip_hash']) $matchQuality++;
				}
			}
		} else if (isset($deviceA['session_id']) && isset($deviceB['session_id'])) {
			$matchQuality = 3;
		}

		return $matchQuality;
	}

	public function get_results($sitting_id = null) {
		App::uses('Scoring', 'Lib');

		$this->autoRender = false;
		$this->layout = '';
		   
	   	$success = true;
	   	
		$this->loadModel('SittingStudent');
		$this->loadModel('ResponseOnline');
		$this->loadModel('ResultData');

		$this->Sitting->contain(array(
			'SittingStudent' => array(
				'conditions' => array(
					'SittingStudent.deleted' => null,
					'OR' => array(
						'SittingStudent.user_id IS NOT NULL',
						'SittingStudent.started IS NOT NULL'
					)
				),
				'User'
			)
		));
		$sitting = $this->Sitting->findById($sitting_id);

		if ($sitting['Sitting']['closed'] != null) {
			$nowTimestamp = time();
			$sittingEndTimestamp = strtotime($sitting['Sitting']['closed']);
			$sitting['Sitting']['status'] = ($sittingEndTimestamp > $nowTimestamp) ? 'open' : 'closed';
		} else $sitting['Sitting']['status'] = 'open';

		foreach ($sitting['SittingStudent'] as $key => &$pStudent) {
			$sittingStudent_cache = $this->SittingStudent->getCache($pStudent['id']);
			if (!empty($sittingStudent_cache['ResponseOnline']) || !empty($pStudent['User'])) {

				if (strlen($pStudent['details']) == 0) $details = array();
				else $details = json_decode($pStudent['details'], true);
		
				if (!empty($pStudent['User'])) {
					$details['Email'] = $pStudent['User']['signup_email'];
				}
				unset($pStudent['User']);
	
				if (empty($details)) $pStudent['details'] = '';
				else $pStudent['details'] = json_encode($details);
			
				$pStudent['ResponseOnline'] = array();
				foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
	
					// Store details for this attempt
	
					$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
	
					$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
					if ($timeRemaining <= 0.0) $attempt_cache['status'] = 'closed';
					else $attempt_cache['status'] = 'open';

					$this->loadModel('VersionData');

					$version_data_id = $attempt_cache['version_data_id'];
					unset($attempt_cache['version_data_id']);

					$scoring_data = $this->VersionData->getScoringCache($version_data_id);
					$attempt_cache['layout'] = json_encode($scoring_data);

					$pStudent['ResponseOnline'][] = $attempt_cache;

				}				

			} else unset($sitting['SittingStudent'][$key]);
		}

		$sitting['SittingStudent'] = array_values($sitting['SittingStudent']);

		$resp = $sitting;
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function close($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';
	   	
	   	$resp = array();
		$success = true;
		
		$this->Sitting->contain(array(
			'SittingStudent' => array(
				'conditions' => array('SittingStudent.deleted' => null)
			)
		));
		$sitting = $this->Sitting->findById($sitting_id);

		if (!empty($sitting)) {
			$closedTime = date('Y-m-d H:i:s');

			$data = array();
			$data['id'] = $sitting_id;
			$data['closed'] = $closedTime;

			$json_data = json_decode($sitting['Sitting']['json_data'], true);

			$json_data['Access']['end']['type'] = 'date';
			$json_data['Access']['end']['date'] = $closedTime;
			$data['json_data'] = json_encode($json_data);
	
			if (!empty($this->Sitting->save($data))) {
				$this->loadModel('SittingStudent');
				$this->loadModel('ResponseOnline');
				foreach ($sitting['SittingStudent'] as $sittingStudent) {
					$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent['id']);
					foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
						$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
						$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
						if ($timeRemaining > 0.0) {
							$attempt_cache['closed'] = $closedTime;
							$this->ResponseOnline->updateCache($attempt_cache);
						}
					}
				}

				$resp['json_data'] = $data['json_data'];

			} else $success = false;
		} else $success = false;

		$resp['sitting_id'] = $sitting_id;
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function open($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';
	   	
	   	$resp = array();
		$success = true;

		$this->Sitting->contain('SittingStudent');
		$sitting = $this->Sitting->findById($sitting_id);

		$oldClosed = $sitting['Sitting']['closed'];

		$closeType = $this->request->data['close_type'];

		if ($closeType == 'Specified') $closedTime = $this->request->data['close_datetime'];
		else $closedTime = null;

		if (!empty($sitting)) {
			$data = array();
			$data['id'] = $sitting_id;
			$data['closed'] = $closedTime;

			$json_data = json_decode($sitting['Sitting']['json_data'], true);
	
			if ($closedTime !== null) {
				$json_data['Access']['end']['type'] = 'date';
				$json_data['Access']['end']['date'] = $closedTime;
			} else {
				$json_data['Access']['end']['type'] = 'none';
				unset($json_data['Access']['end']['date']);
			}
			$data['json_data'] = json_encode($json_data);
	
			if (!empty($this->Sitting->save($data))) {
				$this->loadModel('SittingStudent');
				$this->loadModel('ResponseOnline');
				$status = array();
				foreach ($sitting['SittingStudent'] as $sittingStudent) {
					$sittingStudent_id = $sittingStudent['id'];
					$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
					if (!empty($sittingStudent_cache['ResponseOnline'])) {
						$attemptID = array_pop($sittingStudent_cache['ResponseOnline']);
						$attempt_cache = $this->ResponseOnline->getCache($attemptID);

						if ($attempt_cache['closed'] == $oldClosed) {
							$startTime = $attempt_cache['created'];
							$closedTime = $this->ResponseOnline->getClosed($startTime, $json_data);
	
							$attempt_cache['closed'] = $closedTime;
							$this->ResponseOnline->updateCache($attempt_cache);
						} else $closedTime = $attempt_cache['closed'];

						$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);
						if ($timeRemaining <= 0.0) $status[$sittingStudent_id] = 'closed';
						else $status[$sittingStudent_id] = 'open';
					}
				}
				$resp['json_data'] = $data['json_data'];
				$resp['status'] = $status;
			} else $success = false;
		} else $success = false;
			
		$resp['sitting_id'] = $sitting_id;
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function update_scoring($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';
	   	
	   	$resp = array();
		$success = true;
		$question_changed = false;
		$sitting_changed = false;
		
		if (!empty($this->request->data['data_string'])) {
			$dataString = $this->request->data['data_string'];

			if (empty($this->request->data['data_hash'])) $success = false;
			else if ($this->request->data['data_hash'] != md5($dataString)) $success = false;
		
			if ($success) $json_data = json_decode($dataString, true);
		} else $success = false;

		if ($success) {
			$question_handle_id = $json_data['Source']['question_handle_id'];
			$part_id = $json_data['Source']['part_id'];
	
			// Get question

			$this->loadModel('QuestionHandle');
			$this->QuestionHandle->contain('Question');
			$questionHandle = $this->QuestionHandle->findById($question_handle_id);

			$question_id = intval($questionHandle['Question']['id']);
			$old_question_id = $question_id;

			$question_data = json_decode($questionHandle['Question']['json_data'], true);
			for ($part_index = 0; $part_index < count($question_data['Parts']); ++$part_index) {
				if ($question_data['Parts'][$part_index]['id'] == $part_id) break;
			}
			if ($part_index == count($question_data['Parts'])) $success = false;
		}

		if ($success) {

			// Update question data

			if (array_key_exists('Answers', $json_data)) {
				$question_answers = array();
				foreach ($json_data['Answers'] as $layout_answer) {
					$question_answers[] = array(
						'id' => $layout_answer['id'],
						'template' => $layout_answer['response']
					);
				}
				$question_data['Parts'][$part_index]['Answers'] = $question_answers;
			}
			if (array_key_exists('ScoredResponses', $json_data)) {
				$question_data['Parts'][$part_index]['ScoredResponses'] = $json_data['ScoredResponses'];
			}
			if (array_key_exists('TypeDetails', $json_data)) {
				$question_data['Parts'][$part_index]['TypeDetails'] = $json_data['TypeDetails'];
			}

			// Save question data if needed

			$this->loadModel('Question');

			$question_data_string = json_encode($question_data);
			if ($question_data_string != $questionHandle['Question']['json_data']) {
				$question_changed = true;

				$question_id = $this->Question->getModifiable($question_handle_id);
				if ($question_id !== false) {
					$question_id = intval($question_id);
					$this->Question->id = $question_id;
					$success &= !empty($this->Question->save(array(
						'id' => $question_id,
						'json_data' => $question_data_string
                    )));
				} else $success = false;
			}

			// Return updated question data

			$question_data = $this->Question->get($question_id);
			$question_json = json_decode($question_data['data_string'], true);
	
			$resp['question_data'] = array(
				'question_handle_id' => $question_handle_id,
				'question_id' => intval($question_id),
				'json_data' => $question_json
			);
		}

		if ($success) {

			// Update scoring entry in sitting

			$sitting_cache = $this->Sitting->getCache($sitting_id);

			$content_id = false;
            $matching_question_handle_ids = array();
            
			$assessment_json = $sitting_cache['Assessment'];
			foreach ($assessment_json['Sections'] as $section) {
				if (!array_key_exists('Content', $section)) continue;
				foreach ($section['Content'] as $content) {
					if ($content['type'] == 'item') {
						$this_handle_id = $content['question_handle_id'];
						if ($this_handle_id == $question_handle_id) {
                            $content_id = $content['id'];
                            $matching_question_handle_ids = array($this_handle_id);
                        }
					} else if ($content['type'] == 'item_set') {
						foreach ($content['question_handle_ids'] as $this_handle_id) {
							if ($this_handle_id == $question_handle_id) {
                                $content_id = $content['id'];
                                $matching_question_handle_ids = $content['question_handle_ids'];
                            }
						}
					}
				}
			}

            $matching_part_ids = array();
            foreach ($matching_question_handle_ids as $this_handle_id) {
                $this->QuestionHandle->contain('Question');
                $questionHandle = $this->QuestionHandle->findById($this_handle_id);
                $this_data = json_decode($questionHandle['Question']['json_data'], true);

                if ($part_index < count($this_data['Parts'])) {
                    $matching_part_ids[$this_handle_id] = $this_data['Parts'][$part_index]['id'];
                } else $success = false;
            }

			if ($content_id !== false) {

                $sitting_data = $sitting_cache['Sitting']['json_data'];
				$old_sitting_string = json_encode($sitting_data);
	
                if (array_key_exists('tweaks', $sitting_data['Scoring'])) {
                    foreach ($sitting_data['Scoring']['tweaks'] as $key => &$pEntry) {
                        if (($pEntry['content_id'] == $content_id) && ($pEntry['part_index'] == $part_index)) {
                            unset($sitting_data['Scoring']['tweaks'][$key]);
                        }
                    }    
                    $sitting_data['Scoring']['tweaks'] = array_values($sitting_data['Scoring']['tweaks']);
                }
	
				if (array_key_exists('scoring', $json_data)) {
                    if (!array_key_exists('tweaks', $sitting_data['Scoring'])) {
                        $sitting_data['Scoring']['tweaks'] = array();
                    }
					$sitting_data['Scoring']['tweaks'][] = array(
						'content_id' => $content_id,
						'part_index' => $part_index,
						'type' => $json_data['scoring']
					);
				}

                if (empty($sitting_data['Scoring']['tweaks'])) unset($sitting_data['Scoring']['tweaks']);
	
				// Save if required
	
				$new_sitting_string = json_encode($sitting_data);
				if ($new_sitting_string != $old_sitting_string) {
					$sitting_changed = true;
	
					$this->Sitting->id = $sitting_id;
					if (!$this->Sitting->saveField('json_data', $new_sitting_string)) {
						$success = false;
					}	
				}
	
				// Return sitting data
	
				$resp['sitting_data'] = $sitting_data;
			}
		}


		if ($success && ($question_changed || $sitting_changed)) {

			// Update form layouts

			$this->loadModel('ResultData');

			$layouts = array();
			if (!empty($sitting_cache['Version'])) {
				$layouts[$sitting_cache['Version']['version_data_id']] = $sitting_cache['Version']['scoring_data'];
			}

			$this->ResultData->contain('VersionData');
			$result_data = $this->ResultData->find('all', array(
				'conditions' => array('result_set_id' => $sitting_cache['Sitting']['result_set_id'])
			));
			foreach ($result_data as $this_data) {
				$this_layout = json_decode($this_data['VersionData']['scoring_json'], true);
				$layouts[$this_data['VersionData']['id']] = $this_layout;
			}

			$this->loadModel('VersionData');

			$partIndices = array();
			foreach ($layouts as $version_data_id => $layout) {
				$partIndex = 0;
				foreach ($layout['Sections'] as &$pSection) {
					foreach ($pSection['Content'] as &$pQuestion) {		
						
						// Update data for the target question
						
                        $this_handle_id = $pQuestion['Source']['question_handle_id'];
                        $this_part_id = $pQuestion['Source']['part_id'];

                        if (array_key_exists($this_handle_id, $matching_part_ids) && 
                            ($matching_part_ids[$this_handle_id] == $this_part_id)) {

                            if ($this_handle_id == $question_handle_id) {
                                $partIndices[$version_data_id] = $partIndex;
                                foreach ($json_data as $key => $value) {
                                    if (in_array($key, array('Answers', 'ScoredResponses', 'TypeDetails'))) {
                                        $pQuestion[$key] = $value;
                                    }
                                }
                            }

                            if (array_key_exists('scoring', $json_data)) {
                                $pQuestion['scoring'] = $json_data['scoring'];
                            } else unset($pQuestion['scoring']);

                        }

                        $partIndex++;                        
					}
				}
				
				$this->VersionData->id = $version_data_id;
				if (!$this->VersionData->saveField('scoring_json', json_encode($layout))) {
					$success = false;
				}
			}

			// Update statistics

			$this->loadModel('ResultSet');
			$this->ResultSet->id = $sitting_cache['Sitting']['result_set_id'];
			$this->ResultSet->saveField('stats_status', 'update');
        }

        if ($success && $question_changed && ($json_data['type'] == 'wr')) {

			// Clear scores if criteria for a WR question have changed

            $this->loadModel('ResponseOnline');

            $result_data_ids = array_map(function ($a) {
                return $a['ResultData']['id'];
            }, $result_data);

            $this->ResponseOnline->contain('ResultData');
            $attempts = $this->ResponseOnline->find('list', array(
                'fields' => array('ResponseOnline.id', 'ResultData.version_data_id'),
                'conditions' => array('ResponseOnline.result_data_id' => $result_data_ids)
            ));

            foreach ($attempts as $attempt_id => $version_data_id) {
                $attempt_cache = $this->ResponseOnline->getCache($attempt_id);
                if (!empty($attempt_cache) && array_key_exists($version_data_id, $partIndices)) {
                    $results = json_decode($attempt_cache['results'], true);

                    $partIndex = $partIndices[$version_data_id];
                    $results[$partIndex] = str_repeat(' ', strlen($results[$partIndex]));
                    $attempt_cache['results'] = json_encode($results);

                    $this->ResponseOnline->updateCache($attempt_cache);
                }
            }
		}

		Cache::delete($sitting_id, 'sittings');

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function get_written($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';
	   	
	   	$resp = array();
		$success = true;

		$question_handle_id = intval($this->request->query['question_handle_id']);
		$part_id = $this->request->query['part_id'];

		$this->Sitting->contain(array(
			'SittingStudent' => array(
				'conditions' => array('SittingStudent.deleted' => null),
				'ResponseOnline' => array('ResultData' => 'VersionData'),
				'User'
			)
		));
		$sitting = $this->Sitting->findById($sitting_id);

		$this->loadModel('ResponseOnline');

		$attemptData = array();
		foreach ($sitting['SittingStudent'] as $sittingStudent) {
			if (!empty($sittingStudent['User'])) $studentEmail = $sittingStudent['User']['email'];
			else unset($studentEmail);

			foreach ($sittingStudent['ResponseOnline'] as $attempt) {
				$attempt_cache = $this->ResponseOnline->getCache($attempt['id']);
				if (!empty($attempt['ResultData']['VersionData'])) {
					$entry = array();
					$entry['id'] = $attempt['id'];
					if (!empty($sittingStudent['details'])) {
						$details = json_decode($sittingStudent['details'], true);
						if ($details != null) $entry['details'] = $details;
					}
					if (isset($studentEmail)) {
						if (empty($entry['details'])) $entry['details'] = array();
						$entry['details']['Email'] = $studentEmail;
					}

					$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
					if ($timeRemaining <= 0.0) $entry['status'] = 'closed';
					else $entry['status'] = 'open';

					if (!empty($attempt_cache['responses'])) {
						$responses = json_decode($attempt_cache['responses'], true);
						if ($responses != null) $entry['responses'] = $responses;
					}

					if (!empty($attempt_cache['annotations'])) {
						$annotations = json_decode($attempt_cache['annotations'], true);
						if ($annotations != null) $entry['annotations'] = $annotations;
					}

					if (!empty($attempt_cache['results'])) {
						$results = json_decode($attempt_cache['results'], true);
						if ($results != null) $entry['results'] = $results;
					}

					if (!empty($attempt['ResultData']['VersionData']['html'])) {
						$entry['html'] = $attempt['ResultData']['VersionData']['html'];
					} else $entry['html'] = gzdecode($attempt['ResultData']['VersionData']['html_gzip']);

					$layout = json_decode($attempt['ResultData']['VersionData']['scoring_json'], true);
					if ($layout !== null) {
						$index = 0;
						foreach ($layout['Sections'] as $section) {
							foreach ($section['Content'] as $question) {
								$isMatch = true;
								if ($question['Source']['question_handle_id'] != $question_handle_id) $isMatch = false;
								else if ($question['Source']['part_id'] != $part_id) $isMatch = false;

								if ($isMatch) {
									$entry['index'] = $index;
                                    if (!empty($question['Rubric']['html'])) {
                                        $entry['rubric'] = $question['Rubric']['html'];
                                    } else $entry['rubric'] = "";
									break;
								}

								++$index;
							}
							if (isset($entry['index'])) break;
						}

						if ($success && array_key_exists('index', $entry)) {
							$attemptData[] = $entry;
						}
					}
				}
			}
		}
		
		$resp['attempt_data'] = $attemptData;
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function save_results($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';
	   	
	   	$resp = array();
		$success = true;

		$this->loadModel('ResponseOnline');
		$this->loadModel('SittingStudent');

		if (!empty($this->request->data['results'])) {
			$results_string = $this->request->data['results'];

			$validResults = true;
			if (empty($this->request->data['results_hash'])) $validResults = false;
			else if ($this->request->data['results_hash'] != md5($results_string)) $validResults = false;
			else {
				$results = json_decode($results_string, true);
				if ($results === null) $validResults = false;
			}

			if (!$validResults) {
				App::uses('Email', 'Lib');
				$user_email = empty($this->Auth->user('id')) ? null : $this->Auth->user('email');
				Email::queue_error("SmarterMarks: Bad results in save_results", $user_email, array(
					'Expected hash' => empty($this->request->data['results_hash']) ? '[empty]' : $this->request->data['results_hash'], 
					'Actual hash' => md5($results_string), 
					'String end' => (strlen($results_string) < 1000) ? $results_string : substr($results_string, -1000)
				));
			}

			if ($validResults) {
				foreach ($results as $id => $entry) {
					$attempt_cache = $this->ResponseOnline->getCache($id);
					if (!empty($attempt_cache)) {
						$sittingStudent_cache = $this->SittingStudent->getCache($attempt_cache['sitting_student_id']);
						if ($sittingStudent_cache['SittingStudent']['sitting_id'] == $sitting_id) {
							$results = json_decode($attempt_cache['results'], true);
							if ($entry['index'] < count($results)) {
								$results[$entry['index']] = $entry['result'];
								$attempt_cache['results'] = json_encode($results);
							} else $success = false;	
	
							$responses = json_decode($attempt_cache['responses'], true);
							if ($entry['index'] < count($responses)) {
								if (!empty($entry['comment'])) {
									$responses[$entry['index']]['comment'] = $entry['comment'];
								} else unset($responses[$entry['index']]['comment']);
								$attempt_cache['responses'] = json_encode($responses);
							} else $success = false;

							$this->ResponseOnline->updateCache($attempt_cache);
						} else $success = false;
					} else $success = false;
				}
			} else $success = false;
		} else $success = false;

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function send() {
		$this->autoRender = false;
		$this->layout = '';
		
		$this->loadModel('User');

		$success = true;

		$email_array = $this->request->data['email_list'];
		$item_ids = $this->request->data['item_ids'];

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$toUsers = array();
		foreach ($email_array as $email) {
			$email = trim($email);
			if (strlen($email) == 0) continue;
			
			$this->User->contain();
			$toUser = $this->User->findByEmail($email);
			if ($toUser && ($toUser['User']['role'] == 'teacher')) {
				$toUsers[] = $toUser;
				
				$userID = $toUser['User']['id'];
				if (($key = array_search($userID, $contact_ids)) !== false) {
					unset($contact_ids[$key]);
				}
				array_unshift($contact_ids, $userID);
			} else $success = false;
		}

		// Update contact IDs
		
		$newUserData = array();
		$newUserData['id'] = $this->Auth->user('id');
		$newUserData['contact_ids'] = implode(',', $contact_ids);
		$this->updateUserData($newUserData);

		// Send sittings

		$this->loadModel('Folder');

		foreach ($toUsers as $toUser) {
			$userID = $toUser['User']['id'];
			$folder_id = $this->Folder->getUserFolder($userID, 'sittings', 'home');

			foreach ($item_ids as $sitting_id) {
				$result = $this->Sitting->copyToFolder($sitting_id, $folder_id, $userID);
				if ($result['success']) {
					$this->Sitting->id = $result['sitting_id'];
					$this->Sitting->saveField('is_new', 3);
				} else $success = false;
			}
		}
		
		echo json_encode(
			array(
			    'success' => $success,
				'numSent' => count($item_ids)
			)
		);
	}

	public function copy() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$folder_id = $this->request->data['folder_id'];
		$item_ids = $this->request->data['item_ids'];

		if (isset($this->request->data['is_new'])) {
			$isNew = $this->request->data['is_new'];
		} else $isNew = 2;
			
		foreach ($item_ids as $sitting_id) {
			$result = $this->Sitting->copyToFolder($sitting_id, $folder_id, $this->Auth->user('id'));
			if ($result['success']) {
				$this->Sitting->id = $result['sitting_id'];
				$this->Sitting->saveField('is_new', $isNew);	

				if ($result['source_folder_id'] == $result['target_folder_id']) {
					$this->Sitting->contain();
					$sitting = $this->Sitting->findById($result['sitting_id']);
					$this->Sitting->saveField('name', $sitting['Sitting']['name'] . ' - Copy');
				}
			} else $success = false;
		}

		$resp['success'] = $success;

		if ($success) {
			$resp['count'] = count($item_ids);

			if (count($item_ids) == 1) {
				$resp['id'] = $result['sitting_id'];
			} else $resp['folder_id'] = $folder_id;
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function move() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$folder_id = $this->request->data['folder_id'];
		$item_ids = $this->request->data['item_ids'];

		$this->loadModel('Folder');
		$trash_folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'sittings', 'trash');
		$isTrash = ($folder_id == $trash_folder_id);

		foreach ($item_ids as $sitting_id) {
			$data = array(
				'id' => $sitting_id,
				'user_id' => $this->Auth->user('id'),
				'folder_id' => $folder_id,
				'moved' => date('Y-m-d H:i:s')
			);
			if (!$isTrash) $data['is_new'] = 2;

			$success &= !empty($this->Sitting->save($data));
		}	

		$resp['success'] = $success;

		if ($success) {
			$resp['count'] = count($item_ids);
			$resp['folder_id'] = $folder_id;
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function empty_trash() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;

		$this->loadModel('Folder');
		$folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'sittings', 'trash');

		$trashSittings = $this->Sitting->find('list', array(
			'fields' => array('Sitting.id'),
			'conditions' => array('Sitting.folder_id' => $folder_id)
		));
		
		$deleted_date = date('Y-m-d H:i:s');
		foreach ($trashSittings as $sitting_id) {
			$this->Sitting->id = $sitting_id;
			$this->Sitting->saveField('deleted', $deleted_date);
		}

		echo json_encode(
			array(
		   		'success' => $success
		   	)
		);
	}	

	public function print_questions($sitting_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

		$success = true;

		$sitting_cache = $this->Sitting->getCache($sitting_id);

		if (array_key_exists('html', $this->request->data)) {
			$assessmentHTML = $this->request->data['html'];
		} else if (!empty($sitting_cache['Version']['html'])) {
			$assessmentHTML = $sitting_cache['Version']['html'];
		} else $success = false;

		if (array_key_exists('suppressInputs', $this->request->data)) {
			if ($this->request->data['suppressInputs']) {
				$wrapper_classes = 'suppress_inputs';
			} else $wrapper_classes = '';
		} else $wrapper_classes = '';

		if ($success) {

			$page_settings = $sitting_cache['Assessment']['Settings'];

			// Build HTML
	
			$view = new View($this, false);
			$view->layout = false;
			$view->viewPath='Sittings';
			$view->set(compact('page_settings', 'assessmentHTML'));
			$view->set('mc_format', 'One');
			$view->set('wrapper_classes', $wrapper_classes);
			$viewHTML = $view->render('print_questions');
	
			// Build and return PDF

			$this->html_to_pdf_response($viewHTML, $page_settings);

		} else {

			$this->response->type('json');
			echo json_encode(array(
				'success' => false
			));
	
		}
	}

	public function save_version($sitting_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;

		// Read and validate layout

		if (!empty($this->request->data['layout_sections'])) {
			$layout_string = $this->request->data['layout_sections'];
			
			if (empty($this->request->data['layout_hash'])) $success = false;
			else if ($this->request->data['layout_hash'] != md5($layout_string)) $success = false;
			else {
				$layoutSections = json_decode($layout_string, true);
				if ($layoutSections == null) $success = false;
			}
		} else $success = false;

		// Read and validate HTML

		if (!empty($this->request->data['html'])) {
			$html = $this->request->data['html'];

			if (empty($this->request->data['html_hash'])) $success = false;
			else if ($this->request->data['html_hash'] != md5($html)) $success = false;
		} else $success = false;

		// Create a new version

		$result = $this->Sitting->create_version($sitting_id, $html, $layoutSections);
		$success &= $result['success'];

		if ($success) {
			$this->Sitting->id = $sitting_id;
			$this->Sitting->saveField('version_data_id', $result['version_data_id']);
		}

		Cache::delete($sitting_id, 'sittings');

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
}

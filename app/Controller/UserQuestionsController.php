<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class UserQuestionsController extends AppController {

    public $presetVars = true; // using the model configuration

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
	    }

		if (in_array($this->action, array('send'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (isset($this->request->data['item_ids']) && is_array($this->request->data['item_ids'])) {
				$userquestion_ids = $this->request->data['item_ids'];

				foreach ($userquestion_ids as $userquestion_id) {
					$itemPermissions = $this->UserQuestion->getPermissions($userquestion_id, $this->Auth->user('id'));
					if (!in_array('can_export', $itemPermissions)) return false;
				}
				return true;

			} else return false;
		}

		if (in_array($this->action, array('move'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (isset($this->request->data['item_ids']) && is_array($this->request->data['item_ids'])) {
				$this->loadModel('Question');
				$this->loadModel('QuestionHandle');

				foreach ($this->request->data['item_ids'] as $user_question_id) {
					if (!empty($this->request->data['folder_id'])) {
						$this->loadModel('Folder');
						
						$targetFolderId = $this->request->data['folder_id'];
						$targetPermissions = $this->Folder->getContentPermissions($targetFolderId, 
							$this->Auth->user('id'));

						if (!in_array('can_add', $targetPermissions['all'])) return false;
					} else return false;

					$this->UserQuestion->contain('QuestionHandle');
					$userQuestion = $this->UserQuestion->findById($user_question_id);
					if (empty($userQuestion)) return false;

					$sourceFolderId = $userQuestion['UserQuestion']['folder_id'];
					$itemPermissions = $this->UserQuestion->getPermissions($userQuestion['UserQuestion']['id'], 
						$this->Auth->user('id'));

					if (!in_array('can_delete', $itemPermissions)) return false;
					if ($this->Folder->getCommunityId($sourceFolderId) != $this->Folder->getCommunityId($targetFolderId)) {
						if (!in_array('can_export', $itemPermissions)) return false;
					}
				}
				return true;

			} else return false;
		}

		if (in_array($this->action, array('copy'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (isset($this->request->data['item_ids']) && is_array($this->request->data['item_ids'])) {
				$this->loadModel('Question');
				$this->loadModel('Folder');

				foreach ($this->request->data['item_ids'] as $entry) {
					if (!empty($this->request->data['folder_id'])) {
						$targetFolderId = $this->request->data['folder_id'];
						$targetPermissions = $this->Folder->getContentPermissions($targetFolderId, 
							$this->Auth->user('id'));

						if (!in_array('can_add', $targetPermissions['all'])) return false;
					} else return false;

					if (isset($entry['user_question_id'])) {

						$userQuestionID = $entry['user_question_id'];

						$this->UserQuestion->contain();
						$userQuestion = $this->UserQuestion->findById($userQuestionID);
						if (empty($userQuestion)) return false;

						$sourceFolderId = $userQuestion['UserQuestion']['folder_id'];
						if ($this->Folder->getCommunityId($sourceFolderId) != $this->Folder->getCommunityId($targetFolderId)) {
							$itemPermissions = $this->UserQuestion->getPermissions($userQuestion['UserQuestion']['id'], 
								$this->Auth->user('id'));
							if (!in_array('can_export', $itemPermissions)) return false;
						}

					} else if (isset($entry['question_id'])) {

						$question_id = $entry['question_id'];

						$this->Question->contain();
						$question = $this->Question->findById($question_id);
						if (empty($question) || ($question['Question']['open_status'] == 0)) return false;

					}	
				}
				return true;

			} else return false;
		}

	    if (in_array($this->action, array('delete_deleted'))) {
		    return ($this->Auth->user('role') == 'admin');
	    }
	    
	    if (in_array($this->action, array('get_open'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (isset($this->request->params['pass'][0])) {
				if (is_numeric($this->request->params['pass'][0])) {
					$this->loadModel('Folder');
					$folderId = $this->request->params['pass'][0];
					$permissions = $this->Folder->getContentPermissions($folderId, $this->Auth->user('id'));
					return in_array('can_view', $permissions['all']);
				} else return false;
			} else return true;
	    }

		if (in_array($this->action, array('get_sync_questions'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			$this->loadModel('Folder');
			if (!empty($this->request->data['source_folder'])) {
				$sourceFolder = $this->request->data['source_folder'];
				$permissions = $this->Folder->getContentPermissions($sourceFolder, $this->Auth->user('id'));
				if (!in_array('can_export', $permissions['all'])) return false;
			} else return false;
			if (!empty($this->request->data['target_folder'])) {
				$targetFolder = $this->request->data['target_folder'];
				$permissions = $this->Folder->getContentPermissions($targetFolder, $this->Auth->user('id'));
				if (!in_array('can_add', $permissions['all'])) return false;
			} else return false;
			return true;
		}

		if (in_array($this->action, array('link_handles'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (!empty($this->request->data['handle_ids'])) {
				$handleIDs = $this->request->data['handle_ids'];
				$this->loadModel('QuestionHandle');
				foreach ($handleIDs as $handleId) {
					$permissions = $this->QuestionHandle->getPermissions($handleId, $this->Auth->user('id'));
					if (!in_array('can_view', $permissions)) return false;
				}
				return true;
			} else return false;
			
			if (!empty($this->request->data['folder_id'])) {
				$this->loadModel('Folder');
				$folderId = $this->request->data['folder_id'];
				$permissions = $this->Folder->getContentPermissions($folderId, $this->Auth->user('id'));
				return in_array('can_add', $permissions['all']);
			} else return false;
		}
		
		return false;
	}
	
	public function send() {
		$this->autoRender = false;
		$this->layout = '';
		
		$this->loadModel('User');

		$success = true;

		// Update contact IDs
		
		$email_array = $this->request->data['email_list'];
		$item_ids = $this->request->data['item_ids'];

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$toUsers = array();
		foreach ($email_array as $email) {
			$email = trim($email);
			if (strlen($email) == 0) continue;
			
			$this->User->contain();
			$toUser = $this->User->findByEmail($email);
			if ($toUser && ($toUser['User']['role'] == 'teacher')) {
				$toUsers[] = $toUser;
				
				$userID = $toUser['User']['id'];
				if (($key = array_search($userID, $contact_ids)) !== false) {
					unset($contact_ids[$key]);
				}
				array_unshift($contact_ids, $userID);
			} else $success = false;
		}

		$newUserData = array();
		$newUserData['id'] = $this->Auth->user('id');
		$newUserData['contact_ids'] = implode(',', $contact_ids);

		$this->updateUserData($newUserData);	

		// Send questions
		
		$question_ids = array();
		
		$this->loadModel('Folder');

		foreach ($toUsers as $toUser) {
			$userID = $toUser['User']['id'];
			$folder_id = $this->Folder->getUserFolder($userID, 'questions', 'home');

			foreach ($item_ids as $userQuestion_id) {
				$result = $this->UserQuestion->copyToFolder(array(
					'user_question_id' => $userQuestion_id
				), $folder_id, $userID);

				if ($result['success']) {
					$question_ids[] = $result['question_id'];

					$this->UserQuestion->id = $result['user_question_id'];
					$this->UserQuestion->saveField('is_new', 3);
				} else $success = false;	
			}
		}

		if (count($question_ids) > 0) {
			$this->loadModel('Question');
            $db = $this->Question->getDataSource();
            $this->Question->updateAll(
                array('needs_index' => $db->value(date('Y-m-d H:i:s'), 'string')),
                array('Question.id' => $question_ids)
            );
		}
		
		echo json_encode(
			array(
			    'success' => $success,
				'numSent' => count($item_ids)
			)
		);
	}

	public function copy() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$folder_id = $this->request->data['folder_id'];
		$item_ids = $this->request->data['item_ids'];

		if (isset($this->request->data['is_new'])) {
			$isNew = $this->request->data['is_new'];
		} else $isNew = 2;

		$force_copy = false;
		if (isset($this->request->data['force_copy'])) {
			$force_copy = $this->request->data['force_copy'];
		}

		foreach ($item_ids as $entry) {
			$result = $this->UserQuestion->copyToFolder($entry, $folder_id, $this->Auth->user('id'), $force_copy);

			if ($result['success']) {
				$this->UserQuestion->id = $result['user_question_id'];
				$this->UserQuestion->saveField('is_new', $isNew);

				if ($result['source_folder_id'] == $result['target_folder_id']) {
					$this->loadModel('QuestionHandle');
					$this->QuestionHandle->contain();
					$question_handle = $this->QuestionHandle->findById($result['question_handle_id']);
					$this->QuestionHandle->saveField('name_alias', $question_handle['QuestionHandle']['name_alias'] . ' - Copy');
				}
			} else $success = false;
		}

		$resp['success'] = $success;

		if ($success) {
			$resp['count'] = count($item_ids);

			if (count($item_ids) == 1) {
				$resp['id'] = $result['question_id'];
				$resp['question_handle_id'] = $result['question_handle_id'];
				$resp['user_question_id'] = $result['user_question_id'];
			} else $resp['folder_id'] = $folder_id;
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function move() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$folder_id = $this->request->data['folder_id'];
		$user_question_ids = $this->request->data['item_ids'];

		$this->loadModel('Folder');
		$trash_folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'questions', 'trash');
		$isTrash = ($folder_id == $trash_folder_id);

		$handleData = array();
		$targetCommunity = $this->Folder->getCommunityId($folder_id);
		if ($targetCommunity) $handleData['community_id'] = $targetCommunity;
		else $handleData['user_id'] = $this->Auth->user('id');

		foreach ($user_question_ids as $user_question_id) {
			$this->UserQuestion->contain('QuestionHandle');
			$userQuestion = $this->UserQuestion->findById($user_question_id);
			if (!empty($userQuestion)) {

				// Get a handle for this question

				$handleData['question_id'] = $userQuestion['QuestionHandle']['question_id'];
				$handleData['name_alias'] = $userQuestion['QuestionHandle']['name_alias'];
				$new_handle_id = $this->QuestionHandle->createHandle($handleData, false);

				// Move UserQuestion
				
				$data = array(
					'id' => $user_question_id,
					'user_id' => $this->Auth->user('id'),
					'folder_id' => $folder_id,
					'question_handle_id' => $new_handle_id,
					'moved' => date('Y-m-d H:i:s')
				);
				if (!$isTrash) $data['is_new'] = 2;
	
				$success &= !empty($this->UserQuestion->save($data));
					
    			// Queue an update to the search index

				$this->loadModel('Question');
                $this->Question->id = $userQuestion['QuestionHandle']['question_id'];
                $this->Question->saveField('needs_index', date('Y-m-d H:i:s'));

			} else $success = false;
		}	

		$resp['success'] = $success;

		if ($success) {
			$resp['count'] = count($user_question_ids);
			$resp['folder_id'] = $folder_id;
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function delete_deleted() {
		$this->autoRender = false;
		$this->layout = '';
		
		$ids = $this->UserQuestion->find('list', array(
			'fields' => array('UserQuestion.id'),
			'conditions' => array(
				'NOT' => array('UserQuestion.deleted' => null)
			)
		));
		foreach ($ids as $id) {
			$this->UserQuestion->delete($id);
		}

		$this->Flash->success("Deleted " . count($ids) . " UserQuestions.");			
		$this->redirect(array('controller' => 'UserQuestions', 'action' => 'index'));
	}
	
	public function get_sync_questions() {
		$this->autoRender = false;
		$this->layout = '';
		
		$sourceFolderId = $this->request->data['source_folder'];
		$targetFolderId = $this->request->data['target_folder'];
		
		$success = true;

// Get a list including the source folder and its subfolders

		$this->loadModel('Folder');
		$search_folders = array();
		$edge_folders = array($sourceFolderId);
		do {
			$search_folders = array_merge($search_folders, array_values($edge_folders));
			$edge_folders = $this->Folder->find('list', array(
				'conditions' => array('Folder.parent_id' => $edge_folders),
				'fields' => array('Folder.id')
			));
		} while (!empty($edge_folders));
		$foldersList = implode(',', $search_folders);
		
// Get the conditions to match a handle in the target community

		$this->Folder->contain();
		$targetFolder = $this->Folder->findById($targetFolderId);
		if ($targetFolder['Folder']['community_id'] != null) {
			$targetCondition = 't.community_id = ' . $targetFolder['Folder']['community_id'];
		} else {
			$targetCondition = 't.user_id = ' . $targetFolder['Folder']['user_id'];
		}
		
// Search through search folders to find questions with no handle in the target community

		$result = $this->UserQuestion->query('
			SELECT h.question_id, h.name_alias, q.name
			FROM user_questions as u
			    LEFT JOIN question_handles as h
			    ON u.question_handle_id = h.id
					LEFT JOIN questions as q
					ON q.id = h.question_id
			        LEFT JOIN question_handles as t
			        ON (t.question_id = h.question_id) AND (' . $targetCondition . ')
			WHERE (u.deleted IS NULL) AND (t.question_id IS NULL) AND 
				(u.folder_id IN (' . $foldersList . '))
		');
		
		$missing = array();
		foreach ($result as $entry) {
			$data = array();
			$data['question_id'] = $entry['h']['question_id'];
			$data['name'] = $entry['h']['name_alias'];
			$missing[] = $data;
		}
		
// Find "roots" of the question tree that contains these questions
		
		$this->loadModel('Question');
		
		$workingIDs = array();
		foreach ($missing as $entry) {
			$workingIDs[] = $entry['question_id'];
		}
		$rootIDs = array();
		do {
			$this->Question->contain();
			$children = $this->Question->find('all', array(
				'conditions' => array('Question.id' => $workingIDs)
			));
			
			$workingIDs = array();
			foreach ($children as $child) {
				if ($child['Question']['parent_id'] != null) {
					$workingIDs[] = $child['Question']['parent_id'];
				} else $rootIDs[] = $child['Question']['id'];
			}
		} while (!empty($workingIDs));

// Get data for the whole question tree
		
		$questionData = array();
		$this->Question->contain(array('QuestionHandle'));
		$workingData = $this->Question->find('all', array(
			'conditions' => array('Question.id' => $rootIDs)
		));

		do {
			$parentIDs = array();
			foreach ($workingData as $question) {
				$parentIDs[] = $question['Question']['id'];
				
				$inTarget = false;
				foreach ($question['QuestionHandle'] as $handle) {
					if ($targetFolder['Folder']['community_id'] != null) {
						if ($handle['community_id'] == $targetFolder['Folder']['community_id']) {
							$inTarget = true;
						}
					} else {
						if ($handle['user_id'] == $targetFolder['Folder']['user_id']) {
							$inTarget = true;
						}
					}
				}
				
				$questionData[$question['Question']['id']] = array(
					'parent_id' => $question['Question']['parent_id']
				);				
				
				$rootID = $question['Question']['id'];
				if ($inTarget) {
					while ($questionData[$rootID]['parent_id'] !== null) {
						$rootID = $questionData[$rootID]['parent_id'];
					}
					if (!isset($questionData[$rootID]['target_descendants'])) {
						$questionData[$rootID]['target_descendants'] = array();
					}
					$questionData[$rootID]['target_descendants'][] = $question['Question']['id'];
				}				
			}

			$this->Question->contain(array('QuestionHandle'));
			$workingData = $this->Question->find('all', array(
				'conditions' => array('Question.parent_id' => $parentIDs)
			));
			
		} while (!empty($workingData));
		
// Check for missing questions that have a relative, ancestor, or descendant in the target community

		$missingData = array();
		foreach ($missing as &$entryRef) {
			$question_id = $entryRef['question_id'];

			$rootID = $question_id;
			while ($questionData[$rootID]['parent_id'] !== null) {
				$rootID = $questionData[$rootID]['parent_id'];
			}
			
			if (isset($questionData[$rootID]['target_descendants'])) {
				$targetRelatives = $questionData[$rootID]['target_descendants'];
				$targetAncestors = array();
				$targetDescendants = array();

				// Find direct ancestors that are in the target community

				$workingID = $question_id;
				while ($questionData[$workingID]['parent_id'] !== null) {
					$workingID = $questionData[$workingID]['parent_id'];
					$index = array_search($workingID, $targetRelatives);
					if ($index !== false) {
						$targetAncestors[] = $workingID;
						array_splice($targetRelatives, $index, 1);
					}
				}
				
				// Find descendants that are in the target community

				$parentIDs = array($question_id);
				do {
					$childIDs = array();
					foreach ($parentIDs as $parentID) {
						foreach ($questionData as $workingID => $workingData) {
							if ($workingData['parent_id'] == $parentID) {
								$index = array_search($workingID, $targetRelatives);
								if ($index !== false) {
									$targetDescendants[] = $workingID;
									array_splice($targetRelatives, $index, 1);
								}
								$childIDs[] = $workingID;
							}
						}
					}
					$parentIDs = $childIDs;
				} while (!empty($parentIDs));
				
				// Add to data as required
				
				if (!empty($targetRelatives)) $entryRef['related_ids'] = $targetRelatives;
				if (!empty($targetAncestors)) $entryRef['ancestor_ids'] = $targetAncestors;
				if (!empty($targetDescendants)) $entryRef['descendant_ids'] = $targetDescendants;
			}
		}

		echo json_encode(
			array(
			    'success' => $success,
			    'missing' => $missing
			)
		);
	}
	
	public function link_handles() {
		$this->autoRender = false;
		$this->layout = '';
		
		$folderId = $this->request->data['folder_id'];
		$handleIDs = $this->request->data['handle_ids'];
		
		$success = true;
		foreach ($handleIDs as $handleID) {
			$data = array(
				'user_id' => $this->Auth->user('id'),
				'folder_id' => $folderId,
				'question_handle_id' => $handleID,
				'moved' => date('Y-m-d H:i:s'),
				'is_new' => 2
			);

			$this->UserQuestion->create();
			$success &= !empty($this->UserQuestion->save($data));
		}

		echo json_encode(
			array(
			    'success' => $success
			)
		);
	}

	public function get_open($folder_id = null) {
		$this->autoRender = false;
		$this->layout = '';
		
		$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'questions');
		$search_folders = array();
		$edge_folders = array($folder_id);
		do {
			$search_folders = array_merge($search_folders, $edge_folders);
			$new_folders = array();
			foreach ($nodes as $node) {
				if (in_array($node['data']['parent_id'], $edge_folders)) {
					$new_folders[] = $node['data']['folder_id'];
				}
			}
			$edge_folders = $new_folders;
		} while (count($edge_folders) > 0);
		
		$count = $this->UserQuestion->find('count', array(
			'conditions' => array(
				'UserQuestion.folder_id' => $search_folders,
				'UserQuestion.deleted IS NULL'
			)
		));

		$unshared = $this->UserQuestion->find('count', array(
            'joins' => array(
	            array(
		            'table' => 'question_handles',
		            'alias' => 'QuestionHandle',
		            'type' => 'LEFT',
		            'conditions' => array(
			            'QuestionHandle.id = UserQuestion.question_handle_id'
			        )
		        ),
	            array(
		            'table' => 'questions',
		            'alias' => 'Question',
		            'type' => 'LEFT',
		            'conditions' => array(
			            'Question.id = QuestionHandle.question_id'
			        )
		        )
		    ),
			'conditions' => array(
				'UserQuestion.folder_id' => $search_folders,
				'UserQuestion.deleted IS NULL',
				'Question.open_status' => 0
			)
		));

		$this->UserQuestion->contain(array('QuestionHandle' => 'Question'));
		$userQuestions = $this->UserQuestion->find('all', array(
			'limit' => 100,
			'order' => 'UserQuestion.created DESC',
			'conditions' => array(
				'UserQuestion.folder_id' => $search_folders,
				'UserQuestion.deleted IS NULL'
			)
		));
		
		$data = array();
		foreach ($userQuestions as $userQuestion) {
			$thisData = array();
			$thisData['name'] = $userQuestion['QuestionHandle']['name_alias'];
			if ($userQuestion['QuestionHandle']['Question']['open_status'] == 0) {
				$thisData['policy'] = 'private';
			} else if ($userQuestion['QuestionHandle']['Question']['open_status'] == $this->Auth->user('id')) {
				$thisData['policy'] = 'open';
			} else $thisData['policy'] = 'locked';
			$data[] = $thisData;
		}

		echo json_encode(array(
			'count' => $count,
			'unshared' => $unshared,
			'data' => $data
		));
	}
}

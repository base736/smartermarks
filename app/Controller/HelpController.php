<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class HelpController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();	    
	}

	// isAuthorized function for this Controller
	public function isAuthorized($user) {
		if (parent::isAuthorized($user)) {
	        return true;
	    }
	    
	    return true;
	}

	public function view($includeName = "introduction") {
		$this->set('includeName', $includeName);
		$this->set('imagePath', "/img/help");
	}	
}

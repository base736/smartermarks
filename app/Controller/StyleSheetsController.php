<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class StyleSheetsController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {
		
		if (parent::isAuthorized($user)) {
	        return true;
	    }

        if (in_array($this->action, array('index'))) {
            return true;
        }

	    if (in_array($this->action, array('get'))) {
		    return !empty($this->Auth->user('id'));
	    }

	    if (in_array($this->action, array('save'))) {
            if (empty($this->Auth->user('id'))) return false;

            if (array_key_exists('id', $this->request->data)) {
                $style_sheet_id = $this->request->data['id'];
                $this->StyleSheet->contain();
                $style_sheet = $this->StyleSheet->findById($style_sheet_id);

                if (empty($style_sheet)) return false;
                else return ($style_sheet['StyleSheet']['user_id'] == $this->Auth->user('id'));
             } else return true;
	    }

        if (in_array($this->action, array('delete', 'send'))) {
	    	if (!empty($this->request->data['sheet_ids'])) {
                $sheet_ids = array();
                foreach ($this->request->data['sheet_ids'] as $sheet_id) {
                    if (is_numeric($sheet_id)) $sheet_ids[] = intval($sheet_id);
                    else return false;
                }

                $user_ids = $this->StyleSheet->find('list', array(
                    'fields' => array('StyleSheet.id', 'StyleSheet.user_id'),
                    'conditions' => array('StyleSheet.id' => $sheet_ids)
                ));

                foreach ($user_ids as $sheet_id => $user_id) {
                    if ($user_id != $this->Auth->user('id')) return false;
                }
                return true;

			} else return false;
		}

	}

    public function index() {

        // Get count of received items

        $receivedCount = $this->StyleSheet->find('count', array(
            'conditions' => array(
                'StyleSheet.user_id' => $this->Auth->user('id'),
                'StyleSheet.is_new' => 3
            )
        ));

        // Get contacts

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$this->User->contain();
		$contactRecords = $this->User->find('all', array(
			'conditions' => array('User.id' => $contact_ids))
		);
		$contacts = array();
		foreach ($contact_ids as $id) {
			foreach ($contactRecords as $record) {
				if ($record['User']['id'] == $id) {
					$contacts[] = array(
						'id' => $record['User']['id'],
						'name' => $record['User']['name'],
						'email' => $record['User']['email']
					);
				}
			}
		}
        
        $this->set(compact('receivedCount', 'contacts'));
	}

    public function get() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

        $resp = array();

        $this->StyleSheet->contain();
        $style_sheets = $this->StyleSheet->find('all', array(
            'conditions' => array('StyleSheet.user_id' => $this->Auth->user('id'))
        ));

        $this->StyleSheet->updateAll(
            array('StyleSheet.is_new' => 0),
            array('StyleSheet.user_id' => $this->Auth->user('id'))
        );

        $results = array();
        foreach ($style_sheets as $style_sheet) {
            $results[] = $style_sheet['StyleSheet'];
        }

		$resp['results'] = $results;
		$resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);        
    }

    public function delete() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

        $resp = array();

        $sheet_ids = $this->request->data['sheet_ids'];
        $success &= $this->StyleSheet->deleteAll(array(
            'StyleSheet.id' => $sheet_ids
        ), true, true);

        $resp['count'] = count($sheet_ids);
		$resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);        
    }    

    public function save() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

        $resp = array();

        $data = array(
            'name' => $this->request->data['name'],
            'json_data' => $this->request->data['json_data']
        );
        if (array_key_exists('id', $this->request->data)) {
            $data['id'] = $this->request->data['id'];
            $data['is_new'] = 0;
        } else {
            $data['user_id'] = $this->Auth->user('id');
            $data['is_new'] = 1;
            $this->StyleSheet->create();
        }

        $success &= !empty($this->StyleSheet->save($data));
        
        $resp['id'] = $this->StyleSheet->id;
		$resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);        
    }

    public function send() {
		$this->autoRender = false;
		$this->layout = '';
		
		$this->loadModel('User');

		$success = true;

		$email_array = $this->request->data['email_list'];
		$item_ids = $this->request->data['sheet_ids'];

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$toUsers = array();
		foreach ($email_array as $email) {
			$email = trim($email);
			if (strlen($email) == 0) continue;
			
			$this->User->contain();
			$toUser = $this->User->findByEmail($email);
			if ($toUser && ($toUser['User']['role'] == 'teacher')) {
				$toUsers[] = $toUser;
				
				$userID = $toUser['User']['id'];
				if (($key = array_search($userID, $contact_ids)) !== false) {
					unset($contact_ids[$key]);
				}
				array_unshift($contact_ids, $userID);
			} else $success = false;
		}

		// Update contact IDs
		
		$newUserData = array();
		$newUserData['id'] = $this->Auth->user('id');
		$newUserData['contact_ids'] = implode(',', $contact_ids);
		$this->updateUserData($newUserData);

		// Send class lists

        $this->StyleSheet->contain();
        $original_items = $this->StyleSheet->find('all', array(
            'conditions' => array('StyleSheet.id' => $item_ids)
        ));
        foreach ($original_items as &$pItem) {
            $pItem = $pItem['StyleSheet'];
            unset($pItem['id']);
            unset($pItem['created']);
            $pItem['last_used'] = null;
            $pItem['is_new'] = 3;
        }

		foreach ($toUsers as $toUser) {
			$userID = $toUser['User']['id'];
            foreach ($original_items as $item) {
                $item['user_id'] = $userID;
                $this->StyleSheet->create();
                $this->StyleSheet->save($item);
            }
		}
		
		echo json_encode(
			array(
			    'success' => $success,
				'numSent' => count($item_ids)
			)
		);
	}
}

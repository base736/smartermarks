<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class CommunitiesController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
		}
		
	    if (in_array($this->action, array('get_data'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$community_id = $this->request->params['pass'][0];
				
			    $this->loadModel('CommunityUser');
			    $this->CommunityUser->contain();
			    $communityUser = $this->CommunityUser->find('first', array(
				    'conditions' => array(
					    'CommunityUser.community_id' => $community_id,
					    'CommunityUser.user_id' => $this->Auth->user('id')
				    )
			    ));
			    return !empty($communityUser);				
			} else return false;
	    }

	    if (in_array($this->action, array('save'))) {
		    if (!empty($this->request->data['Community']['id'])) {
			    $community_id = $this->request->data['Community']['id'];	    
			    $this->loadModel('CommunityUser');
			    $this->CommunityUser->contain('Community');
			    $communityUser = $this->CommunityUser->find('first', array(
				    'conditions' => array(
					    'CommunityUser.community_id' => $community_id,
					    'CommunityUser.user_id' => $this->Auth->user('id')
				    )
			    ));
			    
			    if ($communityUser) {
				    if ($communityUser['Community']['user_add_policy'] == 'admin') {
				    	return ($communityUser['CommunityUser']['is_admin'] == 1);
				    } else return true;
			    } else return false;
		    } else return true;
		}
		
		return false;
	}

	public function get_data($community_id) {
		$this->autoRender = false;
		$this->layout = '';

	    $this->loadModel('CommunityUser');
	    $this->CommunityUser->contain();
	    $communityUser = $this->CommunityUser->find('first', array(
		    'conditions' => array(
			    'CommunityUser.community_id' => $community_id,
			    'CommunityUser.user_id' => $this->Auth->user('id')
		    )
	    ));

		if (!empty($communityUser) && ($communityUser['CommunityUser']['is_admin'] == 1)) {
			$containArray = array('CommunityUser' => 'User');
		} else {
			$containArray = array(
				'CommunityUser' => array(
					'conditions' => array('OR' => array(
						'CommunityUser.is_admin' => 1,
						'CommunityUser.user_id' => $this->Auth->user('id')
					)), 
					'User'
				)
			);
		}
		$community = $this->Community->find('first', array(
			'conditions' => array('Community.id' => $community_id),
			'contain' => $containArray
		));
		
		foreach ($community['CommunityUser'] as &$thisCommunityUser) {
			foreach ($thisCommunityUser['User'] as $key => $value) {
				if ($key != 'email') unset($thisCommunityUser['User'][$key]);
			}
		}
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($community);
	}
	
	public function save() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

		// Set up community if it doesn't exist yet

		$this->loadModel('Folder');
			
		$isCommunityAdmin = true;

		$folderTypes = array('forms', 'assessments', 'questions', 'sittings');

		$this->log($this->request->data, 'test');

		$community = $this->request->data['community'];
		$newCommunity = empty($community['id']);
		if ($newCommunity) {
			
			unset($community['id']);
			
			$data = array();
			$data['parent_id'] = null;
			$data['user_id'] = null;
			$data['name'] = $community['name'];

			$folder_ids = array();
			foreach ($folderTypes as $type) {
				$data['type'] = $type;
				$this->Folder->create();
				$success &= !empty($this->Folder->save($data));
				$folder_ids[$type] = $this->Folder->id;
			}
			$community['folder_ids'] = json_encode($folder_ids);

			$this->Community->create();

		} else {

			$this->loadModel('CommunityUser');

			$this->CommunityUser->contain();
		    $communityUser = $this->CommunityUser->find('first', array(
			    'conditions' => array(
				    'CommunityUser.community_id' => $community['id'],
				    'CommunityUser.user_id' => $this->Auth->user('id')
			    )
		    ));
		    if ($communityUser['CommunityUser']['is_admin'] != 1) {
			    $isCommunityAdmin = false;
		    }		    

		}

		$success &= !empty($this->Community->save($community));
		$community_id = $this->Community->id;
		
		$this->Community->contain();
		$community = $this->Community->findById($community_id);

		$data = array();
		if ($newCommunity) {
			$data['community_id'] = $community['Community']['id'];
		} else {
			$data['name'] = $community['Community']['name'];
			$folder_ids = json_decode($community['Community']['folder_ids'], true);
		}
		foreach ($folderTypes as $type) {
			$data['id'] = $folder_ids[$type];
			$this->Folder->save($data);
		}

		// Update members

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$members = json_decode($this->request->data['members_json'], true);
		$memberArray = array();
		foreach ($members['admin'] as $email) $memberArray[strtolower($email)] = 1;
		foreach ($members['other'] as $email) $memberArray[strtolower($email)] = 0;
		
		$this->loadModel('User');
		$this->loadModel('CommunityUser');
		foreach ($memberArray as $email => $is_admin) {
			$this->User->contain();
			$user = $this->User->findByEmail($email);
			if (!empty($user)) {
				$user_id = $user['User']['id'];
				
				$this->CommunityUser->contain();
				$communityUser = $this->CommunityUser->find('first', array(
				    'conditions' => array(
					    'CommunityUser.community_id' => $community_id,
					    'CommunityUser.user_id' => $user_id
				    )
				));
				if (!empty($communityUser)) {
					if ($isCommunityAdmin) {
						$data = array();
						$data['id'] = $communityUser['CommunityUser']['id'];
						$data['is_admin'] = $is_admin;
						if ($success) $this->CommunityUser->save($data);
					}
				} else {
					if (($key = array_search($user_id, $contact_ids)) !== false) {
						unset($contact_ids[$key]);
					}
					array_unshift($contact_ids, $user_id);
					
					if (!$isCommunityAdmin) $is_admin = 0;

					if (($user_id == $this->Auth->user('id')) && !empty($this->request->data['folder_id'])) {
						$folder_id = $this->request->data['folder_id'];						
					} else {
						$folder_id = $this->Folder->getUserFolder($user_id, 'communities');
					}

					$data = array();
					$data['community_id'] = $community_id;
					$data['user_id'] = $user_id;
					$data['folder_id'] = $folder_id;
					$data['is_admin'] = $is_admin;
					$this->CommunityUser->create();
					$success &= !empty($this->CommunityUser->save($data));
				}
			} else $success = false;			
		}
		
		// Remove members that have been deleted
		
		if ($isCommunityAdmin) {
			$allUsers = $this->CommunityUser->find('list', array(
	            'joins' => array(
		            array(
			            'table' => 'users',
			            'alias' => 'User',
			            'type' => 'LEFT',
			            'conditions' => array(
				            'User.id = CommunityUser.user_id'
				        )
			        )
			    ),
				'fields' => array('CommunityUser.id', 'User.email'),
			    'conditions' => array(
				    'CommunityUser.community_id' => $community_id
			    )
			));
			
			foreach ($allUsers as $id => $email) {
				if (!isset($memberArray[strtolower($email)])) $this->CommunityUser->delete($id);
			}
			
			if (count($memberArray) == 0) {
				$data = array();
				$data['id'] = $community_id;
				$data['deleted'] = date('Y-m-d H:i:s');
				$this->Community->save($data);
			}
		}
		
		// Update contact IDs
		
		$newUserData = array();
		$newUserData['id'] = $this->Auth->user('id');
		$newUserData['contact_ids'] = implode(',', $contact_ids);
		$this->updateUserData($newUserData);

		$resp = array();
		$resp['success'] = $success;
		$resp['community'] = $community['Community'];
		$resp['members'] = $this->request->data['members_json'];
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}	
}


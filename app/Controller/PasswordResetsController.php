<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class PasswordResetsController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('send_reset', 'email_sent', 'remote_auth', 'new_password');
	}

	public function send_reset() {
		$this->set('title_for_layout', 'Reset Your Password');

		$this->loadModel('User');

		if (!empty($this->request->data)) {

			$email = $this->request->data['PasswordReset']['email'];

			$this->User->contain();
			$user = $this->User->findByEmail($email);

			if (!empty($user)) {

				$auth_details = $this->User->get_auth_details($user['User']['email']);
				if ($auth_details['auth_method'] == 'local') {

					$token = $this->PasswordReset->getReset($user['User']['id']);
					if ($token !== false) {
		
						// Email User
		
						App::uses('Email', 'Lib');
						Email::send($user['User']['email'], "SmarterMarks: Password Reset", "newpw",
							array('token' => $token, 'userData' => $user['User']));
						$this->redirect(array('action' => 'email_sent', $user['User']['id']));

					} else {

						$this->Flash->error('There was an error resetting your password.');
						$this->redirect(array('action' => 'send_reset'));

					}

				} else {

					$this->redirect(array('action' => 'remote_auth', $user['User']['id']));

				}

			} else {

				$this->Flash->error('No account found with that email.');
				$this->redirect(array('action' => 'send_reset'));

			}
		}
	}

	public function email_sent($userID) {
		$this->loadModel('User');

		$this->User->contain();
		$user = $this->User->findById($userID);
		$email = $user['User']['email'];
		
		$this->set(compact('email'));
	}

	public function remote_auth($userID) {
		$this->loadModel('User');

		$reset_url = false;

		$this->User->contain();
		$user = $this->User->findById($userID);
		
		if ($user) {
			$auth_details = $this->User->get_auth_details($user['User']['email']);
			if (!empty($auth_details['reset_url'])) $reset_url = $auth_details['reset_url'];	
		}

		$this->set(compact('reset_url'));
	}

	function new_password($token = null) {	
		$this->PasswordReset->deleteAll('PasswordReset.created <= DATE_SUB(NOW(), INTERVAL 24 HOUR)');
		$this->PasswordReset->contain();
		$ticket = $this->PasswordReset->findByToken($token);

		if (empty($ticket)) {
			$this->Flash->error("The link you clicked has expired. Please enter your email again.");
			$this->redirect(array('action' => 'send_reset'));
		}

		if ($this->Auth->user('role') == 'admin' && $ticket['PasswordReset']['user_id'] != $this->Auth->user('id')) {
			$this->set('title_text', 'Reset user password');
			$this->set('title_for_layout', 'Reset user password');
		} else {
			$this->set('title_text', 'Choose a new password');
			$this->set('title_for_layout', 'Choose a new password');
		}

		$userID = $ticket['PasswordReset']['user_id'];

		$this->loadModel('User');
		$this->User->contain();
		$ticketUser = $this->User->findById($userID);
		$this->set(compact('ticketUser'));

		if (!empty($this->request->data)) {

			// Update password

			$password = $this->request->data['PasswordReset']['password'];
			$entropy = $this->User->getEntropy($password);

			$data = array();
			$data['id'] = $userID;
			$data['password'] = $password;
			$data['validation_token'] = null;
			$data['entropy'] = $entropy;

			if ($this->updateUserData($data)) {
				if ($entropy < 32) {
					$this->Flash->error("This password does not meet security requirements.");
					$this->redirect(array('controller' => 'PasswordResets', 'action' => 'new_password', $token));
				} else {
					$this->PasswordReset->delete($ticket['PasswordReset']['id']);

					if ($this->Auth->user('role') == 'admin') {
						$this->Flash->success("User password changed.");
						$this->redirect(array('controller' => 'Users', 'action' => 'edit', $this->User->id));
					} else {
						$this->Flash->success("Your password has been changed.");
						$this->redirect_home();
					}
				}
			} else {
				$this->Flash->error("There was an error changing your password.");
				$this->redirect(array('controller' => 'PasswordResets', 'action' => 'new_password', $token));
			}
		}
	}
}

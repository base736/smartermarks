<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

// Default pages on the site. This is used for pages that are not attached to a Model/Controller.

App::uses('AppController', 'Controller');

App::import('Vendor', 'fpdf', array('file' => 'fpdf/fpdf.php'));
App::import('Vendor', 'fpdi', array('file' => 'fpdi/src/autoload.php'));

use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfParser\StreamReader;

class DefaultPagesController extends AppController {

	// beforeFilter() function for this Controller
	public function beforeFilter() {
		// Call the master beforeFilter() in AppController
	    parent::beforeFilter();

	    // Tell the auth component that all actions can be accessed without having to be logged in
		$this->Auth->allow('home', 'student', 'teacher', 'awaiting_approval', 'awaiting_validation', 'userAgreement', 
			'privacyPolicy', 'update_required', 'maintenance', 'report_error', 'unauthorized', 
			'student_create', 'student_validate', 'student_removed', 'js_required', 'upload_image', 'open_bank',
			'build_pdf', 'save_pdf');

        if ($this->action == 'index') {
            $user = $this->Auth->user();
            if (!empty($user['id'])) {
                if ($user['role'] == 'admin') {
                    $controller = false;
                    $action = false;

                    if (isset($_COOKIE[$user['id'] . '_lastController'])) {
                        $controller = $_COOKIE[$user['id'] . '_lastController'];
                        if (!in_array($controller, array('Documents', 'Assessments', 'UserQuestions'))) {
                            if (isset($_COOKIE[$user['id'] . '_lastAction'])) {
                                $action = $_COOKIE[$user['id'] . '_lastAction'];
                            } 
                        }
                    }
                    
                    if (($controller === false) || ($action == false)) {
                        $controller = 'Documents';
                        $action = 'index';
                    }
                    
                    $this->redirect(array('controller' => $controller, 'action' => $action));
                } else if ($user['role'] == 'teacher') {
                    $this->redirect(array('controller' => 'Documents', 'action' => 'index'));
                } else if ($user['role'] == 'school') {
                    $this->redirect(array('controller' => 'Groups', 'action' => 'index'));
                } else if ($user['role'] == 'student') {
                    $this->redirect(array('controller' => 'SittingStudents', 'action' => 'index'));
                }
            } else {
                $secrets = Configure::read('secrets');
                $this->redirect($secrets['login_domain'] . '/DefaultPages/home');
            }
        }

    }

	// isAuthorized function for this Controller
	public function isAuthorized($user) {
		if (parent::isAuthorized($user)) {
	        return true;
	    }	
    }
	
	// Unauthorized redirect
	public function unauthorized() {
		$this->Flash->error("Current user is not authorized to access the requested page.");
		$this->redirect_home();
	}

	// For index page (title_for_layout is the title of the page. You can see it in the layout_head element

	public function home() {
		$this->set('title_for_layout', 'Home');

		if (!empty($_COOKIE['login_role'])) {
			$role = $_COOKIE['login_role'];
			$this->Cookie->write('login_role', $role, false, '6 months');
			$this->redirect('/' . $role);
    	}
    }

	public function teacher() {
		$this->set('title_for_layout', 'Home');
    }

	public function student() {
		$this->set('title_for_layout', 'Home');
    }

	public function awaiting_approval() {
		$this->set('h1_title', 'Welcome');
		$this->set('title_for_layout', 'Awaiting Approval');

		if (!empty($this->Auth->user('id'))) {
			$this->set('user_email', $this->Auth->user('email'));
		} else if ($this->Session->check('awaiting_id')) {

			$this->loadModel('User');
			$this->User->contain();
			$user = $this->User->findById($this->Session->read('awaiting_id'));

			if (!empty($user)) {
				$this->set('user_email', $user['User']['email']);
			} else $this->redirect_home();
		} else $this->redirect_home();
	}

	public function awaiting_validation() {
		$this->set('h1_title', 'Welcome');
		$this->set('title_for_layout', 'Awaiting Email Validation');

		if (!empty($this->Auth->user('id'))) {

			$this->set('user_id', $this->Auth->user('id'));
			$this->set('user_email', $this->Auth->user('email'));

		} else if ($this->Session->check('awaiting_id')) {

			$this->loadModel('User');
			$this->User->contain();
			$user = $this->User->findById($this->Session->read('awaiting_id'));

			if (!empty($user)) {
				$this->set('user_id', $user['User']['id']);
				$this->set('user_email', $user['User']['email']);
			} else $this->redirect_home();
		} else $this->redirect_home();
	}

	// User agreement
	public function userAgreement() {
		$this->set('title_for_layout', 'User Agreement');
	}

	// Privacy policy
	public function privacyPolicy() {
		$this->set('title_for_layout', 'Privacy Policy');
	}

    // Open question bank discussion
	public function open_bank() {
		$this->set('title_for_layout', 'Sharing to the open question bank');

        $this->loadModel('Question');
        $numOpen = $this->Question->find('count', array(
			'conditions' => array('Question.open_status !=' => 0)
		));

        $this->set(compact('numOpen'));
    }

	// ES6 requirement page
	public function update_required() {
		$this->set('title_for_layout', 'Browser udpate required');
	}

	// Maintenance page
	public function maintenance() {
		$this->set('title_for_layout', 'SmarterMarks is undergoing maintenance');
	}

	// Student accounts
	public function student_create() {
		$this->set('title_for_layout', 'Student accounts on SmarterMarks');
	}

	public function student_validate() {
		$this->set('title_for_layout', 'Student accounts on SmarterMarks');
	}

	public function student_removed() {
		$this->set('title_for_layout', 'Sitting not available');
	}

	public function js_required() {
		$this->set('title_for_layout', 'JavaScript required');
	}

	// Error reporting
	public function report_error() {
		$this->autoRender = false;
		$this->layout = '';

		App::uses('Email', 'Lib');
		$user_email = empty($this->Auth->user('id')) ? null : $this->Auth->user('email');
		Email::queue_error("SmarterMarks: Client-side error", $user_email, array(
			'data' => $this->request->data
		));
	}

	public function save_pdf() {
        $this->autoRender = false;
        $this->layout = '';

        $success = true;
        $resp = array();

		$file = $this->request->data['pdf'];
		$pdfTempPath = $file['tmp_name'];
		$filename = $file['name'];
		
		$pdf_data = file_get_contents($pdfTempPath);

        $objectName = md5($pdf_data) . '/' . $filename;

        $secrets = Configure::read('secrets'); 

        App::uses('S3', 'Lib');

        $s3 = S3::getClient();
        $response = $s3->putObject(array(
            'Bucket' => $secrets['s3']['temp_bucket'], 
            'Key' => $objectName,
            'Body' => $pdf_data,
            'ContentType' => 'application/pdf'
        ));
        if ($response) {
            $resp['url'] = $secrets['s3']['temp_prefix'] . $objectName;
        } else $success = false;

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		echo json_encode($resp);
    }

	public function build_pdf() {
		$this->set('title_for_layout', 'Building PDF');

		$data = $this->request->data;
		$this->set(compact('data'));
	}

    public function upload_image() {
		$this->autoRender = false;
		$this->layout = '';
	
		$success = true;
		$resp = array();
	
		// Make sure we have an uploaded file
		if (empty($_FILES['imageFile']['tmp_name'])) {
			$resp['error'] = 'No imageFile in upload';
			echo json_encode($resp);
			return;
		}
	
		// Generate a random name to store temporarily
		$tempFilename = md5(uniqid(rand(), true)); 
	
		// Move from PHP tmp area to /webroot/uploads
		$webroot = Configure::read('approot') . 'webroot/';
		$uploadsPath = $webroot . 'uploads' . DS;
		chdir($uploadsPath);
	
		if (!move_uploaded_file($_FILES['imageFile']['tmp_name'], $tempFilename)) {
			$resp['error'] = 'Could not move uploaded file';
			echo json_encode($resp);
			return;
		}
	
		// Decide compression logic based on user role
		$compress = true; // default for non-teacher
		if (!empty($this->Auth->user('role')) && $this->Auth->user('role') == 'teacher') {
			$compress = false;
		}
	
		// Check if the file is already JPEG or PNG; if not, we convert to JPG
		$imageType = @exif_imagetype($tempFilename);
		$convert  = !in_array($imageType, [IMAGETYPE_PNG, IMAGETYPE_JPEG]);
	
		if ($compress || $convert) {
			$target = $tempFilename . '.jpg';
			$quality = $compress ? '50' : '90';
			// Use ImageMagick to convert/compress
			exec('magick convert -quality ' . $quality . ' ' . $tempFilename . ' ' . $target);
	
			if (file_exists($target)) {
				unlink($tempFilename);  // remove old
				$tempFilename = $target; // new name
			}
		}
	
		// Now rename to MD5 hash
		$finalHash = md5_file($tempFilename);
		rename($tempFilename, $finalHash);
	
		// Upload final hashed file to userdata bucket
		App::uses('S3', 'Lib');
		$secrets = Configure::read('secrets');

		if (!S3::put($secrets['s3']['userdata_bucket'], $finalHash)) {
			$resp['error'] = 'S3 upload failed';
			echo json_encode($resp);
			return;
		}
	
		// Remove local copy and send response
		unlink($finalHash);
	
		$resp['success'] = $success;
		$resp['imageURL'] = $secrets['s3']['userdata_prefix'] . $finalHash;
		$resp['imageHash'] = $finalHash;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		echo json_encode($resp);
	}

}

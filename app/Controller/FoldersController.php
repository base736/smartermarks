<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class FoldersController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
		}
		
	    if (in_array($this->action, array('save'))) {
		    if (empty($this->request->data['id'])) {
			    
// Creating a new folder
			    
			    if (!empty($this->request->data['parent_id'])) {
				    $parent_id = $this->request->data['parent_id'];
					$permissions = $this->Folder->getContentPermissions($parent_id, $this->Auth->user('id'));
					return in_array('can_add', $permissions['all']);
				} else return false;
			} else {
				$folder_id = $this->request->data['id'];
				$permissions = $this->Folder->getPermissions($folder_id, $this->Auth->user('id'));
				return in_array('can_edit', $permissions);
			}
		}

	    if (in_array($this->action, array('move', 'copy'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$folder_id = $this->request->params['pass'][0];
				$folderPermissions = $this->Folder->getPermissions($folder_id, $this->Auth->user('id'));

				if (isset($this->request->data['target_id']) && is_numeric($this->request->data['target_id'])) {
					$target_id = $this->request->data['target_id'];
					$targetPermissions = $this->Folder->getContentPermissions($target_id, $this->Auth->user('id'));

					$old_community_id = $this->Folder->getCommunityId($folder_id);

					$allowed = in_array('can_add', $targetPermissions['all']);
					if ($old_community_id != $this->Folder->getCommunityId($target_id)) {
						$allowed &= in_array('can_export', $folderPermissions);
					}
					if ($this->action == 'move') {
						$this->Folder->contain();
						$folder = $this->Folder->findById($folder_id);
						if (!empty($folder)) {
							if ($folder['Folder']['parent_id'] != null) {
								$allowed &= in_array('can_delete', $folderPermissions);
							}
						} else $allowed = false;
					}
					return $allowed;
				} else return false;
			} else return false;
		}

		if (in_array($this->action, array('delete'))) {
			$folder_id = $this->request->params['pass'][0];
			$permissions = $this->Folder->getPermissions($folder_id, $this->Auth->user('id'));
			return in_array('can_delete', $permissions);
		}
		
		return false;
	}

	public function delete($folder_id) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$this->loadModel('User');
		$user = $this->User->findById($this->Auth->user('id'));
		$folder_ids = json_decode($user['User']['folder_ids'], true);

		$trash_folders = array();
		foreach ($folder_ids as $type => $entry) {
			if (isset($entry['trash'])) $trash_folders[$type] = $entry['trash'];
		}

		$success &= $this->Folder->deleteEngine($folder_id, $trash_folders);

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}	

	public function save() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

		$folder_data = array();
		if (!empty($this->request->data['id'])) {

			$folder_data['id'] = $this->request->data['id'];
			$folder_data['name'] = $this->request->data['name'];

		} else {

			$this->Folder->contain();
			$parent = $this->Folder->findById($this->request->data['parent_id']);

			$folder_data['name'] = $this->request->data['name'];
			$folder_data['user_id'] = $this->Auth->user('id');
			$folder_data['parent_id'] = $this->request->data['parent_id'];
			$folder_data['community_id'] = $parent['Folder']['community_id'];
			$folder_data['type'] = $parent['Folder']['type'];

			$this->Folder->create();

		}
					
		$success &= !empty($this->Folder->save($folder_data));

		if (!empty($folder_data['id'])) {			
			$this->Folder->contain();
			$folder_id = $folder_data['id'];
			$folder = $this->Folder->findById($folder_id);
			
			if ($folder['Folder']['type'] == 'questions') {

				// Get a list of questions in this folder and subfolders, and queue an update to the search index

				$search_folders = array();
				$edge_folders = array($folder_id);
				do {
					$search_folders = array_merge($search_folders, $edge_folders);
					$edge_folders = array_values($this->Folder->find('list', array(
						'fields' => 'Folder.id',
						'conditions' => array('Folder.parent_id' => $edge_folders)
					)));
				} while (count($edge_folders) > 0);

				$this->loadModel('UserQuestion');
				$this->UserQuestion->contain(array('QuestionHandle'));
				$question_ids = $this->UserQuestion->find('list', array(
					'fields' => array('QuestionHandle.question_id'),
					'conditions' => array('UserQuestion.folder_id' => $search_folders)
				));

				$this->loadModel('Question');
                $db = $this->Question->getDataSource();
                $this->Question->updateAll(
                    array('needs_index' => $db->value(date('Y-m-d H:i:s'), 'string')),
                    array('Question.id' => $question_ids)
                );
			}
		}

	   	$resp = array();
        $resp['data'] = $folder_data;
        $resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function copy($folder_id) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$parent_id = $this->request->data['target_id'];
		$copyType = $this->request->data['copy_type'];

		$copyParent = ($copyType == 'folder');

		$result = $this->Folder->copyToFolder($folder_id, $parent_id, $this->Auth->user('id'), $copyParent);
		$success &= $result['success'];

		$resp['success'] = $success;
		$resp['folder_id'] = $result['folder_id'];
		$resp['target_id'] = $parent_id;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function move($folder_id) {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$parent_id = $this->request->data['target_id'];

		if (!empty($parent_id) && !empty($folder_id)) {
			$this->Folder->contain();
			$folder = $this->Folder->findById($folder_id);

			if ($folder['Folder']['parent_id'] == null) {
				$this->loadModel('Community');
				$this->Community->contain(array('CommunityUser'));
				$communities = $this->Community->find('all', array(
					'conditions' => array(
						'OR' => array(
							array('Community.folder_ids LIKE' => '%"forms":"' . $folder_id . '"%'),
							array('Community.folder_ids LIKE' => '%"sittings":"' . $folder_id . '"%'),
							array('Community.folder_ids LIKE' => '%"assessments":"' . $folder_id . '"%'),
							array('Community.folder_ids LIKE' => '%"questions":"' . $folder_id . '"%')
						)
					),
					'contain' => array(
						'CommunityUser' => array(
				            'conditions' => array(
					            'CommunityUser.user_id' => $this->Auth->user('id')
					        )
						)
					)
				));
				if ((count($communities) == 1) && (count($communities[0]['CommunityUser']) == 1)) {
					$data = array();
					$data['id'] = $communities[0]['CommunityUser'][0]['id'];
					$data['folder_id'] = $parent_id;
					$this->loadModel('CommunityUser');
					$this->CommunityUser->save($data);
				} else $success = false;
			} else {
				$data = array();
				$data['id'] = $folder_id;
				$data['parent_id'] = $parent_id;
				$success &= !empty($this->Folder->save($data));
				
				$this->Folder->contain();
				$parent = $this->Folder->findById($parent_id);
				
				if (($parent['Folder']['user_id'] != $folder['Folder']['user_id']) || 
					($parent['Folder']['community_id'] != $folder['Folder']['community_id'])) {

					// Get a list of subfolders

					$search_folders = array();
					$edge_folders = array($folder_id);
					do {
						$search_folders = array_merge($search_folders, $edge_folders);
						$edge_folders = array_values($this->Folder->find('list', array(
							'fields' => 'Folder.id',
							'conditions' => array('Folder.parent_id' => $edge_folders)
						)));
					} while (count($edge_folders) > 0);
					

					$data = array();
					$data['user_id'] = $parent['Folder']['user_id'];
					$data['community_id'] = $parent['Folder']['community_id'];					
					foreach ($search_folders as $folder_id) {
						$data['id'] = $folder_id;
						$this->Folder->save($data);
					}

					$this->loadModel('UserQuestion');					
					$userQuestion_ids = $this->UserQuestion->find('list', array(
						'fields' => 'UserQuestion.id',
						'conditions' => array('UserQuestion.folder_id' => $search_folders)
					));
					foreach ($userQuestion_ids as $userQuestion_id) {
						$this->UserQuestion->fixHandle($userQuestion_id);
					}
					
					$this->loadModel('Assessment');
					$assessment_ids = $this->Assessment->find('list', array(
						'fields' => 'Assessment.id',
						'conditions' => array('Assessment.folder_id' => $search_folders)
					));
					foreach ($assessment_ids as $assessment_id) {
						$this->Assessment->fixHandles($assessment_id);
					}					
				}
			}
			
			// Queue an update to the search index
			
			if ($folder['Folder']['type'] == 'questions') {
				$this->loadModel('UserQuestion');
				$this->UserQuestion->contain(array('QuestionHandle'));
				$question_ids = $this->UserQuestion->find('list', array(
					'fields' => array('QuestionHandle.question_id'),
					'conditions' => array('UserQuestion.folder_id' => $search_folders)
				));
					
				$this->loadModel('Question');
                $db = $this->Question->getDataSource();
                $this->Question->updateAll(
                    array('needs_index' => $db->value(date('Y-m-d H:i:s'), 'string')),
                    array('Question.id' => $question_ids)
                );
			}
		} else $success = false;
		
        $resp['success'] = $success;
		
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
}


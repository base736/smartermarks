<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class SittingStudentsController extends AppController {

    public $presetVars = true; // using the model configuration

	public function beforeFilter() {
	    parent::beforeFilter();

		$isAllowed = false;

		if (in_array($this->action, array('log_event', 'save_data', 'save_annotations'))) {

			if (!empty($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$sittingStudent_id = $this->request->params['pass'][0];
			} else unset($sittingStudent_id);

			if (!empty($this->request->data['response_online_id']) && is_numeric($this->request->data['response_online_id'])) {
				$responseOnline_id = $this->request->data['response_online_id'];
			} else unset($responseOnline_id);

			if (!empty($sittingStudent_id) && !empty($responseOnline_id)) {
				$this->loadModel('ResponseOnline');
				$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);

				if ($attempt_cache !== false) {
					if ($attempt_cache['sitting_student_id'] != $sittingStudent_id) {
						$isAllowed = false;
					} else if ($this->Cookie->check('student_token_' . $attempt_cache['sitting_id'])) {
						$stored_token = $attempt_cache['token'];
						$cookie_token = $this->Cookie->read('student_token_' . $attempt_cache['sitting_id']);
						$isAllowed = !empty($stored_token) && ($stored_token == $cookie_token);
					}
				} else if ($this->action == 'save_data') {

					// Let save_data through so we can fail more gracefully if a sitting is deleted or
					// a student removed.

					$isAllowed = true;
				}
			}
		}

	    if (in_array($this->action, array('lobby', 'view_open', 'view_closed', 'get_open', 'get_closed', 'create_attempt',
	    	'save_details', 'close', 'get_attempt'))) {

			if (!empty($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$sittingStudent_id = $this->request->params['pass'][0];
				
				$this->SittingStudent->id = $sittingStudent_id;
				$sittingID = $this->SittingStudent->field('sitting_id');
				if ($sittingID !== false) {
					if ($this->Cookie->check('student_token_' . $sittingID)) {
						$stored_token = $this->SittingStudent->field('token');
						$cookie_token = $this->Cookie->read('student_token_' . $sittingID);
						$isAllowed = !empty($stored_token) && ($stored_token == $cookie_token);
					}
				}

				if ($isAllowed && ($this->action == 'get_attempt')) {
					if (!empty($this->request->query['response_online_id']) && is_numeric($this->request->query['response_online_id'])) {
						$responseOnline_id = $this->request->query['response_online_id'];

						$this->loadModel('ResponseOnline');
						$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);

						if (empty($attempt_cache['sitting_student_id']) || ($attempt_cache['sitting_student_id'] != $sittingStudent_id)) {
							$isAllowed = false;
						} else if (!empty($attempt_cache) && ($this->action == 'get_attempt')) {
							$closedTime = $attempt_cache['closed'];
							$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);
							if ($timeRemaining > 0) $isAllowed = false;
						}
					} else $isAllowed = false;
				}
			}
		}
	    
		if ($isAllowed) $this->Auth->allow($this->action);
	}

	public function isAuthorized($user) {

		if (parent::isAuthorized($user)) {
	        return true;
		}

		$isStudent = ($this->Auth->user('id') == null) || ($this->Auth->user('role') == 'student');
	    if (in_array($this->action, array('get_overview', 'save_comment', 'delete_comment', 'delete_attempt'))) {
			if ($isStudent) return false;
		}

		if (in_array($this->action, array('index', 'empty_trash'))) {
			return !empty($this->Auth->user('id'));
		}

		if (in_array($this->action, array('get_index'))) {
			if (!empty($this->request->query['folder_ids'])) {
				$this->loadModel('Folder');
				$folder_ids = explode(',', $this->request->query['folder_ids']);
				foreach ($folder_ids as $folder_id) {
					if (is_numeric($folder_id)) {
						$permissions = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
						return in_array('can_view', $permissions['all']);
					} else return false;
				}	
			} else return false;
		}

		if (in_array($this->action, array('lobby', 'view_open', 'view_closed', 'get_open', 'get_closed', 'create_attempt',
	    	'save_details', 'close', 'delete_attempt', 'save_comment', 'delete_comment', 'save_data', 
			'log_event', 'save_annotations', 'update_email'))) {
			if (!empty($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$sittingStudent_id = $this->request->params['pass'][0];

				if (!empty($this->request->data['response_online_id']) && is_numeric($this->request->data['response_online_id'])) {
					$this->loadModel('ResponseOnline');
					$attempt_cache = $this->ResponseOnline->getCache($this->request->data['response_online_id']);
					if (empty($attempt_cache['sitting_student_id']) || ($attempt_cache['sitting_student_id'] != $sittingStudent_id)) {
						return false;
					}
				} else if (in_array($this->action, array('save_data', 'log_event', 'save_annotations', 'delete_attempt', 
					'save_comment', 'delete_comment'))) {
					return false;
				}

				$itemPermissions = $this->SittingStudent->getPermissions($sittingStudent_id, $this->Auth->user('id'));
				if (in_array($this->action, array('save_comment', 'delete_comment', 'delete_attempt'))) {
					return in_array('can_edit', $itemPermissions);
				} else return in_array('can_view', $itemPermissions);
			} else return false;
	    }

		if (in_array($this->action, array('get_attempt'))) {
			if (!empty($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$sittingStudent_id = $this->request->params['pass'][0];

				if (!empty($this->request->query['response_online_id']) && is_numeric($this->request->query['response_online_id'])) {
					$response_online_id = $this->request->query['response_online_id'];

					$this->loadModel('ResponseOnline');
					$attempt_cache = $this->ResponseOnline->getCache($response_online_id);
					if (empty($attempt_cache['sitting_student_id']) || ($attempt_cache['sitting_student_id'] != $sittingStudent_id)) {
						return false;
					}

					if ($isStudent) {
						$this->loadModel('ResponseOnline');
						$attempt_cache = $this->ResponseOnline->getCache($response_online_id);
						$closedTime = $attempt_cache['closed'];
	
						$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);
						if ($timeRemaining > 0) return false;
					}
			
					$itemPermissions = $this->SittingStudent->getPermissions($sittingStudent_id, $this->Auth->user('id'));
					return in_array('can_view', $itemPermissions);

				} else return false;
			} else return false;
	    }

		if (in_array($this->action, array('move'))) {
			if (!empty($this->request->data['item_ids'])) {
				foreach ($this->request->data['item_ids'] as $sittingStudent_id) {
					$itemPermissions = $this->SittingStudent->getPermissions($sittingStudent_id, 
						$this->Auth->user('id'));
	
					if ($this->action ==  'move') {
						if (!empty($this->request->data['folder_id'])) {
							$this->loadModel('Folder');
							
							$targetFolderId = $this->request->data['folder_id'];
							$targetPermissions = $this->Folder->getContentPermissions($targetFolderId, 
								$this->Auth->user('id'));
	
							if (!in_array('can_add', $targetPermissions['all'])) return false;
							if (!in_array('can_delete', $itemPermissions)) return false;
						} else return false;
					} else return false;
				}
				return true;
			} else return false;
		}

		if (in_array($this->action, array('start', 'get_overview'))) {
			if (!empty($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$sittingStudent_id = $this->request->params['pass'][0];
				$itemPermissions = $this->SittingStudent->getPermissions($sittingStudent_id, $this->Auth->user('id'));
				return in_array('can_view', $itemPermissions);
			} else return false;
		}

		if (in_array($this->action, array('open'))) {
			if (!empty($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$sittingStudent_id = $this->request->params['pass'][0];
				$this->SittingStudent->contain();
				$sittingStudent = $this->SittingStudent->findById($sittingStudent_id);
				if ($sittingStudent) $sitting_id = $sittingStudent['SittingStudent']['sitting_id'];

				$this->loadModel('Sitting');
				$permissions = $this->Sitting->getPermissions($sitting_id, $this->Auth->user('id'));
				return in_array('can_edit', $permissions);
			} else return false;
		}
				
		return false;
	}
	
	private function filterSitting(&$pSitting) {
		unset($pSitting['Sitting']['json_data']['Access']['start']['code']);
		unset($pSitting['Sitting']['json_data']['Access']['token_details']['fieldValues']);
		
		$this->loadModel('ResponseOnline');

		$previousClosed = null;

		$responsesOpen = 0;
		$responsesClosed = 0;
		foreach ($pSitting['ResponseOnline'] as $response) {
			if (!empty($response['closed'])) {
				$thisTimestamp = strtotime($response['closed']);
				if (empty($bestTimestamp) || ($thisTimestamp > $bestTimestamp)) {
					$bestTimestamp = $thisTimestamp;
					$previousClosed = $response['closed'];
				}
			}
			$timeRemaining = $this->ResponseOnline->getTimeRemaining($response['closed']);
			if ($timeRemaining <= 0) $responsesClosed++;
			else $responsesOpen++;
		}
		
		$responsesTotal = $responsesOpen + $responsesClosed;
		if (isset($pSitting['Sitting']['closed'])) $pSitting['closed'] = $pSitting['Sitting']['closed'];
		if ($responsesTotal == $pSitting['Sitting']['json_data']['Access']['attempt_limit']) {
			if ($previousClosed != null) $pSitting['closed'] = $previousClosed;
		}

		if (!empty($pSitting['closed'])) {
			$nowTimestamp = time();
			$sittingEndTimestamp = strtotime($pSitting['closed']);
			$pSitting['status'] = ($sittingEndTimestamp > $nowTimestamp) ? 'open' : 'closed';
		} else $pSitting['status'] = 'open';

		$pSitting['responses_open'] = $responsesOpen;
		$pSitting['responses_closed'] = $responsesClosed;

		$keptRows = array('name', 'json_data');
		foreach ($pSitting['Sitting'] as $key => $value) {
			if (!in_array($key, $keptRows)) unset($pSitting['Sitting'][$key]);
		}
		unset($pSitting['ResponseOnline']);
	}

	// Index interface

	public function index() {
		$adminView = false;

		$this->loadModel('Folder');
		$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'sittingstudents');
		$tree = $this->Folder->getTree($nodes);
		$jsonTree = json_encode($tree);

		// Set variables for template
		
		$this->set(compact('jsonTree', 'adminView'));

		$this->set('colCreated', 'SittingStudent.moved');
		$this->set('colName', 'Sitting.name');
		$this->set('itemType', 'SittingStudent');
		$this->set('index_element', '../SittingStudents/index');

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		$this->viewPath = 'Elements';
		$this->render('index_common');
	}
	
	public function get_index() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$query_params = array(
			'contain' => 'Sitting',
			'joins' => array(
				array(
					'table' => 'folders',
					'alias' => 'SittingFolder',
					'type' => 'LEFT',
					'conditions' => array(
						'Sitting.folder_id = SittingFolder.id'
					)
				)
			),
			'conditions' => array(
				'OR' => array(
					'SittingFolder.parent_id IS NOT NULL',
					'SittingFolder.name !=' => 'Trash'
				),
				'Sitting.deleted' => null,
				'SittingStudent.deleted' => null
			)
		);

		$userDefaults = json_decode($this->Auth->user('defaults'), true);

		if (isset($this->request->query['sort'])) {
			$sortBy = $this->request->query['sort'];
		} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$sortBy = 'Sitting.name';
		} else {
			$sortBy = 'SittingStudent.moved';
		}

		if (isset($this->request->query['direction'])) {
			$direction = $this->request->query['direction'];
		} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$direction = 'asc';
		} else {
			$direction = 'desc';
		}

		$query_params['order'] = array($sortBy => $direction);

		if (isset($this->request->query['folder_ids'])) {

			$folder_ids = explode(',', $this->request->query['folder_ids']);
			$query_params['conditions']['SittingStudent.folder_id'] = $folder_ids;

            $this->SittingStudent->contain();
            $count = $this->SittingStudent->find('count', $query_params);
            $resp['count_total'] = $count;
            $resp['count_matching'] = $count;
    
		} else if ($this->Auth->user('role') == 'admin') {

			$query_params['conditions'][] = ['SittingStudent.folder_id IS NOT NULL'];
            $query_params['contain'] = array('User', 'Sitting');
            
		} else $success = false;
		
		if (isset($this->request->query['search'])) {
			$query_params['conditions']['Sitting.name LIKE'] = '%' . $this->request->query['search'] . '%';

			$this->SittingStudent->contain();
			$count = $this->SittingStudent->find('count', $query_params);
            $resp['count_matching'] = $count;
		}

		if (isset($this->request->query['limit'])) {
			if (is_numeric($this->request->query['limit']) && ($this->request->query['limit'] > 0)) {
				$query_params['limit'] = intval($this->request->query['limit']);
			} else if ($this->request->query['limit'] !== 'all') {
                $success = false;
            }
		} else $query_params['limit'] = 10;

		if (isset($this->request->query['page'])) {
			if (is_numeric($this->request->query['page']) && ($this->request->query['page'] > 0)) {
				$query_params['page'] = intval($this->request->query['page']);
			} else $success = false;
		} else $query_params['page'] = 1;

		if ($success) {
			$this->SittingStudent->contain();
			$resp['results'] = $this->SittingStudent->find('all', $query_params);

			$this->loadModel('ResponseOnline');
			foreach ($resp['results'] as &$pEntry) {

				// Unpack sitting data

				$pEntry['Sitting']['json_data'] = json_decode($pEntry['Sitting']['json_data'], true);

				// Add cached attempts
	
				$pEntry['ResponseOnline'] = array();
				$sittingStudent_cache = $this->SittingStudent->getCache($pEntry['SittingStudent']['id']);
				foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
					$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
					$pEntry['ResponseOnline'][] = $attempt_cache;
				}
	
				// Filter entry
	
				$this->filterSitting($pEntry);
			}

			// Update "is_new" for items

			if ($this->Auth->user('role') != 'admin') {
				$visible = array();
				foreach ($resp['results'] as $result) {
					$visible[] = $result['SittingStudent']['id'];
				}
				$this->SittingStudent->updateAll(
					array('SittingStudent.is_new' => 0),
					array('SittingStudent.id' => $visible)
				);
				$this->SittingStudent->updateAll(
					array('SittingStudent.is_new' => 2),
					array(
						'SittingStudent.user_id' => $this->Auth->user('id'),
						'SittingStudent.is_new' => 3
					)
				);
			}
		}

		$resp['page'] = $query_params['page'];
        if (array_key_exists('limit', $query_params)) {
            $resp['limit'] = $query_params['limit'];
        }
		$resp['success'] = $success;

		echo json_encode($resp);
	}

	public function move() {
		$this->autoRender = false;
		$this->layout = '';
		
		$folder_id = $this->request->data['folder_id'];
		$item_ids = $this->request->data['item_ids'];

		$this->loadModel("SittingStudent");

		$success = true;
		foreach ($item_ids as $sittingStudent_id) {
			$this->SittingStudent->contain();
			$sitingStudent = $this->SittingStudent->findById($sittingStudent_id);
			if (!empty($sitingStudent)) {

				$data = array(
					'id' => $sittingStudent_id,
					'folder_id' => $folder_id,
					'moved' => date('Y-m-d H:i:s'),
					'is_new' => 2
				);

				$success &= !empty($this->SittingStudent->save($data));
							
			} else $success = false;
		}

		echo json_encode(
			array(
			    'success' => $success,
			    'folder_id' => $folder_id,
				'count' => count($item_ids)
			)
		);
	}

	public function empty_trash() {
		$this->autoRender = false;
		$this->layout = '';
				
		$success = true;

		$this->loadModel('Folder');
		$folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'sittingstudents', 'trash');

		$this->SittingStudent->contain();
		$trashSittings = $this->SittingStudent->find('list', array(
			'fields' => array('SittingStudent.id'),
			'conditions' => array('SittingStudent.folder_id' => $folder_id)
		));
		
		if (count($trashSittings) > 0) {
			$db = $this->SittingStudent->getDataSource();
			$this->SittingStudent->updateAll(
				array('SittingStudent.deleted' => $db->value(date('Y-m-d H:i:s'), 'string')),
				array('SittingStudent.id' => $trashSittings)
			);
		}

		echo json_encode(
			array(
		   		'success' => $success
		   	)
		);
	}
	
	public function start($sittingStudent_id) {
		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
		
		$token = md5(uniqid('', true));

		$data = array();		
		$data['id'] = $sittingStudent_id;
		$data['token'] = $token;
		$this->SittingStudent->save($data);

		$sittingID = $sittingStudent_cache['SittingStudent']['sitting_id'];
	    $this->Cookie->write('student_token_' . $sittingID, $token, false, '1 month');

		$this->loadModel('ResponseOnline');

		$responsesOpen = 0;
		foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
			$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
			$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
			if ($timeRemaining > 0) {
				$this->redirect(array('controller' => 'SittingStudents', 'action' => 'view_open', $sittingStudent_id));
			}
		}
		$this->redirect(array('controller' => 'SittingStudents', 'action' => 'lobby', $sittingStudent_id));
	}
	
	public function lobby($sittingStudent_id) {
		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);

		$this->loadModel('Sitting');
		$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);
		$json_data = $sitting_cache['Sitting']['json_data'];	
		
		if ($sitting_cache['Sitting']['closed'] != null) {
			$nowTimestamp = time();
			$sittingEndTimestamp = strtotime($sitting_cache['Sitting']['closed']);
			if ($sittingEndTimestamp <= $nowTimestamp) {
				$this->Flash->error("Sitting has been closed.");			
				$this->redirect_home();
			}
		}

		$assessment_data = $sitting_cache['Assessment'];
		if (!empty($assessment_data['Title']['nameLines'])) {
			$dataLines = $assessment_data['Title']['nameLines'];
		} else $dataLines = array();

		if (isset($json_data['Access']['token_details'])) {
			if (!in_array($json_data['Access']['token_details']['fieldName'], $dataLines)) {
				$dataLines[] = $json_data['Access']['token_details']['fieldName'];
			}
		}

		if ($json_data['Access']['start']['type'] == 'code') {
			$dataLines[] = 'Start code';
		}

		$dataLines = array_unique($dataLines);

		$details = $sittingStudent_cache['SittingStudent']['details'];

		if (!empty($json_data['Access']['token_details'])) {
			$join_field = $json_data['Access']['token_details']['fieldName'];
			unset($details[$json_data['Access']['token_details']['fieldName']]);
		} else $join_field = false;

		if ($json_data['Access']['start']['type'] == 'date') {
			$startTimestamp = strtotime($json_data['Access']['start']['date']);
			$nowTimeStamp = time();
			$waitTime = $startTimestamp - $nowTimeStamp;
			if ($waitTime < 0) $waitTime = 0;
		} else $waitTime = 0;	

		// Add sitting data and cached attempts

		$sittingStudent_cache['Sitting'] = $sitting_cache['Sitting'];

		$responseOnline_ids = $sittingStudent_cache['ResponseOnline'];
		$sittingStudent_cache['ResponseOnline'] = array();

		$this->loadModel('ResponseOnline');
		foreach ($responseOnline_ids as $attempt_id) {
			$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
			$sittingStudent_cache['ResponseOnline'][] = $attempt_cache;
		}

		// Filter sitting so we don't send anything we shouldn't

		$this->filterSitting($sittingStudent_cache);
	
		$this->set(compact('sittingStudent_cache', 'waitTime', 'dataLines', 'details', 'join_field'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}

	public function view_open($sittingStudent_id) {
		$permissions = $this->SittingStudent->getPermissions($sittingStudent_id, $this->Auth->user('id'));
		$canEdit = in_array('can_edit', $permissions) ? 1 : 0;

		if (!$this->Cookie->check('cookie_id')) {
			$cookie_id = md5(uniqid(session_id(), true));
			$this->Cookie->write('cookie_id', $cookie_id, false, '6 months');
		}

		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
		
		if ($sittingStudent_cache['SittingStudent']['started'] == null) {
			$data = array();
			$data['id'] = $sittingStudent_id;
			$data['started'] = date('Y-m-d H:i:s');
			$this->SittingStudent->save($data);
		}

		$this->set(compact('sittingStudent_id', 'canEdit'));

		$this->loadModel('Sitting');
		$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);
		$json_data = $sitting_cache['Sitting']['json_data'];
		
		$presentation = $json_data['View']['presentation'];
		$this->set(compact('presentation'));
		
		if (!empty($json_data['View']['warnings'])) {
			$warnings = json_encode($json_data['View']['warnings']);
		} else $warnings = "[]";
		$this->set(compact('warnings'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}
	
	public function view_closed($sittingStudent_id) {	
		$this->set(compact('sittingStudent_id'));

		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);

		$this->loadModel('Sitting');
		$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);

		$json_data = $sitting_cache['Sitting']['json_data'];

		if ($json_data['Access']['attempt_limit'] != 'none') {
			$numAttempts = $json_data['Access']['attempt_limit'];

			$this->loadModel('ResponseOnline');

			$responsesOpen = 0;
			$responsesClosed = 0;
			foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
				$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
				$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
				if ($timeRemaining <= 0) $responsesClosed++;
				else $responsesOpen++;
			}
	
			if ($numAttempts - $responsesOpen - $responsesClosed > 0) $canStart = 1;
			else $canStart = 0;
		} else $canStart = 1;
		
		$this->set(compact('canStart'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}
	
	public function get_closed($sittingStudent_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();

	   	$success = true;
		$message = false;

		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);

		if (!$sittingStudent_cache['SittingStudent']['can_open']) {

			$message = "Assessment can not be opened yet.";
			$success = false;

		} else {
			$this->loadModel('Sitting');
			$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);

			$json_data = $sitting_cache['Sitting']['json_data'];

			$resp['Sitting'] = array(
				'name' => $sitting_cache['Sitting']['name'],
				'View' => $sitting_cache['Sitting']['json_data']['View']
			);
			$resp['details'] = $sittingStudent_cache['SittingStudent']['details'];
			
			if ($sitting_cache['Sitting']['closed'] != null) {
				$nowTimestamp = time();
				$sittingEndTimestamp = strtotime($sitting_cache['Sitting']['closed']);
				$sitting_closed = ($sittingEndTimestamp <= $nowTimestamp);
			} else $sitting_closed = false;

			foreach (array('questions', 'scoring', 'outcomes', 'answers') as $key) {
				$resp['show_' . $key] = false;
			}
			foreach ($json_data['Results'] as $entry) {
				$key = $entry['type'];
				$showItem = $this->get_status($entry, $sitting_closed);
				$resp['show_' . $key] = $showItem;
			}
			
			$this->SittingStudent->cleanResponses($sittingStudent_id);
			
			// Review assessment results

			$this->loadModel('ResponseOnline');

			$resp['closed_responses'] = array();
			foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
				$attempt_cache = $this->ResponseOnline->getCache($attempt_id);

				$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
				if ($timeRemaining <= 0) {
					$resp['closed_responses'][] = array(
						'id' => $attempt_id,
						'created' => $attempt_cache['created']
					);
				}
			}			
		}
		
		$resp['success'] = $success;
		if ($message !== false) $resp['message'] = $message;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function get_open($sittingStudent_id = null) {

		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();

	   	$success = true;
		$message = false;

		$this->loadModel('Sitting');
		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
		$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);

		$this->SittingStudent->cleanResponses($sittingStudent_id);

		$this->loadModel('ResponseOnline');
		if (!empty($sittingStudent_cache['ResponseOnline'])) {
			$lastAttempt_id = array_pop($sittingStudent_cache['ResponseOnline']);
			$attempt_cache = $this->ResponseOnline->getCache($lastAttempt_id);
		} else $attempt_cache = null;

		$hasOpen = true;
		if (empty($attempt_cache)) $hasOpen = false;
		else {
			$closedTime = $attempt_cache['closed'];
			$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);
			if ($timeRemaining <= 0) $hasOpen = false;
		}

		if ($hasOpen) {

			// Continue working on an existing attempt

			$resp['response_online_id'] = $attempt_cache['id'];

			$resp['Sitting'] = array(
				'name' => $sitting_cache['Sitting']['name'],
				'View' => $sitting_cache['Sitting']['json_data']['View']
			);
			$resp['StudentDetails'] = $sittingStudent_cache['SittingStudent']['details'];

			$this->loadModel('VersionData');
			$version_data = $this->VersionData->findById($attempt_cache['version_data_id']);

			if (!empty($version_data['VersionData']['html'])) {
				$html = $version_data['VersionData']['html'];
			} else $html = gzdecode($version_data['VersionData']['html_gzip']);

			$resp['Version'] = array(
				'html' => $html,
				'settings' => $sitting_cache['Assessment']['Settings']
			);
	
			if (!empty($attempt_cache['responses'])) {
				$responses = json_decode($attempt_cache['responses'], true);

				// Remove comments

				foreach ($responses as &$pResponse) {
					unset($pResponse['comment']);
				}

				// Pass along the rest

				$resp['responses'] = $responses;
			}

			if (!empty($attempt_cache['annotations'])) {
				$annotations = json_decode($attempt_cache['annotations'], true);
				$resp['annotations'] = $annotations;
			}

			if (!empty($attempt_cache['events'])) {
				$events = json_decode($attempt_cache['events'], true);
				$maxIndex = 0;
				if (!empty($events['events'])) {
					foreach ($events['events'] as $event) {
						if (!empty($event['index']) && ($event['index'] > $maxIndex)) {
							$maxIndex = $event['index'];
						}
					}
				}
				$resp['max_index'] = $maxIndex;
			}

			if ($timeRemaining < 0) $resp['time_remaining'] = 0.0;
			else if ($timeRemaining < INF) $resp['time_remaining'] = $timeRemaining;

		} else {

			// Get set for a new attempt if possible

			$availability = $this->SittingStudent->getAvailability($sittingStudent_id);
			$resp['status'] = $availability['status'];

			if ($availability['status'] == 'closed') {

				$message = "This assessment has been closed.";
				$success = false;

			} else if ($availability['status'] == 'early') {

				$message = "This assessment is not yet open.";
				$success = false;

			} else if ($availability['status'] == 'complete') {

				$message = "No more attempts possible for this assessment.";
				$success = false;

			} else if (($availability['status'] != 'open') || (!$sittingStudent_cache['SittingStudent']['can_open'])) {

				$resp['status'] = 'locked';
				$message = "This assessment can not be opened yet.";
				$success = false;

			} else {

				$resp['Sitting'] = array(
					'name' => $sitting_cache['Sitting']['name'],
					'View' => $sitting_cache['Sitting']['json_data']['View']
				);
				$resp['StudentDetails'] = $sittingStudent_cache['SittingStudent']['details'];
	
				$shuffle_attempts = $sitting_cache['Sitting']['json_data']['View']['shuffle_attempts'];
				if ($shuffle_attempts) {

					// Send the data needed to create a new assessment version

					$resp['Assessment'] = $sitting_cache['Assessment'];
					$resp['Questions'] = $sitting_cache['Questions'];

				} else {

					// Create a new attempt using the Sitting's assessment version

					$version_data_id = $sitting_cache['Sitting']['version_data_id'];

					$this->loadModel('VersionData');

					$this->VersionData->contain();
					$version_data = $this->VersionData->findById($version_data_id);
					if (!empty($version_data)) {
						$this->loadModel('ResultData');

						$layout = json_decode($version_data['VersionData']['scoring_json'], true);

						$data = array();
						$data['result_set_id'] = $sitting_cache['Sitting']['result_set_id'];
						$data['version_data_id'] = $version_data_id;
						$data['results'] = json_encode($this->ResultData->emptyResults($layout));
						$this->ResultData->create();
						$this->ResultData->save($data);	

						$this->loadModel('ResponseOnline');
				
						$startTime = gmdate("Y-m-d H:i:s");
						$closedTime = $this->ResponseOnline->getClosed($startTime, $sitting_cache['Sitting']['json_data']);

						$data = array();
						$data['sitting_student_id'] = $sittingStudent_id;
						$data['version_data_id'] = $version_data_id;
						$data['result_data_id'] = $this->ResultData->id;
						$data['closed'] = $closedTime;
						$data['events'] = '{}';
						$data['responses'] = json_encode($this->ResponseOnline->emptyResponses($layout));

						$data['last_save'] = null;
						$data['deprecated'] = null;

						$this->ResponseOnline->create();
						if (!empty($this->ResponseOnline->save($data))) {
							$resp['response_online_id'] = $this->ResponseOnline->id;
							$resp['Version'] = array(
								'html' => $sitting_cache['Version']['html'],
								'settings' => $sitting_cache['Assessment']['Settings']
							);

							if ($closedTime !== null) {
								$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);
								$resp['time_remaining'] = $timeRemaining;
							}
						} else $success = false;
					} else $success = false;
				}
			}
		}

		$resp['success'] = $success;
		if ($message !== false) $resp['message'] = $message;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function get_overview($sittingStudent_id) {
		App::uses('Scoring', 'Lib');

		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
	   	$success = true;
	   	
	   	$this->SittingStudent->cleanResponses($sittingStudent_id);

	   	$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);

		$resp['details'] = $sittingStudent_cache['SittingStudent']['details'];
		
		// Attach basic data for each attempt

		$this->loadModel('ResponseOnline');
		$this->loadModel('ResultData');

		$resp['attempts'] = array();
		foreach ($sittingStudent_cache['ResponseOnline'] as $responseOnline_id) {
			$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);

			$entry = array(
				'id' => $responseOnline_id,
				'created' => $attempt_cache['created']
			);

			$this->loadModel('VersionData');

			$version_data_id = $attempt_cache['version_data_id'];
			$scoring_data = $this->VersionData->getScoringCache($version_data_id);
			$response_data = json_decode($attempt_cache['results'], true);
			$entry['layout'] = $scoring_data;
			$entry['results'] = $response_data;
		
			$closedTime = $attempt_cache['closed'];
			$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);		
			if ($timeRemaining <= 0) {
				$startTimestamp = strtotime($attempt_cache['created']);
				$endTimestamp = strtotime($attempt_cache['closed']);
				$entry['duration'] = $endTimestamp - $startTimestamp;
			}
			
			$resp['attempts'][] = $entry;
		}
		
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	private function get_status($entry, $sitting_closed) {
		if (($this->Auth->user('id') == null) || ($this->Auth->user('role') == 'student')) {
			$nowTimestamp = time();

			if ($entry['show']['type'] == 'close') {
				if (!$sitting_closed) return false;
			} else if ($entry['show']['type'] == 'date') {
				if (empty($entry['show']['date'])) return false;
				$showTimestamp = strtotime($entry['show']['date']);
				if ($nowTimestamp <= $showTimestamp) return false;
			}

			if ($entry['hide']['type'] == 'date') {
				if (empty($entry['hide']['date'])) return false;
				$hideTimestamp = strtotime($entry['hide']['date']);
				if ($nowTimestamp > $hideTimestamp) return false;
			}		
		}

		return true;
	}
	
	public function delete_attempt($sittingStudent_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
	   	$success = true;

		$responseOnline_id = $this->request->data['response_online_id'];

		$this->loadModel('ResponseOnline');
		$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);
		if (!empty($attempt_cache) && ($attempt_cache['sitting_student_id'] == $sittingStudent_id)) {
			$attempt_cache['deleted'] = date('Y-m-d H:i:s');
			$success &= $this->ResponseOnline->saveCacheData($attempt_cache);
			$success &= Cache::delete($attempt_cache['sitting_student_id'], 'sitting_students');
		} else $success = false;

		$resp['success'] = $success;
		if ($success) {
			$resp['sitting_student_id'] = $sittingStudent_id;
			$resp['response_online_id'] = $responseOnline_id;
		}
				
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function get_attempt($sittingStudent_id) {
		App::uses('Scoring', 'Lib');

		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
	   	$success = true;
		
		$responseOnline_id = $this->request->query['response_online_id'];

		$this->loadModel('ResponseOnline');
		$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);

		if (!empty($attempt_cache) && ($attempt_cache['sitting_student_id'] == $sittingStudent_id)) {

			$this->loadModel('Sitting');

			$sittingStudent_id = $attempt_cache['sitting_student_id'];
			$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
			$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);

			if ($sitting_cache['Sitting']['closed'] != null) {
				$nowTimestamp = time();
				$sittingEndTimestamp = strtotime($sitting_cache['Sitting']['closed']);
				$sitting_closed = ($sittingEndTimestamp <= $nowTimestamp);
			} else $sitting_closed = false;

			$json_data = $sitting_cache['Sitting']['json_data'];

			$keyed_entries = array();
			foreach ($json_data['Results'] as $entry) {
				$keyed_entries[$entry['type']] = $entry;
			}

			$showAll = (($this->Auth->user('id') != null) && ($this->Auth->user('role') != 'student'));

			// Get version data

			$this->loadModel('VersionData');
			$this->VersionData->contain();
			$version_data = $this->VersionData->findById($attempt_cache['version_data_id']);

			$scoring_data = json_decode($version_data['VersionData']['scoring_json'], true);

			// Get questions

			if (!empty($keyed_entries['questions'])) {
				$showQuestions = $this->get_status($keyed_entries['questions'], $sitting_closed);
			} else $showQuestions = $showAll;

			if ($showQuestions) {
				if (!empty($version_data['VersionData']['html'])) {
					$html = $version_data['VersionData']['html'];
				} else $html = gzdecode($version_data['VersionData']['html_gzip']);

				$resp['Version'] = array(
					'html' => $html,
					'settings' => $sitting_cache['Assessment']['Settings']
				);
			}

			if (!empty($attempt_cache['annotations'])) {
				$annotations = json_decode($attempt_cache['annotations'], true);
				$resp['annotations'] = $annotations;
			}

			// Get key data, including section headers

			if (!empty($keyed_entries['answers'])) {
				$showAnswers = $this->get_status($keyed_entries['answers'], $sitting_closed);
			} else $showAnswers = $showAll;

			if ($showAnswers) {
				$resp['key_sections'] = $scoring_data['Sections'];

				foreach ($resp['key_sections'] as &$pScoringSection) {
					foreach ($sitting_cache['Assessment']['Sections'] as $assessmentSection) {
						if ($assessmentSection['id'] == $pScoringSection['id']) {
							if (array_key_exists('header', $assessmentSection)) {
								$pScoringSection['header'] = $assessmentSection['header'];
							}
						}
					}	
				}
			}

			// Get scoring and outcomes results

			$responses_string = $attempt_cache['responses'];
			if (!empty($responses_string)) {

				// Check whether scoring should be included

				if (!empty($keyed_entries['scoring'])) {
					$showScoring = $this->get_status($keyed_entries['scoring'], $sitting_closed);
				} else $showScoring = $showAll;

				$resp['show_scoring'] = $showScoring;

				$responses = json_decode($responses_string, true);
				if ($responses != null) {

					// Remove comments if scoring isn't available yet

					if (!$showScoring) {
						foreach ($responses as &$pResponse) {
							unset($pResponse['comment']);
						}
					}

					// Return responses

					$resp['responses'] = $responses;
				}

				// Add scoring information if required
	
				if (!empty($keyed_entries['outcomes'])) {
					$showOutcomes = $this->get_status($keyed_entries['outcomes'], $sitting_closed);
				} else $showOutcomes = $showAll;
	
				if ($showScoring || $showOutcomes) {
					$resp['layout'] = $scoring_data;
					$resp['results'] = json_decode($attempt_cache['results'], true);
				}
	
				$resp['show_outcomes'] = $showOutcomes;
			}

			// Get events

			if (($this->Auth->user('id') != null) && ($this->Auth->user('role') != 'student')) {
				$events = json_decode($attempt_cache['events'], true);
				if (!empty($events['events'])) {
					$newEvents = array(array(
						'eventName' => 'start',
						'time' => strtotime($attempt_cache['created']) * 1000
					));

					foreach ($events['events'] as $event) {
						if (!empty($event['serverTime'])) {
							$event['serverTime'] = $event['serverTime'] * 1000;
						}
						if (!empty($event['clientTime'])) {
							$event['clientTime'] = $event['clientTime'];
						}

						if ($event['eventName'] == 'response') {
							$eventPart = $event;
							unset($eventPart['changed']);
							foreach ($event['changed'] as $index => $newAnswer) {
								$eventPart['index'] = $index;
								if ($newAnswer !== true) $eventPart['response'] = str_replace(' ', '_', $newAnswer);
								else unset($eventPart['response']);
								$newEvents[] = $eventPart;
							}
						} else $newEvents[] = $event;
					}
	
					$endTimestamp = null;
					if ($json_data['Access']['duration'] !== 'none') {
						$endTimestamp = strtotime($attempt_cache['created']);
						$endTimestamp += $json_data['Access']['duration'] * 60.0;
								
						$newEvents[] = array(
							'eventName' => 'end',
							'time' => $endTimestamp * 1000
						);
					}
	
					if (!empty($attempt_cache['closed'])) {
						$closedTimestamp = strtotime($attempt_cache['closed']);
						if ($closedTimestamp != $endTimestamp) {
							$newEvents[] = array(
								'eventName' => 'closed',
								'time' => $closedTimestamp * 1000
							);	
						}
					}

					$resp['events'] = $newEvents;
				}
			}

		} else $success = false;
		
		$resp['success'] = $success;
				
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function create_attempt($sittingStudent_id = null) {

		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;

		// Read and validate layout

		if (!empty($this->request->data['layout_sections'])) {
			$layout_string = $this->request->data['layout_sections'];
			
			if (empty($this->request->data['layout_hash'])) $success = false;
			else if ($this->request->data['layout_hash'] != md5($layout_string)) $success = false;
			else {
				$layoutSections = json_decode($layout_string, true);
				if ($layoutSections == null) $success = false;
			}
		} else $success = false;

		// Read and validate HTML

		if (!empty($this->request->data['html'])) {
			$html = $this->request->data['html'];

			if (empty($this->request->data['html_hash'])) $success = false;
			else if ($this->request->data['html_hash'] != md5($html)) $success = false;
		} else $success = false;

		if ($success) {
			$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
			$sitting_id = $sittingStudent_cache['SittingStudent']['sitting_id'];

			// Create a new assessment layout and version

			$this->loadModel('Sitting');
			$result = $this->Sitting->create_version($sitting_id, $html, $layoutSections);
			$success &= $result['success'];
		}

		if ($success) {

			// Create a new ResponseOnline entry

			$sitting_cache = $this->Sitting->getCache($sitting_id);

			$this->loadModel('ResultData');

			$data = array();
			$data['result_set_id'] = $sitting_cache['Sitting']['result_set_id'];
			$data['version_data_id'] = $result['version_data_id'];
			$data['results'] = json_encode($this->ResultData->emptyResults($result['scoring_data']));
			$this->ResultData->create();
			$this->ResultData->save($data);	

			$this->loadModel('ResponseOnline');
	
			$json_data = $sitting_cache['Sitting']['json_data'];

			$startTime = gmdate("Y-m-d H:i:s");
			$closedTime = $this->ResponseOnline->getClosed($startTime, $json_data);
	
			$data = array();
			$data['sitting_student_id'] = $sittingStudent_id;
			$data['version_data_id'] = $result['version_data_id'];
			$data['result_data_id'] = $this->ResultData->id;
			$data['closed'] = $closedTime;
			$data['events'] = '{}';
			$data['responses'] = json_encode($this->ResponseOnline->emptyResponses($result['scoring_data']));

			$data['last_save'] = null;
			$data['deprecated'] = null;

			$this->ResponseOnline->create();
			if (!empty($this->ResponseOnline->save($data))) {
				$resp['response_online_id'] = $this->ResponseOnline->id;

				if ($closedTime !== null) {
					$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);
					$resp['time_remaining'] = $timeRemaining;
				}
			} else {
				$this->loadModel('VersionData');
				$this->VersionData->delete($result['version_data_id']);
				$success = false;
			}
		}

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function save_comment($sittingStudent_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
	   	$success = true;

		if (isset($this->request->data['response_online_id'])) {
			$responseOnline_id = $this->request->data['response_online_id'];
		} else $success = false;

		if (isset($this->request->data['question_index'])) {
			$questionIndex = $this->request->data['question_index'];
		} else $success = false;

		if (isset($this->request->data['comment'])) {
			$newComment = $this->request->data['comment'];
			$resp['html'] = $newComment;
		} else $success = false;

		if ($success) {
			$this->loadModel('ResponseOnline');

			$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);
			if (!empty($attempt_cache) && ($attempt_cache['sitting_student_id'] == $sittingStudent_id)) {
				if (!empty($attempt_cache['responses'])) {
					$responses = json_decode($attempt_cache['responses'], true);
					if (($questionIndex >= 0) && ($questionIndex < count($responses))) {
						$responses[$questionIndex]['comment'] = $newComment;
						$attempt_cache['responses'] = json_encode($responses);
						$success &= $this->ResponseOnline->updateCache($attempt_cache);
					} else $success = false;
				} else $success = false;
			} else $success = false;	
		}
   
		$resp['success'] = $success;
				
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function delete_comment($sittingStudent_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
	   	$success = true;

		if (isset($this->request->data['response_online_id'])) {
			$responseOnline_id = $this->request->data['response_online_id'];
		} else $success = false;

		if (isset($this->request->data['question_index'])) {
			$questionIndex = $this->request->data['question_index'];
		} else $success = false;

		if ($success) {
			$this->loadModel('ResponseOnline');

			$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);
			if (!empty($attempt_cache) && ($attempt_cache['sitting_student_id'] == $sittingStudent_id)) {
				if (!empty($attempt_cache['responses'])) {
					$responses = json_decode($attempt_cache['responses'], true);
					if (($questionIndex >= 0) && ($questionIndex < count($responses))) {
						unset($responses[$questionIndex]['comment']);
						$attempt_cache['responses'] = json_encode($responses);
						$success &= $this->ResponseOnline->updateCache($attempt_cache);
					} else $success = false;
				} else $success = false;
			} else $success = false;	
		}
   
		$resp['success'] = $success;
				
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	private function getDeviceInfo($request_data) {
		$device_info = array();

		// Cookie ID is stored on the client device in view_open. Differing values don't guarantee different 
		// devices (since cookies can be deleted), but same value virtually guarantees the same device unless 
		// the value is being spoofed.

		if ($this->Cookie->check('cookie_id')) {
			$device_info['cookie_id'] = $this->Cookie->read('cookie_id');
		}

		// Fingerprint is derived from a range of browser- and device-specific parameters. The hope is that this
		// is somewhat unique to each device, and generally consistent over a timescale of days, but identically
		// configured devices will give identical results.

		if (!empty($request_data['fingerprint'])) {
			$device_info['fingerprint'] = $request_data['fingerprint'];
		}

		// IP hash should be different for different devices, and the same for the same device, over a timescale
		// of hours. Can be the same for different devices if a proxy is used on the client side. Client 
		// shouldn't be able to spoof this since the load balancer overwrites any value that's sent.

		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$device_info['ip_hash'] = md5($_SERVER['HTTP_X_FORWARDED_FOR']);
		}

		// Sort info so so that there's a standard form, then return it.

		ksort($device_info);
		return $device_info;
	}

	public function save_data($sittingStudent_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
	   	$success = true;

		$this->loadModel('ResponseOnline');

		$responseOnline_id = $this->request->data['response_online_id'];
		$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);

		if (!$attempt_cache) {
			$resp['badSitting'] = true;
			$success = false;
		} else {
			$closedTime = $attempt_cache['closed'];
			$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);
	
			if (($attempt_cache['sitting_student_deleted'] != null) || ($attempt_cache['deleted'] != null)) {
				$resp['badSitting'] = true;
				$success = false;
			} else if ($timeRemaining < -60) {
				$resp['isClosed'] = true;
				$success = false;
			} else {
				if ($timeRemaining < 0) $resp['time_remaining'] = 0.0;
				else if ($timeRemaining < INF) $resp['time_remaining'] = $timeRemaining;

				$response_string = $attempt_cache['responses'];
				$old_responses = empty($response_string) ? array() : json_decode($response_string, true);

				$results_string = $attempt_cache['results'];
				$old_results = json_decode($results_string, true);

				$events = json_decode($attempt_cache['events'], true);

				// Update results

				$new_results = array();
				$response_changes = array();
				$new_responses_string = $this->request->data['responses'];

				$validResponses = true;
				if (empty($this->request->data['responses_hash'])) $validResponses = false;
				else if ($this->request->data['responses_hash'] != md5($new_responses_string)) $validResponses = false;
				else {
					$new_responses = json_decode($new_responses_string, true);
					if ($new_responses === null) $validResponses = false;
				}

				if (!$validResponses) {
					App::uses('Email', 'Lib');
					$user_email = empty($this->Auth->user('id')) ? null : $this->Auth->user('email');
					Email::queue_error("SmarterMarks: Bad responses in save_data", $user_email, array(
						'Expected hash' => empty($this->request->data['responses_hash']) ? '[empty]' : $this->request->data['responses_hash'], 
						'Actual hash' => md5($new_responses_string), 
						'String end' => (strlen($new_responses_string) < 1000) ? $new_responses_string : substr($new_responses_string, -1000)
					));
				}

				if ($validResponses) {
					for ($index = 0; $index < count($new_responses); ++$index) {
						if (empty($old_responses[$index])) $old_response = '';
						else {
							$old_response = $old_responses[$index]['value'];

							// Copy across any comments

							if (!empty($old_responses[$index]['comment'])) {
								$new_responses[$index]['comment'] = $old_responses[$index]['comment'];
							}
						}
	
						$new_response = $new_responses[$index]['value'];
	
						if ($new_responses[$index]['type'] == 'wr') {
							if (empty($old_results[$index])) $old_result = '';
							else $old_result = $old_results[$index];
	
							if ($old_response != $new_response) $new_results[] = '';
							else $new_results[] = $old_result;
						} else $new_results[] = $new_response;
	
						$old_trimmed = empty(trim($old_response)) ? '' : $old_response;
						$new_trimmed = empty(trim($new_response)) ? '' : $new_response;
						if ($old_trimmed != $new_trimmed) {
							if (($new_responses[$index]['type'] == 'wr') && !empty($events['events'])) {
								if (!empty($events[count($events) - 1]['changed'])) {
									unset($events[count($events) - 1]['changed'][$index]);
									if (empty($events[count($events) - 1]['changed'])) {
										unset($events[count($events) - 1]);
									}	
								}
							}
	
							if ($new_responses[$index]['type'] == 'wr') $response_changes[$index] = true;
							else $response_changes[$index] = $new_response;
						}
					}
	
					$attempt_cache['responses'] = json_encode($new_responses);
					$attempt_cache['events'] = json_encode($events);
					$attempt_cache['results'] = json_encode($new_results);
	
					$success &= $this->ResponseOnline->updateCache($attempt_cache);
				} else $success = false;

				if (!empty($response_changes)) {

					// Save response update event

					$device_info = $this->getDeviceInfo($this->request->data);
					if (!empty($this->request->data['client_time'])) {
						$client_time = intval($this->request->data['client_time']);
					} else $client_time = false;

					$success &= $this->ResponseOnline->saveEvent($responseOnline_id, array(
						'serverTime' => time(),
						'clientTime' => $client_time,
						'eventName' => 'response',
						'changed' => (object)$response_changes
					), $device_info);
				}
			}
		}

		$resp['success'] = $success;
		   
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}	

	public function log_event($sittingStudent_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;
		   
		$this->loadModel('ResponseOnline');
		if (!empty($this->request->data['response_online_id'])) {
			$responseOnline_id = $this->request->data['response_online_id'];
			$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);
			if (!empty($attempt_cache) && ($attempt_cache['sitting_student_id'] == $sittingStudent_id)) {

				$device_info = $this->getDeviceInfo($this->request->data);

				if (!empty($this->request->data['details'])) {
					$details = json_decode($this->request->data['details'], true);
					if (empty($details)) $details = array();
				} else {
					$details = array('eventName' => $this->request->data['event_name']);
					if (!empty($this->request->data['client_time'])) {
						$details['clientTime'] = intval($this->request->data['client_time']);
					}
				}

				$details['serverTime'] = time();
				if (!isset($details['clientTime'])) $details['clientTime'] = false;

				$success &= $this->ResponseOnline->saveEvent($responseOnline_id, $details, $device_info);

			} else $success = false;	
		} else $success = false;
			
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}	

	public function save_annotations($sittingStudent_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;
		   
		$this->loadModel('ResponseOnline');
		if (!empty($this->request->data['response_online_id']) && !empty($this->request->data['annotations'])) {
			$responseOnline_id = $this->request->data['response_online_id'];
			$annotations = json_decode($this->request->data['annotations']);
			$attempt_cache = $this->ResponseOnline->getCache($responseOnline_id);
			if (!empty($annotations) && !empty($attempt_cache) && ($attempt_cache['sitting_student_id'] == $sittingStudent_id)) {
				$success &= $this->ResponseOnline->saveAnnotations($responseOnline_id, $annotations);
			} else $success = false;	
		} else $success = false;
			
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}	

	public function update_email($sittingStudent_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;

		$newEmail = $this->request->data['new_email'];

		$this->loadModel('User');

		$this->User->contain();
		$emailMatch = $this->User->findByEmail($newEmail);
		$this->User->contain();
		$signupEmailMatch = $this->User->findBySignupEmail($newEmail);
		$isValid = (filter_var($newEmail, FILTER_VALIDATE_EMAIL) !== FALSE);

		$errorMessage = "";
		if (!$isValid) {
			$errorMessage = "* Invalid email address";
			$success = false;
		} else if ($emailMatch || $signupEmailMatch) {
			$errorMessage = "* Email address already used";
			$success = false;
		} else {
			$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);

			$this->User->contain();
			$user = $this->User->findById($sittingStudent_cache['SittingStudent']['user_id']);
			if (!empty($user)) {
				if (($user['User']['role'] == 'student') && !empty($user['User']['validation_token'])) {
					$sitting_id = $sittingStudent_cache['SittingStudent']['sitting_id'];
					$user_id = $sittingStudent_cache['SittingStudent']['user_id'];
	
					$data = array();
					$data['id'] = $user_id;
					$data['email'] = $newEmail;
					$data['signup_email'] = $newEmail;
					$data['email_bounced'] = 0;
					$this->User->save($data);
	
					$this->User->send_validation($user_id);
				} else $success = false;
			} else $success = false;	
		}

		$resp['success'] = $success;
		if (empty($errorMessage)) {
			$resp['id'] = intval($sittingStudent_id);
			$resp['email'] = $newEmail;
		} else {
			$resp['message'] = $errorMessage;
		}

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function close($sittingStudent_id = null) {
		$this->autoRender = false;
		$this->layout = '';
		   
	   	$resp = array();
	   	$success = true;

		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);

		// Review assessment results

		$this->loadModel('ResponseOnline');
		foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
			$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
			$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
			if ($timeRemaining > 0) {
				$attempt_cache['closed'] = date('Y-m-d H:i:s');
				$success &= $this->ResponseOnline->saveCacheData($attempt_cache);
			}
		}

		$resp['sitting_student_id'] = $sittingStudent_id;
		$resp['success'] = $success;
	   	
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
	
	public function open($sittingStudent_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
	   	$success = true;
	   	
		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);

		$closeType = $this->request->data['close_type'];
		if (!empty($sittingStudent_cache['ResponseOnline'])) {
			$this->loadModel('ResponseOnline');
			$last_id = array_pop($sittingStudent_cache['ResponseOnline']);
			$attempt_cache = $this->ResponseOnline->getCache($last_id);

			$this->loadModel('Sitting');
			$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);

			$json_data = $sitting_cache['Sitting']['json_data'];
	
			if ($closeType == 'Default') {
				$startTime = $attempt_cache['created'];
				$closedTime = $this->ResponseOnline->getClosed($startTime, $json_data);
			} else if ($closeType == 'Specified') {
				$closedTime = $this->request->data['close_datetime'];
			} else $closedTime = null;

			$attempt_cache['closed'] = $closedTime;
			if (!$this->ResponseOnline->updateCache($attempt_cache)) $success = false;

			$timeRemaining = $this->ResponseOnline->getTimeRemaining($closedTime);
			if ($timeRemaining <= 0.0) $resp['status'] = 'closed';
			else $resp['status'] = 'open';
		} else $success = false;
			
		$resp['sitting_student_id'] = $sittingStudent_id;
		$resp['success'] = $success;
	   	
		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}	
	
	public function save_details($sittingStudent_id = null) {

		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
	   	$success = true;
	   	
		$this->loadModel('Sitting');
		$sittingStudent_cache = $this->SittingStudent->getCache($sittingStudent_id);
		$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);

		$data = array();
	   	$data['id'] = $sittingStudent_id;
		
		if (!empty($this->request->data['input_data'])) {
			$newData = json_decode($this->request->data['input_data'], true);
		} else $newData = array();
	   	
	   	$needsID = 0;
	   	$needsCode = 0;
	   	if (!empty($sittingStudent_cache)) {
			$json_data = $sitting_cache['Sitting']['json_data'];

			$user_id = $sittingStudent_cache['SittingStudent']['user_id'];
			if (empty($user_id) || ($user_id != $this->Auth->user('id'))) {
				if (isset($json_data['Access']['token_details'])) {
					$join_field = $json_data['Access']['token_details']['fieldName'];
					if (isset($newData[$join_field])) {
						$student_value = trim($newData[$join_field]);
						$join_values = $json_data['Access']['token_details']['fieldValues'];				
						$needsID = in_array($student_value, $join_values) ? 0 : 1;
					} else $needsID = 1;
				} else $needsID = 1;
			} else $needsID = 0;

		   	if ($json_data['Access']['start']['type'] == 'code') {
				if (isset($newData['Start code'])) {
					if ($newData['Start code'] == $json_data['Access']['start']['code']) {
						$needsCode = 0;
					} else $needsCode = 1;
				} else $needsCode = 1;
			} else $needsCode = 0;
		}

		if ($sittingStudent_cache['SittingStudent']['can_open'] == 0) {
			$canOpen = (!$needsID && !$needsCode);
		} else {
			$needsID = 0;
			$needsCode = 0;
			$canOpen = 1;
		}
		if ($canOpen) $data['can_open'] = 1;
	   	
	   	$resp['needsID'] = $needsID;
	   	$resp['needsCode'] = $needsCode;
	   	$resp['canOpen'] = $canOpen;
		   
		if ($canOpen) {
			$this->loadModel('Folder');
			
			if (empty($sittingStudent_cache['SittingStudent']['details'])) {
				unset($newData['Start code']);
				if (count($newData) == 0) $data['details'] = '';
				else $data['details'] = json_encode($newData);
			}
	
			if (isset($this->request->data['user_id']) && empty($sittingStudent_cache['SittingStudent']['user_id'])) {
				$this->loadModel('User');
				$this->User->contain();
				$user = $this->User->findById($this->request->data['user_id']);
				if (!empty($user)) {
					$folder_id = $this->Folder->getUserFolder($user['User']['id'], 'sittingstudents', 'home');
					$data['user_id'] = $user['User']['id'];
					$data['folder_id'] = $folder_id;
				}
			}
		}

		$success &= !empty($this->SittingStudent->save($data));
	   	
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
}


<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class DocumentsController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	}

	public function isAuthorized($user) {
		
		if (parent::isAuthorized($user)) {
	        return true;
	    }

		if (in_array($this->action, array('index', 'empty_trash'))) {
			return !empty($this->Auth->user('id'));
		}

		if (in_array($this->action, array('get_index'))) {
			if (!empty($this->request->query['folder_ids'])) {
				$this->loadModel('Folder');
				$folder_ids = explode(',', $this->request->query['folder_ids']);
				foreach ($folder_ids as $folder_id) {
					if (is_numeric($folder_id)) {
						$permissions = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
						return in_array('can_view', $permissions['all']);
					} else return false;
				}	
			} else return false;
		}

		if (in_array($this->action, array('create'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (isset($this->request->data['folder_id']) && is_numeric($this->request->data['folder_id'])) {
				$folderId = $this->request->data['folder_id'];
				$this->loadModel('Folder');
				$permissions = $this->Folder->getContentPermissions($folderId, $this->Auth->user('id'));
				return in_array('can_add', $permissions['all']);
			} else return true;
		}

	    if (in_array($this->action, array('send', 'copy', 'move'))) {
		    if ($this->Auth->user('role') == 'student') return false;
			if (!empty($this->request->data['item_ids'])) {
				foreach ($this->request->data['item_ids'] as $document_id) {
					$itemPermissions = $this->Document->getPermissions($document_id, $this->Auth->user('id'));
						
					if ($this->action == 'send') {
						if (!in_array('can_export', $itemPermissions)) return false;
					} else if (in_array($this->action, array('copy', 'move'))) {
						if (!empty($this->request->data['folder_id'])) {
							$this->loadModel('Folder');
							
							$targetFolderId = $this->request->data['folder_id'];
							$targetPermissions = $this->Folder->getContentPermissions($targetFolderId, 
								$this->Auth->user('id'));
	
							$this->Document->contain();
							$document = $this->Document->findById($document_id);
							$sourceFolderId = $document['Document']['folder_id'];
	
							if (!in_array('can_add', $targetPermissions['all'])) return false;
							if ($this->Folder->getCommunityId($sourceFolderId) != $this->Folder->getCommunityId($targetFolderId)) {
								if (!in_array('can_export', $itemPermissions)) return false;
							}
							if ($this->action == 'move') {
								if (!in_array('can_delete', $itemPermissions)) return false;
							}
						} else return false;
					} else return false;
				}
				return true;
			} else return false;
		}
		
		if (in_array($this->action, array('delete'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$document_id = $this->request->params['pass'][0];
				$itemPermissions = $this->Document->getPermissions($document_id, $this->Auth->user('id'));
				return (in_array('can_delete', $itemPermissions));
			} else return false;
		}

        if (in_array($this->action, array('get_student_data'))) {
			if (!empty($this->request->data['documentLists'])) {
                $document_lists = json_decode($this->request->data['documentLists'], true);
                foreach ($document_lists as $document_id => $list_rows) {
                    $itemPermissions = $this->Document->getPermissions($document_id, $this->Auth->user('id'));
                    if (!in_array('can_view', $itemPermissions)) return false;    
                }
            } else return false;

			if (!empty($this->request->data['classListID']) && is_numeric($this->request->data['classListID'])) {
                $class_list_id = intval($this->request->data['classListID']);

                $this->loadModel('ClassList');
                $this->ClassList->contain();
                $class_list = $this->ClassList->findById($class_list_id);

                if (empty($class_list)) return false;
                else if ($class_list['ClassList']['user_id'] != $this->Auth->user('id')) return false;
            }

            return true;
		}

		$needs_view = array('print_key', 'build', 'get', 'get_xml', 'print_questions');
		$needs_edit = array('save');
		if (in_array($this->action, array_merge($needs_view, $needs_edit))) {
			$document_id = null;
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				$document_id = $this->request->params['pass'][0];
			} else return false;
			
			$permissions = $this->Document->getPermissions($document_id, $this->Auth->user('id'));
			if (in_array($this->action, $needs_edit)) return in_array('can_edit', $permissions);
			else return in_array('can_view', $permissions);
		}

		return false;
	}

	// Index interface for response forms

	public function index() {
		$adminView = ($this->Auth->user('role') == 'admin');

        $this->loadModel('User');

		if ($adminView) {
			$jsonTree = "";
            $classLists = array();
		} else {
			$this->loadModel('Folder');
			$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'forms');
			$tree = $this->Folder->getTree($nodes);
			$jsonTree = json_encode($tree);

            $this->User->contain('ClassList');
            $user = $this->User->findById($this->Auth->user('id'));
            $classLists = $user['ClassList'];
		}

		$isExpired = $this->User->isExpired($this->Auth->user('id'));

		// Find any documents that have been sent to this user (is_new == 3)

		if ($this->Auth->user('role') != 'admin') {
			$receivedCount = $this->Document->find('count', array(
				'conditions' => array(
					'Document.user_id' => $this->Auth->user('id'),
					'Document.is_new' => 3
				)
			));
		} else $receivedCount = 0;

		// Get contacts

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$this->User->contain();
		$contactRecords = $this->User->find('all', array(
			'conditions' => array('User.id' => $contact_ids))
		);
		$contacts = array();
		foreach ($contact_ids as $id) {
			foreach ($contactRecords as $record) {
				if ($record['User']['id'] == $id) {
					$contacts[] = array(
						'id' => $record['User']['id'],
						'name' => $record['User']['name'],
						'email' => $record['User']['email']
					);
				}
			}
		}
				
		// Set variables for template
		
		$this->set(compact('adminView', 'jsonTree', 'classLists', 'isExpired', 'receivedCount', 'contacts'));

		$this->set('colCreated', 'Document.moved');
		$this->set('colName', 'Document.save_name');
		$this->set('itemType', 'Document');
		$this->set('index_element', '../Documents/index');

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");

		$this->viewPath = 'Elements';
		$this->render('index_common');
	}

	public function get_index() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$query_params = array('conditions' => array(
			'Document.deleted' => null
		));
		$userDefaults = json_decode($this->Auth->user('defaults'), true);

		if (isset($this->request->query['sort'])) {
			$sortBy = $this->request->query['sort'];
		} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$sortBy = 'Document.save_name';
		} else {
			$sortBy = 'Document.moved';
		}

		if (isset($this->request->query['direction'])) {
			$direction = $this->request->query['direction'];
		} else if (isset($userDefaults['Indices']['sort']) && ($userDefaults['Indices']['sort'] == 'name')) {
			$direction = 'asc';
		} else {
			$direction = 'desc';
		}

		$query_params['order'] = array($sortBy => $direction);

		if (isset($this->request->query['folder_ids'])) {

			$folder_ids = explode(',', $this->request->query['folder_ids']);
			$query_params['conditions']['Document.folder_id'] = $folder_ids;
            $query_params['contain'] = array('VersionData');

            $count = $this->Document->find('count', $query_params);
            $resp['count_total'] = $count;
            $resp['count_matching'] = $count;

        } else if ($this->Auth->user('role') == 'admin') {

			$query_params['conditions'][] = ['Document.folder_id IS NOT NULL'];
			$query_params['contain'] = array('User', 'VersionData');
            
		} else $success = false;
		
		if (isset($this->request->query['search'])) {

			$query_params['conditions'][] = array('OR' => array(
				'Document.save_name LIKE' => '%' . $this->request->query['search'] . '%',
				'Document.id' => $this->request->query['search']
			));
			$count = $this->Document->find('count', $query_params);
            $resp['count_matching'] = $count;
		}

		if (isset($this->request->query['limit'])) {
			if (is_numeric($this->request->query['limit']) && ($this->request->query['limit'] > 0)) {
				$query_params['limit'] = intval($this->request->query['limit']);
			} else if ($this->request->query['limit'] !== 'all') {
                $success = false;
            }
		} else $query_params['limit'] = 10;

		if (isset($this->request->query['page'])) {
			if (is_numeric($this->request->query['page']) && ($this->request->query['page'] > 0)) {
				$query_params['page'] = intval($this->request->query['page']);
			} else $success = false;
		} else $query_params['page'] = 1;

		if ($success) {
			$results = $this->Document->find('all', $query_params);	

			if ($this->Auth->user('role') != 'admin') {

				// Get permissions for each result

				$this->loadModel('Folder');

				$folder_permissions = array();
				foreach ($results as &$pResult) {
					$folder_id = $pResult['Document']['folder_id'];
					if (!array_key_exists($folder_id, $folder_permissions)) {
						$folder_permissions[$folder_id] = $this->Folder->getContentPermissions($folder_id, $this->Auth->user('id'));
					}

					if ($pResult['Document']['user_id'] == $this->Auth->user('id')) {
						$pResult['Permissions'] = $folder_permissions[$folder_id]['owner'];
					} else $pResult['Permissions'] = $folder_permissions[$folder_id]['all'];
				}

				// Update "is_new" for items

				$visible = array();
				foreach ($results as $result) {
					$visible[] = $result['Document']['id'];
				}
				$this->Document->updateAll(
					array('Document.is_new' => 0),
					array('Document.id' => $visible)
				);
				$this->Document->updateAll(
					array('Document.is_new' => 2),
					array(
						'Document.user_id' => $this->Auth->user('id'),
						'Document.is_new' => 3
					)
				);
			}

			$required_keys = array('id', 'moved', 'folder_id', 'save_name', 'is_new', 'has_html', 'version');
			foreach ($results as &$pResult) {
                $layout_data = json_decode($pResult['Document']['layout_json'], true);
                $pResult['Document']['version'] = $layout_data['Settings']['version'];

				if (!empty($pResult['VersionData']['html'])) $pResult['Document']['has_html'] = true;
				else if (!empty($pResult['VersionData']['html_gzip'])) $pResult['Document']['has_html'] = true;
				else $pResult['Document']['has_html'] = false;

				unset($pResult['VersionData']);
				
                foreach ($pResult['Document'] as $key => $value) {
					if (!in_array($key, $required_keys)) unset($pResult['Document'][$key]);
				}
			}

			$resp['results'] = $results;
		}

		$resp['page'] = $query_params['page'];
        if (array_key_exists('limit', $query_params)) {
            $resp['limit'] = $query_params['limit'];
        }
		$resp['success'] = $success;

		echo json_encode($resp);
	}

	public function create() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$this->loadModel('User');
		if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {

			$resp['message'] = 'Your account has expired. Unable to create new forms.';
			$success = false;

		} else {

			$folder_id = $this->request->data['folder_id'];

			if (array_key_exists('scoring_data', $this->request->data)) {
	
				$scoring_data = json_decode($this->request->data['scoring_data'], true);
	
			} else {
	
				$scoring_data = array(
					'Sections' => array(),
					'Outcomes' => array(),
					'Scoring' => array(
						'type' => 'Total',
					)
				);
					
			}

			if (array_key_exists('layout_data', $this->request->data)) {
	
				$layout_data = json_decode($this->request->data['layout_data'], true);
	
			} else {
	
				$defaults = json_decode($this->Auth->user('defaults'), true);

				$layout_data = array(
					'Settings' => array(
						'version' => $defaults['Document']['Settings']['version'],
						'language' => $defaults['Common']['Settings']['language']
					),
					'Page' => array(
						'pageSize' => $defaults['Common']['Settings']['pageSize'],
						'copiesPerPage' => $defaults['Document']['Settings']['copiesPerPage'],
						'marginSizes' => $defaults['Common']['Settings']['marginSizes']
					),
					'Title' => array(
						'text' => array()
					),
					'Body' => array(),
					'StudentReports' => $defaults['Document']['Reports']['Student']
				);

                if ($layout_data['StudentReports']['responses'] == 'none') {
                    unset($layout_data['StudentReports']['incorrectOnly']);
                }

                if ($layout_data['StudentReports']['results'] == 'numerical') {
                    unset($layout_data['StudentReports']['outcomesLevels']);
                }

                if (!empty($defaults['Common']['Title']['nameLines'])) {
                    $layout_data['Title']['nameLines'] = $defaults['Common']['Title']['nameLines'];
                }
            
				if ($defaults['Common']['Title']['logo']['imageHash'] !== false) {
					$layout_data['Title']['logo'] = array(
						'imageHash' => $defaults['Common']['Title']['logo']['imageHash'],
						'height' => $defaults['Common']['Title']['logo']['height']
					);
				}
		
				if ($defaults['Document']['Title']['idField']['show']) {
					$layout_data['Title']['idField'] = array(
						'label' => $defaults['Document']['Title']['idField']['label'],
						'bubbles' => $defaults['Document']['Title']['idField']['bubbles']
					);
				}
		
				if ($defaults['Document']['Title']['nameField']['show']) {
					$layout_data['Title']['nameField'] = array(
						'label' => $defaults['Document']['Title']['nameField']['label'],
						'bubbles' => $defaults['Document']['Title']['nameField']['bubbles']
					);
				}
					
			}

			// Build version data
		
			$data = array(
//				'html' => null,
				'html_gzip' => null,
				'scoring_json' => json_encode($scoring_data)
			);

			$this->loadModel('VersionData');
			$this->VersionData->create();
			$success &= !empty($this->VersionData->save($data));

			if ($success) {

				// Build document

				$data = array(
					'save_name' => "New Form",
					'user_id' => $this->Auth->user('id'),
					'folder_id' => $folder_id,
					'moved' => date('Y-m-d H:i:s'),
					'is_new' => 1,
					'version_data_id' => $this->VersionData->id,
					'layout_json' => json_encode($layout_data)
				);
		
				$this->Document->create();
				if (!empty($this->Document->save($data))) {
					$resp['id'] = $this->Document->id;
				} else {
					$this->VersionData->delete($this->VersionData->id);
					$success = false;	
				}
			}
		}

		$resp['success'] = $success;

		echo json_encode($resp);
	}

	public function send() {
		$this->autoRender = false;
		$this->layout = '';
		
		$this->loadModel('User');

		$success = true;

		$email_array = $this->request->data['email_list'];
		$item_ids = $this->request->data['item_ids'];

		if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
		else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

		$toUsers = array();
		foreach ($email_array as $email) {
			$email = trim($email);
			if (strlen($email) == 0) continue;
			
			$this->User->contain();
			$toUser = $this->User->findByEmail($email);
			if ($toUser && ($toUser['User']['role'] == 'teacher')) {
				$toUsers[] = $toUser;
				
				$userID = $toUser['User']['id'];
				if (($key = array_search($userID, $contact_ids)) !== false) {
					unset($contact_ids[$key]);
				}
				array_unshift($contact_ids, $userID);
			} else $success = false;
		}

		// Update contact IDs
		
		$newUserData = array();
		$newUserData['id'] = $this->Auth->user('id');
		$newUserData['contact_ids'] = implode(',', $contact_ids);
		$this->updateUserData($newUserData);

		// Send documents

		$this->loadModel('Folder');

		foreach ($toUsers as $toUser) {
			$userID = $toUser['User']['id'];
			$folder_id = $this->Folder->getUserFolder($userID, 'forms', 'home');

			foreach ($item_ids as $document_id) {
				$result = $this->Document->copyToFolder($document_id, $folder_id, $userID);
				if ($result['success']) {
					$this->Document->id = $result['document_id'];
					$this->Document->saveField('is_new', 3);
				} else $success = false;
			}
		}
		
		echo json_encode(
			array(
			    'success' => $success,
				'numSent' => count($item_ids)
			)
		);
	}

	public function copy() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$folder_id = $this->request->data['folder_id'];
		$item_ids = $this->request->data['item_ids'];
		
		if (isset($this->request->data['is_new'])) {
			$isNew = $this->request->data['is_new'];
		} else $isNew = 2;

		foreach ($item_ids as $document_id) {
			$result = $this->Document->copyToFolder($document_id, $folder_id, $this->Auth->user('id'));
			if ($result['success']) {
				$this->Document->id = $result['document_id'];
				$this->Document->saveField('is_new', $isNew);

				if ($result['source_folder_id'] == $result['target_folder_id']) {
					$this->Document->contain();
					$document = $this->Document->findById($result['document_id']);
					$this->Document->saveField('save_name', $document['Document']['save_name'] . ' - Copy');
				}
			} else $success = false;
		}

		$resp['success'] = $success;

		if ($success) {
			$resp['count'] = count($item_ids);

			if (count($item_ids) == 1) {
				$resp['id'] = $result['document_id'];
			} else $resp['folder_id'] = $folder_id;
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function move() {
		$this->autoRender = false;
		$this->layout = '';
		
		$success = true;
		$resp = array();

		$folder_id = $this->request->data['folder_id'];
		$item_ids = $this->request->data['item_ids'];

		$this->loadModel('Folder');
		$trash_folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'forms', 'trash');
		$isTrash = ($folder_id == $trash_folder_id);

		foreach ($item_ids as $document_id) {
			$data = array(
				'id' => $document_id,
				'user_id' => $this->Auth->user('id'),
				'folder_id' => $folder_id,
				'moved' => date('Y-m-d H:i:s')
			);
			if (!$isTrash) $data['is_new'] = 2;

			$success &= !empty($this->Document->save($data));
		}

		$resp['success'] = $success;

		if ($success) {
			$resp['count'] = count($item_ids);
			$resp['folder_id'] = $folder_id;
		}

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function empty_trash() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;

		$this->loadModel('Folder');
		$folder_id = $this->Folder->getUserFolder($this->Auth->user('id'), 'forms', 'trash');

		$trashDocuments = $this->Document->find('list', array(
			'fields' => array('Document.id'),
			'conditions' => array('Document.folder_id' => $folder_id)
		));
		
		$deleted_date = date('Y-m-d H:i:s');
		foreach ($trashDocuments as $document_id) {
			$this->Document->id = $document_id;
			$success &= !empty($this->Document->saveField('deleted', $deleted_date));
		}

		echo json_encode(
			array(
		   		'success' => $success
		   	)
		);
	}

    public function get_xml($document_id) {
		$this->autoRender = false;
		$this->layout = '';

        $success = true;
        $resp = array();

        $this->Document->contain();
        $document = $this->Document->findById($document_id);
        $resp['name'] = $document['Document']['save_name'];

        $formatXML = $this->Document->generateXML($document_id, true);
        $resp['formatXML'] = $formatXML;

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		
		echo json_encode($resp);
	}

    public function get_student_data() {
		$this->autoRender = false;
	   	$this->layout = '';

        $success = true;

        $resp = array();

        if (array_key_exists('classListID', $this->request->data)) {
            $this->loadModel('ClassList');
            $class_list_id = intval($this->request->data['classListID']);
            $this->ClassList->id = $class_list_id;
            $this->ClassList->saveField('last_used', date('Y-m-d H:i:s'));    
        }

        $student_data = array();

        $document_lists = json_decode($this->request->data['documentLists'], true);
        foreach ($document_lists as $document_id => $list_rows) {
            $options = array('document_id' => $document_id);
            if ($list_rows !== false) $options['list_rows'] = $list_rows;
            $this_student_data = $this->Document->getStudentData($options);
            
            if ($this_student_data !== false) {
                $student_data[$document_id] = $this_student_data;
            } else $success = false;
        }

        $resp['success'] = $success;

        if ($success) {
            $resp['student_data'] = $student_data;
        }

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
    }

	public function print_key($document_id) {
		$this->autoRender = false;
	   	$this->layout = '';

		$success = true;

		$this->Document->contain('VersionData');
		$document = $this->Document->findById($document_id);

		$scoring_data = json_decode($document['VersionData']['scoring_json'], true);
		$layout_data = json_decode($document['Document']['layout_json'], true);

		if (!$document) {
			$this->Flash->error("Invalid document");
			$this->redirect_home();
		}

		// Build HTML

		$view = new View($this, false);
		$view->layout = false;
		$view->viewPath='Documents';
		$view->set(compact('scoring_data', 'layout_data'));
		$viewHTML = $view->render('print_key');

		$defaults = json_decode($this->Auth->user('defaults'), true);
		$page_settings = $defaults['Common']['Settings'];

		// Build and return PDF

		$this->html_to_pdf_response($viewHTML, $page_settings);
	}

    // build interface
	public function build($document_id) {
		// Protection built into isAuthorized

		$this->Document->contain('VersionData');
		$document = $this->Document->findById($document_id);

		if (!empty($document['VersionData']['html'])) $isGenerated = 1;
		else if (!empty($document['VersionData']['html_gzip'])) $isGenerated = 1;
		else $isGenerated = 0;

		if ($isGenerated) {
			$this->loadModel('ResultData');
			$dataCount = $this->ResultData->find('count', array(
				'conditions' => array(
					'ResultData.version_data_id' => $document['Document']['version_data_id']
				)
			));
			$canReorder = ($dataCount == 0) ? 1 : 0;
		} else {
			$canReorder = 1;
		}

		if (!$document) {
			
			$this->Flash->error("Invalid document");
			$this->redirect_home();

		} else if ($this->Auth->user('role') === 'admin') {
			
			$canEdit = 1;
			$canCancel = 0;
			$showSettings = 0;

            $classLists = array();

		} else {
			
            $this->User->contain('ClassList');
            $user = $this->User->findById($this->Auth->user('id'));
            $classLists = $user['ClassList'];

			$permissions = $this->Document->getPermissions($document_id, $this->Auth->user('id'));
			$canEdit = in_array('can_edit', $permissions) ? 1 : 0;
		
			$this->loadModel('User');
			if ($this->User->isExpired($this->Auth->user('id')) & USER_LOCKED) {
				$canEdit = 0;
			}

			$canCancel = 0;
			$showSettings = 0;
			if ($document['Document']['is_new'] == 1) {             // Document can be cancelled
				$canCancel = 1;
				$showSettings = 1;
			} else if ($document['Document']['is_new'] > 0) {       // Clear sent and copied flags
				$showSettings = 1;
				
				$this->Document->id = $document['Document']['id'];
				$this->Document->saveField('is_new', 0);
			}
			
		}
		$this->set(compact('document_id', 'classLists', 'canEdit', 'isGenerated', 'canReorder', 'canCancel', 'showSettings'));

		$webroot = Configure::read('approot') . 'webroot/';
		$dictionary = file_get_contents($webroot . "Localization/dictionary.json");
		$this->set(compact('dictionary'));

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
	}

	public function get($document_id) {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		// Load assessment data

		$this->Document->contain('VersionData');
		$document = $this->Document->find('first', array(
			'conditions' => array('Document.id' => $document_id),
		));

		$resp['Document'] = array(
			'save_name' => $document['Document']['save_name']
		);

		$scoring_string = $document['VersionData']['scoring_json'];
		$resp['Version']['scoring_string'] = $scoring_string;
		$resp['Version']['scoring_hash'] = md5($scoring_string);

		if (!empty($document['VersionData']['html'])) {
			$resp['Version']['html'] = $document['VersionData']['html'];
		} else if (!empty($document['VersionData']['html_gzip'])) {
			$resp['Version']['html'] = gzdecode($document['VersionData']['html_gzip']);
		}


		$layout_string = $document['Document']['layout_json'];
		$resp['Document']['layout_string'] = $layout_string;
		$resp['Document']['layout_hash'] = md5($layout_string);

		$version_id = Cache::read('forms_' . $document_id, 'version_ids');
		if ($version_id === false) {
			$version_id = md5(microtime() . $document_id);
			Cache::write('forms_' . $document_id, $version_id, 'version_ids');	
		}
		$resp['Document']['version_id'] = $version_id;

		// Load question data

		$scoring_data = json_decode($document['VersionData']['scoring_json'], true);

		$question_handle_ids = array();
		foreach ($scoring_data['Sections'] as $section) {
			foreach ($section['Content'] as $content) {
				if (!empty($content['Source']['question_handle_id'])) {
					$question_handle_ids[] = $content['Source']['question_handle_id'];
				}
			}
		}

		if (!empty($question_handle_ids)) {
			$question_handle_ids = array_unique($question_handle_ids);

			$this->loadModel('Question');
			$this->loadModel('QuestionHandle');

			$this->QuestionHandle->contain();
			$question_map = $this->QuestionHandle->find('list', array(
				'fields' => array('QuestionHandle.question_id', 'QuestionHandle.id'),
				'conditions' => array('QuestionHandle.id' => $question_handle_ids)
			));
			
			$question_ids = array_keys($question_map);
			$questions = $this->Question->get($question_ids);

			foreach ($questions as &$pEntry) {
				$pEntry['question_handle_id'] = intval($question_map[$pEntry['id']]);
			}

			$resp['Questions'] = $questions;
	
			// Load statistics
	
			$resp['Statistics'] = $this->Question->getStatistics($question_ids);	
		}	
		
		// Send response

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function save($document_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$success = true;
	   	$resp = array();

		// Check version ID

		if (array_key_exists('version_id', $this->request->data)) {
			$sent_version_id = $this->request->data['version_id'];
			$cache_version_id = Cache::read('forms_' . $document_id, 'version_ids');
			if (($cache_version_id === false) || ($sent_version_id == $cache_version_id)) {
				$version_id = md5(microtime() . $document_id);
				Cache::write('forms_' . $document_id, $version_id, 'version_ids');	
				$resp['version_id'] = $version_id;
			} else {
				$resp['error_type'] = 'version_conflict';
				$success = false;
			}
		}

		// Check for request data

		$has_save_name = !empty($this->request->data['save_name']);

		$has_scoring_data = !empty($this->request->data['scoring_string']);
		if ($has_scoring_data) {
			$scoring_string = $this->request->data['scoring_string'];

			if (empty($this->request->data['scoring_hash'])) $success = false;
			else if ($this->request->data['scoring_hash'] != md5($scoring_string)) $success = false;
		
		} else $success = false;

		$has_layout_data = !empty($this->request->data['layout_string']);
		if ($has_layout_data) {
			$layout_string = $this->request->data['layout_string'];

			if (empty($this->request->data['layout_hash'])) $success = false;
			else if ($this->request->data['layout_hash'] != md5($layout_string)) $success = false;
		
		} else $success = false;

		// Update scoring data

		$old_scoring_string = false;
		if ($success && $has_scoring_data) {
			$new_scoring = json_decode($this->request->data['scoring_string'], true);

			$this->Document->contain('VersionData');
			$document = $this->Document->findById($document_id);
			if (!empty($document)) {
				$this->loadModel('VersionData');
                $old_scoring_string = $document['VersionData']['scoring_json'];
				$this->VersionData->id = $document['VersionData']['id'];
				$success &= !empty($this->VersionData->saveField('scoring_json', json_encode($new_scoring)));
			} else $success = false;
		}

		// Update document data

		if ($success && ($has_save_name || $has_layout_data)) {
			$data = array(
				'id' => $document_id,
				'is_new' => 0
			);

			if ($has_save_name) {
				$data['save_name'] = $this->request->data['save_name'];
			}

			if ($has_layout_data) {
				$data['layout_json'] = $this->request->data['layout_string'];
			}

			if (empty($this->Document->save($data))) {
                if ($has_scoring_data) {
                    $this->VersionData->id = $document['VersionData']['id'];
                    $this->VersionData->saveField('scoring_json', $old_scoring_string);    
                }
				$success = false;
			}
		}

		$resp['id'] = intval($document_id);
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);

	}

	public function delete($document_id) {
		$this->autoRender = false;
		$this->layout = '';
		
		$resp = array();
		$success = true;

		$success &= $this->Document->delete($document_id);

		$resp['success'] = $success;

        echo json_encode($resp);
	}

	public function print_questions($document_id) {
		$this->autoRender = false;
	   	$this->layout = '';

		$success = true;

		$this->Document->contain('VersionData');
		$document = $this->Document->findById($document_id);
		
		if (array_key_exists('html', $this->request->data)) {

			$assessmentHTML = $this->request->data['html'];

		} else if (!empty($document['VersionData']['html'])) {

			$assessmentHTML = $document['VersionData']['html'];

		} else if (!empty($document['VersionData']['html_gzip'])) {

			$assessmentHTML = gzdecode($document['VersionData']['html_gzip']);

		} else $success = false;

		if ($success) {
			
			// Build HTML

			$layout = json_decode($document['Document']['layout_json'], true);

			$page_settings = $layout['Page'];
			foreach ($layout['Settings'] as $key => $value) $page_settings[$key] = $value;

			$view = new View($this, false);
			$view->layout = false;
			$view->viewPath='Documents';
			$view->set(compact('page_settings', 'assessmentHTML'));
			$viewHTML = $view->render('print_questions');

			// Build and return PDF

			$this->html_to_pdf_response($viewHTML, $page_settings);

		} else {

			$this->response->type('json');
			echo json_encode(array(
				'success' => false
			));
	
		}
	}
}

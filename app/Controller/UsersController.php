<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

/**************************************************************************************************/
/* OAuth auth code based heavily on that created by Katy Nicholson                                */
/*                                                                                                */
/* https://github.com/CoasterKaty                                                                 */
/* https://katytech.blog/                                                                         */
/* https://twitter.com/coaster_katy                                                               */
/**************************************************************************************************/

App::uses('AppController', 'Controller');

class UsersController extends AppController {

	// Include the search plugin
	public $components = array('Search.Prg');

    public $presetVars = true; // using the model configuration
    public $paginate = array('paramType' => 'querystring');

	public function beforeFilter() {
	    parent::beforeFilter();

		$this->Auth->allow('login', 'logout', 'get_auth_redirect', 'auth_external', 'login_oauth', 'signup', 'email_used', 
			'validate_email', 'resend_verification', 'ajax_login', 'set_password', 'get_renewal_details', 
			'validateTeacherList', 'hide_tip', 'clear_message', 'bounced_email', 'expired', 'warning', 
			'renew_main', 'read_agreement');
	}

	// Determine who is authorized to view which pages
	public function isAuthorized($user) {
		
		if (parent::isAuthorized($user)) {
	        return true;
	    }

		if (in_array($this->action, array('set_open_created', 'open_quick', 'renew_group'))) {
	    	return true;
	    }

		if (in_array($this->action, array('add', 'email_list', 'change_approved', 'delete'))) {
		    return ($this->Auth->user('role') == 'admin');
		}

		if (in_array($this->action, array('edit', 'save', 'editPassword', 'history'))) {
			if (isset($this->request->params['pass'][0]) && is_numeric($this->request->params['pass'][0])) {
				return ($this->request->params['pass'][0] == $this->Auth->user('id'));
			} else return true;
	    }

		return false;
	}

	public function resend_verification($user_id = null) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$resp = array();
		$success = true;

		$this->User->contain();
		$user = $this->User->findById($user_id);
		if (!empty($user)) {
			if (!empty($user['User']['validation_token'])) {
				$this->loadModel('User');

                $this->User->id = $user['User']['id'];
				$this->User->saveField('email_bounced', 0);

				$this->User->send_validation($user['User']['id']);
			} else $success = false;
		} else $success = false;

		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}
   
    public function bounced_email() {
		$this->autoRender = false;
		$this->layout = '';

        App::import('Vendor', 'Aws', array('file' => 'AWS/aws-autoloader.php'));
		$message = Aws\Sns\Message::fromRawPostData();

		if ($message->offsetGet('Type') === 'Notification') {
			$data = json_decode($message->offsetGet('Message'), true);
			if (!empty($data['bounce']['bouncedRecipients'])) {
				foreach ($data['bounce']['bouncedRecipients'] as $recipient) {
					$this->User->contain();
					$user = $this->User->findByEmail($recipient['emailAddress']);
					if (!empty($user)) {
						$data = array();
						$data['id'] = $user['User']['id'];
						$data['email_bounced'] = 1;
						$this->User->save($data);
					}
				}
			}
		}
	}
	
	public function change_approved() {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		$user_ids = $this->request->data['user_ids'];
		$approval_status = $this->request->data['new_status'];

		foreach ($user_ids as $user_id) {
			$this->User->contain();
			$find_user = $this->User->findById($user_id);
	
			if (!empty($find_user)) {
	
				$data = array();
				$data['id'] = $user_id;
				$data['approved'] = $approval_status;
				
				if (!empty($this->User->save($data))) {
					App::uses('Email', 'Lib');
					if ($approval_status == 1) {
						$auth_details = $this->User->get_auth_details($find_user['User']['email']);
						$findUser['User']['auth_method'] = $auth_details['auth_method'];
						Email::send($find_user['User']['email'], "SmarterMarks: Account activated", "account_activated", 
							array('userData' => $find_user['User']));
					}
				} else $success = false;
			} else $success = false;
		}

		$resp['success'] = $success;
		$resp['count'] = count($user_ids);

		echo json_encode($resp);
	}
	
	public function warning() {
		$this->set('h1_title', 'Account expired');
		$this->set('title_for_layout', 'Account expired');
	}

	public function expired() {
		$this->set('h1_title', 'Account expired');
		$this->set('title_for_layout', 'Account expired');
	}

	public function read_agreement($response = null) {
		$this->set('title_for_layout', 'User agreement');
		
		if ($response === 'yes')
		{		
			$newUserData = array();
			$newUserData['id'] = $this->Auth->user('id');
			$newUserData['ua_read'] = 1;
			$this->updateUserData($newUserData);

			$this->redirect($this->Auth->redirect());
		}
	}

	public function renew_main() {
		$renew_by_email = false;

		if (!empty($this->Auth->user('id'))) {
			$isExpired = (strtotime($this->Auth->user('expires')) < strtotime('now'));
			if ($isExpired && empty($this->Auth->user('locked'))) {
				$newUserData = array();
				$newUserData['id'] = $this->Auth->user('id');
				$newUserData['locked'] = date('Y-m-d H:i:s', strtotime("+1 week"));
				$this->updateUserData($newUserData);
			}	

			// Check for renew by email

			$secrets = Configure::read('secrets');
			if (isset($secrets['email_no_card'])) {
				foreach ($secrets['email_no_card'] as $entry) {
					if (preg_match($entry['regex'], $this->Auth->user('email')) === 1) $renew_by_email = true;
				}	
			}
		}

		$this->set(compact('renew_by_email'));
	}

	public function renew_group($userID = null) {
		$this->set('h1_title', 'Welcome');
		$this->set('title_for_layout', 'Renewing for a Group');

		// Check for renew by email

		$renew_by_email = false;

		$secrets = Configure::read('secrets');
		if (isset($secrets['email_no_card'])) {
			foreach ($secrets['email_no_card'] as $entry) {
				if (preg_match($entry['regex'], $this->Auth->user('email')) === 1) $renew_by_email = true;
			}	
		}

		if ($renew_by_email) {
			$this->redirect(array('controller' => 'Users', 'action' => 'renew_main'));
		}

		// Get initial group members

		$this->loadModel('UserGroup');

		$groupID = null;
		if (!empty($this->request->query['g'])) {
			$groupID = intval($this->request->query['g']);
		} else {
			$userID = null;
			if ($this->Auth->user('role') != 'admin') {
				$userID = $this->Auth->user('id');
			} else if (!empty($this->request->query['u'])) {
				$userID = intval($this->request->query['u']);
			}

			if ($userID != null) {
				$this->UserGroup->contain('Group');
				$userGroups = $this->UserGroup->find('all', array(
					'conditions' => array(
						'UserGroup.user_id' => $userID,
						'UserGroup.is_active' => 1,
						'UserGroup.is_admin' => 1,
						'Group.is_active' => 1
					),
					'order' => array('UserGroup.id DESC')
				));
	
				if (count($userGroups) > 0) {
					$groupID = $userGroups[0]['UserGroup']['group_id'];
				}
			}
		}

		$users = array();
		$group_name = "";

		if ($groupID != null) {
			$this->loadModel('Group');
			$this->Group->contain();
			$group = $this->Group->findById($groupID);
			if (!empty($group)) $group_name = $group['Group']['name'];

			$this->UserGroup->contain('User');
			$userGroups = $this->UserGroup->find('all', array(
				'conditions' => array(
					'UserGroup.group_id' => $groupID,
					'UserGroup.is_active' => 1
				)
			));

			foreach ($userGroups as $userGroup) {
				$userGroup['User']['is_admin'] = $userGroup['UserGroup']['is_admin'];
				$users[] = $userGroup['User'];
			}
		} else if ($userID != null) {
			$this->User->contain();
			$thisUser = $this->User->findById($userID);

			$thisUser['User']['is_admin'] = 1;
			$users[] = $thisUser['User'];
		}

		// Get member details

		$members = array();
		foreach ($users as $user) {
			$entry = array(
				'id' => intval($user['id']),
				'email' => $user['email'],
				'name' => $user['name'],
				'is_admin' => intval($user['is_admin'])
			);

			if ($user['expires'] !== null) {
				$entry['expires'] = strtotime($user['expires']);
			}

			$members[] = $entry;
		}

		// Get school name

		$school_counts = array();
		$max_count = 0;
		$school_name = '';
		foreach ($users as $user) {
			$thisSchool = $user['school'];
			if (!isset($school_counts[$thisSchool])) $school_counts[$thisSchool] = 0;
			$school_counts[$thisSchool]++;

			if ($school_counts[$thisSchool] > $max_count) {
				$max_count = $school_counts[$thisSchool];
				$school_name = $thisSchool;
			}
		}

		// Get contacts
		
		$contacts = array();
		if ($this->Auth->user('role') != 'admin') {
			if (empty($this->Auth->user('contact_ids'))) $contact_ids = array();
			else $contact_ids = explode(',', $this->Auth->user('contact_ids'));

			$this->loadModel('User');
			$this->User->contain();
			$contactRecords = $this->User->find('all', array(
				'conditions' => array('User.id' => $contact_ids))
			);
			foreach ($contact_ids as $id) {
				foreach ($contactRecords as $record) {
					if ($record['User']['id'] == $id) {
						$contacts[] = array(
							'id' => $record['User']['id'],
							'name' => $record['User']['name'],
							'email' => $record['User']['email']
						);
					}
				}
			}
		}

		$this->set(compact('group_name', 'school_name', 'members', 'contacts'));
	}

	public function email_used() {
//		$this->set('h1_title', 'Email already used');
	}

	private function oauth_redirect($user, $auth_details) {

		// Remove old auth session from database if one exists

		if ($this->Session->check('session_key')) {
			$this->loadModel("AuthSession");
			$this->AuthSession->contain();
			$authSession = $this->AuthSession->findBySessionKey($this->Session->read('session_key'));
			if (!empty($authSession)) {
				$this->AuthSession->delete($authSession['AuthSession']['id']);
				$this->Session->delete('session_key');
			}
		} 

		// Get OAuth challenge
		// Challenge = Base64 Url Encode ( SHA256 ( Verifier ) )
		// Pack (H) to convert 64 char hash into 32 byte hex
		// As there is no B64UrlEncode we use strtr to swap +/ for -_ and then strip off the =

		$verifier = bin2hex(random_bytes(32));
		$challenge = str_replace('=', '', strtr(base64_encode(pack('H*', hash('sha256', $verifier))), '+/', '-_'));
		$challengeMethod = 'S256';

		// Create new auth session in database

		$session_key = bin2hex(random_bytes(32));
		$this->Session->write('session_key', $session_key);

		$this->loadModel('AuthSession');
		$data = array(
			'user_id' => $user['User']['id'],
			'session_key' => $session_key,
			'expires' => date('Y-m-d H:i:s', strtotime('+5 minutes')),
			'code_verifier' => $verifier,
			'access_token' => '', 
			'refresh_token' => '', 
			'id_token' => ''
		);
		$this->AuthSession->create();
		$this->AuthSession->save($data);

		// Request an authorization code
		// https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow

		$client_id = $auth_details['client_id'];
		$scope = $auth_details['scope'];

		$auth_uri = $auth_details['auth_uri'];
		return $auth_uri . 
			'?client_id=' . $client_id . 
			'&response_type=code' . 
			'&redirect_uri=' . urlencode($auth_details['redirect_uri']) . 
			'&scope=' . urlencode($scope) . 
			'&prompt=login' .
			'&login_hint=' . urlencode($user['User']['email']) . 
			'&code_challenge=' . $challenge . 
			'&code_challenge_method=' . $challengeMethod;
	}

	public function login_oauth() {
		$error_flash = 'Unable to complete login. Please try again.';
		$details = false;
		$sendError = true;

		$minExpiry = date('Y-m-d H:i:s', strtotime('-1 day'));

		$this->loadModel("AuthSession");
		$this->AuthSession->deleteAll(array(
			'AuthSession.expires <=' => $minExpiry
		));

		if ($this->Session->check('session_key')) {
			$session_key = $this->Session->read('session_key');

			$this->AuthSession->contain();
			$authSession = $this->AuthSession->findBySessionKey($session_key);

			if (!empty($authSession)) {

				$this->User->contain();
				$user = $this->User->findById($authSession['AuthSession']['user_id']);

				if (strtotime($authSession['AuthSession']['expires']) <= time()) {

					$details = array(
						'message' => "Auth session expired",
						'session_key' => $session_key
					);
		
				}
			} else {

				$details = array(
					'message' => "Auth session not found",
					'session_key' => $session_key
				);

			}

		} else {

			$details = array(
				'message' => "Session key not found"
			);

		}
		
		if (!empty($details)) {

			$error_flash = 'Login session expired. Please try again.';
			$sendError = false;

		} else if (!empty($user)) {

			$auth_details = $this->User->get_auth_details($user['User']['email']);
			
			$client_id = $auth_details['client_id'];

			$post_fields = array(
				'grant_type' => 'authorization_code',
				'client_id' => $client_id,
				'redirect_uri' => $auth_details['redirect_uri'],
				'code' => $this->request->query['code'],
				'code_verifier' => $authSession['AuthSession']['code_verifier']
			);

			if (isset($auth_details['certfile'])) {

				// Prepare certificate credentials
				// https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-certificate-credentials

				$certificates_dir = Configure::read('approot') . 'Config/Certificates/';
				$cert_filename = $certificates_dir . $auth_details['certfile'];
				$key_filename = $certificates_dir . $auth_details['keyfile'];

				$cert = file_get_contents($cert_filename);
				$certKey = openssl_pkey_get_private(file_get_contents($key_filename));
				$certHash = openssl_x509_fingerprint($cert, 'sha1', true);
				$certHash = base64_encode($certHash);

				$caHeader = json_encode(array(
					'alg' => 'RS256', 
					'typ' => 'JWT', 
					'x5t' => $certHash
				));

				$caPayload = json_encode(array(
					'aud' => $auth_details['payload_aud'],
					'exp' => date('U', strtotime('+10 minute')),
					'iss' => $client_id,
					'jti' => bin2hex(random_bytes(32)),
					'nbf' => date('U'),
					'sub' => $client_id
				));

				$caSignature = '';

				$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($caHeader));
				$base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($caPayload));
				$caData = $base64UrlHeader . '.' . $base64UrlPayload;

				openssl_sign($caData, $caSignature, $certKey, OPENSSL_ALGO_SHA256);

				$base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($caSignature));
				$clientAssertion = $caData . '.' . $base64UrlSignature;
				
				$post_fields['client_assertion_type'] = 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer';
				$post_fields['client_assertion'] = $clientAssertion;

			} else if (isset($auth_details['secret'])) {

				// Add client secret

				$post_fields['client_secret'] = urlencode($auth_details['secret']);

			}

			// Redeem authorization code for an access token
			// https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow
			
			$token_uri = $auth_details['token_uri'];
			$ch = curl_init($token_uri);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$response = curl_exec($ch);
			$cError = curl_error($ch);
			curl_close($ch);

			if (empty($cError)) {

				// Parse response
	
				if (!empty($response)) { 
					$reply = json_decode($response, true);
					if (empty($reply['error'])) {
						$id_errors = array();

						$tokenParts = explode('.', $reply['id_token']);
						if (count($tokenParts) != 3) {
							$id_errors[] = 'Incorrect element count in ID token';
						}

						if (empty($id_errors)) {

							// Check that ID token header values are as expected

							$id_header = json_decode(base64_decode($tokenParts[0]), true);

							$expected_header = array(
								'alg' => 'RS256', 
								'typ' => 'JWT'				
							);

							foreach ($expected_header as $key => $value) {
								if (!isset($id_header[$key])) {
									$id_errors[] = 'Missing "' . $key . '" value in header';
								} else if ($id_header[$key] != $value) {
									$id_errors[] = 'Unexpected "' . $key . '" value in header: ' . $id_header[$key];
								}
							}
						}

						if (empty($id_errors)) {

							// Check that ID token contains an auth ID or username that matches the expected

							$id_payload = json_decode(base64_decode($tokenParts[1]), true);
							if (!empty($id_payload)) {

								if (!empty($id_payload['email'])) {
									$received_email = strtolower($id_payload['email']);
								} else {
									$id_errors[] = 'No email record in ID token';
									$id_errors[] = $id_payload;
								}

								if (!empty($id_payload['sub'])) {
									$received_id = $id_payload['sub'];
								} else {
									$id_errors[] = 'No sub record in ID token';
									$id_errors[] = $id_payload;
								}

								if (empty($id_errors)) {
									if (!empty($user['User']['auth_id'])) {
										$expected_id = $user['User']['auth_id'];
										if ($received_id != $expected_id) {
											$id_errors[] = 'Auth ID mismatch in ID token';
											$id_errors[] = 'Expected: ' . $expected_id;
											$id_errors[] = 'Received: ' . $received_id;
										}
									} else {
										$expected_email = strtolower($user['User']['email']);
										if ($received_email != $expected_email) {
											$error_flash = 'Login email used does not match your SmarterMarks account email.';

											$id_errors[] = 'Email mismatch in ID token';
											$id_errors[] = 'Expected: ' . $expected_email;
											$id_errors[] = 'Received: ' . $received_email;
										}
									}
								}
							} else {
								$id_errors[] = 'Received ID token is not JSON encoded';
							}
						}

						if (empty($id_errors)) {

							// Update user auth ID if required

							if (empty($user['User']['auth_id'])) {
								$this->User->id = $user['User']['id'];
								$this->User->saveField('auth_id', $received_id);
								$user['User']['auth_id'] = $received_id;
							}

							// Update auth session

							$expires_seconds = $reply['expires_in'];
							$data = array(
								'id' => $authSession['AuthSession']['id'],
								'expires' => date('Y-m-d H:i:s', strtotime('+' . $expires_seconds . ' seconds')),
								'id_token' => json_encode($id_payload)
							);

							if (array_key_exists('access_token', $reply)) {
								$data['access_token'] = $reply['access_token']; 
							}
							if (array_key_exists('refresh_token', $reply)) {
								$data['refresh_token'] = $reply['refresh_token']; 
							}

							$this->AuthSession->save($data);

							// Log user in

							$this->complete_login($user);

						} else {

							$details = array(
								'message' => "Poorly formed ID token",
								'details' => $id_errors
							);

						}

					} else {

						$details = array(
							'message' => "Error in token response",
							'reply' => $reply
						);

					}
				} else {

					$details = array(
						'message' => "Empty response to token request"
					);

				}
			} else {

				$details = array(
					'message' => "Error in curl response",
					'error' => $cError
				);

			}

		} else {

			$details = array(
				'message' => "Could not find user for id",
				'session' => $authSession['AuthSession']
			);

		}

		$this->set(compact('details'));

		if (!empty($details)) {
			if (!empty($authSession)) {
				$this->AuthSession->delete($authSession['AuthSession']['id']);
			}
			$this->Session->delete('session_key');

			if ($sendError) {
				App::uses('Email', 'Lib');
				$secrets = Configure::read('secrets');
	
				$email_data = array('data' => $details);
				if (!empty($user['User']['email'])) $email_data['emails'] = array($user['User']['email']);
				Email::send($secrets['admin_email'], "OAuth login error", "general_error", $email_data);	
			}

			$this->Flash->error($error_flash);
			$this->redirect_home();
		}
	}

	public function get_auth_redirect() {
		$this->autoRender = false;
		$this->layout = '';

		$success = false;
		$resp = array();

		if (empty($this->request->query['email'])) {

		} else if (!filter_var($this->request->query['email'], FILTER_VALIDATE_EMAIL)) {

		} else {
			$this->User->contain();
			$user = $this->User->findByEmail($this->request->query['email']);
			if (!empty($user)) {
				if ($user['User']['approved'] == 0) {
					$this->Session->write('awaiting_id', $user['User']['id']);
					$resp['redirect'] = '/awaiting_approval';
				} else if (!empty($user['User']['validation_token'])) {
					$this->Session->write('awaiting_id', $user['User']['id']);
					$resp['redirect'] = '/awaiting_validation';
				} else {
					$auth_details = $this->User->get_auth_details($user['User']['email']);
					if ($auth_details['auth_method'] == 'oauth') {
						$resp['redirect'] = $this->oauth_redirect($user, $auth_details);
						$resp['needs_email'] = empty($user['User']['security_email']);
					} else if (empty($user['User']['password'])) {
						$resp['needs_password'] = true;
					}
					$success = true;
				}
			}
		}

		$resp['success'] = $success;

		echo json_encode($resp);
	}

	private function complete_login($user) {

		// Autorenew user account if needed

		if (!empty($user['User']['expires'])) {
			$secrets = Configure::read('secrets');
			$autorenew = $secrets['email_autorenew'];
			foreach ($autorenew as $entry) {
				if (preg_match($entry['regex'], $user['User']['email']) === 1) {
					if (strtotime($user['User']['expires']) < strtotime($entry['expires'])) {
						$newUserData = array();
						$newUserData['id'] = $user['User']['id'];
						$newUserData['expires'] = $entry['expires'];
						$newUserData['locked'] = null;

						$expiryTime = new DateTime($entry['expires']);
						$expiresString = $expiryTime->format('F j, Y');
						$newNote = date('Y-m-d') . ': Autorenewed through ' . $expiresString;
						$thisNote = trim($user['User']['notes']);
						if (!empty($thisNote)) $thisNote .= "\n";
						$thisNote .= $newNote;
						$newUserData['notes'] = $thisNote;

						$this->User->save($newUserData);
						$user['User'] = array_merge($user['User'], $newUserData);
					}
				}
			}
		}

		// Log in

		$this->User->id = $user['User']['id'];
		$this->User->saveField('last_login', date('Y-m-d H:i:s'));
		
		if ($this->Auth->login($user['User'])) {

			// Set security email if required

			if (empty($user['User']['security_email']) && array_key_exists('security_email', $_COOKIE)) {	
				$security_email = strtolower(trim($_COOKIE['security_email']));

				if (empty($security_email)) $security_email = 'NONE';
				else if (!filter_var($security_email, FILTER_VALIDATE_EMAIL)) $security_email = false;
				else if ($security_email == strtolower(trim($user['User']['email']))) $security_email = false;

				if ($security_email !== false) {
					$this->User->id = $user['User']['id'];
					$this->User->saveField('security_email', $security_email);	
				}
			}
		}		
	}

	public function ajax_login() {
		$this->autoRender = false;
		$this->layout = '';

		$success = false;
		$resp = array();

		if (empty($this->request->data['email']) || empty($this->request->data['password'])) {

			$resp['message'] = "Email address and password required. Please try again.";

		} else if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {

			$resp['message'] = "Invalid email address entered. Please try again.";

		} else {

			$this->User->contain();
			$user = $this->User->findByEmail($this->request->data['email']);	
	
			if (!empty($user)) {
				$password_correct = false;

				if ($user['User']['password'] == AuthComponent::password($this->request->data['password'])) {
					$password_correct = true;

					if ($user['User']['entropy'] == 0) {

						// Update password entropy

						$text = $this->request->data['password'];
						$entropy = $this->User->getEntropy($text);

						$this->User->id = $user['User']['id'];
						$this->User->saveField('entropy', $entropy);
						$user['User']['entropy'] = $entropy;
					}								
				}

				if ($password_correct) {
					$this->complete_login($user);
					$success = true;
				} else {
					$resp['message'] = "Incorrect password entered. Please try again.";
					if (($user['User']['role'] == 'student') && empty($user['User']['password'])) {
						$resp['action'] = 'verify';
					}
				}
			} else {

				$resp['message'] = "No user found with this email address. Please try again.";
				
			}
		}

		$resp['success'] = $success;

		echo json_encode($resp);
	}

	public function login() {
		$this->set('title_for_layout', 'User Login');
	}

	public function logout() {
	    $this->redirect($this->Auth->logout());
	}

	public function index() {
		$this->set('title_for_layout', 'Manage Users');

		$this->paginate['order'] = array('User.created' => 'desc');
        $this->paginate['limit'] = 10;

		$conditions = array();
		if (!empty($this->request->query['search'])) {
			$searchTerm = $this->request->query['search'];
			$conditions[] = array('OR' => array(
				array('User.name LIKE' => '%' . $searchTerm . '%'),
				array('User.email LIKE' => '%' . $searchTerm . '%'),
				array('User.school LIKE' => '%' . $searchTerm . '%')
			));
		}
		if (isset($this->request->query['role']) && ($this->request->query['role'] != 'all')) {
			$conditions[] = array('User.role' => $this->request->query['role']);
		}
		$this->paginate['conditions'] = $conditions;

		$pageFound = true;
		try {
			$paginated = $this->paginate('User');
		} catch (Exception $e) {
			if (get_class($e) == 'NotFoundException') $pageFound = false;
		}
		
		if ($pageFound === false) {
			if ($this->Session->check('pageNotFound')) unset($this->request->query['page']);
			else if (isset($this->request->query['page']) && ($this->request->query['page'] > 1)) {
				$this->request->query['page'] = $this->request->query['page'] - 1;
				$this->Session->write('pageNotFound', true);
			} else unset($this->request->query['page']);
			$redirectURL = strtok($_SERVER["REQUEST_URI"], '?');
			if (!empty($this->request->query)) $redirectURL .= '?' . http_build_query($this->request->query);
			header('Location: ' . $redirectURL);
		} else $this->Session->delete('pageNotFound');

        $this->set('users', $paginated);

        $total_users = $this->params['paging']['User']['count'];
        $this->set('total_users', $total_users);
    }

	public function view($id = null) {
		$this->set('title_for_layout', 'View User');
		$this->set('h1_title', 'View user');

		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->set('view_user', $this->User->read(null, $id));
	}
	
    public function history($id = null) {	

        $this->loadModel('Score');

		$userTimezone = new DateTimeZone($this->Auth->user('timezone'));
		$systemTimezone = new DateTimeZone(date_default_timezone_get());
		if (!empty($this->request->query['from'])) {
   	 		$fromTime = new DateTime($this->request->query['from'], $userTimezone);
   	 		$fromTime->setTime(0, 0, 0);
   	 		$fromTime->setTimezone($systemTimezone);
		} else {
	   	 	$fromTime = new DateTime();
   	 		$dates = $this->Score->find('list', array(
	   	 		'fields' => array('created'),
	   	 		'conditions' => array('user_id' => $id)
	   	 	));
   	 		foreach ($dates as $created) {
   	 			$thisTime = new DateTime($created);
	   	 		if ($thisTime < $fromTime) $fromTime = $thisTime;
   	 		}
		}
		if (!empty($this->request->query['to'])) {
   	 		$toTime = new DateTime($this->request->query['to'], $userTimezone);
   	 		$toTime->setTime(23, 59, 59);
   	 		$toTime->setTimezone($systemTimezone);
		} else {
	   	 	$toTime = new DateTime();
		}

   	 	$filters = array();
   	 	$filters[] = '(Score.user_id = ' . $id . ')';
   	 	$filters[] = "(Score.created >= '" . $fromTime->format('Y-m-d H:i:s') . "')";
   	 	$filters[] = "(Score.created <= '" . $toTime->format('Y-m-d H:i:s') . "')";   	
   	 	
		$fromTime->setTimezone($userTimezone);
		$this->set('from_date', $fromTime->format('F j, Y'));
		$toTime->setTimezone($userTimezone);
		$this->set('to_date', $toTime->format('F j, Y'));
		
		// Paginate scores for display
		
		$this->paginate['order'] = array('Score.created' => 'desc');
        $this->paginate['limit'] = 10;
        $this->paginate['contain'] = array('User', 'Document');
		$this->set('scores', $this->paginate($this->Score, $filters));
		
		$this->User->contain();
    	$user = $this->User->findById($id);
		$this->set('user_name', $user['User']['name']);
	}
/*
	public function isUser() {
		$this->autoRender = false;
		
		$fieldId = $this->request->query['fieldId'];
		$fieldValue = $this->request->query['fieldValue'];

		$this->User->contain();
		$user = $this->User->findByEmail($fieldValue);	
		$isUser = !empty($user);
		echo json_encode(
			array($fieldId, $isUser)
		);
	}
*/
	public function signup() {
		$this->autoRender = false;
		
		$resp = array();
		$success = false;

		$showForgot = false;
		$isStudent = false;
		if (!empty($this->request->data['email'])) {
			$email = $this->request->data['email'];
			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				
				$message = "* Must be a valid email address";
				
			} else {
	
				$this->User->contain();
				$emailMatch = $this->User->findByEmail($email);
				$this->User->contain();
				$signupEmailMatch = $this->User->findBySignupEmail($email);
	
				if ($emailMatch || $signupEmailMatch) {
					
					$message = "* This email is already in use. Click 'Forgot my<br>password' below to reset your password.";
					$showForgot = true;
					
				} else {
					
			 		$secrets = Configure::read('secrets');

					$students = $secrets['email_students'];
					foreach ($students as $entry) {
						if (preg_match($entry['regex'], $email) === 1) {
							$isStudent = true;
							break;
						}
					}
					
					$blacklist = $secrets['email_blacklist'];
					foreach ($blacklist as $entry) {
						if (preg_match($entry['regex'], $email) === 1) {
							$message = "* Please use your staff email address.";
					    	break;
						}
					}
					
					if (!$isStudent && !isset($message)) {

						// Create user row

						$token = bin2hex(random_bytes(16));

						$data = array(
							'signup_email' => $email,
							'email' => $email,
							'password' => '',
							'role' => 'teacher',
							'name' => $this->request->data['name'],
							'school' => $this->request->data['school'],
							'timezone' => $this->request->data['timezone'],
							'validation_token' => $token,
							'expires' => date('Y-m-d H:i:s', strtotime("+6 months")),
							'defaults' => $this->User->getDefaults()
						);

						// Check for whitelisted emails
						
						$suffix = substr($email, strpos($email, '@'));
						$numMatches = $this->User->find('count', array(
							'conditions' => array(
								'User.signup_email LIKE' => '%' . $suffix,
								'User.role' => 'teacher',
								'User.approved' => 1
							)
						));
						$isWhitelisted = ($numMatches > 0);
	
						$greylist = $secrets['email_greylist'];
						foreach ($greylist as $entry) {
							if (preg_match($entry['regex'], $email) === 1) $isWhitelisted = false;
						}
	
						$data['approved'] = $isWhitelisted ? 1 : 0;

						// Save user
										
						$this->User->create();
						if (!empty($this->User->save($data))) {					
							App::uses('Email', 'Lib');
							if ($isWhitelisted) {
								$auth_details = $this->User->get_auth_details($email);
								$data['auth_method'] = $auth_details['auth_method'];

								$resp['redirect'] = '/awaiting_validation';
								Email::send($email, "Welcome to SmarterMarks!", "account_activated", array('userData' => $data));
								$subject = "SmarterMarks: New user created";
							} else {
								$resp['redirect'] = '/awaiting_approval';
								Email::send($email, "Welcome to SmarterMarks!", "welcome_email");
								$subject = "SmarterMarks: New user pending";
							}
							$this->Session->write('awaiting_id', $this->User->id);

							$secrets = Configure::read('secrets');
							Email::send($secrets['admin_email'], $subject, "new_user", 
								array('userData' => $data));

							$success = true;
						}
					}			
				}
			}
		}
		
	    $resp['success'] = $success;
		if (isset($message)) $resp['message'] = $message;
		if ($showForgot) $resp['showForgot'] = 1;
		if ($isStudent) $resp['isStudent'] = 1;
		echo json_encode($resp);
	}

	public function add() {
		$this->set('title_for_layout', 'Add Users');
		
		$dataString = "";
		if (!empty($this->request->data)) {
			$dataLines = explode("\n", $this->request->data['user_info']);
			$numAdded = 0;
			$skippedLines = array();
			foreach ($dataLines as $line)
			{
				$elements = str_getcsv($line);
				foreach ($elements as &$element) $element = trim($element);
				if (count($elements) == 3) {
					$name = $elements[0];
					$school = $elements[1];
					$email = $elements[2];
				
					$this->User->contain();
					$emailMatch = $this->User->findByEmail($email);
					$this->User->contain();
					$signupEmailMatch = $this->User->findBySignupEmail($email);
					$isValid = (filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE);

					if ($emailMatch || $signupEmailMatch || !$isValid) {
						$skippedLines[] = $line;
					} else {
						$data = array(
							'signup_email' => $email,
							'email' => $email,
							'password' => '',
							'role' => 'teacher',
							'name' => $name,
							'school' => $school,
							'approved' => 1,
							'timezone' => $this->request->data['timezone'],
							'expires' => date('Y-m-d H:i:s', strtotime("+6 months")),
							'defaults' => $this->User->getDefaults()
						);

						$this->User->create();
						if (!empty($this->User->save($data))) {
							$numAdded++;
							$this->User->send_validation($this->User->id);
						}
					}
				} else {
					$skippedLines[] = $line;
				}
			}
			
			$numSkipped = count($skippedLines);

			if ($numAdded == 0) $flashMessage = "No users added";
			else $flashMessage = strval($numAdded) . " user" . (($numAdded == 1) ? '' : 's') . " added";
			if ($numSkipped > 0) $flashMessage .= "; " . strval($numSkipped) . " skipped";
			$flashMessage .= ".";

			if ($numAdded > 0) $this->Flash->success($flashMessage);
			else $this->Flash->error($flashMessage);
				
			if ($numSkipped == 0) {
				$this->redirect(array('controller' => 'Users', 'action' => 'index'));
			} else $dataString = implode("\n", $skippedLines);
 		}
 		
 		$this->set(compact('dataString'));
	}

	public function set_password($token = null) {
		$this->Auth->logout();

		$this->User->contain();
		$user = $this->User->findByValidationToken($token);

		if ($user) {
			$auth_details = $this->User->get_auth_details($user['User']['email']);
			if ($auth_details['auth_method'] == 'local') {
				$user['User']['password'] = '';
				$this->User->save($user);
				$this->Auth->login($user['User']);
			}
			$this->redirect_home();
		} else {
			$this->Flash->error("The link you clicked has expired. Enter your email to have another sent.");
			$this->redirect('/PasswordResets/send_reset');
		}
	}

	public function edit($id = null) {
		$this->set('title_for_layout', 'Edit User');

		if ($id == null) {
			$id = $this->Auth->user('id');
		}

		if (($this->Auth->user('role') == 'admin') && ($id != $this->Auth->user('id'))) {
			$this->set('target', "User's");
			$this->set('can_change_password', false);
		} else {
			$this->set('target', 'Your');
			$auth_details = $this->User->get_auth_details($this->Auth->user('email'));
			$this->set('can_change_password', ($auth_details['auth_method'] == 'local'));
		}

		$identifierList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
		$tzRegions = array();
		$tzCities = array();
		foreach ($identifierList as $identifier) {
			$exploded = explode('/', $identifier, 2);
			if (sizeof($exploded) == 2) {
				$tzRegions[$exploded[0]] = $exploded[0];
				$cityName = str_replace('_', ' ', implode(", ", array_reverse(explode('/', $exploded[1]))));
				$tzCities[$exploded[0]][$identifier] = $cityName;
			}
		}
		$this->set('tzRegions', $tzRegions);
		$this->set('tzCities', $tzCities);

		$this->User->contain();
		$rawData = $this->User->findById($id);
		$userData = $rawData['User'];

		$send_data = array('id', 'email', 'role', 'name', 'school', 'timezone', 'created', 'defaults', 
			'open_created', 'expires', 'locked', 'notes', 'message');
		foreach ($userData as $key => $value) {
			if (!in_array($key, $send_data)) unset($userData[$key]);
		}
		
		$this->set('user_data', $userData);
	}

	public function save($user_id) {
		$this->autoRender = false;
	   	$this->layout = '';

	   	$success = true;
	   	$resp = array();

		if (!empty($this->request->data['data_string'])) {
			$dataString = $this->request->data['data_string'];

			if (empty($this->request->data['data_hash'])) $success = false;
			else if ($this->request->data['data_hash'] != md5($dataString)) $success = false;
		
		} else $success = false;

		if ($success) {
			$new_data = json_decode($dataString, true);

			$email = strtolower(trim($new_data['email']));
			if (filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE) {
				$matching_ids = $this->User->find('list', array(
					'fields' => array('User.id'),
					'conditions' => array('User.email' => $email)
				));
				if (empty($matching_ids)) {
					if ($this->Auth->user('role') != 'admin') {
						$resp['message'] = 'Email address changed. Please validate your new email.';
						$resp['redirect'] = '/awaiting_validation';
						$this->Session->write('awaiting_id', $this->Auth->user('id'));
						$this->Auth->logout();
					}
				} else if (!in_array($user_id, $matching_ids)) {
					$resp['message'] = 'Email address has already been used.';
					$success = false;
				}
			} else {
				$resp['message'] = 'Invalid email address entered.';
				$success = false;
			}
		}
   
		if ($success) {

			$allowed_data = array('email', 'name', 'school', 'timezone', 'defaults', 'open_created');
			if ($this->Auth->user('role') == 'admin') {
				$allowed_data = array_merge($allowed_data, array('role', 'expires', 'locked', 'notes', 'message'));
			}

			foreach ($new_data as $key => $value) {
				if (!in_array($key, $allowed_data)) unset($new_data[$key]);
			}

			$defaults = json_decode($new_data['defaults'], true);
			$new_data['id'] = $user_id;
			$success &= $this->updateUserData($new_data);
		}

		if ($success && empty($matching_ids)) {
			$this->User->send_validation($user_id);
		}

		$resp['id'] = intval($user_id);
		$resp['success'] = $success;

		header("Cache-Control: no-store, no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}

	public function editPassword($id = null) {
		$this->loadModel('PasswordReset');
		$token = $this->PasswordReset->getReset($id);
		$this->redirect(array('controller' => 'PasswordResets', 'action' => 'new_password', $token));
	}
	
	public function validate_email($token = null) {
		$this->User->contain();
		$tokenUser = $this->User->findByValidationToken($token);
		if (!$tokenUser) {
			$this->Flash->error("Error validating email.");
			$this->redirect_home();
		} else {
			$data = array();
			$data['id'] = $tokenUser['User']['id'];
			$data['validation_token'] = null;
			$data['auth_id'] = '';
			if (!empty($this->User->save($data))) {
				$this->Auth->logout();
				$this->Flash->success("Email validated.  Please log in.");
				$this->redirect(array('controller' => 'Users', 'action' => 'login'));
			}
		}
	}

	public function delete() {
		$this->autoRender = false;
		$this->layout = '';

		$success = true;
		$resp = array();

		$user_ids = $this->request->data['user_ids'];
		foreach ($user_ids as $user_id) {
			$success &= $this->User->delete($user_id);
		}

		$resp['success'] = $success;
		$resp['count'] = count($user_ids);

		echo json_encode($resp);
	}
	
	public function clear_message() {
		$newUserData = array();
		$newUserData['message'] = null;
		$newUserData['id'] = $this->Auth->user('id');
		if ($this->updateUserData($newUserData)) {
			$this->Flash->success("Message tagged as received.");
			$this->redirect_home();
		} else $this->Flash->error("Error clearing message.");
	}
	
	public function hide_tip($name = null) {
		$this->autoRender = false;

		$success = true;
		
		if (empty($this->Auth->user('hidden_tips'))) $hiddenTips = array();
		else $hiddenTips = explode(";", $this->Auth->user('hidden_tips'));

		if (!in_array($name, $hiddenTips)) {
			$hiddenTips[] = $name;

			$newUserData = array();
			$newUserData['hidden_tips'] = implode(";", $hiddenTips);
			$newUserData['id'] = $this->Auth->user('id');		
			$this->updateUserData($newUserData);
		}

		echo json_encode(
			array(
		   		'success' => $success
		   	)
		);
	}

	public function get_renewal_details() {
		$this->autoRender = false;
		$this->layout = '';

		$resp = array();
		$success = true;

		$email_array = $this->request->data['emails'];

		$kept_keys = array('id', 'email', 'name');

		$no_user = array();
		$students = array();
		$users = array();

		$this->loadModel('User');
		foreach ($email_array as $email) {
			if (strlen($email) == 0) continue;
			$this->User->contain();
			$user = $this->User->find('first', array(
				'conditions' => array(
					'User.email' => $email
				)
			));

			if (empty($user)) {
				$no_user[] = $email;
			} else if ($user['User']['role'] == 'student') {
				$students[] = $email;
			} else {
				$data = array();
				foreach ($kept_keys as $key) $data[$key] = $user['User'][$key];

				if (!empty($user['User']['expires'])) {
					$created_timestamp = strtotime($user['User']['created']);
					$trial_timestamp = strtotime('+6 months', $created_timestamp);
					$expires_timestamp = strtotime($user['User']['expires']);

					if (abs($trial_timestamp - $expires_timestamp) < 10) {
						$data['expires'] = 'trial';
					} else $data['expires'] = $expires_timestamp;
				} else $data['expires'] = 'none';
	
				$users[] = $data;
			}
		}
		
		$resp['success'] = $success;
		$resp['details'] = $users;
		if (count($students) > 0) $resp['student'] = $students;
		if (count($no_user) > 0) $resp['no_user'] = $no_user;

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
		echo json_encode($resp);
	}
	
	public function validateTeacherList() {		
		$this->autoRender = false;
		$this->layout = '';

		$email_list = $this->request->query['email_list'];
		$email_list_id = $this->request->query['email_list_id'];
		$email_array = preg_split("/[,;\s]+/", $email_list);

		$this->loadModel('User');
		$errorString = "";
		foreach ($email_array as $email) {
			if (strlen($email) == 0) continue;
			$this->User->contain();
			$user = $this->User->findByEmail($email);
			if (empty($user)) {
				$errorString = '* ' . $email . ' is not a SmarterMarks user';
				break;
			} else if ($user['User']['role'] != 'teacher') {
				$errorString = '* ' . $email . ' is not a teacher account';
				break;
			}
		}
		
		$status = array();
		if (strlen($errorString) > 0) $status[] = array($email_list_id, false, $errorString);
		echo json_encode($status);
	}	

	public function set_open_created() {
		$this->autoRender = false;
		$this->layout = '';
		
		$this->updateUserData(array(
			'id' => $this->Auth->user('id'),
			'open_created' => $this->request->data['value']
		));

		$resp = array();
		$resp['success'] = true;

		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}

	public function open_quick() {
		$this->autoRender = false;
		$this->layout = '';
		
		if (isset($this->request->data['source'])) {
			$this->log(array(
				'id' => $this->Auth->user('id'),
				'email' => $this->Auth->user('email'),
				'source' => $this->request->data['source']
			), 'share_sources');
		}

		$this->loadModel('Folder');
		$nodes = $this->Folder->getNodes($this->Auth->user('id'), 'questions');
		$search_folders = array();
		foreach ($nodes as $node) $search_folders[] = $node['data']['folder_id'];

		$conditions = 'u.folder_id IN (' . implode(',', $search_folders) . ') AND u.deleted IS NULL';

		$this->loadModel('Question');
		$this->Question->query('
			UPDATE user_questions u
				JOIN question_handles h ON (h.id = u.question_handle_id)
				JOIN questions q ON (q.id = h.question_id)
			SET q.open_status = ' . $this->Auth->user('id') . ', 
                q.opened = "' . date('Y-m-d H:i:s') . '", 
                q.needs_index = "' . date('Y-m-d H:i:s') . '"
			WHERE ' . $conditions . ' AND q.creator_id = ' . $this->Auth->user('id') . ' AND q.open_status = 0'
		);

		$resp = array();
		$resp['success'] = true;

		$resp['num_open'] = $this->Question->find('count', array(
			'conditions' => array('Question.open_status !=' => 0)
		));
		
		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: -1");
        echo json_encode($resp);
	}
}

#!/bin/sh
find .. -type f -name "*.js" | xargs sed -i '' 's/2024 SmarterMarks/2025 SmarterMarks/g'
find .. -type f -name "*.ctp" | xargs sed -i '' 's/2024 SmarterMarks/2025 SmarterMarks/g'
find .. -type f -name "*.cc" | xargs sed -i '' 's/2024 SmarterMarks/2025 SmarterMarks/g'
find .. -type f -name "*.h" | xargs sed -i '' 's/2024 SmarterMarks/2025 SmarterMarks/g'
find .. -type f -name "*.php" | xargs sed -i '' 's/2024 SmarterMarks/2025 SmarterMarks/g'

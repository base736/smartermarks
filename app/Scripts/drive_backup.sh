#!/bin/bash

S3_IMAGES="s3:smartermarks-userdata"
DRIVE_IMAGES="drive:images_backup"

rclone sync $S3_IMAGES $DRIVE_IMAGES

S3_BACKUP="s3:smartermarks-db-backup"
DRIVE_BACKUP="drive:database_backup"

recent_backup=$(rclone lsl $S3_BACKUP --max-depth 1 | sort -rk 2 | head -n 1 | awk '{print $NF}')

if [ -z "$recent_backup" ]; then
  echo "No backup found in S3 bucket."
  exit 1
fi

rclone delete $DRIVE_BACKUP/
rclone copy $S3_BACKUP/$recent_backup $DRIVE_BACKUP/
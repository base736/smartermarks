<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class AssessmentItem extends AppModel {
	var $actsAs = array('Containable');
	
	private $handle_ids = array();

	public $belongsTo = array(
		'Assessment' => array(
			'className' => 'Assessment',
			'foreignKey' => 'assessment_id'
		),
		'QuestionHandle' => array(
			'className' => 'QuestionHandle',
			'foreignKey' => 'question_handle_id'
		)
	);

	public function beforeDelete($cascade = true) {
		$this->contain();		
		$assessmentItem = $this->findById($this->id);
		
		if (!empty($assessmentItem['AssessmentItem']['question_handle_id'])) {
			$this->handle_ids[$this->id] = $assessmentItem['AssessmentItem']['question_handle_id'];
		} else unset($this->handle_ids[$this->id]);
		
		return true;
	}

	public function afterDelete() {
		if (isset($this->handle_ids[$this->id])) {
			$this->QuestionHandle->clean($this->handle_ids[$this->id]);
			unset($this->handle_ids[$this->id]);
		}
	}
}

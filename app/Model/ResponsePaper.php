<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class ResponsePaper extends AppModel {
	var $actsAs = array('Containable');
	
	public $useTable = 'responses_paper';

    private $result_set_ids = array();
    private $score_ids = array();

	public $belongsTo = array(
		'Score' => array(
			'className' => 'Score',
			'foreignKey' => 'score_id'
		),
		'ResultSet' => array(
			'className' => 'ResultSet',
			'foreignKey' => 'result_set_id'
		)
	);

    public function beforeDelete($cascade = true) {
		$this->contain();		
		$response_paper = $this->findById($this->id);

		if (!empty($response_paper['ResponsePaper']['score_id'])) {
			$this->score_ids[$this->id] = $response_paper['ResponsePaper']['score_id'];
		} else unset($this->score_ids[$this->id]);

        if (!empty($response_paper['ResponsePaper']['result_set_id'])) {
			$this->result_set_ids[$this->id] = $response_paper['ResponsePaper']['result_set_id'];
		} else unset($this->result_set_ids[$this->id]);

		return true;
	}

	public function afterDelete() {

		if (isset($this->score_ids[$this->id])) {
            $this->Score->delete($this->score_ids[$this->id]);
			unset($this->score_ids[$this->id]);
		}
		if (isset($this->result_set_ids[$this->id])) {
            $this->ResultSet->delete($this->result_set_ids[$this->id]);
			unset($this->result_set_ids[$this->id]);
		}
	}

}

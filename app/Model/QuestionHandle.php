<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class QuestionHandle extends AppModel {
	var $actsAs = array('Containable');
	public $displayField = 'name_alias';

	public $belongsTo = array(
		'Question' => array(
			'className' => 'Question',
			'foreignKey' => 'question_id'
		)
	);
	
	public $hasMany = array(
		'UserQuestion' => array(
			'className' => 'UserQuestion',
			'foreignKey' => 'question_handle_id',
			'dependent' => true
		),
		'AssessmentItem' => array(
			'className' => 'AssessmentItem',
			'foreignKey' => 'question_handle_id',
			'dependent' => true
		)
	);
	
	public function createHandle($data, $lock_open) {
		$question_handle_id = false;

		$conditions = array();
		if (isset($data['question_id'])) $conditions['question_id'] = $data['question_id'];
		else return null;
		
		if (isset($data['user_id'])) $conditions['user_id'] = $data['user_id'];
		else $conditions['user_id'] = null;
		
		if (isset($data['community_id'])) $conditions['community_id'] = $data['community_id'];
		else $conditions['community_id'] = null;

		if (isset($data['sitting_id'])) $conditions['sitting_id'] = $data['sitting_id'];
		else $conditions['sitting_id'] = null;
		
		$this->contain();
		$handle = $this->find('first', array('conditions' => $conditions));
		if ($handle) {	
			$question_handle_id = intval($handle['QuestionHandle']['id']);
		} else {
			$this->Question->contain();
			$question = $this->Question->findById($data['question_id']);
			if (empty($data['name_alias'])) $data['name_alias'] = $question['Question']['name'];

			$this->create();
			$this->save($data);

			$question_handle_id = intval($this->id);

			if (($question['Question']['open_status'] > 0) && $lock_open) {
				$question_ids = array($data['question_id']);
				do {
					$this->Question->updateAll(
						array('Question.open_status' => -1),
						array('Question.id' => $question_ids)
					);
					$question_ids = $this->Question->find('list', array(
						'fields' => array('Question.id'),
						'conditions' => array(
							'Question.parent_id' => $question_ids,
							'Question.open_status !=' => 0
						)
					));
				} while (count($question_ids) > 0);
			}			
		}

		return $question_handle_id;
	}

	public function getPermissions($question_handle_id, $user_id) {
		if (empty($question_handle_id) || !is_numeric($question_handle_id)) return array();
		if (empty($user_id) || !is_numeric($user_id)) return array();

		$allPermissions = array('can_view', 'can_add', 'can_export', 'can_delete', 'can_edit');

		$this->contain(array('UserQuestion', 'AssessmentItem' => 'Assessment'));
		$handle = $this->findById($question_handle_id);
		
		$permissions = array();
		if ($handle) {
			if ($handle['QuestionHandle']['community_id'] != null) {

				$communityModel = ClassRegistry::init('Community');
				$communityPermissions = $communityModel->getPermissions($handle['QuestionHandle']['community_id'],
					$user_id);

				$role = 'all';
				foreach ($handle['UserQuestion'] as $userQuestion) {
					if ($userQuestion['user_id'] == $user_id) $role = 'owner';
				}	
				foreach ($handle['AssessmentItem'] as $assessmentItem) {
					if ($assessmentItem['Assessment']['user_id'] == $user_id) $role = 'owner';
				}
				
				$permissions = $communityPermissions[$role];

			} else if ($handle['QuestionHandle']['user_id'] != null) {

				if ($handle['QuestionHandle']['user_id'] == $user_id) $permissions = $allPermissions;

			} else if ($handle['QuestionHandle']['sitting_id'] != null) {

				$sittingModel = ClassRegistry::init('Sitting');
				$sitting_id = $handle['QuestionHandle']['sitting_id'];
				$permissions = $sittingModel->getPermissions($sitting_id, $user_id);
				
			}
		}
		
		return $permissions;
	}
	
	public function clean($question_handle_id) {
		$this->contain();
		$handle = $this->findById($question_handle_id);
		$success = true;
		
		if ($handle) {
			$isPersistent = true;
			if ($handle['QuestionHandle']['community_id'] !== null) $isPersistent = false;
			else if ($handle['QuestionHandle']['user_id'] !== null) $isPersistent = false;

			if (!$isPersistent) {
				$userQuestionCount = $this->UserQuestion->find('count', array(
					'conditions' => array('UserQuestion.question_handle_id' => $question_handle_id)
				));
				$assessmentItemCount = $this->AssessmentItem->find('count', array(
					'conditions' => array('AssessmentItem.question_handle_id' => $question_handle_id)
				));
				
				if ($userQuestionCount + $assessmentItemCount == 0) {
					$question_id = $handle['QuestionHandle']['question_id'];				
					$success &= $this->delete($question_handle_id);
					$success &= $this->Question->clean($question_id);
				}
			}
		} else $success = false;

		return $success;
	}
}

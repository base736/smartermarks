<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

function tohtml($input) {
	return htmlentities($input, ENT_XML1, 'UTF-8');
}

function rawKeyValue($key, $markByDigits, $addValues) {
	$thisValue = 0.0;

	if (strlen($key) > 0) {
		if (($key[0] == '#') || ($key[0] == '@')) $key = substr($key, 1);
	}
	
	if (strlen($key) > 0) {

		$elementStrings = explode(';', $key);
		foreach ($elementStrings as $elementString) {
			$elements = explode('=', $elementString);
			$answer =  $elements[0];
			if (count($elements) == 1) $keyValue = 1.0;
			else $keyValue = floatval($elements[1]);
			if ($markByDigits) $keyValue *= strlen($answer);
			
			if ($addValues) $thisValue += $keyValue;
			else if ($keyValue > $thisValue) $thisValue = $keyValue;
		}
	}
	
	return $thisValue;
}

class Document extends AppModel {
	var $actsAs = array('Containable');
	public $displayField = 'save_name';

	private $version_data_ids = array();

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		),
		'Folder' => array(
			'className' => 'Folder',
			'foreignKey' => 'folder_id'
		),
		'VersionData' => array(
			'className' => 'VersionData',
			'foreignKey' => 'version_data_id'
		)
	);

	public $hasMany = array(
		'Score' => array(
			'className' => 'Score',
			'foreignKey' => 'document_id',
			'dependent' => true
		)
	);

    public function getQRParts($qr_text) {
        $resp = array();

        // Remove any text in parentheses

        while (preg_match('/\([^)]*\)/', $qr_text)) {
            $qr_text = preg_replace('/\([^)]*\)/', '', $qr_text);
        }

        // Get the name part of the remaining text

        $name = '';
        preg_replace_callback('/([a-zA-Z-\' ,&;]+)/', function($matches) use(&$name) {
            $block = trim($matches[1], '-\' ,');
            if (strlen($block) > strlen($name)) $name = $block;
            return $matches[0];
        }, $qr_text);

        if (strlen($name) > 0) {
            $resp['name'] = ucwords($name);
        }
    
        // Get the ID part of the remaining text

        $id = '';
        preg_replace_callback('/([\d]+)/', function($matches) use(&$id) {
            $block = $matches[1];
            if (strlen($block) > strlen($id)) $id = $block;
            return $matches[0];
        }, $qr_text);                  
        
        if (strlen($id) > 0) {
            $resp['id'] = $id;
        }

        return $resp;
    }

    public function getQRTags($version, $document_id, $count) {
        $datasource = $this->getDataSource();

        $datasource->query('LOCK TABLES documents WRITE');

        $result = $datasource->query('SELECT next_form_id FROM documents WHERE id=' . $document_id);
        if (empty($result)) {
            $datasource->query('UNLOCK TABLES');
            return(false);    
        }

        $form_ids = array();
        $next_form_id = $result[0]['documents']['next_form_id'];
        for ($i = 0; $i < $count; ++$i) {
            $form_ids[] = $next_form_id;
            $next_form_id = ($next_form_id + 1) % 100000;
        }

        $datasource->query('UPDATE documents SET next_form_id=' . $next_form_id . ' WHERE id=' . $document_id);
        $datasource->query('UNLOCK TABLES');

        $qr_tags = array();
        foreach ($form_ids as $form_id) {
            $qr_tag = strval($version);                                // QR format version number
            $qr_tag .= str_pad($document_id, 9, '0', STR_PAD_LEFT);    // Document ID to 9 digits, zero-padded
            $qr_tag .= str_pad($form_id, 5, '0', STR_PAD_LEFT);        // Form ID to 5 digits, zero-padded
            $qr_tags[] = $qr_tag;
        }

        return $qr_tags;
    }

	public function canScore($document_id) {
	
		App::uses('Scoring', 'Lib');

		$this->contain('VersionData');
        $document = $this->findById($document_id);
		$scoring_data = json_decode($document['VersionData']['scoring_json'], true);

		$hasSection = false;
		$message = NULL;
		if (!empty($scoring_data['Sections'])) {
			$hasAnswers = false;
			foreach($scoring_data['Sections'] as $section) {
				foreach ($section['Content'] as $item) {
					if ($item['type'] == 'wr') $hasAnswers = true;
					else if (!empty($item['ScoredResponses'])) $hasAnswers = true;
				}
			}

			if ($hasAnswers) {
				if ($scoring_data['Scoring']['type'] != 'Total') {
					$weights = Scoring::getWeights($scoring_data);
					$hasZeroWeight = false;
					$questionIndex = 0;

					foreach($scoring_data['Sections'] as $section) {
						foreach ($section['Content'] as $item) {
							$isOmitted = true;
							if (!array_key_exists('scoring', $item)) $isOmitted = false;
							else if ($item['scoring'] != 'omit') $isOmitted = false;

							if ((!$isOmitted) && ($weights[$questionIndex] == 0)) $hasZeroWeight = true;
							$questionIndex++;
						}
					}

					if ($hasZeroWeight) {
						$message = "Cannot score assessment. Zero weight for some questions.";
					}
				}
			} else {
				$message = "Cannot score assessment. No scored responses defined.";
			}
		} else {
			$message = "Cannot score assessment. This assessment contains no sections.";
		}

		return $message;
	}
	
	public function getImageHashes($documentID) {
		$hashes = array();

		$this->contain('VersionData');
		$document = $this->findById($documentID);

		$layout_data = json_decode($document['Document']['layout_json'], true);
		if (array_key_exists('logo', $layout_data['Title'])) {
			$hashes[] = $layout_data['Title']['logo']['imageHash'];			
		}

		$scoring_data = json_decode($document['VersionData']['scoring_json'], true);
		foreach ($scoring_data['Sections'] as $section) {
			foreach ($section['Content'] as $item) {
				if ($item['type'] != 'wr') continue;
				else if (!array_key_exists('prompt', $item['TypeDetails'])) continue;
				else if ($item['TypeDetails']['prompt']['type'] != 'image') continue;
				$hashes[] = $item['TypeDetails']['prompt']['imageHash'];
			}
		}
		
		return array_values(array_unique($hashes));
	}

	public function getPermissions($documentID, $user_id) {
		if (empty($documentID) || !is_numeric($documentID)) return array();
		if (empty($user_id) || !is_numeric($user_id)) return array();

		$this->contain();
		$document = $this->findById($documentID);

		$permissions = array();
		if (!empty($document)) {			
			$folderModel = ClassRegistry::init('Folder');
			$folderPermissions = $folderModel->getContentPermissions($document['Document']['folder_id'], $user_id);
			if ($user_id == $document['Document']['user_id']) $permissions = $folderPermissions['owner'];
			else $permissions = $folderPermissions['all'];
		} 
		return $permissions;
	}

	public function copy($document_id) {
		$this->contain('VersionData');
		$document = $this->findById($document_id);

		if (!$document) {
			return array('success' => false);
		}

		$success = true;
		$resp = array();

        $versionDataModel = ClassRegistry::init('VersionData');
		
		unset($document['VersionData']['id']);
		$versionDataModel->create();
		$success &= !empty($versionDataModel->save($document['VersionData']));

		$document['Document']['version_data_id'] = $versionDataModel->id;

		unset($document['Document']['id']);
		unset($document['Document']['created']);

		$document['Document']['user_id'] = null;
		$document['Document']['folder_id'] = null;
		$document['Document']['moved'] = null;
		$document['Document']['next_form_id'] = 0;

		$this->create();
		if (!empty($this->save($document))) {
            $resp['copyID'] = $this->id;
        } else $success = false;

		$resp['success'] = $success;
		return $resp;
	}
	   
    public function copyToFolder($document_id, $folder_id, $user_id) {
		$success = true;
		$resp = array();

		$this->contain();
		$document = $this->findById($document_id);

		$sourceFolderId = $document['Document']['folder_id'];

		$result = $this->copy($document_id);
		$success = $result['success'];

		if ($success) {
			$copyID = $result['copyID'];

			$data = array(
				'id' => $copyID,
				'user_id' => $user_id,
				'folder_id' => $folder_id,
				'moved' => date('Y-m-d H:i:s')
			);
			
			if (!empty($this->save($data))) {
				$resp['document_id'] = $copyID;
				$resp['source_folder_id'] = $sourceFolderId;
				$resp['target_folder_id'] = $folder_id;	
			} else {
				$this->delete($copyID);
				$success = false;
			}
		}
				
		$resp['success'] = $success;
		return $resp;
    }

	private function getXMLAnswers($item) {
		$responseMap = array();
		foreach ($item['Answers'] as $answer) {
			$responseMap[$answer['id']] = $answer['response'];
		}

		$answers = '';
		foreach ($item['ScoredResponses'] as $entry) {
			if ($entry['value'] > 0) {
				if (strlen($answers) > 0) $answers .= ';';
				$ids = array();
				if (array_key_exists('id', $entry)) $ids = array($entry['id']);
				else if (array_key_exists('ids', $entry)) $ids = $entry['ids'];
				
				foreach ($ids as $id) {
					$answers .= $responseMap[$id];
				}

				if ($entry['value'] != 1.0) {
					$answers .= '=' . $entry['value'];
				}

			}
		}

		if (array_key_exists('scoring', $item)) {
			if ($item['scoring'] == 'omit') $answers = '#' . $answers;
			else if ($item['scoring'] == 'bonus') $answers = '@' . $answers;
		}

		return $answers;
	}

    public function getStudentData($options) {

        // Get format XML 

        if (array_key_exists('format_xml', $options)) {
            $formatXML = $options['format_xml'];
        } else if (array_key_exists('document_id', $options)) {
            $formatXML = $this->generateXML($options['document_id'], false);
        } else return false;

        // Parse format XML to get version

        $format = simplexml_load_string($formatXML);
        $version = $format->version;

        // Build QR tags if needed

        if (array_key_exists('list_rows', $options)) {
            $num_tags = count($options['list_rows']);
        } else if ($version == 3) {
            $num_tags = 1;
        } else $num_tags = 0;

        if ($num_tags > 0) {

            $qrDataModel = ClassRegistry::init('QrData');

            if (array_key_exists('document_id', $options)) {
                $qr_tags = $this->getQRTags($version, $options['document_id'], $num_tags);
            } else $qr_tags = array('123456789012345');

            if ($qr_tags === false) return false;

            for ($i = 0; $i < $num_tags; ++$i) {
                if (!empty($options['list_rows'][$i])) {
                    $json_string = json_encode($options['list_rows'][$i]);
                } else $json_string = '{}';

                $qrDataModel->create();
                $qrDataModel->save(array(
                    'qr_tag' => $qr_tags[$i],
                    'last_used' => date('Y-m-d H:i:s'),
                    'json_data' => $json_string
                ));
            }

        }

        // Get student data

        $student_data = array();
        if (array_key_exists('list_rows', $options)) {    

            for ($i = 0; $i < $num_tags; ++$i) {
                if (!array_key_exists('label', $options['list_rows'][$i])) {
                    $parts = array();
                    if (array_key_exists('name', $options['list_rows'][$i])) {
                        $parts[] = $options['list_rows'][$i]['name'];
                    }
                    if (array_key_exists('id', $options['list_rows'][$i])) {
                        $parts[] = $options['list_rows'][$i]['id'];
                    }
                    $label = implode(' ', $parts);
                } else $label = $options['list_rows'][$i]['label'];

                $student_data[] =  array(
                    'qrText' => $qr_tags[$i],
                    'nameText' => $label
                );
            }

        } else if ($version == 3) {

            $student_data[] = array(
                'qrText' => $qr_tags[0]
            );

        }

        return $student_data;
    }

    private function prepareData(&$pArray, &$pID_map, $sortKeys) {    
        foreach ($pArray as $key => &$pValue) {
            if (is_array($pValue)) {
                $sortThis = !in_array($key, array('Sections', 'Content', 'Body'));
                $this->prepareData($pValue, $pID_map, $sortThis);
            }
        }
    
        if ($sortKeys) ksort($pArray);

        if (array_key_exists('id', $pArray)) {
            $old_id = $pArray['id'];
            if (!array_key_exists($old_id, $pID_map)) {
                unset($pArray['id']);
                $new_id = md5(json_encode($pArray));
                $pID_map[$old_id] = $new_id;
            }
            $pArray['id'] = $pID_map[$old_id];
        }
    }    

	private function getFormHash($scoring_data, $layout_data, $retain_text) {

        // Clear elements which should be allowed to change

        unset($layout_data['StudentReports']);

        if (!$retain_text) {
            foreach ($layout_data['Title']['text'] as &$pLine) {
                $pLine = '';
            }    
        }
        
        foreach ($scoring_data['Sections'] as &$pSection) {
			foreach ($pSection['Content'] as &$pItem) {
                $pItem['type'] = substr($pItem['type'], 0, 2);
                if ($pItem['type'] == 'nr') {
                    unset($pItem['Answers']);
                    foreach ($pItem['TypeDetails'] as $key => $value) {
                        if ($key != 'nrDigits') unset($pItem['TypeDetails'][$key]);
                    }
                }
                unset($pItem['ScoredResponses']);
                unset($pItem['scoring']);
            }
        }

        unset($scoring_data['Outcomes']);
        unset($scoring_data['Scoring']);

        // Sort data and (if allowed) replace IDs with a new value based on content

        $id_map = array();
        $this->prepareData($scoring_data, $id_map, true);
        $this->prepareData($layout_data, $id_map, true);

        // Prepare hash

        $hash_string = json_encode(array(
            'layout_data' => $layout_data,
            'scoring_data' => $scoring_data
        ));
		return md5($hash_string);
	}
    
    public function generateXML($id, $include_hash) {

		App::uses('Scoring', 'Lib');

		$this->contain('VersionData');
		$document = $this->findById($id);
		$scoring_data = json_decode($document['VersionData']['scoring_json'], true);
		$layout_data = json_decode($document['Document']['layout_json'], true);
		
		// create the xml document
		$xml_root = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><document></document>');

		// version
		$xml_root->addChild("version", $layout_data['Settings']['version']);

        if ($include_hash) {

            // get form hashes

            $hash_with_text = $this->getFormHash($scoring_data, $layout_data, true);
            $xml_root->addChild("hashWithText", $hash_with_text);
            $hash_no_text = $this->getFormHash($scoring_data, $layout_data, false);
            $xml_root->addChild("hashNoText", $hash_no_text);
        }

        // copiesPerPage
		$xml_root->addChild("copiesPerPage", $layout_data['Page']['copiesPerPage']);

		// pageWidth and pageHeight
		if ($layout_data['Page']['pageSize'] == 'letter') {
			$xml_root->addChild("pageWidth", '8.500');
			$xml_root->addChild("pageHeight", '11.000');
		} else if ($layout_data['Page']['pageSize'] == 'legal') {
			$xml_root->addChild("pageWidth", '8.500');
			$xml_root->addChild("pageHeight", '14.000');
		} else if ($layout_data['Page']['pageSize'] == 'a4') {
			$xml_root->addChild("pageWidth", '8.267');
			$xml_root->addChild("pageHeight", '11.692');
		}	

		// Margin sizes
		$xml_root->addChild("topMarginSize", $layout_data['Page']['marginSizes']['top']);
		$xml_root->addChild("bottomMarginSize", $layout_data['Page']['marginSizes']['bottom']);
		$xml_root->addChild("leftMarginSize", $layout_data['Page']['marginSizes']['left']);
		$xml_root->addChild("rightMarginSize", $layout_data['Page']['marginSizes']['right']);

		// logoHeight
		if (array_key_exists('logo', $layout_data['Title'])) {
			$xml_root->addChild("logoHeight", $layout_data['Title']['logo']['height']);
		}

		// language
		$xml_root->addChild("language", tohtml($layout_data['Settings']['language']));

		// The website no longer uses pages to organize sections. An XML page is created to preserve compatibility
		// with previous versions -- new documents are arranged onto one logical page that includes page breaks.
		$d_page = $xml_root->addChild("page");

		$title = $d_page->addChild("title");

		// logoHash
		if (array_key_exists('logo', $layout_data['Title'])) {
			$title->addChild("logoHash", $layout_data['Title']['logo']['imageHash']);
		}
		
		// nameLines
		if (!empty($layout_data['Title']['nameLines'])) {
			$title->addChild("nameLines", tohtml(implode(',', $layout_data['Title']['nameLines'])));
		}

		// showNameField
		if (array_key_exists('nameField', $layout_data['Title'])) {
			$title->addChild("showNameField", 'YES');
			$title->addChild("nameLabel", tohtml($layout_data['Title']['nameField']['label']));
			$title->addChild("nameLetters", $layout_data['Title']['nameField']['bubbles']);
		} else $title->addChild("showNameField", 'NO');

		// showIDField
		if (array_key_exists('idField', $layout_data['Title'])) {
			$title->addChild("showIDField", 'YES');
			$title->addChild("idLabel", tohtml($layout_data['Title']['idField']['label']));
			$title->addChild("idDigits", $layout_data['Title']['idField']['bubbles']);
		} else $title->addChild("showIDField", 'NO');

		// text
		foreach ($layout_data['Title']['text'] as $line) {
			$title->addChild("text", tohtml($line));
		}
		
		foreach ($layout_data['Body'] as $layout_section) {

			if ($layout_section['type'] == 'page_break') {

				$d_page = $xml_root->addChild("page");

			} else {
				
				$scoring_section = false;
				foreach ($scoring_data['Sections'] as $entry) {
					if ($entry['id'] == $layout_section['id']) $scoring_section = $entry;
				}

                if ($scoring_section !== false) {

                    if (!empty($scoring_section['Content'])) {
                        $startNumber = intval($scoring_section['Content'][0]['number']);
                    } else $startNumber = 1;
        
                    if ($layout_section['type'] == 'mc') {
    
                        $mc_section = $d_page->addChild("mcSection");
    
                        // startNumber
                        $mc_section->addChild("startNumber", $startNumber);
    
                        // header
                        $mc_section->addChild("header", tohtml($layout_section['header']));
    
                        // choiceCount	
                        $mc_section->addChild("choiceCount", 4);
    
                        // columnCount
                        if ($layout_section['columnCount'] == 'auto') $mc_section->addChild("columnCount", -1);
                        else $mc_section->addChild("columnCount", $layout_section['columnCount']);
    
                        // questionCount
                        $mc_section->addChild("questionCount", count($scoring_section['Content']));
    
                        // answers
                        $answers = '';
                        foreach ($scoring_section['Content'] as $item) {
                            if (empty($item['ScoredResponses'])) {
                                if (array_key_exists('scoring', $item) && ($item['scoring'] == 'bonus')) $thisAnswers = '@_';
                                else $thisAnswers = '_';
                            } else $thisAnswers = $this->getXMLAnswers($item);
                            
                            if (strlen($thisAnswers) == 1) $answers .= $thisAnswers;
                            else $answers .= '(' . $thisAnswers . ')';
                        }
    
                        $mc_section->addChild("answers", $answers);
    
                        // outcomes
    
                        foreach ($scoring_data['Outcomes'] as $outcome) {
                            $match_numbers = array();
                            for ($i = 0; $i < count($scoring_section['Content']); ++$i) {
                                $itemID = $scoring_section['Content'][$i]['id'];
                                if (in_array($itemID, $outcome['item_ids'])) {
                                    $match_numbers[] = $i + 1;
                                }
                            }
                            if (!empty($match_numbers)) {
                                $category_p = $mc_section->addChild("category", implode(',', $match_numbers));
                                $category_p->addAttribute("type", $outcome['name']);
                            }
                        }
    
                        // numberingTypes
    
                        $itemNumbers = array();
                        foreach ($scoring_section['Content'] as $item) $itemNumbers[] = $item['number'];
                        $mc_section->addChild("numberingTypes", implode(',', $itemNumbers));
                                
                        // type
                        $mc_section->addChild("defaultType", 'Letter');
                                
                        // questionTypes
                        $itemTypes = array();
                        foreach ($scoring_section['Content'] as $item) {
                            if ($item['type'] == 'mc_truefalse') $thisType = 'T';
                            else if (empty($item['Answers'])) $thisType = 'd';
                            else $thisType = chr(96 + count($item['Answers']));
    
                            $itemTypes[] = $thisType;
                        }
                        $mc_section->addChild("questionTypes", implode('', $itemTypes));
    
                    } else if ($layout_section['type'] == 'nr') {
    
                        $nr_section = $d_page->addChild("nrSection");
    
                        // startNumber
                        $nr_section->addChild("startNumber", $startNumber);
    
                        // header
                        if (array_key_exists('header', $layout_section)) {
                            $nr_section->addChild("header", tohtml($layout_section['header']));
                        }
    
                        // columnCount
                        if ($layout_section['columnCount'] == 'auto') $nr_section->addChild("columnCount", -1);
                        else $nr_section->addChild("columnCount", $layout_section['columnCount']);
    
                        // digitCount
                        $nr_section->addChild("digitCount", 4);
    
                        // columnCount
                        $nr_section->addChild("nrStyle", $layout_section['fieldStyle']);
    
                        // questions
                        foreach ($scoring_section['Content'] as $item) {
                            $question_p = $nr_section->addChild("question");
    
                            // answerFormat
                            $format = ucfirst(substr($item['type'], 3));
                            $question_p->addChild("answerFormat", $format);
    
                            if (empty($format)) {
    
                                // No additional parameters for 'nr' type
    
                            } else if ($format == 'Selection') {
    
                                // ignoreOrder
                                if ($item['TypeDetails']['ignoreOrder']) {
                                    $question_p->addChild("ignoreOrder", "YES");
                                }
    
                                // markByDigits
                                if ($item['TypeDetails']['markByDigits']) {
                                    $question_p->addChild("markByDigits", "YES");
                                }
    
                                // allowPartialMatch
                                if ($item['TypeDetails']['partialValue'] != 0.0) {
                                    $question_p->addChild("partialValue", $item['TypeDetails']['partialValue']);
                                }
    
                            } else if ($format != 'Fraction'){
                    
                                // fudgeFactor
                                if ($item['TypeDetails']['fudgeFactor'] != 0.0) {
                                    $question_p->addChild("fudgeFactor", $item['TypeDetails']['fudgeFactor']);
                                }
                    
                                // factorOfTen
                                if ($item['TypeDetails']['tensValue'] != 0.0) {
                                    $question_p->addChild("factorOfTen", $item['TypeDetails']['tensValue']);
                                }
    
                                // sigDigsBehaviour
                                $question_p->addChild("sigDigsBehaviour", $item['TypeDetails']['sigDigsBehaviour']);
    
                                // badSigDigs
                                if ($item['TypeDetails']['sigDigsValue'] != 1.0) {
                                    $question_p->addChild("badSigDigs", $item['TypeDetails']['sigDigsValue']);
                                }
                            }
    
                            // answer
                            $question_p->addChild("answer", $this->getXMLAnswers($item));
    
                            // outcomes
    
                            foreach ($scoring_data['Outcomes'] as $outcome) {
                                if (in_array($item['id'], $outcome['item_ids'])) {
                                    $category_p = $question_p->addChild("category");
                                    $category_p->addAttribute("type", $outcome['name']);
                                }
                            }
    
                            // numberingType
                            $question_p->addChild("numberingType", $item['number']);
    
                            // digitCount
                            $question_p->addChild("digitCount", $item['TypeDetails']['nrDigits']);
                        }
    
                    } else if ($layout_section['type'] == 'wr') {
    
                        $wr_section = $d_page->addChild("wrSection");
    
                        // startNumber
                        $wr_section->addChild("startNumber", $startNumber);
    
                        // header
                        if (array_key_exists('header', $layout_section)) {
                            $wr_section->addChild("header", tohtml($layout_section['header']));
                        }
    
                        // columnCount
                        if ($layout_section['columnCount'] == 'auto') $wr_section->addChild("columnCount", -1);
                        else $wr_section->addChild("columnCount", $layout_section['columnCount']);
    
                        // includeSpace
                        if ($layout_section['includeSpace']) $wr_section->addChild("includeSpace", 'YES');
                        else $wr_section->addChild("includeSpace", 'NO');
    
                        // questions
                        foreach ($scoring_section['Content'] as $item) {
                            $question_p = $wr_section->addChild("question");
    
                            // prompt type
                            if (array_key_exists('prompt', $item['TypeDetails'])) {
                                $question_p->addChild("promptType", ucfirst($item['TypeDetails']['prompt']['type']));
    
                                if ($item['TypeDetails']['prompt']['type'] == 'text') {
                                    $question_p->addChild("promptText", tohtml($item['TypeDetails']['prompt']['text']));
                                } else if ($item['TypeDetails']['prompt']['type'] == 'image') {
                                    $question_p->addChild("promptImage", $item['TypeDetails']['prompt']['imageHash']);
                                    $question_p->addChild("promptHeight", $item['TypeDetails']['prompt']['height']);
                                }
                            }
    
                            // height
                            $question_p->addChild("height", $item['TypeDetails']['height']);
    
                            // criteria
                            if (!empty($item['TypeDetails']['criteria'])) {
                                $criteria = '';
                                foreach ($item['TypeDetails']['criteria'] as $entry) {
                                    if (strlen($criteria) > 0) $criteria .= ';';
                                    $criteria .= $entry['label'] . '=' . $entry['value'];
                                }
                                if (array_key_exists('scoring', $item)) {
                                    if ($item['scoring'] == 'omit') $criteria = '#' . $criteria;
                                    else if ($item['scoring'] == 'bonus') $criteria = '@' . $criteria;
                                }
    
                                $question_p->addChild("criteria", tohtml($criteria));
                            }
    
                            // outcomes
    
                            foreach ($scoring_data['Outcomes'] as $outcome) {
                                if (in_array($item['id'], $outcome['item_ids'])) {
                                    $category_p = $question_p->addChild("category");
                                    $category_p->addAttribute("type", $outcome['name']);
                                }
                            }
    
                            // numberingType
                            $question_p->addChild("numberingType", $item['number']);
                        }
                    }

                } else return false;
			}
		}
		
		return $xml_root->asXML();
	}

	public function checkLayoutData($json_data) {

        // Re-encode JSON data as an object, and read the schema

        $json_object = json_decode(json_encode($json_data), false);

        $schema_object = $this->getSchema('/schemas/document');
        if ($schema_object !== false) {

            // Validate with schema

            App::uses('Jsv4', 'Vendor');
            $schema_result = Jsv4::validate($json_object, $schema_object);
            unset($schema_object);
            gc_collect_cycles();

            if (!$schema_result->valid) {
                return json_encode($schema_result->errors);
            }

        } else {

            return "Error loading schema";

        }

		// Check report settings

        $isValid = true;

        if ($json_data['StudentReports']['responses'] == 'none') {
            if (array_key_exists('incorrectOnly', $json_data['StudentReports'])) $isValid = false;
        } else if (!array_key_exists('incorrectOnly', $json_data['StudentReports'])) $isValid = false;

        if ($json_data['StudentReports']['results'] == 'numerical') {
            if (array_key_exists('outcomesLevels', $json_data['StudentReports'])) $isValid = false;
        } else if (!array_key_exists('outcomesLevels', $json_data['StudentReports'])) $isValid = false;

		if (!$isValid) {
			return "Unexpected report settings in document";
		}

		return true;
	}

	public function beforeDelete($cascade = true) {
		$this->contain();
		$document = $this->findById($this->id);

		if (!empty($document['Document']['version_data_id'])) {
			$this->version_data_ids[$this->id] = $document['Document']['version_data_id'];
		} else {
			unset($this->version_data_ids[$this->id]);
		}

		return true;
	}

	public function afterDelete() {
		if (isset($this->version_data_ids[$this->id])) {
			$version_data_id = $this->version_data_ids[$this->id];
			$this->VersionData->clean($version_data_id);
		}
	}

	public function beforeSave($options = array()) {
	    if (!empty($this->data['Document']['folder_id'])) {
			$this->Folder->contain();
			$folder = $this->Folder->findById($this->data['Document']['folder_id']);
			if ($folder['Folder']['type'] !== 'forms') {
				$user_email = AuthComponent::user('email');

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Bad folder type in Document->save", $user_email, array(
					'folder ID' => $this->data['Document']['folder_id']
				));
				return false;
			}
		}

		$check_result = true;
		$layout_data = false;

		if (array_key_exists('id', $this->data['Document'])) {
			$document_id = $this->data['Document']['id'];
		} else $document_id = false;

		if (array_key_exists('layout_json', $this->data['Document'])) {
			$layout_data = json_decode($this->data['Document']['layout_json'], true);
			$check_result = $this->checkLayoutData($layout_data);
		}

		if ($check_result !== true) {
			$user_email = AuthComponent::user('email');
			if ($document_id === false) $error_data = array();
			else $error_data = array('id' => $document_id);
			$error_data['json_data'] = json_encode($layout_data);

			App::uses('Email', 'Lib');
			Email::queue_error("SmarterMarks: Validation failed in Document->save", $user_email, array(
				'result' => $check_result,
				'data' => $error_data
			));
			return false;
		}

		$section_match = true;

		if ($layout_data !== false) {
			$version_data_id = false;
			if (array_key_exists('version_data_id', $this->data['Document'])) {
				$version_data_id = $this->data['Document']['version_data_id'];
			} else if ($document_id !== false) {
				$this->contain();
				$document = $this->findById($document_id);
				if (!empty($document)) {
					$version_data_id = $document['Document']['version_data_id'];
				}
			}

			if ($version_data_id !== false) {
				$version_data = $this->VersionData->findById($version_data_id);
				if (!empty($version_data['VersionData']['scoring_json'])) {
					$scoring_data = json_decode($version_data['VersionData']['scoring_json'], true);

					$scoring_index = 0;
					foreach ($layout_data['Body'] as $layout_section) {
						if ($layout_section['type'] != 'page_break') {
							if ($scoring_index < count($scoring_data['Sections'])) {
								$scoring_section = $scoring_data['Sections'][$scoring_index++];
								if ($scoring_section['id'] != $layout_section['id']) {
									$section_match = false;
									break;	
								}
							} else {
								$section_match = false;
								break;
							}
						}
					}	
				} else $section_match = false;
			} else $section_match = false;
		}

		if (!$section_match) {
			$user_email = AuthComponent::user('email');

			App::uses('Email', 'Lib');
			Email::queue_error("SmarterMarks: Section mismatch in Document->save", $user_email, array(
				'layout data' => $layout_data,
				'scoring data' => $scoring_data
			));
			return false;
		}

    	return true;
	}

    public function afterSave($created, $options = array()) {

        // Update score caches

        $user_id = AuthComponent::user('id');
        $cache_data = Cache::read($user_id, 'report_data');
        if (!empty($cache_data)) {
            $this->contain('Score');
            $resultData = $this->findById($this->id);
            if (!empty($resultData['Score'])) {
                foreach ($resultData['Score'] as $thisData) {
                    $score_id = $thisData['id'];
                    unset($cache_data[$score_id]);
                }
            }
            Cache::write($user_id, $cache_data, 'report_data');
        }
	}
}

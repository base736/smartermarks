<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

define("USER_EXPIRED", 1);
define("USER_LOCKED", 2);

class User extends AppModel {
	var $actsAs = array('Containable');
	public $displayField = 'email';

	private $group_ids = array();

	public $filterArgs = array(
    	'search' => array('type' => 'like', 'field' => array('email', 'name', 'school')),
    );
 
	public $hasMany = array(
		'Document' => array(
			'className' => 'Document',
			'foreignKey' => 'user_id'
		),
		'Assessment' => array(
			'className' => 'Assessment',
			'foreignKey' => 'user_id'
		),
		'UserQuestion' => array(
			'className' => 'UserQuestion',
			'foreignKey' => 'user_id'
		),
		'Score' => array(
			'className' => 'Score',
			'foreignKey' => 'user_id'
		),
		'Sitting' => array(
			'className' => 'Sitting',
			'foreignKey' => 'user_id',
			'dependent' => false
		),
		'SittingStudent' => array(
			'className' => 'SittingStudent',
			'foreignKey' => 'user_id',
			'dependent' => true
		),
		'CommunityUser' => array(
			'className' => 'CommunityUser',
			'foreignKey' => 'user_id'
		),
		'PasswordReset' => array(
			'className' => 'PasswordReset',
			'foreignKey' => 'user_id',
			'dependent' => true
		),
		'UserGroup' => array(
			'className' => 'UserGroup',
			'foreignKey' => 'user_id',
			'dependent' => true
        ),
        'ClassList' => array(
			'className' => 'ClassList',
			'foreignKey' => 'user_id',
			'dependent' => true
        ),
        'StyleSheet' => array(
			'className' => 'StyleSheet',
			'foreignKey' => 'user_id',
			'dependent' => true
        )
	);

	public $validate = array(
		'email' => array(
			'notempty' => array(
				'rule' => array('notblank'),
				'message' => "Email Cannot Be Blank"
			), 
		)
	);

	public function get_auth_details($email) {
		$secrets = Configure::read('secrets');

		$auth_methods = $secrets['email_auth_methods'];
		$auth_details = false;
		foreach ($auth_methods as $entry) {
			if (preg_match($entry['regex'], $email) === 1) {
				$auth_details = $entry;
				break;
			}
		}

		if ($auth_details === false) {
			$auth_details = array('auth_method' => 'local');
		}

		return $auth_details;
	}

	public function getEntropy($text) {
		$groups = array(
			"abcdefghijklmnopqrstuvwxyz",  // Lowercase letters
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ",  // Uppercase letters
			"0123456789"                   // Digits
		);
		$special = "_.-!@*$?^%";           // Most common special characters
		$allSpecialCount = 33;             // Count of all special characters

		$included = array();
		for ($i = 0; $i < strlen($text); ++$i) {
			for ($j = 0; $j < count($groups); ++$j) {
				if (strpos($groups[$j], $text[$i]) !== false){
					break;
				}
			}
			if ($j < count($groups)) {
				$included[$j] = strlen($groups[$j]);
			} else {
				$added = strpos($special, $text[$i]);
				if ($added === false) $added = $allSpecialCount;
				else $added++;
				
				if (!isset($included['special']) || ($added > $included['special'])) {
					$included['special'] = $added;								
				}
			}
		}

		$poolSize = 0;
		foreach ($included as $key => $added) {
			$poolSize += $added;
		}

		return strlen($text) * log($poolSize, 2);
	}

	public function isExpired($id = null) {
		$flags = 0;

		$this->contain();
		$user = $this->findById($id);
		if ($user && !empty($user['User']['expires'])) {
			if (!empty($user['User']['locked'])) {
				if (strtotime($user['User']['locked']) < strtotime('now')) $flags |= USER_LOCKED;
				if (strtotime($user['User']['expires']) < strtotime('now')) $flags |= USER_EXPIRED;
			} else if (strtotime($user['User']['expires']) < strtotime('now')) {
				$flags |= USER_LOCKED;
				$flags |= USER_EXPIRED;
			}
		}

		return $flags;
	}
		
	public function send_validation($id = null) {
	
		$this->contain();
		$user = $this->findById($id);
		if ($user) {
			$auth_details = $this->get_auth_details($user['User']['email']);
			$user['User']['auth_method'] = $auth_details['auth_method'];

			$token = bin2hex(random_bytes(16));

			$data = array();
			$data['id'] = $id;
			$data['validation_token'] = $token;

			if (!empty($this->save($data))) {
				App::uses('Email', 'Lib');
				if (!empty($user['User']['last_login']) || !empty($user['User']['validation_token'])) {
					Email::send($user['User']['email'], "SmarterMarks: Email verification required", "validation_email", 
						array('userData' => $user['User'], 'token' => $token));
				} else if ($user['User']['role'] == 'teacher') {
					Email::send($user['User']['email'], "Welcome to SmarterMarks!", "welcome_batch", 
						array('userData' => $user['User'], 'token' => $token));
				} else if ($user['User']['role'] == 'student') {
					Email::send($user['User']['email'], "Welcome to SmarterMarks!", "welcome_student", 
						array('userData' => $user['User'], 'token' => $token));
				}
			}
		}
	}
	
	public function getDefaults() {
		$defaults = array(
			'Indices' => array(
				'sort' => 'created'
			),
			'Common' => array(
				'Settings' => array(
					'pageSize' => 'letter',
					'marginSizes' => array(
						'top' => 0.75,
						'bottom' => 0.75,
						'left' => 0.75,
						'right' => 0.75
					),
					'language' => 'English'
				),
				'Title' => array(
					'logo' => array(
						'imageHash' => false,
						'height' => 0.75
					),
					'nameLines' => ['Name']
				)
			),
			'Document' => array(
				'Settings' => array(
					'version' => 2,
					'copiesPerPage' => 1,
					'formLocation' => 'none'
				),
				'Title' => array(
					'idField' => array(
						'show' => false,
						'label' => 'Student ID',
						'bubbles' => 6
					),
					'nameField' => array(
						'show' => true,
						'label' => 'Name',
						'bubbles' => 15
					),
				),
				'Sections' => array(
					'columnCount' => 'auto',
					'mc_choiceCount' => 4,
					'wr_includeSpace' => true
				),
				'Reports' => array(
					'Student' => array(
						'reportsPerPage' => 'default',
                        'weights' => 'none',
                        'outcomes' => 'numbers',
                        'responses' => 'responses',
                        'incorrectOnly' => false,
                        'results' => 'numerical',
						'outcomesLevels' => array(
							array('name' => 'Needs help',    'from' => 0),
							array('name' => 'Getting there', 'from' => 60),
							array('name' => 'Proficient',    'from' => 75),
							array('name' => 'Excellent',     'from' => 90)
						)
					),
					'Teacher' => array(
						'graph' => true,
						'minorgraphs' => true,
						'meansd' => true,
						'median' => true,
						'cronbach' => true,
						'collusion' => true,
						'categories' => true,
						'average' => true,
						'pearson' => true,
						'responses' => true,
						'experimental' => false,
						'itemsort' => 'question',
						'reportorder' => 'scan',
						'reportlayout' => 'default',
						'outcomesort' => 'alpha'
					)
				)
			),
			'Assessment' => array(
				'Settings' => array(
					'numberingType' => 'Numbers',
					'wrResponseLocation' => 'form',
					'mcFormat' => 'Auto'
				)
			),
			'Question' => array(
				'Engine' => array(
					'sigDigsType' => 'SigDigs',
					'sigDigs' => 3
				),
				'TypeDetails' => array(
					'nr' => array(
						'type' => 'nr_decimal',
						'nrDigits' => 4,
						'promptType' => 'simple',

						'fudgeFactor' => 0.0,
						'tensValue' => 0.0,
						'sigDigsBehaviour' => 'Zeros',
						'sigDigsValue' => 1.0,

						'markByDigits' => false,
						'partialValue' => 0.0,
						'ignoreOrder' => false,

					),
					'wr' => array(
						'height' => 1.0,
						'criteria' => array(
							array('label' => 'Score', 'value' => 5)
						)
					)
				)
			)
		);

		return json_encode($defaults);
	}

	public function checkDefaults($json_data) {
        
        // Re-encode JSON data as an object, and read the schema

        $json_object = json_decode(json_encode($json_data), false);

        $schema_object = $this->getSchema('/schemas/user_defaults');
        if ($schema_object !== false) {

            // Validate with schema

            App::uses('Jsv4', 'Vendor');
            $schema_result = Jsv4::validate($json_object, $schema_object);
            unset($schema_object);
            gc_collect_cycles();

            if (!$schema_result->valid) {
                return json_encode($schema_result->errors);
            }

        } else {

            return "Error loading schema";

        }

		return true;
	}

	public function beforeSave($options = array()) {
	    if (!empty($this->data['User']['defaults'])) {
			$defaults = json_decode($this->data['User']['defaults'], true);
			$check_result = $this->checkDefaults($defaults);
			if ($check_result !== true) {
				$user_email = AuthComponent::user('email');
				if (!array_key_exists('id', $this->data['User'])) $error_data = array();
				else $error_data = array('id' => $this->data['User']['id']);
				$error_data['json_data'] = json_encode($defaults);

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Validation failed in User->save", $user_email, array(
					'result' => $check_result,
					'data' => $error_data
				));
				return false;
			}
		}
		if (!empty($this->data['User']['password'])) {
    	    $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		}
		if (!empty($this->data['User']['group_id'])) {
			$this->group_ids[$this->id] = $this->data['User']['group_id'];
		}

    	return true;
	}

	public function afterSave($created, $options = array()) {
		if (!empty($this->group_ids[$this->id])) {
			$this->Group->clean($this->group_ids[$this->id]);
		}
		unset($this->group_ids[$this->id]);

		Cache::delete($this->id, 'report_data');
	}

	public function beforeDelete($cascade = true) {
		$this->contain();		
		$user = $this->findById($this->id);

		if ($cascade && !empty($user['User']['folder_ids'])) {
			$folder_ids = json_decode($user['User']['folder_ids'], true);

			$folderModel = ClassRegistry::init('Folder');
			foreach ($folder_ids as $type => $data) {
				if (is_array($data)) {
					foreach ($data as $subtype => $id) {
						$folderModel->delete($id);
					}					
				} else $folderModel->delete($data);
			}
		}
				
		return true;
	}
}

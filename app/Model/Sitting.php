<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

/**
 * Sitting Model
 */

class Sitting extends AppModel {

	var $actsAs = array('Containable');
	public $displayField = 'name';

	public $filterArgs = array(
    	'search' => array('type' => 'like', 'field' => array('name')),
    );

	private $result_set_ids = array();
	private $version_data_ids = array();

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		), 
		'Assessment' => array(
			'className' => 'Assessment',
			'foreignKey' => 'assessment_id'
		),
		'VersionData' => array(
			'className' => 'VersionData',
			'foreignKey' => 'version_data_id'
		),
		'ResultSet' => array(
			'className' => 'ResultSet',
			'foreignKey' => 'result_set_id'
		),
		'Folder' => array(
			'className' => 'Folder',
			'foreignKey' => 'folder_id',
		)
	);

	public $hasMany = array(
		'SittingStudent' => array(
			'className' => 'SittingStudent',
			'foreignKey' => 'sitting_id',
			'dependent' => true
		)
	);

	public function getPermissions($sitting_id, $user_id) {
		if (empty($sitting_id) || !is_numeric($sitting_id)) return array();
		if (empty($user_id) || !is_numeric($user_id)) return array();

		$this->contain();
		$sitting = $this->findById($sitting_id);

		$permissions = array();
		if (!empty($sitting)) {			
			$folderModel = ClassRegistry::init('Folder');
			$folderPermissions = $folderModel->getContentPermissions($sitting['Sitting']['folder_id'], $user_id);
			if ($user_id == $sitting['Sitting']['user_id']) $permissions = $folderPermissions['owner'];
			else $permissions = $folderPermissions['all'];
		}
		
		return $permissions;
	}

	public function copyToFolder($sitting_id, $folder_id, $user_id) {
		$success = true;
		$resp = array();

		$this->contain('ResultSet', 'VersionData');
		$sitting = $this->findById($sitting_id);

		// Update sitting values

		$old_folder_id = $sitting['Sitting']['folder_id'];
		$old_assessment_id = $sitting['Sitting']['assessment_id'];
		$old_version_data_id = $sitting['Sitting']['version_data_id'];

		$sitting['Sitting']['assessment_id'] = null;
		$sitting['Sitting']['version_data_id'] = null;

		unset($sitting['Sitting']['id']);
		unset($sitting['Sitting']['created']);

		$sitting['Sitting']['user_id'] = $user_id;
		$sitting['Sitting']['folder_id'] = $folder_id;
		$sitting['Sitting']['moved'] = date('Y-m-d H:i:s');

		// Build a new result set

		$data = array(
			'user_id' => $user_id,
			'source_model' => 'Sitting'
		);
		$this->ResultSet->create();
		$success &= !empty($this->ResultSet->save($data));

		// Build sitting with no assessment_id or version_data_id

		if ($success) {
			$sitting['Sitting']['is_new'] = 1;
			$sitting['Sitting']['result_set_id'] = $this->ResultSet->id;
			$sitting['Sitting']['join_token'] = $this->getJoinToken();

			$this->create();
			if (!empty($this->save($sitting['Sitting']))) {
				$new_sitting_id = $this->id;
			} else $success = false;
		}

		// Create a copy of the assessment with handles that point at the new sitting

		if ($success && ($old_assessment_id !== null)) {

			$handleData = array();
			$handleData['sitting_id'] = $new_sitting_id;

			$assessmentModel = ClassRegistry::init('Assessment');
			$result = $assessmentModel->copy($old_assessment_id, $handleData);
			$success &= $result['success'];
	
			// Attach the new copy to the sitting

			$new_assessment_id = $result['copyID'];

			$data = array();
			$data['id'] = $new_sitting_id;
			$data['assessment_id'] = $new_assessment_id;
			$success &= !empty($this->save($data));
		}

		// Build version data that references new handles

		if ($success && ($old_version_data_id !== null)) {

			// Get a map from old QuestionHandle IDs to new QuestionHandle IDs
			
			$assessmentItemModel = ClassRegistry::init('AssessmentItem');

			$assessmentItemModel->contain('QuestionHandle');
			$old_map = $assessmentItemModel->find('list', array(
				'fields' => array('QuestionHandle.id', 'QuestionHandle.question_id'),
				'conditions' => array('AssessmentItem.assessment_id' => $old_assessment_id)
			));

			$assessmentItemModel->contain('QuestionHandle');
			$new_map = $assessmentItemModel->find('list', array(
				'fields' => array('QuestionHandle.question_id', 'QuestionHandle.id'),
				'conditions' => array('AssessmentItem.assessment_id' => $new_assessment_id)
			));

			$handle_map = array();
			foreach ($old_map as $old_handle_id => $question_id) {
				if (array_key_exists($question_id, $new_map)) {
					$new_handle_id = intval($new_map[$question_id]);
					$handle_map[intval($old_handle_id)] = $new_handle_id;
				} else $success = false;
			}

			if ($success) {

				// Translate scoring data to new handles

				$new_scoring = json_decode($sitting['VersionData']['scoring_json'], true);
				foreach ($new_scoring['Sections'] as &$pSection) {
					foreach ($pSection['Content'] as &$pEntry) {
						if (array_key_exists('Source', $pEntry)) {
							$old_question_handle_id = $pEntry['Source']['question_handle_id'];
							if (array_key_exists($old_question_handle_id, $handle_map)) {
								$new_question_handle_id = $handle_map[$old_question_handle_id];
								$pEntry['Source']['question_handle_id'] = $new_question_handle_id;
							} else $success = false;
						}
					}
				}
	
				// Translate HTML to new handles

				if (!empty($sitting['VersionData']['html'])) {
					$new_html = $sitting['VersionData']['html'];
				} else $new_html = gzdecode($sitting['VersionData']['html_gzip']);

				$handle_regex = '/data-question-handle-id="(\d+)"/';
				$new_html = preg_replace_callback($handle_regex, function($match) use (&$handle_map, &$success) {
					$old_question_handle_id = $match[1];
					if (array_key_exists($old_question_handle_id, $handle_map)) {
						$new_question_handle_id = $handle_map[$old_question_handle_id];
						return 'data-question-handle-id="' . $new_question_handle_id . '"';
					} else $success = false;
				}, $new_html);

			}

			if ($success) {

				// Create VersionData and attach to sitting
					
				$data = array(
//					'html' => $new_html,
					'html_gzip' => gzencode($new_html),
					'scoring_json' => json_encode($new_scoring)
				);

				$this->VersionData->create();
				if (!empty($this->VersionData->save($data))) {
					$data = array();
					$data['id'] = $new_sitting_id;
					$data['version_data_id'] = $this->VersionData->id;
					$success &= !empty($this->save($data));
				} else $success = false;
			}
		}

		$resp['success'] = $success;
		if ($success) {
			$resp['sitting_id'] = $new_sitting_id;
			$resp['source_folder_id'] = $old_folder_id;
			$resp['target_folder_id'] = $folder_id;
		}
		
		return $resp;
    }

	public function getCache($sitting_id) {
		$data = Cache::read($sitting_id, 'sittings');
		if ($data === false) {
			$data = array();

			$this->contain(array('ResultSet', 'VersionData'));
			$sitting = $this->findById($sitting_id);
			if (empty($sitting)) return false;

			$data['Sitting'] = $sitting['Sitting'];
			$data['Sitting']['json_data'] = json_decode($data['Sitting']['json_data'], true);

			$assessmentModel = ClassRegistry::init('Assessment');
			$assessment_id = $sitting['Sitting']['assessment_id'];

			// Get assessment version if available

			if ($sitting['Sitting']['version_data_id'] != null) {
				if (!empty($sitting['VersionData']['html'])) {
					$html = $sitting['VersionData']['html'];
				} else $html = gzdecode($sitting['VersionData']['html_gzip']);

				$data['Version'] = array(
					'version_data_id' => $sitting['Sitting']['version_data_id'],
					'html' => $html,
					'scoring_data' => json_decode($sitting['VersionData']['scoring_json'], true)
				);
			}

			// Get and thin assessment data
			
			$assessmentModel->contain();
			$assessment = $assessmentModel->findById($assessment_id);
			$assessment_data = json_decode($assessment['Assessment']['json_data'], true);

			// Get question data and thin to improve assessment security

			$assessmentItemModel = ClassRegistry::init('AssessmentItem');
			$assessmentItemModel->contain(array('QuestionHandle' => 'Question'));
			$assessment_items = $assessmentItemModel->find('all', array(
				'conditions' => array('AssessmentItem.assessment_id' => $assessment_id)
			));

			$question_data = array();
			$secrets = array(
				'Assessment' => array(
					'Outcomes' => $assessment_data['Outcomes'],
					'Scoring' => $assessment_data['Scoring']
				),
				'Questions' => array()
			);

			unset($assessment_data['Outcomes']);
			unset($assessment_data['Scoring']);

			$allow_multiple_handles = array();
			$allow_negative_handles = array();

			$questionModel = ClassRegistry::init('Question');
			foreach ($assessment_items as $item) {
				$question_handle_id = intval($item['QuestionHandle']['id']);
				$question_id = intval($item['QuestionHandle']['Question']['id']);

				$json_data = json_decode($item['QuestionHandle']['Question']['json_data'], true);
				if (!empty($json_data['Engine']['Imports'])) {
					$import_ids = $json_data['Engine']['Imports'];
					$json_data['Engine']['Imports'] = $questionModel->expandImports($import_ids);
				}
	
				$hasCode = !empty($json_data['Engine']['js']);

				// Remove code wizard if it's here

				unset($json_data['Engine']['data']);
				foreach ($json_data['Engine']['Imports'] as $key => &$pEntry) {
					unset($pEntry['name']);
					unset($pEntry['description']);
					unset($pEntry['details']);
				}

				// Shuffle answers

				if (!empty($json_data['Common']['Answers'])) {
					if ($json_data['Common']['shuffleAnswers']) shuffle($json_data['Common']['Answers']);
				}

				$secrets['Questions'][$question_handle_id] = array(
					'name' => $item['QuestionHandle']['name_alias'],
					'Parts' => array()
				);

				foreach ($json_data['Parts'] as $index => &$pPart) {
                    $pPart['index'] = $index;
                    
					if ($pPart['type'] != 'context') {

						// Augment assessment data with information needed for response interface
	
						if (substr($pPart['type'], 0, 2) == 'mc') {
							if (!empty($pPart['TypeDetails']['shuffleAnswers'])) {
								if (!empty($pPart['Answers'])) shuffle($pPart['Answers']);
							}

							if (array_key_exists('ScoredResponses', $pPart)) {
								foreach ($pPart['ScoredResponses'] as $entry) {
									if (array_key_exists('ids', $entry)) {
										$allow_multiple_handles[] = $question_handle_id;
									}
								}
							}
							
						} else if (substr($pPart['type'], 0, 2) == 'nr') {
							if (!$hasCode) {
								$numDigits = $pPart['TypeDetails']['nrDigits'];
	
								// Make sure correct answer fits
					
								if (isset($pPart['ScoredResponses'])) {
									$bestValue = 0.0;
									foreach ($pPart['Answers'] as $answer) {
										$value = 0.0;
										foreach ($pPart['ScoredResponses'] as $keyEntry) {
											if ($keyEntry['id'] == $answer['id']) $value = $keyEntry['value'];
										}
										if ($value > $bestValue) {
											$bestValue = $value;
											$numDigits = 0;
											$answerText = $answer['template'];
											for ($i = 0; $i < strlen($answerText); ++$i) {
												if (is_numeric($answerText[$i])) $numDigits++;
											}
										}
									}
								}
	
								$pPart['num_digits'] = $numDigits;
	
								$maxExponent = 0;
								$isNegative = 0;
								$expNegative = 0;
								foreach ($pPart['Answers'] as $answer) {
									$answerText = $answer['template'];
									$parts = preg_split('/[eE]/', $answerText);
	
									$coefficient = $parts[0];
									if ((strlen($coefficient) > 0) && ($coefficient[0] == '-')) $isNegative = 1;
	
									if (count($parts) == 2) {
										$exponent = $parts[1];
										if ($exponent[0] == '-') {
											$expNegative = 1;
											$exponent = substr($exponent, 1);
										}
										if (strlen($exponent) > $maxExponent) {
											$maxExponent = strlen($exponent);
										}
									}
								}
	
								if ($pPart['type'] == 'nr_scientific') {
									if ($maxExponent > 0) {
										$pPart['num_exponent'] = $maxExponent;
										$pPart['is_negative'] = $isNegative;
										$pPart['exp_negative'] = $expNegative;
									}
								} else if ($isNegative) {
									$allow_negative_handles[] = $question_handle_id;
								}
							}
						}
	
						// Save elements we'll use to flesh out the layout later
	
						$part_id = $pPart['id'];
	
						$secrets['Questions'][$question_handle_id]['Parts'][$part_id] = array();
						
						if (!$hasCode && array_key_exists('Rubric', $pPart)) {
							$secrets['Questions'][$question_handle_id]['Parts'][$part_id]['Rubric'] = $pPart['Rubric'];
							unset($pPart['Rubric']);
						}

						if (array_key_exists('ScoredResponses', $pPart)) {
							$secrets['Questions'][$question_handle_id]['Parts'][$part_id]['ScoredResponses'] = $pPart['ScoredResponses'];
							unset($pPart['ScoredResponses']);
						}
	
						// Cache answers in secrets if required. Never need to do this with answers in
						// context because those only exist if this is a matching question, which is
						// all multiple choice (so needs answers in the thinned assessment).
	
						if (!$hasCode && (substr($pPart['type'], 0, 2) == 'nr')) {
							$secrets['Questions'][$question_handle_id]['Parts'][$part_id]['Answers'] = $pPart['Answers'];
							unset($pPart['Answers']);
						}
					}
				}

				$question_data[] = array(
					'id' => $question_id,
					'question_handle_id' => $question_handle_id,
					'json_data' => $json_data
				);
			}

 			// Add allow_multiple and allow_negative flags to assessment sections

			foreach ($assessment_data['Sections'] as &$pSection) {
				if ($pSection['type'] == 'page_break') continue;
				
				$pSection['allow_multiple'] = false;
				$pSection['allow_negative'] = false;
				foreach ($pSection['Content'] as $entry) {
					if ($entry['type'] == 'item') {
						$question_handle_ids = array($entry['question_handle_id']);
					} else if ($entry['type'] == 'item_set') {
						$question_handle_ids = $entry['question_handle_ids'];
					} else $question_handle_ids = array();

					foreach ($question_handle_ids as $question_handle_id) {
						if (array_search($question_handle_id, $allow_multiple_handles) !== false) {
							$pSection['allow_multiple'] = true;
						}
						if (array_search($question_handle_id, $allow_negative_handles) !== false) {
							$pSection['allow_negative'] = true;
						}
					}
				}
			}

			// Store assessment data

			$data['Assessment'] = $assessment_data;
			$data['Questions'] = $question_data;
			$data['Secrets'] = $secrets;

			Cache::write($sitting_id, $data, 'sittings');
		}

		return $data;
	}

	public function create_version($sitting_id, $html, $layoutSections) {
		$sitting_cache = $this->getCache($sitting_id);
		$success = ($sitting_cache !== false);

		if ($success) {

			// Add secrets back to assessment data

			$secrets = $sitting_cache['Secrets'];

			$assessment_data = $sitting_cache['Assessment'];
			foreach ($secrets['Assessment'] as $key => $value) {
				$assessment_data[$key] = $value;
			}

			// Add secrets and scoring information to layout sections

            if (!empty($sitting_cache['Sitting']['json_data']['Scoring']['tweaks'])) {
    			$scoring = $sitting_cache['Sitting']['json_data']['Scoring']['tweaks'];
            } else $scoring = array();

			foreach ($layoutSections as &$pSection) {
				foreach ($pSection['Content'] as &$pQuestion) {
					$question_handle_id = $pQuestion['Source']['question_handle_id'];
					$part_id = $pQuestion['Source']['part_id'];
	
					if (isset($secrets['Questions'][$question_handle_id]['Parts'][$part_id])) {

						// Copy secrets

						$partSecrets = $secrets['Questions'][$question_handle_id]['Parts'][$part_id];
						foreach ($partSecrets as $key => $value) {
                            if ($key == 'Answers') {

                                $pQuestion['Answers'] = array();
                                foreach ($value as $secrets_answer) {
                                    $pQuestion['Answers'][] = array(
                                        'id' => $secrets_answer['id'],
                                        'response' => $secrets_answer['template']
                                    );
                                }

                            } else if ($key == 'Rubric') {

                                $rubric_html = false;
                                if ($value['type'] == 'structured') {

                                    // Render rubric

                                    for ($j = 0; $j < count($pQuestion['TypeDetails']['criteria']); ++$j) {
                                        $rubric_html .= 
                                            '<div class="rubric_row_wrapper">';
                                        
                                        if (count($pQuestion['TypeDetails']['criteria']) > 1) {
                                            $rubric_html .= 
                                                '<div class="rubric_row_title">' .
                                                    '<div>' . $pQuestion['TypeDetails']['criteria'][$j]['label'] . '</div>' .
                                                '</div>';
                                        }
                            
                                        $rubric_html .= '<div class="rubric_row_columns">';
                            
                                        if (count($pQuestion['TypeDetails']['criteria']) > 1) {
                                            $rubric_html .= 
                                                    '<div class="rubric_column_wrapper">';
                                            $rubric_html .= 
                                                        '<div class="rubric_column_title">Focus: When marking <i>' . $pQuestion['TypeDetails']['criteria'][$j]['label'] . '</i>, the marker should consider:</div>' .
                                                        '<div class="template_view">' . $value['rows'][$j]['focus'] . '</div>';
                                            $rubric_html .= 
                                                    '</div>';
                                        }
                            
                                        $sorted_scoring = $value['rows'][$j]['scoring'];
                                        usort($sorted_scoring, function ($a, $b) {
                                            return $b['value'] - $a['value'];
                                        });

                                        for ($k = 0; $k < count($sorted_scoring); ++$k) {
                            
                                            $marks = $sorted_scoring[$k]['value'];
                                            $marks_text = strval($marks) . ' marks';
                                            if ($marks == 0) $marks_text = 'No marks';
                                            else if ($marks == 1) $marks_text = '1 mark';
                            
                                            $rubric_html .= 
                                                    '<div class="rubric_column_wrapper">';
                                            $rubric_html .= 
                                                        '<div class="rubric_column_title"><u>' . $marks_text . '</u></div>' .
                                                        '<div class="template_view">' . $sorted_scoring[$k]['description'] . '</div>';
                                            $rubric_html .= 
                                                    '</div>';
                                        }
                            
                                        $rubric_html .= 
                                                '</div>' .
                                            '</div>';
                                    }

                                } else if ($value['type'] == 'free') {

                                    // Copy template to rubric HTML
                                
                                    $rubric_html = $value['template'];

                                }

                                $pQuestion['Rubric'] = array(
                                    'html' => $rubric_html
                                );

                            } else {

                                $pQuestion[$key] = $value;

                            }
						}
					}

					// Copy scoring

                    if (!empty($scoring)) {

                        $content_id = false;
                        foreach ($assessment_data['Sections'] as $section) {
                            if ($section['type'] == 'page_break') continue;
                            foreach ($section['Content'] as $entry) {
                                if ($entry['type'] == 'item') {
                                    if ($question_handle_id == $entry['question_handle_id']) {
                                        $content_id = $entry['id'];
                                    }
                                } else if ($entry['type'] == 'item_set') {
                                    if (in_array($question_handle_id, $entry['question_handle_ids'])) {
                                        $content_id = $entry['id'];
                                    }
                                }		
                            }
                        }
    
                        $part_index = false;
                        foreach ($sitting_cache['Questions'] as $question) {
                            if ($question_handle_id == $question['question_handle_id']) {
                                foreach ($question['json_data']['Parts'] as $index => $part) {
                                    if ($part['id'] == $part_id) $part_index = $index;
                                }
                            }
                        }
    
                        if (($content_id !== false) && ($part_index !== false)) {
                            foreach ($scoring as $entry) {
                                if (($entry['content_id'] == $content_id) && ($entry['part_index'] == $part_index)) {
                                    $pQuestion['scoring'] = $entry['type'];
                                }
                            }    
                        } else $success = false;

                    }
				}
			}

			// Finish and save version data

            $handle_data = array(
                'sitting_id' => $sitting_id
            );

            $scoring_data = $layoutSections;

			$success &= $this->VersionData->finishVersion($scoring_data, $html,
				$handle_data, $assessment_data, $sitting_cache['Sitting']['user_id']);

			if ($success) {
				$data = array(
//					'html' => $html,
					'html_gzip' => gzencode($html),
					'scoring_json' => json_encode($scoring_data)
				);
	
				$this->VersionData->create();
				$success &= !empty($this->VersionData->save($data));
			}
		}

		$resp = array('success' => $success);

		if ($success) {
			$resp['scoring_data'] = $scoring_data;
			$resp['version_data_id'] = $this->VersionData->id;
		}

		return $resp;
	}

	public function getJoinToken() {
		$characters = "ABCDEFGHJKMNPQRSTUVWXYZ23456789";
		$tokenLength = 5;

		$isNew = false;
		while (!$isNew) {
			$joinToken = '';
			for ($i = 0; $i < $tokenLength; ++$i) {
				$joinToken .= $characters[rand(0, strlen($characters) - 1)];
			}
			$isNew = empty($this->findByJoinToken($joinToken));
		}

		return $joinToken;
	}

	public function get_account_data($sitting_id) {
		$this->SittingStudent->contain('User');
		$raw_data = $this->SittingStudent->find('all', array(
			'conditions' => array(
				'SittingStudent.sitting_id' => $sitting_id,
				'SittingStudent.user_id IS NOT NULL',
				'SittingStudent.deleted' => null
			)
		));

		$student_data = array();
		foreach ($raw_data as $sitting_student) {
			$sittingStudent_id = intval($sitting_student['SittingStudent']['id']);

			$thisEntry = array(
				'id' => $sittingStudent_id,
				'user_id' => intval($sitting_student['SittingStudent']['user_id']),
				'started' => $sitting_student['SittingStudent']['started']
			);

			if (strlen($sitting_student['SittingStudent']['details']) == 0) $thisEntry['details'] = array();
			else $thisEntry['details'] = json_decode($sitting_student['SittingStudent']['details'], true);

			$thisEntry['details']['Email'] = $sitting_student['User']['email'];
			$thisEntry['validated'] = empty($sitting_student['User']['validation_token']);

			if ($thisEntry['validated']) $thisEntry['bounced'] = false;
			else if ($sitting_student['User']['email_bounced']) $thisEntry['bounced'] = true;
			else $thisEntry['bounced'] = false;
	
			$student_data[] = $thisEntry;
		}
		return $student_data;
	}

	public function beforeDelete($cascade = true) {
		$this->contain();
		$sitting = $this->findById($this->id);

		if (!empty($sitting['Sitting']['result_set_id'])) {
			$this->result_set_ids[$this->id] = $sitting['Sitting']['result_set_id'];
		} else {
			unset($this->result_set_ids[$this->id]);
		}

		if (!empty($sitting['Sitting']['version_data_id'])) {
			$this->version_data_ids[$this->id] = $sitting['Sitting']['version_data_id'];
		} else {
			unset($this->version_data_ids[$this->id]);
		}

		return true;
	}

	public function afterDelete() {
		Cache::delete($this->id, 'sittings');

		if (isset($this->result_set_ids[$this->id])) {
			$result_set_id = $this->result_set_ids[$this->id];

			$resultDataModel = ClassRegistry::init('ResultData');
			$resultCount = $resultDataModel->find('count', array(
				'conditions' => array('ResultData.result_set_id' => $result_set_id)
			));

			$questionHandleModel = ClassRegistry::init('QuestionHandle');
			if ($resultCount == 0) {
				$questionHandleModel->deleteAll(array('QuestionHandle.sitting_id' => $this->id), true, true);
			} else {
				$questionHandleModel->updateAll(array('QuestionHandle.sitting_id' => null),
					array('QuestionHandle.sitting_id' => $this->id));
			}
			unset($this->result_set_ids[$this->id]);
		}

		if (isset($this->version_data_ids[$this->id])) {
			$version_data_id = $this->version_data_ids[$this->id];
			$this->VersionData->clean($version_data_id);
		}
	}

	public function beforeSave($options = array()) {
	    if (!empty($this->data['Sitting']['folder_id'])) {
			$this->Folder->contain();
			$folder = $this->Folder->findById($this->data['Sitting']['folder_id']);
			if ($folder['Folder']['type'] !== 'sittings') {
				$user_email = AuthComponent::user('email');

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Bad folder type in Sitting->save", $user_email, array(
					'folder ID' => $this->data['Sitting']['folder_id']
				));
				return false;
			}
		}
	    if (!empty($this->data['Sitting']['json_data'])) {
			$json_data = json_decode($this->data['Sitting']['json_data'], true);
			$check_result = $this->checkData($json_data);
			if ($check_result !== true) {
				$user_email = AuthComponent::user('email');
				if (!array_key_exists('id', $this->data['Sitting'])) $error_data = array();
				else $error_data = array('id' => $this->data['Sitting']['id']);
				$error_data['json_data'] = json_encode($json_data);

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Validation failed in Sitting->save", $user_email, array(
					'result' => $check_result,
					'data' => $error_data
				));
				return false;
			}
		}

    	return true;
	}

	public function afterSave($created, $options = array()) {
		Cache::delete($this->id, 'sittings');
	}

	public function checkData($json_data) {

        // Re-encode JSON data as an object, and read the schema

        $json_object = json_decode(json_encode($json_data), false);

        $schema_object = $this->getSchema('/schemas/sitting');
        if ($schema_object !== false) {

            // Validate with schema

            App::uses('Jsv4', 'Vendor');
            $schema_result = Jsv4::validate($json_object, $schema_object);
            unset($schema_object);
            gc_collect_cycles();

            if (!$schema_result->valid) {
                return json_encode($schema_result->errors);
            }

        } else {

            return "Error loading schema";

        }

		return true;
	}
}

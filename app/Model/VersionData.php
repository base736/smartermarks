<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class VersionData extends AppModel {
	var $actsAs = array('Containable');

	public $useTable = 'version_data';

	public $hasMany = array(
		'Document' => array(
			'className' => 'Document',
			'foreignKey' => 'version_data_id',
			'dependent' => true
		),
		'ResultData' => array(
			'className' => 'ResultData',
			'foreignKey' => 'version_data_id',
			'dependent' => true
		),
		'Sitting' => array(
			'className' => 'Sitting',
			'foreignKey' => 'version_data_id',
			'dependent' => true
		)
	);

	public function finishVersion(&$scoring_data, &$html, $handle_data, $assessment_data, $userID) {
		$success = true;

		$new_scoring_data = array(
			'Sections' => $scoring_data,
			'Outcomes' => $assessment_data['Outcomes'],
			'Scoring' => $assessment_data['Scoring']
		);

        // Get map from handle ID to question ID from scoring data

		$old_handle_ids = array();
		foreach ($new_scoring_data['Sections'] as $section) {
			foreach ($section['Content'] as $item) {
				$old_handle_ids[] = $item['Source']['question_handle_id'];
			}
		}
		$old_handle_ids = array_unique($old_handle_ids);

		$questionHandleModel = ClassRegistry::init('QuestionHandle');
		$questionHandleModel->contain();
		$old_handle_map = $questionHandleModel->find('list', array(
			'fields' => array('QuestionHandle.id', 'QuestionHandle.question_id'),
			'conditions' => array('QuestionHandle.id' => $old_handle_ids)
		));

        // Switch to persistent question handles in scoring data and HTML

        $persistent_handle_map = array();
        foreach ($old_handle_map as $old_question_handle_id => $question_id) {
            $handle_data['question_id'] = $question_id;
            $new_question_handle_id = $questionHandleModel->createHandle($handle_data, false);
            $persistent_handle_map[$old_question_handle_id] = $new_question_handle_id;
        }

		foreach ($new_scoring_data['Sections'] as &$pSection) {
			foreach ($pSection['Content'] as &$pItem) {
                $old_question_handle_id = $pItem['Source']['question_handle_id'];
                if (array_key_exists($old_question_handle_id, $persistent_handle_map)) {
                    $new_question_handle_id = $persistent_handle_map[$old_question_handle_id];
                    $pItem['Source']['question_handle_id'] = $new_question_handle_id;
                } else $success = false;
            }
        }

		if (!$success) return false;

        $handle_regex = '/data-question-handle-id="(\d+)"/';
        $new_html = preg_replace_callback($handle_regex, function($match) use ($persistent_handle_map, &$success) {
            $old_question_handle_id = $match[1];
            if (array_key_exists($old_question_handle_id, $persistent_handle_map)) {
                $new_question_handle_id = $persistent_handle_map[$old_question_handle_id];
                return 'data-question-handle-id="' . $new_question_handle_id . '"';
            } else $success = false;
        }, $html);

        if (!$success) return false;

        // Update last used

		$questionModel = ClassRegistry::init('Question');
		$last_used = $questionModel->find('list', array(
			'fields' => array('Question.id', 'Question.last_used'),
			'conditions' => array('Question.id' => array_values($old_handle_map))
		));

		foreach ($last_used as $question_id => $lastUsedString) {
			if (strlen($lastUsedString) == 0) $lastUsed = array();
			else $lastUsed = json_decode($lastUsedString, true);

			$lastUsed[$userID] = date('Y-m-d H:i:s');

			$questionModel->id = $question_id;
			$questionModel->saveField('last_used', json_encode($lastUsed), array(
				'callbacks' => false
			));
		}

		// Get data on question handle roles in assessment

		$handle_data = array();
		foreach ($assessment_data['Sections'] as $section) {
			if (!array_key_exists('Content', $section)) continue;
			foreach ($section['Content'] as $entry) {
				$content_id = $entry['id'];
				$this_data = array(
					'content_id' => $entry['id'],
					'type' => $entry['type']
				);

				if ($entry['type'] == 'item') {

					$old_question_handle_id = $entry['question_handle_id'];
                    if (array_key_exists($old_question_handle_id, $persistent_handle_map)) {
                        $new_question_handle_id = $persistent_handle_map[$old_question_handle_id];
                        $handle_data[$new_question_handle_id] = $this_data;
                    } else $success = false;

				} else if ($entry['type'] == 'item_set') {

                    $found_handle = false;
					foreach ($entry['question_handle_ids'] as $old_question_handle_id) {
                        if (array_key_exists($old_question_handle_id, $persistent_handle_map)) {
                            $found_handle = true;

                            $questionHandleModel->contain('Question');
                            $question = $questionHandleModel->findById($old_question_handle_id);
                            if (!empty($question['Question']['json_data'])) {
                                $json_data = json_decode($question['Question']['json_data'], true);

                                $part_indices = array();
                                foreach ($json_data['Parts'] as $index => $part) {
                                    $part_indices[$part['id']] = $index;
                                }
                                $this_data['part_indices'] = $part_indices;

                                $new_question_handle_id = $persistent_handle_map[$old_question_handle_id];
                                $handle_data[$new_question_handle_id] = $this_data;
                            } else $success = false;
						}
					}
                    if (!$found_handle) $success = false;
                    
				}
			}
		}

		if (!$success) return false;

		// Get map from content IDs and part IDs / indices to item IDs

		$item_map = array();
		foreach ($new_scoring_data['Sections'] as &$pSection) {
			foreach ($pSection['Content'] as &$pItem) {
				$question_handle_id = $pItem['Source']['question_handle_id'];
				$part_id = $pItem['Source']['part_id'];

				if (array_key_exists($question_handle_id, $handle_data)) {
					$this_data = $handle_data[$question_handle_id];

					$content_id = $this_data['content_id'];
					if (!array_key_exists($content_id, $item_map)) $item_map[$content_id] = array();

					if ($this_data['type'] == 'item') {
						$item_map[$content_id][$part_id] = $pItem['id'];
					} else if ($this_data['type'] == 'item_set') {
						if (array_key_exists($part_id, $this_data['part_indices'])) {
							$part_index = $this_data['part_indices'][$part_id];
							$item_map[$content_id][$part_index] = $pItem['id'];
						} else $success = false;
					} else $success = false;
				} else $success = false;
			}
		}

		if (!$success) return false;

		// Translate outcomes references to item IDs

		foreach ($new_scoring_data['Outcomes'] as &$pOutcome) {
			$item_ids = array();
			foreach ($pOutcome['parts'] as $part) {
				if (array_key_exists('part_id', $part)) {
					if (isset($item_map[$part['content_id']][$part['part_id']])) {
						$item_ids[] = $item_map[$part['content_id']][$part['part_id']];
					}
				} else if (array_key_exists('part_index', $part)) {
					if (isset($item_map[$part['content_id']][$part['part_index']])) {
						$item_ids[] = $item_map[$part['content_id']][$part['part_index']];
					}		
				} else $success = false;
			}
			$pOutcome['item_ids'] = $item_ids;
			unset($pOutcome['parts']);
		}

		if (!$success) return false;

		// Swap section weighting for custom weighting if needed

		if ($new_scoring_data['Scoring']['type'] == 'Sections') {
			$scoringSectionIDs = array();
			foreach ($new_scoring_data['Sections'] as $section) {
				$scoringSectionIDs[] = $section['id'];
			}

			$assessmentSectionIDs = array();
			foreach ($assessment_data['Sections'] as $section) {
				$assessmentSectionIDs[] = $section['id'];
			}

			$scoringMatch = true;
			$assessmentMatch = true;
			foreach ($new_scoring_data['Scoring']['weights'] as $entry) {
				if (!in_array($entry['section_id'], $scoringSectionIDs)) $scoringMatch = false;
				if (!in_array($entry['section_id'], $assessmentSectionIDs)) $assessmentMatch = false;
			}

			if ($assessmentMatch && !$scoringMatch) {
				$new_scoring_data['Scoring']['type'] = 'Custom';
				foreach ($new_scoring_data['Scoring']['weights'] as &$pEntry) {
					$section_index = array_search($pEntry['section_id'], $assessmentSectionIDs);

					$item_ids = array();
					foreach ($assessment_data['Sections'][$section_index]['Content'] as $item) {
						$content_id = $item['id'];
						if (!empty($item_map[$content_id])) {
							$item_ids = array_merge($item_ids, array_values($item_map[$content_id]));
						}
					}

					$pEntry['name'] = $assessment_data['Sections'][$section_index]['header'];
					$pEntry['item_ids'] = $item_ids;
					unset($pEntry['section_id']);
				}
			}
		}

        if ($success) {
            $scoring_data = $new_scoring_data;
            $html = $new_html;
        }

		return $success;
	}
	
	public function checkData($json_data) {

        // Re-encode JSON data as an object, and read the schema

        $json_object = json_decode(json_encode($json_data), false);

        $schema_object = $this->getSchema('/schemas/version_data');
        if ($schema_object !== false) {

            // Validate with schema

            App::uses('Jsv4', 'Vendor');
            $schema_result = Jsv4::validate($json_object, $schema_object);
            unset($schema_object);
            gc_collect_cycles();

            if (!$schema_result->valid) {
                return json_encode($schema_result->errors);
            }

        } else {

            return "Error loading schema";

        }
/*
		// Check IDs

		$isValid = true;

		$section_ids = array();
		$item_ids = array();
		foreach ($json_data['Sections'] as $section) {
			if (!array_key_exists('Content', $section)) continue;

			$section_ids[] = $section['id'];
			foreach ($section['Content'] as $entry) {
				$item_ids[] = $entry['id'];
			}	
		}
		
		$outcome_ids = array();
		foreach ($json_data['Outcomes'] as $outcome) {
			$outcome_ids[] = $outcome['id'];
			foreach ($outcome['item_ids'] as $item_id) {
				if (!in_array($item_id, $item_ids)) $isValid = false;
			}
		}

		if (!$isValid) {
			return "Unexpected item ID in version data outcome";
		}

		// Check weights

        if (array_key_exists('weights', $json_data['Scoring'])) {

            foreach ($json_data['Scoring']['weights'] as $weight) {
                if ($json_data['Scoring']['type'] == 'Sections') {
                    if (!in_array($weight['section_id'], $section_ids)) $isValid = false;
                } else if ($json_data['Scoring']['type'] == 'Outcomes') {
                    if (!in_array($weight['outcome_id'], $outcome_ids)) $isValid = false;
                } else if ($json_data['Scoring']['type'] == 'Custom') {
                    foreach ($weight['item_ids'] as $item_id) {
                        if (!in_array($item_id, $item_ids)) $isValid = false;
                    }
                } else $isValid = false;
            }
		}

		if (!$isValid) {
			return "Unexpected or missing weights in version data";
		}

		// Check that question numbers are valid and don't repeat in the same section for any item style

		foreach ($json_data['Sections'] as $section) {
			$used_numbers = array();
			foreach ($section['Content'] as $item) {
				$style = substr($item['type'], 0, 2);
				if (!array_key_exists($style, $used_numbers)) $used_numbers[$style] = array();
				if (in_array($item['number'], $used_numbers[$style])) $isValid = false;
				else $used_numbers[$style][] = $item['number'];
			}	
		}
		if (!$isValid) {
			return "Invalid numbering in version data";
		}

		// Check scored response IDs

        foreach ($json_data['Sections'] as $section) {
			if (array_key_exists('Content', $section)) {
                foreach ($section['Content'] as $item) {
                    if (array_key_exists('ScoredResponses', $item)) {

                        $answer_ids = array();
                        if (!empty($item['Answers'])) {
                            foreach ($item['Answers'] as $answer) {
                                $answer_ids[] = $answer['id'];
                            }
                        }
            
                        foreach ($item['ScoredResponses'] as $entry) {
                            if (array_key_exists('id', $entry)) {
                                if (!in_array($entry['id'], $answer_ids)) $isValid = false;
                            } else if (array_key_exists('ids', $entry)) {
                                if (substr($item['type'], 0, 2) == 'nr') $isValid = false;
                                if (count($entry['ids']) <= 1) $isValid = false;
                                foreach ($entry['ids'] as $entry_id) {
                                    if (!in_array($entry_id, $answer_ids)) $isValid = false;
                                }
                            } else $isValid = false;
                        }

                    }
                }
            }
        }
		if (!$isValid) {
			return "Invalid IDs in scored responses";
		}
*/
		return true;
	}

	public function getScoringCache($id) {
		$json_data = Cache::read($id, 'scoring_data');

		if (!$json_data) {
			$this->contain();
			$version_data = $this->findById($id);
			if (!empty($version_data)) {
				$json_data = json_decode($version_data['VersionData']['scoring_json'], true);
				Cache::write($id, $json_data, 'scoring_data');
			}
		}

		return $json_data;
	}

	public function beforeSave($options = array()) {
	    if (!empty($this->data['VersionData']['scoring_json'])) {
			$json_data = json_decode($this->data['VersionData']['scoring_json'], true);
			$check_result = $this->checkData($json_data);
			if ($check_result !== true) {
				$user_email = AuthComponent::user('email');
				if (!array_key_exists('id', $this->data['VersionData'])) $error_data = array();
				else $error_data = array('id' => $this->data['VersionData']['id']);
				$error_data['json_data'] = json_encode($json_data);

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Validation failed in VersionData->save", $user_email, array(
					'result' => $check_result,
					'data' => $error_data
				));
				return false;
			}
		}

    	return true;
	}

	public function afterSave($created, $options = array()) {
		Cache::delete($this->id, 'scoring_data');

		$this->contain(array('ResultData' => array('ResultSet' => 'ResponsePaper'), 'Sitting'));
		$version_data = $this->findById($this->id);

		foreach ($version_data['Sitting'] as $sitting) {
			Cache::delete($sitting['id'], 'sittings');
		}

        $resultSetModel = ClassRegistry::init('ResultSet');
		foreach ($version_data['ResultData'] as $resultData) {
			
			// Queue stats update

            if ($resultData['ResultSet']['stats_status'] != 'exclude') {
                $resultSetModel->id = $resultData['ResultSet']['id'];
                $resultSetModel->saveField('stats_status', 'update');
            }

            if (!empty($resultData['ResultSet']['ResponsePaper'])) {

				// Update score caches

				$user_id = AuthComponent::user('id');
				$cache_data = Cache::read($user_id, 'report_data');
				if (!empty($cache_data)) {
					foreach ($resultData['ResultSet']['ResponsePaper'] as $thisData) {
						$score_id = $thisData['score_id'];
						unset($cache_data[$score_id]);
					}
					Cache::write($user_id, $cache_data, 'report_data');
				}
			}
        }
	}

	public function clean($id) {
		$this->contain(array('Document', 'ResultData', 'Sitting'));
		$version = $this->findById($id);
		$success = true;
		
		if ($version) {
			$canDelete = true;
			if (count($version['Document']) > 0) $canDelete = false;
			else if (count($version['ResultData']) > 0) $canDelete = false;
			else if (count($version['Sitting']) > 0) $canDelete = false;
			
			if ($canDelete) $success &= $this->delete($id);
		} else $success = false;

		return $success;
	}
}

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class Community extends AppModel {

	var $actsAs = array('Containable');

	public $hasMany = array(
		'CommunityUser' => array(
			'className' => 'CommunityUser',
			'foreignKey' => 'community_id',
			'dependent' => true
		)
	);
	
	public function getPermissions($communityId, $user_id) {
		$permissions = array('all' => array(), 'owner' => array());
		$allPermissions = array('can_view', 'can_add', 'can_export', 'can_delete', 'can_edit');

		if (empty($communityId) || !is_numeric($communityId)) return $permissions;
		if (empty($user_id) || !is_numeric($user_id)) return $permissions;

		$this->CommunityUser->contain(array('Community'));
		$community = $this->CommunityUser->find('first', array(
			'conditions' => array(
				'CommunityUser.community_id' => $communityId,
				'CommunityUser.user_id' => $user_id
			)
		));

		if ($community) {
			if ($community['CommunityUser']['is_admin'] == 1) {
				$permissions['all'] = $allPermissions;
				if (in_array($community['Community']['scores_policy'], array('admin', 'all'))) {
					$permissions['all'][] = 'all_scores';
				}
			} else {
				$permissions['all'][] = 'can_view';
				
				if ($community['Community']['edit_policy'] == 'owner') {
					$permissions['owner'][] = 'can_edit';
				} else if ($community['Community']['edit_policy'] == 'all') {
					$permissions['all'][] = 'can_edit';
				}
	
				if ($community['Community']['add_delete_policy'] == 'owner') {
					$permissions['all'][] = 'can_add';
					$permissions['owner'][] = 'can_delete';
				} else if ($community['Community']['add_delete_policy'] == 'all') {
					$permissions['all'][] = 'can_add';
					$permissions['all'][] = 'can_delete';
				}
	
				if ($community['Community']['export_policy'] == 'open') {	
					$permissions['all'][] = 'can_export';
				}
	
				if ($community['Community']['scores_policy'] == 'all') {
					$permissions['all'][] = 'all_scores';
				}
			}
		}

		$permissions['owner'] = array_merge($permissions['owner'], $permissions['all']);
		$permissions['owner'] = array_values(array_unique($permissions['owner']));

		return $permissions;
	}	

	public function clean($community_id) {
		$success = true;
		$communityUserCount = $this->CommunityUser->find('count', array(
			'conditions' => array('CommunityUser.community_id' => $community_id)
		));
		if ($communityUserCount == 0) {
			$data = array();
			$data['id'] = $community_id;
			$data['deleted'] = date('Y-m-d H:i:s');
			$success &= !empty($this->save($data));
		}

		return $success;
	}

	public function beforeDelete($cascade = true) {
		if ($cascade) {
			$folderModel = ClassRegistry::init('Folder');
	
			$this->contain();
			$community = $this->findById($this->id);
			$folder_ids = json_decode($community['Community']['folder_ids'], true);
			foreach ($folder_ids as $type => $id) $folderModel->delete($id);
		}
				
		return true;
	}
}

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class JsModule extends AppModel {
	var $actsAs = array('Containable');
	public $displayField = 'name';

	public function getCache($id) {
		$data = Cache::read($id, 'js_modules');

		if (!$data) {
			$this->contain();
			$jsModule = $this->findById($id);

			if (!empty($jsModule)) {
				$data = $jsModule['JsModule'];
				Cache::write($id, $data, 'js_modules');
			} else $data = false;
		}

		return $data;
	}
}

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class Question extends AppModel {
	var $actsAs = array('Containable');

	public $displayField = 'name';
       
	public $hasMany = array(
		'QuestionHandle' => array(
			'className' => 'QuestionHandle',
			'foreignKey' => 'question_id',
			'dependent' => true
		)
	);

    public function clean_html($html) {

        $allowed_tags = array(
            'div', 'span',
            'br', 
            'b', 'i', 'u', 'sup', 'sub', 
            'ul', 'ol', 'li', 
            'img', 
            'table', 'tbody', 'tr', 'td',
            'svg', 'path'
        );

        $delete_selectors = array(
            'script', 'style', 'svg', 
            'textarea', 'button', 'fieldset', 
            'del', 
            'mjx-container', 'c-wiz', 'g-dropdown-menu',
            'svg:not(.annotation_wrapper)'
        );

        $replace_tags = array(
            array('regex' => '/^p$/', 'target' => 'div'),
            array('regex' => '/^h[0-9]$/', 'target' => 'div'),
            array('regex' => '/^center$/', 'target' => 'div'), 
            array('regex' => '/^blockquote$/', 'target' => 'div'), 
            array('regex' => '/^header$/', 'target' => 'div'), 
            array('regex' => '/^footer$/', 'target' => 'div'), 
            array('regex' => '/^caption$/', 'target' => 'div'), 
            array('regex' => '/^legend$/', 'target' => 'div'), 
            array('regex' => '/^var$/', 'target' => 'i'),
            array('regex' => '/^strong$/', 'target' => 'b'),
            array('regex' => '/^em$/', 'target' => 'i')
        );

        $allowed_attributes = array(
            'div' => array('class', 'style', 'data-json'),
            'span' => array('class', 'style', 'data-variable-name'),
            'ul' => array('class', 'style'),
            'ol' => array('style'),
            'img' => array('style', 'src'), 
            'table' => array('style'),
            'td' => array('class', 'style', 'colspan', 'width', 'valign'),
            'svg' => array('class', 'data-tool-name'),
            'path' => array('stroke', 'stroke-width', 'd')
        );

        $allowed_classes = array(
            'div' => array('equation', 'code_container', 'html_drawing'),
            'span' => array('block_variable', 'block_bigBlank'),
            'ul' => array('ul_checklist'),
            'td' => array('td_top', 'td_middle', 'td_bottom'),
            'svg' => array('annotation_wrapper')
        );

        $allowed_styles = array(
            'div' => array('text-align', 'font-weight', 'font-style', 'width', 'aspect-ratio'),
            'span' => array('font-weight', 'font-style'),
            'ul' => array('list-style-type'),
            'ol' => array('list-style-type'),
            'img' => array('width'),
            'table' => array('width', 'margin', 'margin-left', 'margin-right'),
            'td' => array('text-align', 'font-weight', 'font-style', 'width', 'border-style',
                'border-left-style', 'border-right-style', 'border-top-style', 'border-bottom-style')
        );

        $strip_empty = array(
            'div', 'span', 
            'b', 'i', 'u', 'sup', 'sub', 
            'ul', 'ol'
        );

        $strip_nested = array(
            'div', 'span', 
        );

        $old_html = $html;

        $dom_changes = array();

        // Clear XML namespaces, which needs to be done while HTML is text

        $html = preg_replace('/ xmlns=\\"[^"]*"/', '', $html);

        // Replace strippable elements with only whitespace inside with a space -- otherwise they just evaluate as
        // empty later.

        foreach ($strip_empty as $element_name) {
            $element_regex = '#<' . $element_name . '>\s+</' . $element_name . '>#';
            if (preg_match($element_regex, $html)) {
                if (empty($dom_changes['empty_' . $element_name])) $dom_changes['empty_' . $element_name] = 0;
                $dom_changes['empty_' . $element_name]++;

                $html = preg_replace($element_regex, ' ', $html);
            }
        }

        // Set up DOMDocument object

        $prefix = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>';
        $suffix = '</body></html>';
        
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($prefix . $html . $suffix);

        $body = $doc->getElementsByTagName('body')->item(0);

        // Remove comments

        $comments = $body->getElementsByTagName('comment');
        if ($comments->length > 0) {
            foreach ($comments as $comment) {
                $comment->parentNode->removeChild($comment);
            }
        }

        // Delete disallowed elements as required

        $delete_elements = array();

        $elements = $body->getElementsByTagName('*');
        foreach ($elements as $element) {
            $element_name = $element->nodeName;
            $classes = array_filter(explode(' ', $element->getAttribute('class')));

            $delete_element = false;
            foreach ($delete_selectors as $selector) {
                if ($selector == 'svg:not(.annotation_wrapper)') {
                    if (($element_name == 'svg') && (!in_array('annotation_wrapper', $classes))) {
                        $delete_element = true;
                    }
                } else if ($selector == $element_name) {
                    $delete_element = true;
                }
            }

            if ($delete_element) {

                // Unshift to ensure that children are encountered first

                array_unshift($delete_elements, array('element' => $element));
            }
        }

        foreach ($delete_elements as $entry) {
            $element = $entry['element'];
            $parent = $element->parentNode;
            $element_name = $element->nodeName;

            $parent->removeChild($element);

            if (empty($dom_changes[$element_name])) $dom_changes[$element_name] = 0;
            $dom_changes[$element_name]++;
        }

        // Replace disallowed elements as required

        $replace_elements = array();
        
        $elements = $body->getElementsByTagName('*');
        foreach ($elements as $element) {
            $element_name = $element->nodeName;

            $new_name = false;
            foreach ($replace_tags as $entry) {
                if (preg_match($entry['regex'], $element_name)) $new_name = $entry['target'];
            }

            if ($new_name !== false) {

                // Unshift to ensure that children are encountered first

                array_unshift($replace_elements, array(
                    'element' => $element,
                    'new_name' => $new_name
                ));
            }
        }

        foreach ($replace_elements as $entry) {
            $element = $entry['element'];
            $parent = $element->parentNode;
            $element_name = $element->nodeName;

            $new_element = $doc->createElement($entry['new_name']);

            if ($element->hasAttributes()) {
                foreach ($element->attributes as $attribute) {
                    $new_element->setAttribute($attribute->nodeName, $attribute->nodeValue);
                }    
            }
            
            if (in_array($element_name, array('h1', 'h2', 'h3'))) {
                $target_element = $doc->createElement('b');
                $new_element->appendChild($target_element);
            } else $target_element = $new_element;

            foreach ($element->childNodes as $child) {
                $target_element->appendChild($child->cloneNode(true));
            }

            $parent->replaceChild($new_element, $element);

            if ($element_name == 'blockquote') {

                $break_element = $doc->createDocumentFragment();
                $break_element->appendXML('<div><br/></div>');

                if ($new_element->nextSibling === null) $parent->appendChild($break_element);
                else $parent->insertBefore($break_element, $new_element->nextSibling);

            } else if ($element_name == 'center') {

                $new_element->setAttribute('style', 'text-align:center;');

            }

            if (empty($dom_changes[$element_name])) $dom_changes[$element_name] = 0;
            $dom_changes[$element_name]++;
        }

        // Clean attributes, classes, and styles

        $elements = $body->getElementsByTagName('*');
        foreach ($elements as $element) {

            $element_name = $element->nodeName;

            // Skip elements that we'll be unwrapping later. We have to clean attributes first because this 
            // may create spans with no style or class, which are unwrapped below.

            if (!in_array($element_name, $allowed_tags)) continue;

            // Remove disallowed attributes

            $remove_attributes = array();
            foreach ($element->attributes as $attribute) {
                if ($attribute->name == 'align') {

                    $style = $element->getAttribute('style');
                    $style = 'text-align:' . $attribute->value . ';' . $style;
                    $element->setAttribute('style', $style);

                    $element->removeAttribute($attribute->name);

                    if (empty($dom_changes['attribute_align'])) $dom_changes['attribute_align'] = 0;
                    $dom_changes['attribute_align']++;
    
                } else if (!array_key_exists($element_name, $allowed_attributes) || 
                    !in_array($attribute->name, $allowed_attributes[$element_name])) {

                    $remove_attributes[] = $attribute->name;

                }
            }

            if (!empty($remove_attributes)) {
                foreach ($remove_attributes as $name) {
                    $element->removeAttribute($name);
                }

                if (empty($dom_changes['attribute_removed'])) $dom_changes['attribute_removed'] = 0;
                $dom_changes['attribute_removed']++;
            }

            $classes = explode(' ', $element->getAttribute('class'));
            if (in_array('block_variable', $classes) || in_array('block_bigBlank', $classes)) {

                // Remove all style elements from these classes

                if (!empty($element->getAttribute('style'))) {
                    $element->removeAttribute('style');

                    if (empty($dom_changes['attribute_style'])) $dom_changes['attribute_style'] = 0;
                    $dom_changes['attribute_style']++;
                }

            } else {

                // Remove disallowed styles and thin repeated styles

                $new_styles = '';
                if (!empty($element->getAttribute('style'))) {

                    $new_style_array = array();

                    $styles = explode(';', $element->getAttribute('style'));
                    foreach ($styles as $style_raw) {
                        $elements = explode(':', $style_raw);
                        $style_name = trim($elements[0]);
                        if (array_key_exists($element_name, $allowed_styles) && 
                            in_array($style_name, $allowed_styles[$element_name])) {

                            $style_value = trim($elements[1]);
                            $new_style_array[$style_name] = $style_value;

                        } else {
                            if (empty($dom_changes['attribute_style'])) $dom_changes['attribute_style'] = 0;
                            $dom_changes['attribute_style']++;
                        }
                    }

                    foreach ($new_style_array as $name => $value) {
                        $new_styles .= $name . ':' . $value . ';';
                    }

                }
    
                if (!empty($new_styles)) {
                    $element->setAttribute('style', $new_styles);
                } else $element->removeAttribute('style');

                // Remove disallowed classes
    
                $new_classes = '';
                if (!empty($element->getAttribute('class'))) {

                    foreach ($classes as $class) {
                        if (array_key_exists($element_name, $allowed_classes) && 
                            in_array($class, $allowed_classes[$element_name])) {
                            if (strlen($new_classes) > 0) $new_classes .= ' ';
                            $new_classes .= $class;
                        } else {
                            if (empty($dom_changes['attribute_class'])) $dom_changes['attribute_class'] = 0;
                            $dom_changes['attribute_class']++;        
                        }
                    }

                }
    
                if (!empty($new_classes)) {
                    $element->setAttribute('class', $new_classes);
                } else $element->removeAttribute('class');

            }
            
        }

        // Unwrap disallowed elements as required

        $unwrap_elements = array();

        $elements = $body->getElementsByTagName('*');
        foreach ($elements as $element) {
            $element_name = $element->nodeName;

            $should_unwrap = !in_array($element_name, $allowed_tags);
            if (($element_name == 'span') && empty($element->getAttribute('style')) && 
                empty($element->getAttribute('class'))) $should_unwrap = true;

            if ($should_unwrap) {

                // Unshift to ensure that children are encountered first

                array_unshift($unwrap_elements, array('element' => $element));
            }
        }

        foreach ($unwrap_elements as $entry) {
            $element = $entry['element'];
            $parent = $element->parentNode;
            $element_name = $element->nodeName;

            if (($element->childNodes->length == 1) && ($element->firstChild->nodeType === XML_TEXT_NODE)) {
                $element_text = $element->textContent;
                $new_text_node = $doc->createTextNode($element_text);
                $parent->insertBefore($new_text_node, $element);

            } else {

                foreach ($element->childNodes as $child) {
                    $parent->insertBefore($child->cloneNode(true), $element);
                }

            }
            $parent->removeChild($element);

            if (empty($dom_changes[$element_name])) $dom_changes[$element_name] = 0;
            $dom_changes[$element_name]++;
        }
       
        // Remove redundant elements

        do {
            
            $elements = $body->getElementsByTagName('*');

            $this_changed = false;
            foreach ($elements as $element) {

                $parent = $element->parentNode;
                $element_name = $element->nodeName;

                if (in_array($element_name, $strip_empty) &&
                    empty($element->getAttribute('class')) && !$element->hasChildNodes()) {

                    // Delete empty elements as needed

                    $parent->removeChild($element);

                    $this_changed = true;
                    if (empty($dom_changes[$element_name])) $dom_changes[$element_name] = 0;
                    $dom_changes[$element_name]++;
                        
                } else if (in_array($element_name, $strip_nested) && 
                    ($element->childNodes->length == 1) && ($element->firstChild->nodeName == $element_name) && 
                    (empty($element->getAttribute('class')) || empty($element->firstChild->getAttribute('class')))) {

                    // Unwrap divs and spans containing only one child, which is of the same type and for which either
                    // parent or child has no classes.

                    $child = $element->firstChild;

                    // Merge styles

                    $merged_styles = array();

                    $element_styles = explode(';', $element->getAttribute('style'));
                    foreach ($element_styles as $style) {
                        $elements = explode(':', $style);
                        if (count($elements) == 2) {
                            $merged_styles[trim($elements[0])] = trim($elements[1]);
                        }
                    }

                    $child_styles = explode(';', $child->getAttribute('style'));
                    foreach ($child_styles as $style) {
                        $elements = explode(':', $style);
                        if (count($elements) == 2) {
                            $merged_styles[trim($elements[0])] = trim($elements[1]);
                        }
                    }

                    $style_string = '';
                    foreach ($merged_styles as $key => $value) {
                        $style_string .= $key . ':' . $value . ';';
                    }

                    if (!empty($style_string)) {
                        $child->setAttribute('style', $style_string);
                    } else $child->removeAttribute('style');

                    // Merge classes

                    if (!empty($element->getAttribute('class'))) {
                        $child->setAttribute('class', $element->getAttribute('class'));
                    }

                    // Replace element with child

                    $parent->replaceChild($child, $element);

                    $this_changed = true;
                    if (empty($dom_changes[$element_name])) $dom_changes[$element_name] = 0;
                    $dom_changes[$element_name]++;
    
                }
            }
        } while ($this_changed);

        if (!empty($dom_changes)) {

            // Return body only

            $new_html = $doc->saveHTML();
            $new_html = str_replace("\u{00a0}", '&nbsp;', $new_html);

            $body_start = strpos($new_html, '<body>');
            $body_end = strrpos($new_html, '</body>');
            $body_html = substr($new_html, $body_start + 6, $body_end - $body_start - 6);

            return $body_html;

        } else {

            return $html;

        }
    }

	public function clean_json($json_data) {

        if (!empty($json_data['Common']['Answers'])) {
            foreach ($json_data['Common']['Answers'] as &$pAnswer) {
                $pAnswer['template'] = $this->clean_html($pAnswer['template']);
            }
        }

        foreach ($json_data['Parts'] as &$pPart) {
            $pPart['template'] = $this->clean_html($pPart['template']);
            if (!empty($pPart['Answers'])) {
                foreach ($pPart['Answers'] as &$pAnswer) {
                    if ($pPart['type'] == 'mc_table') {
                        foreach ($pAnswer['templates'] as &$pColumn) {
                            $pColumn = $this->clean_html($pColumn);
                        }
                    } else {
                        $pAnswer['template'] = $this->clean_html($pAnswer['template']);
                    }
                }    
            }
            if (!empty($pPart['Rubric'])) {
                if (!empty($pPart['Rubric']['template'])) {
                    $pPart['Rubric']['template'] = $this->clean_html($pPart['Rubric']['template']);
                } else if (!empty($pPart['Rubric']['rows'])) {
                    foreach ($pPart['Rubric']['rows'] as &$pRow) {
                        if (array_key_exists('focus', $pRow)) {
                            $pRow['focus'] = $this->clean_html($pRow['focus']);
                        }
                        foreach ($pRow['scoring'] as &$pScoring) {
                            $pScoring['description'] = $this->clean_html($pScoring['description']);
                        }
                    }
                }
            }
            if (!empty($pPart['TypeDetails']['labels'])) {
                foreach ($pPart['TypeDetails']['labels'] as &$pLabel) {
                    $pLabel = $this->clean_html($pLabel);
                }
            }
        }

        return $json_data;
    }

	public function getScoringHash($json_data) {
		$hash_elements = array();

		foreach ($json_data['Parts'] as $part) {
            if ($part['type'] == 'context') continue;

            if (($part['type'] == 'nr_decimal') || ($part['type'] == 'nr_scientific')) {
                $newEntry['type'] = 'nr_number';
            } else $newEntry['type'] = $part['type'];

			$maxValue = 0.0;
			if (substr($part['type'], 0, 2) == 'mc') {

				foreach ($part['ScoredResponses'] as $entry) {
					$thisValue = $entry['value'];
					if ($thisValue > $maxValue) $maxValue = $thisValue;
				}

			} else if (substr($part['type'], 0, 2) == 'nr') {

				if (isset($part['TypeDetails']['markByDigits'])) {
					$markByDigits = $part['TypeDetails']['markByDigits'];
				} else $markByDigits = false;

				foreach ($part['ScoredResponses'] as $entry) {
					$thisValue = $entry['value'];

					if ($markByDigits) {
						$template = '';
						if (array_key_exists('id', $entry)) {
							foreach ($part['Answers'] as $answer) {
								if ($answer['id'] == $entry['id']) $template = $answer['template'];
							}	
						}
						$thisValue *= strlen($template);
					}

					if ($thisValue > $maxValue) $maxValue = $thisValue;
				}

			} else if ($part['type'] == 'wr') {

				$criteria = $part['TypeDetails']['criteria'];
				foreach ($criteria as $entry) $maxValue += $entry['value'];
				
			}

			$newEntry['maxValue'] = $maxValue;
			
            $hash_string = json_encode($newEntry);
			$hash_elements[] = md5($hash_string);
		}

        sort($hash_elements);
        $hash_string = json_encode($hash_elements);
		return md5($hash_string);
	}

	public function getContentIDs($json_data) {
		$hash_elements = array();

		$sort_groups = array();
		$sort_index = 0;

		// Get a string with question elements in a standard order. Include only elements that impact what
		// students will see, so that we can apply scored responses later.

		$hasContent = false;

		if (!empty($json_data['Common']['Answers'])) {
			foreach ($json_data['Common']['Answers'] as $answer) {
				if (!empty($answer['template'])) $hasContent = true;

				$hash_elements[] = array(
					'template' => $answer['template'],
					'id' => $answer['id']
				);
				$sort_groups[] = $sort_index;
			}
			$sort_index++;
		}

		$part_strings = array();
		foreach ($json_data['Parts'] as $part) {
			if (!empty($part['template'])) $hasContent = true;

			$hash_elements[] = array(
				'type' => $part['type'],
				'template' => $part['template'],
				'id' => $part['id']
			);
			$sort_groups[] = $sort_index++;

			if ((substr($part['type'], 0, 2) == 'mc') && !empty($part['Answers'])) {
				foreach ($part['Answers'] as $answer) {
					if (!empty($answer['template'])) {
                        $hasContent = true;
                        $hash_elements[] = array(
                            'template' => $answer['template'],
                            'id' => $answer['id']
                        );
                        $sort_groups[] = $sort_index;
                    } else if (!empty($answer['templates'])) {
                        $hasContent = true;
                        $hash_elements[] = array(
                            'template' => json_encode($answer['templates']),
                            'id' => $answer['id']
                        );
                        $sort_groups[] = $sort_index;
                    }
				}
				$sort_index++;
			}
		}

		if ($hasContent) {

			if (!empty($json_data['Engine']['data'])) {

				$hash_elements[] = $json_data['Engine']['data'];
				$sort_groups[] = $sort_index++;
				
			} else if (!empty($json_data['Engine']['js'])) {
	
				$hash_elements[] = $json_data['Engine']['js'];
				$sort_groups[] = $sort_index++;
				
			} else {
	
				// Sort within each sort group alphabetically
	
				for ($i = 0; $i < $sort_index; ++$i) {
					$indices = array();
					$elements = array();
	
					for ($j = 0; $j < count($sort_groups); ++$j) {
						if ($sort_groups[$j] == $i) {
							$indices[] = $j;
							$elements[] = json_encode($hash_elements[$j]);
						}
					}
	
					sort($elements);
					for ($k = 0; $k < count($indices); ++$k) {
						$hash_elements[$indices[$k]] = json_decode($elements[$k], true);
					}
				}
			}
	
			$hash_string = json_encode($hash_elements);
	
			// Find and remove IDs and variable names, recording their original offsets
	
			$regex_variable_text = 'data-variable-name=\\\\"([^\\\\]+)\\\\"';  // Matches variables in text
			$regex_variable_wizard = '"variableName":"([^"]+)"';               // Matches wizard variables
			$regex_variable_equation = '{"type":"Variable"[^}]+?(?="value":)"value":"([^"]+)"';  // Matches equation variables
			$regex_id = '"id":"([^"]+)"';                                      // Matches IDs from construction above
	
			$regex_to_remove = '/' . $regex_variable_text . '|' . $regex_variable_wizard . '|' . $regex_variable_equation . '|' . $regex_id . '/';
	
			$captured = array();
			while (preg_match($regex_to_remove, $hash_string, $matches, PREG_OFFSET_CAPTURE)) {
				$match_index = $matches[0][1];
				$match_length = strlen($matches[0][0]);
				$hash_string = substr($hash_string, 0, $match_index) . substr($hash_string, $match_index + $match_length);
	
				$id = false;
				for ($i = 1; $i < count($matches); ++$i) {
					if ($matches[$i][1] != -1) $id = $matches[$i][0];
				}
	
				if ($id !== false) {
					if (!array_key_exists($id, $captured)) $captured[$id] = array();
					$captured[$id][] = $match_index;
				}
			}
	
			// Append a string to the hash that is unique to the groupings (ignoring names) of IDs and variables
	
			$captured_json = array();
			foreach ($captured as $name => $offsets) {
				$captured_json[] = json_encode($offsets);
			}
			sort($captured_json);
	
			foreach ($captured_json as $this_json) {
				$hash_string .= ',' . $this_json;
			}
	
			// Return the MD5 hash of the resulting string
	
			$content_ids = array('question' => md5($hash_string));
	
			// Return offsets for each part and answer as well, so responses can be mapped to a common form
	
			$md5_content_ids = array();
	
			if (!empty($json_data['Common']['Answers'])) {
				foreach ($json_data['Common']['Answers'] as $answer) {
					if (!empty($captured[$answer['id']])) {
						$offset = $captured[$answer['id']][0];
						$content_ids[$answer['id']] = $offset;
					}
				}
			}
			foreach ($json_data['Parts'] as $part) {
				if (!empty($captured[$part['id']])) {
					$offset = $captured[$part['id']][0];
					$content_ids[$part['id']] = $offset;
				}
	
				if (!empty($part['Answers'])) {
					foreach ($part['Answers'] as $answer) {
						if (substr($part['type'], 0, 2) == 'mc') {
							if (!empty($captured[$answer['id']])) {
								$offset = $captured[$answer['id']][0];
								$content_ids[$answer['id']] = $offset;
							}	
						} else {
							$template = json_encode($answer['template']);   // Encode so we can use the same regex as above
	
							while (preg_match('/' . $regex_variable_text . '/', $template, $matches, PREG_OFFSET_CAPTURE)) {
								$match_index = $matches[0][1];
								$match_length = strlen($matches[0][0]);
	
								if (empty($captured[$matches[1][0]])) $new_text = '';
								else $new_text = $captured[$matches[1][0]][0];
	
								$template = substr($template, 0, $match_index) . $new_text . substr($template, $match_index + $match_length);
							}
	
							$content_ids[$answer['id']] = md5($template);
						}
					}
				}
			}

		} else {

			$content_ids = array('question' => null);

		}

		return $content_ids;
	}

	public function getPermissions($questionId, $user_id) {
		if (empty($questionId) || !is_numeric($questionId)) return array();
		if (empty($user_id) || !is_numeric($user_id)) return array();

		$permissions = array();
		
		$this->contain();
		$question = $this->findById($questionId);
		if (empty($question)) return array();
		else if ($question['Question']['open_status'] != 0) $permissions[] = 'can_view';

		$questionHandles = $this->QuestionHandle->find('list', array(
			'fields' => 'QuestionHandle.id',
			'conditions' => array('QuestionHandle.question_id' => $questionId)
		));

		foreach ($questionHandles as $questionHandleId) {
			$thisPermissions = $this->QuestionHandle->getPermissions($questionHandleId, $user_id);
			if (!empty($thisPermissions)) {
				$permissions = array_values(array_unique(array_merge($thisPermissions, $permissions)));
			}
		}
		return $permissions;
	}
	
	public function copy($question_id = null) {
		$success = true;

		$this->contain();
		$question = $this->findById($question_id);
		
		if (!empty($question)) {
			unset($question['Question']['id']);
			unset($question['Question']['created']);

			$question['Question']['parent_id'] = $question_id;	

			$question['Question']['last_used'] = '';
			$question['Question']['stats_count'] = 0;
			$question['Question']['stats_difficulty'] = null;
			$question['Question']['stats_discrimination'] = null;

			if ($question['Question']['open_status'] != 0) {
				$question['Question']['opened'] = date('Y-m-d H:i:s');
			}

			$this->create();
			if (!empty($this->save($question))) return $this->id;
			else return false;
		} else return false;
	}
	
	public function getModifiable($question_handle_id) {
		$this->QuestionHandle->contain();
		$questionHandle = $this->QuestionHandle->findById($question_handle_id);
		$question_id = $questionHandle['QuestionHandle']['question_id'];

	   	$success = true;

		if (empty($questionHandle['QuestionHandle']['user_id']) && 
			empty($questionHandle['QuestionHandle']['community_id']) && 
			empty($questionHandle['QuestionHandle']['sitting_id'])) {
			return false;
		}

		$needsCopy = false;
		if (CakeSession::read("Auth.User.role") != 'admin') {
			$numHandles = $this->QuestionHandle->find('count', array(
				'conditions' => array(
					'QuestionHandle.question_id' => $question_id
				)
			));
			if ($numHandles > 1) $needsCopy = true;
			
			if (!$needsCopy) {
				$numChildren = $this->find('count', array(
					'conditions' => array(
						'Question.parent_id' => $question_id
					)
				));
				if ($numChildren > 0) $needsCopy = true;
			}			
		}
		
		if ($needsCopy) {
			$copy_id = $this->copy($question_id);

			if ($copy_id !== false) {
				$data = array();
				$data['id'] = $question_handle_id;
				$data['question_id'] = $copy_id;
				$this->QuestionHandle->save($data);
			} else $success = false;
		} else $copy_id = $question_id;
		
		if ($success) return $copy_id;
		else return false;
	}
	
	public function clean($question_id) {
		$this->contain();
		$question = $this->findById($question_id);
		$success = true;
		
		if ($question) {
			$canDelete = true;
			
            $handleCount = $this->QuestionHandle->find('count', array(
                'conditions' => array('QuestionHandle.question_id' => $question_id)
            ));
            if ($handleCount > 0) $canDelete = false;

			if ($canDelete) {
				$childCount = $this->find('count', array(
					'conditions' => array('Question.parent_id' => $question_id)
				));
				if ($childCount > 0) $canDelete = false;
			}
			
			if ($canDelete) {
                $success &= $this->delete($question_id);
            }
		} else $success = false;
		
		return $success;
	}
	
	public function checkData($json_data) {

        // Re-encode JSON data as an object, and read the schema

        $json_object = json_decode(json_encode($json_data), false);

        $schema_object = $this->getSchema('/schemas/question');
        if ($schema_object !== false) {

            // Validate with schema

            App::uses('Jsv4', 'Vendor');
            $schema_result = Jsv4::validate($json_object, $schema_object);
            unset($schema_object);
            gc_collect_cycles();

            if (!$schema_result->valid) {
                return json_encode($schema_result->errors);
            }

        } else {

            return "Error loading schema";

        }

		// Check that all internal IDs are unique

		$isValid = true;

		$previousIDs = array();
		if (!empty($json_data['Common']['Answers'])) {
			foreach ($json_data['Common']['Answers'] as $answer) {
				if (in_array($answer['id'], $previousIDs)) $isValid = false;
				else $previousIDs[] = $answer['id'];
			}
		}
		foreach ($json_data['Parts'] as $part) {
			if (in_array($part['id'], $previousIDs)) $isValid = false;
			else $previousIDs[] = $part['id'];

			if (array_key_exists('Answers', $part)) {
				foreach ($part['Answers'] as $answer) {
					if (in_array($answer['id'], $previousIDs)) $isValid = false;
					else $previousIDs[] = $answer['id'];
				}
			}
		}
		if (!$isValid) {
			return "Duplicated internal IDs in question";
		}

		// Check for appropriate question types

		if (array_key_exists('Common', $json_data)) {
			foreach ($json_data['Parts'] as $part) {
				$isValid &= in_array($part['type'], array('mc', 'context'));
			}
		}
		if (!$isValid) {
			return "Unexpected question type with answers in context";
		}

		// Check scored response IDs

		foreach ($json_data['Parts'] as $part) {
			if (array_key_exists('ScoredResponses', $part)) {

				$answer_ids = array();
				if (!empty($json_data['Common']['Answers'])) {
					foreach ($json_data['Common']['Answers'] as $answer) {
						$answer_ids[] = $answer['id'];
					}
				}
				if (!empty($part['Answers'])) {
					foreach ($part['Answers'] as $answer) {
						$answer_ids[] = $answer['id'];
					}
				}
	
				foreach ($part['ScoredResponses'] as $entry) {
					if (array_key_exists('id', $entry)) {
						if (!in_array($entry['id'], $answer_ids)) $isValid = false;
					} else if (array_key_exists('ids', $entry)) {
						if (substr($part['type'], 0, 2) == 'nr') $isValid = false;
						if (count($entry['ids']) <= 1) $isValid = false;
						foreach ($entry['ids'] as $entry_id) {
							if (!in_array($entry_id, $answer_ids)) $isValid = false;
						}
					} else $isValid = false;
				}

			}
		}
		if (!$isValid) {
			return "Invalid IDs in scored responses";
		}

        // Check rubric

		foreach ($json_data['Parts'] as $part) {
            if (($part['type'] == 'wr') && !empty($part['Rubric'])) {
                if ($part['Rubric']['type'] == 'structured') {

                    if (count($part['Rubric']['rows']) == count($part['TypeDetails']['criteria'])) {
                        for ($i = 0; $i < count($part['Rubric']['rows']); ++$i) {
                            if (count($part['Rubric']['rows']) == 1) {
                                $isValid &= !array_key_exists('focus', $part['Rubric']['rows'][$i]);
                            } else $isValid &= !empty($part['Rubric']['rows'][$i]['focus']);

                            $available_values = array();
                            $max_value = $part['TypeDetails']['criteria'][$i]['value'];
                            for ($j = 0; $j <= $max_value; ++$j) $available_values[] = $j;

                            $has_max_description = false;
                            foreach ($part['Rubric']['rows'][$i]['scoring'] as $entry) {
                                $key = array_search($entry['value'], $available_values);
                                if ($key !== false) unset($available_values[$key]);
                                else $isValid = false;

                                if (strlen($entry['description']) == 0) $isValid = false;
                                else if ($entry['value'] == $max_value) $has_max_description = true;
                            }

                            if (!$has_max_description) $isValid = false;
                        }
                    } else $isValid = false;
                    
                }
            }
        }
		if (!$isValid) {
			return "Bad rubric in wr part";
		}

		// Check engine

		if ($json_data['Engine']['type'] == 'code') {
			if (array_key_exists('data', $json_data['Engine'])) {
                return "Invalid engine definition";
            }
		}

		return true;
	}

	public function get($question_ids) {
		$success = true;

		$return_array = true;
		if (!is_array($question_ids)) {
			$question_ids = array($question_ids);
			$return_array = false;
		}

		$this->contain();
		$questions = $this->find('all', array(
			'conditions' => array('Question.id' => $question_ids)
		));	

		// Get question entries

		$resp = array();

		foreach ($questions as $question) {
            $question_id = $question['Question']['id'];
			$thisEntry = array();

			$thisEntry = array(
				'id' => intval($question_id),
				'name' => $question['Question']['name'],
				'open_status' => $question['Question']['open_status'],
				'scoring_hash' => $question['Question']['scoring_hash']
			);

			$json_data = json_decode($question['Question']['json_data'], true);

			if (!empty($json_data['Engine']['Imports'])) {
				$import_ids = $json_data['Engine']['Imports'];
				$json_data['Engine']['Imports'] = $this->expandImports($import_ids);
			}

			$json_string = json_encode($json_data);
			$thisEntry['data_string'] = $json_string;
			$thisEntry['data_hash'] = md5($json_string);
	
			if ($success) {
				if ($return_array) $resp[] = $thisEntry;
				else $resp = $thisEntry;
			} else break;
		}
		
		if ($success) return $resp;
		else return false;
	}

	public function getStatistics($question_ids, $conditions = array()) {
		$success = true;

		$return_array = true;
		if (!is_array($question_ids)) {
			$question_ids = array($question_ids);
			$return_array = false;
		}

		// Get statistics

		$this->contain();
		$questions = $this->find('all', array(
			'conditions' => array('Question.id' => $question_ids)
		));	

		$resp = array();

		foreach ($questions as $question) {
			$question_id = $question['Question']['id'];
			$question_data = json_decode($question['Question']['json_data'], true);

            if (empty($question['Question']['stats_json'])) $stats_json = '{}';
            else $stats_json = $question['Question']['stats_json'];

			$thisEntry = array(
				'Question' => array(
					'count' => intval($question['Question']['stats_count']),
					'difficulty' => floatval($question['Question']['stats_difficulty']),
					'discrimination' => floatval($question['Question']['stats_discrimination']),
					'stats_json' => $stats_json
				),
				'QuestionParts' => array()
			);

			if ($question['Question']['question_content_id'] !== null) {

				$conditions['QuestionStatistics.question_content_id'] = $question['Question']['question_content_id'];

				$questionStatisticsModel = ClassRegistry::init('QuestionStatistics');
				$questionStatisticsModel->contain('ResultSet');
				$statistics = $questionStatisticsModel->find('all', array(
					'conditions' => $conditions
				));
	
				$content_ids = json_decode($question['Question']['content_ids'], true);
				$content_ids = array_flip($content_ids);
				foreach ($content_ids as &$pValue) $pValue = strval($pValue);

				$answerCounts = array();
				foreach ($statistics as $entry) {
					$stats = json_decode($entry['QuestionStatistics']['statistics'], true);
	
					foreach ($stats as $part_content_id => $part_stats) {
						if (!isset($content_ids[$part_content_id])) continue;
						$question_part_id = $content_ids[$part_content_id];

						$part_type = false;
						foreach ($question_data['Parts'] as $part) {
							if ($part['id'] == $question_part_id) $part_type = $part['type'];
						}
	
						if (empty($part_stats)) continue;
						
						if (!array_key_exists($question_part_id, $thisEntry['QuestionParts'])) {
							$thisEntry['QuestionParts'][$question_part_id] = array();
						}
	
						if ($part_type == 'wr') {
	
                            $thisEntry['QuestionParts'][$question_part_id][] = $part_stats;
	
						} else {
	
                            $new_sums = array();
							foreach ($part_stats as $entry) {
								$includeEntry = true;
								if (array_key_exists('ids_matched', $entry)) {
									foreach ($entry['ids_matched'] as &$pID) {
										if (isset($content_ids[$pID])) $pID = $content_ids[$pID];
										else $includeEntry = false;
									}
									sort($entry['ids_matched']);
								}
	
								if ($includeEntry) $new_sums[] = $entry;
							}

                            if (!empty($new_sums)) {
                                $thisEntry['QuestionParts'][$question_part_id][] = $new_sums;
                            }
	
						}
					}
				}
			}

			if ($success) {
				if ($return_array) $resp[$question_id] = $thisEntry;
				else $resp = $thisEntry;
			} else break;
		}

		if ($success) return $resp;
		else return false;
	}

	public function expandImports($import_ids) {
		$jsModuleModel = ClassRegistry::init('JsModule');

		$fullImports = array();
		foreach ($import_ids as $import_id) {
			$importDetails = $jsModuleModel->getCache($import_id);
			if ($importDetails !== false) {
				$fullImports[$importDetails['id']] = array(
					'name' => $importDetails['name'],
					'description' => $importDetails['description'],
					'details' => $importDetails['details'],
					'js' => $importDetails['js']
				);	
			}
		}

		return $fullImports;
	}

    public function find_weaviate($search_text, $options = array()) {
        $secrets = Configure::read('secrets');

        $filters = array();

        // Get filter for part types

        if (array_key_exists('allowed_types', $options)) {
            $allowed_types = $options['allowed_types'];
            $type_filter =
                '{
                    operator: Or,
                    operands: [';

            foreach ($allowed_types as $index => $type) {
                if ($index > 0) $type_filter .= ',';
                $type_filter .= 
                    '{
                        path: ["type"], 
                        operator: Equal,
                        valueText: "' . $type . '"
                    }';
            }

            $type_filter .= ']}';
            $filters[] = $type_filter;
        }

        // Get filter for parts count

        if (array_key_exists('parts_count', $options)) {
            $parts_count = intval($options['parts_count']);
            $count_filter = '{ path: ["parts_count"], operator: Equal, valueInt: ' . $parts_count . ' }';
            $filters[] = $count_filter;
        }

        // Get filter for question location

        if (array_key_exists('folder_id', $options)) {
            $folder_id = $options['folder_id'];
            if ($folder_id == 'open') $folder_filter = '{ path: ["is_open"], operator: Equal, valueBoolean: true }';
            else $folder_filter = '{ path: ["folder_ids"], operator: Equal, valueInt: ' . $folder_id . ' }';
            $filters[] = $folder_filter;
        } else return false;

        // Get filter text including all filters

        $filter_text = '';
        if (count($filters) > 1) {
            
            $filter_text = 
                'where: {
                    operator: And,
                    operands: [' . implode(',', $filters) . ']
                }';
                
        } else if (count($filters) == 1) {

            $filter_text = 
            'where: ' . $filters[0];

        }

        // Get limit for results

        if (array_key_exists('offset', $options)) $offset = $options['offset'];
        else $offset = 0;

        if (array_key_exists('limit', $options)) $limit = $options['limit'];
        else return false;

        // Get search vector

        $search_text_hash = md5($search_text);
        $vector = Cache::read($search_text_hash, 'embedding_vectors');
        if ($vector === false) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $secrets['openai']['embedding_url']);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $secrets['openai']['secret_key']
            ));
    
            $post_data = array("input" => $search_text);
            $post_data += $secrets['openai']['embedding_options'];
    
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    
            $attempt_count = 0;
            while ($vector === false && ++$attempt_count < 5) {
                $result = curl_exec($ch);

                if (!empty($result)) {
                    $data = json_decode($result, true);
                    if (!empty($data['data'])) {
                        $vector = $data['data'][0]['embedding'];
                    }
                }

                if ($vector === false) sleep(1);
            }

            if ($vector !== false) {

                Cache::write($search_text_hash, $vector, 'embedding_vectors');

            } else {

                App::uses('Email', 'Lib');
                $user_email = empty($this->Auth->user('id')) ? null : $this->Auth->user('email');
                Email::queue_error("SmarterMarks: Failed to get search text vector", $user_email, array(
                    'Search text' => $search_text
                ));

            }
        }

        if ($vector === false) return false;

        // Search for questions

        $weaviate_url = $secrets['weaviate']['url'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $weaviate_url . '/v1/graphql');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        // Request hybrid search results. Alpha below 0.5 results in searches for "kinetic energy", for example, 
        // yielding results that relate to energy in general, or even acceleration. Alpha above 0.5 results in
        // a search for "Science 30" yielding science questions more generally.

        // Avoiding Weaviate's 'offset' in the below since it doesn't seem to work. Query limit is multiplied below
        // to allow for some repetition of questions, since the Weaviate database indexes question parts. We iterate
        // until we can get a consistent set for the requested search.

        $query_template = 
            '{
                Get {
                    Question(
                        hybrid: {
                            query: "' . str_replace('"', '\"', $search_text) . '",
                            vector: ' . json_encode($vector) . ',
                            properties: ["keywords"],
                            alpha: 0.5
                        }';

        if (strlen($filter_text) > 0) {
            $query_template .= ',' . $filter_text;
        }

        $query_template .= ', limit: {$limit}';
        $query_template .= 
                    '){
                        question_content_id,
                        part_content_id
                    }
                }
            }';

        $query_template = preg_replace('/\s+/', ' ', $query_template);
        
        $query_limit = $offset + $limit;
        $last_count = 0;

        do {

            $query_limit = ceil($query_limit * 1.2);
            $query_substituted = strtr($query_template, array(
                '{$limit}' => $query_limit
            ));

            $post_data = array("query" => $query_substituted);
    
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
            $result = curl_exec($ch);
    
            $success = true;
            if (!empty($result)) {
    
                $result_data = json_decode($result, true);
                if (!empty($result_data['data']['Get']['Question'])) {

                    $discard_parts = array();
                    $return_parts = array();
    
                    foreach ($result_data['data']['Get']['Question'] as $results_entry) {
                        $question_content_id = $results_entry['question_content_id'];
                        $part_content_id = $results_entry['part_content_id'];

                        $is_discarded = array_key_exists($question_content_id, $discard_parts);
                        $is_returned = array_key_exists($question_content_id, $return_parts);
                        if ($is_discarded || $is_returned) {

                            // Skip this part, since a better match in this question has already been found

                        } else if (count($discard_parts) < $offset) {

                            $discard_parts[$question_content_id] = $part_content_id;

                        } else if (count($return_parts) < $limit) {

                            $return_parts[$question_content_id] = $part_content_id;

                        }
                    }
                    
                } else $success = false;
            } else $success = false;

            if ($success) {
                if (count($return_parts) == $limit) $done = true;
                else if (count($return_parts) == $last_count) $done = true;
                else $done = false;

                $last_count = count($return_parts);
            } else $done = true;

        } while (!$done);

        if ($success) return $return_parts;
        else return false;
    }

	public function beforeSave($options = array()) {
	    if (!empty($this->data['Question']['json_data'])) {
			$json_data = json_decode($this->data['Question']['json_data'], true);
			$check_result = $this->checkData($json_data);
			if ($check_result !== true) {
				$user_email = AuthComponent::user('email');
				if (!array_key_exists('id', $this->data['Question'])) $error_data = array();
				else $error_data = array('id' => $this->data['Question']['id']);
				$error_data['json_data'] = json_encode($json_data);

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Validation failed in Question->save", $user_email, array(
					'result' => $check_result,
					'data' => $error_data
				));
				return false;
			}

			// Update content IDs
			
			$content_ids = $this->getContentIDs($json_data);

    	    $this->data['Question']['question_content_id'] = $content_ids['question'];
			unset($content_ids['question']);
			
    	    $this->data['Question']['content_ids'] = json_encode($content_ids);

			// Update scoring hash

			$scoring_hash = $this->getScoringHash($json_data);
    	    $this->data['Question']['scoring_hash'] = $scoring_hash;
		}

        // Queue index update

        $this->data['Question']['needs_index'] = date('Y-m-d H:i:s');

    	return true;
	}
}

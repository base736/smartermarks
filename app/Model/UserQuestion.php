<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class UserQuestion extends AppModel {
	var $actsAs = array('Containable');

	private $handle_ids = array();

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		),
		'QuestionHandle' => array(
			'className' => 'QuestionHandle',
			'foreignKey' => 'question_handle_id',
		),
		'Folder' => array(
			'className' => 'Folder',
			'foreignKey' => 'folder_id'
		)
	);

	public function beforeFind($queryData) {

		// Sort by ID where the other sort doesn't give an order, so that index is always well-ordered
		
		if (isset($queryData['order']) && is_array($queryData['order'])) {
			if (!isset($queryData['order']['UserQuestion.id'])) {
				$queryData['order']['UserQuestion.id'] = 'desc';
			}
		}

	    return $queryData;
	}

	public function fixHandle($userQuestion_id) {
		$success = true;

		$this->contain(array('Folder', 'QuestionHandle'));
		$userQuestion = $this->findById($userQuestion_id);
				
		if (!empty($userQuestion)) {
			$handleCommunity = $userQuestion['QuestionHandle']['community_id'];
			$handleUser = $userQuestion['QuestionHandle']['user_id'];
			$folderCommunity = $userQuestion['Folder']['community_id'];
			if ($folderCommunity !== null) $folderUser = null;
			else $folderUser = $userQuestion['Folder']['user_id'];
			
			if (($handleCommunity != $folderCommunity) || ($handleUser != $folderUser)) {
				$data = array();
				$data['question_id'] = $userQuestion['QuestionHandle']['question_id'];
				$data['name_alias'] = $userQuestion['QuestionHandle']['name_alias'];
				$data['community_id'] = $folderCommunity;
				$data['user_id'] = $folderUser;
				
				$oldHandle_id = $userQuestion['UserQuestion']['question_handle_id'];
				$userQuestion['UserQuestion']['question_handle_id'] = $this->QuestionHandle->createHandle($data, false);
				$success &= !empty($this->save($userQuestion));
				
				$this->QuestionHandle->clean($oldHandle_id);
			}
		} else $success = false;
		
		return $success;
	}
   
	public function copyToFolder($item_ids, $folder_id, $user_id, $force_copy = false) {
		$success = true;
		$resp = array();

		$data = array();
		$targetCommunity = $this->Folder->getCommunityId($folder_id);
		if ($targetCommunity) {
			$data['community_id'] = $targetCommunity;
			$data['user_id'] = null;
		} else {
			$data['community_id'] = null;
			$data['user_id'] = $user_id;
		}

		// Get a question handle if possible

		$question_handle_id = null;
		$sourceFolderId = false;
		if (!empty($item_ids['question_handle_id'])) {
			$question_handle_id = $item_ids['question_handle_id'];
		} else if (!empty($item_ids['user_question_id'])) {
			$this->contain();
			$userQuestion = $this->findById($item_ids['user_question_id']);
			if (!empty($userQuestion)) {
				$sourceFolderId = $userQuestion['UserQuestion']['folder_id'];
				$question_handle_id = $userQuestion['UserQuestion']['question_handle_id'];
			} else $success = false;
		}

		// Get a question ID and name for the new handle
		
		$question_id = null;
		if ($question_handle_id !== null) {
			$this->QuestionHandle->contain();
			$questionHandle = $this->QuestionHandle->findById($question_handle_id);
			if (!empty($questionHandle)) {
				$question_id = $questionHandle['QuestionHandle']['question_id'];

				$data['name_alias'] = $questionHandle['QuestionHandle']['name_alias'];		

				if (!$force_copy) {
					$needsCopy = true;
					if ($data['community_id'] != $questionHandle['QuestionHandle']['community_id']) $needsCopy = false;
					else if ($data['user_id'] != $questionHandle['QuestionHandle']['user_id']) $needsCopy = false;
				} else $needsCopy = true;

				if ($needsCopy) $question_id = $this->QuestionHandle->Question->copy($question_id);

				if ($question_id !== false) $data['question_id'] = $question_id;
				else $success = false;
			} else $success = false;
		} else if (!empty($item_ids['question_id'])) {
			$question_id = $item_ids['question_id'];

			$this->QuestionHandle->Question->contain();
			$question = $this->QuestionHandle->Question->findById($question_id);
			if (!empty($question)) {
				$data['question_id'] = $question_id;
				$data['name_alias'] = $question['Question']['name'];
			} else $success = false;
		} else $success = false;

		if ($success) {

			// Build or get question handle
		
			$new_handle_id = $this->QuestionHandle->createHandle($data, false);

			// Build UserQuestion

			$data = array(
				'user_id' => $user_id,
				'folder_id' => $folder_id,
				'question_handle_id' => $new_handle_id,
				'moved' => date('Y-m-d H:i:s')
			);

			$this->create();
			$success &= !empty($this->save($data));
			if ($success) {
				$resp['question_id'] = $question_id;
				$resp['question_handle_id'] = $new_handle_id;
				$resp['user_question_id'] = $this->id;

				$resp['source_folder_id'] = $sourceFolderId;
				$resp['target_folder_id'] = $folder_id;
			}

            $this->QuestionHandle->Question->id = $question_id;
            $this->QuestionHandle->Question->saveField('needs_index', date('Y-m-d H:i:s'));
		}
		
		$resp['success'] = $success;
		return $resp;
	}

	public function getPermissions($userQuestionId, $user_id) {
		if (empty($userQuestionId) || !is_numeric($userQuestionId)) return array();
		if (empty($user_id) || !is_numeric($user_id)) return array();

		$this->contain();
		$userQuestion = $this->findById($userQuestionId);
		
		$permissions = array();
		if (!empty($userQuestion)) {			
			$folderModel = ClassRegistry::init('Folder');
			$folderPermissions = $folderModel->getContentPermissions($userQuestion['UserQuestion']['folder_id'], $user_id);
			if ($user_id == $userQuestion['UserQuestion']['user_id']) $permissions = $folderPermissions['owner'];
			else $permissions = $folderPermissions['all'];
		} 
		return $permissions;
	}

	public function beforeSave($options = array()) {
	    if (!empty($this->data['UserQuestion']['folder_id'])) {
			$this->Folder->contain();
			$folder = $this->Folder->findById($this->data['UserQuestion']['folder_id']);
			if ($folder['Folder']['type'] !== 'questions') {
				$user_email = AuthComponent::user('email');

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Bad folder type in UserQuestion->save", $user_email, array(
					'folder ID' => $this->data['UserQuestion']['folder_id']
				));
				return false;
			}
		}

		return true;
	}

	public function beforeDelete($cascade = true) {
		$this->contain();		
		$userQuestion = $this->findById($this->id);
		if (!empty($userQuestion['UserQuestion']['question_handle_id'])) {
			$this->handle_ids[$this->id] = $userQuestion['UserQuestion']['question_handle_id'];
		} else unset($this->handle_ids[$this->id]);
		
		return true;
	}

	public function afterDelete() {
		if (isset($this->handle_ids[$this->id])) {
			$this->QuestionHandle->clean($this->handle_ids[$this->id]);
			unset($this->handle_ids[$this->id]);
		}
	}
}

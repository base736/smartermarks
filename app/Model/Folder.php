<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class Folder extends AppModel {

	var $actsAs = array('Containable');

	public $hasMany = array(
		'Document' => array(
			'className' => 'Document',
			'foreignKey' => 'folder_id'
		),
		'Assessment' => array(
			'className' => 'Assessment',
			'foreignKey' => 'folder_id'
		),
		'UserQuestion' => array(
			'className' => 'UserQuestion',
			'foreignKey' => 'folder_id'
		),
		'Sitting' => array(
			'className' => 'Sitting',
			'foreignKey' => 'folder_id'
		),
		'SittingStudent' => array(
			'className' => 'SittingStudent',
			'foreignKey' => 'folder_id'
		)
	);

	public function copyToFolder($folder_id, $parent_id, $user_id, $copyParent = true) {
		$success = true;
		$resp = array();
		
		if (!empty($parent_id) && !empty($folder_id)) {
			$this->contain(array('Document', 'Assessment', 'UserQuestion', 'Sitting'));
			$folder = $this->findById($folder_id);
			if ($folder) {
				if ($copyParent) {
					$this->contain();
					$parent = $this->findById($parent_id);
		
					unset($folder['Folder']['id']);
					unset($folder['Folder']['created']);
					
					$folder['Folder']['user_id'] = $user_id;
					$folder['Folder']['community_id'] = $parent['Folder']['community_id'];
					$folder['Folder']['parent_id'] = $parent_id;

					$this->create();
					$success &= !empty($this->save($folder));
					if ($success) {
						$parent_id = $this->id;
						$resp['folder_id'] = $parent_id;
					}
				}

				if ($success) {
					foreach ($folder['Document'] as $document) {
						if (!empty($document['deleted'])) continue;
						$result = $this->Document->copyToFolder($document['id'], $parent_id, $user_id);
						$success &= $result['success'];
					}
					
					foreach ($folder['Assessment'] as $assessment) {
						if (!empty($assessment['deleted'])) continue;
						$result = $this->Assessment->copyToFolder($assessment['id'], $parent_id, $user_id);
						$success &= $result['success'];
					}
					
					foreach ($folder['UserQuestion'] as $userQuestion) {
						if (!empty($userQuestion['deleted'])) continue;
						$result = $this->UserQuestion->copyToFolder(array(
							'question_handle_id' => $userQuestion['question_handle_id']
						), $parent_id, $user_id);
						$success &= $result['success'];
					}
					
					foreach ($folder['Sitting'] as $sitting) {
						if (!empty($sitting['deleted'])) continue;
						$result = $this->Sitting->copyToFolder($sitting['id'], $parent_id, $user_id);
						$success &= $result['success'];
					}
					
					$children = $this->find('list', array(
						'fields' => 'Folder.id',
						'conditions' => array('Folder.parent_id' => $folder_id)
					));
					foreach ($children as $childID) {
						$result = $this->copyToFolder($childID, $parent_id, $user_id);
						$success &= $result['success'];
					}
				}
			} else $success = false;
		} else $success = false;
		
		$resp['success'] = $success;
		return $resp;
	}
	
	public function deleteEngine($folder_id, $trash_folders) {

		$success = true;

		// Delete child folders first

		$children = $this->find('list', array(
			'fields' => array('Folder.id'),
			'conditions' => array('Folder.parent_id' => $folder_id)
		));

		foreach ($children as $child_id) {
			$success &= $this->deleteEngine($child_id, $trash_folders);
		}
	
		// Move forms in this folder to the trash

		if (isset($trash_folders['forms'])) {
			$this->Document->updateAll(
				array('Document.folder_id' => $trash_folders['forms']),
				array('Document.folder_id' => $folder_id)
			);
		}

		// Move assessments in this folder to the trash

		if (isset($trash_folders['assessments'])) {
			$this->Assessment->updateAll(
				array('Assessment.folder_id' => $trash_folders['assessments']),
				array('Assessment.folder_id' => $folder_id)
			);
		}
		
		// Move questions in this folder to the trash

		if (isset($trash_folders['questions'])) {
			$this->UserQuestion->contain(array('QuestionHandle'));
			$userquestions = $this->UserQuestion->find('list', array(
				'fields' => array('UserQuestion.id', 'QuestionHandle.question_id'),
				'conditions' => array('UserQuestion.folder_id' => $folder_id)
			));
			if (!empty($userquestions)) {
				$data = array();
				$data['folder_id'] = $trash_folders['questions'];
				foreach($userquestions as $userquestion_id => $question_id) {
					$data['id'] = $userquestion_id;
					$this->UserQuestion->save($data);
					$this->UserQuestion->fixHandle($userquestion_id);
				}
		
				$questionModel = ClassRegistry::init('Question');
                $db = $questionModel->getDataSource();
                $questionModel->updateAll(
                    array('needs_index' => $db->value(date('Y-m-d H:i:s'), 'string')),
                    array('Question.id' => array_values($userquestions))
                );
			}
		}
			
		// Move sittings in this folder to the trash

		if (isset($trash_folders['sittings'])) {
			$this->Sitting->updateAll(
				array('Sitting.folder_id' => $trash_folders['sittings']),
				array('Sitting.folder_id' => $folder_id)
			);
		}
		
		// Move sitting students in this folder to the trash

		if (isset($trash_folders['sittingstudents'])) {
			$this->Sitting->SittingStudent->updateAll(
				array('SittingStudent.folder_id' => $trash_folders['sittingstudents']),
				array('SittingStudent.folder_id' => $folder_id)
			);
		}
			
		// Delete this folder
		
		$success &= $this->delete($folder_id);

		return $success;
	}	
		
	public function getPermissions($folderId, $user_id) {
		if (empty($folderId) || !is_numeric($folderId)) return array();
		if (empty($user_id) || !is_numeric($user_id)) return array();

		$this->contain();
		$folder = $this->findById($folderId);
		if (!empty($folder)) {
			if (!empty($folder['Folder']['parent_id'])) $parentId = $folder['Folder']['parent_id'];
			else {
				if ($folder['Folder']['community_id'] != null) {
					$communityUserModel = ClassRegistry::init('CommunityUser');
					$communityUserModel->contain();
					$communityUser = $communityUserModel->find('first', array(
						'conditions' => array(
							'CommunityUser.user_id' => $user_id,
							'CommunityUser.community_id' => $folder['Folder']['community_id']
						)
					));
					
					if ($communityUser) $parentId = $communityUser['CommunityUser']['folder_id'];
					else return array();
				} else $parentId = $folderId;
			}
		} else return array();
		
		$parentPermissions = $this->getContentPermissions($parentId, $user_id);

		if ($user_id == $folder['Folder']['user_id']) $permissions = $parentPermissions['owner'];
		else $permissions = $parentPermissions['all'];

		if (empty($folder['Folder']['parent_id'])) {

			// Prevent deletion and editing of folders that don't have a parent

			$disallowed = ['can_delete', 'can_edit'];
			foreach ($permissions as $key => $value) {
				if (array_search($value, $disallowed) !== false) unset($permissions[$key]);
			}
		}

		return $permissions;
	}

	public function getContentPermissions($folderId, $user_id) {
		$permissions = array('all' => array(), 'owner' => array());
		$allPermissions = array('can_view', 'can_add', 'can_export', 'can_delete', 'can_edit');

		$userModel = ClassRegistry::init('User');
		$userModel->contain();
		$user = $userModel->findById($user_id);
		if ($user) {
			if ($user['User']['role'] == 'admin') {
				$permissions['all'] = $allPermissions;
				$permissions['all'][] = 'all_scores';
				$permissions['owner'] = $permissions['all'];
			} else {
				$this->contain();
				$folder = $this->findById($folderId);
				if ($folder) {
					if ($folder['Folder']['community_id'] != null) {
						$communityModel = ClassRegistry::init('Community');
						$permissions = $communityModel->getPermissions($folder['Folder']['community_id'], $user_id);
					} else if ($user_id == $folder['Folder']['user_id']) {
						$permissions['all'] = $allPermissions;
						$permissions['owner'] = $permissions['all'];
					}
				}
			}
		}
				
		return $permissions;
	}
	
	public function getCommunityId($folderId) {
		$this->contain();
		$folder = $this->findById($folderId);
		if ($folder) return $folder['Folder']['community_id'];
		else return null;
	}
	
	public function findCommunityUser($folderId, $user_id, $tree, $communityId = null) {
		foreach ($tree as $node) {
			if (isset($node['data']['community_id'])) $thisCommunityId = $node['data']['community_id'];
			else $thisCommunityId = $communityId;
			
			if ($node['data']['folder_id'] == $folderId) {
				if ($thisCommunityId == null) return null;
				else {
					$communityUserModel = ClassRegistry::init('CommunityUser');
					$communityUserModel->contain(array('Community'));
					$communityUser = $communityUserModel->find('first', array(
						'conditions' => array(
							'CommunityUser.user_id' => $user_id,
							'CommunityUser.community_id' => $thisCommunityId
						)
					));
					return $communityUser;
				}
			} else if (isset($node['children'])) {
				$result = $this->findCommunityUser($folderId, $user_id, $node['children'], $thisCommunityId);
				if ($result != null) return $result;
			}
		}
		return null;
	}

	private function getRootFolders($user_id, $type) {
		$userModel = ClassRegistry::init('User');

		$userModel->contain();
		$user = $userModel->findById($user_id);
		if (empty($user)) return false;
		else if (empty($user['User']['folder_ids'])) $folder_ids = array();
		else $folder_ids = json_decode($user['User']['folder_ids'], true);

		if (!array_key_exists($type, $folder_ids)) {
			if ($type == 'communities') {

				$data = array();
				$data['name'] = 'Communities';
				$data['type'] = 'communities';
				$data['user_id'] = $user_id;
				$data['community_id'] = null;
				$this->create();
				$this->save($data);

				$folder_ids[$type] = $this->id;
		
			} else $folder_ids[$type] = array();
		}

		if ($type != 'communities') {
			$required_types = array('home', 'archive', 'trash');
			foreach ($required_types as $subtype) {
				if (!array_key_exists($subtype, $folder_ids[$type])) {

					$data = array();
					$data['name'] = ucfirst($subtype);
					$data['type'] = $type;
					$data['user_id'] = $user_id;
					$data['community_id'] = null;
					$this->create();
					$this->save($data);

					$folder_ids[$type][$subtype] = $this->id;
			
				}
			}
		}

		$new_json = json_encode($folder_ids);
		if ($new_json != $user['User']['folder_ids']) {
			$userModel->id = $user_id;
			$userModel->saveField('folder_ids', $new_json);
		}

		return $folder_ids[$type];
	}
	
	public function getUserFolder($user_id, $type, $subtype = false) {
		$rootFolders = $this->getRootFolders($user_id, $type);

		if ($rootFolders == false) return false;
		else if ($type == 'communities') return $rootFolders;
		else if ($subtype === false) return false;
		else if (array_key_exists($subtype, $rootFolders)) {
			return $rootFolders[$subtype];
		} else return false;
	}

	public function getNodes($user_id, $type) {
		$folderNodes = array();

		$rootFolders = $this->getRootFolders($user_id, $type);
		if ($rootFolders !== false) {
			$rootFolders['communities'] = $this->getRootFolders($user_id, 'communities');

			$communityUserModel = ClassRegistry::init('CommunityUser');
			$communityUserModel->contain(array('Community'));
			$communities = $communityUserModel->find('all', array(
				'conditions' => array(
					'CommunityUser.user_id' => $user_id,
					'Community.deleted' => null
				)
			));
			
			$edge_folders = array_values($rootFolders);
			foreach ($communities as $community) {
				$folder_ids = json_decode($community['Community']['folder_ids'], true);
				if (array_key_exists($type, $folder_ids)) $edge_folders[] = $folder_ids[$type];
			}

			$folder_ids = array();
			do {
				$folder_ids = array_unique(array_merge($folder_ids, $edge_folders));
				$edge_folders = array_values($this->find('list', array(
					'fields' => 'Folder.id',
					'conditions' => array('Folder.parent_id' => $edge_folders)
				)));
			} while (count($edge_folders) > 0);

			$this->contain();
			$rawFolders = $this->find('all', array(
				'conditions' => array('Folder.id' => $folder_ids)
			));
			
			$folders = array();
			foreach ($rawFolders as $folder) {
				$folder_id = $folder['Folder']['id'];
				$folders[$folder_id] = $folder['Folder'];
			}

			foreach ($rootFolders as $subtype => $folder_id) {
				$folder_id = intval($folder_id);

				$folderPermissions = $this->getPermissions($folder_id, $user_id);
				$contentPermissions = $this->getContentPermissions($folder_id, $user_id);

				$folderNodes[] = array(
					'id' => md5($folder_id),
					'text' => $folders[$folder_id]['name'],
					'type' => ($subtype == 'trash') ? 'trash' : 'folder',
					'data' => array(
						'folder_id' => $folder_id,
						'parent_id' => null,
						'folder_permissions' => $folderPermissions,
						'content_permissions' => $contentPermissions
					)
				);
			}

			$index = 0;
			while ($index < count($folderNodes)) {
				$parentNode = $folderNodes[$index++];
				$parent_folder_id = intval($parentNode['data']['folder_id']);

				foreach ($folders as $folder_id => $folder) {
					$folder_id = intval($folder_id);
					
					if ($folder['parent_id'] == $parent_folder_id) {
						$folderPermissions = $this->getPermissions($folder_id, $user_id);
						$contentPermissions = $this->getContentPermissions($folder_id, $user_id);

						$folderNodes[] = array(
							'id' => md5($parentNode['id'] . $folder_id),
							'parent_node' => $parentNode['id'],
							'text' => $folder['name'],
							'type' => 'folder',
							'data' => array(
								'folder_id' => $folder_id,
								'parent_id' => $parent_folder_id,
								'folder_permissions' => $folderPermissions,
								'content_permissions' => $contentPermissions
							)
						);		
					}
				}

				foreach ($communities as $community) {
					if ($community['CommunityUser']['folder_id'] == $parent_folder_id) {
						$communityId = $community['Community']['id'];
						$communityFolders = json_decode($community['Community']['folder_ids'], true);
						if (array_key_exists($type, $communityFolders)) {
							$community_folder_id = $communityFolders[$type];
							$folderPermissions = $this->getPermissions($community_folder_id, $user_id);
							$contentPermissions = $this->getContentPermissions($community_folder_id, $user_id);
	
							// Get new folder node
							
							$newNode = array(
								'id' => md5($parentNode['id'] . $community_folder_id),
								'parent_node' => $parentNode['id'],
								'text' => $community['Community']['name'],
								'type' => 'community',
								'data' => array(
									'folder_id' => $community_folder_id,
									'parent_id' => $parent_folder_id,
									'community_user_id' => $community['CommunityUser']['id'],
									'community_id' => $community['Community']['id'],
									'is_admin' => ($community['CommunityUser']['is_admin'] == 1),
									'user_add_policy' => $community['Community']['user_add_policy'],
									'folder_permissions' => $folderPermissions,
									'content_permissions' => $contentPermissions
								)
							);
	
							// Get admin details if required
	
							if ($community['CommunityUser']['is_admin'] == 1) {
								$communityUserModel->contain();
								$communityUsers = $communityUserModel->find('all', array(
									'conditions' => array('community_id' => $community['Community']['id'])
								));
								
								$communityAdminCount = 0;
								$communityOtherCount = 0;
								foreach ($communityUsers as $thisUser) {
									if ($thisUser['CommunityUser']['is_admin'] == 1) $communityAdminCount++;
									else $communityOtherCount++;
								}
								$newNode['data']['admin_count'] = $communityAdminCount;
								$newNode['data']['other_count'] = $communityOtherCount;
							}
	
							// Add node
	
							$folderNodes[] = $newNode;
						}
					}
				}
			}
		}

		return $folderNodes;
	}
	
	public function getTree($folderNodes) {

		$nodeMap = array();
		foreach ($folderNodes as $node) {
			$nodeMap[$node['id']] = $node;
		}

		$folderTree = array();
		foreach ($nodeMap as $id => &$pNode) {
			if (!empty($pNode['parent_node'])) {
				$parent_id = $pNode['parent_node'];
				unset($pNode['parent_node']);
				if (!isset($nodeMap[$parent_id]['children'])) $nodeMap[$parent_id]['children'] = array();
				$nodeMap[$parent_id]['children'][] =& $pNode;
			} else $folderTree[] =& $pNode;
		}

		return $folderTree;
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['Folder']['id'])) {
			$this->contain();
			$folder = $this->findById($this->data['Folder']['id']);
			$folder_type = $folder['Folder']['type'];

			if (!empty($this->data['Folder']['type'])) {
				if ($folder_type !== $this->data['Folder']['type']) {
					$user_email = AuthComponent::user('email');
	
					App::uses('Email', 'Lib');
					Email::queue_error("SmarterMarks: Changing folder type in Folder->save", $user_email, array(
						'old type' => $folder_type,
						'folder data' => $this->data['Folder']
					));
					return false;
				}
			}
		} else if (!empty($this->data['Folder']['type'])) {
			$folder_type = $this->data['Folder']['type'];
		} else return false;

		if (!empty($this->data['Folder']['parent_id'])) {
			$this->contain();
			$parent = $this->findById($this->data['Folder']['parent_id']);
			if ($parent['Folder']['type'] !== $folder_type) {
				$user_email = AuthComponent::user('email');

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Bad parent type in Folder->save", $user_email, array(
					'folder data' => $this->data['Folder']
				));
				return false;
			}
		}
	}

	public function beforeDelete($cascade = true) {
		if ($cascade) {
			$folderID = $this->id;
			
			$communityUserModel = ClassRegistry::init('CommunityUser');
			$communityUser_ids = $communityUserModel->find('list', array(
				'fields' => array('CommunityUser.id'),
				'conditions' => array('CommunityUser.folder_id' => $folderID)
			));
			foreach ($communityUser_ids as $communityUser_id) {
				$communityUserModel->delete($communityUser_id);
			}

			$child_ids = $this->find('list', array(
				'fields' => array('Folder.id'),
				'conditions' => array('Folder.parent_id' => $folderID)
			));
			foreach ($child_ids as $child_id) {
				$this->delete($child_id);
			}

			$deleted_date = "'" . date('Y-m-d H:i:s') . "'";     // Quotes needed for updateAll

			$this->Document->updateAll(
				array('Document.deleted' => $deleted_date),
				array(
					'Document.folder_id' => $folderID,
					'Document.deleted' => NULL
				)
			);

			$this->Assessment->updateAll(
				array('Assessment.deleted' => $deleted_date),
				array(
					'Assessment.folder_id' => $folderID,
					'Assessment.deleted' => NULL
				)
			);

			$this->UserQuestion->updateAll(
				array('UserQuestion.deleted' => $deleted_date),
				array(
					'UserQuestion.folder_id' => $folderID,
					'UserQuestion.deleted' => NULL
				)
			);

			$this->Sitting->updateAll(
				array('Sitting.deleted' => $deleted_date),
				array(
					'Sitting.folder_id' => $folderID,
					'Sitting.deleted' => NULL
				)
			);

			$this->SittingStudent->updateAll(
				array('SittingStudent.deleted' => $deleted_date),
				array(
					'SittingStudent.folder_id' => $folderID,
					'SittingStudent.deleted' => NULL
				)
			);

			$this->id = $folderID;
		}

		return true;
	}
}

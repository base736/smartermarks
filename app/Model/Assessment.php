<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class Assessment extends AppModel {
	var $actsAs = array('Containable');

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
		'Folder' => array(
			'className' => 'Folder',
			'foreignKey' => 'folder_id',
		)
	);

	public $hasMany = array(
		'AssessmentItem' => array(
			'className' => 'AssessmentItem',
			'foreignKey' => 'assessment_id',
			'dependent' => true
		),
		'Sitting' => array(
			'className' => 'Sitting',
			'foreignKey' => 'assessment_id'
		)
	);
	
	public function getPermissions($assessmentId, $user_id) {
		if (empty($assessmentId) || !is_numeric($assessmentId)) return array();
		if (empty($user_id) || !is_numeric($user_id)) return array();

		$sittingModel = ClassRegistry::init('Sitting');
		$sittings = $sittingModel->find('list', array(
			'fields' => array('Sitting.folder_id', 'Sitting.user_id'),
			'conditions' => array('Sitting.assessment_id' => $assessmentId)
		));

		$this->contain();
		$assessment = $this->findById($assessmentId);

		$permissions = array();
		if (!empty($assessment['Assessment']['folder_id'])) {			
			$folderModel = ClassRegistry::init('Folder');
			$folderPermissions = $folderModel->getContentPermissions($assessment['Assessment']['folder_id'], $user_id);
			if ($user_id == $assessment['Assessment']['user_id']) $permissions = $folderPermissions['owner'];
			else $permissions = $folderPermissions['all'];
		} 

		$folderModel = ClassRegistry::init('Folder');
		foreach ($sittings as $folderId => $ownerId) {
			$folderPermissions = $folderModel->getContentPermissions($folderId, $user_id);
			if ($user_id == $ownerId) $permissions = array_merge($folderPermissions['owner'], $permissions);
			else $permissions = array_merge($folderPermissions['all'], $permissions);
		}
		$permissions = array_values(array_unique($permissions));
		return $permissions;
	}

	public function copy($assessment_id = null, $handleData) {
		$this->contain(array('AssessmentItem' => 'QuestionHandle'));
		$assessment = $this->findById($assessment_id);
		
		if (!$assessment) {
			return array('success' => false);
		}

		$success = true;
		$resp = array();

		unset($assessment['Assessment']['id']);
		unset($assessment['Assessment']['created']);

		$assessment['Assessment']['user_id'] = null;
		$assessment['Assessment']['folder_id'] = null;
		$assessment['Assessment']['moved'] = null;

		$this->create();
		if (!empty($this->save($assessment))) {
			$copyID = $this->id;

			$json_data = json_decode($assessment['Assessment']['json_data'], true);

			// Copy AssessmentItem entries
	
			$questionHandleMap = array();
			foreach ($assessment['AssessmentItem'] as $a => $b) {
				$oldItemID = $b['id'];
				$oldHandleID = $b['question_handle_id'];

				$data = $handleData;
				$data['question_id'] = $b['QuestionHandle']['question_id'];
				$data['name_alias'] = $b['QuestionHandle']['name_alias'];
				
				$handleID = $this->AssessmentItem->QuestionHandle->createHandle($data, false);

				$b['id'] = NULL;
				$b['assessment_id'] = $copyID;
				$b['question_handle_id'] = $handleID;

				$this->AssessmentItem->create();
				if (!empty($this->AssessmentItem->save($b))) {
					$questionHandleMap[$oldHandleID] = $handleID;
				} else $success = false;
			}
	
			foreach ($json_data['Sections'] as &$pSection) {
				if ($pSection['type'] == 'page_break') continue;

				foreach ($pSection['Content'] as &$pEntry) {
					if (array_key_exists('question_handle_id', $pEntry)) {
						if (isset($questionHandleMap[$pEntry['question_handle_id']])) {
							$pEntry['question_handle_id'] = $questionHandleMap[$pEntry['question_handle_id']];
						} else $success = false;
					} else if (array_key_exists('question_handle_ids', $pEntry)) {
						$new_question_handle_ids = array();
						foreach ($pEntry['question_handle_ids'] as $question_handle_id) {
							if (isset($questionHandleMap[$question_handle_id])) {
								$new_question_handle_ids[] = $questionHandleMap[$question_handle_id];
							} else $success = false;
						}
						$pEntry['question_handle_ids'] = $new_question_handle_ids;
					}
				}
			}

		} else {

			$copyID = false;
			$success = false;

		}

		// Finish up

		if ($success) {
			$this->id = $copyID;
			$this->saveField('json_data', json_encode($json_data));
			$resp['copyID'] = $copyID;
		} else if ($copyID !== false) {
			$this->delete($copyID);
		}

		$resp['success'] = $success;

		return $resp;
	}
   
    public function copyToFolder($assessment_id, $folder_id, $user_id) {
		$success = true;
		$resp = array();

		$this->contain();
		$assessment = $this->findById($assessment_id);

		$sourceFolderId = $assessment['Assessment']['folder_id'];

		$targetCommunity = $this->Folder->getCommunityId($folder_id);
		$sourceCommunity = $this->Folder->getCommunityId($sourceFolderId);

		$handleData = array();
		if ($targetCommunity) $handleData['community_id'] = $targetCommunity;
		else $handleData['user_id'] = $user_id;

		$result = $this->copy($assessment_id, $handleData);
		$success = $result['success'];

		if ($success) {
			$copyID = $result['copyID'];

			$data = array(
				'id' => $copyID,
				'user_id' => $user_id,
				'folder_id' => $folder_id,
				'moved' => date('Y-m-d H:i:s')
			);	
			
			if (!empty($this->save($data))) {
				$resp['assessment_id'] = $copyID;
				$resp['source_folder_id'] = $sourceFolderId;
				$resp['target_folder_id'] = $folder_id;	
			} else {
				$this->delete($copyID);
				$success = false;
			}

		}

		$resp['success'] = $success;
		return $resp;
    }

	public function fixHandles($assessment_id) {
		$success = true;

		$this->contain(array('Folder', 'AssessmentItem' => 'QuestionHandle'));
		$assessment = $this->findById($assessment_id);
		
		if (!empty($assessment)) {
			$folderCommunity = $assessment['Folder']['community_id'];
			if ($folderCommunity !== null) $folderUser = null;
			else $folderUser = $assessment['Folder']['user_id'];

			$data = array();
			$data['community_id'] = $folderCommunity;
			$data['user_id'] = $folderUser;

			$new_handle_map = array();
			$questionHandleModel = ClassRegistry::init('QuestionHandle');
			$assessmentItemModel = ClassRegistry::init('AssessmentItem');
			foreach ($assessment['AssessmentItem'] as $item) {
				$needs_new = false;
				if ($item['QuestionHandle']['community_id'] != $folderCommunity) $needs_new = true;
				else if ($item['QuestionHandle']['user_id'] != $folderUser) $needs_new = true;

				if ($needs_new) {
					$old_handle_id = $item['QuestionHandle']['id'];

					$data['question_id'] = $item['QuestionHandle']['question_id'];
					$data['name_alias'] = $item['QuestionHandle']['name_alias'];
					$new_handle_id = $questionHandleModel->createHandle($data, false);

					$item_data = array(
						'id' => $item['id'],
						'question_handle_id' => $new_handle_id
					);
					$success &= !empty($assessmentItemModel->save($item_data));
					if ($success) {
						$new_handle_map[$old_handle_id] = $new_handle_id;
						$questionHandleModel->clean($old_handle_id);
					}
				}
			}

			$json_data = json_decode($assessment['Assessment']['json_data'], true);
			foreach ($json_data['Sections'] as &$pSection) {
				if ($pSection['type'] == 'page_break') continue;
				foreach ($pSection['Content'] as &$pEntry) {
					if (array_key_exists('question_handle_id', $pEntry)) {
						$old_handle_id = $pEntry['question_handle_id'];
						if (array_key_exists($old_handle_id, $new_handle_map)) {
							$pEntry['question_handle_id'] = $new_handle_map[$old_handle_id];
						}
					} else if (array_key_exists('question_handle_ids', $pEntry)) {
						$new_question_handle_ids = array();
						foreach ($pEntry['question_handle_ids'] as $old_handle_id) {
                            if (array_key_exists($old_handle_id, $new_handle_map)) {
								$new_question_handle_ids[] = $new_handle_map[$old_handle_id];
							} else $new_question_handle_ids[] = $old_handle_id;
						}
						$pEntry['question_handle_ids'] = $new_question_handle_ids;
					}
				}
			}

			$new_json_string = json_encode($json_data);
			if ($new_json_string != $assessment['Assessment']['json_data']) {
				$this->id = $assessment_id;
				$this->saveField('json_data', $new_json_string, array(
					'callbacks' => false
				));	

				$version = md5(microtime() . $assessment_id);
				Cache::write('assessments_' . $assessment_id, $version, 'version_ids');
			}
		} else $success = false;

		return $success;
	}

	public function checkData($json_data) {

        // Re-encode JSON data as an object, and read the schema

        $json_object = json_decode(json_encode($json_data), false);

        $schema_object = $this->getSchema('/schemas/assessment');
        if ($schema_object !== false) {

            // Validate with schema

            App::uses('Jsv4', 'Vendor');
            $schema_result = Jsv4::validate($json_object, $schema_object);
            unset($schema_object);
            gc_collect_cycles();

            if (!$schema_result->valid) {
                return json_encode($schema_result->errors);
            }

        } else {

            return "Error loading schema";

        }

		// Check question handle IDs

        $isValid = true;

		$question_handle_ids = array();
		$repeated_id = false;

		foreach ($json_data['Sections'] as $section) {
			if ($section['type'] == 'page_break') continue;

			foreach ($section['Content'] as $entry) {
				if ($entry['type'] == 'item') {
                    $question_handle_id = $entry['question_handle_id'];
                    if (!in_array($question_handle_id, $question_handle_ids)) {
                        $question_handle_ids[] = $question_handle_id;
                    } else $repeated_id = true;
				} else if ($entry['type'] == 'item_set') {
                    foreach ($entry['question_handle_ids'] as $question_handle_id) {
                        if (!in_array($question_handle_id, $question_handle_ids)) {
                            $question_handle_ids[] = $question_handle_id;
                        } else $repeated_id = true;
                    }
				}
			}
		}
		
		if (!$isValid) {
			return "Invalid item entry in assessment content";
		} else if ($repeated_id) {
			return "Duplicate question handle ID in assessment";
		}

		// Check outcome content IDs

		$content_ids = array();
		foreach ($json_data['Sections'] as $section) {
			if ($section['type'] == 'page_break') continue;
			foreach ($section['Content'] as $entry) {
				if (in_array($entry['type'], array('item', 'item_set'))) {
					$content_ids[] = $entry['id'];
				}
			}
		}

		$outcome_ids = array();
		foreach ($json_data['Outcomes'] as $outcome) {
			$outcome_ids[] = $outcome['id'];
			foreach ($outcome['parts'] as $entry) {
				if (!in_array($entry['content_id'], $content_ids)) $isValid = false;
			}
		}

		if (!$isValid) {
			return "Unexpected content ID in assessment outcome";
		}

		// Check weights

		if (in_array($json_data['Scoring']['type'], array('Sections', 'Outcomes'))) {

            if ($json_data['Scoring']['type'] == 'Sections') {

                $section_ids = array();
                foreach ($json_data['Sections'] as $section) {
                    if ($section['type'] != 'page_break') $section_ids[] = $section['id'];
                }
                
                foreach ($json_data['Scoring']['weights'] as $weight) {
                    if (!in_array($weight['section_id'], $section_ids)) $isValid = false;
                }

            } else if ($json_data['Scoring']['type'] == 'Outcomes') {

                foreach ($json_data['Scoring']['weights'] as $weight) {
                    if (!in_array($weight['outcome_id'], $outcome_ids)) $isValid = false;
                }

            }
		}

		if (!$isValid) {
			return "Unexpected weights in assessment template";
		}

		return true;
	}

	public function beforeSave($options = array()) {
	    if (!empty($this->data['Assessment']['folder_id'])) {
			$this->Folder->contain();
			$folder = $this->Folder->findById($this->data['Assessment']['folder_id']);
			if ($folder['Folder']['type'] !== 'assessments') {
				$user_email = AuthComponent::user('email');

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Bad folder type in Assessment->save", $user_email, array(
					'folder ID' => $this->data['Assessment']['folder_id']
				));
				return false;
			}
		}
	    if (!empty($this->data['Assessment']['json_data'])) {
			$json_data = json_decode($this->data['Assessment']['json_data'], true);
			$check_result = $this->checkData($json_data);
			if ($check_result !== true) {
				$user_email = AuthComponent::user('email');
				if (!array_key_exists('id', $this->data['Assessment'])) $error_data = array();
				else $error_data = array('id' => $this->data['Assessment']['id']);
				$error_data['json_data'] = json_encode($json_data);

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Validation failed in Assessment->save", $user_email, array(
					'result' => $check_result,
					'data' => $error_data
				));
				return false;
			}
		}

    	return true;
	}
}

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class ResultData extends AppModel {

	var $actsAs = array('Containable');

	public $useTable = 'result_data';

	private $version_data_ids = array();

	public $belongsTo = array(
		'VersionData' => array(
			'className' => 'VersionData',
			'foreignKey' => 'version_data_id'
		),
		'ResultSet' => array(
			'className' => 'ResultSet',
			'foreignKey' => 'result_set_id'
		)
	);

	public function emptyResults($layout) {
		if ($layout !== null) {
			$results = array();
			foreach ($layout['Sections'] as $section) {
				foreach ($section['Content'] as $question) {
					if (substr($question['type'], 0, 2) == 'mc') {
						$results[] = '';
					} else if (substr($question['type'], 0, 2) == 'nr') {
						$nr_digits = $question['TypeDetails']['nrDigits'];
						$results[] = str_repeat(' ', $nr_digits);
					} else {
						$num_criteria = count($question['TypeDetails']['criteria']);
						$results[] = str_repeat(' ', 2 * $num_criteria);
					}
				}
			}
		} else $results = false;

		return $results;
	}

	public function beforeDelete($cascade = true) {
		$this->contain();
		$result_data = $this->findById($this->id);

		if (!empty($result_data['ResultData']['version_data_id'])) {
			$this->version_data_ids[$this->id] = $result_data['ResultData']['version_data_id'];
		} else {
			unset($this->version_data_ids[$this->id]);
		}

		return true;
	}

	public function afterDelete() {
		if (isset($this->version_data_ids[$this->id])) {
			$version_data_id = $this->version_data_ids[$this->id];
			$this->VersionData->clean($version_data_id);
		}
	}

	public function afterSave($created, $options = array()) {

		// Queue stats update

        $this->contain(array('ResultSet' => 'ResponsePaper'));
		$resultData = $this->findById($this->id);
		if ($resultData['ResultSet']['stats_status'] != 'exclude') {
			$this->ResultSet->id = $resultData['ResultSet']['id'];
			$this->ResultSet->saveField('stats_status', 'update');
		}

        if (!empty($resultData['ResultSet']['ResponsePaper'])) {

			// Update score caches

			$user_id = AuthComponent::user('id');
			$cache_data = Cache::read($user_id, 'report_data');
			if (!empty($cache_data)) {
				foreach ($resultData['ResultSet']['ResponsePaper'] as $thisData) {
					$score_id = $thisData['score_id'];
					unset($cache_data[$score_id]);
				}
				Cache::write($user_id, $cache_data, 'report_data');
			}
		}
	}
}

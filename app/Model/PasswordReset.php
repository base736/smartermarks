<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class PasswordReset extends AppModel {

	var $actsAs = array('Containable');

	var $name = 'PasswordReset';
	var $recursive = -1;

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

	public function getReset($userID) {
		$token = bin2hex(random_bytes(16));

		$data = array();
		$data['user_id'] = $userID;
		$data['token'] = $token;

		$this->create();
		if (!empty($this->save($data))) return $token;
		else return false;
	}
}

?>

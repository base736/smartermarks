<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');
/**
 * Group Model
 *
 * @property Document $Document
 */
class Group extends AppModel {

	var $actsAs = array('Containable', 'Search.Searchable');
	public $displayField = 'name';

	public $filterArgs = array(
    	'search' => array('type' => 'like', 'field' => array('name')),
    );

	public $hasMany = array(
		'UserGroup' => array(
			'className' => 'UserGroup',
			'foreignKey' => 'group_id',
			'dependent' => true
		)
	);

	public function addMember($group_id, $user_id, $is_admin = 0) {
		$userGroupModel = ClassRegistry::init('UserGroup');

		// Check and update existing user groups

		$userGroups = $userGroupModel->find('all', array(
			'conditions' => array(
				'user_id' => $user_id
			)
		));

		$user_group_id = false;
		$check_group_ids = array($group_id);
		foreach ($userGroups as $userGroup) {
			if ($userGroup['UserGroup']['group_id'] == $group_id) {
				$user_group_id = $userGroup['UserGroup']['id'];
				if ($userGroup['UserGroup']['is_active'] == 0) {
					$userGroupModel->id = $user_group_id;
					$userGroupModel->saveField('is_active', 1);
				}
			} else if ($userGroup['UserGroup']['is_admin'] == 0) {
				$userGroupModel->id = $userGroup['UserGroup']['id'];
				$userGroupModel->saveField('is_active', 0);
				$check_group_ids[] = $userGroup['UserGroup']['group_id'];
			}
		}

		// Create UserGroup if necessary

		if ($user_group_id === false) {
			$data = array();
			$data['user_id'] = $user_id;
			$data['group_id'] = $group_id;
			$data['is_admin'] = $is_admin;
			$data['is_active'] = 1;
			$userGroupModel->create();
			$userGroupModel->save($data);

			$user_group_id = $userGroupModel->id;
		}

		// Update is_active for groups

		$check_group_ids = array_unique($check_group_ids);
		foreach ($check_group_ids as $check_id) {
			$this->contain('UserGroup');
			$group = $this->findById($check_id);
			$active_count = 0;
			$inactive_count = 0;
			foreach ($group['UserGroup'] as $userGroup) {
				if ($userGroup['is_active'] == 1) $active_count++;
				else $inactive_count++;
			}

			if ($inactive_count >= $active_count) {
				if ($group['Group']['is_active'] == 1) {
					$this->id = $check_id;
					$this->saveField('is_active', 0);
				}
			} else {
				if ($group['Group']['is_active'] == 0) {
					$this->id = $check_id;
					$this->saveField('is_active', 1);
				}
			}
		}

		return $user_group_id;
	}

	public function clean($group_id) {
		$userCount = $this->User->find('count', array(
			'conditions' => array('User.group_id' => $group_id)
		));
		if ($userCount == 0) $this->delete($group_id);
	}
}

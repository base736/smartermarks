-- MySQL dump 10.13  Distrib 5.7.39, for osx10.12 (x86_64)
--
-- Host: localhost    Database: smartermarks_local
-- ------------------------------------------------------
-- Server version	5.7.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_statistics`
--

DROP TABLE IF EXISTS `admin_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_statistics` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL,
  `json_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=510 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assessment_items`
--

DROP TABLE IF EXISTS `assessment_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_items` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `assessment_id` int(11) DEFAULT NULL,
  `question_handle_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_question_handle_id` (`question_handle_id`),
  KEY `idx_assessment_id` (`assessment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4265056 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assessments`
--

DROP TABLE IF EXISTS `assessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `folder_id` int(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `moved` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `is_new` int(5) NOT NULL DEFAULT '0',
  `save_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_data` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_folder_id` (`folder_id`)
) ENGINE=InnoDB AUTO_INCREMENT=232525 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_sessions`
--

DROP TABLE IF EXISTS `auth_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_key` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires` datetime NOT NULL,
  `code_verifier` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `refresh_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138682 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cake_sessions`
--

DROP TABLE IF EXISTS `cake_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cake_sessions` (
  `id` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `data` mediumtext NOT NULL,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_lists`
--

DROP TABLE IF EXISTS `class_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `last_used` datetime DEFAULT NULL,
  `is_new` int(5) NOT NULL DEFAULT '0',
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10492 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `communities`
--

DROP TABLE IF EXISTS `communities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communities` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder_ids` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `edit_policy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add_delete_policy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `export_policy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scores_policy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_add_policy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3348 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `community_users`
--

DROP TABLE IF EXISTS `community_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `community_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `folder_id` int(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_admin` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_community_id` (`community_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20533 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `delete_jobs`
--

DROP TABLE IF EXISTS `delete_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delete_jobs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `token` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `folder_id` int(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `moved` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `is_new` int(5) NOT NULL DEFAULT '0',
  `save_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version_data_id` int(20) DEFAULT NULL,
  `layout_json` longtext COLLATE utf8mb4_unicode_ci,
  `next_form_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_folder_id` (`folder_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_version_data_id` (`version_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=376517 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email_jobs`
--

DROP TABLE IF EXISTS `email_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_jobs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `sent` datetime(6) DEFAULT NULL,
  `json_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=214934 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `folders`
--

DROP TABLE IF EXISTS `folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `community_id` int(20) DEFAULT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `type` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'document',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=512178 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `renewal_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prepaid_count` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=996 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `js_modules`
--

DROP TABLE IF EXISTS `js_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_modules` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_public` int(5) NOT NULL DEFAULT '0',
  `is_default_import` int(5) NOT NULL DEFAULT '0',
  `js` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_token` (`token`(191))
) ENGINE=InnoDB AUTO_INCREMENT=151716 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stripe_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `details` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2728 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qr_data`
--

DROP TABLE IF EXISTS `qr_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qr_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `qr_tag` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_used` datetime DEFAULT NULL,
  `json_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_qr_tag` (`qr_tag`)
) ENGINE=InnoDB AUTO_INCREMENT=841379 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question_handles`
--

DROP TABLE IF EXISTS `question_handles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_handles` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `question_id` int(20) NOT NULL,
  `community_id` int(20) DEFAULT NULL,
  `user_id` int(20) DEFAULT NULL,
  `sitting_id` int(20) DEFAULT NULL,
  `name_alias` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_question_id` (`question_id`),
  KEY `idx_community_id` (`community_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_sitting_id` (`sitting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3851454 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question_statistics`
--

DROP TABLE IF EXISTS `question_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_statistics` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `result_set_id` int(20) DEFAULT NULL,
  `question_handle_id` int(20) DEFAULT NULL,
  `question_content_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statistics` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_result_set_id` (`result_set_id`),
  KEY `idx_question_content_id` (`question_content_id`),
  KEY `idx_question_handle_id` (`question_handle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29264276 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question_tags`
--

DROP TABLE IF EXISTS `question_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_tags` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `question_content_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_question_content_id` (`question_content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=327676 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `creator_id` int(20) DEFAULT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `needs_stats` int(20) NOT NULL DEFAULT '0',
  `needs_index` datetime DEFAULT NULL,
  `last_content_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_status` int(5) NOT NULL DEFAULT '0',
  `opened` datetime DEFAULT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci,
  `json_data` longtext COLLATE utf8mb4_unicode_ci,
  `last_used` mediumtext COLLATE utf8mb4_unicode_ci,
  `question_content_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_ids` mediumtext COLLATE utf8mb4_unicode_ci,
  `scoring_hash` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stats_count` int(20) NOT NULL DEFAULT '0',
  `stats_difficulty` float DEFAULT '0',
  `stats_discrimination` float DEFAULT '0',
  `stats_quality` float NOT NULL DEFAULT '0',
  `stats_json` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_parent_id` (`parent_id`),
  KEY `idx_creator_id` (`creator_id`),
  KEY `idx_question_content_id` (`question_content_id`),
  KEY `idx_needs_stats` (`needs_stats`),
  KEY `idx_needs_index` (`needs_index`)
) ENGINE=InnoDB AUTO_INCREMENT=641299 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `responses_online`
--

DROP TABLE IF EXISTS `responses_online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responses_online` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitting_student_id` int(11) DEFAULT NULL,
  `result_data_id` int(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `deprecated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `events` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `responses` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_save` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_sitting_student_id` (`sitting_student_id`),
  KEY `idx_deprecated` (`deprecated`),
  KEY `idx_last_save` (`last_save`),
  KEY `idx_created` (`created`),
  KEY `idx_result_data_id` (`result_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1458184 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `responses_paper`
--

DROP TABLE IF EXISTS `responses_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responses_paper` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `result_set_id` int(20) DEFAULT NULL,
  `score_id` int(20) DEFAULT NULL,
  `user_id` int(20) NOT NULL,
  `date` datetime NOT NULL,
  `pdf_hash` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_pdf_hash` (`pdf_hash`),
  KEY `idx_result_set_id` (`result_set_id`),
  KEY `idx_score_id` (`score_id`)
) ENGINE=InnoDB AUTO_INCREMENT=692150 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `result_data`
--

DROP TABLE IF EXISTS `result_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `result_set_id` int(20) NOT NULL,
  `version_data_id` int(20) DEFAULT NULL,
  `results` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_result_set_id` (`result_set_id`),
  KEY `idx_version_data_id` (`version_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15053637 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `result_sets`
--

DROP TABLE IF EXISTS `result_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result_sets` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `source_model` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stats_status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'update',
  `average_score` float NOT NULL DEFAULT '0',
  `alpha` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_stats_status` (`stats_status`)
) ENGINE=InnoDB AUTO_INCREMENT=785816 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scores`
--

DROP TABLE IF EXISTS `scores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `document_id` int(20) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `viewed` datetime DEFAULT NULL,
  `job_created` datetime DEFAULT NULL,
  `job_start` datetime DEFAULT NULL,
  `job_end` datetime DEFAULT NULL,
  `pdf_hash` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pageCount` int(5) DEFAULT NULL,
  `errorCount` int(5) DEFAULT NULL,
  `details_json` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `needs_data` int(5) DEFAULT '1',
  `dropbox_email` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dropbox_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_document_id` (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=684049 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_data`
--

DROP TABLE IF EXISTS `search_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `question_content_id` varchar(32) NOT NULL,
  `folder_ids` mediumtext NOT NULL,
  `is_open` int(11) NOT NULL DEFAULT '1',
  `keywords_string` mediumtext NOT NULL,
  `vectors_json` mediumtext NOT NULL,
  `subjects` mediumtext,
  PRIMARY KEY (`id`),
  KEY `idx_question_content_id` (`question_content_id`),
  KEY `idx_subjects` (`subjects`(32))
) ENGINE=InnoDB AUTO_INCREMENT=6146932 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sitting_students`
--

DROP TABLE IF EXISTS `sitting_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitting_students` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sitting_id` int(20) NOT NULL,
  `user_id` int(20) DEFAULT NULL,
  `is_new` int(5) NOT NULL DEFAULT '0',
  `folder_id` int(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `moved` datetime DEFAULT NULL,
  `details` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `started` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `can_open` int(5) NOT NULL DEFAULT '0',
  `token` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_folder_id` (`folder_id`),
  KEY `idx_sitting_id` (`sitting_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1948482 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sittings`
--

DROP TABLE IF EXISTS `sittings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sittings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `folder_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `moved` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `is_new` int(5) NOT NULL DEFAULT '0',
  `assessment_id` int(20) DEFAULT NULL,
  `version_data_id` int(20) DEFAULT NULL,
  `result_set_id` int(20) DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_data` mediumtext COLLATE utf8mb4_unicode_ci,
  `join_token` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_folder_id` (`folder_id`),
  KEY `idx_result_set_id` (`result_set_id`),
  KEY `idx_version_data_id` (`version_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=96101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_sheets`
--

DROP TABLE IF EXISTS `style_sheets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_sheets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `last_used` datetime DEFAULT NULL,
  `is_new` int(5) NOT NULL DEFAULT '0',
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21435 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_questions`
--

DROP TABLE IF EXISTS `user_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_questions` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `folder_id` int(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `moved` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `is_new` int(5) NOT NULL DEFAULT '0',
  `question_handle_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_question_handle_id` (`question_handle_id`),
  KEY `idx_folder_id` (`folder_id`)
) ENGINE=InnoDB AUTO_INCREMENT=808514 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `auth_id` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `signup_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `security_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email_bounced` int(5) NOT NULL DEFAULT '0',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved` int(20) NOT NULL DEFAULT '0',
  `created_by_id` int(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'America/Edmonton',
  `validation_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_teachers` int(5) NOT NULL DEFAULT '1',
  `defaults` mediumtext COLLATE utf8mb4_unicode_ci,
  `open_created` int(5) NOT NULL DEFAULT '0',
  `expires` datetime DEFAULT NULL,
  `expiry_warnings` int(11) NOT NULL DEFAULT '0',
  `locked` datetime DEFAULT NULL,
  `ua_read` int(5) NOT NULL DEFAULT '0',
  `folder_ids` mediumtext COLLATE utf8mb4_unicode_ci,
  `hidden_tips` mediumtext COLLATE utf8mb4_unicode_ci,
  `show_servers` int(5) NOT NULL DEFAULT '0',
  `message` mediumtext COLLATE utf8mb4_unicode_ci,
  `notes` mediumtext COLLATE utf8mb4_unicode_ci,
  `contact_ids` mediumtext COLLATE utf8mb4_unicode_ci,
  `banner_count` int(5) NOT NULL DEFAULT '0',
  `targeted` mediumtext COLLATE utf8mb4_unicode_ci,
  `entropy` float NOT NULL DEFAULT '0',
  `can_write_code` int(5) NOT NULL DEFAULT '0',
  `allow_horizontal` int(11) NOT NULL DEFAULT '0',
  `smart_allow` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `smart_usage` mediumtext COLLATE utf8mb4_unicode_ci,
  `local_beta` int(11) NOT NULL DEFAULT '0',
  `correlations_beta` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_signup_email` (`signup_email`),
  KEY `idx_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=93046 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `version_data`
--

DROP TABLE IF EXISTS `version_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `html_gzip` mediumblob,
  `html` longtext COLLATE utf8mb4_unicode_ci,
  `scoring_json` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1810161 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-09-26 15:57:59

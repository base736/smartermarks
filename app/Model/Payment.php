<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class Payment extends AppModel {

	var $actsAs = array('Containable');

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		)
	);
	
	private function getToken() {
		$secrets = Configure::read('secrets');

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $secrets['paypal']['api_url'] . 'v1/oauth2/token');
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_USERPWD, $secrets['paypal']['client_id'] . ':' . 
			$secrets['paypal']['secret']);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
		
		$result = curl_exec($ch);

		curl_close($ch);		
		
		if (!empty($result)) {
			$result = json_decode($result, true);
			if (!empty($result['access_token'])) {
				return $result['access_token'];
			} else return false;
		}
		else return false;
	}

	public function createOrder($userID, $itemList) {
		$secrets = Configure::read('secrets');

		$items = array();
		$item_total = 0;
		foreach ($itemList as $entry) {
			$item_id = $entry['item_id'];
			if (!empty($secrets['paypal']['items'][$item_id])) {
				$item = array();
				$thisDetails = $secrets['paypal']['items'][$item_id];
				$item['name'] = $thisDetails['item_name'];
				$item['unit_amount'] = array(
					'currency_code' => $secrets['paypal']['currency'],
					'value' => $thisDetails['amount']
				);
				$item['quantity'] = $entry['quantity'];
				$items[] = $item;

				$item_total += $item['unit_amount']['value'] * $item['quantity'];
			} else return false;
		}
		$tax_total = 0.05 * $item_total;


		$purchase_unit_request = array();
		$purchase_unit_request['items'] = $items;
		$purchase_unit_request['amount'] = array(
			'currency_code' => $secrets['paypal']['currency'],
			'value' => ($item_total + $tax_total),
			'breakdown' => array(
				'item_total' => array(
					'currency_code' => $secrets['paypal']['currency'],
					'value' => $item_total
				),
				'tax_total' => array(
					'currency_code' => $secrets['paypal']['currency'],
					'value' => $tax_total
				)
			)
		);

		$data = array();
		$data['intent'] = 'CAPTURE';
		$data['purchase_units'] = array($purchase_unit_request);

		$token = $this->getToken();

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $secrets['paypal']['api_url'] . 'v2/checkout/orders');
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Authorization: Bearer ' . $token
		));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		
		$result = curl_exec($ch);
		curl_close($ch);		
		
		$orderID = false;
		if (!empty($result)) {
			$result = json_decode($result, true);
			if (!empty($result['id'])) {
				$orderID = $result['id'];

				$data = array();
				$data['user_id'] = $userID;
				$data['order_id'] = $orderID;
				$data['details'] = json_encode(array('items' => $itemList));
				$data['status'] = 'created';
				$this->create();
				$this->save($data);
			}
		}

		return $orderID;
	}

	public function cancel($orderID) {
		$payment = $this->findByOrderId($orderID);
		if (!empty($payment)) {
			$data = array();
			$data['id'] = $orderID;
			$data['status'] = 'cancelled';
			$this->save($data);
		} else return false;

		return true;
	}

	public function capture($orderID) {
		$payment = $this->findByOrderId($orderID);
		if (!empty($payment)) {
			$secrets = Configure::read('secrets');
			$token = $this->getToken();

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $secrets['paypal']['api_url'] . 'v2/checkout/orders/' . 
				$orderID . '/capture');
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Authorization: Bearer ' . $token
			));
			
			$result = curl_exec($ch);
			curl_close($ch);		
			
			if (!empty($result)) {
				$result = json_decode($result, true);
				if (!empty($result['status'])) {
					if ($result['status'] == 'COMPLETED') {
						$details = json_decode($payment['Payment']['details'], true);
						$details['result'] = $result;
		
						$data = array();
						$data['id'] = $orderID;
						$data['details'] = json_encode($details);
						$data['status'] = 'complete';
						$this->save($data);
					} else return false;
				} else return false;
			} else return false;
		} else return false;

		return true;
	}

	public function apply($purchase, $id = null) {
		$groupModel = ClassRegistry::init('Group');

		foreach ($purchase['items'] as $item) {

			if (isset($item['group_id'])) {

				$group_id = $item['group_id'];

			} else if (isset($item['group_name'])) {

				$groupName = $item['group_name'];
				$expiryTime = new DateTime('@' . $item['expires']);

				$data = array();
				$data['renewal_type'] = $item['type'];
				$data['expires'] = $expiryTime->format('Y-m-d H:i:s');
				$data['name'] = $groupName;
				$data['contact_id'] = 1;
			
				$groupModel->create();
				$groupModel->save($data);

				$group_id = $groupModel->id;

			} else {

				$group_id = null;

			}

			$expiryTime = new DateTime('@' . $item['expires']);
			$newExpires = $expiryTime->format('Y-m-d H:i:s');

			$noteDate = date('Y-m-d');
			if ($group_id !== null) {
				$addedNote = $noteDate . ': Added to group ' . $group_id;
			} else $addedNote = $noteDate . ': Skipped renewal';
			
			$renewedNote = $noteDate . ': Renewed through ' . $expiryTime->format('F j, Y');
			if ($id == null) $renewedNote .= ' (no charge)';
			else $renewedNote .= ' (payment ID ' . $id . ')';
	
			if (isset($item['admin_ids'])) $admin_ids = $item['admin_ids'];
			else $admin_ids = [];

			if (!empty($item['school_name'])) $groupSchool = $item['school_name'];
			else $groupSchool = '';

            if (!empty($item['new_prepaid']) && ($group_id !== null)) {
                $groupModel->contain();
                $group = $groupModel->findById($group_id);

                $new_prepaid_count = $group['Group']['prepaid_count'] + $item['new_prepaid'];

                $groupModel->id = $group_id;
                $groupModel->saveField('prepaid_count', $new_prepaid_count);
            }

			if (!empty($item['new_users'])) {
				if (!isset($item['user_ids'])) $item['user_ids'] = array();

				$groupTimezone = '';

				if ($group_id !== null) {
						
					// Get default school name and timezone
	
					$groupModel->contain(array('UserGroup' => 'User'));
					$group = $groupModel->findById($group_id);
	
					$school_counts = array();
					$timezone_counts = array();
					foreach ($group['UserGroup'] as $userGroup) {
						$thisSchool = $userGroup['User']['school'];
						if (!isset($school_counts[$thisSchool])) $school_counts[$thisSchool] = 0;
						$school_counts[$thisSchool]++;

						$thisTimezone = $userGroup['User']['timezone'];
						if (!isset($timezone_counts[$thisTimezone])) $timezone_counts[$thisTimezone] = 0;
						$timezone_counts[$thisTimezone]++;
					}

					if (empty($groupSchool)) {
						$max_count = 0;
						foreach ($school_counts as $school => $count) {
							if ($count > $max_count) {
								$max_count = $count;
								$groupSchool = $school;
							}		
						}						
					}

					$max_count = 0;
					foreach ($timezone_counts as $timezone => $count) {
						if ($count > $max_count) {
							$max_count = $count;
							$groupTimezone = $timezone;
						}		
					}						
				}

				if (empty($groupTimezone)) $groupTimezone = 'America/Edmonton'; // TEMPORARY
	
				foreach ($item['new_users'] as $entry) {
					$data = array(
						'signup_email' => $entry['email'],
						'email' => $entry['email'],
						'password' => '',
						'role' => 'teacher',
						'name' => $entry['name'],
						'school' => $groupSchool,
						'approved' => 1,
						'timezone' => $groupTimezone,
						'expires' => $newExpires,
						'notes' => '',	
						'defaults' => $this->User->getDefaults()
					);

					$this->User->create();
					if (!empty($this->User->save($data))) {
						$item['user_ids'][] = $this->User->id;
						$this->User->send_validation($this->User->id);
					}
				}
			}

			if (!empty($item['user_ids'])) {
				foreach ($item['user_ids'] as $user_id) {
					if ($group_id !== null) {
						$is_admin = in_array($user_id, $admin_ids) ? 1 : 0;
						$groupModel->addMember($group_id, $user_id, $is_admin);
					}
	
					$this->User->contain();
					$user = $this->User->findById($user_id);
	
					$data = array();
					$data['id'] = $user_id;
					$data['locked'] = null;
	
					$newNote = $addedNote;
					if (!empty($user['User']['expires'])) {
						$oldExpiryTimestamp = strtotime($user['User']['expires']);
						if ($oldExpiryTimestamp < $item['expires']) {
							$data['expires'] = $newExpires;
							$newNote = $renewedNote;
						}
					}
	
					$thisNote = trim($user['User']['notes']);
					if (!empty($thisNote)) $thisNote .= "\n";
					$data['notes'] = $thisNote . $newNote;
	
					if (!empty($groupSchool)) {
						$data['school'] = $groupSchool;
					}
	
					$this->User->save($data);
				}
			}
		}
	}
}

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

class CommunityUser extends AppModel {

	var $actsAs = array('Containable');

	private $community_ids = array();

	public $belongsTo = array(
		'Community' => array(
			'className' => 'Community',
			'foreignKey' => 'community_id'
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

	public function beforeDelete($cascade = true) {
		$this->contain();		
		$communityUser = $this->findById($this->id);
		$this->community_ids[$this->id] = $communityUser['CommunityUser']['community_id'];
		
		return true;
	}

	public function afterDelete() {
		$this->Community->clean($this->community_ids[$this->id]);
		unset($this->community_ids[$this->id]);
	}
}

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

/**
 * Sitting Model
 */

class ResponseOnline extends AppModel {

	var $actsAs = array('Containable');
	
	public $useTable = 'responses_online';

    private $result_data_ids = array();

	protected $include_deleted = false;

	public function includeDeleted() {
		$this->include_deleted = true;
	}
	public function excludeDeleted() {
		$this->include_deleted = false;
	}

	public $belongsTo = array(
		'SittingStudent' => array(
			'className' => 'SittingStudent',
			'foreignKey' => 'sitting_student_id'
		),
		'ResultData' => array(
			'className' => 'ResultData',
			'foreignKey' => 'result_data_id'
		)
	);

	public function getCache($id) {
		$data = Cache::read($id, 'responses_online');

		if (!$data) {
			$this->contain(array('SittingStudent', 'ResultData'));

			$this->includeDeleted();
			$response = $this->findById($id);
			$this->excludeDeleted();

			if (empty($response) || ($response['ResponseOnline']['sitting_student_id'] == null)) {
				$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Error in ResponseOnline->getCache()", null, array(
					'ID' => $id, 
					'Response' => $response, 
					'Backtrace' => $backtrace
				));

				$data = false;
			} else {
				$data = $response['ResponseOnline'];
				$data['version_data_id'] = $response['ResultData']['version_data_id'];
				$data['sitting_id'] = $response['SittingStudent']['sitting_id'];
				$data['sitting_student_deleted'] = $response['SittingStudent']['deleted'];
				$data['token'] = $response['SittingStudent']['token'];
				$data['results'] = $response['ResultData']['results'];

				Cache::write($id, $data, 'responses_online');
			}
		}

		return $data;
	}

	public function updateCache($cache_data) {
		$success = true;
		$message = false;

		$id = $cache_data['id'];
		
		$now = date('Y-m-d H:i:s');
		$cache_data['last_save'] = $now;
		if (empty($cache_data['deprecated'])) {
			$this->id = $id;
			$this->saveField('deprecated', $now);
			$cache_data['deprecated'] = $now;
		}

		try {

			$old_data = Cache::read($id, 'responses_online');

			// Delete old scores if necessary

			if (!empty($old_data) && ($old_data['results'] != $cache_data['results'])) {
				$result_data_id = $cache_data['result_data_id'];
				Cache::delete($result_data_id, 'string_scores');
			}
	
			// Update cache
		
			Cache::write($id, $cache_data, 'responses_online');

		} catch (Exception $e) {

			$message = $e->getMessage();

			// Unable to update cache -- update disk copy

			if (!$this->saveCacheData($cache_data)) {
				$message = "Disk write error";
				$success = false;
			}

		}

		if ($message !== false) {
			$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

			App::uses('Email', 'Lib');
			Email::queue_error("SmarterMarks: Error in ResponseOnline->updateCache()", null, array(
				'Message' => $message,
				'Cache data' => $cache_data,
				'Backtrace' => $backtrace
			));
		}

		return $success;
	}

	public function saveCacheData($cache_data) {
        $success = true;

		$id = $cache_data['id'];

		$this->contain();
		$old_data = $this->findById($id);
        
		if (empty($old_data['ResponseOnline']['last_save'])) $saveData = true;
		else if (empty($cache_data['last_save'])) $saveData = false;
		else {
			$old_saved = strtotime($old_data['ResponseOnline']['last_save']);
			$new_saved = strtotime($cache_data['last_save']);
			$saveData = ($new_saved >= $old_saved);
		}

		if ($saveData) {
			$cache_data['deprecated'] = null;
			$success &= !empty($this->save($cache_data));

			$data = array();
			$data['id'] = $cache_data['result_data_id'];
			$data['results'] = $cache_data['results'];
			$success &= !empty($this->ResultData->save($data));
	
			if ($success) {
				Cache::write($id, $cache_data, 'responses_online');
			} else {
				$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
	
				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Error in ResponseOnline->saveCacheData()", null, array(
					'Cache Data' => $cache_data,
					'Backtrace:' => $backtrace
				));
			}	
		} else $success = false;

		return $success;
	}

	public function getClosed($startTime, $sittingData) {
		$endTimestamp = null;
		if ($sittingData['Access']['duration'] != 'none') {
			$thisTimestamp = strtotime($startTime);
			$thisTimestamp += $sittingData['Access']['duration'] * 60.0;
			if (($endTimestamp == null) || ($thisTimestamp < $endTimestamp)) {
				$endTimestamp = $thisTimestamp;
			}
		}
		if ($sittingData['Access']['end']['type'] == 'date') {
			$thisTimestamp = strtotime($sittingData['Access']['end']['date']);
			if (($endTimestamp == null) || ($thisTimestamp < $endTimestamp)) {
				$endTimestamp = $thisTimestamp;
			}
		}

		if ($endTimestamp !== null) {
			return gmdate("Y-m-d H:i:s", $endTimestamp);
		} else return null;
	}
	 
	public function getTimeRemaining($close_datetime) {
		if ($close_datetime === null) return INF;
		else return strtotime($close_datetime) - time();
	}
	
	public function emptyResponses($layout) {
		if ($layout !== null) {
			$responses = array();
			foreach ($layout['Sections'] as $section) {
				foreach ($section['Content'] as $question) {
					if (substr($question['type'], 0, 2) == 'nr') {
						$nr_digits = $question['TypeDetails']['nrDigits'];
						$blankResponse = str_repeat(' ', $nr_digits);
						$empty_result[] = $blankResponse;
					} else $blankResponse = '';

					$responses[] = array(
						'type' => $question['type'],
						'value' => $blankResponse
					);
				}
			}
		} else $responses = false;

		return $responses;
	}

	public function saveEvent($id, $details, $device_info) {
		$success = true;

		$attempt_cache = $this->getCache($id);
		if ($attempt_cache) {

			// Get events wrapper for this attempt

			$events_string = $attempt_cache['events'];
			if (empty($events_string)) $event_data = array();
			else $event_data = json_decode($events_string, true);

			if (empty($event_data['devices'])) $event_data['devices'] = array();
			if (empty($event_data['events'])) $event_data['events'] = array();

			// Make sure all decoded entries have indices even if they look like an array

			foreach ($event_data['events'] as &$pEntry) {
				foreach ($pEntry as $key => &$pValue) {
					if (is_array($pValue)) $pValue = (object)$pValue;
				}
			}

			// Add device index to event

			for ($device_index = 0; $device_index < count($event_data['devices']); ++$device_index) {
				if ($event_data['devices'][$device_index] == $device_info) break;
			}
			$details['deviceID'] = $device_index;

			// Add event and (if necessary) device to events wrapper

			$event_data['events'][] = $details;

			if ($device_index == count($event_data['devices'])) {
				$event_data['devices'][] = $device_info;
			}

			// Save to cache

			$attempt_cache['id'] = $id;
			$attempt_cache['events'] = json_encode($event_data);
			$success &= $this->updateCache($attempt_cache);
			
		} else $success = false;

		return $success;
	}

	public function saveAnnotations($id, $new_annotations) {
		$success = true;

		$attempt_cache = $this->getCache($id);
		if ($attempt_cache) {

			if (!empty($attempt_cache['annotations'])) {
				$annotations = json_decode($attempt_cache['annotations'], true);
			} else $annotations = array();

			foreach ($new_annotations as $content_id => $this_annotations) {
				$annotations[$content_id] = $this_annotations;
			}

			// Save to cache

			$attempt_cache['id'] = $id;
			$attempt_cache['annotations'] = json_encode($annotations);
			$success &= $this->updateCache($attempt_cache);
			
		} else $success = false;

		return $success;
	}

    public function beforeDelete($cascade = true) {
		$this->contain();		
		$response_online = $this->findById($this->id);
		
		if (!empty($response_online['ResponseOnline']['result_data_id'])) {
			$this->result_data_ids[$this->id] = $response_online['ResponseOnline']['result_data_id'];
		} else unset($this->result_data_ids[$this->id]);

		return true;
	}

	public function afterDelete() {
		$attempt_cache = $this->getCache($this->id);
		Cache::delete($this->id, 'responses_online');
		Cache::delete($attempt_cache['sitting_student_id'], 'sitting_students');		

		if (isset($this->result_data_ids[$this->id])) {
            $this->ResultData->delete($this->result_data_ids[$this->id]);
			unset($this->result_data_ids[$this->id]);
		}
	}

	public function afterSave($created, $options = array()) {
		if ($created) {
			$attempt_cache = $this->getCache($this->id);
			Cache::delete($attempt_cache['sitting_student_id'], 'sitting_students');		
		}

		return true;
	}

	public function beforeFind($query) {
		if (!$this->include_deleted) {
			$find_condition = array('ResponseOnline.deleted' => NULL);
			if (is_array($query['conditions'])) $query['conditions'] += $find_condition;
			else $query['conditions'] = $find_condition;
		}

		return $query;
	}
}

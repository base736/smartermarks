<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');

/**
 * Sitting Model
 */

class SittingStudent extends AppModel {

	var $actsAs = array('Containable');
	public $displayField = 'Sitting.name';
	
	public $filterArgs = array(
    	'search' => array('type' => 'like', 'field' => array('Sitting.name')),
    );

	protected $include_deleted = false;

	public function includeDeleted() {
		$this->include_deleted = true;
	}
	public function excludeDeleted() {
		$this->include_deleted = false;
	}

	public $belongsTo = array(
		'Sitting' => array(
			'className' => 'Sitting',
			'foreignKey' => 'sitting_id'
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		),
		'Folder' => array(
			'className' => 'Folder',
			'foreignKey' => 'folder_id',
		)
	);
	
	public $hasMany = array(
		'ResponseOnline' => array(
			'className' => 'ResponseOnline',
			'foreignKey' => 'sitting_student_id'
		)
	);

	public function getPermissions($sittingStudent_id, $user_id) {
		if (empty($sittingStudent_id) || !is_numeric($sittingStudent_id)) return array();
		if (empty($user_id) || !is_numeric($user_id)) return array();

		$permissions = array();

		$this->contain('Sitting');
		$sittingStudent = $this->findById($sittingStudent_id);
		if (!empty($sittingStudent)) {
			if ($user_id == $sittingStudent['SittingStudent']['user_id']) {
				$folderModel = ClassRegistry::init('Folder');
				$folderPermissions = $folderModel->getContentPermissions($sittingStudent['SittingStudent']['folder_id'], $user_id);
				$permissions = $folderPermissions['owner'];
			} else if ($user_id == $sittingStudent['Sitting']['user_id']) {
				$permissions = array('can_view', 'can_edit');
			} else {
				$assessmentModel = ClassRegistry::init('Assessment');
				$assessmentID = $sittingStudent['Sitting']['assessment_id'];
				$permissions = $assessmentModel->getPermissions($assessmentID, $user_id);
				if (in_array('all_scores', $permissions)) {
					$permissions = array('can_view', 'can_edit');
				}		
			}
		}

		return $permissions;
	}
	
	public function cleanResponses($sittingStudent_id) {
		$sittingStudent_cache = $this->getCache($sittingStudent_id);

		// Close any attempts that are not the most recent

		$now = date('Y-m-d H:i:s');

		for ($i = 0; $i < count($sittingStudent_cache['ResponseOnline']) - 1; ++$i) {
			$attempt_id = $sittingStudent_cache['ResponseOnline'][$i];
			$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
			$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
			if ($timeRemaining > 0) {
				$attempt_cache['closed'] = $now;
				$this->ResponseOnline->updateCache($attempt_cache);
			}
		}
	}

	public function getAvailability($sittingStudent_id) {
		$resp = array();
		$resp['status'] = 'open';

		$sittingStudent_cache = $this->getCache($sittingStudent_id);
		$sitting_cache = $this->Sitting->getCache($sittingStudent_cache['SittingStudent']['sitting_id']);

		$nowTimestamp = time();

		$sittingEndTimestamp = null;
		if (!empty($sitting_cache['Sitting']['closed'])) {
			$sittingEndTimestamp = strtotime($sitting_cache['Sitting']['closed']);
		}
	
		if (($sittingEndTimestamp != null) && ($nowTimestamp >= $sittingEndTimestamp)) {
			$resp['status'] = 'closed';
		} else {
			$json_data = $sitting_cache['Sitting']['json_data'];
	
			if ($json_data['Access']['start']['type'] == 'date') {
				$startTimestamp = strtotime($json_data['Access']['start']['date']);
				if ($nowTimestamp < $startTimestamp) {
					$resp['status'] = 'early';
				}
			}
			
			if ($json_data['Access']['end']['type'] == 'date') {
				$endTimestamp = strtotime($json_data['Access']['end']['date']);
				if ($nowTimestamp >= $endTimestamp) {
					$resp['status'] = 'closed';
				}
			}
	
			if ($resp['status'] == 'open') {
				$numClosed = 0;
				foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
					$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
					$timeRemaining = $this->ResponseOnline->getTimeRemaining($attempt_cache['closed']);
					if ($timeRemaining <= 0) $numClosed++;
				}
				if (($json_data['Access']['attempt_limit'] != 'none') && ($numClosed >= $json_data['Access']['attempt_limit'])) {
					$resp['status'] = 'complete';
				}
			}
		}

		return $resp;
	}

	public function getCache($id) {
		$data = Cache::read($id, 'sitting_students');

		if (!$data) {
			$data = array();

			$this->contain(array(
				'ResponseOnline' => array(
					'conditions' => array('ResponseOnline.deleted' => null),
					'order' => array('ResponseOnline.created' => 'ASC')
				)
			));

			$this->includeDeleted();
			$sittingStudent = $this->findById($id);
			$this->excludeDeleted();

			if (!empty($sittingStudent)) {
				$data['SittingStudent'] = $sittingStudent['SittingStudent'];

				if (empty($data['SittingStudent']['details'])) $data['SittingStudent']['details'] = array();
				else $data['SittingStudent']['details'] = json_decode($data['SittingStudent']['details'], true);

				$response_online_ids = array();
				foreach ($sittingStudent['ResponseOnline'] as $attempt) {
					$response_online_ids[] = $attempt['id'];
				}
				$data['ResponseOnline'] = $response_online_ids;

				Cache::write($id, $data, 'sitting_students');
			} else $data = false;
		}

		return $data;
	}

	public function beforeDelete($cascade = true) {
		$sittingStudent_cache = $this->getCache($this->id);
		
		foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
			$attempt_cache = $this->ResponseOnline->getCache($attempt_id);

			$attempt_cache['sitting_student_id'] = null;
			$attempt_cache['version_data_id'] = null;

			if ($this->ResponseOnline->saveCacheData($attempt_cache)) {
				Cache::delete($attempt_id, 'responses_online');
			}	
		}
	}

	public function afterDelete() {
		Cache::delete($this->id, 'sitting_students');
	}

	public function beforeSave($options = array()) {
	    if (!empty($this->data['SittingStudent']['folder_id'])) {
			$this->Folder->contain();
			$folder = $this->Folder->findById($this->data['SittingStudent']['folder_id']);
			if ($folder['Folder']['type'] !== 'sittingstudents') {
				$user_email = AuthComponent::user('email');

				App::uses('Email', 'Lib');
				Email::queue_error("SmarterMarks: Bad folder type in SittingStudent->save", $user_email, array(
					'folder ID' => $this->data['SittingStudent']['folder_id']
				));
				return false;
			}
		}

		return true;
	}

	public function afterSave($created, $options = array()) {		
		Cache::delete($this->id, 'sitting_students');
		$sittingStudent_cache = $this->getCache($this->id);

		foreach ($sittingStudent_cache['ResponseOnline'] as $attempt_id) {
			$attempt_cache = $this->ResponseOnline->getCache($attempt_id);
			$attempt_cache['sitting_student_deleted'] = $sittingStudent_cache['SittingStudent']['deleted'];
			$this->ResponseOnline->updateCache($attempt_cache);
		}

		return true;
	}

	public function beforeFind($query) {
		if (!$this->include_deleted) {
			$find_condition = array('SittingStudent.deleted' => NULL);
			if (is_array($query['conditions'])) $query['conditions'] += $find_condition;
			else $query['conditions'] = $find_condition;
		}

		return $query;
	}

}

<?php

App::uses('Model', 'Model');

class AppModel extends Model {
	
	public $recursive = -1;

    private function getAbsoluteReferenceNames(&$schema, $schema_uri) {
        $reference_names = array();
        if (is_object($schema)) {
            foreach ($schema as $key => &$pValue) {
                if ($key == '$ref') {

                    // Change relative reference names to absolute ones

                    if ($pValue[0] == '#') {
                        $pValue = $schema_uri . substr($pValue, 1);
                    }

                    // This is a reference -- store its name

                    $reference_names[] = $pValue;

                } else {

                    // This is not a reference -- keep looking

                    $this_names = self::getAbsoluteReferenceNames($pValue, $schema_uri);
                    $reference_names = array_merge($reference_names, $this_names);
                }
            }
        } else if (is_array($schema)) {

            // This is not a reference -- keep looking

            foreach ($schema as $value) {
                $this_names = self::getAbsoluteReferenceNames($value, $schema_uri);
                $reference_names = array_merge($reference_names, $this_names);
            }
        }

        return array_values(array_unique($reference_names));
    }

    private function linkReferenceObjects(&$schema, $references) {
        $success = true;

        if (is_object($schema)) {
            $new_properties = new stdClass();
            foreach ($schema as $key => &$pValue) {
                if ($key == '$ref') {
                    if (property_exists($references, $pValue)) {
                        foreach ($references->{$pValue} as $ref_key => &$ref_value) {
                            if (!in_array($ref_key, array('$schema', 'id', 'definitions'))) {
                                $new_properties->{$ref_key} = &$ref_value;
                            }
                        }
                        unset($schema->{$key});
                    } else $success = false;
                } else {
                    $success &= $this->linkReferenceObjects($pValue, $references);
                }
            }
            foreach ($new_properties as $key => &$pValue) {
                $schema->{$key} = &$pValue;
            }
        } else if (is_array($schema)) {
            foreach ($schema as &$pValue) {
                $success &= $this->linkReferenceObjects($pValue, $references);
            }
        }

        return $success;
    }

    protected function getSchema($uri) {
        $success = true;

        $schema_serialized = Cache::read($uri, 'schemas');
		if ($schema_serialized === false) {

            // Read all schemas

            $schema_root = Configure::read('approot') . 'webroot/schemas/';

            $schema_set = array();
            if ($handle = opendir($schema_root)) {
                while (($file = readdir($handle)) !== false) {
                    $file_path = $schema_root . $file;
                    if (($file[0] !== '.') && is_file($file_path)) {
                        $file_contents = file_get_contents($file_path);
                        $schema_data = json_decode($file_contents);
                        if ($schema_data !== null) {
                            if (property_exists($schema_data, 'id')) {
                                $schema_set[$schema_data->id] = $schema_data;
                            }        
                        } else {
                            print "Invalid schema in " . $file_path . "\n";
                        }
                    }
                }
                closedir($handle);
            }

            if (!array_key_exists($uri, $schema_set)) return false;

            // Get names for all references in the schema set

            $reference_names = array();
            foreach ($schema_set as $key => $value) {
                $this_names = $this->getAbsoluteReferenceNames($value, $value->id);
                $reference_names = array_merge($reference_names, $this_names);
            }
            $reference_names = array_values(array_unique($reference_names));        

            // Get references for each name

            $references = new stdClass();
            foreach ($reference_names as $reference_name) {

                // Find root schema

                unset($reference_schema);
                foreach ($schema_set as $test_uri => &$test_schema) {
                    if (strpos($reference_name, $test_uri) === 0) {
                        if (!isset($reference_schema)) {
                            $reference_schema = &$test_schema;
                            $relative_name = substr($reference_name, strlen($test_uri) + 1);
                        } else $success = false;
                    }
                }
                if (!isset($reference_schema)) $success = false;

                if ($success) {

                    // Get referenced part of the schema

                    if (strlen($relative_name) == 0) $refPath = array();
                    else $refPath = explode('/', $relative_name);

                    while ($success && !empty($refPath)) {
                        $ref = array_shift($refPath);
                        if (property_exists($reference_schema, $ref)) {
                            $reference_schema = &$reference_schema->{$ref};
                        } else $success = false;
                    }

                }

                if ($success) {

                    // Add a reference to the list if it has been found
                    
                    $references->{$reference_name} = $reference_schema;
                }
            }

            // Apply references

            foreach ($schema_set as $key => &$pValue) {
                $success &= self::linkReferenceObjects($pValue, $references);
            }

            // Serialize and save to cache

            foreach ($schema_set as $key => $value) {
                Cache::write($key, serialize($value), 'schemas');
            }

            // Get the requested schema

            if (array_key_exists($uri, $schema_set)) $schema = $schema_set[$uri];
            else $schema = false;

        } else {

            $schema = unserialize($schema_serialized);

        }

        // Return requested schema

        if ($success) return $schema;
        else return false;
    }
}

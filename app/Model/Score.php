<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('AppModel', 'Model');
/**
 * Score Model
 *
 * @property Document $Document
 */
class Score extends AppModel {

	var $actsAs = array('Containable');
	
	public $filterArgs = array(
    	'search' => array('type' => 'like', 'field' => array('User.name')),
    );
    
	public $belongsTo = array(
		'Document' => array(
			'className' => 'Document',
			'foreignKey' => 'document_id'
		), 
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

    public $hasOne = array(
        'ResponsePaper' => array(
            'className' => 'ResponsePaper',
            'foreignKey' => 'score_id'
        )
    );

    private function naive_csv($string) {
        $elements = array();
        
        $inQuotes = false;
        $thisElement = '';
        $string = str_replace(array("\n", "\r"), '', $string);
        for ($i = 0; $i < strlen($string); $i++) {
            if (!$inQuotes && ($string[$i] == ',')) {
                $elements[] = $thisElement;
                $thisElement = '';
            } else if ($string[$i] == '"') {
                if ($i + 1 < strlen($string)) {
                    if ($string[$i + 1] == '"') {
                        $thisElement .= '"';
                        $i++;
                    } else $inQuotes = !$inQuotes;
                } else $inQuotes = !$inQuotes;
            } else $thisElement .= $string[$i];
        }
        $elements[] = $thisElement;
            
        return $elements;
    }

    public function create_engine($score_details, $formatXML, $responses) {
        $success = true;

        if (!array_key_exists('user_id', $score_details)) $success = false;
        if (!array_key_exists('document_id', $score_details)) $success = false;
        if (!array_key_exists('title', $score_details)) $success = false;
        if (!$success) return false;

        // Initialize Score, ResultSet, and ResponsePaper

        if (empty($score_details['details_json'])) {
            $score_details['details_json'] = '[]';
        }

        $this->create();
        $success &= !empty($this->save($score_details));
        
        $documentModel = ClassRegistry::init('Document');

        $documentModel->contain();
        $document = $documentModel->findById($score_details['document_id']);
        $version_data_id = $document['Document']['version_data_id'];

        if ($success) {
            $score_id = $this->id;

            $resultSetModel = ClassRegistry::init('ResultSet');

            $data = array();
            $data['user_id'] = $score_details['user_id'];
            $data['source_model'] = 'ResponsePaper';
            if (!empty($score_details['pdf_hash'])) {
                $data['stats_status'] = 'update';
            } else $data['stats_status'] = 'exclude';

            $resultSetModel->create();
            if (empty($resultSetModel->save($data))) {
                $this->delete($score_id);
                $success = false;
            }
        }
        
        if ($success) {
            $result_set_id = $resultSetModel->id;

            $responsePaperModel = ClassRegistry::init('ResponsePaper');

            $data = array();
            $data['user_id'] = $score_details['user_id'];
            $data['date'] = date('Y-m-d H:i:s');
            $data['result_set_id'] = $result_set_id;
            $data['score_id'] = $score_id;
            if (!empty($score_details['pdf_hash'])) {
                $data['pdf_hash'] = $score_details['pdf_hash'];
            } else $data['pdf_hash'] = null;

            $responsePaperModel->create();
            if (empty($responsePaperModel->save($data))) {
                $this->delete($score_id);
                $resultSetModel->delete($result_set_id);
                $success = false;
            }
        }

        if ($success) {

            // Add responses to ResultSet

            $details = json_decode($score_details['details_json'], true);
            $details['format'] = $formatXML;
            $details['responses'] = array();

            $resultDataModel = ClassRegistry::init('ResultData');
            $qrDataModel = ClassRegistry::init('QrData');

            foreach ($responses as $response) {
                $result_data = array();
                $result_data['result_set_id'] = $result_set_id;
                $result_data['version_data_id'] = $version_data_id;
                $result_data['results'] = json_encode($response['results']);
                $resultDataModel->create();
                $resultDataModel->save($result_data);

                if (!empty($response['details'])) {
                    if (array_key_exists('qr', $response['details'])) {
                        $response_qr = $response['details']['qr'];
                        if (preg_match('/^\d{15}$/', $response_qr) == 1) {
                            $qrDataModel->contain();
                            $qr_data = $qrDataModel->find('first', array(
                                'conditions' => array('QrData.qr_tag' => $response_qr)
                            ));
                            if (!empty($qr_data['QrData']['json_data'])) {
                                $qr_json_data = json_decode($qr_data['QrData']['json_data'], true);
                                foreach ($qr_json_data as $key => $value) {
                                    $response['details'][$key] = $value;
                                }
                            }
                        }
                    }

                    $details['responses'][$resultDataModel->id] = $response['details'];
                }
            }

            // Update score details

            $this->id = $score_id;
            $this->saveField('details_json', json_encode($details));    
        }

        if ($success && !empty($score_details['pdf_hash'])) {
            $hash = $score_details['pdf_hash'];

            // Finish stats update for any ResponsePapers with the same hash that haven't finished
            // updating their stats yet

            App::uses('Scoring', 'Lib');

            $responsePaperModel->contain('ResultSet');
            $result_set_ids = $responsePaperModel->find('list', array(
                'fields' => array('ResultSet.id'),
                'conditions' => array(
                    'ResponsePaper.pdf_hash' => $hash,
                    'ResultSet.stats_status' => 'update'
                )
            ));

            foreach ($result_set_ids as $result_set_id) {
                Scoring::update_set_stats($result_set_id);							
            }

            // Find ResponsePapers with a matching PDF hash

            $responsePaperModel->contain(array('ResultSet' => 'ResultData'));
            $hashPapers = $responsePaperModel->find('all', array(
                'conditions' => array(
                    'ResponsePaper.pdf_hash' => $hash,
                    'ResultSet.stats_status' => 'ready'
                ),
                'order' => array('ResultSet.average_score' => 'DESC')
            ));

            if (count($hashPapers) > 1) {

                // For versions before version 3, include only the highest-scoring response data for 
                // each PDF in stats. For version 3, do the same for each version_data_id

                $format = simplexml_load_string($formatXML);
                if ($format->version < 3) {

                    $best_score = array_shift($hashPapers);
                    foreach ($hashPapers as $responsePaper) {
                        $resultSetModel->id = $responsePaper['ResponsePaper']['result_set_id'];
                        $resultSetModel->saveField('stats_status', 'exclude');
                    }

                } else {

                    $found_version_data_ids = array();
                    foreach ($hashPapers as $responsePaper) {
                        $include_response_paper = false;
                        if (!empty($responsePaper['ResultSet']['ResultData'])) {
                            $version_data_id = $responsePaper['ResultSet']['ResultData'][0]['version_data_id'];
                            if (!in_array($version_data_id, $found_version_data_ids)) {
                                $found_version_data_ids[] = $version_data_id;
                                $include_response_paper = true;
                            }
                        }

                        if ($include_response_paper) {
                            $resultSetModel->id = $responsePaper['ResponsePaper']['result_set_id'];
                            $resultSetModel->saveField('stats_status', 'exclude');    
                        }
                    }

                }

            }

        }        

        if ($success) return $score_id;
        else return false;
    }

    public function save_results($results, $score_common) {
        $success = true;
        $forward_url = false;

        if ($results['status'] == 'complete') {
            $results_data = $results['data'];

            $documentModel = ClassRegistry::init('Document');

            $has_responses = false;
            $expected_formats = array();

            $response_ids = array();
            foreach ($results_data as &$pEntry) {

                // Add a response ID so we can reference these later

                do {
                    $response_id = bin2hex(random_bytes(4));
                } while (in_array($response_id, $response_ids));

                $pEntry['id'] = $response_id;

                if ($pEntry['type'] == "unknown") {
                
                    /******************************************************************************/
                    /* Entry corresponds to a response whose document ID could not be determined. */
                    /******************************************************************************/

                    $has_responses = true;

                } else if ($pEntry['type'] == "expected") {

                    /******************************************************************************/
                    /* Entry corresponds to a format file sent with the scanned PDF               */
                    /******************************************************************************/

                    $document_id = $pEntry['documentID'];

                    $documentModel->contain();
                    $document = $documentModel->findById($document_id);
                    if (!empty($document)) $pEntry['documentName'] = $document['Document']['save_name'];
                    else $pEntry['documentName'] = 'No name';

                    $formatXML = $pEntry['formatXML'];

                    if (empty($pEntry['responses'])) {

                        $pEntry['type'] = 'missing';
                        
                    } else {

                        $errors_pages = array();
                        foreach ($pEntry['errors'] as $error) {
                            $errors_pages = array_merge($errors_pages, $error['pages']);
                        }
                        $errors_pages = array_values(array_unique($errors_pages));

                        $score_details = $score_common;
                        $score_details['document_id'] = $document_id;
                        $score_details['errorCount'] = count($errors_pages);

                        $responses = $pEntry['responses'];

                        $score_id = $this->create_engine($score_details, $formatXML, $responses);
                        if ($score_id !== false) {
    
                            // Add warnings and errors to score details if needed
    
                            $this->contain();
                            $score = $this->findById($score_id);
    
                            $details = json_decode($score['Score']['details_json'], true);
                            $details_modified = false;
    
                            if (!empty($pEntry['warnings'])) {
                                $details['warnings'] = $pEntry['warnings'];
                                $details_modified = true;
                            }
        
                            if (!empty($pEntry['errors'])) {
                                $details['errors'] = $pEntry['errors'];
                                $details_modified = true;
                            }
    
                            if ($details_modified) {
                                $this->id = $score_id;
                                $this->saveField('details_json', json_encode($details));
                            }
    
                            $pEntry['scoreID'] = $score_id;
                            $has_responses = true;
    
                        } else $success = false;

                    }

                } else if ($pEntry['type'] == "loaded") {
                
                    /******************************************************************************/
                    /* Entry corresponds to a format file that was loaded as it was read          */
                    /******************************************************************************/

                    $document_id = $pEntry['documentID'];

                    $documentModel->contain();
                    $document = $documentModel->findById($document_id);
                    if (!empty($document)) $pEntry['documentName'] = $document['Document']['save_name'];
                    else $pEntry['documentName'] = 'No name';

                    if (!empty($pEntry['responses'])) {
                        $has_responses = true;
                    }
                }
            }

            // Add forwarding information to server job

            if ($has_responses) {

                if (count($results_data) == 1) {

                    $foward_url = '/Scores/view_results/' . $results_data[0]['scoreID'];

                } else {

                    $temp_id = uniqid('', true);
                    $cache_details = array(
                        'user_id' => $score_common['user_id'],
                        'response_data' => $results_data,
                        'score_common' => $score_common
                    );
                    
                    Cache::write($temp_id, $cache_details, 'scans_temp');
                    $foward_url = '/Scores/set_view/' . $temp_id;
    
                }
    
            } else {

                $temp_id = uniqid('', true);
                $cache_details = array(
                    'user_id' => $score_common['user_id'],
                    'response_data' => $results_data,
                    'message' => 'No complete responses in uploaded PDF',
                );

                Cache::write($temp_id, $cache_details, 'scans_temp');
                $foward_url = '/Scores/failed/' . $temp_id;
    
            }

        } else {

            $temp_id = uniqid('', true);
            $cache_details = array(
                'user_id' => $score_common['user_id'],
                'message' => $results['message']
            );

            Cache::write($temp_id, $cache_details, 'scans_temp');
            $foward_url = '/Scores/failed/' . $temp_id;

        }

        if ($success) return $foward_url;
        else return false;
    }
    
    public function afterSave($created, $options = array()) {

        // Update score caches

        $user_id = AuthComponent::user('id');
        $cache_data = Cache::read($user_id, 'report_data');
        if (!empty($cache_data)) {
            unset($cache_data[$this->id]);
            Cache::write($user_id, $cache_data, 'report_data');
        }
    }
}

<?php

App::import('Vendor', 'Aws', array('file' => 'AWS/aws-autoloader.php'));

class S3 {

	public static function getClient() {
		$secrets = Configure::read('secrets');
		$s3 = new Aws\S3\S3Client([
			'version' => '2006-03-01',
			'region' => $secrets['s3']['region'],
			'credentials' => array(
				'key' => $secrets['s3']['access_key'],
				'secret' => $secrets['s3']['secret_key']
			)
		]);

		return $s3;
	}

	public static function get($bucket, $filename, $returnData = false) {
		$s3 = S3::getClient();
		try {
			$parameters = array(
				'Bucket' => $bucket, 
				'Key' => $filename
			);
			if (!$returnData) $parameters['SaveAs'] = $filename;
			$response = $s3->getObject($parameters);
			if ($returnData) return $response['Body'];
			else return true;
		} catch (Exception $e) {
			if ($returnData) return false;
			else return $e->getMessage();
		}
	}

	public static function put($bucket, $filename, $data = false) {
		$s3 = S3::getClient();
		try {
			if ($data === false) {
				$response = $s3->putObject(array(
					'Bucket' => $bucket, 
					'Key' => $filename,
					'SourceFile' => $filename
				));
			} else {
				$response = $s3->putObject(array(
					'Bucket' => $bucket, 
					'Key' => $filename,
					'Body' => $data
				));
			}
			return $response;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public static function check($bucket, $filename) {
		$s3 = S3::getClient();
		$response = $s3->doesObjectExist($bucket, $filename);
		return $response;
	}

	public static function delete($bucket, $filename) {
		$s3 = S3::getClient();
		try {
			$response = $s3->deleteObject(array(
				'Bucket' => $bucket, 
				'Key' => $filename
			));
			return $response;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public static function getFolder($bucket, $folder) {
		$s3 = S3::getClient();
		try {
			$response = $s3->downloadBucket(getcwd(), $bucket, $folder);
			return true;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public static function putFolder($bucket, $folder) {
		$s3 = S3::getClient();

		$dir = new DirectoryIterator('.');
		foreach ($dir as $fileinfo) {
			if (!$fileinfo->isDot()) {
				try {
					$response = $s3->putObject(array(
						'Bucket' => $bucket, 
						'Key' => $folder . '/' . $fileinfo->getFilename(),
						'SourceFile' => $fileinfo->getFilename()
					));
				} catch (Exception $e) {
					return $e->getMessage();
				}
			} 
		}
		return true;
	}

	public static function deleteFolder($bucket, $folder) {
		$s3 = S3::getClient();

		try {
			$s3->deleteMatchingObjects($bucket, $folder);
		} catch (Exception $e) {
			return $e->getMessage();
		}
		return true;
	}
}

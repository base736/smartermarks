<?php

class Email {
	
	public static function send($address, $subject, $messageName, $viewVars = null) {
		$emailJobModel = ClassRegistry::init('EmailJob');

		$data = array();
		$data['address'] = $address;
		$data['subject'] = $subject;
		$data['messageName'] = $messageName;
		if ($viewVars != null) $data['viewVars'] = $viewVars;

		$emailJobModel->create();
		$emailJobModel->save(array(
			'json_data' => json_encode($data)
		));
	
		chdir(Configure::read('approot'));
        putenv('CAKE_APPROOT=' . Configure::read('approot'));
		exec("Console/cake sendEmail main > /dev/null 2>&1 &");
	}

    public static function queue_error($subject, $email, $errorData) {
        if (empty($email)) $email = 'unknown user';

        if (array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
            $errorData['browser'] = $_SERVER['HTTP_USER_AGENT'];
        }

        $key = md5($subject);
        $cache_data = Cache::read('error_reports');
        if (empty($cache_data)) $reports = array();
        else $reports = json_decode($cache_data, true);

        if (empty($reports[$key])) {
            $reports[$key] = array(
                'subject' => $subject,
                'count' => 0,
                'emails' => array()
            );
        }
        
        if (!in_array($email, $reports[$key]['emails'])) {
            $reports[$key]['emails'][] = $email;
        }
        $reports[$key]['data'] = $errorData;
        $reports[$key]['count']++;

        Cache::write('error_reports', json_encode($reports));
	}
}

?>
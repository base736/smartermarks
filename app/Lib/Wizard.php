<?php

class Wizard {

    private static function getJSExpression($objectArray) {
            
        // Gather adjacent symbols together
        
        $targetIndex = null;
        $modifiedArray = false;
        foreach ($objectArray as $index => $object) {
            if ($object['type'] == 'Symbol') {
                if ($targetIndex === null) $targetIndex = $index;
                else {
                    $objectArray[$targetIndex]['value'] .= $object['value'];
                    unset($objectArray[$index]);
                    $modifiedArray = true;
                }
            } else $targetIndex = null;
        }
        if ($modifiedArray) $objectArray = array_values($objectArray);

        // Validity checks
        
        if (count($objectArray) > 0) {
            if ($objectArray[0]['type'] == 'TenToThe') return null;
            if ($objectArray[0]['type'] == 'Superscript') return null;
            if (($objectArray[0]['type'] == 'Operator') && ($objectArray[0]['value'] == '!')) {
                return null;
            }
            if ($objectArray[count($objectArray) - 1]['type'] == 'Function') return null;
        }
        
        // Parse the array of expression elements
        
        $expression = "";
        
        $skipIndices = array();
        foreach ($objectArray as $i => $object) {

            if (in_array($i, $skipIndices)) continue;
            
            if (($i > 0) && ($objectArray[$i - 1]['type'] != 'Operator')) {
                if (($object['type'] != 'Operator') && ($object['type'] != 'TenToThe')) 
                    $expression .= '*';
            }

            $superscriptExpression = null;
            $isFactorial = false;
            $searchIndex = ($object['type'] == 'Function') ? ($i + 2) : ($i + 1);
            if ($searchIndex < count($objectArray)) {
                if ($objectArray[$searchIndex]['type'] == 'Superscript') {
                    $superscriptExpression = Wizard::getJSExpression($objectArray[$searchIndex]['operands']['superscript']);
                    if ($superscriptExpression === null) return null;
                    $skipIndices[] = $searchIndex;
                    $expression .= "Core.pw(";
                }
                if (($objectArray[$searchIndex]['type'] == 'Operator') && 
                    ($objectArray[$searchIndex]['value'] == '!')) {
                    $skipIndices[] = $searchIndex;
                    $isFactorial = true;
                    $expression .= 'Core.fc(';
                }
            }
            
            if ($object['type'] == 'Symbol') {
                $expression .= $object['value'];
            } else if ($object['type'] == 'TenToThe') {
                if ($objectArray[$i - 1]['type'] != 'Symbol') $expression .= '*1';
                $exponent = Wizard::getJSExpression($object['operands']['exponent']);
                if (($exponent === null) || (filter_var($exponent, FILTER_VALIDATE_INT) === false)) return null;
                $expression .= 'e' . $exponent;
            } else if ($object['type'] == 'Variable') {
                if ($object['value'] == 'π') $expression .= 'Math.PI';
                else if ($object['value'] == 'e') $expression .= 'Math.E';
                else $expression .= $object['value'];
            } else if ($object['type'] == 'Function') {
                if ($i + 1 >= count($objectArray)) return null;				
                $functionParameter = Wizard::getJSExpression(array($objectArray[$i + 1]));
                if ($functionParameter === null) return null;
                $skipIndices[] = $i + 1;

                $trigMap = array('sin' => 'sd', 'cos' => 'cd', 'tan' => 'td', 'asin' => 'as', 'acos' => 'ac', 'atan' => 'at');

                $functionName = $object['value'];
                if (in_array($functionName, array('floor', 'ceil', 'abs'))) {
                    $expression .= 'Math.' . $functionName . '(' . $functionParameter . ')';
                } else if (isset($trigMap[$functionName])) {
                    $expression .= 'Core.' . $trigMap[$functionName] . '(' . $functionParameter . ')';
                } else if ($functionName == 'sec') {
                    $expression .= '(1.0 / Core.cd(' . $functionParameter . '))';
                } else if ($functionName == 'csc') {
                    $expression .= '(1.0 / Core.sd(' . $functionParameter . '))';
                } else if ($functionName == 'cot') {
                    $expression .= '(1.0 / Core.td(' . $functionParameter . '))';
                } else if ($functionName == 'asec') {
                    $expression .= 'Core.ac(1.0 / (' . $functionParameter . '))';
                } else if ($functionName == 'acsc') {
                    $expression .= 'Core.as(1.0 / (' . $functionParameter . '))';
                } else if ($functionName == 'acot') {
                    $expression .= 'Core.at(1.0 / (' . $functionParameter . '))';
                } else if ($functionName == 'log') {
                    $expression .= 'Core.l2(' . $functionParameter . ')';
                } else if ($object['value'] == 'ln') {
                    $expression .= 'Core.l1(' . $functionParameter . ')';
                }
            } else if ($object['type'] == 'Operator') {
                if ($object['value'] == '!') {

                } else if ($object['value'] == '×') $expression .= '*';
                else if ($object['value'] == '−') $expression .= '-';
                else if ($object['value'] == '=') $expression .= '==';
                else $expression .= $object['value'];
            } else if ($object['type'] == 'BracketPair') {
                $bracketedExpression = Wizard::getJSExpression($object['operands']['bracketedExpression']);
                if ($bracketedExpression === null) return null;

                if ($object['value'] == 'absValBracket') {
                    $expression .= 'Math.abs(' . $bracketedExpression . ')';
                } else $expression .= '(' . $bracketedExpression . ')';
            } else if ($object['type'] == 'StackedFraction') {
                $numeratorExpression = Wizard::getJSExpression($object['operands']['numerator']);
                if ($numeratorExpression === null) return null;
                $denominatorExpression = Wizard::getJSExpression($object['operands']['denominator']);
                if ($denominatorExpression === null) return null;
                $expression .= '(' . $numeratorExpression . ')/(' . $denominatorExpression . ')';
            } else if ($object['type'] == 'SquareRoot') {
                $radicandExpression = Wizard::getJSExpression($object['operands']['radicand']);
                if ($radicandExpression === null) return null;
                $expression .= "Core.sr(" . $radicandExpression . ")";
            } else if ($object['type'] == 'NthRoot') {
                $radicandExpression = Wizard::getJSExpression($object['operands']['radicand']);
                if ($radicandExpression === null) return null;
                $degreeExpression = Wizard::getJSExpression($object['operands']['degree']);
                if ($degreeExpression === null) return null;
                $expression .= "Core.pw(" . $radicandExpression . ", 1.0 / (" . $degreeExpression . "))";
            }
            
            if ($superscriptExpression != null) {
                $expression .= ',' . $superscriptExpression . ')';
            } else if ($isFactorial) {
                $expression .= ')';
            }
        }
        
        return $expression;
    }

    private static function getDependencies($object) {
        $variables = array();
        if ($object['type'] == 'Variable') $variables[] = $object['value'];
        else if (!empty($object['operands'])) {
            foreach ($object['operands'] as $operand) {
                foreach ($operand as $nextObject) {
                    $variables = array_merge($variables, Wizard::getDependencies($nextObject));
                }
            }
        }
        return array_unique($variables);
    }

    public static function getCode($tableData) {
        $newCode = "";

        // Move variable data into a single array

        $variables = array();
        foreach ($tableData as $type => $typeData) {
            foreach ($typeData as $variable) {
                $variable['type'] = $type;
                $variables[$variable['variableName']] = $variable;	
            }
        }

        if (empty($variables)) return $newCode;

        $variableNames = array_keys($variables);
        sort($variableNames);

        $newCode .= "var ";
        foreach ($variableNames as $index => $name) {
            $newCode .= $name . ',';
        }
        $newCode .= "t;";

        // Sort variables to deal with dependencies

        $equation_keys = array('json', 'if_json', 'then_json', 'else_json');

        $dependencies = array();
        foreach ($variableNames as $variableName) {
            $dependencies[$variableName] = array();
            foreach ($equation_keys as $key) {
                if (isset($variables[$variableName][$key])) {
                    $dependencies[$variableName] = array_merge($dependencies[$variableName], 
                        Wizard::getDependencies($variables[$variableName][$key]));
                }    
            }
        }
        
        shuffle($variableNames);
        $sortedNames = array('π', 'e');
        do {
            $numProcessed = 0;
            foreach ($variableNames as $index => $variableName) {
                $canProcess = true;
                $thisVariable = $variables[$variableName];
                $thisDependencies = $dependencies[$variableName];
                foreach ($thisDependencies as $dependency) {
                    if (!in_array($dependency, $sortedNames)) $canProcess = false;
                }
                
                if ($canProcess) {
                    $sortedNames[] = $variableName;
                    unset($variableNames[$index]);
                    $numProcessed++;
                }
            }
        } while ($numProcessed > 0);
        $sortedNames = array_slice($sortedNames, 2);

        if (empty($variableNames)) {

            // Build code

            $setCode = "";

            foreach ($sortedNames as $variableName) {
                if ($variables[$variableName]['type'] == 'constants') {

                    $constant = $variables[$variableName];
                    $stringValue = strtolower($constant['value']);
                    if (strpos($stringValue, 'e') !== false) {
                        $stringValue = str_replace('e', '&times;10<sup>', $stringValue) . '</sup>';
                    }
                    
                    $newCode .= $constant['variableName'] . "=" . $constant['value'] . ";";
                    $setCode .= "Core.st(this,'" . $constant['variableName'] . "','" . $stringValue . "');";
                    
                } else if ($variables[$variableName]['type'] == 'randomized') {

                    $randomized = $variables[$variableName];
                    $newCode .= $randomized['variableName'] . "=Core.rn(" . $randomized['min'] . 
                        "," . $randomized['max'] . "," . $randomized['step'] . ");";

                    if ($randomized['sdType'] == 'Fraction') {
                        $setCode .= "Core.st(this,'" . $randomized['variableName'] . "',Core.rv(" . 
                            $randomized['variableName'] . "));";
                    } else {
                        $newCode .= "t=Core.sv(" . $randomized['variableName'] . "," . 
                            $randomized['sd'] . ",'" . $randomized['sdType'] . "');";
                        $newCode .= "Core.st(this,'" . $randomized['variableName'] . "',t);";
                        $newCode .= $randomized['variableName'] . "=Core.vs(t);";
                    }

                } else if ($variables[$variableName]['type'] == 'from_list') {

                    $from_list = $variables[$variableName];
                    $newCode .= "var " . $from_list['variableName'] . "_raw=Core.rl(['" . implode("','", $from_list['list']) . "']);";
                    $newCode .= $from_list['variableName'] . "=parseFloat(" . $from_list['variableName'] . "_raw);";
                    $setCode .= "Core.st(this,'" . $from_list['variableName'] . "', " . $from_list['variableName'] . "_raw);";

                } else if ($variables[$variableName]['type'] == 'calculated') {

                    $calculated = $variables[$variableName];
                    if (isset($calculated['expression'])) $expression = $calculated['expression'];
                    else {
                        if ($calculated['json']['type'] != 'Equation') $root_array = null;
                        else if (count($calculated['json']['operands']) == 0) $root_array = null;
                        else if (!isset($calculated['json']['operands']['topLevelContainer'])) $root_array = null;
                        else $root_array = $calculated['json']['operands']['topLevelContainer'];					    
                        $expression = Wizard::getJSExpression($root_array);
                    }
                    
                    if ($expression === null) {
                        $newCode .= "throw{message:'Invalid expression in calculated variable \'" . 
                            $calculated['displayName'] . "\''};";
                    } else {
                        $newCode .= $calculated['variableName'] . "=" . $expression . ";";
                        
                        if ($calculated['sdType'] == 'Fraction') {
                            $setCode .= "Core.st(this,'" . $calculated['variableName'] . "',Core.rv(" . 
                                $calculated['variableName'] . "));";
                        } else {
                            if (isset($calculated['round'])) $roundImmediately = $calculated['round'];
                            else $roundImmediately = 0;
                            if ($roundImmediately) {
                                $newCode .= "t=Core.sv(" . $calculated['variableName'] . "," . 
                                    $calculated['sd'] . ",'" . $calculated['sdType'] . "');";
                                $newCode .= "Core.st(this,'" . $calculated['variableName'] . "',t);";
                                $newCode .= $calculated['variableName'] . "=Core.vs(t);";
                            }  else {
                                $setCode .= "Core.st(this,'" . $calculated['variableName'] . "',Core.sv(" . 
                                    $calculated['variableName'] . "," . $calculated['sd'] . ",'" . 
                                    $calculated['sdType'] . "'));";
                            }	
                        }
                    }	
                    
                } else if ($variables[$variableName]['type'] == 'conditional') {

                    $conditional = $variables[$variableName];

                    $part_expressions = array();
                    $part_names = array('if_json', 'then_json', 'else_json');
                    $good_parts = true;

                    foreach ($part_names as $part_name) {
                        $json_data = $conditional[$part_name];
                        if ($json_data['type'] != 'Equation') $good_parts = false;
                        else if (count($json_data['operands']) == 0) $good_parts = false;
                        else if (!isset($json_data['operands']['topLevelContainer'])) $good_parts = false;
                        else {
                            $root_array = $json_data['operands']['topLevelContainer'];

                            if ($part_name == 'if_json') {

                                // Make sure if expression is a comparison

                                $if_good = false;
                                foreach ($root_array as $root_element) {
                                    if ($root_element['type'] == 'Operator') {
                                        if ($root_element['value'] == '=') $if_good = true;
                                        else if ($root_element['value'] == '<') $if_good = true;
                                        else if ($root_element['value'] == '>') $if_good = true;
                                    }
                                }
                                if (!$if_good) $good_parts = false;
                            }
                            
                            $this_expression = Wizard::getJSExpression($root_array);
                            if ($this_expression !== null) {
                                $part_expressions[$part_name] = $this_expression;
                            } else $good_parts = false;
                        }
                    }
                    
                    if (!$good_parts) {
                        $newCode .= "throw{message:'Invalid expression in calculated variable \'" . 
                            $conditional['displayName'] . "\''};";
                    } else {
                        $expression = "(" . $part_expressions['if_json'] . ")?(" . $part_expressions['then_json'] . 
                            "):(" . $part_expressions['else_json'] . ")";
                        $newCode .= $conditional['variableName'] . "=" . $expression . ";";
                        
                        if ($conditional['sdType'] == 'Fraction') {
                            $setCode .= "Core.st(this,'" . $conditional['variableName'] . "',Core.rv(" . 
                                $conditional['variableName'] . "));";
                        } else {
                            if (isset($conditional['round'])) $roundImmediately = $conditional['round'];
                            else $roundImmediately = 0;
                            if ($roundImmediately) {
                                $newCode .= "t=Core.sv(" . $conditional['variableName'] . "," . 
                                    $conditional['sd'] . ",'" . $conditional['sdType'] . "');";
                                $newCode .= "Core.st(this,'" . $conditional['variableName'] . "',t);";
                                $newCode .= $conditional['variableName'] . "=Core.vs(t);";
                            }  else {
                                $setCode .= "Core.st(this,'" . $conditional['variableName'] . "',Core.sv(" . 
                                    $conditional['variableName'] . "," . $conditional['sd'] . ",'" . 
                                    $conditional['sdType'] . "'));";
                            }	
                        }
                    }	
                    
                }
            }

            $newCode .= $setCode;
        } else {
            $newCode .= "throw{message:'Bad reference in calculated variables'};";			
        }

        return $newCode;
    }
}
<?php

class Scoring {
	
    public static $responseAnswerIds = false;

	public static function getWeightGroups($scoring_data) {
		$scoring_type = $scoring_data['Scoring']['type'];
		$rawWeights = $scoring_data['Scoring']['weights'];

		$groups = array();
		if ($scoring_type == 'Total') {

			$groups = false;

		} else if ($scoring_type == 'Sections') {

			$questionIndex = 0;
			foreach ($scoring_data['Sections'] as $section) {
				$indices = array();
				foreach ($section['Content'] as $item) {
					$indices[] = $questionIndex;
					++$questionIndex;
				}

				$weight = 0.0;
				foreach ($rawWeights as $entry) {
					if ($section['id'] == $entry['section_id']) $weight = $entry['weight'];
				}

				$groups[] = array(
					'section_id' => $section['id'],
					'weight' => $weight,
					'indices' => $indices
				);
			}

		} else if ($scoring_type == 'Outcomes') {

			$questionIndices = array();
			$questionIndex = 0;
			foreach ($scoring_data['Sections'] as $section) {
				foreach ($section['Content'] as $item) {
					$item_id = $item['id'];
					$questionIndices[$item_id] = $questionIndex++;
				}
			}

			foreach ($scoring_data['Outcomes'] as $outcome) {
				$indices = array();
				foreach ($outcome['item_ids'] as $item_id) {
					$indices[] = $questionIndices[$item_id];
				}

				$weight = 0.0;
				foreach ($rawWeights as $entry) {
					if ($outcome['id'] == $entry['outcome_id']) $weight = $entry['weight'];
				}

				$groups[] = array(
					'outcome_id' => $outcome['id'],
					'weight' => $weight,
					'indices' => $indices
				);
			}

		} else if ($scoring_type == 'Custom') {

			$questionIndices = array();
			$questionIndex = 0;
			foreach ($scoring_data['Sections'] as $section) {
				foreach ($section['Content'] as $item) {
					$item_id = $item['id'];
					$questionIndices[$item_id] = $questionIndex++;
				}
			}

			foreach ($rawWeights as $entry) {
				$indices = array();
				foreach ($entry['item_ids'] as $item_id) {
					$indices[] = $questionIndices[$item_id];
				}

				$groups[] = array(
					'name' => $entry['name'],
					'weight' => $entry['weight'],
					'indices' => $indices
				);
			}

		}

		return $groups;
	}

	public static function getWeights($scoring_data) {
		$scoring_type = $scoring_data['Scoring']['type'];

		$weights = array();

		if ($scoring_type == 'Total') {

			foreach ($scoring_data['Sections'] as $section) {
				foreach ($section['Content'] as $question) {
					$weights[] = 1.0;
				}
			}

		} else {

			$questionData = array();
			foreach ($scoring_data['Sections'] as $section) {
				foreach ($section['Content'] as $question) {

					// Get this question's max score

					$isOmitted = false;
					if (isset($question['scoring']) && ($question['scoring'] == 'omit')) {
						$maxValue = 0.0;
						$isOmitted = true;
					} else if (isset($question['scoring']) && ($question['scoring'] == 'bonus')) {
						$maxValue = 0.0;
					} else if ($question['type'] == 'wr') {
						$maxValue = 0.0;
						$criteria = $question['TypeDetails']['criteria'];
						foreach ($criteria as $entry) $maxValue += $entry['value'];
					} else {
						$answer_map = array();
						foreach ($question['Answers'] as $answer) {
							$answer_map[$answer['id']] = $answer['response'];
						}

						$maxValue = 0.0;
						foreach ($question['ScoredResponses'] as $entry) {
							$ids = array();
							if (array_key_exists('id', $entry)) $ids = array($entry['id']);
							else if (array_key_exists('ids', $entry)) $ids = $entry['ids'];

							$response = '';
							foreach ($ids as $id) $response .= $answer_map[$id];

							$thisValue = $entry['value'];
							if ($question['type'] == 'nr_selection') {
								if ($question['TypeDetails']['markByDigits']) $thisValue *= strlen($response);
							}
							if ($thisValue > $maxValue) $maxValue = $thisValue;
						}
					}

					$questionData[] = array(
						'omitted' => $isOmitted,
						'maxValue' => $maxValue
					);
				}
			}

			$groups = Scoring::getWeightGroups($scoring_data);

			for ($i = 0; $i < count($questionData); ++$i) $weights[] = 0.0;
			
			$totalWeight = 0.0;
			foreach ($groups as $group) {
				$totalWeight += $group['weight'];
			}
			foreach ($groups as $group) {
				$numQuestions = count($group['indices']);
	
				$totalValue = 0.0;
				for ($i = 0; $i < $numQuestions; ++$i) {
					$questionIndex = $group['indices'][$i];
					if (!$questionData[$questionIndex]['omitted']) {
						$totalValue += $questionData[$questionIndex]['maxValue'];
					}
				}

				if (($totalValue == 0.0) || ($totalWeight == 0.0)) $scale = 0.0;
				else $scale = ($group['weight'] / $totalWeight) / $totalValue;
				for ($i = 0; $i < $numQuestions; ++$i) {
					$questionIndex = $group['indices'][$i];
					if (!$questionData[$questionIndex]['omitted']) {
						$weights[$questionIndex] += $scale;
					}
				}
			}
			
		}

		return $weights;
	}

	private static function significantDigits($value) {
        preg_match('/[^\d.-]/', $value, $matches, PREG_OFFSET_CAPTURE);
		$end = empty($matches) ? strlen($value) : $matches[0][1];

		preg_match('/[^0.-]/', $value, $matches, PREG_OFFSET_CAPTURE);
		$start = empty($matches) ? $end : $matches[0][1];
        if ($start != $end) {

            $decimalPos = strpos($value, '.', $start);
            if (($decimalPos !== false) && ($decimalPos < $end)) $end--;
            
            return $end - $start;
    
        } else return substr_count($value, '0', 0, $end);
	}

    private static function bumpValue($value) {

        // Bump numeric string up by a little bit to ensure that 5's round correctly
    
        $bump = str_contains($value, '.') ? '1' : '.1';

        $eIndex = strpos($value, 'e');
        if ($eIndex === false) {
            return $value . $bump;
        } else {
            $mantissa = substr($value, 0, $eIndex);
            $exponent = substr($value, $eIndex);
            return $mantissa . $bump . $exponent;
        }
    }
    
	public static function scoreNumericResponse($response, $question) {
		$responseScore = 0.0;

		$questionType = $question['type'];
        if (substr($questionType, 0, 2) != 'nr') return false;
        if (strpos($response, '!') !== false) return false;

        if ($questionType != 'nr_selection') {
            $response = trim($response);
        }

		// Process response and options

		if (in_array($questionType, array('nr_decimal', 'nr_scientific'))) {
			
			$fudgeFactor = floatval($question['TypeDetails']['fudgeFactor']);
			if ($fudgeFactor >= 1.0) $fudgeFactor /= 100.0;
			if ($fudgeFactor < 1.0e-5) $fudgeFactor = 1.0e-5;
			
			$factorOfTenValue = floatval($question['TypeDetails']['tensValue']);
			$sigDigsBehaviour = $question['TypeDetails']['sigDigsBehaviour'];
			$sigDigsValue = floatval($question['TypeDetails']['sigDigsValue']);
	
		} else if ($questionType == 'nr_selection') {
				
			$markByDigits = $question['TypeDetails']['markByDigits'];
			$ignoreOrder = $question['TypeDetails']['ignoreOrder'];
			$partialValue = floatval($question['TypeDetails']['partialValue']);

		}
		
		// Check keyed responses
		
		foreach ($question['ScoredResponses'] as $entry) {
			$answer = false;
			foreach ($question['Answers'] as $answer_entry) {
				if ($answer_entry['id'] == $entry['id']) {
					if (array_key_exists('template', $answer_entry)) $answer = $answer_entry['template'];
					else if (array_key_exists('response', $answer_entry)) $answer = $answer_entry['response'];
				}
			}
			$value = $entry['value'];

			if (array_key_exists('ids', $entry)) $answerIds = $entry['ids'];
			else if (array_key_exists('id', $entry)) $answerIds = array($entry['id']);
			else $answerIds = false;

			$thisScore = 1.0;
			$answerMatched = false;
			if (in_array($questionType, array('nr_decimal', 'nr_scientific'))) {
				
				// Convert answer to scientific notation

                $adjustedResponse = preg_replace("/\s+/u", " ", $response); // Handle nbsp
                $adjustedResponse = trim($adjustedResponse);
                
				if (preg_match('/^-?(\d+(\.\d*)?|\.\d+)$/', $adjustedResponse)) {

                    $adjustedAnswer = strtolower(trim($answer));

					if ($questionType == 'nr_scientific') {
						if (($adjustedAnswer[0] == '+') || ($adjustedAnswer[0] == '-')) {
							$adjustedAnswer = substr($adjustedAnswer, 1);
						}

						$eIndex = strpos($adjustedAnswer, 'e');
						if ($eIndex === false) $exponentDigits = 0;
						else {
							if (($adjustedAnswer[$eIndex + 1] == '+') || ($adjustedAnswer[$eIndex + 1] == '-')) {
								$adjustedAnswer = substr($adjustedAnswer, 0, $eIndex + 1) . 
									substr($adjustedAnswer, $eIndex + 2);
							}
							$exponentDigits = strlen($adjustedAnswer) - $eIndex - 1;
						}
						
						// Convert response to scientific notation and count significant digits
						
						if ((strpos($response, '.') !== false) || ($exponentDigits  == 0) || (strlen($response) <= $exponentDigits)) {

							$responseSD = Scoring::significantDigits($response);
                            if ($responseSD == 0) $responseSD = 1;

                            $adjustedResponse = sprintf('%.' . ($responseSD - 1) . 'e', 
								floatval($response));

						} else {

							$temp = sprintf('%s.%se%s', $response[0], 
								substr($response, 1, strlen($response) - $exponentDigits - 1),
								substr($response, strlen($response) - $exponentDigits, 
								$exponentDigits));
							$responseValue = floatval($temp);

                            $responseSD = Scoring::significantDigits($temp);
                            if ($responseSD == 0) $responseSD = 1;

                            $adjustedResponse = sprintf('%.' . ($responseSD - 1) . 'e', 
								$responseValue);
                                
						}

					}
					
					// Get sig digs for this answer

					$answerSD = Scoring::significantDigits($adjustedAnswer);
					
					// Get sig digs for response

					$responseSD = Scoring::significantDigits($adjustedResponse);
					
					// Check and (if necessary) correct significant digits
					
					if ($answerSD != $responseSD) {
						if ($sigDigsBehaviour == 'Strict') $thisScore = 0.0;
						else {
							if ($thisScore > $sigDigsValue) $thisScore = $sigDigsValue;
							if ($sigDigsBehaviour == 'Round') {
								$minSD = min($answerSD, $responseSD);
								if ($minSD == 0) $minSD = 1;
								
                                $adjustedAnswer = Scoring::bumpValue($adjustedAnswer);
								$adjustedAnswer = sprintf('%.' . ($minSD - 1) . 'e', floatval($adjustedAnswer));

                                $adjustedResponse = Scoring::bumpValue($adjustedResponse);
								$adjustedResponse = sprintf('%.' . ($minSD - 1) . 'e', 
									floatval($adjustedResponse));
							}
						}
					}
					
					// Check the answer, allowing for a tolerance if it's been given
					
					$answerValue = floatval($adjustedAnswer);
					$responseValue = floatval($adjustedResponse);
					$minGood = $answerValue / (1.0 + $fudgeFactor);
					$maxGood = $answerValue * (1.0 + $fudgeFactor);
					if ($maxGood < $minGood) {
						$temp = $maxGood;
						$maxGood = $minGood;
						$minGood = $temp;
					}
					
					if (($responseValue >= $minGood) && ($responseValue <= $maxGood)) {
						$answerMatched = true;
					} else if ($factorOfTenValue > 0.0) {
						if ($responseValue != 0.0) $responseValue *= pow(10.0, 
							ceil(log10(abs($minGood / $responseValue))));
						if (($responseValue >= $minGood) && ($responseValue <= $maxGood)) {
							$answerMatched = true;
							if ($thisScore > $factorOfTenValue) $thisScore = $factorOfTenValue;
						} else $thisScore = 0.0;
					} else $thisScore = 0.0;
				} else $thisScore = 0.0;
				
			} else if ($questionType == 'nr_selection') {

				// Process selection question

				$answerDigits = strlen($answer);
				$responseDigits = strlen($response);

				$numMatches = 0;
				if ($ignoreOrder) {

					$numWildcards = 0;
					$remaining = str_split(str_replace(' ', '', $response));
                    $responseDigits = count($remaining);

					for ($j = 0; $j < $answerDigits; ++$j) {
						if ($answer[$j] != '?') {
							$matchIndex = array_search($answer[$j], $remaining);
							if ($matchIndex !== FALSE) {
								unset($remaining[$matchIndex]);
								$numMatches++;
							}
						} else $numWildcards++;
					}
					$numMatches += min($numWildcards, count($remaining));

				} else {

                    // If answer does not include the full number of digits available to students, we have to allow for 
                    // spaces around the student response in various places. Check all possibilities and give students
                    // the benefit of the doubt.

                    if ($answerDigits != $responseDigits) {
                        $trimmedResponse = trim($response);
                        $responseDigits = strlen($trimmedResponse);    
                    } else $trimmedResponse = $response;

                    $maxOffset = abs($answerDigits - $responseDigits);
					$checkDigits = min($answerDigits, $responseDigits);

                    $maxMatches = 0;
                    for ($offset = 0; $offset <= $maxOffset; ++$offset) {
                        if ($answerDigits > $responseDigits) {
                            $thisAnswer = substr($answer, $offset, $checkDigits);
                            $thisResponse = $trimmedResponse;
                            $goodOffset = true;
                        } else {
                            $thisAnswer = $answer;
                            $thisResponse = substr($trimmedResponse, $offset, $checkDigits);
                            $remainder = substr($trimmedResponse, 0, $offset) . substr($trimmedResponse, $offset + $checkDigits);
                            $goodOffset = (strlen(str_replace(' ', '', $remainder)) == 0);
                        }

                        if ($goodOffset) {
                            $numMatches = 0;
                            for ($j = 0; $j < $checkDigits; ++$j) {
                                if (($thisAnswer[$j] == '?') || ($thisResponse[$j] == $thisAnswer[$j])) {
                                    $numMatches++;
                                }
                            }    
    
                            if ($numMatches > $maxMatches) $maxMatches = $numMatches;    
                        }
                    }
                    $numMatches = $maxMatches;

				}

				if (!preg_match('/^[\d ]+$/', $response)) {
					$thisScore = 0.0;
				} else if ($markByDigits) {
					if ($numMatches > 0) $answerMatched = true;
					$thisScore = $numMatches;
				} else if (($numMatches == $answerDigits) && ($responseDigits == $answerDigits)) {
					$answerMatched = true;
					$thisScore = 1.0;
				} else if ($partialValue > 0.0) {
					if ($numMatches >= $answerDigits / 2) {
						$answerMatched = true;
						$thisScore = $partialValue;
					} else $thisScore = 0.0;
				} else $thisScore = 0.0;

			} else if ($questionType == 'nr_fraction') {

                $responseSplit = explode('/', $response);
                $answerSplit = explode('/', $answer);

                if (count($responseSplit) !== count($answerSplit)) {
                    $thisScore = 0.0;
                } else {
                    $answerMatched = true;

                    $numParts = count($responseSplit);
                    for ($i = 0; $i < $numParts; $i++) {
                        $responsePart = trim($responseSplit[$i]);
                        $answerPart = trim($answerSplit[$i]);
                        
                        if (preg_match('/^\d+$/', $answerPart)) {
                            if (preg_match('/^\d+$/', $responsePart)) {
                                if (intval($responsePart) !== intval($answerPart)) {
                                    $answerMatched = false;
                                }
                            } else $answerMatched = false;
                        } else if ($responsePart !== $answerPart) {
                            $answerMatched = false;
                        }                        
                    }

                    $thisScore = $answerMatched ? 1.0 : 0.0;
                }

            }

			$thisScore *= $value;
			if ($answerMatched && ($thisScore >= $responseScore)) {
				Scoring::$responseAnswerIds = $answerIds;
				$responseScore = $thisScore;
			}
		}

		return $responseScore;
	}

    public static function augmentQuestion($question) {

        if ($question['type'] == 'wr') {

            // Get max value

            $criteria = $question['TypeDetails']['criteria'];

            $maxValue = 0.0;
            foreach ($criteria as $entry) $maxValue += $entry['value'];

        } else if (!empty($question['ScoredResponses'])) {

            if (isset($question['TypeDetails']['markByDigits'])) {
                $markByDigits = $question['TypeDetails']['markByDigits'];
            } else $markByDigits = false;

            // Get max value and add response to ScoredResponses entries
            
            $maxValue = 0.0;
            foreach ($question['ScoredResponses'] as &$pEntry) {
                $entry_ids = array();
                if (array_key_exists('id', $pEntry)) $entry_ids = array($pEntry['id']);
                else if (array_key_exists('ids', $pEntry)) $entry_ids = $pEntry['ids'];

                $response_array = array();
                foreach ($question['Answers'] as $answer) {
                    if (in_array($answer['id'], $entry_ids)) $response_array[] = $answer['response'];
                }

                if (!empty($response_array)) {
                    
                    sort($response_array);
                    $pEntry['response'] = implode('', $response_array);

                    $thisValue = $pEntry['value'];
                    if ((strlen($pEntry['response']) > 0) && $markByDigits) $thisValue *= strlen($pEntry['response']);
                    if ($thisValue > $maxValue) $maxValue = $thisValue;

                } else $pEntry['response'] = '';
            }

            // Add unscored answers

            foreach ($question['Answers'] as $entry) {
                $has_scored = false;
                foreach ($question['ScoredResponses'] as $scored) {
                    if (array_key_exists('id', $scored) && ($scored['id'] == $entry['id'])) {
                        $has_scored = true;
                        break;
                    }
                }

                if (!$has_scored) {
                    $question['ScoredResponses'][] = array(
                        'id' => $entry['id'],
                        'response' => $entry['response'],
                        'value' => 0.0
                    );
                }
            }

        } else $maxValue = 0.0;

        $question['maxValue'] = $maxValue;

        return $question;
    }

    public static function getResponseScore($response, $question) {
        $questionType = $question['type'];
        Scoring::$responseAnswerIds = false;
        $responseScore = 0.0;

        if (strlen($response) == 0) return 0.0;
        else if (strpos($response, '!') !== false) return false;

        if ($questionType == 'wr') {
            
            $criteria = $question['TypeDetails']['criteria'];

            $regexString = '';
            foreach ($criteria as $entry) {
                $regexString .= '[ 0-' . $entry['value'] . '][ \+]';
            }

            if ($response[0] == '+') $response = ' ' . $response;
            if (strlen($response) % 2 == 1) $response .= ' ';

            if (preg_match('/^' . $regexString . '$/', $response) === 1) {

                $responseScore = 0.0;
                for ($j = 0; $j < count($criteria); ++$j) {
                    if ($response[2 * $j] == ' ') $this_score = 0;
                    else $this_score = intval($response[2 * $j]);
                    
                    $entry = $criteria[$j];
                    if (($this_score < $entry['value']) && ($response[2 * $j + 1] == '+')) {
                        $this_score += 0.5;
                    }

                    $responseScore += $this_score;
                }
                
            } else $responseScore = false;

        } else if (substr($questionType, 0, 2) == 'mc') {

            // Sort multiple choice responses alphabetically

            $characters = str_split(trim($response));
            sort($characters);
            $response = implode('', $characters);

            // Check keyed responses
            
            $responseScore = 0.0;
            foreach ($question['ScoredResponses'] as $entry) {
                $value = $entry['value'];
                if (($response == $entry['response']) && ($value >= $responseScore)) {
                    $responseScore = $value;
                }
            }

            // Get response IDs

            $matchedAnswerIDs = array();
            $validResponses = true;
            for ($j = 0; $j < strlen($response); ++$j) {
                $found = false;
                for ($k = 0; $k < count($question['Answers']); ++$k) {
                    if ($question['Answers'][$k]['response'] == $response[$j]) {
                        if (array_key_exists('id', $question['Answers'][$k])) {
                            $matchedAnswerIDs[] = $question['Answers'][$k]['id'];
                        } else $validResponses = false;
                        $found = true;
                        break;
                    }
                }
                if (!$found) $validResponses = false;
            }
            if (count($matchedAnswerIDs) != strlen($response)) $validResponses = false;
            
            if ($validResponses) {
                if (!empty($matchedAnswerIDs)) {
                    Scoring::$responseAnswerIds = $matchedAnswerIDs;
                }
            } else {
                $responseScore = false;
            }

        } else if (substr($questionType, 0, 2) == 'nr') {

            $responseScore = Scoring::scoreNumericResponse($response, $question);

        } else $responseScore = false;

        return $responseScore;
    }
	
	public static function scoreResults($scoring_data, $response_data) {
		App::uses('Scoring', 'Lib');

        $question_count = 0;
        foreach ($scoring_data['Sections'] as $section) {
            if (!array_key_exists('Content', $section)) continue;
            foreach ($section['Content'] as $question) {
                $question_count++;
            }
        }
        if (count($response_data) != $question_count) return false;

        $weights = Scoring::getWeights($scoring_data);

        $response_scores = array();
        $questions = array();

        $totalScore = 0.0;
        foreach ($scoring_data['Sections'] as $section) {
            if (!array_key_exists('Content', $section)) continue;
            foreach ($section['Content'] as $question) {

                $question = Scoring::augmentQuestion($question);

                $question['weight'] = $weights[count($questions)];
                
                if (empty($question['scoring'])) {
                    $totalScore += $question['maxValue'];
                }

                $newEntry = array(
                    'item_id' => $question['id'],
                    'maxValue' => $question['maxValue'],
                    'weight' => $question['weight']
                );

                if (!empty($question['scoring'])) {
                    if ($question['scoring'] == 'omit') $newEntry['omitted'] = 1;
                    else if ($question['scoring'] == 'bonus') $newEntry['bonus'] = 1;
                }

                $response_scores[] = $newEntry;
                $questions[] = $question;
            }
        }

        $results = array(
            'outOf' => $totalScore,
            'weighted' => ($scoring_data['Scoring']['type'] != 'Total')
        );

        $studentAnswerIds = array();
        $studentScores = array();
        for ($i = 0; $i < count($response_data); ++$i) {
            $response = $response_data[$i];
            $question = $questions[$i];

            $responseScore = Scoring::getResponseScore($response, $question);

            $response_scores[$i]['response'] = $response;
            if ($responseScore !== false) {
                $response_scores[$i]['score'] = $responseScore;
                if (Scoring::$responseAnswerIds !== false) {
                    $response_scores[$i]['ids'] = Scoring::$responseAnswerIds;
                }
            } else $response_scores[$i]['score'] = false;
        }
        
        $results['questions'] = $response_scores;

		return $results;
	}
    
	public static function update_set_stats($result_set_id) {
		$success = true;
		$approot = Configure::read('approot');

        // Delete old stats

        $questionStatisticsModel = ClassRegistry::init('QuestionStatistics');
        $questionStatisticsModel->deleteAll(array(
            'QuestionStatistics.result_set_id' => $result_set_id
        ));

        // Build new stats
        
        $resultSetModel = ClassRegistry::init('ResultSet');
        $resultSetModel->contain();
        $result_set = $resultSetModel->findById($result_set_id);

        if (!empty($result_set)) {
            if ($result_set['ResultSet']['stats_status'] != 'update') $success = false;
        } else $success = false;

        if (!$success) return false;

		$resultDataModel = ClassRegistry::init('ResultData');

        $resultDataModel->contain();
		$result_data = $resultDataModel->find('all', array(
			'conditions' => array('ResultData.result_set_id' => $result_set_id)
		));

/**************************************************************************************************/
/* Filter scored results                                                                          */
/**************************************************************************************************/

		$numQuestions = false;
		$outOf = false;

        $scoring_data = array();

        $good_responses = array();

        $versionDataModel = ClassRegistry::init('VersionData');

		foreach ($result_data as $this_data) {
			$data_id = $this_data['ResultData']['id'];

            // Get version data

            $version_data_id = $this_data['ResultData']['version_data_id'];
            if (!array_key_exists($version_data_id, $scoring_data)) {
                $versionDataModel->contain();
                $this_version_data = $versionDataModel->findById($version_data_id);
                if (!empty($this_version_data)) {
                    $this_scoring_data = json_decode($this_version_data['VersionData']['scoring_json'], true);
                    $scoring_data[$version_data_id] = $this_scoring_data;
                } else $this_scoring_data = false;
            } else $this_scoring_data = $scoring_data[$version_data_id];

            // Get scores for this response data

			if ($this_scoring_data !== false) {
				$response_data = json_decode($this_data['ResultData']['results'], true);
				$scores = Scoring::scoreResults($this_scoring_data, $response_data);
			} else $scores = false;

            if (!empty($scores['questions'])) {

                // Adjust scoring to build in weights (TEMPORARY)

                $thisOutOf = 0.0;
                foreach ($scores['questions'] as &$pQuestion) {
                    if ($pQuestion['score'] !== false) {
                        $pQuestion['score'] *= $pQuestion['weight'];
                        $pQuestion['maxValue'] *= $pQuestion['weight'];
                        $thisOutOf += $pQuestion['maxValue'];
                    }
                }
                $scores['outOf'] = $thisOutOf;                
            }
            
			if (!empty($scores['questions']) && ($scores['outOf'] > 0.0)) {

                $totalScore = 0.0;
                foreach ($scores['questions'] as $index => $scores_entry) {

                    // Thin questions to valid scores that are not omitted or bonus questions

                    $keep_question = true;
                    if ($scores_entry['score'] === false) $keep_question = false;
                    else if ($scores_entry['maxValue'] == 0.0) $keep_question = false;
                    else if (!empty($scores_entry['omitted'])) $keep_question = false;
                    else if (!empty($scores_entry['bonus'])) $keep_question = false;

                    if ($keep_question) $totalScore += $scores_entry['score'];
                    else unset($scores['questions'][$index]);
                }
                $scores['questions'] = array_values($scores['questions']);

                // Make sure question count and max score is consistent

				if ($numQuestions === false) $numQuestions = count($scores['questions']);
				else if ($numQuestions != count($scores['questions'])) $success = false;

				if ($outOf === false) $outOf = $scores['outOf'];
				else {
                    if (abs($outOf - $scores['outOf']) / $outOf > 1.0e-10) $success = false;
                }

                if ($success && ($totalScore > 0.0)) {
                    $good_responses[] = array(
                        'data_id' => $data_id,
                        'version_data_id' => $version_data_id,
                        'scores' => $scores,
                        'totalScore' => $totalScore
                    );
                }

            }
        }

        if (count($good_responses) == 0) $success = false;
        if ($numQuestions === false) $success = false;
        if ($outOf === false) $success = false;

        $resultSetModel = ClassRegistry::init('ResultSet');

        if (!$success) {
            $data = array(
                'id' => $result_set_id,
                'stats_status' => 'error',
                'average_score' => 0.0,
                'alpha' => 0.0
            );
            $resultSetModel->save($data);
    
            return false;
        }

/**************************************************************************************************/
/* Update result set statistics                                                                   */
/**************************************************************************************************/

        $average = 0.0;
        for ($i = 0; $i < count($good_responses); ++$i) {
            $average += $good_responses[$i]['totalScore'] / $good_responses[$i]['scores']['outOf'];
        }

        $average /= count($good_responses);

        if ((count($good_responses) > 1) && ($numQuestions > 5)) {
            $meanVariance = 0.0;
            $meanCovariance = 0.0;

            for ($i = 0; $i < $numQuestions; ++$i) {
                for ($j = 0; $j <= $i; ++$j) {
                    $average_i = 0.0;
                    $average_j = 0.0;
                    for ($k = 0; $k < count($good_responses); ++$k) {
                        $average_i += $good_responses[$k]['scores']['questions'][$i]['score'];
                        $average_j += $good_responses[$k]['scores']['questions'][$j]['score'];
                    }

                    if (count($good_responses) > 1) {
                        $average_i /= count($good_responses);
                        $average_j /= count($good_responses);
                        
                        $covariance = 0.0;
                        for ($k = 0; $k < count($good_responses); ++$k) {
                            $score_i = $good_responses[$k]['scores']['questions'][$i]['score'];
                            $score_j = $good_responses[$k]['scores']['questions'][$j]['score'];
                            $covariance += ($score_i - $average_i) * ($score_j - $average_j);
                        }
                        $covariance /= count($good_responses);
                        
                        if ($i == $j) $meanVariance += $covariance;
                        else $meanCovariance += $covariance;    
                    }    
                }
            }

            $meanVariance /= $numQuestions;
            $meanCovariance /= $numQuestions * ($numQuestions - 1) / 2;
        
            $denominator = $meanVariance + ($numQuestions - 1) * $meanCovariance;
            if ($denominator > 0.0) {
                $alpha = $numQuestions * $meanCovariance / $denominator;
                if ($alpha < 0.0) $alpha = 0.0;
            } else $alpha = 0.0;
        } else $alpha = 0.0;

		$data = array(
            'id' => $result_set_id,
            'stats_status' => 'ready',
            'average_score' => $average,
            'alpha' => $alpha
        );
		$resultSetModel->save($data);

/**************************************************************************************************/
/* Update question statistics                                                                     */
/**************************************************************************************************/

        $part_data = array();

        $questionHandleModel = ClassRegistry::init('QuestionHandle');
        $question_handle_map = array();

        foreach ($good_responses as $responses_entry) {
            $data_id = $responses_entry['data_id'];
            $version_data_id = $responses_entry['version_data_id'];
            $scores = $responses_entry['scores'];
            
            $this_scoring_data = $scoring_data[$version_data_id];

            // Update the map from question handles to question IDs

            $new_question_handle_ids = array();
            foreach ($this_scoring_data['Sections'] as $section) {
                foreach ($section['Content'] as $item) {
                    if (array_key_exists('Source', $item)) {
                        $question_handle_id = $item['Source']['question_handle_id'];
                        if (!array_key_exists($question_handle_id, $question_handle_map)) {
                            $new_question_handle_ids[] = $question_handle_id;
                        }
                    }
                }
            }

            if (!empty($new_question_handle_ids)) {
                $questionHandleModel->contain();
                $new_handles_map = $questionHandleModel->find('list', array(
                    'fields' => array('QuestionHandle.id', 'QuestionHandle.question_id'),
                    'conditions' => array('QuestionHandle.id' => $new_question_handle_ids)
                ));

                foreach ($new_handles_map as $question_handle_id => $question_id) {
                    $question_handle_map[$question_handle_id] = $question_id;
                }
            }

            // Get score data for this part

            foreach ($scores['questions'] as $scores_entry) {
                if ($scores_entry['score'] === false) continue;

                $item_id = $scores_entry['item_id'];

                // Get sums for x and y

                $sumX = 0.0;
                $nx = 0;
                $sumY = 0.0;
                $ny = 0;
                foreach ($scores['questions'] as $question) {
                    $score = $question['score'] / $question['maxValue'];
                    if ($question['item_id'] == $item_id) {
                        $sumX += $score;
                        $nx++;
                    } else {
                        $sumY += $score;
                        $ny++;
                    }
                }
    
                // Find QuestionHandle ID and part ID for this item

                $question_handle_id = false;
                $part_id = false;
                foreach ($this_scoring_data['Sections'] as $section) {
                    foreach ($section['Content'] as $item) {
                        if ($item['id'] == $item_id) {
                            if (array_key_exists('Source', $item)) {
                                $question_handle_id = $item['Source']['question_handle_id'];
                                $part_id = $item['Source']['part_id'];
                            }
                        }
                    }
                }

                $attachQuestion = true;
                if ($question_handle_id !== false) {
                    if (!array_key_exists($question_handle_id, $question_handle_map)) $attachQuestion = false;	
                } else $attachQuestion = false;

                if (($nx > 0) && ($ny > 0) && $attachQuestion) {
                    $question_id = $question_handle_map[$question_handle_id];
                    $part_key = $question_id . '.' . $part_id;

                    if (!array_key_exists($part_key, $part_data)) {
                        $part_data[$part_key] = array();
                    }

                    // Get a statistics entry for this question part

                    $new_entry = array(
                        'x' => $sumX / $nx,
                        'y' => $sumY / $ny
                    );

                    if (array_key_exists('ids', $scores_entry)) {
                        $new_entry['ids'] = $scores_entry['ids'];
                    }
                    if (array_key_exists('response', $scores_entry)) {
                        $new_entry['response'] = $scores_entry['response'];
                    }

                    $part_data[$part_key][$data_id] = $new_entry;
                }
            }
        }

        $variables_regex = '#<(span)[^>]*?\\bclass\\s*=\\s*"[^"]*\\bblock_variable\\b[^"]*"[^>]*>(.*?)<\\/\\1>#';

        $question_map = array_flip($question_handle_map);

        $questionModel = ClassRegistry::init('Question');

		if (!empty($question_map)) {

			// Get question data

            $emptySums = array('n' => 0, 'sumY' => 0, 'sumY2' => 0);
            
			foreach ($question_map as $question_id => $question_handle_id) {

                $questionModel->contain();
				$question = $questionModel->findById($question_id);
				if (!empty($question) && ($question['Question']['question_content_id'] !== null)) {

					$json_data = json_decode($question['Question']['json_data'], true);
					$content_ids = json_decode($question['Question']['content_ids'], true);

                    // Update QuestionStatistics entries
                
                    $newStats = array();
                    $has_stats = false;

                    // Get part and response sums

                    foreach ($json_data['Parts'] as $part) {
                        if ($part['type'] == 'context') continue;

                        $part_id = $part['id'];
                        $part_content_id = $content_ids[$part_id];

                        $newStats[$part_content_id] = array();

                        $part_key = $question_id . '.' . $part_id;

                        if (substr($part['type'], 0, 2) == 'mc') {
                            $sumType = 'ids_matched';
                        } else if (substr($part['type'], 0, 2) == 'nr') {

                            $has_variables = false;
                            foreach ($part['Answers'] as $questionAnswer) {
                                $template = $questionAnswer['template'];
                                $this_has_variables = preg_match($variables_regex, $template);
                                if ($this_has_variables) $has_variables = true;
                            }

                            $sumType = $has_variables ? 'ids_matched' : 'response';

                        } else if ($part['type'] == 'wr') {
                            $sumType = 'unkeyed';
                        }

                        if (array_key_exists($part_key, $part_data)) {
                            foreach ($part_data[$part_key] as $data_id => $data) {
                                $x = $data['x'];
                                $y = $data['y'];
    
                                $has_stats = true;

                                if ($sumType == 'ids_matched') {

                                    if (empty($data['ids'])) {

                                        if (array_key_exists('response', $data)) {
                                            $response = $data['response'];
    
                                            for ($index = 0; $index < count($newStats[$part_content_id]); ++$index) {
                                                if (array_key_exists('response', $newStats[$part_content_id][$index])) {
                                                    if ($response == $newStats[$part_content_id][$index]['response']) break;
                                                }
                                            }
                                            if ($index == count($newStats[$part_content_id])) {
                                                $newEntry = array_merge(array(
                                                    'response' => $response
                                                ), $emptySums);
                                                $newStats[$part_content_id][] = $newEntry;
                                            }
                                            $newStats[$part_content_id][$index]['n']++;
                                            $newStats[$part_content_id][$index]['sumY'] += $y;
                                            $newStats[$part_content_id][$index]['sumY2'] += $y * $y;	
                                        }

                                    } else {

                                        $ids_array = array();
                                        $goodIDs = true;
                                        foreach ($data['ids'] as $thisID) {
                                            if (!empty($content_ids[$thisID])) {
                                                $ids_array[] = $content_ids[$thisID];
                                            } else $goodIDs = false;
                                        }
    
                                        if ($goodIDs) {
    
                                            sort($ids_array);
        
                                            for ($index = 0; $index < count($newStats[$part_content_id]); ++$index) {
                                                if (array_key_exists('ids_matched', $newStats[$part_content_id][$index])) {
                                                    if ($ids_array == $newStats[$part_content_id][$index]['ids_matched']) break;
                                                }
                                            }
                                            if ($index == count($newStats[$part_content_id])) {
                                                $newEntry = array_merge(array(
                                                    'ids_matched' => $ids_array
                                                ), $emptySums);
                                                $newStats[$part_content_id][] = $newEntry;
                                            }
                                            $newStats[$part_content_id][$index]['n']++;
                                            $newStats[$part_content_id][$index]['sumY'] += $y;
                                            $newStats[$part_content_id][$index]['sumY2'] += $y * $y;	
        
                                        }

                                    }
    
                                } else if ($sumType == 'response') {
    
                                    if (array_key_exists('response', $data)) {
                                        $response = $data['response'];

                                        for ($index = 0; $index < count($newStats[$part_content_id]); ++$index) {
                                            if (array_key_exists('response', $newStats[$part_content_id][$index])) {
                                                if ($response == $newStats[$part_content_id][$index]['response']) break;
                                            }
                                        }
                                        if ($index == count($newStats[$part_content_id])) {
                                            $newEntry = array_merge(array(
                                                'response' => $response
                                            ), $emptySums);
                                            $newStats[$part_content_id][] = $newEntry;
                                        }
                                        $newStats[$part_content_id][$index]['n']++;
                                        $newStats[$part_content_id][$index]['sumY'] += $y;
                                        $newStats[$part_content_id][$index]['sumY2'] += $y * $y;	
                                    }
    
                                } else if ($sumType == 'unkeyed') {
                                    
                                    if (empty($newStats[$part_content_id])) {
                                        $newStats[$part_content_id] = array(
                                            'n' => 0.0,
                                            'sumX' => 0.0,
                                            'sumY' => 0.0,
                                            'sumX2' => 0.0,
                                            'sumXY' => 0.0,
                                            'sumY2' => 0.0
                                        );		
                                    }
    
                                    $newStats[$part_content_id]['n']++;
                                    $newStats[$part_content_id]['sumX'] += $x;
                                    $newStats[$part_content_id]['sumY'] += $y;
                                    $newStats[$part_content_id]['sumX2'] += $x * $x;
                                    $newStats[$part_content_id]['sumXY'] += $x * $y;
                                    $newStats[$part_content_id]['sumY2'] += $y * $y;	
    
                                }
                            }
                        }
                    }

                    // Store new stats

                    if ($has_stats) {
                        $data = array();
                        $data['result_set_id'] = $result_set_id;
                        $data['question_handle_id'] = $question_handle_id;
                        $data['question_content_id'] = $question['Question']['question_content_id'];
                        $data['statistics'] = json_encode($newStats);
                        $questionStatisticsModel->create();
                        $questionStatisticsModel->save($data);
                    }
                }
			}
		}

        return true;
	}
}

?>
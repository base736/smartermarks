<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('Email', 'Lib');

class SendErrorsShell extends AppShell {
	public $uses = array('ErrorReport');
	
    public function main() {
        $cache_data = Cache::read('error_reports');
		if (!empty($cache_data)) {
			Cache::delete('error_reports');

			$reports = json_decode($cache_data, true);
			foreach ($reports as $key => $report) {
				$secrets = Configure::read('secrets');
				Email::send($secrets['admin_email'], $report['subject'], "general_error",
					array('data' => $report['data'], 'emails' => $report['emails'])
				);	
			}
		}
	}
}

?>

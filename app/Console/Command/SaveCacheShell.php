<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('Email', 'Lib');

class SaveCacheShell extends AppShell {
	public $uses = array('ResponseOnline');
	
    public function main() {
		$approot = Configure::read('approot');
		$lockFile = fopen($approot . "Console/save_cache.lock", "c");
	    if ($lockFile !== false) {
		    if (flock($lockFile, LOCK_EX | LOCK_NB)) {
				$minTime = date('Y-m-d H:i:s', strtotime("-30 minutes"));

				$this->ResponseOnline->includeDeleted();
				$attempt_ids = $this->ResponseOnline->find('list', array(
					'fields' => array('ResponseOnline.id'),
					'conditions' => array('ResponseOnline.deprecated <=' => $minTime)
				));
				$this->ResponseOnline->excludeDeleted();

				foreach ($attempt_ids as $attempt_id) {
					$cache_data = Cache::read($attempt_id, 'responses_online');
					if (!empty($cache_data)) {
						$this->ResponseOnline->saveCacheData($cache_data);
					} else {
						$data = array();
						$data['id'] = $attempt_id;
						$data['deprecated'] = null;
						$this->ResponseOnline->save($data);

						$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
						Email::queue_error("SmarterMarks: Missing cache in SaveCacheShell", null, array(
							'ID' => $attempt_id, 
							'Backtrace' => $backtrace
						));
					}
				}
			}

			fclose($lockFile);
		}
	}
}
?>

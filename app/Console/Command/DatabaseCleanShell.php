<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class DatabaseCleanShell extends AppShell {
	public $uses = array('Document', 'Assessment', 'Sitting', 'UserQuestion', 'Community', 'Sitting');
	
    public function main() {
		$approot = Configure::read('approot');

		$this->Document->deleteAll('Document.deleted <= DATE_SUB(NOW(), INTERVAL 1 MONTH)', 
			true, true);

		$this->Assessment->deleteAll('Assessment.deleted <= DATE_SUB(NOW(), INTERVAL 1 MONTH)', 
			true, true);

		$this->Sitting->deleteAll('Sitting.deleted <= DATE_SUB(NOW(), INTERVAL 1 MONTH)', 
			true, true);

		$this->UserQuestion->deleteAll('UserQuestion.deleted <= DATE_SUB(NOW(), INTERVAL 1 MONTH)', 
			true, true);

		$this->Community->deleteAll('Community.deleted <= DATE_SUB(NOW(), INTERVAL 1 MONTH)', 
			true, true);

		$this->Sitting->updateAll(
			array(
				'Sitting.thinned_cache' => '',
				'Sitting.secrets_cache' => '',
				'Sitting.caches_accessed' => NULL
			), array(
				'Sitting.caches_accessed <= DATE_SUB(NOW(), INTERVAL 1 DAY)'
			)
		);
	}
}
?>

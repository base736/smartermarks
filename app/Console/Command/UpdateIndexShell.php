<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class UpdateIndexShell extends AppShell {
	public $uses = array('Question', 'Folder', 'SearchData');

    public function html_to_text($html) {
        $html = preg_replace('#<img[^>]*>#', ' [Image] ', $html);
        $html = preg_replace('#<div[^>]*class="[^"]*equation[^"]*"[^>]*>#', ' [Equation] ', $html);
        $html = preg_replace('#<span[^>]*class="[^"]*block_variable[^"]*"[^>]*>#', ' [Variable] ', $html);
        $html = preg_replace('#<span[^>]*class="[^"]*block_bigBlank[^"]*"[^>]*>#', ' [Blank] ', $html);

        $html = preg_replace('#<\\\\?/li>\s*<\\\\?/ol>#', '.', $html);
        $html = preg_replace('#<\\\\?/li>\s*<\\\\?/ul>#', '.', $html);
        $html = preg_replace('#<\\\\?/li>#', ';', $html);

        $html = preg_replace('#<\\\\?/td>\s*<\\\\?/tr>#', '.', $html);
        $html = preg_replace('#<\\\\?/td>#', ';', $html);

        $skip_tags = array('span', 'b', 'i', 'u', 'sup', 'sub');
        foreach ($skip_tags as $tag_name) {
            $html = preg_replace('#<' . $tag_name . '[^>]*>#', '', $html);
            $html = preg_replace('#<\\\\?/' . $tag_name . '>#', '', $html);
        }

        $html = preg_replace('#<[^>]*>#', ' ', $html);
        $html = preg_replace('#</[^>]*>#', ' ', $html);
        $text = html_entity_decode($html);
        
        $text = str_replace("\u{00A0}", ' ', $text);
        $text = preg_replace('/\s+;/', ';', $text);
        $text = preg_replace('/\s+\./', '.', $text);

        return $text;
    }

    public function get_vector($text) {
        $vector = false;
        
        if (strlen($text) > 0) {
            $secrets = Configure::read('secrets');

            $post_data = array("input" => $text);
            $post_data += $secrets['openai']['embedding_options'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $secrets['openai']['embedding_url']);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $secrets['openai']['secret_key']
            ));

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));

            $attempt_count = 0;
            while ($vector === false && ++$attempt_count < 5) {
                $result = curl_exec($ch);

                if (!empty($result)) {
                    $data = json_decode($result, true);
                    if (!empty($data['data'])) {
                        $vector = $data['data'][0]['embedding'];
                    }
                }

                if ($vector === false) sleep(1);
            }

        }
        
        return $vector;
    }

    public function weaviate_delete_by_question_content_id($question_content_id) {
        $secrets = Configure::read('secrets');
        $weaviate_url = $secrets['weaviate']['url'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $weaviate_url . '/v1/graphql');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $post_data = array(
            "query" => '{
                Get {
                    Question(where: {
                        path: ["question_content_id"],
                        operator: Equal,
                        valueText: "' . $question_content_id . '"
                    }) {
                        question_content_id,
                        part_content_id,
                        _additional { id }
                    }
                }       
            }'
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($ch);

        if (!empty($result)) {
            $result_data = json_decode($result, true);

            if (!empty($result_data['data']['Get']['Question'])) {
                foreach ($result_data['data']['Get']['Question'] as $results_entry) {  
                    if (!empty($results_entry['_additional']['id'])) {
                        $delete_id = $results_entry['_additional']['id'];
                        
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $weaviate_url . '/v1/objects/Question/' . $delete_id);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json'
                        ));
                
                        $curl_result = curl_exec($ch);
                    }
                }
            }
        }
    }

    public function main() {
		ini_set('memory_limit','512M');

        $secrets = Configure::read('secrets');
        $weaviate_url = $secrets['weaviate']['url'];

		$approot = Configure::read('approot');
		$lockFile = fopen($approot . "Console/index_update.lock", "c");
		if (flock($lockFile, LOCK_EX | LOCK_NB)) {
					
            /**************************************************************************************************/
            /* Update vector database                                                                         */
            /**************************************************************************************************/

            $log_entry = "[" . date('Y-m-d H:i:s') . "] Starting scheduled update.\n"; 
            file_put_contents($approot . 'tmp/logs/index.log', $log_entry, FILE_APPEND);

            $minTime = date('Y-m-d H:i:s', strtotime("-3 days"));

            $db = $this->SearchData->getDataSource();

            $updated_question_content_ids = array();

            $minID = 0;
            $chunkSize = 100;
            $total_updated = 0;
    
            do {

                $post_data = array('objects' => array());

                $this->Question->contain();
				$results = $this->Question->find('all', array(
					'conditions' => array(
                        'Question.id >' => $minID,
                        'Question.needs_index IS NOT NULL',
                        'Question.needs_index >=' => $minTime
                    ),
                    'order' => 'Question.id ASC',
                    'limit' => $chunkSize
				));

                if (!empty($results)) {
    
                    foreach ($results as $root_question) {
                        $question_content_id = $root_question['Question']['question_content_id'];
                        $last_content_id = $root_question['Question']['last_content_id'];

                        if ($question_content_id != $last_content_id) {
                            $this->Question->id = $root_question['Question']['id'];
                            $this->Question->saveField('last_content_id', $question_content_id, array('callbacks' => false));
                        }

                        if (!empty($question_content_id) && !in_array($question_content_id, $updated_question_content_ids)) {

                            $updated_question_content_ids[] = $question_content_id;

                            // Save vectors for old content ID

                            $old_vectors = array();

                            $this->SearchData->contain();
                            $content_search_data = $this->SearchData->findByQuestionContentId($question_content_id);
                            if (!empty($content_search_data)) {
                                $this_entries = json_decode($content_search_data['SearchData']['vectors_json'], true);
                                foreach ($this_entries as $this_entry) {
                                    if (!empty($this_entry['part_text'])) {
                                        $old_vectors[$this_entry['part_text']['text_hash']] = $this_entry['part_text']['vector'];
                                    }
                                    if (!empty($this_entry['outcomes'])) {
                                        $old_vectors[$this_entry['outcomes']['text_hash']] = $this_entry['outcomes']['vector'];
                                    }
                                }
                            }

                            // Handle data that's orphaned by this change
                            
                            if ($last_content_id !== $question_content_id) {

                                $this->SearchData->contain();
                                $last_search_data = $this->SearchData->findByQuestionContentId($last_content_id);
                                if (!empty($last_search_data)) {
                                    $this_entries = json_decode($last_search_data['SearchData']['vectors_json'], true);
                                    foreach ($this_entries as $this_entry) {
                                        if (!empty($this_entry['part_text'])) {
                                            $old_vectors[$this_entry['part_text']['text_hash']] = $this_entry['part_text']['vector'];
                                        }
                                        if (!empty($this_entry['outcomes'])) {
                                            $old_vectors[$this_entry['outcomes']['text_hash']] = $this_entry['outcomes']['vector'];
                                        }
                                    }
                                }
        
                                $question_count = $this->Question->find('count', array(
                                    'conditions' => array(
                                        'Question.question_content_id' => $last_content_id
                                    )
                                ));

                                if ($question_count == 0) {

                                    // Remove SearchData entry for last content ID and matching objects from vector database

                                    $this->SearchData->deleteAll(array('SearchData.question_content_id' => $last_content_id));
                                    $this->weaviate_delete_by_question_content_id($last_content_id);
                                }
                            }
        
                            // Remove SearchData entry for this content ID and matching objects from vector database

                            $this->SearchData->deleteAll(array('SearchData.question_content_id' => $question_content_id));
                            $this->weaviate_delete_by_question_content_id($question_content_id);
        
                            // Set aside space for data we'll use in Weaviate, but gather while building our
                            // SearchData entry

                            $best_correlations = array();
                            $folder_ids = array();
                            $isOpen = false;

                            // Get data for SearchData entry

                            $keywords_elements = array();
                            $outcomes_elements = array();

                            $matching_ids = $this->Question->find('list', array(
                                'fields' => array('Question.id'),
                                'conditions' => array('Question.question_content_id' => $question_content_id)
                            ));

                            foreach ($matching_ids as $matching_id) {
                                $this->Question->contain(array(
                                    'QuestionHandle' => array(
                                        'AssessmentItem' => 'Assessment',
                                        'UserQuestion'
                                    )
                                ));
                                $question = $this->Question->findById($matching_id);

                                $json_data = json_decode($question['Question']['json_data'], true);
                                $content_ids = json_decode($question['Question']['content_ids'], true);

                                // Update best correlations

                                if ($question['Question']['stats_count'] > 50) {
                                    $stats_data = json_decode($question['Question']['stats_json'], true);
                                    if (!empty($stats_data)) {
                                        foreach ($json_data['Parts'] as $part_index => $part) {
                                            $part_id = $part['id'];
            
                                            if (array_key_exists($part_id, $stats_data)) {
                                                $this_correlation = $stats_data[$part_id]['correlation'];
            
                                                if (array_key_exists($part_id, $content_ids)) {
                                                    $part_content_id = $content_ids[$part_id];
                                                    if (!array_key_exists($part_content_id, $best_correlations) || ($this_correlation > $best_correlations[$part_content_id])) {
                                                        $best_correlations[$part_content_id] = $this_correlation;
                                                    }
                                                }
                                            }
                                        }    
                                    }
                                }

                                // Update is_open

                                if ($question['Question']['open_status'] != 0) $isOpen = true;

                                // Update label and outcomes elements
                                
                                foreach ($question['QuestionHandle'] as $handle) {

                                    if (!empty($handle['name_alias'])) {
                                        $keywords_elements[] = $handle['name_alias'];
                                    }

                                    if (!empty($handle['AssessmentItem'])) {
                                        foreach ($handle['AssessmentItem'] as $item) {
                                            if (!empty($item['Assessment']['json_data'])) {
                                                
                                                $assessment_data = json_decode($item['Assessment']['json_data'], true);

                                                // Add the name, title, and section header for each assessment this question is used in

                                                $keywords_elements[] = $item['Assessment']['save_name'];
                                                foreach ($assessment_data['Title']['text'] as $title_line) {
                                                    $trimmed_line = trim($title_line);
                                                    if (strlen($trimmed_line) > 0) $keywords_elements[] = $trimmed_line;
                                                }

                                                // Get content ID for this question in this assessment

                                                $content_id = false;
                                                foreach ($assessment_data['Sections'] as $section) {
                                                    if (empty($section['Content'])) continue;

                                                    foreach ($section['Content'] as $content) {
                                                        if (array_key_exists('question_handle_id', $content)) {
                                                            $content_handle_id = $content['question_handle_id'];
                                                            if ($content_handle_id == $handle['id']) {
                                                                $content_id = $content['id'];
                                                            }
                                                        } else if (array_key_exists('question_handle_ids', $content)) {
                                                            foreach ($content['question_handle_ids'] as $content_handle_id) {
                                                                if ($content_handle_id == $handle['id']) {
                                                                    $content_id = $content['id'];
                                                                }    
                                                            }
                                                        }
                                                    }    
                                                }

                                                // Add all outcomes each part has been tagged with
                                        
                                                if (($content_id !== false) && !empty($json_data)) {
                                                    foreach ($json_data['Parts'] as $part_index => $part) {
                                                        if ($part['type'] == 'context') continue;

                                                        $part_id = $part['id'];
                                                        if (array_key_exists($part_id, $content_ids)) {
                                                            $part_content_id = $content_ids[$part_id];

                                                            foreach ($assessment_data['Outcomes'] as $outcome) {
                                                                $is_included = false;
                                                                foreach ($outcome['parts'] as $outcomes_part) {
                                                                    $this_included = true;
                                                                    if ($outcomes_part['content_id'] != $content_id) $this_included = false;
                                                                    else if (array_key_exists('part_id', $outcomes_part)) {
                                                                        if ($outcomes_part['part_id'] != $part_id) $this_included = false;
                                                                    } else if (array_key_exists('part_index', $outcomes_part)) {
                                                                        if ($outcomes_part['part_index'] != $part_index) $this_included = false;
                                                                    }
                                                                    if ($this_included) $is_included = true;
                                                                }

                                                                if ($is_included) {
                                                                    if (!array_key_exists($part_content_id, $outcomes_elements)) {
                                                                        $outcomes_elements[$part_content_id] = array();
                                                                    }
                                                                    $outcomes_elements[$part_content_id][] = $outcome['name'];                                                        
                                                                }
                                                            }                                                    
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                    
                                    // Add the names of all folders in which this question is stored
                    
                                    if (!empty($handle['UserQuestion'])) {
                                        foreach ($handle['UserQuestion'] as $userQuestion) {
                                            if ($userQuestion['deleted'] == null) {
                                                $folder_id = intval($userQuestion['folder_id']);
                                                $folder_ids[] = $folder_id;

                                                do {
                                                    $this->Folder->contain();
                                                    $folder = $this->Folder->findById($folder_id);
                                                    if (empty($folder)) $folder_id = null;
                                                    else {
                                                        if ($folder['Folder']['parent_id'] != null) {
                                                            $keywords_elements[] = $folder['Folder']['name'];
                                                        }
                                                        $folder_id = $folder['Folder']['parent_id'];
                                                    }
                                                } while ($folder_id != null);
                                            }
                                        }
                                    }
                                }			
                                
                                // Add the question name
                                
                                if (!empty($question['Question']['name'])) {
                                    $keywords_elements[] = $question['Question']['name'];
                                }

                            }

                            if ($isOpen || !empty($folder_ids)) {

                                $keywords_elements = array_unique($keywords_elements);
                                $keywords_string = strtolower(implode(' / ', $keywords_elements));
                                $keywords_string = preg_replace('#[^a-z0-9\s\-./]+#', '', $keywords_string);
                                $keywords_string = preg_replace('#\s+#', ' ', $keywords_string);
        
                                $json_data = json_decode($root_question['Question']['json_data'], true);
                                $content_ids = json_decode($root_question['Question']['content_ids'], true);
        
                                $new_vectors = array();
                                foreach ($json_data['Parts'] as $part_index => $part) {
                                    if ($part['type'] == 'context') continue;
        
                                    if (array_key_exists($part['id'], $content_ids)) {
                                        $part_content_id = $content_ids[$part['id']];
        
                                        // Get text for this part and preceding contexts
        
                                        $part_text = '';
        
                                        $style = substr($part['type'], 0, 2);
                                        if ($style == 'mc') $part_text .= "Multiple choice (MC): ";
                                        else if ($style == 'nr') $part_text .= "Numeric response (NR): ";
                                        else if ($style == 'wr') $part_text .= "Written response (WR): ";
                                        
                                        for ($index = 0; $index < $part_index; ++$index) {
                                            if ($json_data['Parts'][$index]['type'] == 'context') {
                                                $part_text .= $this->html_to_text($json_data['Parts'][$index]['template']) . ' ';
                                            }
                                        }
        
                                        $part_text .= $this->html_to_text($part['template']). ' ';
        
                                        if ($style == 'mc') {
                                            $answers_text = array();
                                            if (array_key_exists('Common', $json_data)) {
                                                foreach ($json_data['Common']['Answers'] as $answer) {
                                                    $answers_text[] = $this->html_to_text($answer['template']);
                                                }
                                            } else if (!empty($part['Answers'])) {
                                                foreach ($part['Answers'] as $answer) {
                                                    if (array_key_exists('template', $answer)) {
                                                        $answers_text[] = $this->html_to_text($answer['template']);
                                                    } else if (array_key_exists('templates', $answer)) {
                                                        $columns_text = array();
                                                        foreach ($answer['templates'] as $column_template) {
                                                            $columns_text[] = $this->html_to_text($column_template);
                                                        }
                                                        $answers_text[] = implode(',', $columns_text);
                                                    }
                                                }
                                            } 
                                            $part_text .= implode('; ', $answers_text) . '. ';
                                        }
        
                                        // Get part text
        
                                        $part_text = preg_replace('/\s+/', ' ', $part_text);
        
                                        // Get outcomes text
        
                                        if (array_key_exists($part_content_id, $outcomes_elements)) {
                                            $sorted_elements = array_unique($outcomes_elements[$part_content_id]);
                                            sort($sorted_elements);
                                            $outcomes_text = implode('; ', $sorted_elements);    
                                        } else $outcomes_text = '';
        
                                        // Add these to the new vectors list
        
                                        $this_entry = array(
                                            'part_content_id' => $part_content_id,
                                            'part_type' => $part['type']
                                        );
        
                                        $vector_elements = array(
                                            'part_text' => $part_text,
                                            'outcomes' => $outcomes_text
                                        );
        
                                        foreach ($vector_elements as $name => $text) {
                                            if (strlen($text) > 0) {
                                                $text_hash = md5($text);

                                                if (array_key_exists($text_hash, $old_vectors)) {
                                                    $text_vector = $old_vectors[$text_hash];
                                                } else $text_vector = $this->get_vector($text);

                                                if ($text_vector !== false) {
                                                    $this_entry[$name] = array(
                                                        'text_hash' => $text_hash,
                                                        'vector' => $text_vector
                                                    );
                                                }
                                            }    
                                        }
        
                                        $new_vectors[] = $this_entry;
                                    }
                                }
        
                                // Build and save SearchData entry
        
                                $data = array(
                                    'question_content_id' => $question_content_id,
                                    'folder_ids' => implode(',', $folder_ids),
                                    'is_open' => $isOpen ? 1 : 0,
                                    'keywords_string' => $keywords_string,
                                    'vectors_json' => json_encode($new_vectors)
                                );
                    
                                $this->SearchData->create();
                                $this->SearchData->save($data);
            
                                // Update Weaviate database. Outcomes are weighted higher to ensure that expected terms are emphasized 
                                // when they're present, but the question itself can be used when they're not, or when they're insufficient.
                
                                $vector_weights = array(
                                    'part_text' => 0.3,
                                    'outcomes' => 0.7
                                );
                
                                foreach ($new_vectors as $entry) {
                                    $part_content_id = $entry['part_content_id'];
        
                                    $combined_vector = false;
                                    $total_weight = 0.0;
                                    foreach ($vector_weights as $key => $weight) {
                                        $is_empty = false;
                                        if (empty($entry[$key])) $is_empty = true;
                                        else if ($weight == 0.0) $is_empty = true;
                    
                                        if (!$is_empty) {
                                            if ($combined_vector === false) {
                                                $combined_vector = array();
                                                for ($i = 0; $i < count( $entry[$key]['vector']); ++$i) {
                                                    $combined_vector[] = $weight * $entry[$key]['vector'][$i];
                                                }
                                            } else if (count($combined_vector) == count($entry[$key]['vector'])) {
                                                for ($i = 0; $i < count($entry[$key]['vector']); ++$i) {
                                                    $combined_vector[$i] += $weight * $entry[$key]['vector'][$i];
                                                }    
                                            } else {
                                                $combined_vector = false;
                                                break;
                                            }
                                            $total_weight += $weight;
                                        }    
                                    }
                    
                                    if ($combined_vector !== false) {
                                          
                                        if (array_key_exists($part_content_id, $best_correlations)) {
                                            $best_correlation = $best_correlations[$part_content_id];
                                        } else $best_correlation = 0;

                                        // Get correlation weighted vector. Correlation weight: 0.2 results in many off-topic 
                                        // WR results with high correlation for "social studies 20 identity"
            
                                        $correlation_weight = 0.08; 
                    
                                        $best_vector = array();
                                        $scale = exp($correlation_weight * $best_correlation) / $total_weight;
                                        foreach ($combined_vector as $value) $best_vector[] = $scale * $value;
            
                                        $post_data['objects'][] = array(
                                            'class' => 'Question',
                                            'properties' => array(
                                                'question_content_id' => $question_content_id,
                                                'part_content_id' => $entry['part_content_id'],
                                                'keywords' => $keywords_string,
                                                'parts_count' => count($new_vectors),
                                                'type' => substr($entry['part_type'], 0, 2),
                                                'folder_ids' => $folder_ids,
                                                'is_open' => $isOpen
                                            ),
                                            'vector' => $best_vector
                                        );         
                                    }
                                }

                            }
                        }
                    }

                    $minID = $results[count($results) - 1]['Question']['id'];
                }

                if (!empty($post_data['objects'])) {

                    // Add new items as a batch

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $weaviate_url . '/v1/batch/objects?consistency_level=ALL');
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json'
                    ));
            
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
            
                    $curl_result = curl_exec($ch);

                    $total_updated += count($post_data['objects']);
                }   

            } while (count($results) >= $chunkSize);

            $log_entry = "[" . date('Y-m-d H:i:s') . "] Done updating " . $total_updated . " questions.\n"; 
            file_put_contents($approot . 'tmp/logs/index.log', $log_entry, FILE_APPEND);

		} else {

            $log_entry = "[" . date('Y-m-d H:i:s') . "] Skipping scheduled update.\n"; 
            file_put_contents($approot . 'tmp/logs/index.log', $log_entry, FILE_APPEND);

        }
		
		fclose($lockFile);
	}
}

?>

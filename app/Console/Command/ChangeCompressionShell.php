<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class ChangeCompressionShell extends AppShell {
	public $uses = array('VersionData');

    public function main() {
		ini_set('memory_limit','512M');

		$minID = 0;
        $chunkSize = 1000;

		do {
            $this->VersionData->contain();
			$results = $this->VersionData->find('all', array(
				'conditions' => array('VersionData.id >' => $minID),
				'order' => array('VersionData.id'),
				'limit' => $chunkSize
			));

			if (!empty($results)) {
				print "Reading VersionData " . $results[0]['VersionData']['id'] . " to " . 
					$results[count($results) - 1]['VersionData']['id'] . "\n";

				foreach ($results as $result) {
					if (empty($result['VersionData']['html_gzip'])) $html = NULL;
                    else {
						$html = gzdecode($result['VersionData']['html_gzip']);
						if ($html === false) {
							print $result['VersionData']['id'] . "\n";
							exit(0);
						}
					}

                    $this->VersionData->id = $result['VersionData']['id'];
                    $this->VersionData->saveField('html', $html, array(
						'callbacks' => false
					));
				}

                $minID = $results[count($results) - 1]['VersionData']['id'];
			}
		} while (count($results) >= $chunkSize);

    }
}
?>
import express from 'express';
import bodyParser from 'body-parser';
import * as ScoringCommon from '/js/common/scoring.js';

const app = express();
const port = 3000;

app.use(bodyParser.json({ limit: '50mb' }));

app.post('/', async (req, res) => {
    const data = req.body;
    const scoringData = data.scoring_data;
    const responseData = data.response_data;

    const scores = ScoringCommon.scoreResults(scoringData, responseData);

    // Respond immediately
    res.json(scores);
});

app.listen(port, async () => {
    console.log(`Server listening at http://localhost:${port}`);
});
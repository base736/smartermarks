<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('Scoring', 'Lib');

class UpdateStatsShell extends AppShell {
	public $uses = array('ResultSet', 'ResultData', 'QuestionStatistics', 
        'QuestionHandle', 'Question');

    public function main() {
		ini_set('memory_limit','512M');

		$approot = Configure::read('approot');
		$lockFile = fopen($approot . "Console/stats_update.lock", "c");
		if (flock($lockFile, LOCK_EX | LOCK_NB)) {
					
            /**************************************************************************************************/
            /* Update QuestionStatistics entries                                                              */
            /**************************************************************************************************/

			$chunkSize = 100;

			do {
				
				$update_ids = $this->ResultSet->find('list', array(
					'fields' => array('ResultSet.id'),
					'conditions' => array('ResultSet.stats_status' => 'update'),
                    'order' => array('ResultSet.id DESC'),
					'limit' => 100
				));	
				$update_ids = array_values($update_ids);
	
				if (!empty($update_ids)) {
					print "Updating ResultSet " . $update_ids[0] . " - " . $update_ids[count($update_ids) - 1] . "...\n";
				}
	
				foreach ($update_ids as $setIndex => $result_set_id) {
					Scoring::update_set_stats($result_set_id);
				}
	
			} while (count($update_ids) == $chunkSize);
            
		}
		
		fclose($lockFile);
	}
}

?>

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('Scoring', 'Lib');

function arrays_are_equal_recursive(array $array1, array $array2): bool {
    // Check if they have the same number of elements
    if (count($array1) !== count($array2)) {
        print "Count mismatch\n";
        return false;
    }
    
    // Check if they have the same keys
    $keys1 = array_keys($array1);
    $keys2 = array_keys($array2);
    sort($keys1);
    sort($keys2);
    if ($keys1 !== $keys2) {
        print "Keys mismatch\n";
        return false;
    }

    // Compare values recursively
    foreach ($array1 as $key => $value1) {
        $value2 = $array2[$key];
        
        // If both values are arrays, recurse
        if (is_array($value1) && is_array($value2)) {
            if (!arrays_are_equal_recursive($value1, $value2)) {
                return false;
            }
        } else {
            // Compare scalar or non-array values strictly
            if ($value1 != $value2) {
                print "Value mismatch in " . $key . "\n";
                print json_encode($value1) . "\n";
                print json_encode($value2) . "\n";
                return false;
            }
        }
    }

    return true;
}

class CheckScoringShell extends AppShell {
	public $uses = array('ResultData');

    public function main() {

		$minID = 0;
        $chunkSize = 1000;

		do {
            $this->ResultData->contain(array(
                'VersionData' => array(
                    'fields' => array('id', 'scoring_json')
                )
            ));
			$results = $this->ResultData->find('all', array(
				'conditions' => array('ResultData.id >' => $minID),
				'order' => array('ResultData.id'),
				'limit' => $chunkSize
			));

			if (!empty($results)) {
				print "Reading ResultData " . $results[0]['ResultData']['id'] . " to " . 
					$results[count($results) - 1]['ResultData']['id'] . "\n";

				foreach ($results as $result) {
                    $scoring_data = json_decode($result['VersionData']['scoring_json'], true);
                    $response_data = json_decode($result['ResultData']['results'], true);
                    $scores_php = Scoring::scoreResults($scoring_data, $response_data);

                    $url = 'http://localhost:3000';
                    $post_data = array('Hello' => 'world');
                    $json_data = json_encode(array(
                        'scoring_data' => $scoring_data,
                        'response_data' => $response_data
                    ));

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                
                    $response = curl_exec($ch);
                    $curl_error = curl_error($ch);
                
                    if (!$response || $curl_error) {

                        print "ERROR: Bad response from score server.\n";
                        exit(-1);

                    } else {

                        $scores_js = json_decode($response, true);
                        if (!empty($scores_js) && !empty($scores_php)) {
                            $scores_match = arrays_are_equal_recursive($scores_js, $scores_php);
                            if (!$scores_match) {

                                print "Mismatch in ResultData " . $result['ResultData']['id'] . "\n\n";
                                print json_encode($scores_js) . "\n\n";
                                print json_encode($scores_php) . "\n\n";
                                exit(0);        
                                                        
                            }    
                        } else if ($scores_js != $scores_php) {

                            print "Mismatch in ResultData " . $result['ResultData']['id'] . "\n\n";
                            print json_encode($scores_js) . "\n\n";
                            print json_encode($scores_php) . "\n\n";
                            exit(0);                                

                        }
                    }
				}

                $minID = $results[count($results) - 1]['ResultData']['id'];
			}
		} while (count($results) >= $chunkSize);

    }
}
?>
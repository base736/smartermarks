<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('Email', 'Lib');

class CheckDatabaseShell extends AppShell {
	public $uses = array('Sitting', 'Question', 'Assessment', 'Document', 'VersionData', 'User');
	
    public function main() {

		$chunkSize = 100;

		$minID = 0;
		do {
			$this->Question->contain();
			$results = $this->Question->find('all', array(
				'conditions' => array('Question.id >' => $minID),
				'order' => array('Question.id'),
				'limit' => $chunkSize
			));

			if (!empty($results)) {
				print "Reading Question " . $results[0]['Question']['id'] . " to " . 
					$results[count($results) - 1]['Question']['id'] . "\n";

				foreach ($results as $result) {
					$json_data = json_decode($result['Question']['json_data'], true);
                    if (!empty($json_data)) {
                        $check_result = $this->Question->checkData($json_data);
                        if ($check_result !== true) {
                            print $result['Question']['id'] . "\n\n";
                            print json_encode($json_data) . "\n\n";
                            print $check_result . "\n";
                            exit(-1);
                        }
                    }
				}

                $minID = $results[count($results) - 1]['Question']['id'];
			}
		} while (count($results) >= $chunkSize);

        $chunkSize = 100;
		
		$minID = 0;
		do {
			$this->Assessment->contain();
			$results = $this->Assessment->find('all', array(
				'conditions' => array('Assessment.id >' => $minID),
				'order' => array('Assessment.id'),
				'limit' => $chunkSize
			));

			if (!empty($results)) {
				print "Reading Assessment " . $results[0]['Assessment']['id'] . " to " . 
					$results[count($results) - 1]['Assessment']['id'] . "\n";

				foreach ($results as $result) {
					$json_data = json_decode($result['Assessment']['json_data'], true);
                    if (!empty($json_data)) {
                        $check_result = $this->Assessment->checkData($json_data);
                        if ($check_result !== true) {
                            print $result['Assessment']['id'] . "\n\n";
                            print json_encode($json_data) . "\n\n";
                            print $check_result . "\n";
                            exit(-1);
                        }

                    }
				}

                $minID = $results[count($results) - 1]['Assessment']['id'];
			}
		} while (count($results) >= $chunkSize);

        $chunkSize = 100;
		
		$minID = 0;
		do {
			$this->Sitting->contain();
			$results = $this->Sitting->find('all', array(
				'conditions' => array('Sitting.id >' => $minID),
				'order' => array('Sitting.id'),
				'limit' => $chunkSize
			));

			if (!empty($results)) {
				print "Reading Sitting " . $results[0]['Sitting']['id'] . " to " . 
					$results[count($results) - 1]['Sitting']['id'] . "\n";

				foreach ($results as $result) {
					$json_data = json_decode($result['Sitting']['json_data'], true);
                    if (!empty($json_data)) {
                        $check_result = $this->Sitting->checkData($json_data);
                        if ($check_result !== true) {
                            print $result['Sitting']['id'] . "\n\n";
                            print json_encode($json_data) . "\n\n";
                            print $check_result . "\n";
                            exit(-1);
                        }
                    }
				}

                $minID = $results[count($results) - 1]['Sitting']['id'];
			}
		} while (count($results) >= $chunkSize);

        $chunkSize = 100;
                
        $minID = 0;
        do {
            $this->Document->contain();
            $results = $this->Document->find('all', array(
                'conditions' => array('Document.id >' => $minID),
                'order' => array('Document.id'),
                'limit' => $chunkSize
            ));

            if (!empty($results)) {
                print "Reading Document " . $results[0]['Document']['id'] . " to " . 
                    $results[count($results) - 1]['Document']['id'] . "\n";

                foreach ($results as $result) {
                    $json_data = json_decode($result['Document']['layout_json'], true);
                    if (!empty($json_data)) {
                        $check_result = $this->Document->checkLayoutData($json_data);
                        if ($check_result !== true) {
                            print $result['Document']['id'] . "\n\n";
                            print json_encode($json_data) . "\n\n";
                            print $check_result . "\n";
                            exit(-1);
                        }
                    }
                }

                $minID = $results[count($results) - 1]['Document']['id'];
            }
        } while (count($results) >= $chunkSize);

        $chunkSize = 100;

        $minID = 0;
        do {
            $this->User->contain();
            $results = $this->User->find('all', array(
                'conditions' => array('User.id >' => $minID),
                'order' => array('User.id'),
                'limit' => $chunkSize
            ));

            if (!empty($results)) {
                print "Reading User " . $results[0]['User']['id'] . " to " . 
                    $results[count($results) - 1]['User']['id'] . "\n";

                foreach ($results as $result) {
                    $json_data = json_decode($result['User']['defaults'], true);
                    if (!empty($json_data)) {
                        $check_result = $this->User->checkDefaults($json_data);
                        if ($check_result !== true) {
                            print $result['User']['id'] . "\n\n";
                            print json_encode($json_data) . "\n\n";
                            print $check_result . "\n";
                            exit(-1);
                        }

                    }
                }

                $minID = $results[count($results) - 1]['User']['id'];
            }
        } while (count($results) >= $chunkSize);

        $chunkSize = 100;

        $minID = 0;
        do {
            $this->VersionData->contain();
            $results = $this->VersionData->find('all', array(
                'conditions' => array('VersionData.id >' => $minID),
                'order' => array('VersionData.id'),
                'limit' => $chunkSize
            ));

            if (!empty($results)) {
                print "Reading VersionData " . $results[0]['VersionData']['id'] . " to " . 
                    $results[count($results) - 1]['VersionData']['id'] . "\n";

                foreach ($results as $result) {
                    $json_data = json_decode($result['VersionData']['scoring_json'], true);
                    if (!empty($json_data)) {
                        $check_result = $this->VersionData->checkData($json_data);
                        if ($check_result !== true) {
                            print $result['VersionData']['id'] . "\n\n";
                            print json_encode($json_data) . "\n\n";
                            print $check_result . "\n";
                            exit(-1);
                        }
                    }
                }

                $minID = $results[count($results) - 1]['VersionData']['id'];
            }
        } while (count($results) >= $chunkSize);

    }
}
?>

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class FixDocumentOptionsShell extends AppShell {
	public $uses = array('User', 'Document');

    public function main() {

		$minID = 0;
        $chunkSize = 1000;

		do {
			$this->Document->contain();
			$results = $this->Document->find('all', array(
				'conditions' => array('Document.id >' => $minID),
				'order' => array('Document.id'),
				'limit' => $chunkSize
			));

			if (!empty($results)) {
				print "Reading Document " . $results[0]['Document']['id'] . " to " . 
					$results[count($results) - 1]['Document']['id'] . "\n";

				foreach ($results as $result) {
					$json_data = json_decode($result['Document']['layout_json'], true);
                    if (!empty($json_data) && ($json_data['StudentReports']['responses'] == 'none')) {
                        unset($json_data['StudentReports']['incorrectOnly']);

                        $check_result = $this->Document->checkLayoutData($json_data);
                        if ($check_result === true) {
                            $this->Document->id = $result['Document']['id'];
                            $this->Document->saveField('layout_json', json_encode($json_data), array(
                                'callbacks' => false
                            ));
                        } else {
                            print "Error in Document " . $result['Document']['id'] . "\n\n";
                            print $check_result . "\n\n";
                            print json_encode($json_data) . "\n";
                            exit(-1);
                        }
                    }
				}

                $minID = $results[count($results) - 1]['Document']['id'];
			}
		} while (count($results) >= $chunkSize);

    }
}
?>

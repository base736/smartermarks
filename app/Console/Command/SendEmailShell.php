<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('CakeEmail', 'Network/Email');

class SendEmailShell extends AppShell {
	public $uses = array('EmailJob');
	
    public function main() {
		$approot = Configure::read('approot');
		$lockFile = fopen($approot . "Console/email_send.lock", "c");
	    if ($lockFile !== false) {
		    if (flock($lockFile, LOCK_EX | LOCK_NB)) {

				$this->EmailJob->deleteAll('EmailJob.sent <= DATE_SUB(NOW(), INTERVAL 7 DAY)');
	
				do {
					$this->EmailJob->contain();
					$emailJob = $this->EmailJob->find('first', array(
						'conditions' => array('EmailJob.sent IS NULL'),
						'order' => array('EmailJob.created ASC')
					));
					if (!empty($emailJob)) {
	
						// Send oldest email
							
						$email = new CakeEmail('ses');
						
						$email->from(array('support@smartermarks.com' => 'SmarterMarks'));
						$email->emailFormat('html');
				
						$json_data = json_decode($emailJob['EmailJob']['json_data'], true);
				
						$errorMessage = false;
						try {

                            if (!empty($json_data['address'])) $email->to($json_data['address']);
                            if (!empty($json_data['viewVars'])) $email->viewVars($json_data['viewVars']);
                            if (!empty($json_data['subject'])) $email->subject($json_data['subject']);
                            if (!empty($json_data['messageName'])) $email->template($json_data['messageName']);

                            $response = $email->send();

						} catch (\Exception $e) {

							$errorMessage = $e->getMessage();
                            
						}
						usleep(150000); // Sleep for 0.15 seconds (twice what's needed) to avoid SES throttling
						
						$data = array();
						$data['id'] = $emailJob['EmailJob']['id'];
						$microtime = number_format(microtime(true), 6, '.', '');
						$now = DateTime::createFromFormat('U.u', $microtime);
						$data['sent'] = $now->format("Y-m-d H:i:s.u");
						$data['response'] = json_encode(array(
							'response' => $response,
							'error' => $errorMessage
						));
						$this->EmailJob->save($data);
					}
				} while (!empty($emailJob));
			
			}

			fclose($lockFile);
		}
	}
}
?>

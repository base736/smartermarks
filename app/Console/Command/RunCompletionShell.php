<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('Email', 'Lib');

class RunCompletionShell extends AppShell {
	public $uses = array('User', 'Question');

    public function submit() {
        $request_id = $this->args[0];
        $cache_data = Cache::read($request_id, 'completion_jobs');

        if (!empty($cache_data)) {
            if (empty($cache_data['status'])) return -1;
            if ($cache_data['status'] != 'pending') return -1;

            if (empty($cache_data['user_id'])) return -1;
            else $user_id = $cache_data['user_id'];

            if (empty($cache_data['request'])) return -1;
            else $request = $cache_data['request'];
    
            if (empty($cache_data['json_data'])) return -1;
            else $json_data = $cache_data['json_data'];
        } else return -1;

        $success = true;

        Cache::write($request_id, array(
            'status' => 'running',
            'user_id' => $user_id,
            'started' => date('Y-m-d H:i:s')
        ), 'completion_jobs');

        // Store old imports, then remove js and imports from engine

        if (empty($json_data['Engine']['data'])) {
            $json_data['Engine']['data'] = array(
                'constants' => array(),
                'randomized' => array(),
                'calculated' => array()
            );
        }

        $old_imports = $json_data['Engine']['Imports'];

        unset($json_data['Engine']['js']);
        unset($json_data['Engine']['Imports']);

        // Create and send completion request
        
        $json_string = json_encode($json_data);

        $system = file_get_contents(Configure::read('approot') . '/Prompts/versioning.txt');
        $prompt = $request . "\n\n" . $json_string;

        $secrets = Configure::read('secrets');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $secrets['openai']['completion_url']);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $secrets['openai']['secret_key']
        ));

        $post_data = array(
            'messages' => array(
                array(
                    'role' => 'system',
                    'content' => $system
                ),
                array(
                    'role' => 'user',
                    'content' => $prompt
                )
            ),
            'user' => strval($user_id)
        );

        $post_data += $secrets['openai']['completion_options'];

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));

        $result = curl_exec($ch);
        if (empty($result)) $success = false;

        // Extract content from response

        if ($success) {

            $response_data = json_decode($result, true);

            if (!empty($response_data['choices'][0]['message']['content'])) {
                $new_json_string = $response_data['choices'][0]['message']['content'];
                $new_json_data = json_decode($new_json_string, true);

                if (!empty($new_json_data)) {
                    $new_json_data = $this->Question->clean_json($new_json_data);
                } else $success = false;
            } else $success = false;

        } else $response_data = false;

        // Complete new engine -- rename variables and create js code

        if ($success) {
        
            $new_json_data['Engine']['Imports'] = array();
            foreach ($old_imports as $import_id => $import_entry) {
                $new_json_data['Engine']['Imports'][] = $import_id;
            }

            // Gather variable names from engine and check that each is valid (won't mess with regex later)

            $variable_names = array();
            if (!empty($new_json_data['Engine']['data'])) {
                foreach ($new_json_data['Engine']['data'] as $key => $entries) {
                    if (empty($entries)) unset($new_json_data['Engine']['data'][$key]);
                    foreach ($entries as $entry) {
                        $variable_names[] = $entry['variableName'];
                    }
                }
            }

            if ($success && !empty($variable_names)) {

                $new_json_string = json_encode($new_json_data);

                // Deal with common errors

                $new_json_string = str_replace('"value":"\u00f7"', '"value":"/"', $new_json_string);

                // Reassign variable names -- GPT often assigns meaningful names (which we don't want) or uses
                // reserved names.

                $variable_characters = str_split('abcdefghijklmnopqrstuvwxyz');
                shuffle($variable_characters);
        
                $excluded_names = array('t', 'e', 'do', 'if', 'in', 'for', 'int', 'let', 'new', 'try', 'var');
                $new_variable_map = array();

                foreach ($variable_names as $this_name) {
        
                    // Replace variable names with a temporary value so we can change them later
        
                    $regex1 = '/(data-variable-name=([\\\\]*"|\'))(' . $this_name . ')(\2)/';
                    $regex2 = '/(([\\\\]*"|\')variableName\2:\2)(' . $this_name . ')(\2)/';
                    $regex3 = '/(([\\\\]*"|\')type\2:\2Variable\2,\2value\2:\2)(' . $this_name . ')(\2)/';

                    $new_json_string = preg_replace($regex1, '$1_$3$4', $new_json_string);
                    $new_json_string = preg_replace($regex2, '$1_$3$4', $new_json_string);
                    $new_json_string = preg_replace($regex3, '$1_$3$4', $new_json_string);
        
                    // Get a new variable name
        
                    $index = -1;
                    $newVariable = false;
                    do {
        
                        $temp = ++$index;
                        $new_name = '';
                        do {
                            $i = $temp % count($variable_characters);
                            $new_name = $new_name . $variable_characters[$i];
                            $temp = floor($temp / count($variable_characters));
                        } while ($temp > 0);
                
                    } while (in_array($new_name, $excluded_names));
        
                    $new_variable_map['_' . $this_name] = $new_name;
                    $excluded_names[] = $new_name;
                }

                foreach ($variable_names as $this_name) {
                    $regex1 = '/(data-variable-name=([\\\\]*"|\'))(_' . $this_name . ')(\2)/';
                    $regex2 = '/(([\\\\]*"|\')variableName\2:\2)(_' . $this_name . ')(\2)/';
                    $regex3 = '/(([\\\\]*"|\')type\2:\2Variable\2,\2value\2:\2)(_' . $this_name . ')(\2)/';
        
                    $new_json_string = preg_replace_callback($regex1, function($matches) use ($new_variable_map) {
                        return $matches[1] . $new_variable_map[$matches[3]] . $matches[4];
                    }, $new_json_string);
        
                    $new_json_string = preg_replace_callback($regex2, function($matches) use ($new_variable_map) {
                        return $matches[1] . $new_variable_map[$matches[3]] . $matches[4];
                    }, $new_json_string);
        
                    $new_json_string = preg_replace_callback($regex3, function($matches) use ($new_variable_map) {
                        return $matches[1] . $new_variable_map[$matches[3]] . $matches[4];
                    }, $new_json_string);
                }

                $new_json_data = json_decode($new_json_string, true);
                if (!empty($new_json_data)) {

                    // Rebuild Javascript code

                    App::uses('Wizard', 'Lib');
                    $wizardData = $new_json_data['Engine']['data'];
                    $new_json_data['Engine']['js'] = Wizard::getCode($wizardData);

                } else $success = false;

            } else {

                unset($new_json_data['Engine']['data']);
                $new_json_data['Engine']['js'] = '';

            }
        }

        // Check that the result is valid question data

        if ($success) {
            $check_result = $this->Question->checkData($new_json_data);
            if ($check_result === true) {
                $new_json_data['Engine']['Imports'] = $old_imports;
            } else $success = false;
        }

        // Send an email

        if ($response_data !== false) {
            $this->User->contain();
            $user = $this->User->findById($user_id);

            Email::send($secrets['admin_email'], "SmarterMarks: Smart version used", "smart_versioning", 
                array('data' => array(
                    'User email' => $user['User']['email'],
                    'User prompt' => $prompt,
                    'Result' => $response_data,
                    'Success' => $success ? 'true' : 'false'
                ))
            );    
        }

        if ($success) {

            $json_string = json_encode($new_json_data);

            Cache::write($request_id, array(
                'status' => 'success',
                'user_id' => $user_id,
                'data_string' => $json_string,
                'data_hash' => md5($json_string)
            ), 'completion_jobs');

        } else {

            Cache::write($request_id, array(
                'status' => 'failed',
                'user_id' => $user_id
            ), 'completion_jobs');

        }

        return 0;
    }
}
?>

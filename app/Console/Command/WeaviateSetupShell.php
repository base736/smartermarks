<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

class WeaviateSetupShell extends AppShell {
	public $uses = array('SearchData', 'Question');

    public function main() {
        $secrets = Configure::read('secrets');
        $weaviate_url = $secrets['weaviate']['url'];

        // Create schema

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $weaviate_url . '/v1/schema');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $post_data = array(
            'class' => 'Question',
            'description' => 'Assessment questions',
            'properties' => array(
                array(
                    'name' => 'question_content_id',
                    'description' => 'Content ID for the whole question',
                    'dataType' => array('text')
                ),
                array(
                    'name' => 'part_content_id',
                    'description' => 'Content ID for this question part',
                    'dataType' => array('int')
                ),
                array(
                    'name' => 'keywords',
                    'description' => 'Keywords JSON',
                    'dataType' => array('text')
                ),
                array(
                    'name' => 'parts_count',
                    'description' => 'Number of parts in this question',
                    'dataType' => array('int'),
                    'indexSearchable' => false
                ),
                array(
                    'name' => 'type',
                    'description' => 'Type for this question part',
                    'dataType' => array('text'),
                    'indexSearchable' => false
                ),
                array(
                    'name' => 'folder_ids',
                    'description' => 'Folders in which this question appears',
                    'dataType' => array('int[]'),
                    'indexSearchable' => false
                ),
                array(
                    'name' => 'is_open',
                    'description' => 'Indicates that this question is in the open bank',
                    'dataType' => array('boolean'),
                    'indexSearchable' => false
                )
            ),
            'vectorIndexConfig' => array(
                'distance' => 'dot'
            ) 
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));

        $result = curl_exec($ch);
        if (!empty($result)) {
            print "Schema created.\n";
        } else {
            print "Error creating schema.\n";
            exit(-1);
        }
/*
        // Add existing SearchData entries to database

        $post_data = array('objects' => array());

        $chunkSize = 100;
            
        $minID = 0;
        do {
            $this->SearchData->contain();
            $results = $this->SearchData->find('all', array(
                'conditions' => array('SearchData.id >' => $minID),
                'order' => array('SearchData.id'),
                'limit' => $chunkSize
            ));

            if (!empty($results)) {
                print "Reading SearchData " . $results[0]['SearchData']['id'] . " to " . 
                    $results[count($results) - 1]['SearchData']['id'] . "\n";

                foreach ($results as $result) {
                    $question_content_id = $result['SearchData']['question_content_id'];
                    $vector_data = json_decode($result['SearchData']['vectors_json'], true);

                    $this->Question->contain();
                    $question = $this->Question->find('first', array(
                        'conditions' => array('Question.question_content_id' => $question_content_id),
                        'order' => array('Question.stats_discrimination DESC')
                    ));

                    if (!empty($vector_data) && !empty($question)) {

                        $best_quality = $question['Question']['stats_discrimination'];

                        $folder_strings = explode(',', $result['SearchData']['folder_ids']);
                        $folder_ids = array_map(function ($a) { return intval($a); }, $folder_strings);
                        $is_open = ($result['SearchData']['is_open'] == 1);

                        // Update Weaviate database. Outcomes are weighted higher to ensure that expected terms are emphasized 
                        // when they're present, but the question itself can be used when they're not, or when they're insufficient.
        
                        $vector_weights = array(
                            'part_text' => 0.3,
                            'outcomes' => 0.7
                        );
        
                        foreach ($vector_data as $entry) {
                            $part_content_id = $entry['part_content_id'];

                            $combined_vector = false;
                            $total_weight = 0.0;
                            foreach ($vector_weights as $key => $weight) {
                                $is_empty = false;
                                if (empty($entry[$key])) $is_empty = true;
                                else if ($weight == 0.0) $is_empty = true;
            
                                if (!$is_empty) {
                                    if ($combined_vector === false) {
                                        $combined_vector = array();
                                        for ($i = 0; $i < count( $entry[$key]['vector']); ++$i) {
                                            $combined_vector[] = $weight * $entry[$key]['vector'][$i];
                                        }
                                    } else if (count($combined_vector) == count($entry[$key]['vector'])) {
                                        for ($i = 0; $i < count($entry[$key]['vector']); ++$i) {
                                            $combined_vector[$i] += $weight * $entry[$key]['vector'][$i];
                                        }    
                                    } else {
                                        $combined_vector = false;
                                        break;
                                    }
                                    $total_weight += $weight;
                                }    
                            }
            
                            if ($combined_vector !== false) {
                                    
                                // Get quality weighted vector. Quality weight: 0.2 results in many off-topic WR results with 
                                // high discrimination for "social studies 20 identity"; 0.1 is better but still some bad results.

                                $quality_weight = 0.08;
            
                                $best_vector = array();
                                $scale = exp($quality_weight * $best_quality) / $total_weight;
                                foreach ($combined_vector as $value) $best_vector[] = $scale * $value;

                                if (count($best_vector) != 1536) {
                                    print $result['SearchData']['id'] . "\n";
                                    exit(-1);
                                }

                                $post_data['objects'][] = array(
                                    'class' => 'Question',
                                    'properties' => array(
                                        'question_content_id' => $question_content_id,
                                        'part_content_id' => $entry['part_content_id'],
                                        'keywords' => $result['SearchData']['keywords_string'],
                                        'parts_count' => count($vector_data),
                                        'type' => substr($entry['part_type'], 0, 2),
                                        'folder_ids' => $folder_ids,
                                        'is_open' => $is_open
                                    ),
                                    'vector' => $best_vector
                                );
                            }
                        }
                    }
                }

                $minID = $results[count($results) - 1]['SearchData']['id'];
            }

            if ((count($post_data['objects']) >= 100) || empty($results)) {

                // Add new items as a batch

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $weaviate_url . '/v1/batch/objects?consistency_level=ALL');
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json'
                ));
        
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        
                $curl_result = curl_exec($ch);

                $post_data['objects'] = array();
            }
        } while (count($results) >= $chunkSize);
*/
    }

}

?>

<?php

/**************************************************************************************************/
/* SmarterMarks                                                                                   */
/* Copyright (C) 2012-2025 SmarterMarks Inc.                                                      */
/*                                                                                                */
/* This program is free software; you can redistribute it and/or modify it under the terms of the */
/* GNU General Public License as published by the Free Software Foundation; either version 3 of   */
/* the License, or (at your option) any later version.                                            */
/*                                                                                                */
/* You should have received a copy of the GNU General Public License along with this program. If  */
/* not, see <http://www.gnu.org/licenses/>                                                        */
/**************************************************************************************************/

App::uses('Email', 'Lib');

class RescueCacheShell extends AppShell {
	public $uses = array('ResponseOnline', 'ResultData');
	
    public function main() {
		$approot = Configure::read('approot');
		$minTime = date('Y-m-d H:i:s', strtotime("-1 week"));
		$maxTime = date('Y-m-d H:i:s', strtotime("-12 hours"));

		$this->ResponseOnline->includeDeleted();
		
		$attempt_ids = $this->ResponseOnline->find('list', array(
			'fields' => array('ResponseOnline.id'),
			'conditions' => array(
				'ResponseOnline.created >=' => $minTime,
				'ResponseOnline.created <=' => $maxTime
			)
		));

		$approot = Configure::read('approot');
		$numRescued = 0;
		foreach ($attempt_ids as $attempt_id) {
			$cache_data = Cache::read($attempt_id, 'responses_online');
			if (!empty($cache_data)) {
				$this->ResponseOnline->contain('ResultData');
				$old_data = $this->ResponseOnline->findById($attempt_id);
				$old_saved = strtotime($old_data['ResponseOnline']['last_save']);
				$new_saved = strtotime($cache_data['last_save']);
				if (($new_saved > $old_saved) && ($old_data['ResultData']['results'] != $cache_data['results'])) {
					$numRescued++;
					file_put_contents($approot . 'tmp/logs/rescued.txt', $attempt_id . "\n", FILE_APPEND);
					file_put_contents($approot . 'tmp/logs/rescued.txt', json_encode($old_data) . "\n", FILE_APPEND);
					file_put_contents($approot . 'tmp/logs/rescued.txt', json_encode($cache_data) . "\n", FILE_APPEND);
					$this->ResponseOnline->saveCacheData($cache_data);
				}
			}
		}

		$this->ResponseOnline->excludeDeleted();

		if ($numRescued > 0) {
			file_put_contents($approot . 'tmp/logs/rescued.txt', date('Y-m-d H:i:s') . ": Rescued " . $numRescued . " attempts.\n", FILE_APPEND);
		}
	}
}
?>
